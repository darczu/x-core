clear; clc; close all;
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load initial data
% also testing script
%run INPUT_SETUP.m  %initial setup
%addpath('./Database/Density/FIGS/');  %Add MAT folder


XCore(); %Load package
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Basic examples
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Density
T = linspace(300,3000,5);

%% 
rho_UO2(T,'Carbajo')
XCore('rho_UO2',T,'Carbajo')

%%
rho_PuO2(T,'Carbajo')
XCore('rho_PuO2',T,'Carbajo')

%%
XCore('rho_AIC',T)
XCore('rho_AIR',T)

%%
XCore('rho_B2O3',T, 'Kolev')
XCore('rho_B4C', [1000 600], 'MATPRO')
%%
rho_B4C([1000 1500],'MATPRO')
XCore('rho_CON',3000)
%%
rho_CS(T,'MELCOR')
XCore('rho_CS',T,'MELCOR')
XCore('rho_FE',T)
XCore('rho_FPO2',T)
XCore('rho_FP',T)
%%
rho_GRP(T,'MELCOR')

%%
rho_He(T,'MHTGR')

%%



XCore('rho_UO2',2000, 'MELCOR')
XCore('k_UO2',5000)
XCore('k_ZRO2',5000)
XCore('k_ZR',274)
XCore('k_SS',3000)
XCore('k_SS304',301)
XCore('k_He',300,'Wallenius')



XCore('rho_UO2',1000,'Carbajo')


% XCore('rho_UO2',{[1000, 2000, 3000] 'Carbajo'})
% T=linspace(300,3000);  plot(T,XCore('rho_UO2', {T 'Carbajo'}),'.')
%
% Example 1: Density of Uranium Dioxide in 2500K - both produce same result
% rho = XCore('rho_UO2',[2500]) 
% rho = rho_UO2(2500)
%
% XCore('Num_Re',10,0.2,0.1,1e-5)
% XCore('Num_Nu',{1,'sqr',-1})
% XCore('Num_Re',10,0.2,0.1,1e-5)
% XCore('Num_Nu',{1,'sqr',-1})
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% XCore('rho_UO2',{[1000, 2000, 3000] 'Carbajo'})
% T=linspace(300,3000);  plot(T,XCore('rho_UO2', {T 'Carbajo'}),'.')
%
% Example 1: Density of Uranium Dioxide in 2500K - both produce same result
% rho = XCore('rho_UO2',[2500]) 
% rho = rho_UO2(2500)
%
% XCore('Num_Re',10,0.2,0.1,1e-5)
% XCore('Num_Nu',{1,'sqr',-1})
% XCore('Num_Re',10,0.2,0.1,1e-5)
% XCore('Num_Nu',{1,'sqr',-1})
%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Output=XCore('function',[input paramers])
% Output=XCore('function',[options])
% Examples - Test cases:
% XCore('Num_Re',{10,0.2,0.1,1e-5})
% XCore('Num_Nu',{1,'sqr',-1})
% XCore('rho_AIC',{1000,[]})  or % XCore('rho_AIC',1000)  or % XCore('rho_AIC',[1000])
% XCore('rho_B2O3',{2222 'Kolev'})
% XCore('rho_UO2',{2000, 'MELCOR'})
% XCore('k_UO2',5000)
% XCore('k_ZRO2',5000)
% XCore('k_ZR',274)
% XCore('k_SS',3000)
% XCore('k_SS304',301)
% XCore('k_He',{300 'Wallenius'})
% XCore('rho_B4C', {[1000 600] 'MATPRO'}) or rho_B4C([1000, 600],'MATPRO')
% XCore('rho_UO2',{1000 'Carbajo'})
% XCore('rho_UO2',{[1000, 2000, 3000] 'Carbajo'})
% T=linspace(300,3000);  plot(T,XCore('rho_UO2', {T 'Carbajo'}),'.')

% NOT RECOMMENDED - STILL NOT DEVELOPED PROPERLY
% IT IS RECOMMENDED TO USE FUNCTIONS DIRECTLY: E.G:
% rho_UO2(T,'Carbajo')
