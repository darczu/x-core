%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MAIN FUNCTION - Revision 21.02.2021
% X-Steam stylized way to call X-Core package functions: Output = XCore('function', in1, in2, ....)
% Function calls other functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Currently it works for density and specific heat capacity  rho_ and Cp_ functions only
% To used other functions user has to call them directly
% type dir **/*.m to see the list of available m-files
% If the package was not added to you path type:
% XCore()        - it loads all folders and all functions are avialable. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function out = XCore(fun_name,in)
% output - vector
% fun_name - name of the function
% in - variable number of inputs, vectors, matrices or cells
% type help fun_name to obtain info about functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Examples:
% Output=XCore('function', in1, in2, ....)
% XCore()        - load all folders and all functions are avialable.  
% XCore('rho_UO2',[1000 1500],'Carbajo')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%function out = XCore(fun_name,in)
function out = XCore(fun_name,varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Solution: 
%https://uk.mathworks.com/help/matlab/ref/varargin.html
%fun_name
%varargin
in = varargin;
%varargin{:};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD PACKAGE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(nargin < 1)
	run INPUT_SETUP.m;

	disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
	disp('X-Core activated');
	disp('X-Core path was loaded');
	disp('You can run it without XCore function (it is recommended for BETA version)');
	disp('type: help LIST_OF_FUNCTIONS.m  to see list of available functions (Alternative).');
	disp('type: dir **/*.m    to see list of available functions.');
	disp('type: help <function_name> to see its description.');
	disp('For example UO2 density. Type:  help rho_UO2  ');
	disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN VARIOUS FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
else
	%N = numel(in);	%Size of input

	%run CRIT_NUM.m			%Crtiterial Numbers
	run DENSITY.m			%Densities 
	run TH_COND.m			%Thermal Conductivities
	run SPEC_HEAT_CAP.m		%Spec. Heat Capacities
	% run DYN_VISC.m			%Dynamic Visocisites

	% Melting Temp
	% Latent Heat of Fusion
	% Specific Enthalpy
	% Temper as a function of enthalpy
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

