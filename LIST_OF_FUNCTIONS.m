% OLD FILE - NOT UPDATED SINCE LONG TIME
% List of functions
% dir **/*.m
%
% Files Found in Current Folder:
%
dir **/*.m


%{
Files Found in Current Folder 18-02-2021:

Example_FPT1.m              INPUT_SETUP.m               MAIN_XCore.m                
Example_RHO_PLOTTER_Rev1.m  LIST_OF_FUNCTIONS.m         XCore.m                     

Files Found in: Database

CRIT_NUM.m       DENSITY.m        DYN_VISC.m       SPEC_HEAT_CAP.m  TH_COND.m        

Files Found in: Database\CommonFunctions

BisecSearch.m         POST_PROCESSING3.m    getGlobalx.m          plot_arc.m            setGlobalx.m          
PHYSICAL_CONSTANTS.m  SH_FILE_GEN.m         normalization.m       plot_arc2.m           

Files Found in: Database\CommonFunctions\FOLDERS_AND_FILES_OPERATIONS

copy_and_rename_folder.m  create_folders.m          detectfile.m              linecount.m               
count_lines.m             create_folders_example.m  folder_gen.m              

Files Found in: Database\CommonFunctions\PLOTTING

POST_PROCESSING3.m      plot_arc.m              standard_plot1.m        standard_plot1v3.m      standard_plot2old.m     standard_plot2v4.m      
colormapTestImage.m     plot_arc2.m             standard_plot1old.m     standard_plot1v4.m      standard_plot2v1.m      standard_plot2v4old.m   
colormaps_and_labels.m  standard_plot01.m       standard_plot1v1.m      standard_plot1v4_old.m  standard_plot2v2.m      varycolor.m             
linespacer.m            standard_plot02.m       standard_plot1v2.m      standard_plot2.m        standard_plot2v3.m      

Files Found in: Database\CommonFunctions\PLOTTING\Publication_Quality_Graphics

Publication_Quality_Graphics.m  

Files Found in: Database\CommonFunctions\PLOTTING\SHADDED-ERROR-BAR

demo_shadedErrorBar.m  shadedErrorBar.m       

Files Found in: Database\ComplexFunctions

A_mix.m                  MAT_DATABASE.m           MAT_LIST_TRANSFORM.m     N_mix.m                  V_mixMAT.m               rho_liq_pT.m             wfaf_chem.m              
A_mixMAT.m               MAT_DATABASE_INPUT.m     MAT_MELT_DATABASE.m      N_mixMAT.m               ZAID2A.m                 rho_mix.m                
CORIUM_MASS2N.m          MAT_LIST.m               MAT_MELT_DATABASE_OLD.m  READ_XSDATA.m            ZAID2Z.m                 rho_mixMAT.m             
DENSITY_BASE.m           MAT_LIST_REMOVAL.m       MOLAR_BASE.m             T_fuel_melt.m            af2wf.m                  wf2af.m                  

Files Found in: Database\EBEPU\workshop2020\ans666-pde-solver-ref

IN_REACTIVITY.m     PDE.m               PDE_SIMPLE.m        RHS_PDE.m           RHS_PDE_EXPLICIT.m  

Files Found in: Database\EBEPU\workshop2020\test1

IN_REACTIVITY.m     PDE.m               PDE_SIMPLE.m        RHS_PDE.m           RHS_PDE_EXPLICIT.m  

Files Found in: Database\Geometry\PebbleBeds\PebbleBedGeneratorModule_PD

3d_pbed.m       DBGen_module.m  

Files Found in: Database\Geometry\PebbleBeds\PebbleBedGeneratorModule_PD\MG_mods

3d_pbed.m        DBGen_module.m   DBGen_module1.m  

Files Found in: Database\Geometry\PebbleBeds\PebbleBedGenerator_PK

BoucingBalls.m                  bubbleplot3.m                   create_ball_mass.m              vector_randomization.m          
DataCheck.m                     calculate_balls_collision.m     create_ball_radius.m            
MasterThesis.m                  calculate_geometry_collision.m  create_balls.m                  

Files Found in: Database\MLC_package\PostProcessing\CSV_processing

MERGER_CSV_Rev1.m      MERGER_CSV_Rev1_old.m  mergeCSV.m             

Files Found in: Database\MLC_package\PostProcessing\MATLAB_MELCOR_FILE_MERGER

Merger.m   Merger2.m  Merger3.m  merge.m    merge2.m   

Files Found in: Database\MLC_package\PostProcessing\MATLAB_MELCOR_RESULTS_VISUALIZATION\forever

EDF_LOAD_FOREVER_v1.m             NARSIS_PLOT_TOOL_FOREVER_VER10.m  PLOT_TH_FOREVER_v1.m              

Files Found in: Database\MLC_package\PostProcessing\MATLAB_MELCOR_RESULTS_VISUALIZATION\fpt1

EDF_LOAD.m               PLOT.m                   PLOT_IC.m                PLOT_SS.m                PLOTv8.m                 
MERGER_CSV_Rev1.m        PLOT_CONT_TH.m           PLOT_RCS_TH.m            PLOT_TH.m                mergeCSV.m               
MLC_EDF_GEN_LP_4_2019.m  PLOT_DEP.m               PLOT_RN_6.m              PLOT_TH_OLD.m            

Files Found in: Database\MLC_package\PostProcessing\MATLAB_MELCOR_RESULTS_VISUALIZATION\genII

EDF_LOAD_GENII_v3.m             NARSIS_PLOT_TOOL_GENII_VER13.m  PLOT_CORE_GENII_v3.m            PLOT_TEMPS_v3.m                 
NARSIS_PLOT_TOOL_GENII_VER11.m  PLOT_CAV_GENII_v3.m             PLOT_RCS_GENII_v3.m             PLOT_TH2.m                      
NARSIS_PLOT_TOOL_GENII_VER12.m  PLOT_CONT_GENII_v3.m            PLOT_SIS_GENII_v3.m             create_folders_genII_2.m        

Files Found in: Database\MLC_package\PostProcessing\MATLAB_MELCOR_RESULTS_VISUALIZATION\genIII

EDF_LOAD.m                               NARSIS_EDF_READ_VER9.m                   PLOT_TH.m                                standard_plot1v1.m                       
FILES_DATA.m                             NARSIS_EDF_reader8_LUNCHPAD_FULL_ver9.m  RN_1ver4.m                               standard_plot2v1.m                       
MAAP_DATA.m                              PLOT_CORE.m                              files.m                                  
MAAPdata.m                               PLOT_MCCI.m                              mergeCSV.m                               
MERGER_CSV.m                             PLOT_RN.m                                read2_302.m                              
MERGER_CSV_Rev1.m                        PLOT_SS.m                                sec2hms.m                                

Files Found in: Database\MLC_package\PostProcessing\MATLAB_MELCOR_RESULTS_VISUALIZATION\genIII\LEGACY

EDF_reader8.m                     LBLOCA_ERMSAR_Rev1.m              RN_1ver4.m                        read2_195snap.m                   readLOCARST.m                     
EDF_reader8_LUNCHPAD_FULL_ver4.m  LBLOCA_POSTER.m                   files.m                           read2_215.m                       readRST.m                         
EDF_reader8_LUNCHPAD_FULL_ver6.m  MAAPdata.m                        plot_7full_boron_2018.m           read2_215_ver2.m                  sec2hms.m                         
EDF_reader8_LUNCHPAD_FULL_ver7.m  PLOT_CONT_TH.m                    plotplot.m                        read2_230.m                       standard_plot1.m                  
EDF_reader8_LUNCHPAD_FULL_ver8.m  PLOT_RCS_TH.m                     read.m                            read2_250.m                       standard_plot2.m                  
EDF_reader8_LUNCHPAD_FULL_ver9.m  PLOT_RPV_TH.m                     read2.m                           read2_95C.m                       
EDF_reader8_SNAP.m                RN_1.m                            read2_191_E.m                     read2_LOCA.m                      
EDF_reader8_SNAP_FULL.m           RN_1ver2.m                        read2_195.m                       read2_LOOP.m                      
LBLOCA_ERMSAR.m                   RN_1ver3.m                        read2_195backup.m                 readLOCA.m                        

Files Found in: Database\MLC_package\PostProcessing\MATLAB_MELCOR_RESULTS_VISUALIZATION\genIII\LOOP\case0002_B4

MERGER_CSV_Rev1.m  

Files Found in: Database\MLC_package\PostProcessing\MATLAB_MELCOR_RESULTS_VISUALIZATION\genIII\LOOP\case0002_B5

MERGER_CSV_Rev1.m  

Files Found in: Database\MLC_package\PostProcessing\MATLAB_MELCOR_RESULTS_VISUALIZATION\marv

FIGURESv3.m       INITIAL_READ.m    INITIAL_READv3.m  PLOTv2.m          PLOTv3.m          

Files Found in: Database\MLC_package\PostProcessing\OLD_MATLAB_MELCOR_MCCI_VISUALIZATION

MCCI_visu.m          MCCI_visu2.m         MCCI_visu3.m         MCCI_visu3_LBLOCA.m  MCCI_visu3_SBO.m     

Files Found in: Database\MLC_package\PostProcessing\OLD_MATLAB_MELCOR_STEADY_STATE

SS_Reader.m  

Files Found in: Database\MLC_package\PreProcessing\MATLAB_MELCOR_COR_GEN

CORE_GEN_AREAVOLMASS.m   CORE_GEN_OTH.m           INPUT_DATA_GenII.m       INPUT_DATA_GenII_FL.m    INPUT_DATA_MAIN_GenII.m  MAIN_CORE_GEN.m          
CORE_GEN_MELCOR.m        CORE_GEN_POWERDIST.m     INPUT_DATA_GenII_CVH.m   INPUT_DATA_GenII_HS.m    INPUT_DATA_MAPS_GenII.m  

Files Found in: Database\MLC_package\PreProcessing\MATLAB_MELCOR_COR_GEN\Subroutines\MLC

MLC_LH.m  

Files Found in: Database\MLC_package\PreProcessing\MATLAB_MELCOR_COR_GEN\Subroutines\MLC\USELESS

MLC_CORVOL.m          MLC_CVH_READ.m        MLC_MASS_MAIN_READ.m  MLC_MPOOL_READ.m      
MLC_COR_READ.m        MLC_DATA_READ.m       MLC_MASS_READ.m       MLC_TEMP_READ.m       

Files Found in: Database\MLC_package\PreProcessing\MATLAB_MELCOR_CVH_HS_FL_GEN

ALL_READ.m                 ALL_WRITE.m                COUNT_LINES.m              MAIN_GENERATOR.m           MAIN_GENERATOR_MARVIKEN.m  

Files Found in: Database\MLC_package\PreProcessing\MATLAB_MELCOR_CVH_HS_FL_GEN\OLDER_CODES

Code_SARCAM_LIV.m              Generator_ver3_4CV.m           INPUT_SETUP.m                  MLCmodule_EDF_ver_CORE_4CV.m   OUTPUT_CONTROL.m               
Generator_ver3.m               INPUT_MAIN.m                   MLCmodule_EDF_ver_CORE_16CV.m  Main_2016.m                    

Files Found in: Database\MLC_package\PreProcessing\MATLAB_MELCOR_EDF_GEN

MLC_EDF_GEN_LP_4_2019.m           MLC_EDF_GEN_LP_4_2019_GenIII.m    MLC_EDF_GEN_LP_4_2019_GenII_LH.m  

Files Found in: Database\MLC_package\PreProcessing\MATLAB_MELCOR_EDF_GEN\case1

MLC_EDF_GEN_LP_4_2019.m  

Files Found in: Database\MLC_package\PreProcessing\MATLAB_MELCOR_LHC_GEN

INPUT_DATA_LHC_GenIII.m  LHC_GEN_GEO.m            MAIN_LHC_GEN.m           

Files Found in: Database\MLC_package\Running

RUNNER_MELCORv3_FPT1.m      RUNNER_MELCORv5_MARVIKEN.m  create_folders_genII.m      
RUNNER_MELCORv5.m           RUNNER_MELCORv5_NARSIS.m    create_folders_genII_2.m    

Files Found in: Database\Mechanical

Examples1_15Mo3.m  

Files Found in: Database\Mechanical\ElasticModulus

E_SS.m  

Files Found in: Database\Mechanical\LarsonMiller

LMP_15Mo3_data.m  

Files Found in: Database\Mechanical\YieldStrength

Yield_Strength_Fit.m  sigmaY_SS.m           

Files Found in: Database\ReactorPhysics\Boron

PPM_INTEGRATION.m  boron_calc.m       

Files Found in: Database\ReactorPhysics\PowerDistribution

Examples1.m           Examples2.m           map2radial.m          quarter2map.m         radial2map.m          radial_axial2map3D.m  

Files Found in: Database\ReactorPhysics\PowerDistribution\hexagonal

example.m         example1.m        example_ver2.m    example_ver3.m    hexScatter.m      hexScatterDemo.m  input_core0.m     oxide_core0.m     

Files Found in: Database\S&UA

SUA_Distributions_v1.m  SUA_Sampling_v1.m       SUA_VerifyPlot_v4.m     plot_sample2.m          
SUA_MainGenerator_v1.m  SUA_Save2File_v1.m      plot_sample1.m          

Files Found in: Database\S&UA\LEGACY

SUA_Distributions_v1.m  SUA_Sampling_v1.m       SUA_VerifyPlot_v4.m     plot_sample2.m          
SUA_MainGenerator_v1.m  SUA_Save2File_v1.m      plot_sample1.m          

Files Found in: Database\S&UA\Probability_and_Statistics

log_triangular_cdf.m  log_triangular_pdf.m  

Files Found in: Database\S&UA\Probability_and_Statistics\TruncatedGaussian

TruncatedGaussian.m  

Files Found in: Database\S&UA\Probability_and_Statistics\lhsgeneral

lhsgeneral.m  

Files Found in: Database\S&UA\Probability_and_Statistics\randraw

randraw.m  

Files Found in: Database\ThermalHydraulics\CriterialNumbers

Num_Nu.m    Num_Re.m    Num_Re_s.m  

Files Found in: Database\ThermalHydraulics\CriticalFlow

CritFlowHEM.m  

Files Found in: Database\ThermalHydraulics\Density

DEN_THEXP.m   rho_B2O3.m    rho_CON.m     rho_FP.m      rho_He.m      rho_Na.m      rho_PuO2.m    rho_ThPuO2.m  rho_UO2.m     rho_liq_pT.m  
rho_AIC.m     rho_B2ZR.m    rho_CS.m      rho_FPO2.m    rho_INC.m     rho_PAINT.m   rho_SS.m      rho_ThUO2.m   rho_ZR.m      
rho_Air.m     rho_B4C.m     rho_FE.m      rho_GRP.m     rho_MOX.m     rho_Pb.m      rho_SSOX.m    rho_U.m       rho_ZRO2.m    

Files Found in: Database\ThermalHydraulics\Density\LEGACY

rho_SS304.m  rho_SS316.m  

Files Found in: Database\ThermalHydraulics\DynamicViscosity

mi_He.m  mi_Na.m  mi_Pb.m  

Files Found in: Database\ThermalHydraulics\Hydraulic

A2D.m           Dhyd.m          FL_LEN.m        HS_ND.m         HS_ND_OLD.m     MLC_LH.m        SphereCalc.m    f_CW.m          
D2A.m           Example_FPT1.m  FL_USL.m        HS_ND_CHECK.m   K_form.m        PipeVol.m       SphericalCap.m  fun_CW.m        

Files Found in: Database\ThermalHydraulics\Hydraulic\LEGACY\MATLAB_PUMP_Calculator

pump1.m  

Files Found in: Database\ThermalHydraulics\Hydraulic\LEGACY\MATLAB_PUMP_Calculator\Kod_hydrauliczny

Dhydro.m         DhydroO.m        Obstacle_ver1.m  Reynolds.m       fCW.m            formloss.m       funCW.m          

Files Found in: Database\ThermalHydraulics\Hydraulic\LEGACY\MATLAB_PUMP_Calculator\Kod_hydrauliczny\LEGACY

hydro1.m  

Files Found in: Database\ThermalHydraulics\Oxidation

graph_5curves_log_time.m  graph_zr_curves.m         

Files Found in: Database\ThermalHydraulics\SpecificHeatCapacity

Cp_AIC.m    Cp_B4C.m    Cp_CS.m     Cp_He.m     Cp_Na.m     Cp_Pb.m     Cp_SS304.m  Cp_SSOX.m   Cp_UO2.m    Cp_ZRO2.m   
Cp_AIR.m    Cp_CON.m    Cp_GRP.m    Cp_INC.m    Cp_PAINT.m  Cp_SS.m     Cp_SS316.m  Cp_U.m      Cp_ZR.m     

Files Found in: Database\ThermalHydraulics\ThermalConductivity

k_AIC.m         k_B4C.m         k_GRP.m         k_INC.m         k_PAINT.m       k_Pu_delta.m    k_SS304.m       k_U.m           k_UO2.m         k_porosity.m    
k_AIR.m         k_CON.m         k_H.m           k_MOX.m         k_Pb.m          k_Pu_epsilon.m  k_SS316.m       k_U10Zr.m       k_ZR.m          
k_Am.m          k_CS.m          k_He.m          k_Na.m          k_PuN.m         k_SS.m          k_SSOX.m        k_UN.m          k_ZRO2.m        

Files Found in: Database\ThermalHydraulics\VapourPressure

VAPOUR_PRESSURE.m       VAPOUR_PRESSURE_2019.m  

Files Found in: Database\ThermalHydraulics\XSteam

XSteam.m    XSteamUS.m  example.m   

Files Found in: Documentation

HISTORY.m      LEGACY_CODE.m  REFERENCES.m   

%}