% You can simple add it to matlab Path -> ADD path and all subfolders.
% Script to load all folders
% SETUP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
format long g


% REMOVE ALL FOLDERS: 
%rmpath(genpath('.\'))

% CURRENT LIST OF REMOVED FOLDER IN .gitignore file
addpath(genpath('./')); % Add main folder all subfolders
rmpath(genpath('./LEGACY')); %Remove legacy folder
rmpath(genpath('./.git')); %Remove git folder
rmpath(genpath('./ToolBoxInstallationFile')); %Remove git folder
rmpath(genpath('LEGACY')); %Remove legacy folder

%rmpath(genpath('.\..\LEGACY')); %Remove legacy folder
%rmpath(genpath('./../LEGACY')); %Remove legacy folder
%rmpath('./../LEGACY'); %Remove legacy folder

%rmpath(genpath('.  '));

rmpath(genpath('.\Modules\LEGACY'));
rmpath(genpath('./Modules/MaterialDatabase/LEGACY'));
rmpath(genpath('./Modules/CommonFunctions/LEGACY'));
rmpath(genpath('.\Modules\MaterialDatabase\Density\LEGACY'));
rmpath(genpath('.\Modules\MaterialDatabase\Density\FIGS'));
rmpath(genpath('.\Modules\MaterialDatabase\Mechanical\LEGACY'));
rmpath(genpath('.\Modules\MLC_package\PostProcessing\OLD_MATLAB_MELCOR_MCCI_VISUALIZATION'));
rmpath(genpath('.\Modules\MLC_package\PostProcessing\OLD_MATLAB_MELCOR_STEADY_STATE'));
rmpath(genpath('.\Modules\MLC_package\Running\LEGACY'));
rmpath(genpath('.\Modules\MLC_package\SenstivityUncertanityAnalysis\LEGACY'));
rmpath(genpath('.\Modules\ThermalHydraulics\Hydraulic\LEGACY\MATLAB_PUMP_Calculator\Kod_hydrauliczny'));
%rmpath(genpath('.\Modules\ThermalHydraulics\Hydraulic'));
rmpath(genpath('.\Modules\MLC_package\SenstivityUncertanityAnalysis'));
rmpath(genpath('.\Modules\MLC_package\PreProcessing\MATLAB_MELCOR_EDF_GEN'));
rmpath(genpath('.\Modules\MLC_package\PreProcessing\MATLAB_MELCOR_CVH_HS_FL_GEN'));
rmpath(genpath('.\Modules\MLC_package\PreProcessing\MATLAB_MELCOR_COR_GEN'));
rmpath(genpath('.\Modules\MLC_package\PostProcessing\MATLAB_MELCOR_RESULTS_VISUALIZATION\marv'));
rmpath(genpath('.\Modules\MLC_package\PostProcessing\MATLAB_MELCOR_RESULTS_VISUALIZATION\genIII'));
rmpath(genpath('.\Modules\MLC_package\PostProcessing\MATLAB_MELCOR_RESULTS_VISUALIZATION\genII'));
% SOME ARTIFACT:
%{
%% WORKING FOLDERS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
addpath(['./Database/XSteam/']); 		   	% ADD PATH TO X-STEAM PACKAGE 
addpath(['./Database/']);				   	% Add database folder
addpath(['./Database/CommonFunctions/']);   % Commonly used functions
%addpath(['./Documentation/']);  
addpath(['./Database/Geometry']);			% Geometry routines
addpath(['./Database/Hydraulic/']); 		% Hydraulic foutines
addpath(['./Database/ComplexFunctions']);  	% Complicated functions
addpath(['./Database/CriterialNumbers/']);	% Critierial numbers  
addpath(['./Database/Density/']);			
addpath(['./Database/DynamicViscosity/']);
addpath(['./Database/SpecificHeatCapacity/']);
addpath(['./Database/ThermalConductivity/']);

%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%