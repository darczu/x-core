% Developed by Piotr Darnowski 2022, Institute of Heat Engineering, Warsaw University of Technology,
% APAL - Horizon 2020 Research Project
% Favor to matrix function
% Provide FAVOR results in form of a matrix 1xM. For example M=16*N data 
% Transform into useful data to Nx16
% A = rand(16*5,1)

function  [out] = favor2mat(Inp,Conv,TIME)

% FAVOR discretization
%N = 16;
N = 1671;

% Input vector is a set of stacked 1:16 vectors

switch (Conv)
    case 'KSI'   %convert [KSI] to pressure in [MPA]
        % [MPA] = 6.8947572932*[KSI]

        Inp =  6.8947572932.*Inp;
    
    case 'F'    %convert [F] to temperature in [C]
        % [degC] = (5/9)*([degF] - 32)
        Inp = (5./9).*(Inp-32.0);
        
    otherwise
        error('wong input');
end


M = numel(Inp)/N;


if floor(M)==ceil(M)
else
    error('Input numel has to be divisible by N');
end

%floor(M)==ceil(M)

out = reshape(Inp, [M, N]);



