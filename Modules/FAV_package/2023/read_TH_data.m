% Read data from file provided by other particiants
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [pres0, htc0, temp0] = read_TH_data(name, casename, folder, filename, sheet, variables, range, skip_step, flag)
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

filepath = [folder, '\', filename]

opts = detectImportOptions(filepath)
opts.Sheet = sheet
opts.SelectedVariableNames = variables
opts.DataRange = range

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data= readmatrix(filepath,opts);  % Read base case results
% M = readmatrix('basic_matrix.xls','Sheet','Sheet1','Range','B1:D3')
% DayOfMonth = readvars('airlinesmall_subset.xlsx','Sheet','2004','Range','C2:C11')
% https://ch.mathworks.com/help/matlab/import_export/add_spreadsheet_data_arrays_variables.html

% data(:,1)


pres     = data(:,1);  %Pa
time     = data(:,2);  %seconds
temp     = data(:,3); %degC
htc      = data(:,4); %W/m2/K

if strcmp(flag, 'FRA') |  strcmp(flag, 'FRA2') 
    htc = htc.*1000;
end

if strcmp(flag, 'SSTC')
    temp = temp - 273.15;  % K to degC
end

%% SI DATA
pres_MPa = pres./1e6;
% time
% temp
% htc


%N = numel(data(:,2));

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Convert
pres_conv = conv_Pa2ksi(pres);
time_conv = time./60;
temp_conv = conv_degC2F(temp);



htc_conv  = conv_htc2btu(htc);
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Sampling - FAVOR accepts up to 1000 cases
N_sample = numel(time_conv);

%sample = [1:11:N_sample];
sample = [1:skip_step:N_sample];
numel(sample)

pres_conv_0 = pres_conv(sample);
time_conv_0 = time_conv(sample) ;
time_conv_0(end) = 167.0  % Manually change to 167 min
temp_conv_0 = temp_conv(sample);
htc_conv_0 =  htc_conv(sample);

pres0 = [time_conv_0, pres_conv_0]; 
htc0  = [time_conv_0, htc_conv_0];
temp0 = [time_conv_0, temp_conv_0];

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
time_SI_0     = time(sample);       % sec
pres_SI_MPa_0 = [time_SI_0, pres_MPa(sample)];  % MPa
temp_SI_0     = [time_SI_0, temp(sample)];       % degC
htc_SI_0      = [time_SI_0, htc(sample)];           % W/m2/K

save([folder, '\', 'LTO_htc_SI_',name,'.txt'],'htc_SI_0','-ascii')
save([folder, '\','LTO_pres_SI_',name,'.txt'],'pres_SI_MPa_0','-ascii')
save([folder, '\','LTO_temp_SI_',name,'.txt'],'temp_SI_0','-ascii')


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Save base case variables to file
save([folder, '\', 'LTO_htc_',name,'.txt'],'htc0','-ascii')
save([folder, '\','LTO_pres_',name,'.txt'],'pres0','-ascii')
save([folder, '\','LTO_temp_',name,'.txt'],'temp0','-ascii')


