Rem https://www.tutorialspoint.com/batch_script/batch_script_creating_files.htm
Rem @echo off
Rem echo "Hello">C:\new.txt

Rem copy rename and past C:\temp\test.bat > C:\temp\test.log
Rem or copy /y C:\temp\log1k.txt C:\temp\log1k_copied.txt

mkdir "BaseCase_WUT_RELAP_WUT_1D_CW-CL"
mkdir "LTO1_HPIS_Tank_45_WUT_RELAP_WUT_1D_CW-CL"
mkdir "LTO2_ACCs_50_WUT_RELAP_WUT_1D_CW-CL"
mkdir "LTO3_LPIS_Tank_45_WUT_RELAP_WUT_1D_CW-CL"
mkdir "LTO4_HPSI_Head_80_WUT_RELAP_WUT_1D_CW-CL"
mkdir "LTO5_HPSI_Capacity_80_WUT_RELAP_WUT_1D_CW-CL"
mkdir "LTO7_ACC_Pressure_20_WUT_RELAP_WUT_1D_CW-CL"
mkdir "LTO8_CooldownRate_200_WUT_RELAP_WUT_1D_CW-CL"
mkdir "LTO9_ACC_Isolation_500_WUT_RELAP_WUT_1D_CW-CL"
mkdir "LTO3_HPSI_Head_75_WUT_RELAP_SSTC_1D_CW-CL"
mkdir "LTO3_HPSI_Capacity_75_WUT_RELAP_SSTC_1D_CW-CL"
mkdir "LTO9_ACC_Isolation_500_WUT_KWU-MIX_FRA-G_1D_CW-CL"
