% OLD
% Developed by Piotr Darnowski 2023, Institute of Heat Engineering, Warsaw University of Technology,
% APAL - Horizon 2020 Research Project
% Reads xlsx files with input data for FAVOR code.
% Process it to form useful for FAVOR. It includes interpolation of large
% 10k lines data to 1000 lines - limit of FAVOR
% Reads data from RELAP or other TH codes to matrices. Converts SI units to
% British units used by FAVOR.
close all; clear; clc;
%folder_main        = 'C:\OneDrive\OneDrive - Politechnika Warszawska\Mazgaj Piotr\APAL\WPs\WP3\FAVOR\Benchmark_3_1\Results\LTO';
folder_main        = 'C:\OneDrive\OneDrive - Politechnika Warszawska\Mazgaj Piotr\APAL\WPs\WP3\Benchmark_3_1\Results\LTO';
%folder_base_data   = 'WUT_base_case_LTO';
folder_base_data   = 'LTO';
folder_LTO_data    = 'LTO';

flag_base = 0; % Read BaseCasse?

%subfolder_LTO_data{1} = 'FRA-G_LTO_data_website';
%subfolder_LTO_data{2} = 'SSTC_LTO_data_website';
%subfolder_LTO_data{3} = 'WUT_LTO_data_website';

path_LTO_all = [folder_main,'\', folder_LTO_data,'\'];
% path_LTO = {};
subfolder = 'LTO_revision1';
% for i=1:numel(subfolder_LTO_data)
%   path_LTO{i} = [path_LTO_all, subfolder_LTO_data{i}]
% end
%path_basecase = [folder_main,'\', folder_base_data, '\']
path_basecase = [folder_main,'\', folder_base_data, '\', subfolder]
path_LTO = [path_LTO_all, subfolder]

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% https://ch.mathworks.com/matlabcentral/answers/770002-loading-in-multiple-excel-files-using-readmatrix
% folders_base = {''}; %empty no sub folders
if flag_base == 1
    name = 'Base'
    filename_base = ['RELAP5-33lf-WUT_Base_Case.xlsx'];
    %filepath_base = [ path_basecase, filename_base ];
    sheet = 'Sheet1';
    variables = [1,2,7,8];
    range = '44:10044';
    %range = 
    skip_step = 11;
    flag = 'BaseCase';
    
    case_basecase = 'case0';
    path_basecase = [path_basecase, '\', case_basecase];

    read_TH_data(name, case_basecase, path_basecase, filename_base, sheet, variables, range, skip_step, flag)
end
%    read_TH_data(name, casename, folder, filename, sheet, variables, range, skip_step, flag);  
% filepath = [path_basecase, filename_base]
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read data from LTO folders

% List of files in folders - only LTO  % not used now
%for i=1:numel(subfolder_LTO_data)
%    listing = dir([path_LTO{i},'\*.xlsx']);
%    filenames{i} = {listing.name};    
%end
% filenames{2}{2}   % filenames2{i} = string(filenames{i})
% https://ch.mathworks.com/matlabcentral/answers/693495-how-to-create-an-array-of-files-names-from-a-structure-created-by-dir-result
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% filenames with data
name_LTO = { ... 
%'BaseCase_WUT_RELAP_WUT_1D_CW-CL', ...
%'LTO1_HPIS_Tank_45_WUT_RELAP_WUT_1D_CW-CL', ...
%'LTO2_ACCs_50_WUT_RELAP_WUT_1D_CW-CL', ...
%'LTO3_LPIS_Tank_45_WUT_RELAP_WUT_1D_CW-CL', ...
%'LTO4_HPSI_Head_80_WUT_RELAP_WUT_1D_CW-CL', ...
%'LTO5_HPSI_Capacity_80_WUT_RELAP_WUT_1D_CW-CL', ...
%'LTO6_HPIS_Flow_1800_WUT_RELAP_WUT_1D_CW-CL', ...
%'LTO7_ACC_Pressure_20_WUT_RELAP_WUT_1D_CW-CL', ...
%'LTO8_CooldownRate_200_WUT_RELAP_WUT_1D_CW-CL', ...
%'LTO9_ACC_Isolation_500_WUT_RELAP_WUT_1D_CW-CL', ...
%'LTO3_HPSI_Head_75_WUT_RELAP_SSTC_1D_CW-CL', ...
%'LTO3_HPSI_Capacity_75_WUT_RELAP_SSTC_1D_CW-CL', ...
%'LTO9_ACC_Isolation_500_WUT_KWU-MIX_FRA-G_1D_CW-CL', ...

'LTO3_LPIS_Tank_45_WUT_RELAP_WUT_1D_CW-CL', ...
'LTO4_HPSI_Head_80_WUT_RELAP_WUT_1D_CW-CL', ...
'LTO5_HPSI_Capacity_80_WUT_RELAP_WUT_1D_CW-CL', ...


};
case_LTO = {...
%'case0' ...
%'case1' ...
%'case2' ...
%'case3' ...
%'case4' ...
%'case5' ...
%'case6' ...
%'case7' ...
%'case8' ...
%'case9' ...
%'case10' ...
%'case11' ...
%'case12' ...
'case3' ...
'case4' ...
'case5' ...
'case14' ...
'case15' ...

    };
%name_LTO = case_LTO;

flag_LTO = {...
%'WUT', ...
%'WUT', ...
%'WUT', ...
%'WUT', ...
%'WUT', ...
%'WUT', ...
%'WUT', ...
%'WUT', ...
%'WUT', ...
%'WUT', ...
%'SSTC', ...
%'SSTC', ...
%'FRA', ...
'WUT2', ...
'WUT2', ...
'WUT2', ...
'WUT2', ...
'WUT2', ...
};

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find XLSX file in folders and generate paths to read those files
for i =1:numel(name_LTO)
    %directory = [path_LTO, '\', name_LTO{i}];
    directory = [path_LTO, '\', case_LTO{i}];
    listing_LTO = dir([directory,'\*.xlsx']);
    %filename_LTO{i} = {listing_LTO.name}; 
    filename_LTO{i} = listing_LTO.name;   % LTO TH data filename
    folder_LTO{i} = listing_LTO.folder;  % LTO TH data folder
    filepath_LTO{i} =  [folder_LTO{i}, '/', filename_LTO{i}];  % LTO THdata full path with folder and file xslx   
end
% opts = detectImportOptions(filepath_LTO{1})
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i =1:numel(name_LTO)
    name = name_LTO{i}
    filename = filename_LTO{i};
    folder = folder_LTO{i};
    flag = flag_LTO{i};
    casename = case_LTO{i};

    if strcmp(flag_LTO{i},'WUT')
       sheet = 'Datas for 1D';   % sheet = 'Sheet1';
       variables = [1,2,7,8];         % vars = [1,2,7,8]; pressure, time, temp, htc
       range = '11:10011';        % range = '44:10044';
       skip_step = 11;
    % Example: 'A5' or [5 1]

    elseif strcmp(flag_LTO{i},'WUT2')
       sheet = 'Sheet1';
       variables = [1,2,7,8];         % vars = [1,2,7,8]; pressure, time, temp, htc
       range = '2:10002';        % range = '44:10044';
       skip_step = 2;


   elseif strcmp(flag_LTO{i},'SSTC')
       sheet = 'Data-WP34';  
       variables = [1,2, 3, 4];        
       range = '9:1250';       
       skip_step = 2;
   elseif strcmp(flag_LTO{i},'FRA')    
        sheet = 'out8';  
        skip_step = 2;
        % CL3 095 202.5   plume
        variables = [2, 1, 4, 10];   % pressure, time, temp, htc
        range = '3:982';   
    else
      
    end
    read_TH_data(name, casename, folder, filename, sheet, variables, range, skip_step, flag);  
    
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%














%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %{
mkdir "./LTO_revision1/BaseCase_WUT_RELAP_WUT_1D_CW-CL"
mkdir "./LTO_revision1/LTO1_HPIS_Tank_45_WUT_RELAP_WUT_1D_CW-CL"
mkdir "./LTO_revision1/LTO2_ACCs_50_WUT_RELAP_WUT_1D_CW-CL"
mkdir "./LTO_revision1/LTO3_LPIS_Tank_45_WUT_RELAP_WUT_1D_CW-CL"
mkdir "./LTO_revision1/LTO4_HPSI_Head_80_WUT_RELAP_WUT_1D_CW-CL"
mkdir "./LTO_revision1/LTO5_HPSI_Capacity_80_WUT_RELAP_WUT_1D_CW-CL"
mkdir "./LTO_revision1/LTO7_ACC_Pressure_20_WUT_RELAP_WUT_1D_CW-CL"
mkdir "./LTO_revision1/LTO8_CooldownRate_200_WUT_RELAP_WUT_1D_CW-CL"
mkdir "./LTO_revision1/LTO9_ACC_Isolation_500_WUT_RELAP_WUT_1D_CW-CL"
mkdir "./LTO_revision1/LTO3_HPSI_Head_75_WUT_RELAP_SSTC_1D_CW-CL"
mkdir "./LTO_revision1/LTO3_HPSI_Capacity_75_WUT_RELAP_SSTC_1D_CW-CL"
mkdir "./LTO_revision1/LTO9_ACC_Isolation_500_WUT_KWU-MIX_FRA-G_1D_CW-CL"
 %}
%filename_LTO = { ... 
%};
%{
filename_LTO = { ... 
%'BaseCase_WUT_RELAP_WUT_1D_CW-CL.xlsx', ...
'LTO1_HPIS_Tank_45_WUT_RELAP_WUT_1D_CW-CL.xlsx', ...
'LTO2_ACCs_50_WUT_RELAP_WUT_1D_CW-CL.xlsx', ...
'LTO3_LPIS_Tank_45_WUT_RELAP_WUT_1D_CW-CL.xlsx', ...
'LTO4_HPSI_Head_80_WUT_RELAP_WUT_1D_CW-CL.xlsx', ...
'LTO5_HPSI_Capacity_80_WUT_RELAP_WUT_1D_CW-CL.xlsx', ...
'LTO7_ACC_Pressure_20_WUT_RELAP_WUT_1D_CW-CL.xlsx', ...
'LTO8_CooldownRate_200_WUT_RELAP_WUT_1D_CW-CL.xlsx', ...
'LTO9_ACC_Isolation_500_WUT_RELAP_WUT_1D_CW-CL.xlsx', ...
'LTO3_HPSI_Head_75_WUT_RELAP_SSTC_1D_CW-CL.xlsx', ...
'LTO3_HPSI_Capacity_75_WUT_RELAP_SSTC_1D_CW-CL.xlsx', ...
'LTO9_ACC_Isolation_500_WUT_KWU-MIX_FRA-G_1D_CW-CL.xlsx', ...
};
%}
%%