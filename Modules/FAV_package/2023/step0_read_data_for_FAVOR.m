% Developed by Piotr Darnowski 2023, Institute of Heat Engineering, Warsaw University of Technology,
% APAL - Horizon 2020 Research Project
% Reads xlsx files with input data for FAVOR code.
% Process it to form useful for FAVOR. It includes interpolation of large
% 10k lines data to 1000 lines - limit of FAVOR
% Reads data from RELAP or other TH codes to matrices. Converts SI units to
% British units used by FAVOR.
close all; clear; clc;
folder_main        = 'C:\OneDrive\OneDrive - Politechnika Warszawska\Mazgaj Piotr\APAL\WPs\WP3\FAVOR\Benchmark_3_1\Results\LTO';
folder_base_data   = 'WUT_base_case_LTO';
folder_LTO_data    = 'LTO';

subfolder_LTO_data{1} = 'FRA-G_LTO_data_website';
subfolder_LTO_data{2} = 'SSTC_LTO_data_website';
subfolder_LTO_data{3} = 'WUT_LTO_data_website';

path_LTO_all = [folder_main,'\', folder_LTO_data,'\'];
path_LTO = {};

for i=1:numel(subfolder_LTO_data)
    path_LTO{i} = [path_LTO_all, subfolder_LTO_data{i}]
end
path_basecase = [folder_main,'\', folder_base_data, '\'];

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% https://ch.mathworks.com/matlabcentral/answers/770002-loading-in-multiple-excel-files-using-readmatrix
% folders_base = {''}; %empty no sub folders
filename_base = ['RELAP5-33lf-WUT_Base_Case.xlsx'];

filepath_base = [ path_basecase, filename_base ];
opts = detectImportOptions(filepath_base);

opts.Sheet = 'Sheet1';
opts.SelectedVariableNames = [1,2,7,8]; 
opts.DataRange = '44:10044';

data= readmatrix(filepath_base,opts);  % Read base case results
pres     = data(:,1);  %seconds
time     = data(:,2);  %bars
temp     = data(:,3); %degC
htc      = data(:,4); %W/m2/K
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Convert
pres_conv = conv_Pa2ksi(pres);
time_conv = time./60;
temp_conv = conv_degC2F(temp);
htc_conv  = conv_htc2btu(htc);
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sampling - FAVOR accepts up to 1000 cases
N_sample = numel(time_conv);
sample = [1:11:N_sample];
numel(sample)

pres_conv_0 = pres_conv(sample);
time_conv_0 = time_conv(sample) ;
time_conv_0(end) = 167.0  % Manually change to 167 min
temp_conv_0 = temp_conv(sample);
htc_conv_0 =  htc_conv(sample);

pres0 = [time_conv_0, pres_conv_0]; 
htc0  = [time_conv_0, htc_conv_0];
temp0 = [time_conv_0, temp_conv_0];
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Save base case variables to file
save([path_basecase, 'LTO_base_htc.txt'],'htc0','-ascii')
save([path_basecase, 'LTO_base_pres.txt'],'pres0','-ascii')
save([path_basecase, 'LTO_base_temp.txt'],'temp0','-ascii')

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read data from LTO folders

% List of files in folders - only LTO  % not used now
for i=1:numel(subfolder_LTO_data)
    listing = dir([path_LTO{i},'\*.xlsx']);
    filenames{i} = {listing.name};    
end
% filenames{2}{2}   % filenames2{i} = string(filenames{i})
% https://ch.mathworks.com/matlabcentral/answers/693495-how-to-create-an-array-of-files-names-from-a-structure-created-by-dir-result
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% filenames with data
folders_LTO = { ... 
'BaseCase_WUT_RELAP_WUT_1D_CW-CL', ...
'LTO1_HPIS_Tank_45_WUT_RELAP_WUT_1D_CW-CL', ...
'LTO2_ACCs_50_WUT_RELAP_WUT_1D_CW-CL', ...
'LTO3_LPIS_Tank_45_WUT_RELAP_WUT_1D_CW-CL', ...
'LTO4_HPSI_Head_80_WUT_RELAP_WUT_1D_CW-CL', ...
'LTO5_HPSI_Capacity_80_WUT_RELAP_WUT_1D_CW-CL', ...
'LTO7_ACC_Pressure_20_WUT_RELAP_WUT_1D_CW-CL', ...
'LTO8_CooldownRate_200_WUT_RELAP_WUT_1D_CW-CL', ...
'LTO9_ACC_Isolation_500_WUT_RELAP_WUT_1D_CW-CL', ...
'LTO3_HPSI_Head_75_WUT_RELAP_SSTC_1D_CW-CL', ...
'LTO3_HPSI_Capacity_75_WUT_RELAP_SSTC_1D_CW-CL', ...
'LTO9_ACC_Isolation_500_WUT_KWU-MIX_FRA-G_1D_CW-CL', ...
};
filenames_LTO = { ... 
'BaseCase_WUT_RELAP_WUT_1D_CW-CL.xlsx', ...
'LTO1_HPIS_Tank_45_WUT_RELAP_WUT_1D_CW-CL.xlsx', ...
'LTO2_ACCs_50_WUT_RELAP_WUT_1D_CW-CL.xlsx', ...
'LTO3_LPIS_Tank_45_WUT_RELAP_WUT_1D_CW-CL.xlsx', ...
'LTO4_HPSI_Head_80_WUT_RELAP_WUT_1D_CW-CL.xlsx', ...
'LTO5_HPSI_Capacity_80_WUT_RELAP_WUT_1D_CW-CL.xlsx', ...
'LTO7_ACC_Pressure_20_WUT_RELAP_WUT_1D_CW-CL.xlsx', ...
'LTO8_CooldownRate_200_WUT_RELAP_WUT_1D_CW-CL.xlsx', ...
'LTO9_ACC_Isolation_500_WUT_RELAP_WUT_1D_CW-CL.xlsx', ...
'LTO3_HPSI_Head_75_WUT_RELAP_SSTC_1D_CW-CL.xlsx', ...
'LTO3_HPSI_Capacity_75_WUT_RELAP_SSTC_1D_CW-CL.xlsx', ...
'LTO9_ACC_Isolation_500_WUT_KWU-MIX_FRA-G_1D_CW-CL.xlsx', ...
};


























 %{
mkdir "./LTO_revision1/BaseCase_WUT_RELAP_WUT_1D_CW-CL"
mkdir "./LTO_revision1/LTO1_HPIS_Tank_45_WUT_RELAP_WUT_1D_CW-CL"
mkdir "./LTO_revision1/LTO2_ACCs_50_WUT_RELAP_WUT_1D_CW-CL"
mkdir "./LTO_revision1/LTO3_LPIS_Tank_45_WUT_RELAP_WUT_1D_CW-CL"
mkdir "./LTO_revision1/LTO4_HPSI_Head_80_WUT_RELAP_WUT_1D_CW-CL"
mkdir "./LTO_revision1/LTO5_HPSI_Capacity_80_WUT_RELAP_WUT_1D_CW-CL"
mkdir "./LTO_revision1/LTO7_ACC_Pressure_20_WUT_RELAP_WUT_1D_CW-CL"
mkdir "./LTO_revision1/LTO8_CooldownRate_200_WUT_RELAP_WUT_1D_CW-CL"
mkdir "./LTO_revision1/LTO9_ACC_Isolation_500_WUT_RELAP_WUT_1D_CW-CL"
mkdir "./LTO_revision1/LTO3_HPSI_Head_75_WUT_RELAP_SSTC_1D_CW-CL"
mkdir "./LTO_revision1/LTO3_HPSI_Capacity_75_WUT_RELAP_SSTC_1D_CW-CL"
mkdir "./LTO_revision1/LTO9_ACC_Isolation_500_WUT_KWU-MIX_FRA-G_1D_CW-CL"
 %}