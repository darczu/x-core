@ECHO OFF
ren convergence_table_fail.out %1_convergence_table_fail.out
ren convergence_table_ini.out %1_convergence_table_ini.out
ren PDFCPF.OUT %1_PDFCPF.OUT
ren PDFCPI.OUT %1_PDFCPI.OUT
move %1_convergence_table_fail.out ./FAVPOSTout
move %1_convergence_table_ini.out ./FAVPOSTout
move %1_PDFCPF.OUT ./FAVPOSTout
move %1_PDFCPI.OUT ./FAVPOSTout
