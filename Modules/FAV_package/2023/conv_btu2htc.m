function  [outW] = conv_Btu2htc(InpBtu)

%[BTU/h/ft2/F] = 0.176228 * [W/m2/K]					0.176228
%[W/m2/K] = 5.674467 * [BTU/h/ft2/F]					5.674467168
%[MPA] = 6.8947572932*[KSI]					6.894757293
%[ksi] = [Mpa]/6.8947572932					0.145037738
%[degC] = (5/9)*([degF] - 32)					
%[degF] = (9/5)*[degC] + 32;					

    	
    outW = InpBtu.*5.674467  ;   % [W/m2/K]	=		 [BTU/h/ft2/F] *  5.674467 
      
        
		