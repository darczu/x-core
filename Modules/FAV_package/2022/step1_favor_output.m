% Developed by Piotr Darnowski 2022, Institute of Heat Engineering, Warsaw University of Technology,
% APAL - Horizon 2020 Research Project
% Read FAVOR output and create xlsx files for post-processing
% Creates XLSX files with data to be read by favor.m
clear; clc; close all;

%casenames = {'case1', 'case2', 'case3', 'case4', 'case5', 'case6', 'case7'}
%casenames = { 'case2', 'case3', 'case4', 'case5', 'case6', 'case7'}
%casenames = {'case1'};
casenames = {'case3'};
name_folder = 'revision3';

for iii = 1:numel(casenames)
    
    clearvars -except casenames name_folder iii

    %name = 'case7';
    name = casenames{iii}
  
    %folder = name;
    folder = [name_folder,'/', name];
    %input_file = ['LOAD_' name '_5.out'];
    input_file = ['LOAD_' name '.out'];
    output_file = ['favor_' name '_out.xlsx'];

    fileID = ['./', folder, '/', input_file];
    fileOD = ['./', folder, '/', output_file];

    favor_read_data(name,folder,fileID,fileOD);

end


