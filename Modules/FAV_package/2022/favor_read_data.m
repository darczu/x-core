% Developed by Piotr Darnowski 2022, Institute of Heat Engineering, Warsaw University of Technology,
% APAL - Horizon 2020 Research Project
% https://www.mathworks.com/matlabcentral/answers/53512-reading-header-line-from-text-file
function [] = favor_read_data(name,folder,fileID,fileOD)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



key0 = '//';
key{1} = '//Discretization Data Dimensions: NTIMES, NCDH';
key{2} = '//Time Discretization Data: DTIME(1:NTIMES)';
key{3} = '//PRESSURE TIME HISTORIES FOR EACH TRANSIENT: PRESS(1:MTRAN,1:NTIMES)';
key{4} = '//TEMPERATURE PROFILE TIME HISTORIES FOR EACH TRANSIENT';
key{5} = '//HOOP STRESS PROFILE TIME HISTORIES (W/O RESIDUAL STRESSES) FOR EACH TRANSIENT';
key{6} = '//AXIAL STRESS PROFILE TIME HISTORIES (W/O RESIDUAL STRESSES) FOR EACH TRANSIENT';
key{7} = '//HOOP STRESS PROFILE TIME HISTORIES (WITH RESIDUAL STRESSES) FOR EACH TRANSIENT';
key{8} = '//AXIAL STRESS PROFILE TIME HISTORIES (WITH RESIDUAL STRESSES) FOR EACH TRANSIENT';

keyName{1} = 'Dimension'; 
keyName{2} = 'Time';
keyName{3} = 'Pressure';
keyName{4} = 'Temperature';
keyName{5} = 'Hoop Stress w/o RS';
keyName{6} = 'Axial Stress w/o RS';
keyName{7} = 'Hoop Stress w RS';
keyName{8} = 'Axial Stress w RS';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fid 	= fopen(fileID);            % open file
nLines 	= linecount(fid);           % number of lines in the file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
keyline = {};                       % variable to store ket line location
frewind(fid);                       % move to the begning
for j=1:numel(key)
    frewind(fid);                       % move to the begning
    for i=0:(nLines-1)                  % loop for all lines
        line = fgetl(fid);              % read line
        isKey = strfind(line,key{j});
        if isKey>0
            keyline{j} = i+1;
        end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dummy1={};
frewind(fid);                       % move to the begning
dummy1 = textscan(fid,'%d %d \n','headerlines',keyline{1}); %read data
NTIMES = dummy1{1};  % 168
NCDH   = dummy1{2};  % 16

% size og keyline data
keyNum{2} = NTIMES;
keyNum{3} = NTIMES;
keyNum{4} = NTIMES*NCDH;
keyNum{5} = NTIMES*NCDH;
keyNum{6} = NTIMES*NCDH;
keyNum{7} = NTIMES*NCDH;
keyNum{8} = NTIMES*NCDH;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dummy2={}; key_data2 = {};
frewind(fid);          
textscan(fid,'\n','headerlines',keyline{2}); % move to location
dummy2 = textscan(fid,'   %s  %s', keyNum{2}); % move to location

for i=1:2
    for j=1:keyNum{2}
        key_data2{j,i} = str2num(dummy2{i}{j});
    end
end

vars = {keyName{2}};
T1 =  table(cell2mat(key_data2),'VariableNames',vars);
writetable(T1,['./',folder,'/','time_',name,'.xlsx']);



%vars = {'Inner'; 'Fusion Clad'; 'Fusion Base';'Crack';'Outer'};
%T = table(T1,T2int,T2ext,T3,T4,'VariableNames',vars);
%writetable(T,[  'interp_', cases, '_', varNames{iTemp}  ,'.xlsx']);

% writecell(key_data2,'file.xlsx')
%dummy2 = textscan(fid,'   %s  %s', NTIMES,'CollectOutput',1); % move to location
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dummy={}; keyTab = [];
for i = 3:8
    frewind(fid);
    textscan(fid,'\n','headerlines',keyline{i}); % move to location
    dummy{i} = textscan(fid,'%f', keyNum{i}); % move to location
end

T = array2table(zeros(NTIMES*NCDH,5));
T.Properties.VariableNames = {keyName{4:8}};

for i = 4:8
    T{:,i-3} = dummy{1,i}{:};  %,'VariableNames',{keyName{i}})
end

writetable(T,fileOD);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fclose(fid);
