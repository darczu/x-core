%% Read RELAP STRIPF file with Matlab
% https://www.mathworks.com/matlabcentral/answers/71374-how-to-read-a-text-file-line-by-line
% https://www.mathworks.com/help/matlab/ref/textscan.html?searchHighlight=textscan&s_tid=srchtitle_textscan_1
%
% lepiej uzyc frewind(fileID)
function [time, vars, names, comps] = stripf_read(filename)

key1 = 'plotinf';
key2 = 'plotalf';
key3 = 'plotnum';
key4 = 'plotrec';

Npos = 8;

FID2 	= fopen(filename); lineN2 	= linecount(FID2); %counting lines in the flie
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FIND LINE WITH 'plotinf'
frewind(FID2);
key1line =[];
for i=0:lineN2-1
	line = fgetl(FID2);
	k = strfind(line,key1);
	if(k>0)
		key1line = [key1line; i+1]; %Numer lini z key1
	end
end
Nkey1 		= numel(key1line);
FID2 		= fopen(filename);
plotinf1 	= textscan(FID2,'%s %f %f','headerlines',key1line-1);
Nplots 		= plotinf1{2};  %pplus time and plus plotalf
Nplots_lines = ceil(Nplots/Npos);  % 8 position in a row
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plotalf jest jedna linia nizej
key2line = key1line + 1;



%clear plotalf1
frewind(FID2);
textscan(FID2,'','headerlines',key2line-1);
%getl(FID2)
plotalf1 = cell(Nplots_lines,Npos);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% read data
for i = 1:Nplots_lines
	dummy = textscan(FID2,'%s',Npos);
	for j=1:Npos	
      plotalf1{i,j} = dummy{1}{j};	  
	end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% remove wrong positions
Nremove = Nplots_lines*Npos - Nplots;
for k=1:Nremove
 plotalf1{end,end-k+1} = '';
end

plotalf10 = reshape(plotalf1',[numel(plotalf1) 1]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% move to plotnum
frewind(FID2);
textscan(FID2,'','headerlines',key2line-1+Nplots_lines);

plotnum1 = cell(Nplots_lines,Npos);
% read data
for i = 1:Nplots_lines
	dummy = textscan(FID2,'%s',Npos);
	for j=1:Npos	
      plotnum1{i,j} = dummy{1}{j};	  
	end
end
% remove wrong positions
Nremove = Nplots_lines*Npos - Nplots;
for k=1:Nremove
 plotnum1{end,end-k+1} = '';
end

plotnum10 = reshape(plotnum1',[numel(plotnum1) 1]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read time steps
% key4line - locations of plotrec
frewind(FID2);
key4line =[];
for i=0:lineN2-1
	line = fgetl(FID2);
	k = strfind(line,key4);
	if(k>0)
		key4line = [key4line; i+1]; %Numer lini z key1
	end
end


N_time_steps = numel(key4line); % Number of time steps
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Npos2 = 5;							% number of objects in plotred bloc
Nplots_lines2 = ceil(Nplots/Npos2);   %number of lines to read in plotrec
plotrec1 = cell(Nplots_lines2,Npos2,N_time_steps); 
Nremove2 = Nplots_lines2*Npos2 - Nplots;


for k =1:N_time_steps
	line = key4line(k); %current line number
	frewind(FID2);
	textscan(FID2,'','headerlines',line-1);
	
	% read data
	for i = 1:Nplots_lines2		
		if i == Nplots_lines2
			dummy = textscan(FID2,'%s',Npos2-Nremove2);
			
				for j=1:Npos2-Nremove2	
					plotrec1{i,j,k} = dummy{1}{j};	  
				end
			
		else
			dummy = textscan(FID2,'%s',Npos2);
				for j=1:Npos2	
					plotrec1{i,j,k} = dummy{1}{j};	  
				end
		end	
		

	end
	
%	for l=1:Nremove2
%		%plotrec1{end,end-l+1,k} = '';
%	end
end

for k =1:N_time_steps
	plotrec10(:,k) = reshape(plotrec1(:,:,k)',[numel(plotrec1(:,:,k)) 1]);
end

format short g;
plotrec100 = str2double(plotrec10);


time = plotrec100(2,:);
vars = plotrec100(2:end-Nremove2,:);  %including time
%vars = plotrec100(2:end,:);  %including time
names =  plotalf10(2:end-Nremove);
comps = plotnum10(2:end-Nremove);

fclose(FID2);


