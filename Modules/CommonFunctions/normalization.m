% Redeveloped to work with matrices 14-01-2020
% Rev 28-03-2017
% Developed in 2016 by Piotr Darnowski
% Function dedicated to normalize vector or matrix
% 
% vec_i - vector of values for example: atom numbers, vol. fractions, mass
% fraction etc...
% Example 
% normalization([2 1])  
% [a, b] = normalization([1,2,3; 1, 2, 3])

function [norm, vec_sum] = normalization(vec)

N = max(numel(size(vec))); %maxium levels/dimensions of matrix - matrix max dimensions 3x5 matrix has 2 dims 3x5x6 has 3 dims

% calculations if vector
if N == 1
	vec_sum = sum(vec);
	norm = vec./vec_sum;
	
% calculations if matrix	
else
	vec_sum = sum(vec);
	for i = 1:N-1 % should be N-1
		vec_sum = sum(vec_sum);  
	end
	%vec_sum = vec_sum;
	norm = vec./vec_sum;
end




