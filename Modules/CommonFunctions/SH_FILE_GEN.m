% =========================================================================
% Revision 05.04.2017 
% SH FILES BASH GENERATOR
% BASH FILES FOR LINUX - to automate SERPENT code execution
% =========================================================================
%Bash_File - name of the bash file for the case
if(flag_bash == 1)     
    bash = fopen(bash_filename,'w');
    fprintf(bash,['# %% --- Bash File to run cases\n\n']);
    %fpritnf(bash,['# CASE: ', bash_filename, '\n'] );
    fprintf(bash,['#! /bin/bash \n']);
    fprintf(bash, [SSS_main_path, SSS_path, SSS_exe, SSS_MPI_arg, WorkingFolder, 'input' ]);
    fprintf(bash,'\n');
    fclose(bash);
end
% =========================================================================
 