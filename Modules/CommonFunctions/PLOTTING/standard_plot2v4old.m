set(0, 'DefaulttextInterpreter', 'none'); 
 l = legend(labels); 
set(l, 'Interpreter','none');
set(l,'FontSize',legend_font_size);


 if flag_info1 == 1
 	if k_num == numel(cases)
 		annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',cases{1},'FitBoxToText','off','EdgeColor','none','Interpreter','none','FontSize',9 );
 		annotation(gcf,'textbox', [0.80 0.04 0.02 0.02],'String',EDF.file{k},'FitBoxToText','off','EdgeColor','none','Interpreter','none','FontSize',9 );
 	end
elseif flag_info1 == 2
	if k_num == numel(cases)
 
 		annotation(gcf,'textbox', [0.70 0.04 0.02 0.02],'String',mlcfile,'FitBoxToText','off','EdgeColor','none','Interpreter','none','FontSize',9 );
	end
elseif flag_info1 == 3	
	if flag3 == 0 
		annotation(gcf,'textbox', [0.04 0.04 0.02 0.02],'String',cases_list_names{k_num},'FitBoxToText','off','EdgeColor','none','Interpreter','none','FontSize',9 );
	end	
end
 


%if k_num == numel(cases)  %save figures during after the last case
if flag2 == 1
    if flag3 == 0   %Save to each folder seperaetly
            %print('-dtiff',[case1,'\', name, code1, '.tiff']);
           % print('-dtiff',[cases{1},'\', name,'_', code1, '.tiff']);
		   
			folder_name = [ cases_list{k_num}];
			
			if flag_allplottime  == 1
				results_folder = 'Results_long';
			else
				results_folder = 'Results';
			end
			
			mkdir([folder{:},'\',folder_name,'\',results_folder]);
			print('-dtiff',[folder{:},'\',folder_name,'\',results_folder,'\', name, '.tiff']);
			savefig([folder{:},'\',folder_name,'\',results_folder,'\', name, '.fig']);
			%mkdir([folder{:},'\',folder_name,'\Results']);
			%print('-dtiff',[folder{:},'\',folder_name,'\Results\', name, '.tiff']);
			%savefig([folder{:},'\',folder_name,'\Results\', name, '.fig']);
			

            %savefig([case1,'\', name, code1, '.fig']);
            %savefig([cases{1},'\', name, '_', code1, '.fig']);
			
             %savefig([file '/Figures/Fig1.fig'])
             %saveas(gcf,[file '/Figures/Fig1.fig'],'fig');
                %print('-djpeg100','-r200',[file '/Figures/Fig1.jpg'])
    end
end
%end		
        
		
		
if flag2 == 1
	if flag3 == 1
		%if k_num == numel(cases)  %save figures during after the last case
		if k_num == last_case  %save figures during after the last case



            %folder_name = ['COMPARISON_' cases_names{:}];
        %    folder_name = ['COMPARISON_' cases_names{:}];
		    folder_name = ['COMPARISON_' cases_list{:}];
            mkdir([folder{:},'\',folder_name]);

			print('-dtiff',[folder{:},'\',folder_name,'\', name, '.tiff']);
            savefig([folder{:},'\',folder_name,'\', name, '.fig']);
        
		%	print('-dtiff',[folder_name,'\', name,'_', code1, '.tiff']);
         %   savefig([folder_name,'\', name, '_', code1, '.fig']);

            %print('-dtiff',[folder,'\',folder_name,'\', name,'_', code1, '.tiff']);
            %savefig([folder,'\',folder_name,'\', name, '_', code1, '.fig']);
			
			
        end
    end
end

% if flag3 == 0 
% 	hold off;
% end	
% if flag3 == 1 
% 	hold on;
% end

%end %plot omit if (sum(plot_omit == a) == 0)  %plot omit   the if is in standard_plot1v4

%{
%% Figure 1 PPS

h=figure(1+a)

% ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
 title(file);
elseif(title_flag==2)
 title('Pressurizer Pressure')   
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15 );

% ==
hold on
if(flagMAAP == 1)
    plot(MAAP4_T,MAAP4_1,'-r','LineWidth',2);
end

plot(a1(:,1)./Tconst,a1(:,2),'LineWidth',2,cl,col(2,:),ls,'-');
 set(0, 'DefaulttextInterpreter', 'none');
box on;
grid on;
xlabel('Time, [s]','Interpreter','none','FontSize',15 );
if( flag_hours == 1)    xlabel('Time, [h]','Interpreter','none','FontSize',15 ); end
if(flagMAAP == 1)
legend('MAAP4','MELCOR','Location','NorthEast','Orientation','vertical');
else
 legend('MELCOR','Location','NorthEast','Orientation','vertical');
end

ylabel('Pressure, [Pa]','Interpreter','none','FontSize',15 );
xlim([0 tend]);
if (flag2 == 1)
    %print('-djpeg100','-r200',[file '/Figures/Fig1.jpg'])
    print('-dtiff','-r300',[file '/Figures/Fig1.tiff'])
    %savefig([file '/Figures/Fig1.fig'])
    saveas(gcf,[file '/Figures/Fig1.fig'],'fig');
end
%}

