function [P, x,y] = plot_arc2(a,b,h,k,r,flag,dd)
% Plot a circular arc as a pie wedge.
% a is start of arc in radians, 
% b is end of arc in radians, 
% (h,k) is the center of the circle.
% r is the radius.
% Try this:   plot_arc(pi/4,3*pi/4,9,-4,3)
% Author:  Matt Fig
t = linspace(a,b,10000);
%t = linspace(a,b);
%t = linspace(a,b,1000); %alternatywa z gesta nodalizacja
x = r*cos(t) + h;
y = r*sin(t) + k;
%x = [x h x(1)];
%y = [y k y(1)];
if flag==1  %jeden zwykly , inny kreskowany
    plot(x,y,'r-.','LineWidth',dd);
else 
    plot(x,y,'r-','LineWidth',dd);
end
%P = fill(x,y,'w');
%axis equal; 
%axis square;
if ~nargout
    clear P
end

%Now from the command line:
%
%P = plot_arc(pi/4,3*pi/3,9,-4,3);
%set(P,'edgecolor','b','linewidth',4)