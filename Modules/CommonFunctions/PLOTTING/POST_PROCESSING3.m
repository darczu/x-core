%% POST PROCESSING


%% FUEL MASS MAP =====================================
f1 = figure(1)
hold on
mat = FAMASS3;
%mat = rand(5);           %# A 5-by-5 matrix of random values from 0 to 1
imagesc(mat,[20800, 21500]);            %# Create a colored plot of the matrix values
textStrings = num2str(mat(:),'%0.2f');  %# Create strings from the matrix values
textStrings = strtrim(cellstr(textStrings));  %# Remove any space padding
[x,y] = meshgrid(1:15);   %# Create x and y coordinates for the strings
hStrings = text(x(:),y(:),textStrings(:),...      %# Plot the strings
                'HorizontalAlignment','center');
midValue = mean(get(gca,'CLim'));  %# Get the middle value of the color range
%textColors = repmat(mat(:) > midValue,1,3);  %# Choose white or black for the
textColors = [0 0 0];
set(hStrings,{'Color'},num2cell(textColors,2));  %# Change the text colors
set(gca)


%% BURN-UP CHART =====================================================

%FABURNUP = zeros(CoreDim,CoreDim,Nkey1,Nlines); %Masses Map (wiersz,kolumna,krok wypalania,poziom)
%FABURNUP(h,m,i,k)
%LAXIAL, PARCSTIME
Burnup_AV =  sum(FABURNUP(:,:,PARCS_TIME,:),4)./LAXIAL;
Burnup_AV2 = sum(sum(Burnup_AV))./FA;
Burnup_AV3 =  sum(FABURNUP(:,:,:,:),4)./LAXIAL; %For animation
Burnup_AV_EOC = sum(FABURNUP(:,:,end,:),4)./LAXIAL;
Burnup_AV2_EOC = sum(sum(Burnup_AV_EOC))./FA;

figure(2);
f2 = figure;
Z = Burnup_AV;
b=bar3(Z);
colorbar
zlabel('Burnup [GWd/tHM]');
%ONLY MATLAB2014b

for k = 1:length(b)
    zdata = b(k).ZData;
  b(k).CData = zdata;
   b(k).FaceColor = 'interp';
end

% === ANIMATION
%{
figure
Z = Burnup_AV3(:,:,1);
%bar3(Z);
axis tight manual
axis([0 15 0 15 0 20])
ax = gca;
ax.NextPlot = 'replaceChildren';

loops = 40;
F(loops) = struct('cdata',[],'colormap',[]);
for j = 1:loops
  bar3(Burnup_AV3(:,:,j));
    %drawnow
    F(j) = getframe;
end
hold on
movie(F,2,3);
%}

%% === 2D fuel Burnup map =================================================
figure(3);
f3 = figure;
hold on
mat = Burnup_AV;
imagesc(mat,[0, 18]);            %# Create a colored plot of the matrix values
%colormap(flipud(gray));  %# Change the colormap to gray (so higher values
textStrings = num2str(mat(:),'%0.2f');  %# Create strings from the matrix values
textStrings = strtrim(cellstr(textStrings));  %# Remove any space padding
[x,y] = meshgrid(1:15);   %# Create x and y coordinates for the strings
hStrings = text(x(:),y(:),textStrings(:),...      %# Plot the strings
                'HorizontalAlignment','center');
midValue = mean(get(gca,'CLim'));  %# Get the middle value of the color range
%textColors = repmat(mat(:) > midValue,1,3);  %# Choose white or black for the
textColors = [0 0 0];                                       
set(hStrings,{'Color'},num2cell(textColors,2));  %# Change the text colors
set(gca)
hold off

%% END OF CYCLE FUEL BURNUP

figure(333);
f3 = figure;
hold on
mat = Burnup_AV_EOC;
imagesc(mat,[0, 18]);            %# Create a colored plot of the matrix values
%colormap(flipud(gray));  %# Change the colormap to gray (so higher values
textStrings = num2str(mat(:),'%0.2f');  %# Create strings from the matrix values
textStrings = strtrim(cellstr(textStrings));  %# Remove any space padding
[x,y] = meshgrid(1:15);   %# Create x and y coordinates for the strings
hStrings = text(x(:),y(:),textStrings(:),...      %# Plot the strings
                'HorizontalAlignment','center');
midValue = mean(get(gca,'CLim'));  %# Get the middle value of the color range
%textColors = repmat(mat(:) > midValue,1,3);  %# Choose white or black for the
textColors = [0 0 0];                                       
set(hStrings,{'Color'},num2cell(textColors,2));  %# Change the text colors
set(gca)
colormap(jet)
hold off

%% ====================  ACTINIDES =============================
% PLOT SETUP
uran = 1:8;
uran2 = [1:5,7:8];  %no u-238
pu = 14:22;
np = 9:13;
am = 23:28;
cm = 29:36;
ao = 37:43;
ma = [9:13,23:36,43];
tru = [9:36];
otha = [37:42];
act = [1:43];
total = 44;

%%  ACTINIDES MASSESS =================================
barunit = 'Mass, [g]';

bardata(4,uran,M_FAACT_5,ACT_LIST,flag_savefig,folder,barunit);   %URANIUM
bardata(5,uran2,M_FAACT_5,ACT_LIST,flag_savefig,folder,barunit);    %URANIUM not U238
bardata(6,pu,M_FAACT_5,ACT_LIST,flag_savefig,folder,barunit);   %Plutonium
bardata(7,ma,M_FAACT_5,ACT_LIST,flag_savefig,folder,barunit);   %Minor Actinides
bardata(8,np,M_FAACT_5,ACT_LIST,flag_savefig,folder,barunit);   %Neptunium
bardata(9,cm,M_FAACT_5,ACT_LIST,flag_savefig,folder,barunit);   %Curium
bardata(10,tru,M_FAACT_5,ACT_LIST,flag_savefig,folder,barunit); %Transuranics
bardata(11,otha,M_FAACT_5,ACT_LIST,flag_savefig,folder,barunit);    %Other Actinides
%bardata(12,otha,M_FAACT_5,ACT_LIST,flag_savefig,folder,barunit); %All Actinides

%%  ACTINIDES ACTIVITIES =================================
barunit = 'Activity, [Bq]';

bardata(13,uran,A_FAACT_5,ACT_LIST,flag_savefig,folder,barunit);
bardata(14,uran2,A_FAACT_5,ACT_LIST,flag_savefig,folder,barunit);
bardata(15,pu,A_FAACT_5,ACT_LIST,flag_savefig,folder,barunit);
bardata(16,ma,A_FAACT_5,ACT_LIST,flag_savefig,folder,barunit);
bardata(17,np,A_FAACT_5,ACT_LIST,flag_savefig,folder,barunit);
bardata(18,cm,A_FAACT_5,ACT_LIST,flag_savefig,folder,barunit);
bardata(19,tru,A_FAACT_5,ACT_LIST,flag_savefig,folder,barunit);
bardata(20,otha,A_FAACT_5,ACT_LIST,flag_savefig,folder,barunit);
%bardata(21,act,A_FAACT_5,ACT_LIST,flag_savefig,folder,barunit);

%Fission products 49
a1 = [1:12];
a2 = [13:25];
a3 = [26:38];
a4 = [39:49];
barunit = 'Mass, [g]';
bardata(500,a1,M_FAFP_5,FP_LIST,flag_savefig,folder,barunit);   
bardata(501,a2,M_FAFP_5,FP_LIST,flag_savefig,folder,barunit);    
bardata(502,a3,M_FAFP_5,FP_LIST,flag_savefig,folder,barunit);  
bardata(503,a4,M_FAFP_5,FP_LIST,flag_savefig,folder,barunit);   

barunit = 'Activity, [Bq]';
bardata(504,a1,A_FAFP_5,FP_LIST,flag_savefig,folder,barunit);   
bardata(505,a2,A_FAFP_5,FP_LIST,flag_savefig,folder,barunit);    
bardata(506,a3,A_FAFP_5,FP_LIST,flag_savefig,folder,barunit);  
bardata(507,a4,A_FAFP_5,FP_LIST,flag_savefig,folder,barunit);   




%%  NUCLEAR DENSITIES ==================================
%FOR 9 different 
%N_ACT1 or N_ACT
%N_ACT = zeros(ACT_Isotopes,FATypes,TRITON_steps);
plotunit = 'Nuclear density, [at/b-cm]';
timeunit = 'Burnup, [GWd/tHM]';
nAS = 1;
ttt = 30;
for nAS=1:9
plotdata(ttt,nAS,uran,BURN_T2,N_ACT,ACT_LIST,flag_savefig,folder,plotunit,timeunit,'Uranium');
plotdata(ttt+1,nAS,pu,BURN_T2,N_ACT,ACT_LIST,flag_savefig,folder,plotunit,timeunit,'Plutonium');
plotdata(ttt+2,nAS,ma,BURN_T2,N_ACT,ACT_LIST,flag_savefig,folder,plotunit,timeunit,'Minor Actinides');
ttt = ttt + 3;
%plotdata(25,nAS,tru,BURN_T2,N_ACT,ACT_LIST,flag_savefig,folder,plotunit,timeunit,'Transuranics');
end

%% FISSION PRODUCTS ========================================
%% 
plotunit = 'Nuclear density, [at/b-cm]';
timeunit = 'Burnup, [GWd/tHM]';
nAS = 1;
ttt = 100;
for nAS=1:9
plotdata(ttt,nAS,[1:12],BURN_T2,N_FP,FP_LIST,flag_savefig,folder,plotunit,timeunit,'1-12');
plotdata(ttt+1,nAS,[13:25],BURN_T2,N_FP,FP_LIST,flag_savefig,folder,plotunit,timeunit,'13-25');
plotdata(ttt+2,nAS,[26:38],BURN_T2,N_FP,FP_LIST,flag_savefig,folder,plotunit,timeunit,'26-38');
plotdata(ttt+3,nAS,[39:49],BURN_T2,N_FP,FP_LIST,flag_savefig,folder,plotunit,timeunit,'39-49');
ttt = ttt + 4;
%plotdata(25,nAS,tru,BURN_T2,N_ACT,ACT_LIST,flag_savefig,folder,plotunit,timeunit,'Transuranics');
end




