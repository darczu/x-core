%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Function for testing new plotting Rev 21-02-2021
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Consider Plot function
% https://uk.mathworks.com/matlabcentral/answers/438218-many-plots-in-object-oriented-way
%
%
% Consider Chart Class:
% https://uk.mathworks.com/help/matlab/examples.html?category=developing-chart-classes&s_tid=CRUX_topnav
%
% function plotMyData(figIndex, seq, dc, exportFileName, plotOpts)
%
%







% ===================================================================
% Typical plot 
% ===================================================================	

a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
		content = {'Decay Heat Long'};
		run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{1};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			y1 = EDF.data{k}(:,2); %
			semilogy(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'EFPD' ] ];
		end
		% DHR
		mlcfile = EDF.file{12};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			y2 = EDF.data{k}(:,2); %CAV0DHR
			semilogy(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'CAV'] ];
		end
	end	
		ylabel(labelPower); xlabel(labelTime);  % xlabel('Time, [h]');  %ylim([0 2.5e7]); %xlim([0 66]);	
		legend('Location','northeast');
		xlim([0 mediumtime]);
		run standard_plot2v4.m;
end	
