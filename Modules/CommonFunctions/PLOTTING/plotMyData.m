%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Function for plotting Rev 21-02-2021
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Based on https://uk.mathworks.com/matlabcentral/answers/438218-many-plots-in-object-oriented-way
% Author TADA Mathwork user
%
% 
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plotMyData(figIndex, seq, dc, exportFileName, plotOpts)
    if nargin < 4; exportFileName = ['figure' num2str(figIndex) '.emf']; end
    if nargin < 5; plotOpts = struct; end
    if ~isfield(plotOpts, 'title');         plotOpts.title = ['abcdb' num2str(figIndex)]; end
    if ~isfield(plotOpts, 'startPlotAt');   plotOpts.startPlotAt = 10; end
    if ~isfield(plotOpts, 'xlabel');        plotOpts.xlabel = 'Duartion (s)'; end
    if ~isfield(plotOpts, 'ylabel');        plotOpts.ylabel = '2nd(-)'; end
    if ~isfield(plotOpts, 'legend');        plotOpts.legend = {num2str(figIndex), num2str(figIndex)}; end
    if ~isfield(plotOpts, 'ylim');          plotOpts.ylim = [0 6]; end
    if ~isfield(plotOpts, 'xlim');          plotOpts.xlim = [0 20]; end
    
    figure(figIndex);
    plot(seq(plotOpts.startPlotAt:end),dc(plotOpts.startPlotAt:end),'b');
    hold on;
    plot(seq,ones(size(seq)) * 5,'r')
    ylim('auto');
    title(plotOpts.title);
    xlabel(plotOpts.xlabel);
    ylabel(plotOpts.ylabel);
    legend(plotOpts.legend);
    ylim(plotOpts.ylim);
    xlim(plotOpts.xlim);
    grid on;
    hold off;
    print('-dmeta',exportFileName);
end