cmap6 = hsv(6); cmap11 = hsv(11); cmap14 = hsv(14); cmap28 = hsv(28); cmap16 = hsv(16); %color maps
cmapT = ['k', 'r', 'b', 'm', 'y','c','k','b','r','m','c','y'];
smap = {'-', '--', '-.', '.','-x','-o','-s','<-','s-'};
cmapdyn = [lines(7); colorcube(15)];

cmap6 = hsv(6); cmap8 = hsv(8); cmap11 = hsv(11); cmap14 = hsv(14); cmap28 = hsv(28); cmap16 = hsv(16); cmap18 = hsv(19); %color maps
cmapT = ['k', 'r', 'b', 'm', 'y','c','k','b'];
cmapcases = hsv(numel(cases));
col1 = { 'b','r', 'm', 'c'}; col = char(col1);
%col2 = {'b', 'r', 'm', 'c', 'g', 'y'};

colL = lines(15); 
col2 = colL;
col3 = repmat(colL(1,:),[15,1]);

colP = prism(10);
smap2 = {'-', '--', '-.', ':','-', '--', '-.', ':'};
ls = char({'LineStyle'}); 
cl = char({'Color'}); %col = char(col1);
ms = char({'MarkerSize'});
lw = char({'LineWidth'});
msym = char({'Marker'});

% Color map with different color for different case
%cmapdyn = hsv(numel(cases));
%cmapdyn = [lines(7); hsv(numel(cases)-7)];
%cmapdyn = [lines(7); jet(numel(cases)-7+2)];
%cmapdyn = [lines(6); hsv(numel(cases)-7+2)];
%cmapdyn = [lines(7); prism(6); colorcube(10)];
%cmapdyn = [linespacer(22)]

%cmapdyn = [lines(7); prism(6); varycolor(20)];
%cmapdyn = [lines(7); prism(6); linespacer(20)]; %%
%cmapdyn = [lines(7);  linespacer(20)]; %%
%cmapdyn = [lines(7);  varycolor(20)]; %%
%cmapdyn = linespacer(numel(cases));
%cmapdyn = linespacer(numel(cases),'qualitative');
%cmapdyn = linespacer(numel(cases),'sequential');
% colormapTestImage(cmapdyn)

labelT = 'Temperature, [K]'; labelP = 'Pressure, [Pa]'; 
labelD = 'Displacement, [mm]'; labelM = 'Mass, [kg]';
labelDistance = 'Distance, [m]';
labelPower = 'Power, [W]';
labelHours = 'Time, [h]';
labelSeconds = 'Time, [s]';
 %xlabel('Time, [s]');
labelS = 'Vol. Strain, [-]';



