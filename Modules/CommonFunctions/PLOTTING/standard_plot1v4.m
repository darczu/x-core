%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OPERATIONS BEFORE PLOT VERSION 4
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%a = a+1;
fig=figure(a);
%figure(a)

if flag3 == 0 
	clf(fig,'reset')
end	
%if flag3 == 1 
%	hold on;
%end

	hold on; 


set(0, 'DefaulttextInterpreter', 'none'); 

if flag3 == 1
	%if k_num == numel(cases)
	if k_num == last_case
		name = ['Fig', num2str(a)];
		for xxx = 1:numel(cases)
			%name = [name, '_', code1, '_', cases_names{xxx}];
			name = [name, '_', cases_names{xxx}];
			%name = [name, '_', cases{xxx}];
		end
	end
end

if flag3 == 0
		name = ['Fig', num2str(a)];
		%for xxx = 1:numel(cases)
			%name = [name, '_', code1, '_', cases_names{xxx}];
			name = [name, '_', cases_names{k_num}];
			%name = [name, '_', cases{xxx}];
		%end
end



%xlabel('Time, [s]');
xlabel('Time, [s]','Interpreter','none'); %'FontSize',15 
if( flag_hours == 1)    xlabel('Time, [h]','Interpreter','none' ); end % ,'FontSize',15
%ylabel('Mass, [kg]');
%ylabel('Mass, [kg]','Interpreter','none'); % 'FontSize',15


grid on; 
box on;
grid on;

%xlim([0, ceil(tend)]); % ylim([0 inf]);
%xlim([0 inf])
%xlim([0 plottime]);
xlim([0 plottime./Tconst]);

if k_num == 1 
    labels = {};
	labels1 = {};
	labels2 = {};
end

if k_num > 1

	
	%	labels = {};
	%else
		hLegend = findobj(gcf, 'Type', 'Legend');
		%get text
		
	if isempty(hLegend)	
		labels = {};
	else
		labels = hLegend.String;
	end
end

%legend('Location','northwest');
legend('Location','northeast');

legend_font_size = 9;

if(title_flag==1) 
    title(file,'FontSize',9 );
elseif(title_flag==2)
    title(content,'FontSize',9 );   % title('Pressurizer Pressure')   
end

set(0, 'DefaulttextInterpreter', 'none');



% ===================================================================
% ===================================================================
% ===================================================================



%{
%% Figure 1 PPS

h=figure(1+a)

% ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
 title(file);
elseif(title_flag==2)
 title('Pressurizer Pressure')   
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15 );

% ==
hold on
if(flagMAAP == 1)
    plot(MAAP4_T,MAAP4_1,'-r','LineWidth',2);
end

plot(a1(:,1)./Tconst,a1(:,2),'LineWidth',2,cl,col(2,:),ls,'-');
 set(0, 'DefaulttextInterpreter', 'none');
box on;
grid on;
xlabel('Time, [s]','Interpreter','none','FontSize',15 );
if( flag_hours == 1)    xlabel('Time, [h]','Interpreter','none','FontSize',15 ); end
if(flagMAAP == 1)
legend('MAAP4','MELCOR','Location','NorthEast','Orientation','vertical');
else
 legend('MELCOR','Location','NorthEast','Orientation','vertical');
end

ylabel('Pressure, [Pa]','Interpreter','none','FontSize',15 );
xlim([0 tend]);
if (flag2 == 1)
    %print('-djpeg100','-r200',[file '/Figures/Fig1.jpg'])
    print('-dtiff','-r300',[file '/Figures/Fig1.tiff'])
    %savefig([file '/Figures/Fig1.fig'])
    saveas(gcf,[file '/Figures/Fig1.fig'],'fig');
end
%}