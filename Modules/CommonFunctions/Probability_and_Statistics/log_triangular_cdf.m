%Log-Triangular Cumulative Distribution Function
% Developed by Piotr Darnowski
function  cdf = log_triangular_cdf(x,LB,mode,UB)

%https://www.vosesoftware.com/riskwiki/LogTriangledistribution.php
% pr http://www.minem.gob.pe/minem/archivos/file/dgaam/publicaciones/curso_cierreminas/02_T%C3%A9cnico/03_Calidad%20de%20Aguas/TecCalAg-L4_GoldSim_App%20A-B.pdf
%b = log(mode)

a = log(LB);
b = log(mode);
c = log(UB);

N = numel(x);


for i=1:1:N


	if x(i) <= LB
		cdf(i) = 0.0;
	elseif x(i) <= mode
		cdf(i) = ((log(x(i))-a).^2)./((b-a).*(c-a));	

%((log(x-a)^2)/((b-a)*(c-a))
% Inverse function is:
%e^(sqrt(x) sqrt(a - b) sqrt(a - c)) + a



%https://www.wolframalpha.com/widgets/view.jsp?id=d08726019e4a2a15cb1d49092e4d0522


	elseif x(i) > mode
		cdf(i) =  1-((c-log(x(i))).^2)./((c-b).*(c-a));
		
	%	1-((c-log(x))^2)/((c-b)*(c-a))   == 
	% (c^2 -c*a - b*c + b*a - (c - log(x))^2)/((c-b)*(c-a))
	
	
	
	
	%	y=exp √-xc2+xca+xbc-xba+c2-ca-bc+ba+c
	
	%x = %e^(sqrt(c^2-b*c-a*c+a*b)+c)

		
		% in the reference there is error [1]
	elseif x(i) >= UB
		cdf(i) = 1.0;
	end	

end