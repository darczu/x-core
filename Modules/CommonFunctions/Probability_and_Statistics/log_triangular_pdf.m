%Log-Triangular Distribution Function
function  pdf = log_triangular_pdf(x,LB,mode,UB)
% Developed by Piotr Darnowski
%https://www.vosesoftware.com/riskwiki/LogTriangledistribution.php
% pr http://www.minem.gob.pe/minem/archivos/file/dgaam/publicaciones/curso_cierreminas/02_T%C3%A9cnico/03_Calidad%20de%20Aguas/TecCalAg-L4_GoldSim_App%20A-B.pdf
%b = log(mode)

N = numel(x);

b = log(mode);
a = log(LB);
c= log(UB);


for i=1:1:N
	if x(i) <= LB
		pdf(i) = 0.0;
	elseif x(i) <= mode
		pdf(i) = 2.*(log(x(i))-a)./(x(i).*(b-a).*(c-a));
	elseif x(i) > mode
		pdf(i) = 2.*(c-log(x(i)))./(x(i).*(c-b).*(c-a));
	elseif x(i) >= UB
		pdf(i) = 0.0;
	end	

end
