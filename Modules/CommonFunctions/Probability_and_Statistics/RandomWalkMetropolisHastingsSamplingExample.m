%% Random Walk Metropolis-Hastings Sampling
% Use Random Walk Metropolis-Hastings sampling to generate sample data from 
% a standard normal distribution.

rng default  % For reproducibility
delta = .5;
pdf = @(x) normpdf(x);
proppdf = @(x,y) unifpdf(y-x,-delta,delta);
proprnd = @(x) x + rand*2*delta - delta;   
nsamples = 15000;
x = mhsample(1,nsamples,'pdf',pdf,'proprnd',proprnd,'symmetric',1);
%% 
% Plot the sample data.

figure;
h = histfit(x,50);
h(1).FaceColor = [.8 .8 1];
%% 
% _Copyright 2015 The MathWorks, Inc._