function base=BisecSearch(x, temp)

bot = 1;
top = length(temp);
cntr = 0;

while(top-bot > 1)
    mid = floor((top+bot)/2);
    if(x<temp(mid))
        top = mid;
    else
        bot = mid;
    end
    cntr = cntr + 1;
    if(cntr > 10)
        break
    end

end

base = bot;

