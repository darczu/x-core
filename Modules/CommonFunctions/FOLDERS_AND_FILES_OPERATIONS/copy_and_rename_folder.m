% function creating folders in the folder where it is executed
% input is cell with names of the folders
% input1 - folder to be copied
% input2 - names of folders
% Example 
% input1 = 'case1'  max size 1
% input2 = {'case1a' 'case1b'}   cell type vector

function copy_and_rename_folder(input1,input2)


% Moves matlab CD to the location where source file is located
% if(~isdeployed)
%   cd(fileparts(which(mfilename)));
% end



n = numel(input2);

dir = cd;

for i=1:n
	cmd1{i} = ['xcopy ', '',input1, ' ',input2{i} , ' /s /e /i'];
end




for i=1:n
	disp(cmd1{i});
	%system([cmd1{i},'&'], '-echo');
	system([cmd1{i}]);
	
end	
