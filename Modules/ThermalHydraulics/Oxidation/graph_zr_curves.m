%% script for generating the graphs

% creation August 6,2019


%% Testing formulas - oxidation rate

% baker-just formula

x=[4:0.01:7];

x1 = [4: 0.01:5.33] ;
x2 = [5.40:0.01:7]  ;

xx= [x1 x2];

%%  Baker-Just

y_baker = 33.3 * 100 * exp(-22897./ (  1e4./x   )    );


%% Lemmon

%33.3e6/4.82e8 * 

y_lemon = 0.7821 * exp(-17111./ ( 1e4./x)  );


%% Urbanic-Heidrick 

% below 1580 degC / 1853.15 K --- do 5.39621
y_urbanic2= 29.6*exp(-16820./ (  1e4./x2   )    );

% above 1580 degC / 1873.15 K
y_urbanic1 = 87.9*exp(-16610./ (  1e4./x1   )    );

xx = [x1 x2]  ;
y_urbanic =[y_urbanic1 y_urbanic2] ;

%% Prater-Courtright Leistikow-Schanz

% below 1853 K --- do 5.33
y_prater_schanz2 = 425.8 *exp(-20962./ (  1e4./x2   )    );

% above 1873 ---- od 5.40
y_prater_schanz1 = 26763.6 *exp(-26440./ (  1e4./x1   )    );

y_prater_schanz = [y_prater_schanz1 y_prater_schanz2] ;

%% Prater-Courtright Cathcart-Pawel

% below 1853 K --- do 5.33
y_prater_cathcart2 = 294.2 *exp(-20100./ (  1e4./x2   )    );

% above 1873 ---- od 5.40
y_prater_cathcart1 = 26763.6 *exp(-26440./ (  1e4./x1   )    );

y_prater_cathcart = [y_prater_cathcart1 y_prater_cathcart2] ;


%% Urbanic-Heidrick Cathcart-Pawel

% below 1853 K --- do 5.33
y_urbanic_cathcart2 = 294.2 *exp(-20100./ (  1e4./x2   )    );

% above 1873 ---- od 5.40
y_urbanic_cathcart1 = 87.9 *exp(-16610./ (  1e4./x1   )    );

y_urbanic_cathcart = [y_urbanic_cathcart1 y_urbanic_cathcart2] ;

%%

graph_5curves_log_time( x,y_baker, 'Baker-Just' ,...
       xx,y_urbanic, 'Urbanic-Heidrick' ,...
       xx,y_prater_schanz, 'Prater-Courtright Leistikow-Schanz' ,...
       xx,y_prater_cathcart, 'Prater-Courtright Cathcart-Pawel' ,...
       xx,y_urbanic_cathcart, 'Urbanic-Heidrick Cathcart-Pawel' ,...
    4,7,  'Parabolic models for Zr oxidation, reactions with steam', '10 000/T (T in K)', 'kg^2/(m^4s)', ...
  'NorthEast', 'Formulas_oxidation_comparison', 'ON_')

