%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Heat Structure Nodalization Calculations  Based on MELCOR input  Rev 10-07-2019
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: In1 		- Inner Radius (or elevation)
% 2: In2 		- Outer Radius (or elevation) - can be vector with different values with boundary location for HS intervals
% 3: n          - number of divisions,  n+1 is number of nodes, n, can be a vector with number of division for subsequent zones 
% 4: mode 		- geometry type:
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1:			- Nodalization as single vector
% 2:			- Area calculations =  [area_inner, area_outer, area_cross];
% 3:			- material number/zone number
% 4:			- Nodaliation as matrix for different zones
% 5:			- Input for MELCOR
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 		1 or	'CYLINDRICAL'  		or 'CYL'			data - height   size 1x1
%		2 or    'RECTANGULAR'  		or 'RECT'			data - side length [side1, side2] size 2x1
%		3 or 	'SPHERICAL'			or 'SPHERE'  		data - null
%		4 or 	'BOTTOMHALFSPHERE' 	or 'B_HEMI'			data - fraction of a sphere
%		5 or 	'TOPHALFSPHERE'	   	or 'U_HEMI'			data - fraction of a sphere
% 5: data  - additional geometry data for area/other calclations - see #4
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [ND, A, NDadd]  = HS_ND(4.8,5.0,10)
% [ND, A, NDadd]  = HS_ND(4.8,5.0,10,1)
% [ND, A, NDadd]  = HS_ND(4.8,5.0,10,'CYL')
% [ND, A, NDadd]  = HS_ND(4.8,[4.9 5.0], [5,4], 'CYL' )			%Two layers, first divided to 5 and second to 4 intervals
% [ND, A, NDadd]  = HS_ND(4.800,[4.9000 5.0000 5.1], [4,4,4], 'CYL' )
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function  [ND,  A, mat,  HS_ND, NDadd] = HS_ND(In1,In2,n,mode,data)

if(nargin < 5)
	data = 1.0;
end
if(nargin < 4) 
	mode = 1;   %Default
end
if(nargin < 3)
	n = 5; 				%Default
end
if(nargin < 2)
	error('ERROR: Not enough arguments');
end

if numel(In2) ~= numel(n) 
	error('ERROR: Second parameter In2 has to have same number of arguments as third n');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if numel(In2) == 1
	IR = In1;  		
	OR = In2;

	if(OR<=IR) 
		error('ERROR:  R1 >= R2');
	end

	thickness = OR-IR;  %thickness
	delR = thickness/abs(n);
	ND = IR:delR:OR; 
	ND = ND';
	NDadd = [];
	mat = ones(numel(ND),1);
	mat(end)=0;
else 
	IR = In1;  		
	OR = In2(end);
	OOR = In2;

	if(OR<=IR) 
		error('ERROR:  Rinner >= Router');
	end

	thickness_total = OR - IR;
	n_total = sum(n);

	for i=1:numel(In2)
		if i == 1
			thickness(i) = OOR(i)-IR;	
		else
			thickness(i) = OOR(i)-OOR(i-1);
		end	
	end
	
	delR = thickness./abs(n);

	ND1 =  []; ND1 = -1.*ones(max(n)+1,numel(In2));
	ND2 = []; ND2 = zeros(n_total,1);

	for i=1:numel(In2)
		if i == 1
			 NDnew = IR:delR(i):OOR(i); NDnew = NDnew';
			 ND1(1:(n(i)+1),i) = NDnew;
			 ND2 = NDnew;
			 mat = ones(numel(NDnew),1);
		else
			NDnew = OOR(i-1):delR(i):OOR(i);   NDnew= NDnew';
			ND1(1:(n(i)+1),i) = NDnew;
			ND2 = [ND2(1:end-1); NDnew];
			mat = [mat(1:end-1); i.*ones(numel(NDnew),1)];
		end	
	end
	mat(end)=0;
	
	ND = ND2;
	NDadd = ND1;
	%for i=1:numel(delR)
	%	newND = OOR:delR(i):OOR(i); 
	%	newND = newND' 
	%end
end


switch mode
	case {1,'CYL','CYLINDIRCAL','C'}			%1 - Inner Rad. (In1) and Outer Rad. (In2) (or elevation)
		height = data; %Input data is height of the cylinder
		area_inner = 2*pi*IR*height;
		area_outer = 2*pi*OR*height;

		area_cross   = pi*OR*OR-pi*IR*IR;  


	case {2, 'SPH', 'SPHERICAL', 'S'}
		area_inner = 4*pi*IR*IR;
		area_outer = 4*pi*OR*OR;
		area_cross = 0;


	case {3, 'REC', 'RECTANGULAR', 'R'}
		side1  = data(1); 

		if numel(data)>1 
			side2  = data(2);
		else
			side2=side1;
		end

		area_inner = side1*side2; 
		area_outer = area_inner;
		area_cross = 0; 

	case {4, 'BHS', 'BOTTOMHALFSPHERE', 'B'}
		fraction = data(1);  %Fraction of a half sphere, if fraction = 1 it is half hemisphere
		area_inner = fraction*4*pi*IR*IR/2;
		area_outer = fraction*4*pi*OR*OR/2;
		area_cross = 0;


	case {5, 'THS', 'TOPHALFSPHERE', 'T'}
		fraction = data(1);  %Fraction of a half sphere, if fraction = 1 it is half hemisphere
		area_inner = fraction*4*pi*IR*IR/2;
		area_outer = fraction*4*pi*OR*OR/2;
		area_cross = 0;



	otherwise
		error('ERROR: No such option available');
end


A = [area_inner, area_outer, area_cross];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Temporary part
num = 1:numel(ND);
num = num';
500.0.*ones(numel(ND),1);
HS_ND=[num, num, ND, 500.0.*ones(numel(ND),1), mat];








