%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Heat Structure Nodalization Check
% Applied formula form MELCOR UG Manual to check if the node thickness is appropiate
% Based on MELCOR input
% 17-10-2018
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In MELCOR - surface mesh interval should not be smaller than thermal diffusion length LD
% dx_surf >= LD = sqrt(4*k*dt/rho/Cp) = sqrt(4*k*dt/CpVol)
% k - thermal cond.
% dt - maximum comp. timestep
% CpVol - Vol. Heat Capacity times density
% rho - density
%Cp - specigic heat capacity
% LD - thermal diffusion lenght
% RHO IN SI UNITS!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXAMPLES:
% [LD, check] = HS_ND_CHECK(15.0,2500,500,0.1,1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function  [LD, check] = HS_ND_CHECK(k,rho,Cp,dx,dt)



if(nargin < 4) 
 dx = 0.1;   %Default
end

if(nargin < 5) 
 dt = 1.0;   %Default
end

LD = sqrt(4.*k.*dt./(rho.*Cp));

check = dx >=LD;


