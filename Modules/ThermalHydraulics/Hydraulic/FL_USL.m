%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Form Losses Calculations for Flow Path in MELCOR models
% TO BE USED IN FL_USL MELCOR INPUT FIELD
% Based on MELCOR Manual and Traning Materials
% 15-10-2018, small update 20-04-2020
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In1 		- Form Local Loss Factors Vector 
% In2 		- FLARA = Nominal Area, scalar or vector - minimal value is selected
% In3		- FL_SEG  SAREA = Segment Area
% In4		- FLOPO = Open Fraction - Can be omitted
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXAMPLES:
% K = [1.0 0.2 2.0]; FLARA =0.05; SAREA =[0.05, 0.785, 0.68];
% Kform =   FL_USL(K, FLARA, SAREA);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function  [Kout] = FL_USL(In1,In2,In3,In4)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function  [Kout] = FL_USL(In1,In2,In3,In4)

N = numel(In1);

if(nargin < 4)
 In4 = ones(N,1);  %Default - all fully open
end


K  = In1; 		% Form Loss Factors  
Aj = min(In2);	% FLARA
As = In3; 		% FL_SEG  SAREA
Fj = In4; 		% FLOPO

Kout = sum( K.*((Aj./As).^2)) 
