%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rev 16-10-2018
% Right Hand Side for Colebrooke-White equations calcs.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Function F(x) = 0 = y  where C-W equation equals to zero.
function y = funCW(x,Re,D,e)

y = [ 1./sqrt(x) - 3.48 + 4.0.*log10(2.0.*e./D + 9.35./(Re.*sqrt(x))  )];
%fun = inline('1/sqrt(f) - 3.48 + 4.0*log10(2.0*e/D + 9.35/(Re*sqrt(f))  )');

