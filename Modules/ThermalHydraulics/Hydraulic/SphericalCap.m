%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sphere Cap Proporties Calc Tool   
% Rev-1 22-05-2019; Rev-2 03-05-2020
% Calculates spherical cap proporties
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Provide two values defining spherical cap
% In1       - R, h, a, r, theta,V, S
% In2       -     
% option1   - info about In1
% option2   - info about In2
% option3   - info about output
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Out       - 
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In1       -
% option1   -
% option2   -
% OPTION1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  'R', 'SphereRadius'      - Sphere radius
%  'h', 'CapHeight'         - Cap height - distance beetwen sphere tip and cap base
%  'a', 'Distance'          - Distance - distance beetwen center of sphere and cap base
%  'r', 'CapBaseRadius'     - Cap base radius http://www.ambrsoft.com/TrigoCalc/Sphere/Cap/SphereCap.htm
%  'theta', 'CapAngle'      - Cap angle - angle beetwen sphere axis and radius connected with cap base radius http://www.ambrsoft.com/TrigoCalc/Sphere/Cap/SphereCap.htm
%  'V', 'CapVolume'         - Cap Volume
%  'S', 'CapSurface'        - Cap surface without base area
% OPTION2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  'R', 'SphereRadius'      - Sphere radius
%  'h', 'CapHeight'         - Cap height - distance beetwen sphere tip and cap base
%  'a', 'Distance'          - Distance - distance beetwen center of sphere and cap base
%  'r', 'CapBaseRadius'     - Cap base radius http://www.ambrsoft.com/TrigoCalc/Sphere/Cap/SphereCap.htm
%  'theta', 'CapAngle'      - Cap angle - angle beetwen sphere axis and radius connected with cap base radius http://www.ambrsoft.com/TrigoCalc/Sphere/Cap/SphereCap.htm
%  'V', 'CapVolume'         - Cap Volume
%  'S', 'CapSurface'        - Cap surface without base area
% OPTION3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  'R', 'SphereRadius'      - Sphere radius
%  'h', 'CapHeight'         - Cap height - distance beetwen sphere tip and cap base
%  'a', 'Distance'          - Distance - distance beetwen center of sphere and cap base
%  'r', 'CapBaseRadius'     - Cap base radius http://www.ambrsoft.com/TrigoCalc/Sphere/Cap/SphereCap.htm
%  'theta', 'CapAngle'      - Cap angle - angle beetwen sphere axis and radius connected with cap base radius http://www.ambrsoft.com/TrigoCalc/Sphere/Cap/SphereCap.htm
%  'V', 'CapVolume'         - Cap Volume
%  'S', 'CapSurface'        - Cap surface without base area
%  'SB', 'CapSurfaceBase'   - Cap surface with base area
% REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m 
% http://www.ambrsoft.com/TrigoCalc/Sphere/Cap/SphereCap.htm
% http://mathworld.wolfram.com/SphericalCap.html
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Out] = SphericalCap(In1,In2,option1,option2,option3)

    if(option1 == option2)  error('Cannot use same input twice');  end
    if(nargin <1)           error('No such option');  end
    if(nargin <2)           error('No such option');  end
    if(nargin <3)           option1 = 'R';      end
    if(nargin <4)           option2 = 'h';      end
    if(nargin <5)           option3 = 'V';      end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    switch option1
        case {1,'R','SphereRadius'}
            R = In1;
        case {2,'h','CapHeight'}
            h = In1;
        case {3,'a','Distance'}
            a = In1;
        case {4,'theta','CapAngle' }
            theta = In1;
        case {5,'V','CapVolume'}
            V = In1;
        case {6,'S','CapSurface'}   
            S = In1;
        otherwise
            disp('ERROR#1: No such case');
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%            
    switch option2
        case {1,'R','SphereRadius'}
            R = In2;
        case {2,'h','CapHeight'}
            h = In2;
        case {3,'a','Distance'}
            a = In2;
        case {4,'theta','CapAngle' }
            theta = In2;
        case {5,'V','CapVolume'}
            V = In2;
        case {6,'S','CapSurface'}   
            S = In2;
    otherwise
            disp('ERROR#2: No such case');
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%            



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%            
    switch option3
        case {0,'all'}
            Out = [R, h, a, theta, V, S, SB];
            
        case {1,'R','SphereRadius'}
            Out = R;
        case {2,'h','CapHeight'}
            Out = h;
        case {3,'a','Distance'}
            Out = a;
        case {4,'theta','CapAngle' }
            Out = theta;
        case {5,'V','CapVolume'}
            Out = V;
        case {6,'S','CapSurface'}   
            Out = S;
        case {7,'SB','CapSurfaceBase'}   
            Out = SB;
    otherwise
     disp('ERROR#2: No such case');
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%            