%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Colebrook-White equation for friction factor (fanning)
% Rev-2 03-05-2020, Rev-1 16-10-2018
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Solves Colebrook-White Equation
% WARNING: It is fanning friction factor !!!!
% Reynolds can be given by Num_Re or Num_Re_s
% C-W works for Re >=5000, for Re <= 2000 f=16/Re, in beetwen there is transition region
% Remeber! Darcy Weisbach is 4 times Fanning Friction factor
% (Darcy-Weisbach Friction Factor) = 4 * (Fanning friction factor)    f = 4*C_f   
% f_FF = f_DW/4 ;  i.e. f_FF/Re=16/Re   f_DW=64/Re
%  Fanning C_f = 2*tau_w/(rho*v^2)     
%  D-W		f  = 8*tau_w/(rho*v^2) 
% dp = 4*C_f*(L/D)*(rho*v^2)/(2) = f*(L/D)*(rho*v^2)/(2)
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Re	-  Reynolds Number, 
% D		-  Diameter, 
% e		-  Roughness
% flag	-  Testing flag: 0 - all (compare), 1 - fsolve, 2 - fzero, 3 - iteration
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% out	- fanning friction factor
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% f_CW(10000,0.1,1e-7)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%function [out] = f_CW(Re,D,e)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [out] = f_CW(Re,D,e,flag)

	if (nargin < 4) flag = 3;  end %Default iteration method	
	if (nargin < 3)  e = 0.0;  end	
	if (Re<0 | D<0 | e<0) error('ERROR#1:  Input lower than zero'); end
	f1 = 0; f2=0; f3=0; f0 = 0.5; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(Re<5000)
    out = 16./Re;  %    fanning friction factor !
else    

	if((flag == 1)|(flag == 0))

		f0 = 0.5; %first guess for f
		[f1, Fx] = fsolve(@(x)fun_CW(x,Re,D,e),[f0]);  %1 way
		out = f1;  %output
	end	
	if((flag == 2)|(flag == 0))

		f0 = 0.5; %first guess for f
		[f2, Fxx] = fzero(@(x)fun_CW(x,Re,D,e),[f0]);  %2 way
		out = f2;  %output
	end	
	if((flag == 3)|(flag == 0))
		%Iterative scheme - 3 way
		eps = 1e-7;
		f3 = f0;  					%First guess 
		f3prev=1.0; 				%Initial value
		i=0;
			while (abs((f3 - f3prev)/f3)) > eps 
				f3prev = f3;   %przypisanie wartosci z poprzedniej iteracji n-1
				LHS = 3.48 - 4.0.*log10(2.0.*e./D + 9.35./(Re.*sqrt(f3prev))); %RHS of the C-W
				f3 = (1./LHS).^2;
				i=i+1;
			end
		i;  %Number of iterations
		out = f3;  %output
	end
	
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(flag==0)
	disp('f1 - fsolve | f2 - fzero | f3 - iteration scheme')
	out= [f1, f2, f3];
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%f = linspace(0,1,500);
%f = f0;
%fun - równanie C-W przeyrównane do zera dla zastosowanie funkcji fsolve
%fun = 1./sqrt(f) - 3.48 + 4.0.*log10(2.0.*e./D + 9.35./(Re.*sqrt(f))  );
%fun = 1/sqrt(f) - 3.48 + 4.0*log10(2.0*e/D + 9.35/(Re*sqrt(f))  );
