%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Form loss coefficients (K factors) database for minor losses
% Expansions, Contractions, Elbows, Valves
% Revision 2 30-04-2020 //  Revision 1 20-10-2018 // Revision 0 2013. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Data mostly taken from: Fluid Mechanics, Cengel, Cimbala, p. 349, 350, 351 but also other reference. TABLE 8-4.
% Pressure drop: dp = K*v^2/2rho     Head:   		  H = K*v^2/2g
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function K = K_form(mode,In1,In2,In3)
% mode - function number/name
% In1, In2, In3   - different input parameters
% flow - type  L - laminar; T - turbulent; 0  - no
% d, D - diamter d - lower, D - higher
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% K 	- form loss ceofficient dp = K *v^2/2rho
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1  Sharp-edge inlet  	- based on Cengel   (DEFAULT)
% mode = {1 'Inlet_Sharp_Edge' 'Sharp_Edge_Inlet' 'InShEd'}  
% In1 - no; In2 - no; In3 - no
%   |_____
% -> _____
%   |
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2 or 3	Rounded inlet   - Cengel
% mode = {2 3 'Inlet_Rounded_Edge' 'InRoEd'}
% In1	- D  pipe diameter
% In2	- r	  edge radius	
%	if r >  0.2*D : Well 	 rounded inlet edge, r/D > 0.2 
%	if r <= 0.2*D :	Slightly rounded inlet edge, r/D = 0.1   
%   \_____
% -> _____
%   /
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 4	Pipe inlet with reentrant - Cengel
% mode = {4 'Inlet_Reentrant' 'InReEn'} 
% In1 - no; In2 - no; In3 - no 
% only mode
% It works only when:  wall thickness t << D and pipe length in the upstream volume  l ~= 0.1D
% It current development these conditions are not checked !!!
%	__|_____ 
%->	__ _____
%	  |   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5 Pipe exit: Sharp-edged or rounded or reentrant to infinite volume - Cengel
% All have the same value, dependent only on the flow regime.
% The kinetic energy correction factor alpha is a result.
% mode = {5 'Outlet' 'Exit' 'Outflow' 'Pipe_Exit'} 
% In1 - flow type:  1, 'T', 'Turbulent' or 2, 'L', 'Laminar' 
% In2 - no; In3 - no 
%  _____|		%  _____/		%  _____|_
%->_____	or	%->_____	or	%->_____ _
%       |		%       \		%       |
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 6  Sudden Expansion for inlet d < D outlet 
% In1		- d, smaller inlet diameter
% In2		- D, larger outlet diameter 
% In3		- No.
%         ______
%   _____|    
% ->_____    D
%   d    |______   
% Note: larger velocity, associated with smaller pipe section, is used by convention in head loss calculations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 7 Sudden Contraction
% In1		- d, smaller inlet diameter
% In2		- D, larger outlet diameter 
% In3		- type,  'W' 'Wikipedia'  or 'C' 'Cengel'
% Wikipedia based equation. In Cengel there is curve
% Wikipedia is default
% K = 0.45.*(1 - B);  	B = (d.^2) ./ (D.^2);
% ___
%    |____ 
%-> D ____d
% ___|
% Note: larger velocity, associated with smaller pipe section, is used by convention in head loss calculations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 8  Gradual Expansion - Cengel based 
% In1		- d, smaller inlet diameter
% In2		- D, larger outlet diameter 
% In3		- no input	   
% For this case theta = 20 degress, in Cengel there is only 20 degress
%         ______ 
%   _____/    
% ->_____    D   theta
%   d    \______    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 9  Gradual Contraction - Cengel
% theta = angle - full angle of the expansion
% In1	- theta angle of of the cone
% In2, In3 - no
% ___
%    \____ 
%-> D ____d		theta
% ___/
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 10	Smooth Bend 90 degress, flanged (kołnierzowy) or threaded (gwintowane) - Cengel 
% In1	- type: 1 'F' 'Flanged' or 2 'T' 'Threaded'
%	_______
%->	_____  \
%		 | |
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 11   Miter Bend with and without vanes - 90 degress - Cengel 
% In1	- type: 1 'NV'   2  'WV'
%	_______ 	%	_______ 	
%->	_____  |	%->	_____\\|
%		 | |    %		 | |       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 12	Threaded Elbow 45 degress - Cengel 
% In1 - no input
%	_______  45 deg
%->	_____  \
%		  \ \
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 13 	Return Bend 180 degress - Cengel 
% In1	- type: 1 'F' 'Flanged' or 2 'T' 'Threaded'
%	_______  180 deg
%->	_____  \
%	_____| |
%   _______/   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 14	TEE Branch Flow - Cengel 
% In1	- type:  1 'F' 'Flanged' or 2 'T' 'Threaded'
% In2   - type2: 1 'Branch'   or 2 'Line'      select flow path 
%        ^ Branch
%	 ___| |__
% -> ________ -> Line
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 15 Threaded Union - Cengel 
% In1 - no input
%	 ___...____
% -> ___...____ -> Line
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 16 Globe Valve Fully open
% In1 - no input
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 17 Angle Valve Fully open
% In1 - no input
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 18 Ball Valve Fully open
% In1 - no input
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 19 Swing Check Valve
% In1 - no input
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXAMPLES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% K = K_form(1)
% K = K_form(2, 0.5, 0.1); K = K_form(3, 0.2,1.0)
% K = K_form(4)
% K = K_form(5,'T')
% K = K_form(6, 0.1,1.0)
% K = K_form(7, 0.1,1.0); K = K_form(7, 0.1,1.0,'W')
% K = K_form(8, 0.1,1.0)
% K = K_form(9, 50)
% K_form(10,'T');   K_form(10,'F')
% K_form(11,'NV');  K_form(11,'WV')
% K_form(14,2,1);   K_form(14,1,1);   K_form(14,1,2);   K_form(14,2,2);
% K_form(15)
% K_form(16); K_form(17);  K_form(18);  K_form(19)
% K_form(20,0.5)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function K = K_form(mode,In1,In2,In3)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function K = K_form(mode,In1,In2,In3)
	if(nargin <1)   mode = 1; end  %Default
	if(nargin <2)	In1=[]; end     
	if(nargin <3)	In2=[]; end    
	if(nargin <4) 	In3=[]; end    
	if ( (In1<0) | (In2<0) | (In3<0)) error('ERROR #1: Zero input entry'); end
switch mode
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PIPE INLET %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {1 'Inlet_Sharp_Edge' 'Sharp_Edge_Inlet' 'InShEd'}  %Default
		if(nargin>1) disp('WARNING: In1,In2,In3 was not used'); end
		%if(In1 > 0)	disp('WARNING: In1 was not used'); end
		%if(In2 > 0)	disp('WARNING: In2 was not used'); end
		%if(In3 > 0)	disp('WARNING: In3 was not used'); end
		K = 0.5;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
	case {2 3 'Inlet_Rounded_Edge' 'InRoEd'}
		r = In2;	
		D = In1;
		if(In3 > 0)	disp('WARNING: In3 was not used'); end
		if(r > 0.2*D)
			K = 0.03;
		elseif(r <= 0.2*D)
			K = 0.12;
		end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
	case {4 'Inlet_Reentrant' 'InReEn'} 
		if(nargin>1) disp('WARNING: In1,In2,In3 was not used'); end
		disp('WARNING: It works only when:  wall thickness t << D and pipe length in the upstream volume  l ~= 0.1D');
		K = 0.8;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PIPE EXIT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {5 'Outlet' 'Exit' 'Outflow' 'Pipe_Exit'} 
		if(nargin <2)	disp('WARNING: In1 was not given, default used');  In1 = 'T';   end 
		if(In2 > 0)		disp('WARNING: In2 was not used'); end
		if(In3 > 0)		disp('WARNING: In3 was not used'); end
		flow = In1;
		switch flow
			case {1 'T' 'Turbulent'}
				K = 1.0;    % The kinetic energy correction factor alpha = 1.0
			case {2 'L' 'Laminar'}
				K = 2.0;	% The kinetic energy correction factor alpha = 2.0
			otherwise
				error('ERROR: No such option');
		end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
%%% EXPANSIONS AND CONTRACTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {6 'Expansion_Sudden' 'Expansion' 'Sudden_Expansion' 'SuExp'}		
		if(In1>=In2) error('ERROR: Condtion: d < D'); end
		if (In3>0) error('WARRNING: In3 not used'); end
		d = In1;
		D = In2;
		K = (1-((d.^2)./(D.^2))).^2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {7 'Contraction_Sudden' 'Sudden_Contraction' 'SuCon'} 
		if(In1>=In2)  error('ERROR: Condtion: d < D'); end
		if(nargin<4) disp('WARNING: In3 was not given, default used');  In3 = 'W';   end
			d = In1;
			D = In2;
			B = (d.^2) ./ (D.^2);
			type = In3;
			
			switch type
			% Two curves are different at low values.
				case {1, 'Wikipedia', 'W' } 
					K = 0.45.*(1 - B); %Wikipedia based equation !!!! Be careful
				case {2, 'Cengel', 'C' }
					% Cengel based interpolation - measured with software
					d2D2 = [0.0 0.1  0.2  0.3  0.4 0.5   0.6  0.7 0.8  0.9 1.0];
					KL   = [0.50      0.48293      0.43614      0.37337      0.30569       0.2402      0.18023      0.12616     0.078055     0.035332 0.0];
					K = interp1(d2D2, KL, B, 'linear','extrap');
				otherwise
					error('ERROR: Not such input');
			end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {8 'Expansion_Gradual' 'Gradual_Expansion' 'GrExp'} 
		% only for theta = 20 degres
		if(In1>=In2)  error('ERROR: Condtion: d < D'); end
		if(nargin>3) disp('WARNING: In3 not used');  end
		d = abs(In1);
		D = abs(In2);
		dD = d/D;			
		dD1 = [0.2, 0.4 , 0.6 , 0.8];
		K1  = [0.3, 0.25, 0.15, 0.1];
		K = interp1(dD1, K1, dD, 'linear','extrap');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {9 'Contraction_Gradual' 'Gradual_Contraction' 'GrCon'}
		if(nargin>2) disp('WARNING: In2, In3 not used');  end
		if ((In1 <= 15) | (In1 >= 90)) error('ERROR: Angle has to be larger than 0 and lower than 90 degress'); end
		theta = abs(In1);   %In degrees	
		angle1 = [30, 45, 60];
		K1     = [0.02, 0.04, 0.07];
		K = interp1(angle1, K1, theta, 'linear','extrap');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% BENDS AND BRANCHES AND ELBOWS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {10 'Smooth_Bend' 'Bend_Smooth'}
		if(nargin>2) disp('WARNING: In2, In3 not used');  end
		if(nargin<2) type = 'F';   end  %Default
		type = In1; %type
		switch type
			case {1, 'F', 'Flanged'}
				K = 0.3;			
			case {2, 'T', 'Threaded'}
				K = 0.9;
			otherwise
			error('ERROR: bad input');
		end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {11 'Miter_Bend' 'Bend_Miter'}
		if(nargin>2) disp('WARNING: In2, In3 not used');  end
		if(nargin<2) type = 'NV';   end  %Default
		type = in1; %type
		switch type
			case {1, 'NV', 'Without_Vanes', 'No_Vanes'}
				K = 1.1;			
			case {2, 'WV', 'With_Vanes', 'Vanes'}
				K = 0.2;
			otherwise
				error('ERROR: bad input');
		end	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {12 'Elbow' 'Elbow_45' '45_Elbow' 'Threaded_Elbow' 'Elbow_Threaded'}
		if(nargin>1) disp('WARNING: In1, In2, In3 not used');  end
			K = 0.4;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {13 'Return_Bend' 'Bend_180'}  
		if(nargin>2) disp('WARNING: In2, In3 not used');  end
		if(nargin<2) type = 'F';   end  %Default
		type = In1; %type
		switch type
			case {1, 'F', 'Flanged'}
				K = 0.2;			
			case {2, 'T', 'Threaded'}
				K = 1.5;
			otherwise
				error('ERROR: bad input');
		end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {14 'TEE' 'Branch_Flow' 'Tee'}
			if(nargin>2) disp('WARNING: In2, In3 not used');  end
			if(nargin<2) type = 'F';   end  %Default
		type = In1; %type
		type2 = In2; %type2 branch or line flow 
		switch type2
			case {1 'Branch'}		% Branch Flow
				switch type
					case {1, 'F', 'Flanged'}
						K = 1.0;			
					case {2, 'T', 'Threaded'}
						K = 2.0;
					otherwise
						error('ERROR: bad input');
				end
			case {2 'Line'} 		% Line Flow
				switch type
					case {1, 'F', 'Flanged'}
						K = 0.2;			
					case {2, 'T', 'Threaded'}
						K = 0.9;
					otherwise
						error('ERROR: bad input');
				end
		end			
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {15 'Th_Union' 'Threaded_Union' 'Union' 'ThUn'}
		if(nargin>1) disp('WARNING: In1, In2, In3 not used');  end
			K = 0.08;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% VALVES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {16 'VLV_Globe' 'GVLV' 'Globe_Valve' 'Valve_Globe' 'VLV_G'}
		if(nargin>1) disp('WARNING: In1, In2, In3 not used');  end
			K = 10.0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {17 'VLV_Angle' 'AVLV' 'Angle_Valve' 'Valve_Angle' 'VLV_A'}
		if(nargin>1) disp('WARNING: In1, In2, In3 not used');  end
			K = 5.0;
	
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {18 'VLV_Ball' 'BVLV' 'Ball_Valve' 'Valve_Ball' 'VLV_B'}
		if(nargin>1) disp('WARNING: In1, In2, In3 not used');  end
			K = 0.05;
			
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {19 'VLV_SwingChk' 'SCVLV' 'Swing_Check_Valve' 'Check_Valve_Swing' 'VLV_SC'}
		if(nargin>1) disp('WARNING: In1, In2, In3 not used');  end
			K = 2.0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {20 'VLV_Gate' 'GVLV' 'Gate_Valve' 'Valve_Gate' 'VLV_G'}
		if(nargin>2) disp('WARNING:  In2, In3 not used');  end
		if(nargin<2) OpenFrac = 1.0; end %Default				
		OpenFrac = In1;
		if(OpenFrac > 1.0) disp('Open Fraction cannot be larger than 1.0'); end
		if(OpenFrac < 0.0) disp('Open Fraction cannot be negative'); end
		if(OpenFrac < 0.25) error('ERROR: Cannot use this function for valve open fraction < 0.25'); end
		OpenF = [1.0, 0.75, 0.5, 0.25];
		KL    = [0.2, 0.3 , 2.1, 17.0];
		K = interp1(OpenF, KL, OpenFrac, 'linear','extrap');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	otherwise
		error('ERROR: Unknown input');
end
	
	
	
	
%1. Pipe entrance or exit
%2. Sudden expansion or contraction
%3. Bends, elbows, tees, and other fittings
%4. Valves,open or partially closed
%5. Gradual expansions or contractions

% More: https://en.wikipedia.org/wiki/Minor_losses_in_pipe_flow
% http://fluid.itcmp.pwr.wroc.pl/~znmp/dydaktyka/fundam_FM/Lecture11_12.pdf
%
% https://www.nrc.gov/docs/ML1220/ML12209A041.pdf
% https://www.nuclear-power.net/nuclear-engineering/fluid-dynamics/minor-head-loss-local-losses/
% https://en.wikipedia.org/wiki/Minor_losses_in_pipe_flow
% http://www.thermexcel.com/english/ressourc/pdclocal.htm
% https://www.katmarsoftware.com/articles/pipe-fitting-equivalent-length.htm
