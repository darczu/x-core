%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sphere Proporties Tools
% Rev-1 22-05-2019, Rev-2 03-05-2020 
% Calculates Sphere/SphereCap Volume, Area proporties
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In1       - input value , r,D,V,S,C := radius, diameter, volume, surface, circumference
% option1   - info about input In1 value 'r','D','V','S','C'  := radius, diameter, volume, surface, circumference
% option2   - info about output value 'all', r','D','V','S','C'  := all, radius, diameter, volume, surface, circumference
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Out   	- Output: all, radius, diameter, volume, surface, circumference
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In1		- 1.0; option1	- 'r';  option2	- 'all'
% OPTION1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% option1   - default 'r'
%  info about input In1 value 'r','D','V','S','C'  'radius', 'diameter', 'volume', 'surface', 'circumference'
% OPTION2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% option2   - default 'all'
%  info about output  value 'all', r','D','V','S','C'  radius', 'diameter', 'volume', 'surface', 'circumference'
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% http://www.ambrsoft.com/TrigoCalc/Sphere/Cap/SphereCap.htm
% http://mathworld.wolfram.com/SphericalCap.html
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SphereCalc(1.0,'r',1)
% SphereCalc(1.0,'r','all')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [Out] = SphereCalc(In1,option1,option2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Out] = SphereCalc(In1,option1,option2)
    if(nargin <1)   error('No such option'); end
    if(nargin <2)   option1 = 'Default';     end
    if(nargin <3)   option2 = 'Default';     end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    switch option1
        case {'radius', 'r','Radius',1}
            r = In1;
            D = 2*r;
            V = (4./3).*pi.*(r.^3);
            S = 4.*pi*r.^2;
            C = 2.*pi.*r;
        case {'diameter', 'D','Diameter',2}
            D = In1;
            r = D./2;
            V = (4./3).*pi.*(r.^3);
            S =  4.*pi*r.^2;
            C =  2.*pi.*r;
        case {'V', 'volume', 'Volume',3}
            V = In1;
            r = (3*V./(4.*pi)).^(1./3)
            D = 2.*r;
            S = 4.*pi*r.^2;
            C = 2.*pi.*r;
        case {'S', 'A', 'area', 'Area','Surface','surface',4}
            S = In1;
            r = 0.5.*sqrt(S./pi);
            D = 2.*r;
            V = (4./3).*pi.*(r.^3);
            C = 2.*pi.*r; 
        case {'C','Circ','Circumference','circumference',5}
            C = In1;
            r = C./(2.*pi);
            D = 2.*r;
            V = (4./3).*pi.*(r.^3);
            S = 4.*pi*r.^2;
        otherwise
            disp('ERROR#1: No such case');
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    switch option2
        case {'all', 'Default', 'All',0}
            disp(['radius, diameter, volume, surface, circumference'])
            Out = [r, D, V, S, C];
        case {'radius', 'r','Radius',1}
            Out = r;
        case {'diameter', 'D','Diameter',2}
            Out = D;
        case {'V','Volume',3}
            Out = V;
        case {'S', 'A','Area','Surface','surface','area',4}
            Out = S;
        case {'C','Circ','Circumference','circumference',5}        
            Out = C;
        otherwise
            disp('ERROR#2: No such case');
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%