% Rev 06-04-2017 NOT USED IN THE CORE REVISE
% =============================================================== %
% Piotr Darnowski Warsaw University of Technology
% Lower Head Calculation - crossing points
% Developed in the begining of the 2015
% Added as a part of the SACRAM code in June 2015
% Modified 18.06.2015
%
% =============================================================== %
%NTLP       = COR_LP(5)  %number of axial nodes for lower plenum
%COR_ZP_LAST_LH
%Z_0 - elevation zero

Z_DZ2 = zeros(NTLP,nR);
for i=1:nR
 Z_DZ2(1:NTLP,i) = COR_ZP_DZ(1:NTLP);    %Matrix with DZ for all rings
end

Z_Z2 = zeros(NTLP,nR);
for i=1:nR
 Z_Z2(:,i) = COR_ZP_Z(1:NTLP);    %Matrix with Z elevation for all rings
end

R_DR2 = zeros(NTLP,nR);
R_DR2(:,1)    = RINGR(1); 
for i=2:nR
R_DR2(1:NTLP,i) = RINGR(i)-RINGR(i-1);    %Matrix with DR for all rings
end

R_R2 = zeros(NTLP,nR);
for i=1:NTLP
 R_R2(i,:) = RINGR';    %Matrix with R for all rings
end

Z_LAST_LH3 = Z_Z2(NTLP,:)+Z_DZ2(NTLP,:); %LAST LH TOP ELEVATION
RR2 = R_R2; rr2 = (R_R2 - R_DR2); %VOLUMES IN THE LOWER PLENUM
% Volumes of cylindrical elements of the LH
VOL_2 = pi.*Z_DZ2.*(RR2.^2 - rr2.^2 );
VOL_TOT_2 = sum(sum(VOL_2));
VOL_FRAC_2 = VOL_2./VOL_TOT_2; 
Z_RVLH = Z_0 + RVLH; %elevation of the lower head radius center

% =============================================================== %
% METHODS TO CALCULATE LOWER HEAD COR NODES  RINGS AND AXIAL LEVELS
% =============================================================== %
Z_LH1 = [Z_Z2(:,1);   Z_LAST_LH3(1)];  %LOWER HEAD AXIAL ELEVATIONS

%(Z_RVLH-Z)^2 + R^2 = RVLH^2 %Equation for lower head for known Z
%PROPOSED VALUES OF AXIAL LEVELS IN LOWER PLENUM BASED ON RINGR INPUT
cross_z=[];
for i=1:nR
    ring = RINGR(i);
    cross_z = [cross_z, fzero(@(x) ring.^2 + (Z_RVLH-x).^2 - RVLH^2,0)];
end
cross_z=cross_z';
disp('LOWER HEAD RINGS AND ELEVATIONS OF COROSECTION WITH LOWER HEAD');
disp('   RING [m]    ||   LEVEL [m]');

LH_CROSS_Z = [ [0.0; RINGR] [Z_0 ;cross_z]];
disp(LH_CROSS_Z); %AXIAL LEVELS AT WHICH RINGS CROSS WITH LOWER HEAD
%fzero(@(x) RINGR(1).^2 + (Z_RVLH-x).^2 - RVLH^2,0);
%R_LH1 = fzero(@(x) sqrt(x.^2 - Z_LH1))

%CORSS FOR AXIAL LEVES N Z_LH1
cross_r = [];

for i=1:NTLP 
    level = Z_LH1(i+1);
    cross_r = [cross_r, fzero(@(x) x.^2 + (Z_RVLH-level).^2 - RVLH^2,5)];
end
cross_r=cross_r';
LH_CROSS_R = [ [0.0; cross_r], Z_LH1];
disp(LH_CROSS_R); %AXIAL LEVELS AT WHICH RINGS CROSS WITH LOWER HEAD

% =============================================================== %
% CALCULATION OF LOWER PLENUM COR PACKAGE VOLUMES
% SPECIAL CAUTION SHOULD BE TAKEN TO TOP LOWER PLENUM LEVELS
% FORMULA Vol = 1/6 pi*h*(3*a^2+3*b^2 + h^2) is correct only for Spherical
% Segment. IT is no correct for disk woth a=b.!!!!!!!!!
% Vol = 1/6 pi*h*(3*a^2+3*b^2 + h^2)
% Vol' = Vol - pi*h*r^2
% Surface area of the surface - with no top and bottom S=2pi*R*h
% =============================================================== %
%VOLUMEs use LH_CROSS variable
VOL_LH=[]; VOL_LH_TOT=0.0;
for i=1:NTLP
    a= LH_CROSS_R(i,1);
    b= LH_CROSS_R(i+1,1);
    h= Z_LH1(i+1) -Z_LH1(i);
    VOL_LH(i) = (1./6).*pi*h*( 3*(a^2)+3*(b^2) + h^2);
    VOL_LH_TOT = VOL_LH_TOT + VOL_LH(i);
    VOL_LH_TAB(i)= VOL_LH_TOT;
end

 

