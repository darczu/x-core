%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Volume Calculation Tool   Rev 02-05-2019
% UNDER-CONSTRUCTION
% Useful tool for pipe volume calculations
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In1   - Outer diameter vector, [m]
% In2   - Inner diameter vector, [m] - use 0.0 for solid
% In3   - Length vector, [m]
% option1 - shape
% option2 - calculation mode
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Volume - volume [m3]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In1       - no default
% In2       - 0.0    solid rod
% In3       - 1.0    1m length default
% option1   - 'circle' 
% option2   - 
% OPTION1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cross-secton shape
% 'circle' or 'circ' or 1 or 'C' or 'pipe'  
% 'square' or 'sqr' or 2 or 'S' or 'duct'
% 'rectangle' or 'rec' or 3 'R' 
% OPTION2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% length-type
%
%
%
% option3 - volume calculation approach - trapezoidal ?
% Trapezoidal intergration
% provide curve with centerline X,Y
% provide curve with centerline and function for shape of the pipe 
%
%
%
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Volume = PipeVol(In1,In2,In3,option1,option2,option3)
error('NOT FINISHED');


    if(nargin <1)
       error('No such option'); 
    end
    if(nargin <2)
        In2 = 0.0; %Default is solid rod
    end
    if(nargin <3)
        In3 = 1.0; %Default 1 m
    end
    if(nargin <4)
        option1 = 'circle'; 
    end
    if(nargin <5)
        option2 = {'normal'};
    end
    if(nargin <6)
        option3 = {''};
    end

    if(isempty(In2))
        In2 = zeros(numel(In1)); %Solid rod
    end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%




    switch option1
        case {'circle', 'circ',1,'C', 'pipe'}
            OD = In1;   %outer diam vector
            ID = In2;   %inner diam vector
            dL  = In3;   %length vector

            A1 = 0.25.*pi.*ID.^2;
            A2 = 0.25.*pi.*(OD.^2 - ID.^2);

            Vol1 = A1.*dL;   %Volume of the fluid 
            Vol2 = A2.*dL;   %Volume of the wall


        otherwise
            disp('ERROR#1: VolCalc - No such case');
    end

    Volume = [Vol1', Vol2'];



    error('DOES NOT WORK');

    