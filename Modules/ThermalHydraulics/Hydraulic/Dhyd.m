%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Hydraulic diameter calculations
% Rev 2 Update 30-04-2020 Rev 1 2019, Rev 0 2013
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1:	In1		-  pipe/inner diameter    also    outer diameter for annular
% 2:	mode	-  type of geometry
% 3:	In2 	-  outer diameter or inner for annulus
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: D_h	- Hydraulic diamter , [m]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In1		-	no default value, input has to be provided
% mode		-	'circular'
% In2		-	0.0
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1 or 'circular'	or '' or '' 	- pipe					 			% In1 - Diamter   In2 - No Meaning
% 2 or 'sqr_rods' 	or '' or '' 	- square lattice of rods 			% In1 - Diamter   In2 - Pitch
% 3 or 'tri_rods'	or '' or '' 	- triangular lattice of rods  		% In1 - Diamter   In2 - Pitch
% 4 or 'annulus'	or '' or '' 	- circular annulus - two circles  	% In1 - Diamter Outer   In2 - Diameter Inner 
% 5 or 'sqr_duct'	or '' or '' 	- square annulus - two ducts  		% In1 - Side Length   In2 - Side Langth 
% 6 or 'rec_duct'   or '' or ''  	- rectangular annulus - two ducts	% In1 - Side Length   In2 - No Meaning
%
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dhyd(0.0099,'sqr_rods',0.0126)    % In1 = fuel rod diameter   In2 = Fuel Pitch
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%function [D_h] = Dhyd(In1,mode,In2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [D_h] = Dhyd(In1,mode,In2)

if (nargin < 2)
	mode = 'circular';  %default
end	

if (nargin < 3)
	In2=0.0;
end	

if or(In1<0,In2<0)
	error('ERROR#1:  Input lower than zero')
end


switch mode
	case {1 'circular' 'circle'  'c' 'circ' 'O' 0}  % In1 - Diamter   In2 - No Meaning
		D = In1;
		A = 0.25*pi*D*D;
		P = pi*D;
		
		if(In2 > 0)	disp('WARNING: In2 was not used'); end
		
	case {2 'sqr_rods' 'square_lattice' 'PWR'} 	% In1 - Diamter   In2 - Pitch
		D = In1;
		pitch = In2;
		A = pitch*pitch - 0.25*D*D*pi;	
		P = pi*D;
		
	case {2 'tri_rods' 'triangle_lattice' 'triangular_lattice' 'SFR'} 	% In1 - Diamter   In2 - Pitch
		D = In1;
		pitch = In2;
		A = (sqrt(3)/4)*pitch*pitch - pi*D*D/8;
		P = pi*D*0.5;	

	case {3 'annulus' 'ann' 'circular_annuli'}      % In1 - Diamter Outer   In2 - Diameter Inner
		OD = In1;
		ID = In2;
	   
	   A = pi*0.25*(OD*OD-ID-ID);
	   P = pi*(OD+ID);
	
	case {4 'sqr_duct' 'square_duct'}     % In1 - Side Length   In2 - Side Langth 
	  a = In1;
	  A = 4*a*a;
	  P = 4*a;
	  
	  	if(In2 > 0)
			disp('WARNING: In2 was not used');
		end
	
	case {5 'rec_duct' 'rectangular_duct'}     % In1 - Side Length   In2 - No Meaning
	  a = In1;
	  b = In2;
	  A = 4*a*b;
	  P = 2*(a+b);
	
	
end

% S - Area
% P - Perimeter
% D - diamter

D_h = 4*A/P;

