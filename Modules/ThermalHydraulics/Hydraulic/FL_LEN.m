%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Inertial Flow Path Length Calculations for MELCOR models
% Based on MELCOR Manual
% 15-10-2018
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In1 		- Vector with Areas
% In2 		- Vector with Section Lengths
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXAMPLES:
%  A =[0.5 1.0 3.0]; dx = [1.0 0.5 3.0]; [FLLEN, FLARA] = FL_LEN(x,dx)
%  x =[0.05 0.785 0.68]; dx = [1.0 0.25 1.5]; [FLLEN, FLARA] = FL_LEN(x,dx)  - MELCOR MATERIALS BASED 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function  [FLLEN, FLARA] = FL_LEN(In1,In2)

A  = In1; %Vector with Areas
dx = In2; %Vector with lengths 

FLARA = min(In1);
FLLEN = FLARA*sum(dx./A);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Among the fundamental properties of a flow path are its area and its length. In many 
%cases, a flow path represents a geometry with varying flow area. In most cases, the 
%area input as FLARA on the FL_GEO record in FL input should be chosen as the 
% >>>> minimum area along the path. <<<

%It is common, but not universal, in 
%constructing FL package input to choose FLARA as the >>>maximum area<<< that will ever be 
%open in the flow path. 
% 
% IT IS NOT CLEAR - BUT IN MATERIALS IT IS MINIMUM AREA (!). Hence, the second statment is for open fraction
%
%The length specified as FLLEN on the FL_GEO record in the FL input will be used as an 
%inertial length. The inertia of a flow path is a measure of the average mass per unit area 
%along its length. If the area is not constant, a rigorous approach is to choose the length 
%input as FLLEN in conjunction with the flow path area, FLARA, to match this average:
% FLLEN/FLARA = Integral dx/A(x)  beetwen "from" to "to" - center of CVs.
%Here x measures distance along a path from the center of the “From”’ volume to the 
%center of the “To” volume, and A(x) is the flow area at x. In practice, this is rarely 
%necessary.