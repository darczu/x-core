% Developed in 2018. Updated 2020.
% Example of hydraulic flow path calculations in MELCOR with local losses.
% Use of FL_USL and K_form functions

FLARA1= [0.004185387	...
0.001809557	...
0.000706858	...
0.000706858	...
0.00066052	...
0.00066052	...
0.000706858	...
0.000314159	...
0.000314159	...
0.000314159	...
0.000314159	...
0.000314159	...
0.000314159	...
0.000706858	...
0.000706858	...
];

K=[];
%FL-100 
FormLoss = [K_form(1)  K_form(5,'T')]; 
FLARA    = [ FLARA1(1) ];
FLSEG    = [0.004185387   0.003520];

K = [K; FL_USL(FormLoss, FLARA,FLSEG)];

%FL-150
FormLoss = [K_form(5,'T')];  
FLARA    = [ FLARA1(2)];
FLSEG    = [0.003520  ];
K = [K; FL_USL(FormLoss, FLARA,FLSEG)];

%FL-201
FormLoss = [K_form(9,48,76)  ];  %0.63
FLARA    = [ FLARA1(3)];
FLSEG    = [0.001809557 ];
K = [K; FL_USL(FormLoss, FLARA,FLSEG)];

%FL-203
FormLoss = [K_form(9,30,48)  ];  %0.8
FLARA    = [ FLARA1(4)];
FLSEG    = [0.000706858 ];
K = [K; FL_USL(FormLoss, FLARA,FLSEG)];

%FL-250
FormLoss = [0.9 ];  
FLARA    = [ FLARA1(5)];
FLSEG    = [ 0.00066052 ];
K = [K; FL_USL(FormLoss, FLARA,FLSEG)];

%FL-301
FormLoss = [0.4 ];  
FLARA    = [ FLARA1(6)];
FLSEG    = [ 0.00066052  ];
K = [K; FL_USL(FormLoss, FLARA,FLSEG)];

%FL-303
FormLoss = [0.9  ];  
FLARA    = [ FLARA1(7)];
FLSEG    = [0.000706858  ];
K = [K; FL_USL(FormLoss, FLARA,FLSEG)];

%FL-350
FormLoss = [K_form(9,20,30)  ];     %0.66
FLARA    = [ FLARA1(8)];
FLSEG    = [0.000314159  ];
K = [K; FL_USL(FormLoss, FLARA,FLSEG)];

%FL-351
FormLoss = [0.0  ];  
FLARA    = [ FLARA1(9)];
FLSEG    = [ 0.000314159 ];
K = [K; FL_USL(FormLoss, FLARA,FLSEG)];

%FL-352
FormLoss = [0.0  ];  
FLARA    = [ FLARA1(10)];
FLSEG    = [ 0.000314159 ];
K = [K; FL_USL(FormLoss, FLARA,FLSEG)];

%FL-353
FormLoss = [0.0  ];  
FLARA    = [ FLARA1(11)];
FLSEG    = [ 0.000314159 ];
K = [K; FL_USL(FormLoss, FLARA,FLSEG)];

%FL-354
FormLoss = [1.5  ];  
FLARA    = [ FLARA1(12)];
FLSEG    = [ 0.000314159 ];
K = [K; FL_USL(FormLoss, FLARA,FLSEG)];

%FL-359
FormLoss = [K_form(8,16)  0.9 ];  
FLARA    = [ FLARA1(13)];
FLSEG    = [ 0.000314159   0.000706858];
K = [K; FL_USL(FormLoss, FLARA,FLSEG)];

%FL-360
FormLoss = [0.0  ];  
FLARA    = [ FLARA1(14)];
FLSEG    = [ 0.000706858 ];
K = [K; FL_USL(FormLoss, FLARA,FLSEG)];

%FL-365
FormLoss = [0.9  K_form(8,30)  ];  
FLARA    = [ FLARA1(15)];
FLSEG    = [0.000706858 0.000706858  ];
K = [K; FL_USL(FormLoss, FLARA,FLSEG)];
