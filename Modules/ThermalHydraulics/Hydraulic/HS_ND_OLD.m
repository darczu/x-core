%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Heat Structure Nodalization Calculations  Based on MELCOR input
% Update 03-07-2019 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In1 		- input 1 
% In2 		- input 2
% option 	- type of calculation 
%			1 - Inner Rad. and Outer Rad. (or elevation)
%			2 - Outer Rad. and Thickness (or lower elevation)
%			3 - Inner Rad. and Thickness (or upper elevation)
% type 		- geometry type:
% 			'CYLINDRICAL'  		or 'CYL'
%			'RECTANGULAR'  		or 'RECT'
%			'SPHERICAL'			or 'SPHERE'  
%			'BOTTOMHALFSPHERE' 	or 'B_HEMI'
%			'TOPHALFSPHERE'	   	or 'U_HEMI'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXAMPLES:
%  HS_ND(4.8,5.0,10,1,'CYL')
%  HS_ND(4.8,5.0,10)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function  [ND] = HS_ND(In1,In2,n,option,type)

if(nargin < 5) 
 type = 'CYLINDRICAL';   %Default
end

if(nargin < 4)
 option = 1;			%Default
end

if(nargin < 3)
 n = 5; 				%Default
end

%if (strcmp(type,'CYL'))
%	type = 'CYLINDRICAL';
%elseif(strcmp(type,'SPHERE'))
%	type = 'SPHERICAL';
%elseif(strcmp(type,'RECT'))
%	type = 'RECTANGULAR';
%elseif(strcmp(type,'B_HEMI'))
%	type = 'BOTTOMHALFSPHERE';
%elseif(strcmp(type,'U_HEMI'))	
%	type = 'TOPHALFSPHERE';
%end

switch option
	case 1			%1 - Inner Rad. (In1) and Outer Rad. (In2) (or elevation)
		
				IR = In1;
				OR = In2;
				
				if(OR<=IR) 
					error('ERROR #1:  R1 >= R2');
				end
				
				 dR = OR-IR;  %thickness
				 delR = dR/abs(n);
				 ND = IR:delR:OR;
			%switch type	
			%case 'CYLINDRICAL'			
			%case 'SPHERICAL'
			%case 'RECTANGULAR'
			%case 'BOTTOMHALFSPHERE'
			%case 'TOPHALFSPHERE'
			%end
	case 2			%2 - Outer Rad. or Top Elevation (In1)  and  Thickness (In2) 
		
				OR = In1;
				dR = In2;
				IR = OR - dR;
				delR = dR/abs(n);
				ND = IR:delR:OR;			
			%switch type
			%case 'CYLINDRICAL'
			%case	'SPHERICAL'
			%case 'RECTANGULAR'
			%case 'BOTTOMHALFSPHERE'
			%case 	'TOPHALFSPHERE'		
			%end
	case 3			%3 - Inner Rad. or Low Elevation (In1)  and  Thickness (In2)
				IR = In1;
				dR = In2;
				OR = IR + dR;
				delR = dR/abs(n);
				ND = IR:delR:OR;	
		
			% switch type
			%case 'CYLINDRICAL'
			%case 'SPHERICAL'
			%case 'RECTANGULAR'
			%case 'BOTTOMHALFSPHERE'
			%case 'TOPHALFSPHERE'
			%end
end








