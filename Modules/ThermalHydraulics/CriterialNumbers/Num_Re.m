%Reynolds number calculation
% m - mass flow [kg/s]
% A - flow area [m2]
% G - mass flux m/A  [kg/s/m2]
% Dh - hydrualic diam  [m]
% mi - dynamics viscosity  [Pa s]
%
% function [Re] = fun_Re(m,A,Dh,mi)

function [Re] = Num_Re(m,A,Dh,mi)

G = m./A;

Re = G.*Dh./mi;

