%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Piotr Darnowski 2015, Start: June 2015
% Debris Bed generating tool - Part of the SACRAM code
% Tootal dedicated to generate random debris beds for Lower Plenum Criticality Analysis
% Basically to use with Explicit particle/ pebble bed fuel model
%
% Output file format: <x> <y> <z> <r> <u>  as in the SERPENT manual
% pbed <u0> <uf> "<inputfile>" [<options>] SERPENT input part
% It is difficult to develop it - use available algorithms
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear; clc; close all;

flag_type = 2; %1 - layered Z; 2 - random x,y,z; 
flag_plot = 1;  %1 - Visualization; 0 - no visualization
flag_save = 1; %1 - write to file; 0 - do not write file
file_pbed = 'part117.inp';  %file name

%Lx = 500; Ly=500; Lz=50;
%dimension
Lx = 50; Ly=50; Lz=50;
%LxT = LyT = 
LzT = 200.0; %Translation
d = 20.0;  %diameters of the shpere
r=d/2;     %radius 

count1 = 0;
control = 1e5; %number of iterations without sphere generation to stop iteration
%max for about 1e4 f=0.47752; 1e5 iter f=0.5068; for 1e6 f=0.5362 ; for 1e7 f = 0.544
% 1e8 - very veyr long f=0.53198 238 sec.
tic
switch flag_type
    % Z LAYERED STRUCUTRE =============================== 
    case 1
      
      N = Lz/d;  %number of layers
     X=[]; Y=[]; Z=[]; x0 =0; y0=0; z0=r; z = [];

    for i=1:N
    count1 =0; count2 = 0;
    x0 = rand*Lx; y0=rand*Ly; z0=i*d-r;
         while count1 < control  
           x = rand*Lx; 
           y = rand*Ly;
           z = i*d - r;
          count2 = count2+1;
           %
%           if(  ((x-x0).^2 + (y-y0).^2)  > r^2 )
           if(  ((x-x0).^2 + (y-y0).^2)  > d^2 )
            x0 = [x0; x];
            y0 = [y0; y];
            z0 = [z0; z];

           else
            count1 = count1+1;   
           end
         end
      X = [X; x0];
      Y = [Y; y0];
      Z = [Z; z0];
      count2

    end
    NN = size(X,1)

 % X,Y,Z RANDOM LAYERED STRUCUTRE ===============================
    case 2
       
         X=[]; Y=[]; Z=[]; x=[]; y=[]; z = [];
         count1 =0;
         x0 = rand*Lx; y0=rand*Ly; z0=rand*Lz;  %first sphere
       
         while count1 < control  
           x = rand*Lx; 
           y = rand*Ly;
           z = rand*Lz;
          
           if(   ((x-x0).^2 + (y-y0).^2 + (z-z0).^2)  >= d^2 )
%           if(   sum( ~(((x-x0).^2 + (y-y0).^2 + (z-z0).^2)  >= r^2) ) == 0   )
            x0 = [x0; x];
            y0 = [y0; y];
            z0 = [z0; z];

           else
            count1 = count1+1;   
           end
         end
      X = [X; x0];
      Y = [Y; y0];
      Z = [Z; z0];
     
        NN = size(X,1) 
 %  ===============================
    otherwise
        error('No such a type of strucutre generator');
end
    
    toc
    
    
    
    
    
    
    

disp('Volume fraction:');
f = (NN*((4/3)*pi*r^3))/(Lx*Lx*Lz)

%sum(z0 == i*d-r)
R = ones(NN,1).*r;
u = ones(NN,1).*2;


% PLOT SPHERES
if(flag_plot ==1)
    figure(1)
    hold on
    grid 

    scatter3(X,Y,Z)
    axis equal


    figure(2)
    hold on
    [a,b,c] = sphere;
    a=a*r; b=b*r; c=c*r;
    hold on
    grid on
    for i=1:NN
     surf(a+X(i),b+Y(i),c+Z(i));
    end
    axis equal
end




% SAVE FILE WITH SPHERES
if(flag_save == 1)
    partfile = [X-Lx/2, Y-Ly/2, Z+LzT, R, u];  %TRANSFORMATION
    dlmwrite(file_pbed,partfile,'delimiter','\t')
end
%Files generator
%dbed = fopen(dbed_filename,'w');
%fclose(dbed);