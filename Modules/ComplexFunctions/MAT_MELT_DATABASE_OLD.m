% Rev 1, 14-07-2017
% Simple database crated on the basis of the Kolev textbook - Ref. [10] Chapter 17 Table 17.2 - it is impelemnation of this data
% It is only for selected liquid materials !!!
% It estimates therma-hydraluc proporties of the matrerials
% ALL IN SI UNITS !!!!
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1:- Kolev DataBase ID of the material
% ID 		  	= [1					2		   		3				4				5				6				7				8				9				10			11];
% mat_type	= {'UO2'			'Cor'			'Zr'			'ZrO'			'Steel'		'AL2O3'		'SiO2'		'FeO+' 		'Mo' 			'Al'			'B2O3'};
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OLD 1: 		- RESULTS vector with variables  ident, T0, rho0, rho0beta, kappa, cpliq
% OLD 2: 		- INFO cell type vector with material info and info about variables
%
% 1: ident					- material id number
% 2: T0					- reference temperature, [K] - it is metling temperature
% 3: rho0					- density, [kg/m3] - it corresponds to the melting temperature
% OLD 4: rho0beta	- product of the density and volumetric thermal expansion coefficient [1/K]*[kg/m3] = [kg/K/m3]
% 4: beta					- volumetric thermal expansion coefficient [1/K]
% 5: kappa				- isothermal coefficient of compresiiblity     [1/Pa] 
% 6: cpliq					- specific heat cpaacity [J/(kgK)]
% 7: INFO					- info about the material, material name and variables info
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%OLD [RESULT, INFO] = MAT_MELT_DATABASE(1)
%
%[id, T0, rho0, beta, kappa, cpliq,INFO ] = MAT_MELT_DATABASE(1)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%OLD: function [RESULT, INFO] = MAT_MELT_DATABASE(id)
%OLD: function [ ident(id), T0(id), rho0(id), rho0beta(id), kappa(id), cpliq(id), INFO ] = MAT_MELT_DATABASE(id)
function [ ident, T0, rho0, beta, kappa, cpliq, INFO ] = MAT_MELT_DATABASE(id)


ident 		  	= [1					2		   		3				4				5				6				7				8				9				10			11];
mat_type	= {'UO2'			'Cor'			'Zr'			'ZrO'			'Steel'		'AL2O3'		'SiO2'		'FeO+' 		'Mo' 			'Al'			'B2O3'};
T0 		   	= [3113.15		2920			2098			2973			1700			2324.15	1993.15	1642			2896			933.2		723.0];
rho0 			= [8860			8105.92	6130			5991.4		6980			3055.78	2136.55	5282.07	9330			2357			1614.5];
rho0beta	= [0.6448171	0.916		0.573		0.916		0.573		0.965		0.270676	0.9			0.5496		0.233		0.270676];
cpliq			= [503				485.2		391.5546	815			776.2		1421.713	1327.889	949.1858	571.5868	1082.201	1911.473];
kappa		= [7.740216E-12	6.130165E-11	5.761464E-11	7.080252E-11	1.062111E-11	1.490410E-10	6.489103E-11	6.135623E-11	4.610873E-11	4.949039E-11	5.550999E-11];
format long g;
beta = rho0beta./rho0;

%RESULT = [ident(id), T0(id), rho0(id), rho0beta(id), kappa(id), cpliq(id)];

 ident		=ident(id);
 T0			=T0(id);
 rho0			=rho0(id);
 beta	        = beta(id);
 kappa		=kappa(id);
 cpliq		= cpliq(id);
INFO = { cell2mat(mat_type(id)), 'Variables:', '1 - ID', '2 - T0', '3 - rho0', '4 - beta', '5 - kappa', '6 - c_p_liq', 'ALL IN SI UNITS !!!!' };

%mat_type = cell2mat(mat_type(id))
