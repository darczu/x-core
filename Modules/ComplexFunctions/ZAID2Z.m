% Rev 30-03-2017
% Calculates Z knowing ZAID

function Z = ZAID2Z(ZAID)

Z = floor(ZAID./1000);
