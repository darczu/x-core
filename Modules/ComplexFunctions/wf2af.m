% Rev 28-03-2017 
% Developed in 2016 by Piotr Darnowski
% Function dedicated to re-calculate  weightfraction to the atom fraction 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Based on: MCNP Crticiality Primer II Appendix B: Calculating Atom Densities
% wf - weight fractions vector 
% af - atom fractions vector
% A_i - Atomic weight of the isotope/material
% A_m - Atomic weight of the mixture
% af = N(Isotope i)/sum(N(All isotopes))
% wf = Mass(Isotope i)/sum(Mass(All isotopes))
% if mode <= 0 calculate on its own!! otherwise uses A_mix function
% EXAMPLE:  
%      af_i = wf2af([0.333 0.667],[1.0 16.0],-1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function af_i = wf2af(wf_i,A_i,mode)

if(nargin < 3) mode = 1; end

%if(numel(mode)>1) error('ERROR (wf2af): mode only one element'); end



wf_i = wf_i./sum(wf_i); %normalization necesarry
%wf_i = wf_i';

if( mode>0)  %default

	Amix = sum(wf_i./A_i);
	
    Amix = 1./Amix;
    af_i = wf_i.*Amix./A_i;

	
elseif(mode <=0)

   Amix = A_mix(wf_i,A_i,'wf');
    af_i = wf_i.*Amix./A_i;
else
	error('N/A');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
