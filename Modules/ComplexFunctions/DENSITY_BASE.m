% Rev 01-10-2018 TO BE REVIEWED - BE VERY CAREFUL!!!
% Rev 14-07-2017
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: i 				- material number
% 2: T				- temperature
% 3: porosity	- porosity
% 4: y				- Pu vector
% 5: p				- presseure - NOT AVILABLE NOW 
% 6:
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1:	RHO			- density, [g/cm3]			
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Example: DENSITY_BASE(3,300)     - materials number
% Example: DENSITY_BASE('SS',300)  - material identification string or cell 	
% Example: DENSITY_BASE({'SS'},300)  - material identification string or cell 	
% INPUT SHOULD BE ONE SCALAR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [RHO]=DENSITY_BASE(i,T,porosity,y)
%function [RHO]=rho_MAT(i,T,porosity,y)

%intorduce pressure dependee ?

flag=1;  %Basic materials
%flag=2; %re-caclulate materials

if(nargin<4)
 y = 0.0; %no plutonium in MOX
 %disp('no plutonium in MOX fuel');
end
if(nargin<3)
 porosity = 0.0;
end



% Load available MATERIALs 
[MATERIAL, MATERIAL_NO, TYPE] = MAT_LIST();
MAT_SIZE = numel(MATERIAL);  %number of materials



%Load XSDATA Atomic masses and ZAIDs
%load XSDATA.mat  %XSDATA = [ZAID, MASS] 


%For input with cell string
if(or(iscellstr(i),ischar(i)))
	  for j=1:numel(MATERIAL)
		  if( sum(strcmp(i, MATERIAL))==0 )
			  error('ERROR #1 (DENSITY_BASE): There is no such Material available !!!');
		  end
	  end
	  i= sum(strcmp(i,MATERIAL).*MATERIAL_NO);  %Identification is the corresp. number of the material
	  %i = nonzeros(strcmp(i,MATERIAL));
%For input with integer

elseif(isnumeric(i))
	if (i>MAT_SIZE)
		error('ERROR #2 (DENSITY_BASE): There are no such material');
	elseif(i<=0)
		error('ERROR #3 (DENSITY_BASE): There are no such material');
	end	
else
 error('ERROR #4 (DENSITY_BASE): Wrong input: not integer and not cell')
end
 
% MOLAR MASSES OF MATERIALS - DEFAULT VALUES
if(nargin == 2)
	%Mol(1:MAT_SIZE)=0.0;

% Basic MELCOR materials - NEW DIVISION 2017	
	density(1) =  rho_B4C(T);       	% 'CRP' Default 
	density(2) =  rho_INC(T);    	% 'INC' Inconel 600 [5] rho = 8.47
	density(3) =  rho_SS(T);    	% 'SS304' [5]
	density(4) =  rho_SSOX(T); % 'SSOX' 
	density(5) =  rho_UO2(T,'Carbajo');   	% 'UO2'
	density(6) =  rho_ZR(T);      % 'ZR' - Zircaloy-4 [5]
	density(7) =  rho_ZRO2(T); % 'ZRO2'
	density(8) =  rho_GRP(T);   		% 'GRP' pure carbon 12
% Additional materials
	density(9) =  rho_U(T);		  % 'U'
	density(10) = rho_PUO2(T);        % 'PUO2'
	density(11) = rho_MOX(T);        % 'MOX', 
	density(12) = rho_UO2(T);    	  % 'ACTO2'
	density(13) = rho_ZR(T);      % 'FP'
	density(14) = rho_ZRO2(T); % 'FPO2'
	density(15) = rho_B4C(T);   % 'B4C' 
	density(16) = rho_AIC(T);   % 'AIC'
	density(17) = rho_FE(T);   % 'FE'	%[5]	
	density(18) = rho_SS304(T);   % 'SS' as SS-304
	density(19) = rho_CS(T);   % 'CS' based on [5] 
	density(20) = rho_AIR(T);	%air 21% 79% O2 N2 %at
	density(21) = rho_CON(T);  %[5] Concerete, Ordinary no 91 PNNL
	density(22) = rho_B2ZR(T); %Zirconium Diboride  % https://www.americanelements.com/zirconium-diboride-12045-64-6
	density(23) = rho_B2O3(T);
	
elseif(nargin == 3)
	error('ERROR #5 (DENSITY_BASE): Still not available');
	

else
 error('ERROR #6 (DENSITY_BASE): No such option');
end



RHO=density(i);