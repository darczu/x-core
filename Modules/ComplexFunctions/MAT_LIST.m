% Rev 11-04-2017 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Default Materials Compositions - MAIN MATERIALS
% To be used with MAT_LIST.m
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Basic MELCOR materials - NEW DIVISION 2017
%1	- 'CRP'
%2	- 'INC'
%3	- 'SS' - zamieniony
%4	- 'SSOX'
%5	- 'UO2'
%6	- 'ZR'
%7	- 'ZRO2'
%8	- 'GRP' 
% Additional materials
%9	- 'U'
%10	- 'PUO2'
%11	- 'MOX' 
%12	- 'ACTO2'
%13	- 'FP'
%14	- 'FPO2'
%15	- 'B4C' 
%16	- 'AIC'
%17	- 'FE'
%18	- 'SS'
%19	- 'CS'
%20	- 'AIR'
%21	- 'CON', 	normal concrete add ?
%22 - 'B2ZR'   , added in April
%23  -'B2O3'   , added 11-07-2017

% materials are also in MOLAR_BASE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [MATERIAL, MATERIAL_NO, TYPE]=MAT_LIST(TYPE)

   if(nargin<1)
		TYPE = 'BWR';
   end

   MATERIAL = {'CRP', 'INC' , 'SS', 'SSOX', 'UO2', 'ZR', 'ZRO2', 'GRP', ...  %1 - 8 Basic  
						'U' , 'PUO2' , 'MOX', 'ACTO2', ...
						'FP', 'FPO2', ...
						'B4C', 'AIC', ...
						'FE', 'SS304', 'CS', ...
						'AIR', 'CON', ...
						'B2ZR', 'B2O3'...
			   };
  

  % 
   MATERIAL_NO = 1:1:numel(MATERIAL);
   MATERIAL = MATERIAL';
   MATERIAL_NO = MATERIAL_NO';
   

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
%Default Molar Masses.







