% Rev2 10-07-2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function dedicated to calculate corium mixture isotopic number densities with input in form of mass vectors
% calculates many parameters - including number of moles
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: mass_i 		- vector with massess of elements MELCOR TYPE [kg]
% 2: mat_i  		- vector of material names it is cell type [string]
% 3: T 			- Temp_i vector or scalar [K]
% 4: rho_model 	- density model: 'AtomcFrac' 'MassFrac' 'Hull'
% 5: A_model		- atomic weight calculations: 'Complex' 'Simple' 
% 6: output_mode - type of output
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: N_mix		- total mixture number density   for corium
% 2: N_i			- material number density  for mixed corium !!!
% 3: ZAID			- ZAID numbers of isotopes
% 4: rho_mix		- mixture density 
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% rho_model 	= 	'AtomcFrac'
%							'MassFrac' 
%							'Hull'
% A_model			'Simple'
%							'Complex'
% output_mode	'Mixed'
%							'Separate'							
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXAMPLE 1: mass_i = [30000, 20000]; mat_i = {'UO2' 'SSOX'}; T = 2500; rho_model = 'MassFrac'; A_model = 'Simple' 
% EXAMPLE 2: mass_i = [30000, 20000]; mat_i = {'UO2' 'SSOX'}; T = 2500; rho_model = 'MassFrac'; A_model='Complex';
% EXAMPLE 3:      mass_i=[ 578       1173.6        24891       1961.3        34174        24985       7887.1];
% 			     mat_i = 	 {'CRP'    'INC'    'SS'    'SSOX'    'UO2'    'ZR'    'ZRO2'}
%				Temp_i = 2500; rho_model = 'MassFrac'; A_model = 'Simple'; 
% CORIUM_MASS2N(mass_i,mat_i,T,rho_model, A_model)
% CORIUM_MASS2N(MASS_MATRIX,MATERIAL,Temp_i,rho_model, A_model)
% CORIUM_MASS2N(1000,{'SS'},TEMP,rho_model, A_model);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [N_corium, rho_corium, A_corium, A_i, wf_i, af_i, N_i, V_corium, n_i, n_corium, mat_i] = CORIUM_MASS2N(mass_i,mat_i,Temp_i,rho_model, A_model)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global N_A; %N_A = 0.602214179;  % [atoms/mole]Avogadro number


%% IF NON ZERO VECTOR IS AN INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if( ne(sum(mass_i),0.0) )

	[MATERIAL, MATERIAL_NO] = MAT_LIST(); %Available MATERIALs

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INITIAL TESTS 
		if(~(numel(mass_i)==numel(mat_i)))
			error('ERROR #1 (CORIUM_MASS2N): mass_i should have the same size as mat_i');
		end
		if(~or(numel(Temp_i)==numel(mat_i) , numel(Temp_i)==1))	 
			error('ERROR #2 (CORIUM_MASS2N): Temp_i can be scalar or vector with number of values equal to number of material inputs'); 
		end 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% REMOVAL OF ZERO  - to change
	dummy1 = mass_i(mass_i ~=0);
	dummy2 = mat_i(mass_i~=0);
	dummy3 = Temp_i;
	if(numel(Temp_i)>1) dummy3 = Temp_i(mass_i~=0); end	
	mass_i = dummy1; 
	mat_i = dummy2; 
	Temp_i = dummy3;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% [ZAID, COMP]=MAT_DATABASE(3,0)
	%Calculate mass fractions - mass information gives opportunity to find mass fractions
	mass 	= sum(mass_i);   										%Total mass of the mixture
	wf_i 	= mass_i./mass; 						 				%Calculates Mass fractions of MATerial in the corium mixture
	% x_mode = 'wf' by default - format of the input vector


	rho_corium = rho_mixMAT(wf_i,mat_i,Temp_i,'wf',rho_model,A_model);  %Density of the mixture
	[A_corium, A_i] = A_mixMAT(wf_i,mat_i,'wf',A_model); 			%Vector with Atomic Weight for the materials mat_i
	N_corium   = N_mix(rho_corium,A_corium);						%Atom density of the mixture

	af_i = wf2af(wf_i, A_i);
	N_i = af_i.*N_corium;
	V_corium = mass./rho_corium; %Total volume

	n_corium = V_corium.*N_corium./N_A; %total atoms
	n_i = V_corium.*N_i./N_A;					 %total atoms of i=th material


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% IN CASE OF ZERO INPUT VECTOR
 else
 
	disp('WARNING #1 (CORIUM_MASS2N): empty input vector was used')
	 N_corium 		= 0.0;
	 rho_corium 	= 0.0;
	 A_corium 		= 0.0;
	 A_i 			= zeros(numel(mat_i),1);
	 wf_i 			= zeros(numel(mat_i),1);
	 af_i 			= zeros(numel(mat_i),1);
	 N_i 			= zeros(numel(mat_i),1);
	 V_corium 		= zeros(numel(mat_i),1);
	 n_i 			= zeros(numel(mat_i),1);
	 n_corium 		= zeros(numel(mat_i),1);
	 mat_i			= mat_i;
 
end 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

