% Rev 09-08-2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function dedicated to calculate volume of the materials mixture for given number fo moles or atomic number density
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% n_input		- input mole number data
% mat_i			- material names vector
% Temp_i		- Temperature vector
% rho_model 	- density model: 'AtomcFrac' 'MassFrac' 'Hull'
% A_model		- atomic weight calculations: 'Complex' 'Simple' 
% input_mode	- input data are moles of maerials 'mole'   or aotmic number density 'dens' (NOT ACTIVE!)
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
%
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% input_mode =		'mole'	input n_i are numbr of moles
%					'dens'	input n_i are atomic number densities
%
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXAMPLE 1: 	  n_input = [10, 10, 10 ]; mat_i={'UO2' 'SS' 'SSOX'}; Temp_i = 2500; A_model = 'Complex'; rho_model = 'MassFrac' ; input_mode = 'mole';
% [V_mix, rho_mix, mass_i, mass_mix, N_i, N_mix, n_i, n_mix] = ...
% 						V_mixMAT(n_input ,mat_i, Temp_i,rho_model, A_model,input_mode)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [V_mix, rho_mix, mass_i, mass_mix, N_i, N_mix, n_i, n_mix, mat_i] = V_mixMAT(n_input,mat_i, Temp_i,rho_model, A_model,input_mode)

global N_A;
if(nargin <6)	input_mode = 'mole'; end %Default

%% IF NON ZERO VECTOR IS AN INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if( ne(sum(n_input),0.0) )

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INITIAL TESTS 
		if(~(numel(n_input)==numel(mat_i)))
			error('ERROR #1 (V_mixMAT): mass_i should have the same size as mat_i');
		end
		if(~or(numel(Temp_i)==numel(mat_i) , numel(Temp_i)==1))	 
			error('ERROR #2 (V_mixMAT): Temperature can be scalar or vector with number of values equal to number of material inputs'); 
		end 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% REMOVAL OF ZERO ENTRIES - the function MAT_LIST_REMOVAL may be used here but it will be too complex
	dummy1 = n_input(n_input ~=0);
	dummy2 = mat_i(n_input~=0);
	dummy3 = Temp_i;
	if(numel(Temp_i)>1) dummy3 = Temp_i(n_input~=0); end	
	n_input = dummy1; 
	mat_i = dummy2; 
	Temp_i = dummy3;



%% INPUT ARE MOLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(input_mode == 'mole')
		n_i = n_input;
		n_mix = sum(n_i);									% total number of moles in the mixture
		x_i = n_i./n_mix;									% atomic fractions
		[A_mix, A_i] = A_mixMAT(x_i,mat_i,'af',A_model); % atomic weight of the mixture and materials
		mass_i  = n_i.*A_i; 								% mass of the i-th material is number of moles times the atomic weight
		mass_mix = sum(mass_i);								% total mass of the mixture
		rho_mix = rho_mixMAT(x_i,mat_i,Temp_i,'af',rho_model,A_model);	%mixture density
		V_mix = mass_mix./rho_mix;													%mixture volume
	
		N_i = n_i.*N_A./V_mix;								% atomic number densities
		N_mix = sum(N_i);									% atomic number density of the mixture
				
			
			
			
%% INPUT ARE ATOMIC DENSITIES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	elseif(input_mode == 'dens')
		error('ERROR #3 (V_mixMAT): Not available mode');
	%N_i = n_input;
	%N_mix = sum(N_i);
	%x_i = N_i./N_mix;
	%[A_mixMAT, A_i] = A_mixMAT(x_i,mat_i,'af',A_model); % atomic weight of the mixture and materials
	%mass_i = 
	%rho_mix = rho_mixMAT(x_i,mat_i,Temp_i,'af',rho_model,A_model);	%mixture density
	%Probably not possible !!! without data about the volume or the mass
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	else
		error('ERROR #4 (V_mixMAT): No such mode available');
	end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% IN CASE OF ZERO INPUT VECTOR
else
 %V_mix, rho_mix, mass_i, mass_mix, N_i, N_mix, n_i, n_mix, mat_i
	 disp('WARNING #1 (V_mixMAT): empty input vector was used')
	 V_mix 			= 0.0;
	 rho_mix 		= 0.0;
	 mass_mix 		= 0.0;
	 N_mix 			= 0.0;
	 n_mix 			= 0.0;
	 N_i 			= zeros(numel(mat_i),1);
	 mass_i 		= zeros(numel(mat_i),1);
	 n_i 			= zeros(numel(mat_i),1);
	 mat_i			= mat_i;
 
end 


