% Rev 11-07-2017
% TO BE VERIFIED, 'Complex' does not work ?
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function dedicated to calculate mean atomic weight for the Material corresponding to the MELCOR output
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: x_i			- fractions atomic or weight
% 2: MATERIAL_i		- material names - cell type
% 3: x_mode		- type of the input - atomic or weight 
% 4: A_mode		- calculation mode
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: A_mixMAT		- mixture atomic mass/weight
% 2: A_i			- atomic weight of the isotope/Material
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% af = N(Isotope i)/sum(N(All isotopes))  - atom fractions vector
% wf = Mass(Isotope i)/sum(Mass(All isotopes))- weight fractions vector
% x_mode = 'af' or 'wf' - uses vector with atom fractions or weight fractions
%
% x_mode     = 'af'   or 1    -> atom fractions
%              'wf'   or 2    -> weight fractions DEFAULT
% A_mode         =  'Simple'  or 1   -> Simple values without complex Material calcs. DEFAULT
%                =  'Complex' or 2   -> Complex calculations of the Atomic Weights
% TYPE		  =  Default is BWR
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXAMPLE 1:   A_mixMAT([0.2,0.8],{'UO2', 'SS'},'af','Simple')
% A_mix = A_mixMAT(x_i,MATERIAL_i,x_mode,MATERIAL_mode)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [A_mixMAT, A_i] = A_mixMAT(x_i,MATERIAL_i,x_mode,A_mode)
% Available MATERIALs
[MATERIAL, MATERIAL_NO, TYPE] = MAT_LIST();                                   
    %Matrix size test
     if(numel(x_i)~=numel(MATERIAL_i))
         error('ERROR#1 (A_mixMAT): x_i has diffrent size than MATERIAL_i. Probably MATERIALS variable in not the CELL type -> use { } instead of [ ]');
     end
     % MATERIAL avability TEST
     for j=1:numel(MATERIAL_i)
			if(iscell(MATERIAL_i))
				dummy = sum(strcmp(MATERIAL_i{j}, MATERIAL));
			else
				dummy = sum((MATERIAL_i(j) == MATERIAL_NO));
			end
     	if( dummy==0 )
          error('ERROR#2 (A_mixMAT): There is no such Material available in MAT_LIST database!!!');
		end
     end

	if(nargin <= 3)
	   disp('INFO #1 (A_mixMAT): There is no 4th argument A_mode. DEFAULT: Complex ');
	   A_mode = 'Complex';
	end
	
	if(nargin <= 2)
       disp('INFO #2 (A_mixMAT): There is no 3rd & 4th argument x_mode & A_mode. DEFAULT: Mass Fractions & Complex');
	   x_mode = 'wf';
	   A_mode = 'Complex';
	end
	
	 disp(['INFO #3 (A_mixMAT): ', x_mode,'	',A_mode]);
	
	%if(nargin <5)
	%  TYPE = 'BWR';  %Deafult BWR type
	%end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    A_i = zeros(numel(MATERIAL_i),1)';
		
    for j=1:numel(MATERIAL_i)
			Aa = -1;	%Material presence test
		% Basic MELCOR materials - NEW DIVISION 2017
		%input as string
		if( iscell(MATERIAL_i)) 
		%	if( strcmp(MATERIAL_i{j}, MATERIAL(1)))
		%		if(strcmp(TYPE,'BWR'))     		Aa = MOLAR_BASE(15);   %B4C for BWRs %ro = rho_B4C(T);  
		%		elseif(strcmp(TYPE,'PWR')) 	Aa = MOLAR_BASE(16);  %AIC for PWRs 	%ro = rho_AIC(T);  
		%		else 	error('ERROR (A_mixMAT): No such reactor type avaiable'); 	end
         %   else
				for k=1:numel(MATERIAL)
					if strcmp(MATERIAL_i{j}, MATERIAL(k))
						%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						if( or(strcmp(A_mode,'Simple'),(A_mode == 1)))   	
								Aa = MOLAR_BASE(k);
						elseif( or(strcmp(A_mode,'Complex'),(A_mode == 2))) 
					%		if(k<8)
								[COMP,ZAID, NAME, UNIT, SUMC] = MAT_DATABASE(k, 0);  % READ MATERIAL ISOTOPES	
								%A_mix = A_mix(f_i,A_i,mode,norm)
								%COMP
								%ZAID
								Aa = A_mix(COMP,ZAID,'af','off');  	
						%	else
							%	Aa = MOLAR_BASE(k);
							%end
						else
							error('ERROR #3 (A_mixMAT): N/A mode');
						end					
						%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
					end 
				end	
			%end	 
			
		%input as numeric	
		elseif( isnumeric(MATERIAL_i) )
		%   	if( MATERIAL_i(j) == MATERIAL_NO(1))
		%		if(strcmp(TYPE,'BWR'))     		Aa = MOLAR_BASE(15);   %B4C for BWRs %ro = rho_B4C(T);  
		%		elseif(strcmp(TYPE,'PWR'))		Aa = MOLAR_BASE(16);  %AIC for PWRs 	%ro = rho_AIC(T);  
		%		else 	error('ERROR (A_mixMAT): No such reactor type avaiable');	end
        %    else
				for k=1:numel(MATERIAL_NO)
					if (MATERIAL_i(j) == MATERIAL_NO(k))
						%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						if( or(strcmp(A_mode,'Simple'),(A_mode == 1))) 
								Aa = MOLAR_BASE(k);
						elseif( or(strcmp(A_mode,'Complex'),(A_mode == 2)))  
							if(k<8)
								[COMP,ZAID, NAME, UNIT, SUMC] = MAT_DATABASE(k, 0);  % READ MATERIAL ISOTOPES	
								Aa = A_mix(COMP,ZAID,'af','off');  	
							else
								Aa = MOLAR_BASE(k);
							end
						else
							error('ERROR #4 (A_mixMAT): N/A mode');
						end
						%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
					end 
				end	
		%	end	 
		end   
		   
		if(Aa < 0) error('ERROR #5 (A_mixMAT):There is no such material - check input MATERIAL vector'); end
		
		A_i(j) = Aa;
    end


%A_i
%x_i



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% CALCULATIONS - ATOMIC WEIGHT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if(or((x_mode == 1),strcmp(x_mode,'af')))
        A_mixMAT = A_mix(x_i,A_i,'af');
    elseif(or((x_mode == 2),strcmp(x_mode,'wf')))
	 
        A_mixMAT = A_mix(x_i,A_i,'wf');		
    else
       error('ERROR #6 (A_mixMAT): There is no such a model for Atom. Weight calculation');
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	disp('INFO #4 (A_mixMAT) Results:');
	A_mixMAT
	A_i

	
