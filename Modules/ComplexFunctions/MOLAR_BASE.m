% Rev 19-07-2017, Mode 2 activated but check it
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Orginal version Czerwiec 2012 ; Modified in 04.2016
% Small Data base with atomic weights of the basic MELCOR materials
% Intended to use for simple calculations without any detailed isotopic composition
% Results: [g/mol] - Gram Atomic Weight or Molar Mass
% i - material number or string/cell/char 
% mode  - 1 or 'Default' 	 data base 
% mode  - 2 or  'XSDATA' 	XSDATA database
% Example: MOLAR_BASE(3)     - materials number, default database used
% Example: MOLAR_BASE('SS')  - material identification string or cell 	
% Example: MOLAR_BASE({'SS'})  - material identification string or cell
% Example: MOLAR_BASE({'SS'},'XSDATA'); 	
% Example: MOLAR_BASE({'SS'}, 2 );  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [M]=MOLAR_BASE(i,mode)

if(nargin<2)
   mode = 2; %DEFAULT mode ='Default'
end



% Load available MATERIALs 
[MATERIAL, MATERIAL_NO, TYPE] = MAT_LIST();
MAT_SIZE = numel(MATERIAL);  %number of materials

%Load XSDATA Atomic masses and ZAIDs
%load XSDATA.mat  %XSDATA = [ZAID, MASS] 


%For input with cell string
if(or(iscellstr(i),ischar(i)))
	  for j=1:numel(MATERIAL)
		  if( sum(strcmp(i, MATERIAL))==0 )
			  error('ERROR: There is no such Material available !!!');
		  end
	  end
	  i= sum(strcmp(i,MATERIAL).*MATERIAL_NO);  %Identification is the corresp. number of the material
	  %i = nonzeros(strcmp(i,MATERIAL));
%For input with integer

elseif(isnumeric(i))
	if (i>MAT_SIZE)
		error('There are no such material');
	elseif(i<=0)
		error('There are no such material');
	end	
else
 error('Wrong input: not integer and not cell')
end
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% MOLAR MASSES OF MATERIALS - DEFAULT VALUES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if( or((mode == 1), strcmp(mode,'Default')))
disp('INFO (MOLAR_BASE): Default mode');
	mBoron = 10.0129370*0.199+0.801*11.0093055;  %only boron natural mixture
	mB4C = 55.2547; 	%[5]
	%mAIC = 109.2338
	mAIC =  109.0790;	%US EPR Calculated with Amix	Ag-In-Cd 80/15/5 wt%  rho=10.17g/cc
	
	Mol(1:MAT_SIZE)=0.0;

% Basic MELCOR materials - NEW DIVISION 2017	
	Mol(1) =  mB4C;       		% 'CRP' Default 
	Mol(2) =  56.8875;    		% 'INC' Inconel 600 [5] rho = 8.47
	Mol(3) =  54.9425;    		% 'SS' -> 'SS304' [5]
	Mol(4) =  Mol(3)+2*16; 	% 'SSOX' 
	Mol(5) =  238+2*16;   		% 'UO2'
	Mol(6) =  90.8154;     	 	% 'ZR' - Zircaloy-4 [5]
	Mol(7) =  Mol(6)+2*16; 	% 'ZRO2'
	Mol(8) =  12.0;   				% 'GRP' pure carbon 12
	
% Additional materials
	Mol(9) =  238.0;		  		% 'U'
	Mol(10) = 270;        			% 'PUO2'
	Mol(11) = 270;        			% 'MOX', 
	Mol(12) = 270;    	  			% 'ACTO2'
	Mol(13) = (90+100+130+140)*0.25;     % 'FP'
	Mol(14) = Mol(13)+2*16.0; 		 		  % 'FPO2'	
	Mol(15) = mB4C;   			% 'B4C' 
	Mol(16) = mAIC;   			% 'AIC'
	Mol(17) = 55.8468;   		% 'FE'	%[5]	
	Mol(18) = 54.9425;   		% 'SS' as SS-304
	Mol(19) = 54.8457;   		% 'CS' based on [5] 
	Mol(20) = 15.2088;			%air 21% 79% O2 N2 %at
	Mol(21) = 13.923;  			%[5] Concerete, Ordinary no 91 PNNL
	Mol(22) = 111.923315;         % https://www.americanelements.com/zirconium-diboride-12045-64-6
	Mol(23) = 69.6182;%B2O3   % https://pl.wikipedia.org/wiki/Tritlenek_diboru
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% XSDATA BASED
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
elseif(or((mode == 2),strcmp(mode,'XSDATA')))
	%error('Still not available - not here');
	disp('INFO (MOLAR_BASE): XSDATA mode');
	
		[COMP,ZAID, NAME, UNIT, SUMC] = MAT_DATABASE(i); %default MELCOR materials
		Mol(i) =  A_mix(COMP,ZAID,'af','off') ;
	 
	
	 
else
 error('No such option');
end

M=Mol(i);


