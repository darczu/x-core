% Rev 11-07-2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function dedicated to calculate mean atomic weight for the material
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1:	f_i			- fraction of the material atomic or weight	
% 2: A_i 		   	- Atomic weight of the isotope/material; if A_i>1000 it is ZAID number!
% 3: mode		   	- type of input atomic fraction or weight fraction
% 4: norm   (!!!)  	- normalziation flag. When present it is actvie. No matters what is it. It can be even []; DEFAULT - WITH noramlization
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: A_mix		- mixture atomic mass/weight
%
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% af - Atomic Fraction  wf - Mass Fraction
% mode - 1 or 'af' atom fractions  - DEFAULT ->  A_mix(f_i,A_i)
%        		2 or 'wf' weight fractions 
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXAMPLE 1: 	   A = A_mix([1, 2],[16, 1],1) 				- be careful with molecules - it does normalizes
% EXAMPLE 2:	   A = A_mix([1, 2],[16, 1])   				- no mode - normaliation and atomic frac
% EXAMPLE 3:       A = A_mix([0.05, 0.95],[235, 238],2)   		- it  normalizes
% EXAMPLE 4:       A = A_mix([0.05, 5.95],[235, 238],2,'off')  	- normalization NOT applied
% function A_mix = A_mix(f_i,A_i,mode,normalization)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function A_mix = A_mix(f_i,A_i,mode,norm)
tol = 1e-6;  %tolerance - problems with floating point number in comparison mode
 %Load XSDATA Atomic masses and ZAIDs - When it is necesarry
if(sum(A_i>=1000)>0) 
	load XSDATA.mat;  %XSDATA = [ZAID, MASS] 
end

% Changes ZAIDs to corresponding A
for i=1:numel(A_i)
	if(A_i(i)>=1000)  %then A_i is ZAID number
		A_i(i)=sum(XSDATA(:,2).*(A_i(i) == XSDATA(:,1)));
	end
end



% A_i nonzero test
if(sum(A_i == 0)>0) 
	disp('One of the A_i input numbers is zero or ZAID number is not in the database');
	disp('Check if the ZAID number is in the XSDATA file. For example oxygen 8000 is no available - it should be 8016. Code calculates zero molar mass');
	error(['ERROR #1 (A_mix): Molar mass cannot be zero. You have netered zero or WRONG ZAID NUMBERs']); 
end


if (nargin<3)
	mode = 1;
	norm = 1;
	disp('INFO #1 (A_mix): ATOMIC FRACTION ON (DEFAULT)');
end
if (nargin<4) 
	%f_i = normalization(f_i);
	norm = 1;
	disp('INFO #2 (A_mix): NORMALIZATION ON (DEFAULT)');
end


if( sum(norm == 1) || strcmp(norm,'1') || strcmp(norm,'on')  )
	 f_i = normalization(f_i);
	 disp('INFO #3 (A_mix): NORMALIZATION ON');
else
	 disp('INFO #4 (A_mix): NORMALIZATION OFF'); 
end 

    % Atomic Fraction Mode
	if(or(strcmp(mode,'af'),(mode==1)))
		if( abs(sum(f_i) - 1.0) > tol)
			disp(['INFO #5 (A_mix): WARNING: Weight fractions does not add to one ']);
		end
			disp('INFO #6 (A_mix): ATOMIC FRACTION ON');	 
		A_mix = sum(f_i.*A_i);

	% Mass Fraction Mode	
	elseif(or(strcmp(mode,'wf'),(mode==2)))

		if(abs(sum(f_i) - 1.0) > tol)
			error('ERROR #2 (A_mix): NOT POSSIBLE TO USE MASS FRACTION WHEN NO NORMALIZATION');
		end
		disp('INFO #7 (A_mix): MASS FRACTION');
		
		A_mix = sum(f_i./A_i);
		A_mix = 1./A_mix;
else
    error('ERROR #3 (A_mix): No such a mode avaialble only'); 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%