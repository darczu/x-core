%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SCRIPT DEDICATED PERFORM SAMPLING - CREATION OF PARAM_SELECT VARIABLES
% DEVELOPED BY PIOTR DARNOWSKI FOR NARSIS PROJECT 2021 - ALL RIGHTS RESEVERD - SEE LICENSE PIOTR.DARNOWSKI@PW.EDU.PL, WARSAW UNIVERSITY OF TECHNOLOGY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SIMPLE RANDOM SAMPLING
if strcmp(sampling_method,'SRS')
    % VERSION 2 FOR ALL VARIABLES
    for i = 1:nParams
        % Continous variable sampling
        if strcmp(param(i).type,'Continous')
            param_select{i,1} = random(pd_select(i),N,1);  
        end
        %Discrete variable sampling
        if strcmp(param(i).type,'Discrete')
            param_select{i,1} = ceil(random(pd_select(i),N,1));  
        end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LATIN HYPER CUBE SAMPLING
if strcmp(sampling_method,'LHS')
    for i = 1:nParams
        % Continous variable sampling
        if strcmp(param(i).type,'Continous')
            lhsSample = lhsdesign(N,1);              %matlab LHS for uniform dist
            sample = icdf(pd_select(i),lhsSample);   %inverse CDF for PDF
            param_select{i,1} = sample;  
        end
        %Discrete variable sampling
        if strcmp(param(i).type,'Discrete')
            lhsSample = lhsdesign(N,1);             %matlab LHS for uniform dist
            sample = icdf(pd_select(i),lhsSample);  %inverse CDF for PDF
            param_select{i,1} = ceil(sample);   % only difference, ceil to have integer value, variable in [0,1] -> 1  etc.
        end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LHS EXAMPLE - CONTINOUS VARIABLE
%X = lhsdesign(100,1); 
%mu = 0;
%sigma = 1;
%pd = makedist('Normal','mu',mu,'sigma',sigma);
%sample = icdf(pd,X)
%histogram(sample)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% WRITE BEST ESTIMATE VALUE TO VARIABLE - NOT RELATED TO SAMPLING
for i = 1:nParams
        
    param_select_best{i,1} = param(i).BEST;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EQUAL TYPE DISTIRBUTION, IF SOME PARAMETER HAS THE SAME VALUES AS OTHER. IT IS NOT SAMPLED
    for i = 1:nParams
        if strcmp(param(i).type,'Equal')
            % IT IS NOT SAMPLING
            %param_select{i,1} =param_select{ param(i).pdtype,1} ;
            param_select{i,1} =param_select{ param(i).pdparam,1} ;
        end
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OLD SOLUTION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DISCRETE VARIABLES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LEGACY CODE
%{
% 
% % Parameter 12 - Uniform Distribution discerete for oxidstion coeffcient
% %param0=unidrnd(5,[N 1]);		% Discerete Uniform PDF ALTERNATIVE  r = randi(10,5)
% 
% i = 12; %Changed from Parameter 0 -> 12
% %pd_discrete{i} = unidrnd(Nalt12,N,1);  %Uniform alternative ways
% pd_discrete{i} =  datasample(s_discrete,alter12, N,'Weights',probs12);
% param{i,1} = pd_discrete{i};
% param_select{i,1} = param{i,1}'   ;
% 
% %  param_select{i,1} = random(pd_select(i),N,1)
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Parameter 13 - Fuel failure criterion function Fuel Rod Collapse Model Logic
% i = 13;
% pd_discrete{i} =  datasample(s_discrete,alter13, N,'Weights',probs13);
% param{i,1} = pd_discrete{i};
% param_select{i,1} = param{i,1}'   ;
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % CONTINOUS VARIABLES
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% % Parameter 1 		% SC1131	SC1131(2)   TZXMX
% % Molten Material Holdup Parameters	Zr Melt Release Temperature/ Zr Melt Breakout Temp	
% i = 1;
% %param{i,1} = random(pd(i),N,1); %Gauntt
% %param{i,2} = random(pd1(i),N,1) ;  %SOARCA
% 
% param_select{i,1} = random(pd_select(i),N,1) ;  %SOARCA
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% % Parameter 2		% SC1132 	SC1132(1)  TRDFAI  
% % Core (Fuel) Component Failure Parameters	Fuel Rod Failure temp	
% i = 2;
% %param{i,1} = random(pd(i),N,1);   %Gauntt
% %param{i,2} = random(pd1(i),N,1) ; %SOARCA
% 
% param_select{i,1} = random(pd_select(i),N,1) ;  
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% % Parameter 2	
% i = 14;
% % For INT model it should be equal to melting temperature and SC1132(1)
% param_select{i,1} = param_select{2,1} % same as i=2
% 
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% % Parameter 3		COR_CMT FUOZR
% % Secondary Material Transport Parameters Secondary UO2 Content, dissolved UO2	COR00007	
% i = 3;
% %param{i,1} = random(pd(i),N,1);
% %param{i,2} = random(tpd(i),N,1) ;
% 
% param_select{i,1} = random(pd_select(i),N,1) ;  
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% % Parameter 4 		COR_CHT(1)
% %4	Candling Heat Transfer Coefficients	Zr Freezing Coeff	COR00005		
% i = 4;
% %param{i,1} = random(pd(i),N,1);
% %param{i,2} = random(tpd(i),N,1) ;
% param_select{i,1} = random(pd_select(i),N,1) ;  
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% % Parameter 5		COR_EDR	DHYPD 
% %5	Core-Region Particulate Debris Diameter		Pozamieniane	CORijj04
% i = 5;
% %param{i,1} = random(pd(i),N,1);
% %param{i,2} = random(tpd(i),N,1) ;
% param_select{i,1} = random(pd_select(i),N,1) ;  %SOARCA
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% % Parameter 6			COR_EDR	DHYPD 
% %6	Lower Plenum Debris Diamter	Pozamieniane	CORZjj01	COR_EDR	DHYPD 
% i = 6;
% %param{i,1} = random(pd(i),N,1);
% %param{i,2} = random(tpd(i),N,1) ;
% %param_select{i,1} = param{i,2}   ;
% param_select{i,1} = random(pd_select(i),N,1) ;  %SOARCA
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% % Parameter 7		PORDP 
% %7	Debris porosity (PORDP)	CORZjj01		PORDP 
% i = 7;
% %param{i,1} = random(pd(i),N,1);
% %param{i,2} = random(tpd(i),N,1) ;
% %param_select{i,1} = param{i,2}   ;
% param_select{i,1} = random(pd_select(i),N,1) ;  %SOARCA
% %% ================================================
% % Parameter 8
% % Radial Radiation Exchange Factors FCELR for PWR
% i = 8;
% %param{i,1} = random(pd(i),N,1);
% %param{i,2} = random(tpd(i),N,1) ;
% %param_select{i,1} = param{i,2}   ;
% 
% 
% param_select{i,1} = random(pd_select(i),N,1) ;  %SOARCA
% %% ================================================
% % Paameter 9
% % Radial Radiation Exchange Factors FCELR for FPT-1
% i = 9;
% %param{i,1} = random(pd(i),N,1);
% %param{i,2} = random(tpd(i),N,1) ;
% param_select{i,1} = random(pd_select(i),N,1) ;  %SOARCA
% 
% %% ================================================
% % Parameter 10
% %  Axial Radiation Exchange Factors FCELA for both models
% i = 10;
% %param{i,1} = random(pd(i),N,1);
% %param{i,2} = random(tpd(i),N,1) ;
% param_select{i,1} = random(pd_select(i),N,1) ;  %SOARCA
% %% ================================================
% % Parameter 11
% %Figure 23  In-Vessel Falling Debris HTC HDBH2O
% i = 11;
% %param{i,1} = random(pd(i),N,1);
% %param{i,2} = random(pd1(i),N,1) ;
% %param{i,3} = random(tpd1(i),N,1) ;
% %param_select{i,1} = param{i,3}   ;
% param_select{i,1} = random(pd_select(i),N,1) ;  %SOARCA
% 
%}