%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function which creates pdf type object with input being pd type, paramters types and values of paramers
% Creates Discrete distribution
% CREATED BY PIOTR DARNOWSKI, WUT, 2021
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXAMPLE
%pd =  create_discrete_dist('PiecewiseLinear',[1 2 3] ,[0.1 0.2 0.7]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function pd_select = create_discrete_dist(pdtype,pdparam,pdvalue)

if sum(pdvalue) ~= 1.0
    error('Probabilities in pdvalue parameter does not sum to one !!!');
end    


if ~strcmp('PiecewiseLinear',pdtype)
    error('if discrete pdf modelled with create_discrete_dist this function it has to be pdtype = PiecewiseLinear ');
end
%function pd_select = create_dist(pdtype,pdparam,pdvalue,istruncate,trlimits)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if iscell(pdparam) 
    
    for i = 1:numel(pdparam)
        if ischar(pdparam{i})
            pdparam{i} = str2num(pdparam{i})
        end
    end

    pdparam = cell2mat(pdparam);
end

if iscell(pdvalue) 
    pdvalue2 = cell2mat(pdvalue);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
x = pdparam;
x = [0, x];

Fx = pdvalue;
CFx = cumsum(Fx);
CFx = [0, CFx];

%CFx(end) = 1.000;  %necesarry due to numerical roundoff errors

if CFx(end) >= 1.0
    CFx(end) = floor(CFx(end));
else
    CFx(end) = ceil(CFx(end));
end


%format long

%CFx(1)
%CFx(end)

%CFx(1) == 0
%CFx(end) == 1

%class(CFx)
%class(x)
%pd_params = {'x'  'Fx'}
%pd_params1 = [num2cell(x'),num2cell(CFx')]

%pd_params2 = [pd_params; pd_params1 ]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%pd = makedist(pdtype,  pd_params2{:});

pd_select = makedist(pdtype, 'x', x  ,'Fx', CFx);