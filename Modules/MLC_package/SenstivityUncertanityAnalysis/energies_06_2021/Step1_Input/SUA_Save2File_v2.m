%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Developed by Piotr Darnowski, Warsaw University of Technology
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create input file for the code
% Save to file Loop 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if flag_save == 1
	% CREATE FOLDERS
		mkdir(case_name);
		for i=0:N  							% 0 becouse folder with BE case
			folder_case_name = [case_name,'/',folder_name,num2str(i)];
			mkdir(['./',folder_case_name]);
		end

	% COPY XML FILE
		if flag_save == 1
			copyfile(['./', xml_file], [case_name,'/', xml_file],'f');
		end

		

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%for j=1:nCases
for j=0:nCases  %0 is best estimate
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	variablefile_location = [case_name,'/',folder_name,num2str(j),'/',variables_file];
	fid = fopen(variablefile_location, 'wt');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% SAVE TO FILE WITH VARIABLES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PRE
	fprintf(fid,'! =======================================================\n');
	fprintf(fid,'! MELCOR UNCERTANITY VARIABLES FILE\n');
	fprintf(fid,'! TOOL DEVELOPED BY PIOTR DARNOWSKI FOR NARSIS PROJECT 2021 \n');
	fprintf(fid,'! VARIABLE INPUT FUNCTIONALITY\n');
	fprintf(fid,'! INPUT VARIABLES:  {{VARIABLE_NAME = VALUE}}\n');
	fprintf(fid,'! =======================================================\n');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CONTINOUS
fprintf(fid,'! =======================================================\n');
fprintf(fid,'! CONTINOUS VARIABLES\n');
fprintf(fid,'! =======================================================\n');
	for i=1:nParams
		if strcmp(param(i).type,'Continous')  
			fprintf(fid,'! Parameter %s	\t',  string(i));
			fprintf(fid,'! Field %s\t\t',  string(param(i).fullname));
			fprintf(fid,'! Info %s   \n', string(param(i).descrip));
		end
	end	
	fprintf(fid,'! =======================================================\n');
	for i=1:nParams
			if strcmp(param(i).type,'Continous')
				if j > 0
					fprintf(fid, '{{{%s=%5.5f}}}\n',string(param(i).fullname),		param_select{i,1}(j));
				elseif j == 0
					fprintf(fid, '{{{%s=%5.5f}}}\n',string(param(i).fullname),		param_select_best{i,1});	
				end
			end
	end 
	fprintf(fid,'! =======================================================\n');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DISCRETE
fprintf(fid,'! =======================================================\n');
fprintf(fid,'! DISCRETE VARIABLES\n');
fprintf(fid,'! =======================================================\n');
for i=1:nParams
	% MROE COMPLEX. BECOUSE IT CAN BE BOTH STRING, CHAR AND NUMERIC
	if strcmp(param(i).type,'Discrete')  
		fprintf(fid,'! Parameter %s \n',  string(i));
		fprintf(fid,'! Field %s ',  string(param(i).fullname));
		fprintf(fid,'\n! %s\n', string(param(i).descrip));
		if j > 0
			k = param_select{i,1}(j); 					%sampled number of the option
		elseif j == 0
			k = param_select_best{i,1}; 					%sampled number of the option
		end
		fprintf(fid,'! Sampled Option:  %s   %s \n', string(k), string(param(i).add_data(k).name ) );
		dummy = param(i).add_data(k).value;
		%if numerical value compose if char just string
		if isnumeric(dummy)
			dummy = compose("%.5f", dummy);		
		else
			dummy = string(dummy);
		end
		nFields = numel(dummy); %number of values  1....6
		dummy_name = string(param(i).fullname);
		for m = 1:nFields  % loop across all values
			fprintf(fid, '{{{%s=%s}}}\n',dummy_name(m), dummy(m));
		end	
		fprintf(fid,'! =======================================================\n');
	end
end 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIAL
fprintf(fid,'! =======================================================\n');
fprintf(fid,'! SPECIAL VARIABLES\n');
fprintf(fid,'! =======================================================\n');
for i=1:nParams
	if strcmp(param(i).type,'Equal')  
		fprintf(fid,'! Parameter %s \n',  string(i));
		fprintf(fid,'! Field %s ',  string(param(i).fullname));
		fprintf(fid,'\n! %s\n', string(param(i).descrip));
		if j > 0
			fprintf(fid, '{{{%s=%5.5f}}}\n',string(param(i).fullname),		param_select{i,1}(j));
		elseif j == 0
			fprintf(fid, '{{{%s=%5.5f}}}\n',string(param(i).fullname),		param_select_best{i,1});
		end
		fprintf(fid,'! =======================================================\n');
	end
end 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf(fid,'! \n');
fclose(fid);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end %nCases
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end  %ENDF SAVE


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SAVE SUMMARY FILE  - COLUMNWISE FILES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i =1:N
	row_numbers{i} =  num2str(i);
end


name_numbers = vertcat(param(:).num);

%if flag_read_dist == 1
	%for k = 1:numel(name_numbers)
		if sum(isnan(name_numbers))
			name_numbers = name_numbers(~isnan(name_numbers));
		%elseif 	isempty(name_numbers)			
		%	name_numbers = name_numbers(~isempty(name_numbers));
		elseif sum(ismissing(name_numbers))
			name_numbers = name_numbers(~ismissing(name_numbers));	
		end
	%end	
%end	

name ={param(:).name}';
name = name (name_numbers);

%Convert strings to char arrays in cells
for k=1:numel(name)
	if ~ischar(name{k})
		name{k} = char(name{k})
	end
end


   % names of variables
   %names=num2cell([1:1:numel(name)]')
   %names_data = [names name']


   	for i=1:numel(param_select)
	   param_data1{i} = param_select{i}';
	   param_data_best1{i} = param_select_best{i}';
   	end
	
	   param_data =	   	param_data1(name_numbers);
	   param_data_best = param_data_best1(name_numbers);

   %param_data =vertcat(param_data1{:});
%param_data_best =vertcat(param_data_best1{:});

   param_data =vertcat(param_data{:});
   param_data_best = vertcat(param_data_best{:});
   

   param_data = param_data';
   param_data_best = param_data_best';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   % TABLES
   T1 = array2table(param_data, 'VariableNames', name);
   T1.Row=row_numbers'; 
   names_data = [num2cell(name_numbers) name];
   T2 = array2table(param_data','VariableNames', row_numbers');
   T2.Row=name; %Table names
   Tbest = array2table(param_data_best, 'VariableNames', name);
   Tbest.Row = {'0'};
%T2.Properties.DimensionNames;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% WRITE TO DATA FILE
if flag_save == 1
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	writetable(T1,['./', case_name,'/data_cols.dat'],'WriteRowNames',true);   % 'Delimiter','\t');
	writetable(T1,['./', case_name,'/data_cols.csv'],'WriteRowNames',true);
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	writetable(Tbest,['./', case_name,'/best_est_data_cols.dat'],'WriteRowNames',true);   % 'Delimiter','\t');
	writetable(Tbest,['./', case_name,'/best_est_data_cols.csv'],'WriteRowNames',true);
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% ALTERNATIVE ROWWISE FILES 
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	writetable(T2,['./', case_name,'/data_rows.dat'],'WriteRowNames',true);  % 'Delimiter','\t');
	writetable(T2,['./', case_name,'/data_rows.csv'],'WriteRowNames',true);  
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end  %ENDF WRITE 

   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% WRITE TO DATA FILE
if flag_save == 1

   writecell(names_data,['./', case_name,'/var_names.dat']);
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   % SAVE INFO FILE
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
   fid00 = fopen([case_name,'/','case_info_file.txt'], 'wt');
   fprintf(fid00,'! =======================================================\n');
   fprintf(fid00,'! %s\n', datetime);
   fprintf(fid00,'! =======================================================\n');
   fprintf(fid00,'\n');
   fprintf(fid00,' Case Name (folder) case_name 		%s \n',case_name  );  
   fprintf(fid00,' Flag - flag_type			 		%s \n',flag_type  );
   fprintf(fid00,' Input file source input_folder  	%s \n',input_folder );
   fprintf(fid00,' Input file - input_file				%s \n',input_file );
   fprintf(fid00,' Distributions	0 m-file 1 XML-file	 		%d \n', flag_read_dist  );
   fprintf(fid00,' Folder with Best Estiamte Case  	%s \n',Best_Estimate_folder   );
   fprintf(fid00,' Variables_file  					%s \n',variables_file  );
   fprintf(fid00,' Sampling method 						%s \n',sampling_method);
   fprintf(fid00,' N Sample Size 						%d \n',N);
   fprintf(fid00,' Seed 								%d \n',Seed);
   %fprintf(fid00,' Discrete Seed 								%d \n',s_discrete.Seed);
   %fprintf(fid00,' Discrete Seed 								%s \n',s_discrete.NormalTransform);
   %fprintf(fid00,' Discrete Seed 								%s \n',s_discrete.Type);
   fprintf(fid00,'! =======================================================\n');
   fprintf(fid00,'Additional files: \n');
   fprintf(fid00,'\n');
   fprintf(fid00,'! =======================================================\n');
   fclose(fid00);
end  %ENDF WRITE 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SHOW
%T1
T2
T1
T1(1:2,:)
Tbest
T1.Properties.VariableNames
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



