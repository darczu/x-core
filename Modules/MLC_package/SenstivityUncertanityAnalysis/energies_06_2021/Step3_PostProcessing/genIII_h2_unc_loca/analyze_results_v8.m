%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Developed by Piotr Darnowski, Warsaw University of Technology
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%https://uk.mathworks.com/matlabcentral/answers/498767-troubles-assigning-colorbar-values-in-a-bivariate-histogram-plot
% https://uk.mathworks.com/help/matlab/matlab_prog/create-a-table.html
% https://uk.mathworks.com/help/stats/gplotmatrix.html
% https://uk.mathworks.com/help/matlab/ref/plotmatrix.html
% https://uk.mathworks.com/help/matlab/ref/categorical.html
% SHADED AREA PLOTS:
% https://uk.mathworks.com/matlabcentral/fileexchange/58262-shaded-area-error-bar-plot
% https://uk.mathworks.com/matlabcentral/fileexchange/69203-shaded-plots-and-statistical-distribution-visualizations

%https://uk.mathworks.com/matlabcentral/answers/498767-troubles-assigning-colorbar-values-in-a-bivariate-histogram-plot
%https://uk.mathworks.com/matlabcentral/answers/83519-2d-scatter-plot-with-z-value-in-color
% SCRIPT FOR UNCERTANITY PLOTTING
%legend([p3(1)],'M2.2.18')
%legend([p1(1)],'Success')
%legend([p2(1)],'Partial')
% legend([p1 p3],{'First','Third'})
% legend({'cos(x)','cos(2x)','cos(3x)','cos(4x)'},'Location','northwest','NumColumns',2)


clear; clc; close all; format short g;
default_color = lines(100);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
flag_plot = 1;  %Plot figure of merit
flag_save = 1;  %Save figure of merit
flag_reduced = 1;
flag_type = 'PWR'
%flag_type = 'FPT'

%T=readtable('data_cols.csv')
%Tbest=readtable('best_est_data_cols.csv')

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
%flag_read0 = 0; %Read case0
%set(0, 'DefaulttextInterpreter', 'none'); 
%mkdir([folder{:},'\',folder_name,'\',results_folder]);
%print('-dtiff',[folder{:},'\',folder_name,'\',results_folder,'\', name, '.tiff']);
%savefig([folder{:},'\',folder_name,'\',results_folder,'\', name, '.fig']);
% x = rand(100,1)*50;
%catnames = ["small","medium","large"];
%binnedData = discretize(x,[0 15 35 50],'categorical',catnames);
% figure('Renderer', 'painters', 'Position', [10 10 900 600])
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
% INPUT ANALYSIS
if flag_type == 'PWR'
%root_folder = 'C:\CloudStation\ROOT\NARSIS-GenIII\NARSIS_UNC_PWR_010_INT_Results_M2218\'
%root_folder1  = 'C:\CloudStation\ROOT\NARSIS-GenIII\EDFresults_NARSIS_UNC_PWR_014_INT_Results_M2218\'
root_folder1  = 'C:\CloudStation\ROOT\NARSIS-GenIII\EDFresults_NARSIS_UNC_PWR_015_INT_Results_M2218\'
root_folder = root_folder1;
edffile = 'EDF_5_MH2CR'; colH2 = 2;
partial_success = 2500;
nCases = 400;
str = 'DEBRIS EJECTION THROUGH FAILED VESSEL ABOUT TO START'
file_diag = 'MELPWR.DIA';
%run find_failed_runs_v7.m
run find_failed_runs_v8_2.m
end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
if flag_type == 'FPT'
root_folder2 = 'C:\CloudStation\ROOT\NARSIS-FPT1\M2218\EDFfiles_NARSIS_UNC_FPT_E7_INT_Results_M2218\'
root_folder = root_folder2;
partial_success = 16000;
nCases = 50;
edffile = 'EDF_5_MH2CR'; colH2 = 2;
%edffile = 'EDF_H2';  colH2 = 3;
str = 'USER INPUT TEND'
file_diag = 'MELFPT1.DIA';
%run find_failed_runs_v7.m

run find_failed_runs_v8_2.m

end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		

% Read file with names % Problems with names
Tbl_names 	= readtable([root_folder, 'var_names.dat']);
Tbl_variables  =  Tbl_names(:,2)
variables =  table2cell( Tbl_variables);

Tbl_calc 	= readtable([root_folder, 'data_cols.dat']);
Tbl_calc.Properties.VariableNames(2:end) = variables;
Tbl_best 	= readtable([root_folder, 'best_est_data_cols.dat']);
Tbl_best.Properties.VariableNames(2:end) = variables;

% Table with best and normal
Tbl_calc_all    = [Tbl_best; Tbl_calc];
Tbl_calc_all  = Tbl_calc_all(1:mCases,:)
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Table with results and parameters
Tbl_all 	  = [Tbl_calc_all, T_all];
Tbl_partial = Tbl_all(isPartial==1,:);
Tbl_success = Tbl_all(isSuccess==1,:);
Tbl_failed  = Tbl_all(isSuccess==0,:);
Tbl_best 	= Tbl_all(isBest == 1,:);

writetable(Tbl_all,[root_folder, 'Tbl_all.dat']); 
Tbl_all.Properties.VariableNames

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
%% PLOT OTHER CASES
if flag_plot == 1

T_MAX = max(Tbl_all.("TimeEnd"))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
% INTERPOLATE DATA
T_MAX2 = ceil(T_MAX/1000)*1000;
t_interp = linspace(0,T_MAX2,5000)';

for i=1:nCases	
	x{i} = data{i}(:,1);
	y{i} = data{i}(:,2);
end	
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
% SHADED PLOT
y_interp = [];
for i=1:nCases	
	y_interp1 = interp1(x{i},y{i},t_interp);
	y_interp2 = fillmissing(y_interp1,'previous');
	y_interp = [y_interp, y_interp2];
end	

y_interp_norm = mean(y_interp(end,:))

figure;
hold on;
plot(t_interp, y_interp./y_interp_norm);
%plot(t_interp, y_interp);


figure;
hold on;
p1 = [95 50 ]
p2 = [68.27, 95.45, 99.73]
%plot_distribution(t_interp',y_interp');
%plot_distribution_prctile(t_interp',(y_interp)','Prctile',p2);
plot_distribution_prctile(t_interp',(y_interp./y_interp_norm)','Prctile',p2);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
% PLOT BEST ESTIMATE AND EXP DATA
if flag_type == 'FPT'
h = figure; hold on;
% PLOT BEST CASE
x0 = data0{1}(:,1); y0 = data0{1}(:,2);
p1 = plot_fom(h,x0,y0,'-','k',2);  % isSuccess(1));  

load('C:\CloudStation\NARSIS\MODELE_AND_RESEARCH\SCRIPTS\MATLAB_XCore\x-core\Modules\MLC_package\PostProcessing\MATLAB_MELCOR_RESULTS_VISUALIZATION\fpt1\EXP_DATA6.mat')

x1 = EXP_H2_TOT(:,1); y1 = EXP_H2_TOT(:,2);
x2 = EXP_H2_TOT_SNL2(:,1); y2 = EXP_H2_TOT_SNL2(:,2);
p2 = plot_fom(h,x1,y1,'none',default_color(1,:),0.5,'o',5);  % isSuccess(1));  
p3 = plot_fom(h,x2,y2,'none',default_color(2,:),0.5,'<',5);  % isSuccess(1));  
legend('M2.2.18','EXP#1','EXP#2','Location','NorthWest');
ax = fig.CurrentAxes; 
title(ax,'Hydrogen Generation');  xlabel(ax,'Time, [s]'); ylabel(ax,'Mass, [kg]'); xlim(ax,[0 inf]);
end




%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
fig1 = figure; ax1 = fig1.CurrentAxes; hold on;
for i=1:nCases	
	x{i} = data{i}(:,1); y{i} = data{i}(:,2);
end	
for i=1:nCases	
	if isSuccess(i+1) == 1
		p1(i) = plot_fom(1,x{i},y{i},'-',[0.5 0.5 0.5],1);	
	end	
end
for i=1:nCases		
	if (isSuccess(i+1) == 0) & (isPartial(i+1) == 1)
		p1(i) = plot_fom(1,x{i},y{i},'-','r',1);	
		examplePartial = i;
	end	
end		
% PLOT BEST CASE
x0 = data0{1}(:,1); y0 = data0{1}(:,2);
p3 = plot_fom(1,x0,y0,'-','black',2,'none');  % isSuccess(1));  	
ax1 = fig1.CurrentAxes;
% OTHER OPTIONS	
xlim(ax1,[0 inf]);  
ylim(ax1,[0 inf]);
title(ax1,'Hydrogen Generation'); 
xlabel(ax1,'Time, [s]'); 
ylabel(ax1,'Mass, [kg]');
legend([p3(1), p1(1), p1(examplePartial)],'M2.2.18','Success','Partial')


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
j = 100; 
fig(j) = figure(j); hold off; ax(j)=gca;
s = scatterhistogram(Tbl_success,'FoM','TimeEnd','GroupVariable','IRODDAMAGE','HistogramDisplayStyle','smooth','LineStyle','-')
s.LegendVisible = 'on';
s.LegendTitle = 'IRODDAMAGE';

j=j+1; fig(j) = figure(j); 
hold off; ax(j)=gca;
s = scatterhistogram(Tbl_success,'FoM','TimeEnd','GroupVariable','SC1001','HistogramDisplayStyle','smooth','LineStyle','-')
s.LegendVisible = 'on';
s.LegendTitle = 'SC1001';

j=j+1; fig(j) = figure(j);
plotmatrix(A_success(:,[4 5]))

j=j+1; fig(j) = figure(j);
histogram(A_success(:,4))






%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	fig15 = figure;
%X(:,i) = data{i}(:,1);
%shadedErrorBar(x0, y0,{@mean,@std},'lineprops','-b','transparent',1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
fig2 = figure;
	boxplot(FOM_partial); box on; grid on;
	ax2 = gca;	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  	
fig3 = figure;	
	boxchart(FOM_partial); ylim([0 inf]); grid on; box on;
	ax3 = gca;	
	xlabel('Partial')
	hold on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
fig4 = figure;
	tiledlayout(1,2);
	nexttile;
	boxchart(FOM,'Notch','on'); ylim([0 inf]);  grid on; box on;
	xlabel('All')
	nexttile;
	boxchart(FOM_partial,'Notch','on'); ylim([0 inf]); grid on; box on;
	hold on; box on; grid on;
	xlabel('Partial')
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  

fig6 = figure;
tcl=tiledlayout(1,6);
ax1.Parent=tcl;
ax1.Layout.TileSpan = [1, 5];
ax3.Parent=tcl;
ax3.Layout.Tile=6;
hold on; box on; grid on;
fig7 = figure;
ax7 = copyobj(ax1,fig7);
hold on; box on; grid on;
fig8 = figure;
ax8 = copyobj(ax3,fig8);
hold on; box on; grid on;


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
figure;
x1 = ones(1,numel(FOM));
y1= FOM;
x2 = 2*ones(1,numel(FOM_partial));
y2 = FOM_partial;

swarmchart(x1,y1,5); hold on; box on; grid on;
swarmchart(x2,y2,5); hold on; box on; grid on;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot ECDF
fig_ecdf = figure;
plot_ecdf(fig_ecdf,FOM_partial,'-',default_color(1,:))
xlabel('H2 Mass, [kg]'); 



%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
Tbl_all.Properties.VariableNames

figALL1 =figure;
Aplot=Tbl_success{:,2:17};
%plotmatrix(Aplot)
title('Comparison of all parameters')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figALL2 = figure;
tiledlayout(4,4); 
%X = Tbl_success.("FoM");
Y = Tbl_success.("FoM");
%fig.Position =   [90, 90, 1600, 800];
figALL2.WindowState = 'maximized';


for i=2:17
	nexttile;
	%Y=Tbl_success{:,i};
	X=Tbl_success{:,i};
	label = Tbl_success.Properties.VariableNames(i);

	if   ~sum(strcmp(label,{'SC1001','IRODDAMAGE'}))
		% https://uk.mathworks.com/matlabcentral/fileexchange/76467-trendline
		trends=trendline(X,Y,'k'); 
	end
	plot(X,Y,'.','Color',[0, 0.4470, 0.7410])	%plotmatrix(X,Y); 
	box on; grid on; 

	%title(num2str(i-1))
	ylabel('FoM'); 
	%xlabel(Tbl_success.Properties.VariableNames(i))
	xlabel(['#', num2str(i-1),' ',Tbl_success.Properties.VariableNames{i}])
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If pval(a,b) is small (less than 0.05), then the correlation rho(a,b) is significantly different from zero.
% Because the p-value is less than the significance level of 0.05, it indicates rejection of the hypothesis that no correlation exists between the two columns.
figBar1 = figure;
h1=  subplot(1,6,1) 


%X = Tbl_success.("FoM");
Y = Tbl_success.("FoM");
%Y = Tbl_success{:,2:17};
X = Tbl_success{:,2:17};
label = Tbl_success.Properties.VariableNames(2:17);

[rho1,pval1] = corr(X,Y,'Type','Pearson');
[rho2,pval2] = corr(X,Y,'Type','Spearman');
[rho3,pval3] = corr(X,Y,'Type','Kendall');

rho = [rho1, rho2, rho3];
pval = [pval1, pval2, pval3];

(pval < 0.05).*(abs(rho)>0.2)


subplot(1,6,[1 4])  
bar1 = barh(rho);
yticks([1:1:numel(label)])
yticklabels(label);
set(gca,'xTick',-1:.2:1)
set(gca,'FontSize',8)
xlabel('\rho-value')
grid on; box on;
xlim([-0.4,0.6]);

subplot(1,6, [5 6])  
bar2 = barh(pval);
yticks([1:1:numel(label)])
set(gca,'xTick',0:.2:1)
set(gca,'FontSize',8)
xlabel('p-value')
grid on; box on;
xlim([0,1]);

figBar1.Position =  [10 10 600 700]

sgtitle('Pearson, Spearman, Kendall Corr. Coeff.','FontSize',12)
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
















%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end	% END OF FLAG PLOT



%Current color	Old color
%          	[0, 0.4470, 0.7410]	          	[0, 0, 1]
%          	[0.8500, 0.3250, 0.0980]	          	[0, 0.5, 0]
%          	[0.9290, 0.6940, 0.1250]	          	[1, 0, 0]
%          	[0.4940, 0.1840, 0.5560]	          	[0, 0.75, 0.75]
%          	[0.4660, 0.6740, 0.1880]	          	[0.75, 0, 0.75]
%          	[0.3010, 0.7450, 0.9330]	          	[0.75, 0.75, 0]
%          	[0.6350, 0.0780, 0.1840]	          	[0.25, 0.25, 0.25]
%






















%tcl.TileSpacing ='compact';
% openfig('fig1.fig');
% ax1=gca;
% openfig('fig2.fig')
% ax2=gca;
%  
% figure;
% tcl=tiledlayout(1,2);
% ax1.Parent=tcl;
% ax1.Layout.Tile=1;
% ax2.Parent=tcl;
% ax2.Layout.Tile=2;
