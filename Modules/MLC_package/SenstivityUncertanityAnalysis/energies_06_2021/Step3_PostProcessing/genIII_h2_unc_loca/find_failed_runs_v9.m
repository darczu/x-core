%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Developed by Piotr Darnowski, Warsaw University of Technology
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This file reads all MELCOR massage files.  It also checks EDF file and provides isSuccess about last timestep
% attempts to find failed cases. in this case it covers cases which are expected to cause debris ejection-but fails
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% USER INPUT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [T_all, T_best, T_success, T_partial, T_failed, ...
	Tbl_calc_all , Tbl_names, Tbl_variables, Tbl_calc, Tbl_best, Tbl_all, Tbl_partial, Tbl_success, ...
	variables, A_success, A_partial, A_failed, A, A_all, A_best,...
	data,  data0, ...
	timeend_success, timeend_partial, ...
	FOM, FOM_partial, FOM_success, FOM_failed, mCases, ...
	isPartial, isBest, isSuccess, isFailed ...  
	T_MAX, T_MAX2, x, y, x0, y0, t_interp, y_interp, y_interp_mean, y_interp_mean_all ...
	] = ...
	find_failed_runs_v9(varargin);

nargin

dfile = 'matlab-log.log';
if  exist(dfile, 'file'); 
	delete(dfile); 
end	
	diary(dfile)
	diary on
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%if ~exist('root_folder','var')
if nargin < 1
    root_folder = '.\'
else
	root_folder = varargin{1}
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%if ~exist('edffile','var')
if nargin < 2 
	edffile = 'EDF_5_MH2CR'
	%   edffile = 'EDF_5_MH2CR'; 
else
	edffile = varargin{2}
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%if ~exist('nCases','var')
if nargin < 3
	nCases = 100
else
	nCases = varargin{3}
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%if ~exist('partial_success','var')
if nargin < 4
	partial_success = 2500
else
	partial_success = varargin{4}	
	%   partial_success = 16000;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%if ~exist('file_diag','var')
if nargin < 5
    %file_diag = 'MELPWR.DIA'
		file_diag = 'MELFPT1.DIA';
else
	file_diag = varargin{5}
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin < 6
    colFOM = 2;
else
	colFOM = varargin{6}
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin < 7
    str = 'DEBRIS EJECTION THROUGH FAILED VESSEL ABOUT TO START'
	%   str = 'USER INPUT TEND'
else
	str = varargin{7}
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% END OF USER INPUT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% https://uk.mathworks.com/matlabcentral/answers/49414-check-if-a-file-exists
%% Check in output file if the following phrase is present.
isSuccess = []; idx = []; timeend =[]; FOM = []; isBest = [];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Read case0 - read best/default case
% if flag_read0 == 1
	i = 0; 
	folder = [ root_folder, 'case',num2str(i)];
    content  = fileread([folder,'\',file_diag]) ;
    content2 = importdata([folder,'\',edffile]) ;
    timeend = [timeend; content2(end,1)];   
    %FOM = [FOM; content2(end,2)];
	FOM = [FOM; content2(end,colFOM)];
    token0  = regexp( content, str, 'match' )  ;
	token_check0 = ~isempty(token0);	
    isSuccess = [isSuccess;  token_check0];
    idx = [idx; i];    
	data0{1} = content2(:,[1,colFOM]);
   % Zr_data0 =  content2(:,[1,3]);
	isBest = 1;
%end	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
%% read other cases
for i=1:nCases
    content = []; content2 = [];
	folder = [ root_folder, 'case',num2str(i)];    
        
    if exist([folder,'\',edffile])
        content2 = importdata([folder,'\',edffile]) ;
        
        if exist([folder,'\',file_diag])
            content  = fileread([folder,'\',file_diag]) ;
            token{i}  = regexp( content, str, 'match' )  ;
            
        else
            content = {''}; 
            %token{i}  = {0};
            token{i}  = {};
        end
          
    else
        content2 = zeros(1,colFOM);
        content = {''}; 
        token{i}  = {};
        %token{i}  = {0};
    end
  
    
	timeend = [timeend; content2(end,1)];
	%FOM = [FOM; content2(end,2)];      
	FOM = [FOM; content2(end,colFOM)];      
	%data{i} = content2(:,[1:2]);    	
	data{i} = content2(:,[1,colFOM]);    	

	   
	token_check = ~isempty(token{i});
	isSuccess = [isSuccess;  token_check];
	idx = [idx; i];   
	isBest = [isBest; 0];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Logical controls	
isSuccess;
isPartial = (timeend > partial_success);
isFailed  = ~isSuccess;
isSuccessNotBest = (isSuccess & ~isBest)
isPartialNotBest = (isPartial & ~isBest)
isFailedNotBest  = (isFailed & ~isBest)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
A 		= [idx, isSuccess, isPartial, timeend, FOM]
%A_all  = A(2:end,:); %without best case
A_all = A;
A_best = A(1,:);

% without best
isPartial2 = isPartial(2:end);
isSuccess2 = isSuccess(2:end);
isFailed2  = isFailed(2:end);

nSuccess = sum(isSuccess2 == 1);
nFailed = sum(isSuccess2 == 0);
nPartial = sum(isPartial2 == 1); %Patial scuees

ratio = nSuccess/(numel(isSuccess2));
partial_ratio = nPartial/(numel(isSuccess2));
failed_cases = []; success_cases = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
% Arrays with number s of dailed and success casses
	A_failed  = A_all(isSuccess==0,:);
	A_success = A_all(isSuccess==1,:);
    A_partial = A_all(isPartial==1,:);

FOM_success = FOM(isSuccess==1);
FOM_partial = FOM(isPartial==1);
FOM_failed  = FOM(isFailed==1);

timeend_success = timeend(isSuccess==1);
timeend_partial = timeend(isPartial==1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
% Table with all cases
T_all 			= array2table(A_all, 'VariableNames',{'Case','Success','Partial','TimeEnd','FoM'})
    			writetable(T_all,[root_folder, 'Fail-to-success.dat']);

% Best estimate				
T_best 			= array2table(A_best, 'VariableNames',{'Case','Success','Partial','TimeEnd','FoM'})
				writetable(T_best,[root_folder, 'Best.dat']);

% Table with failed cases	
T_failed 		= array2table(A_failed, 'VariableNames',{'Case','Success','Partial','TimeEnd','FoM'})
				writetable(T_failed,[root_folder, 'Failed-cases.dat']);  

% Table with success cases
T_success 		= array2table(A_success, 'VariableNames',{'Case','Success','Partial','TimeEnd','FoM'})
				writetable(T_success,[root_folder, 'Success-cases.dat']);  

% Partial results
T_partial 		= array2table(A_partial, 'VariableNames',{'Case','Success','Partial','TimeEnd','FoM'})
				writetable(T_partial,[root_folder, 'Partial-success-cases.dat']); 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
disp(['Partial sccuess is for cases with t>Time limit:', num2str(partial_success)])
disp(['Estimations - without case0'])
disp(['N = ', num2str(nCases)])
disp(['success = ', num2str(nSuccess)])
disp(['partial success = ', num2str(nPartial)])
disp(['failure = ', num2str(nFailed)])
disp(['success/failure = ', num2str(ratio)])
disp(['partial success/failure = ', num2str(partial_ratio)])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
% Number of loaded cases	
mCases = size(A_all,1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
% Read file with names % Problems with names
Tbl_names 	= readtable([root_folder, 'var_names.dat']);
Tbl_variables  =  Tbl_names(:,2)
variables =  table2cell( Tbl_variables);

Tbl_calc 	= readtable([root_folder, 'data_cols.dat']);
Tbl_calc.Properties.VariableNames(2:end) = variables;
Tbl_best 	= readtable([root_folder, 'best_est_data_cols.dat']);
Tbl_best.Properties.VariableNames(2:end) = variables;

% Table with best and normal
Tbl_calc_all    = [Tbl_best; Tbl_calc];
Tbl_calc_all  = Tbl_calc_all(1:mCases,:)
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Table with results and parameters
Tbl_all 	  = [Tbl_calc_all, T_all];
Tbl_partial = Tbl_all(isPartial==1,:);
Tbl_success = Tbl_all(isSuccess==1,:);
Tbl_failed  = Tbl_all(isSuccess==0,:);
Tbl_best 	= Tbl_all(isBest == 1,:);

writetable(Tbl_all,[root_folder, 'Tbl_all.dat']); 
Tbl_all.Properties.VariableNames
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
diary off


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
% FOM
for i=1:nCases	
	x{i} = data{i}(:,1);
	y{i} = data{i}(:,2);
end	
x0 = data0{1}(:,1);  y0 = data0{1}(:,2);
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
% INTERPOLATE DATA
y_interp = []; 
T_MAX = max(Tbl_all.("TimeEnd")); 
T_MAX2 = ceil(T_MAX/1000)*1000;
% SHADED PLOT
t_interp = linspace(0,T_MAX2,1000)';
for i=1:nCases
   if isPartial(i+1)
	y_interp1 = interp1(x{i},y{i},t_interp);
	y_interp2 = fillmissing(y_interp1,'previous');
	y_interp = [y_interp, y_interp2];
   end
end	



y_interp_mean_all = mean(y_interp,2);
y_interp_mean = mean(y_interp(end,:));

disp(['nargin = ', num2str(nargin)]);