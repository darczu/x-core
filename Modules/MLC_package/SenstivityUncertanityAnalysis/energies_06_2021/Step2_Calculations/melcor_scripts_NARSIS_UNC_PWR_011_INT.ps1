# =======================================================
# POWER-SHELL SCRIPTS WITH ALIASES TO RUN MELCOR 
  echo "MELCOR PARALLEL EXECUTION ENGINE" 
# =======================================================
$global:main_folder = 'E:\DARO\MELCOR\ROOT\NARSIS-GenIII' 
$global:results =	'\NARSIS_UNC_PWR_011_INT_Results_M2218' 
$global:case_folder =	'\case' 
# =======================================================
# RUN MELGEN
function global:m1 {	
	Param ([string]$name)	
	Invoke-Expression -Command	"E:\DARO\MELCOR\ROOT\NARSIS-GenIII\melgen_18019.exe i=$name ow=o"	
}
Set-alias melgen m1 -Scope Global	
# =======================================================
# RUN MELCOR
function global:m2 {	
	Param ([string]$name)	
	Invoke-Expression -Command "E:\DARO\MELCOR\ROOT\NARSIS-GenIII\melcor_18019.exe  i=$name ow=o"	
}	
Set-alias melcor m2 -Scope Global	
# =======================================================
function global:m100 {	
	Param ([string]$name)	
	$name_folder = $results + $case_folder +$name 	
	echo "Main: $main_folder"	
	$folderX = $main_folder+$name_folder 	
	echo "Runfolder: $folderX"	
	Invoke-Expression -Command "cd $folderX"	
	echo "Current: $PWD"	
	melgen INPUT.INP 
	melcor INPUT.INP 
}	
Set-alias melcor_unc m100 -Scope Global  
# =======================================================
