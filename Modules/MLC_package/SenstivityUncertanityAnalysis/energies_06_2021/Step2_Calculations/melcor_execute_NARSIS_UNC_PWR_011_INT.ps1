# =======================================================
# MELCOR EXECUTION FILE FOR POWER SHELL
# =======================================================
$scripts = @(  
"melcor_unc 1; melcor_unc 2; melcor_unc 3; melcor_unc 4; melcor_unc 5; melcor_unc 6; "
"melcor_unc 7; melcor_unc 8; melcor_unc 9; melcor_unc 10; melcor_unc 11; melcor_unc 12; "
"melcor_unc 13; melcor_unc 14; melcor_unc 15; melcor_unc 16; melcor_unc 17; melcor_unc 18; "
"melcor_unc 19; melcor_unc 20; melcor_unc 21; melcor_unc 22; melcor_unc 23; melcor_unc 24; "
"melcor_unc 25; melcor_unc 26; melcor_unc 27; melcor_unc 28; melcor_unc 29; melcor_unc 30; "
"melcor_unc 31; melcor_unc 32; melcor_unc 33; melcor_unc 34; melcor_unc 35; melcor_unc 36; "
"melcor_unc 37; melcor_unc 38; melcor_unc 39; melcor_unc 40; melcor_unc 41; melcor_unc 42; "
"melcor_unc 43; melcor_unc 44; melcor_unc 45; melcor_unc 46; melcor_unc 47; melcor_unc 48; "
"melcor_unc 49; melcor_unc 50; melcor_unc 51; melcor_unc 52; melcor_unc 53; melcor_unc 54; "
"melcor_unc 55; melcor_unc 56; melcor_unc 57; melcor_unc 58; melcor_unc 59; melcor_unc 60; "
"melcor_unc 61; melcor_unc 62; melcor_unc 63; melcor_unc 64; melcor_unc 65; melcor_unc 66; "
"melcor_unc 67; melcor_unc 68; melcor_unc 69; melcor_unc 70; melcor_unc 71; melcor_unc 72; "
"melcor_unc 73; melcor_unc 74; melcor_unc 75; melcor_unc 76; melcor_unc 77; melcor_unc 78; "
"melcor_unc 79; melcor_unc 80; melcor_unc 81; melcor_unc 82; melcor_unc 83; melcor_unc 84; "
"melcor_unc 85; melcor_unc 86; melcor_unc 87; melcor_unc 88; melcor_unc 89; melcor_unc 90; "
"melcor_unc 91; melcor_unc 92; melcor_unc 93; melcor_unc 94; melcor_unc 95; melcor_unc 96; "
"melcor_unc 97; melcor_unc 98; melcor_unc 99; melcor_unc 100; melcor_unc 101; melcor_unc 102; "
"melcor_unc 103; melcor_unc 104; melcor_unc 105; melcor_unc 106; melcor_unc 107; melcor_unc 108; "
"melcor_unc 109; melcor_unc 110; melcor_unc 111; melcor_unc 112; melcor_unc 113; melcor_unc 114; "
"melcor_unc 115; melcor_unc 116; melcor_unc 117; melcor_unc 118; melcor_unc 119; melcor_unc 120; "
"melcor_unc 121; melcor_unc 122; melcor_unc 123; melcor_unc 124; melcor_unc 125; melcor_unc 126; "
"melcor_unc 127; melcor_unc 128; melcor_unc 129; melcor_unc 130; melcor_unc 131; melcor_unc 132; "
"melcor_unc 133; melcor_unc 134; melcor_unc 135; melcor_unc 136; melcor_unc 137; melcor_unc 138; "
"melcor_unc 139; melcor_unc 140; melcor_unc 141; melcor_unc 142; melcor_unc 143; melcor_unc 144; "
"melcor_unc 145; melcor_unc 146; melcor_unc 147; melcor_unc 148; melcor_unc 149; melcor_unc 150; "
"melcor_unc 0;  "
)  
# =======================================================
foreach($script in $scripts) {	
	Start-Process powershell.exe "-NoExit .\melcor_scripts_NARSIS_UNC_PWR_011_INT.ps1; $script" 
  }  
# =======================================================
 Pause 
 'Finished Calculations...' 
# =======================================================
