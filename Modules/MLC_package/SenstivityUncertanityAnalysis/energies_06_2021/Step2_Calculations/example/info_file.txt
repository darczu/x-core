! =======================================================
! 03-Apr-2021 14:53:39
! =======================================================

 Flag - flag_type			 		PWR 
 Case Name (folder) case_name 		NARSIS_UNC_PWR_011_INT 
 Input file source input_folder  	C:\CloudStation\NARSIS\MODELE_AND_RESEARCH\git-narsis\narsis-calculations\INPUTS\MELCOR\0100_GenIII_NARSIS\MODEL_NO_CONT\UNCERTANITY\case0004_C19b 
 Input file - input_file				INPUT.INP 
 variables_file  					Variables.dat 
 N Sample Size 						150 
 Seed 								987654321 
 Discrete Seed 								987654321 
 Discrete Seed 								Ziggurat 
 Discrete Seed 								mlfg6331_64 
! =======================================================
Additional files: 

! =======================================================
