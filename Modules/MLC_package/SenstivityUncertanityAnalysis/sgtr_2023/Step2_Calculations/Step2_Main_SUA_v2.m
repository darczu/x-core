%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Developed by Piotr Darnowski, Warsaw University of Technology
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STEP 2 OF SUA - FIRST RUN STEP 2.  Step1_Main_SUA_v1.m
% GENERATES POWER SHELL SCRIPT TO RUN ALL CASES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Developed by Piotr Darnowski, Warsaw University of Technology
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% USER INPUT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear; clc; close all;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
flag_source_folder = 1;  % 1 - copy source folder for Step-1;   0 - source folder already in the folder for Step-2
flag_add_best  = 0; 	% ADDS BEST ESTIMATE CASE0 TO LIST OF RUNS
%flag_generate_run = 1; 	% 1 - Create Power-Shell scripts for calculations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%N = 120					% NUMBER OF CASES ALL
N = 20					% NUMBER OF CASES ALL
% CASES TO BE RUN WITH GENERATED SCRIPT
Nstart = 1;  				%You select reduced number of inputs to run. Normally Nstart = 1 and Nend = N
Nend = 20;
%Nend = 400;

M = 12				% Number of calculation terminals (CPU thereads?)/batches for power shell execution, Code will run N melcor cases in M terminals
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ABSOLUTE DIRECTORIES FOR STEP1 AND STEP2. SPEARATE EXECUTION DESIRED.
%step1_directory =  'C:\CloudStation\NARSIS\MODELE_AND_RESEARCH\SCRIPTS\MATLAB_XCore\x-core\Modules\MLC_package\SenstivityUncertanityAnalysis\Step1_Input'      		%Step 1 location
step1_directory =  'C:\OneDrive\OneDrive - Politechnika Warszawska\git\MY_PROJECTS\MATLAB_XCore\x-core\Modules\MLC_package\SenstivityUncertanityAnalysis\sgtr_2023\Step1_Input'      		%Step 1 location

%step2_directory =  'C:\CloudStation\NARSIS\MODELE_AND_RESEARCH\SCRIPTS\MATLAB_XCore\x-core\Modules\MLC_package\SenstivityUncertanityAnalysis\Step2_Caclulations' 	%Step 2 location
step2_directory =  'C:\OneDrive\OneDrive - Politechnika Warszawska\git\MY_PROJECTS\MATLAB_XCore\x-core\Modules\MLC_package\SenstivityUncertanityAnalysis\sgtr_2023\Step2_Calculations' 	%Step 2 location

% Local
%step1_directory = '.\Step1_Input'
%step2_directory ='.\Step2_Caclulations'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Absolute directory with calculations. can be the same as step2
%%calcs_folder_abs    = 'C:\CloudStation\ROOT\NARSIS-GenIII';					% Main folder for calculations
%%calcs_folder_abs = 'D:\ROOT\NARSIS-GenIII'
%%calcs_folder_abs = 'C:\CloudStation\ROOT\NARSIS-GenIII'


%calcs_folder_abs = 'C:\CloudStation\ROOT\NARSIS-GenIII'  %HP
%calcs_folder_abs = 'E:\ROOT\NARSIS-GenIII'  %Xeon40

calcs_folder_abs = 'E:\DARO\MELCOR\ROOT\NARSIS-GenIII'  %Xeon30

%calcs_folder_abs = 'E:\DARO\MELCOR\ROOT\NARSIS-FPT1'  %Xeon30

%calcs_folder_abs = 'C:\CloudStation\ROOT\NARSIS-FPT1'  %HP 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Local 	%Main case name as in Step1. Folder with main case generated in Step-1. This step will read it. HAS TO BE THE SAME AS SOURCE FOLDER IN STEP-1
%calcs_folder_abs    = '.\step2_directory2';
%subfolder		= 'MLC_UNC_Test1';						
%subfolder		= 'NARSIS_UNC_PWR_008_INT'
%subfolder		= 'NARSIS_UNC_PWR_004_EUT'

%subfolder		= 'NARSIS_UNC_PWR_014_INT' %PWR Peak profile
%subfolder		= 'NARSIS_UNC_PWR_015_INT' %PWR FPT profile
%subfolder		= 'NARSIS_UNC_FPT_E8_INT'

%subfolder		= 'NARSIS_UNC_PWR_016_WILKS_UNIFORM_INT' %PWR
%subfolder		= 'NARSIS_UNC_PWR_016_WILKS_BEST_INT' %PWR 

%subfolder		= 'NARSIS_UNC_PWR_014_WILKS_INT' %PWR
%subfolder		= 'NARSIS_UNC_PWR_014_SRS_INT' %PWR

subfolder = 'sgtr_uniform_1'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%subfolder_calcs	= 'NARSIS_UNC_PWR_004_Results';					%Main case name for working (calculations) folder - this step will generate it	
%subfolder_calcs	= [subfolder, '_Results_M2218']
%subfolder_calcs	= [subfolder, '_Results_M2211']
subfolder_calcs	= [subfolder, '_Results_M2218']

subsubfolder 	= 'case';									%name of case folder (without numbers), if the name is case, it will generate case1, case2, etc...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Location of MELCOR/MELGEN executables
%melcor_exe =  'melcor_15254.exe'; melgen_exe = 'melgen_15254.exe';
melcor_exe =  'melcor_18019.exe'; melgen_exe =  'melgen_18019.exe';
%melcor_exe =  'melcor_11932.exe'; melgen_exe =  'melgen_11932.exe';


melcor_folder = calcs_folder_abs 
%melcor_folder = 'C:\CloudStation\ROOT\NARSIS-GenIII'  %HP
%melcor_folder = 'E:\ROOT\NARSIS-GenIII'  %Xeon40
%melcor_folder = 'E:\DARO\MELCOR\ROOT\NARSIS-GenIII'  %Xeon30





%melcor_location = 'C:\CloudStation\ROOT\NARSIS-GenIII\melcor.exe';
%melgen_location = 'C:\CloudStation\ROOT\NARSIS-GenIII\melgen.exe';
%melcor_location = 'D:\ROOT\NARSIS-GenIII\melcor_15254.exe'
%melgen_location = 'D:\ROOT\NARSIS-GenIII\melgen_15254.exe'
%melcor_location = 'C:\CloudStation\ROOT\NARSIS-GenIII\melcor_15254.exe'
%melgen_location = 'C:\CloudStation\ROOT\NARSIS-GenIII\melgen_15254.exe'
%melcor_location = 'D:\ROOT\NARSIS-GenIII\melcor_18019.exe'
%melgen_location = 'D:\ROOT\NARSIS-GenIII\melgen_18019.exe'


melcor_location = [melcor_folder, '\', melcor_exe];
melgen_location = [melcor_folder, '\', melgen_exe];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Currently ALL has to be in single FILE !!!
%melcor_input_file   = 'INPUT.INP'; %Basic MELCOR/MELGEN inputfile name	
%melgen_input_file   = 'INPUT.INP'; %Basic MELCOR/MELGEN inputfile name	
melcor_input_file   = 'input.txt'; %Basic MELCOR/MELGEN inputfile name	
melgen_input_file   = 'input.txt'; %Basic MELCOR/MELGEN inputfile name
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% END OF USER INPUT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%name_powershell_1 =  ['melcor_execute_', subfolder, '.ps1'];  			%Power Shell Execution file  - use this file to start calculations
%name_powershell_2 =  ['melcor_scripts_', subfolder, '.ps1'];			%Power Shell scripts - additional script with alliases etc.

name_powershell_1 =  ['melcor_execute_', subfolder,'_cases_',num2str(Nstart) ,'to',num2str(Nend) ,'.ps1'];  			%Power Shell Execution file  - use this file to start calculations
name_powershell_2 =  ['melcor_scripts_', subfolder,'_cases_',num2str(Nstart) ,'to',num2str(Nend) ,'.ps1'];			%Power Shell scripts - additional script with alliases etc.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COPY FOLDER FROM STEP-1 TO CALCULATIONS FOLER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% USE IT IF SOURCE FOLDER IS NOT IN THE STEP-2 FOLDER ALREADY
if flag_source_folder == 1
	step1_source_folder      = [step1_directory,'\', subfolder];
	%desitnation_folder = [calcs_folder_abs, '\', subfolder_calcs];	
	desitnation_folder = [step2_directory, '\', subfolder_calcs];
	copyfile(step1_source_folder,desitnation_folder,'f');
end

%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GENERATE POWER SHELL FILES TO RUN MELCOR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
run SUA_Generate_PowerShell_v2.m

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN MELCOR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CURRENTLY ONLY MANUAL RUN WITH POWER-SHELL SCRIPTS
% !powershell -inputformat none -file myfile.ps1
% system('powershell -inputformat none -file myfile.ps1')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%







 
