%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SCRIPT DEDICATED TO DEFINE DISTRIBUTIONS OF UNCERTANITY PARAMETERS 
% DEVELOPED BY PIOTR DARNOWSKI FOR NARSIS PROJECT 2021 - ALL RIGHTS RESEVERD - SEE LICENSE PIOTR.DARNOWSKI@PW.EDU.PL, WARSAW UNIVERSITY OF TECHNOLOGY
% UPDATED 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% UNDER CONSTRUCTION - at this moment it is still not ready
% error('Under construction!')
% v2 = version 2 based on strucutres modified !
% version 1 without strucutres used for parameters  Distributions definition for matlab file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% USER CAN DEFINE DISTRIBUTIIN BELOW OR PROVIDE XML FILE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% READ FROM MATLAB INPUT AND SAVE STRUCUTRE
if flag_read_dist  == 0
    % READ MATLAB-TYPE INPUT FILE
    run struct_prototype.m 
    run INPUT_SUA_Distributions_v2.m 
    %write all param to single strucutre S, necesarry for writestruct to work
        S.param(:) = param(:);  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% READ STRUCUTRE FROM XML FILE
elseif flag_read_dist == 1
    %run struct_prototype.m 
    %run struct_prototype.m
    % READ XML FILE 
    % S = readstruct("music.txt","FileType","xml")
    % allows to read and save distributions for parameters as structures
    % if read and 1st does not have all expected fields, it can cause problems
    % In case of problems first entry in the xml should contain all field with at least empty value
    
    % FIRST trfield and is truncate in the XML input file cannot be empty!!!!
    S = readstruct([filename_input_parameters]);

    % Create separate param - array of strcutures - to simplfty our life
    for i=1:numel(S.param)    
        param(i) = S.param(i);
    end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
else
    error('Unknown flag_read_dist');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SAVE ALL PARAMETERS TO XML FILE - IT IS A COPY OR NEW FILE
xml_file = ['Saved_', case_name, filename_input_parameters];
writestruct(S,xml_file);

nParams = numel(param);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set to empty when is string or when does not exists - trlimits and
% istruncate variables
for i = 1:nParams
    if ~exist('param(i).istruncate')
        param(i).istruncate = [];
    elseif isstring(param(i).istruncate)
        if strcmp(param(i).istruncate,'')  param(i).istruncate = []; end
    end

    if ~exist('param(i).trlimits')
        param(i).trlimits = [];
    elseif isstring(param(i).trlimits)
        if strcmp(param(i).trlimits,'')  param(i).trlimits = []; end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% writestruct does not work is pdparam field is cell type with values
% it can be cell array with charcters only or matrix with numbers, it do nto know why, but it work this way

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE DISTRIBUTIONS
for i = 1:nParams 
    i  
    if strcmp(param(i).type,'Continous')                   
        pd_select(i) = create_dist(param(i).pdtype, param(i).pdparam, param(i).pdvalue,param(i).istruncate, param(i).trlimits);
    end
    if strcmp(param(i).type,'Discrete')
        pd_select(i) =  create_discrete_dist(param(i).pdtype,param(i).pdparam ,param(i).pdvalue);
    end
end
for i = 1:nParams 
    if strcmp(param(i).type,'Equal')
       % pd_select(i) = pd_select(param(i).pdtype); 
       pd_select(i) = pd_select(param(i).pdparam); 
    end    
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOOP AROUND FIELD NAMES

% fn = fieldnames(mystruct);
%for k=1:numel(fn)
%    if( isnumeric(mystruct.(fn{k})) )
%        % do stuff
%    end
% param(1).(fn{2})
%end
