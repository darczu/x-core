%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MATLAB MelSUA Toolbox for Uncertanity MELCOR
% DEVELOPED BY PIOTR DARNOWSKI FOR NARSIS PROJECT 2021 - ALL RIGHTS RESEVERD - SEE LICENSE PIOTR.DARNOWSKI@PW.EDU.PL, WARSAW UNIVERSITY OF TECHNOLOGY
% UPDATE 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VERSION OF THE ENGINE v4
% THIS CODE IS DEDICATED TO GENERATE PACKAGE OF FILES TO RUN MELCOR UNCERTANITY/SENSTIVITY ANALYSIS.  IT PREPARES FILES FOR STEP2- CALCULATIONS. 
% IT WILL BE MOVED TO SEPARATE TOOLBOX IN THE FUTURE, MAYBE REWRITTEN USING CLASSES.
% IT COVERS: SELECTION OF DISTIRBUTIONS, GIVEN IN INPUT_SUA_... .m FILE OR READ FROM XML TYPE INPUT FILE
% SAMPLING, ONLY MONTE CARLO TYPE SRS, LHS TO BE DEVELOPED, ALLOWS TRUNCATED DIST. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DEVELOPED WITH MATLAB 2021, DEMANDS STATISTICAL MATLAB TOOLBOX. UPDATED 2022, 2023
% SOME FUNCTIONALITIES MAY NOT WORK WITH ERALIER MATLAB VERSIONS - ESPECIALLY XML PROCESSING
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear; clc; close all; format long g; %format long g;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% USER INPUT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FLAGS
flag_save 		= 1  % 1 - SAVE TO FILE - GENERATE INPUT FILES; 0 - NO
flag_plot 		= 1  % 1 - PLOT DISTIRBUTIONS AND COMPARE WITH INPUT DATA; 0 - NO
flag_sample 	= 1  % 1 - PLOT FIGURES WITH SAMPLED DATA ; 0 - NO 
flag_savefig 	= 1  % 1 - SAVE FIG/TIFF; 0 - NO   

flag_type 		= 'PWR'  		% FLAGS - CAN INTRODUCE ADDITIONAL FLAG	% In this version: flag_type =  'PWR' or 'FPT' 
%flag_type = 'FPT' 

flag_copy_input = 1  % 1 - COPY MELCOR INPUT FILE TO FOLDERS, 0 - NO

%flag_read_dist  = 0 % 1 - READS DISTRIBTUIONS FROM XML FILE, 0 - DISTIRBUTION GIVEN IN MATLAB INPUT FILE INPUT_SUA...  
flag_read_dist  = 1 % 1 - READS DISTRIBTUIONS FROM XML FILE, 0 - DISTIRBUTION GIVEN IN MATLAB INPUT FILE INPUT_SUA...  
% XML DOES NOT WORK PROPERLY WITH OLDER MATLAB VERSIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SELECT SAMPLNG METHOD
sampling_method = 'SRS'; %Simple Monte Carlo. Simple Random Sampling
%sampling_method = 'LHS'; %Latin Hyper Cube
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FOLDER NAMES AND CASES % NAME OF THE FOLDER WITH INPUT CASES - IT WILL BE GENERATED IN THE SAME FOLDER AS THIS SCRIPT		
% NARSIS_UNC_PWR_010_INT Seed = 123456789 		123456789 
%case_name = 'NARSIS_UNC_PWR_013_INT'	 % Seed = 987654321
%case_name = 'example_read_XML'
%case_name = 'example_read_MAT'

%case_name = 'NARSIS_UNC_PWR_014_INT'	 % Seed = 214288134   %Peaked Profile
%% case_name = 'NARSIS_UNC_PWR_014_SRS_INT'	 % Peaked profile with SRS 400  %Seed = 968399680
%cases
%case_name = 'NARSIS_UNC_PWR_014_WILKS_INT'	 % Seed = 214288134   %Peaked Profile
%case_name = 'NARSIS_UNC_PWR_015_INT'	 % Seed = 214288134   %FPT-1 Profile
%case_name = 'NARSIS_UNC_FPT_E8_INT'	 % Seed = 915212742 
%case_name = 'NARSIS_UNC_PWR_016_WILKS_BEST_INT'	 
%case_name = 'NARSIS_UNC_PWR_016_WILKS_UNIFORM_INT'	 
case_name = 'sgtr_uniform_1';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% if flag_read_dist = 1      THIS FILE HAS TO EXIST 
% if  flag_read_dist = 0 filename_input_parameters  DOES NOT HAVE TO EXIST
% - it is created
% If flag is 1 then it will be saved to the file 'Saved_.... xml'   %'Example_Input_Parameters.xml'
%filename_input_parameters = 'Input_Parameters.xml';  
%filename_input_parameters = 'Example_Input_Parameters.xml';  

%filename_input_parameters = 'NARSIS_UNC_FPT_E8_INT.xml';  
%filename_input_parameters = 'NARSIS_UNC_PWR_016_WILKS_BEST_INT.xml';    % Seed =  214288134 
%filename_input_parameters = 'NARSIS_UNC_PWR_016_WILKS_UNIFORM_INT.xml';    %Seed  =  922128261  % Wilks Uniform
%filename_input_parameters = 'NARSIS_UNC_PWR_015_INT.xml';  
%filename_input_parameters = 'NARSIS_UNC_PWR_WILKS_014_INT.xml';  
%filename_input_parameters = 'NARSIS_UNC_PWR_SRS_014_INT.xml';  

filename_input_parameters = 'sgtr_uniform_1.xml';

% BE CAREFUL IT IS ABLE TO READ MISSING (XML) AND EMPTY VARIABLES (MAT)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NAME TEMPLATE FOR SINGLE CASE FOLDER WITH INPUT FILE, CASE1, CASE2, ETC... % IT WILL BE SET OF SUB-FOLDERS IN YOUR MAIN FOLDER
folder_name = 'case'				
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MELCOR INPUT FODLER AND INPUT FILE % PROVIDE LOCATION OF THE INPUT FILE WHICH WILL BE TEMPLATE % folder with base input file
% THIS FOLDER HAS TO EXIST
%input_folder = './INPUT'    
%input_folder = 'C:\CloudStation\NARSIS\MODELE_AND_RESEARCH\git-narsis\narsis-calculations\INPUTS\MELCOR\0100_GenIII_NARSIS\MODEL_NO_CONT\UNCERTANITY\case0004_C18c';  %INT
% input_folder = 'C:\CloudStation\NARSIS\MODELE_AND_RESEARCH\git-narsis\narsis-calculations\INPUTS\MELCOR\0100_GenIII_NARSIS\MODEL_NO_CONT\UNCERTANITY\case0004_C21';  %INT
% input_folder = 'C:\OneDrive\OneDrive - Politechnika Warszawska\DROPBOX\PHEBUS\PAA_Phebus\FPT1-MODEL\E8_MERGED_UNC'; %INT

input_folder =  'C:\OneDrive\OneDrive - Politechnika Warszawska\git\MY_PROJECTS\MATLAB_XCore\x-core\Modules\MLC_package\SenstivityUncertanityAnalysis\sgtr_2023\Step1_Input\INPUT'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NAME OF THE INPUTFILE IN THE FOLDER WITH INPUTFILE % INPUT FILE, LOCATED IN INPUT_FOLDER 
%  IT HAS TO EXIST 
%input_file   = 'INPUT.INP'			
input_file   = 'test_input.txt'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NAME OF VARIABLES FILE % NAME OF THE VARIABLES.DAT FILE, FOR MELCOR VARIABLE INPUT FUNCTIONALITY
% THIS FILE WILL BE CREATED
%variables_file = 'Variables.dat'  	
variables_file = 'sgtr_variables.dat'  	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SAMPLE SIZE - NUMBER OF INPUTDECKS TO BE GENERATED
%N = 120    
%N = 400    
N = 100
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SEED - in V1 it was only for continous variable
%Seed = 120398536   %Seed = 987654321	%Seed = 123456789 
%Seed = 214288134  % Randomly selected

% Seed = 214288134 % PWR Peak   C20
% Seed =  350648396 % PWR FPT-1 profile  C21
%Seed = 669123320  %14 PEaked WILKS
%Seed =  968399680  %14 PEaked 400 SRS
%Seed = 915212742 % FPT  E8

 %Seed =  214288134  % Wilks BE
 %Seed  =  922128261  % Wilks Uniform

Seed = 100000000 + (899999999).*rand(1,1)  % Radom generator of the seed
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% END OF USER INPUT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Necesarry to have repeatable results:
s          = rng(Seed)				% Use seed %s = rng('shuffle');  %based on time
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OLD PART OF RN GENERATOR FOR DISCRETE SAMPLING - NOW IT USES CONTINOUS FUNCTION
% % SEED SET FOR DISCRETE VARIALBE - USE THE SAME AS FOR CONT.
% % DEACTIVATED IN v2
%s          = rng(Seed)				% Use seed %s = rng('shuffle');  %based on time
% %%%s_discrete = RandStream('mlfg6331_64','Seed',Seed)  %Discrete random stream seed
% %s=rng('default')
% %s=rng('shuffle')
% %Seed = 100000000 + (899999999).*rand(1,1);
% %rng(Seed);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
nCases = N;	
step1_directory1 = cd;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% BASE CASE INPUT FILE - PROVIDE THE NAME OF THE FOLDER WITH BEST ESTIMATE INPUTDECK - IT WILL BE GENERATED BASED ON THE DEFAULT VALUES
Best_Estimate_folder = 'case0'    % DO NOT CHANGE %Best_Estimate_folder = 'BestEstimate' % do not change it % IT IS FIXED
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SCRIPT WITH DEFINITION OR READING XMLs WITH PDFS AND CDFS FOR THE ANALYSIS.
run SUA_Distributions_v4.m   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% SCRIPT WITH SAMPLING OF VALUES
run SUA_Sampling_v2.m
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% SAVE DATA TO FILE - FILE GENERATION

%if  flag_read_dist == 1 
	run SUA_Save2File_v4.m
%elseif flag_read_dist == 0
%	run SUA_Save2File_v2.m
%end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN PLOTTING TO VERIFY GENERATION, COMPARE WITH ALTERNATIVE DATA, EXPERIMENT ETC.

if flag_plot == 1
	% run	SUA_VerifyPlot_v3.m
    %add corrplot(T1)
end	

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
if flag_copy_input == 1
   run SUA_copy_input.m
end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	




