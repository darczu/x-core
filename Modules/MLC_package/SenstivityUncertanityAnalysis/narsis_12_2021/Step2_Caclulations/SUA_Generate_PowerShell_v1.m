%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Developed by Piotr Darnowski, Warsaw University of Technology
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script generates input for Power-Shell
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NO USER INPUT HERE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% "melcor_unc 1; melcor_unc 2; Pause"
% iseven = rem(m, 2) == 0

% K - number of code runs in terminal
% M - number of terminals

% e.g.  N = 100 cases
%   M = 30 terminals
%   K = 4 runs per terminal
%   20 runs too much
% 
% Number of runs per terminal, last is reduced
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generation of string to execute power-shell script
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%N = 25
%M = 3

if (M > N) 
	error('number of terminals larger than number of cases');
end

K = ceil(N./M);
%K = ceil((Nend-Nstart)./M);

string0 = 'melcor_unc';
string = cell(M,1);

counter0 = 0;
k = 1;

%for j=1:N
for j=Nstart:Nend
	 if( rem(j,(K)) == 0)
		new_string = [string{k}, [string0,' ', num2str(j), '; ']]; 
		%string{k} = ['"', new_string, ' Pause" '];
		string{k} = ['"', new_string, '"'];
		if j < N
		 k = k + 1;
		end
	 else
		string{k} = [ string{k}, [string0,' ', num2str(j), '; ']]; 
	 end
end
 
  if( rem(j,(K)) ~= 0)
		%string{k} = ['"', string{k},  ' Pause" '];
		string{k} = ['"', string{k},  '"'];
  end
	
% REMOVE EMPTY CELLS	
string=string(~cellfun('isempty',string))
	

disp(string)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Save to file - generation of power-shell scripts
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% File for execution
%fid1 = fopen(name_powershell_1 , 'wt');
fid1 = fopen([step2_directory,'\', subfolder_calcs, '\' , name_powershell_1] , 'wt');

fprintf(fid1,'# =======================================================\n');
fprintf(fid1,'# MELCOR EXECUTION FILE FOR POWER SHELL\n');
fprintf(fid1,'# =======================================================\n');
fprintf(fid1,'$scripts = @(  \n');
%for i=1:numel(string)
for i=1:numel(string)
	fprintf(fid1,'%s\n', string{i});
end

% Default model
fprintf(fid1,'"melcor_unc 0;  "\n' );

fprintf(fid1,')  \n');
fprintf(fid1,'# =======================================================\n');

fprintf(fid1,'foreach($script in $scripts) {	\n');
fprintf(fid1,'	Start-Process powershell.exe "-NoExit .\\%s; $script" \n ', name_powershell_2);
%fprintf(fid1,'	Start-Process powershell.exe "-NoExit; Start-Transcript -Path .\\console_log_$script.txt; .\\%s; $script; Stop-Transcript" \n ', name_powershell_2);
%fprintf(fid1,'	Start-Process powershell.exe "-NoExit; Start-Transcript -Path .\\console_log.txt; .\\%s; $script; Stop-Transcript" \n ', name_powershell_2);
fprintf(fid1,' }  \n');
fprintf(fid1,'# =======================================================\n');

fprintf(fid1,' Pause \n' );
fprintf(fid1,' ''Finished Calculations...'' \n');
fprintf(fid1,'# =======================================================\n');

fclose(fid1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% File script, SCRIPT WITH ALIASES TO RUN MELCOR
%fid2 = fopen(name_powershell_2 , 'wt');
fid2 = fopen([step2_directory,'\', subfolder_calcs, '\' ,name_powershell_2 ], 'wt');

fprintf(fid2,'# =======================================================\n');
fprintf(fid2,'# POWER-SHELL SCRIPTS WITH ALIASES TO RUN MELCOR \n');
fprintf(fid2,'  echo "MELCOR PARALLEL EXECUTION ENGINE" \n');
fprintf(fid2,'# =======================================================\n');
fprintf(fid2,'$global:main_folder = ''%s'' \n',calcs_folder_abs );
fprintf(fid2,'$global:results =	''\\%s'' \n', subfolder_calcs);
fprintf(fid2,'$global:case_folder =	''\\%s'' \n', subsubfolder);

fprintf(fid2,'# =======================================================\n');
fprintf(fid2,'# RUN MELGEN\n');
fprintf(fid2,'function global:m1 {	\n');
fprintf(fid2,'	Param ([string]$name)	\n');
%fprintf(fid2,'	Invoke-Expression -Command	"$main_folder\melgen.exe i=$name ow=o"	\n');
fprintf(fid2,'	Invoke-Expression -Command	"%s i=$name ow=o"	\n', melgen_location);
fprintf(fid2,'}\n');
fprintf(fid2,'Set-alias melgen m1 -Scope Global	\n');

fprintf(fid2,'# =======================================================\n');
fprintf(fid2,'# RUN MELCOR\n');
fprintf(fid2,'function global:m2 {	\n');
fprintf(fid2,'	Param ([string]$name)	\n');
%fprintf(fid2,'	Invoke-Expression -Command "$main_folder\melcor.exe  i=$name ow=o"	\n');
fprintf(fid2,'	Invoke-Expression -Command "%s  i=$name ow=o"	\n', melcor_location);
fprintf(fid2,'}	\n');
fprintf(fid2,'Set-alias melcor m2 -Scope Global	\n');
fprintf(fid2,'# =======================================================\n');


fprintf(fid2,'function global:m100 {	\n');
fprintf(fid2,'	Param ([string]$name)	\n');
fprintf(fid2,'	$name_folder = $results + $case_folder +$name 	\n');
fprintf(fid2,'	echo "Main: $main_folder"	\n');
fprintf(fid2,'	$folderX = $main_folder+$name_folder 	\n');
fprintf(fid2,'	echo "Runfolder: $folderX"	\n');
fprintf(fid2,'	Invoke-Expression -Command "cd $folderX"	\n');
fprintf(fid2,'	echo "Current: $PWD"	\n');

fprintf(fid2,'	melgen %s \n', melgen_input_file  );
fprintf(fid2,'	melcor %s \n', melcor_input_file  );

fprintf(fid2,'}	\n');
fprintf(fid2,'Set-alias melcor_unc m100 -Scope Global  \n');


fprintf(fid2,'# =======================================================\n');
fclose(fid2);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




