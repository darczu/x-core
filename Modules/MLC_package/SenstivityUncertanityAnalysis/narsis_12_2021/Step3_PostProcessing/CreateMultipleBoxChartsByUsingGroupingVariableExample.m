%% TAKEN FROM MATHWORKS SITE
%% Create Multiple Box Charts Using Positional Grouping Variable
% Plot the magnitudes of earthquakes according to the month in which they occurred. 
% Use a vector of earthquake magnitudes and a grouping variable indicating the 
% month of each earthquake. For each group of data, create a box chart and place 
% it in the specified position along the _x_-axis.
% 
% Read a set of tsunami data into the workspace as a table. The data set includes 
% information on earthquakes as well as other causes of tsunamis. Display the 
% first eight rows, showing the month, cause, and earthquake magnitude columns 
% of the table.

tsunamis = readtable('tsunamis.xlsx');
tsunamis(1:8,["Month","Cause","EarthquakeMagnitude"])
%% 
% Create the table |earthquakes|, which contains data for the tsunamis caused 
% by earthquakes.

unique(tsunamis.Cause)
idx = contains(tsunamis.Cause,'Earthquake');
earthquakes = tsunamis(idx,:);
%% 
% Group the earthquake magnitudes based on the month in which the corresponding 
% tsunamis occurred. For each month, display a separate box chart. For example, 
% |boxchart| uses the fourth, fifth, and eighth earthquake magnitudes, as well 
% as others, to create the third box chart, which corresponds to the third month.

boxchart(earthquakes.Month,earthquakes.EarthquakeMagnitude)
xlabel('Month')
ylabel('Earthquake Magnitude')
%% 
% Notice that because the month values are numeric, the _x_-axis ruler is also 
% numeric.
%% 
% For more descriptive month names, convert the |earthquakes.Month| column to 
% a |categorical| variable.

monthOrder = ["Jan","Feb","Mar","Apr","May","Jun","Jul", ...
    "Aug","Sep","Oct","Nov","Dec"];
namedMonths = categorical(earthquakes.Month,1:12,monthOrder);
%% 
% Create the same box charts as before, but use the |categorical| variable |namedMonths| 
% instead of the numeric month values. The _x_-axis ruler is now categorical, 
% and the order of the categories in |namedMonths| determines the order of the 
% box charts.

boxchart(namedMonths,earthquakes.EarthquakeMagnitude)
xlabel('Month')
ylabel('Earthquake Magnitude')
%% 
% _Copyright 2019 The MathWorks, Inc._