% Function to plot Figure of Merit 
function p = plot_ecdf(numFig,Data, LineType, LineColor, LineWidth,MarkerType,MarkerSize,LegendEntry)  %, information)

    if nargin < 7; MarkerSize = 6; end
    if nargin < 6; MarkerType = 'none'; end
    if nargin < 5; LineWidth = 1; end
    if nargin < 4; LineColor = 'k'; end	
    if nargin < 3; LineType = '-'; end	
    

        [f,x] = ecdf(Data);

        xData = x;
        yData = f;
        
        figure(numFig);
        p = plot(xData,yData);
        p.LineWidth = LineWidth;
        p.Color = LineColor;
        p.LineStyle = LineType;
        p.Marker = MarkerType; 
        p.MarkerSize  = MarkerSize;
    
        fig = gcf;	
        ax = fig.CurrentAxes;
        ax.GridAlpha = 0.1;
        ax.Box = 'on';
       
        uistack(p,'top') ;

        ylabel('ECDF, [-]');
        xlabel('FoM');
        grid on;   box on; 


        %[f,x] = ecdf(FOM_partial);
        %plot(x,f,'LineWidth',1.5)