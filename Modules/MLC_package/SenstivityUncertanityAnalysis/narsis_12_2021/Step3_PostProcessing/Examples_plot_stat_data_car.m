%% TAKEN FROM MATHWORKS SITE
%% MATLAB  Example https://uk.mathworks.com/help/stats/visualizing-multivariate-data.html
load carbig
X = [MPG,Acceleration,Displacement,Weight,Horsepower];
varNames = {'MPG'; 'Acceleration'; 'Displacement'; 'Weight'; 'Horsepower'};

%% Scatter Plot Matrices
figure(1)
gplotmatrix(X,[],Cylinders,['c' 'b' 'm' 'g' 'r'],[],[],false);
text([.08 .24 .43 .66 .83], repmat(-.1,1,5), varNames, 'FontSize',8);
text(repmat(-.12,1,5), [.86 .62 .41 .25 .02], varNames, 'FontSize',8, 'Rotation',90);

%% Parallel Coordinates Plots
figure(2)
Cyl468 = ismember(Cylinders,[4 6 8]);
parallelcoords(X(Cyl468,:), 'group',Cylinders(Cyl468), ...
               'standardize','on', 'labels',varNames)
                    
%% Paralell with quantiles
figure(3)
parallelcoords(X(Cyl468,:), 'group',Cylinders(Cyl468), ...
               'standardize','on', 'labels',varNames, 'quantile',.25)
           
%% Andrewsplot
figure(4)
andrewsplot(X(Cyl468,:), 'group',Cylinders(Cyl468), 'standardize','on')
t1 = 1/3;
[1/sqrt(2) sin(2*pi*t1) cos(2*pi*t1) sin(4*pi*t1) cos(4*pi*t1)]

%% Glyph Plots
figure(5)
h = glyphplot(X(1:9,:), 'glyph','star', 'varLabels',varNames, 'obslabels',Model(1:9,:));
set(h(:,3),'FontSize',8);

%% Glyph Plots and Multidimensional Scaling
models77 = find((Model_Year==77));
dissimilarity = pdist(zscore(X(models77,:)));
Y = mdscale(dissimilarity,2);
figure(6)
glyphplot(X(models77,:), 'glyph','star', 'centers',Y, ...
          'varLabels',varNames, 'obslabels',Model(models77,:), 'radius',.5);
title('1977 Model Year');

%% Faceplot
figure(7)
glyphplot(X(models77,:), 'glyph','face', 'centers',Y, ...
          'varLabels',varNames, 'obslabels',Model(models77,:));
title('1977 Model Year');
