
% location of the input file
input_location = [input_folder,'\',input_file];

% Create copy of the base input file
copyfile(input_location, [case_name,'/','Base_INPUT.INP'], 'f');
%copyfile(input_location, [case_name,'/Best_Estimate/','INPUT.INP'], 'f');
copyfile(input_location, [case_name,'/',Best_Estimate_folder,'/','INPUT.INP'], 'f');


% Copy XML file to folder
%copyfile(['./', xml_file], [case_name,'/', xml_file],'f');
%movefile(['./', xml_file], [case_name,'/', xml_file],'f');


% Create copies of input files for uncertanity calcs
for i = 1:N
	input_destination = [case_name,'/',folder_name,num2str(i)];
	[status,message,messageId] = copyfile(input_location, input_destination, 'f');
	%[status,message,messageId] = copyfile(input_location, input_destination);
end


