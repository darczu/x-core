%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Developed by Piotr Darnowski, Warsaw University of Technology
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SCRIPT DECICATED TO FIT DISTIRUBTIONS, VISUALIZE DISTRIBUTIONS AND SAMPLES, COMPARE WITH OTHER DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load X-Core package
%addpath(genpath('D:\NARSIS_DOCS\MODELE_AND_RESEARCH\SCRIPTS\MATLAB_XCore\x-core'));  % Add X-Core - Matlab Nuc. Eng. Material Databse
% Data generated with https://apps.automeris.io/wpd/
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SETUP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% clear; clc; close all; format short g; %format long g;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLOT SAMPLED DATA - IF YES CREATES 3 PLOTS IN ONE, OTHERWISE 2
if flag_sample == 1
  N_subplot = 3;
else 
  N_subplot = 2;
end  


set(0,'defaulttextInterpreter','none')  %LATEX AXIS LABELS
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DISCRETE VARIABLES PLOTS
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameter 12
i = 12;
figure(i);  grid on; box on;
subplot(3,1,1)
bar(alter12,probs12);
title('Parameter 12 - Discrete PDF');
subplot(3,1,2)
histogram(param_select{i,1},edges12)
title('Samples');
subplot(3,1,3)
histogram(param_select{i,1},edges12,'Normalization','probability')
title('PDF - Samples');
xlabel('Oxidation Model Number');
if flag_savefig == 1
	figure_file = ['./',case_name,'/Fig_',num2str(i),'_',flag_type]; savefig(figure_file); print('-dtiff',figure_file);	
end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameter 13
i = 13;
figure(i);  grid on; box on;
subplot(3,1,1)
bar(alter13,probs13);
title('Parameter 13 - Discrete PDF');
subplot(3,1,2)
histogram(param_select{i,1},edges13)
title('Samples');
subplot(3,1,3)
histogram(param_select{i,1},edges13,'Normalization','probability')
title('PDF - Samples');
xlabel('Fuel Failure Criterion Model Model Number');
if flag_savefig == 1
	figure_file = ['./',case_name,'/Fig_',num2str(i),'_',flag_type]; savefig(figure_file); print('-dtiff',figure_file);	
end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CONTINOUS VARIABLES PLOTS
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameter 1 - Based n Figure 15 Molten Material Holdup Parameters - Zr Melt Release
% SC1131(2): Zircaloy melt breakout temperature (k)) -  PB Uncertanity
i = 1;																	% FIGURE NUMBER
file = './DATA/Fig15.csv';												% FILE WITH DATA
data=csvread(file);  data = sort(data,1); x = data(:,1); y = data(:,2); % MANUALLY FITTED DATA FOR CDFs.
xx = linspace(2050,2700,300); 											% FOR PLOTTING
		
% Normal fitted to Data in Gauntt H2
pdff = pdf(pd(i),xx); 													% CREATE PDF FUNCTION FOR DISTRIBUTION PD OVER REGION XX	
cpd  = cdf(pd(i),xx);													% CREATE CDF FUNCTION FOR DISTRIBUTION PD OVER REGION XX	
%% Triangular fitted new - PB SOARCA UA
pdff1 = pdf(pd1(i),xx); 												
cpd1  = cdf(pd1(i),xx);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(i);													
subplot(N_subplot,1,1);  hold on; grid on; box on; ylim([0 1]); 
plot(x,y,'ro','MarkerSize',5);
plot(xx,cpd,'-r'); 
plot(xx,cpd1,'-b');
line([SOARCA{i} SOARCA{i}], [0 1],'Color','m','LineStyle','-');
line([BEST_EST{i} BEST_EST{i}], [0 1],'Color','g','LineStyle','--');
line([DEFAULT{i} DEFAULT{i}], [0 1],'Color','y','LineStyle','--');

title('Parameter 1 - CDF SC1131(2)');
legend('Gauntt Fig15','Fit Norm,','SOARCA Tri.', 'SOARCA', 'BE','DFT','Location','NorthWest')

subplot(N_subplot,1,2); hold on; grid on; box on;
plot(xx,pdff,'-r'); 
plot(xx,pdff1,'-b');
title('PDF - SC1131(2)');
legend('Gauntt Fit','SOARCA Tri.','Location','NorthWest')
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLOT ADDITIONAL SUBPLOT WITH SAMPLED VALUES
if flag_sample == 1
	run plot_sample1.m
end
xlabel('Parameter 1 - Zircaloy Melt Breakout Temperature SC1131(2)');
% SAVE TO FIGURE TYPE FILE
if flag_savefig == 1
	figure_file = ['./',case_name,'/Fig_',num2str(i),'_',flag_type]; savefig(figure_file); print('-dtiff',figure_file);	
end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameter 2
%Figure 16 Core (Fuel) Component Failure Parameters	 Fuel Rod Failure/Collapse temp	SC1132(1)
%SOARCA = 2800;  %PB Uncertanity
i=2;
file = './DATA/Fig16.csv';
data=csvread(file);  data = sort(data,1); x = data(:,1);  y = data(:,2);
xx = linspace(2200,2900,300); %For plotting

%% Normal fitted to Data in Gauntt H2
pdff 	= pdf(pd(i),xx); 
cpd 	= cdf(pd(i),xx);
%% Normal from SOARCA SEQ
pdff1 	= pdf(pd1(i),xx); 
cpd1	= cdf(pd1(i),xx);
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(i);
subplot(N_subplot,1,1);  hold on; grid on; box on;  ylim([0 1]); 
plot(x,y,'ro','MarkerSize',5);
plot(xx,cpd,'-r');
plot(xx,cpd1,'-b');
line([SOARCA{i} SOARCA{i}], [0 1],'Color','m','LineStyle','-');
line([BEST_EST{i} BEST_EST{i}], [0 1],'Color','g','LineStyle','--');
line([DEFAULT{i} DEFAULT{i}], [0 1],'Color','y','LineStyle','--');

title('Paramter 2 - CDF SC1132(1)');
legend('Gauntt Fig16','Fit Norm.','SOARCA Norm.', 'SOARCA', 'BE','DFT','Location','NorthWest');


subplot(N_subplot,1,2); hold on; grid on; box on;
plot(xx,pdff,'-r'); 
plot(xx,pdff1,'-b');
title('PDF - SC1132(1)');
legend('Fit Norm.','SOARCA Tri.','Location','NorthWest')
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if flag_sample == 1
 run plot_sample1.m
end
xlabel('Parameter 2 - Fuel Rod Failure Temperature SC1132(1)');
if flag_savefig == 1
	figure_file = ['./',case_name,'/Fig_',num2str(i),'_',flag_type]; savefig(figure_file); print('-dtiff',figure_file);	
end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameter 3
%Figure 17  FUOZR	Secondary Material Transport Parameters	Secondary UO2 Content, dissolved UO2
i=3;
file = './DATA/Fig17.csv';
data=csvread(file);  data = sort(data,1); x = data(:,1);  y = data(:,2);
xx = linspace(-0.25,0.5,300); %For plotting

pdff = pdf(pd(i),xx);
cpd  = cdf(pd(i),xx);
tpdff = pdf(tpd(i),xx);
ctpd  = cdf(tpd(i),xx);
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(i);
subplot(N_subplot,1,1);  hold on; grid on; box on; ylim([0 1]); 
plot(x,y,'ro','MarkerSize',5);
plot(xx,cpd,'-r'); 
plot(xx,ctpd,'-b');
line([SOARCA{i} SOARCA{i}], [0 1],'Color','m','LineStyle','-');
line([BEST_EST{i} BEST_EST{i}], [0 1],'Color','g','LineStyle','--');
line([DEFAULT{i} DEFAULT{i}], [0 1],'Color','y','LineStyle','--');

title('Parameter 3 - CDF COR_CMT FUOZR');
legend('Gauntt Fig17','Fit Norm.','Fit Tr. Norm.','SOARCA','BE','DFT','Location','NorthWest')

subplot(N_subplot,1,2); hold on; grid on; box on;
plot(xx,pdff,'-r'); 
plot(xx,tpdff,'-b');
title('PDF - FUOZR');
legend('Fit Norm.','Fit Tr. Norm.','Location','NorthWest')

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if flag_sample == 1
	run plot_sample2.m
end
xlabel('Parameter 3 - Secondary Material Transport UO2 FUOZR')
if flag_savefig == 1
	figure_file = ['./',case_name,'/Fig_',num2str(i),'_',flag_type]; savefig(figure_file); print('-dtiff',figure_file);	
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameter 4
%Figure 18 - Candling Heat Transfer Coefficients Zr Freezing Coeff COR_CHT(2)
i=4;
file = './DATA/Fig18.csv';
data=csvread(file);  data = sort(data,1); x = data(:,1);  y = data(:,2);
xx = linspace(0,25000,300); %For plotting

pdff 	= pdf(pd(i),xx); 
cpd 	= cdf(pd(i),xx);
tpdff 	= pdf(tpd(i),xx);0,
ctpd 	= cdf(tpd(i),xx);
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(i)
subplot(N_subplot,1,1);  hold on; grid on; box on;  ylim([0 1]); 
plot(x,y,'ro','MarkerSize',5);
plot(xx,cpd,'-r');
plot(xx,ctpd,'--b');
line([SOARCA{i} SOARCA{i}], [0 1],'Color','m','LineStyle','-');
line([BEST_EST{i} BEST_EST{i}], [0 1],'Color','g','LineStyle','--');
line([DEFAULT{i} DEFAULT{i}], [0 1],'Color','y','LineStyle','--');

title('Parameter 4 - CDF COR_CHT(1) HFRZZR');
legend('Gauntt Fig18', 'Fit Log-Norm.','Fit Tr. Log-Norm', 'SOARCA','BE','DFT','Location','NorthWest');

subplot(N_subplot,1,2); hold on; grid on; box on;
plot(xx,pdff,'-r'); 
plot(xx,tpdff,'--b');
legend('Fit Log-Normal','Trunc. LogNorm','Location','NorthWest');
title('PDF - COR_CHT(1) HFRZZR');

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if flag_sample == 1
	run plot_sample2.m
end
xlabel('Parameter 4 - Zr Freezing Coefficient COR_CHT(1) HFRZZR')
if flag_savefig == 1
	figure_file = ['./',case_name,'/Fig_',num2str(i),'_',flag_type]; savefig(figure_file); print('-dtiff',figure_file);	
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameter 5
%Figure 19   Core-Region Particulate Debris Diameter
i=5;		
file = './DATA/Fig19.csv'; data=csvread(file);  data = sort(data,1); x = data(:,1);  y = data(:,2);
xx = linspace(0,0.05,300); %For plotting

pdff = pdf(pd(i),xx);
cpd  = cdf(pd(i),xx);

tpdff = pdf(tpd(i),xx);
ctpd  = cdf(tpd(i),xx);
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(i);
subplot(N_subplot,1,1);  hold on; grid on; box on; ylim([0 1]); 
plot(x,y,'ro','MarkerSize',5);
plot(xx,cpd,'-r'); 
plot(xx,ctpd,'--b');
line([SOARCA{i} SOARCA{i}], [0 1],'Color','m','LineStyle','-');
line([BEST_EST{i} BEST_EST{i}], [0 1],'Color','g','LineStyle','--');
line([DEFAULT{i} DEFAULT{i}], [0 1],'Color','y','LineStyle','--');

title('Fig19 CDF Core-Region Particulate Debris Diameter DHYD');		
legend('Gauntt Data','Fit LogNorm.','Trunc. LogNorm','SOARCA','BE','DFT','Location','NorthWest')

subplot(N_subplot,1,2); hold on; grid on; box on;
plot(xx,pdff,'-r'); 
plot(xx,tpdff,'--b');
title('Fig19 PDF');
legend('Fit Norm.','Trunc. Norm.','Location','NorthWest')

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if flag_sample == 1
	run plot_sample2.m
end
xlabel('Parameter 5 - Core-Region Particulate Debris Diameter DHYD')
if flag_savefig == 1
	savefig(['./',case_name,'/Fig_',num2str(i)]);
end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameter 6
%Figure 20  Lower Plenum Debris Diamter		
i=6;
file = './DATA/Fig20.csv';
data=csvread(file);  data = sort(data,1); x = data(:,1);  y = data(:,2);
xx = linspace(0.0,0.07,300); %For plotting

pdff = pdf(pd(i),xx);
cpd  = cdf(pd(i),xx);

tpdff = pdf(tpd(i),xx);
ctpd = cdf(tpd(i),xx);
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(i);
subplot(N_subplot,1,1);  hold on; grid on; box on; ylim([0 1]); 
plot(x,y,'ro','MarkerSize',5);
plot(xx,cpd,'-r'); 
plot(xx,ctpd,'--b');
line([SOARCA{i} SOARCA{i}], [0 1],'Color','m','LineStyle','-');
line([BEST_EST{i} BEST_EST{i}], [0 1],'Color','g','LineStyle','--');
line([DEFAULT{i} DEFAULT{i}], [0 1],'Color','y','LineStyle','--');

title('Fig20 CDF Lower Plenum Particulate Debris Diameter DHYD');		
legend('Gauntt Data','Fit LogNorm.','Trunc. LogNorm','SOARCA','BE','DFT','Location','NorthWest')

subplot(N_subplot,1,2); hold on; grid on; box on;
plot(xx,pdff,'-r'); 
plot(xx,tpdff,'--b');
title('Fig20 PDF');
legend('Fit Norm.','Truncated Norm.','Location','NorthWest')


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if flag_sample == 1
	run plot_sample2.m
end
if flag_savefig == 1
	figure_file = ['./',case_name,'/Fig_',num2str(i),'_',flag_type]; savefig(figure_file); print('-dtiff',figure_file);	
end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameter 7
% Figure 21 Debris porosity (PORDP)
i=7;
file = './DATA/Fig21.csv'; data=csvread(file);  data = sort(data,1); x = data(:,1);  y = data(:,2);
xx = linspace(0,0.5,300); %For plotting

pdff = pdf(pd(i),xx);
cpd  = cdf(pd(i),xx);

tpdff = pdf(tpd(i),xx);
ctpd = cdf(tpd(i),xx);
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(i);
subplot(N_subplot,1,1);  hold on; grid on; box on; ylim([0 1]); xlim([0 0.6]); 
plot(x,y,'ro','MarkerSize',5);
plot(xx,cpd,'-r'); 
plot(xx,ctpd,'--b');
line([SOARCA{i} SOARCA{i}], [0 1],'Color','m','LineStyle','-');
line([BEST_EST{i} BEST_EST{i}], [0 1],'Color','g','LineStyle','--');
line([DEFAULT{i} DEFAULT{i}], [0 1],'Color','y','LineStyle','--');

title('Fig21 CDF Debris Porosity PORDP');		
legend('Gauntt Data','Fit LogNorm.','Trunc. LogNorm','SOARCA','BE','DFT','Location','NorthWest')

subplot(N_subplot,1,2); hold on; grid on; box on;
plot(xx,pdff,'-r'); 
plot(xx,tpdff,'--b');
title('Fig21 PDF');
legend('Fit Norm.','Truncated Norm.','Location','NorthWest')

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if flag_sample == 1
	run plot_sample2.m
end
if flag_savefig == 1
	figure_file = ['./',case_name,'/Fig_',num2str(i),'_',flag_type]; savefig(figure_file); print('-dtiff',figure_file);	
end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameter 8
%Figure 24 - Radial Radiation Exchange Factors FCELR for PWR
i=8;	
file = './DATA/Fig24.csv'; data=csvread(file);  data = sort(data,1); x = data(:,1);  y = data(:,2);
xx = linspace(0,0.2,300); %For plotting

pdff = pdf(pd(i),xx);
cpd  = cdf(pd(i),xx);
tpdff = pdf(tpd(i),xx);
ctpd = cdf(tpd(i),xx);
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(i);
subplot(N_subplot,1,1);  hold on; grid on; box on; ylim([0 1]); 
plot(x,y,'ro','MarkerSize',5);
plot(xx,cpd,'-r'); 
plot(xx,ctpd,'--b');
line([SOARCA{i} SOARCA{i}], [0 1],'Color','m','LineStyle','-');
line([BEST_EST{i} BEST_EST{i}], [0 1],'Color','g','LineStyle','--');
line([DEFAULT{i} DEFAULT{i}], [0 1],'Color','y','LineStyle','--');

title('Fig24 CDF Radial Radiation Exchange FCELR - PWR Reactor');		
legend('Gauntt Data','Normal','Trunc. Normal','SOARCA','BE','DFT','Location','NorthWest')
subplot(N_subplot,1,2); hold on; grid on; box on;
plot(xx,pdff,'-r'); 
plot(xx,tpdff,'--b');
title('Fig24 PDF');
legend('Normal','Truncated','Location','NorthWest')

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if flag_sample == 1
	run plot_sample2.m
end
if flag_savefig == 1
	figure_file = ['./',case_name,'/Fig_',num2str(i),'_',flag_type]; savefig(figure_file); print('-dtiff',figure_file);	
end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Paameter 9
%Figure 24 - Radial Radiation Exchange Factors FCELR for FPT-1
i=9;	

%file = './DATA/Fig24.csv'; data=csvread(file);  data = sort(data,1); x = data(:,1);  y = data(:,2);
xx = linspace(0,1.0,400); %For plotting

pdff = pdf(pd(i),xx);
cpd  = cdf(pd(i),xx);
tpdff = pdf(tpd(i),xx);
ctpd = cdf(tpd(i),xx);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(i);
subplot(N_subplot,1,1);  hold on; grid on; box on; ylim([0 1]); 

plot(xx,cpd,'-r'); 
plot(xx,ctpd,'--b');

line([SOARCA{i} SOARCA{i}], [0 1],'Color','m','LineStyle','-');
line([BEST_EST{i} BEST_EST{i}], [0 1],'Color','g','LineStyle','--');
line([DEFAULT{i} DEFAULT{i}], [0 1],'Color','y','LineStyle','--');


title('Fig24 CDF Radial Radiation Exchange FCELR - FPT-1 Exp.');		
%legend('Normal','Trunc. Normal','SOARCA','BE','MLC Assesment','Location','NorthWest')
legend('Normal','Trunc. Normal','SOARCA','BE','DFT','Location','NorthWest')
subplot(N_subplot,1,2); hold on; grid on; box on;
plot(xx,pdff,'-r'); 
plot(xx,tpdff,'--b');
title('Fig24 PDF');
legend('Normal','Truncated','Location','NorthWest')

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if flag_sample == 1
	run plot_sample2.m
end
if flag_savefig == 1
	figure_file = ['./',case_name,'/Fig_',num2str(i),'_',flag_type]; savefig(figure_file); print('-dtiff',figure_file);	
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameter 10
%Figure 24 - Axial Radiation Exchange Factors FCELA for both models
i=10;	
%SOARCA = 0.1;  %FCELR %FCELA
file = './DATA/Fig24.csv'; data=csvread(file);  data = sort(data,1); x = data(:,1);  y = data(:,2);
xx = linspace(0,0.2,300); %For plotting

pdff  = pdf(pd(i),xx);
cpd   = cdf(pd(i),xx);
tpdff = pdf(tpd(i),xx);
ctpd  = cdf(tpd(i),xx);
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(i);
subplot(N_subplot,1,1);  hold on; grid on; box on; ylim([0 1]); 
plot(x,y,'ro','MarkerSize',5);
plot(xx,cpd,'-r'); 
plot(xx,ctpd,'--b');
%line([SOARCA{i} SOARCA{i}], [0 1],'Color','m','LineStyle','--');

line([SOARCA{i} SOARCA{i}], [0 1],'Color','m','LineStyle','-');
line([BEST_EST{i} BEST_EST{i}], [0 1],'Color','g','LineStyle','--');
line([DEFAULT{i} DEFAULT{i}], [0 1],'Color','y','LineStyle','--');


title('Fig24 CDF Axial Radiation Exchange - PWR&FPT');		
legend('Gauntt Data','Normal','Trunc. Normal','SOARCA','BE','DFT','Location','NorthWest')
subplot(N_subplot,1,2); hold on; grid on; box on;
plot(xx,pdff,'-r'); 
plot(xx,tpdff,'--b');
title('Fig24 PDF');
legend('Normal','Truncated','Location','NorthWest')


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if flag_sample == 1
	run plot_sample2.m
end
if flag_savefig == 1
	figure_file = ['./',case_name,'/Fig_',num2str(i),'_',flag_type]; savefig(figure_file); print('-dtiff',figure_file);	
end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameter 11
%Figure 23  In-Vessel Falling Debris HTC HDBH2O
i=11;
file = './DATA/Fig23.csv'; data=csvread(file);  data = sort(data,1); x = data(:,1);  y = data(:,2);
xx = linspace(0,2100,500); %For plotting

pdff = pdf(pd(i),xx);
cpd  = cdf(pd(i),xx);

pd1ff = pdf(pd1(i),xx);
cpd1  = cdf(pd1(i),xx);

tpd1ff = pdf(tpd1(i),xx);
ctpd1 = cdf(tpd1(i),xx);
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(i);
subplot(N_subplot,1,1);  hold on; grid on; box on; ylim([0 1]); 
%xlim([100 500])
plot(x,y,'ro','MarkerSize',5);
plot(xx,cpd,'-r'); 
plot(xx,cpd1,'-b');
plot(xx,ctpd1,'--m');

line([SOARCA{i} SOARCA{i}], [0 1],'Color','m','LineStyle','-');
line([BEST_EST{i} BEST_EST{i}], [0 1],'Color','g','LineStyle','--');
line([DEFAULT{i} DEFAULT{i}], [0 1],'Color','y','LineStyle','--');


title('Fig23 CDF In-Vessel Falling HTC');		
legend('Gauntt Data','Fit Tri.','Galushin','Truncated','SOARCA','BE','DFT','Location','NorthEast')

subplot(N_subplot,1,2); hold on; grid on; box on;
plot(xx,pdff,'-r'); 
plot(xx,pd1ff,'-b');
plot(xx,tpd1ff,'--m');
%xlim([100 500])
title('Fig23 PDF');
legend('Tri. Fit Gauntt', 'Galushin','Truncated','Location','NorthEast')

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if flag_sample == 1
	subplot(N_subplot,1,3); hold on; grid on; box on;
	title(['Sample N=', num2str(N)]);
	plot(xx,cpd,'-r');  plot(xx,cpd1,'-b'); plot(xx,ctpd1,'--m');
	xxx = param_select{i,1};
	plot(xxx,cdf(pd_select(i),xxx),'.r','MarkerSize',10);
end	
if flag_savefig == 1
	figure_file = ['./',case_name,'/Fig_',num2str(i),'_',flag_type]; savefig(figure_file); print('-dtiff',figure_file);	
end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



