%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function which creates pdf type object with input being pd type, paramters types and values of paramers
% CREATED BY PIOTR DARNOWSKI, WUT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXAMPLE
%  create_dist('Normal',{'mu','sigma'},{0.0, 10.0 })
% Creates normal distribution with mu= 0 and sigma=10
% Inputs are cell type arrays
% EXAMPLES:
% ALL INPUTS CAN BE CELL
% pd = create_dist('Normal', {'mu' 'sigma'}, [0, 1],'Yes', [0.5 1.0])
% pd = create_dist('Normal', {'mu' 'sigma'}, {0, 1},'Yes', [0.5 1.0])
% pd_ = create_dist({'Normal'}, {'mu' 'sigma'}, {0, 1},{'Yes'}, {0.5 1.0})
% pd = create_dist('Normal', {'mu' 'sigma'}, [0, 1],{}, {})
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function pd_select = create_dist(pdtype,pdparam,pdvalue,istruncate,trlimits)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Check if pdparam has the same size as pdvalue.
if numel(pdparam) ~= numel(pdvalue)
    error('Number of parameters in pdparam has to be equal to pdvalue');
end

% Check if some variables are missing. If true change values
if ismissing(trlimits)
    trlimits = 'NaN';
 end
 if ismissing(istruncate)
     istruncate = 'No';
  end

  if ismissing(pdparam)
    pdparam = 'NaN';
 end

 if ismissing(pdvalue)
    pdvalue = -1.1;
 end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% chech number of arguments
if nargin < 5 
    istruncate = 'No'; 
    trlimits = 'NaN';  
end

% Check if pdvalue is  string array, if yes then convert to numeric
if isstring(pdvalue)
    pdvalue = str2num(pdvalue);
end

if ~iscell(pdparam)
   pdparam = cellstr(pdparam);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ALL INPUTS CAN BE CELL can be cell type so convert to num
if iscell(pdtype) 
    pdtype = cell2mat(pdtype);
 end
if iscell(pdvalue) 
   pdvalue = cell2mat(pdvalue);
end
if iscell(istruncate) 
    istruncate = cell2mat(istruncate);
end
if iscell(trlimits) 
    trlimits = cell2mat(trlimits);
end


%istruncate
%class(istruncate)

%pdparam
%pdvalue
%class(pdvalue)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% check if istrucate is empty
if isempty(istruncate)
    istruncate = 'No'; trlimits = 'NaN'  ;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% check if trlimits is empty
if ~isempty(trlimits)  %is non empty
    if(numel(trlimits)==2)
        LB = trlimits(1); UB = trlimits(2);
        if LB >= UB
            error('Lower boundary larger or equal to  upper boundary');
        end
    else
        warning(['trlmits for is not empty but has number of fields other than 2 ']);    
    end    
else
    istruncate = 'No'; trlimits = 'NaN';    
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% convert vector to cell
pdvalue2 = num2cell(pdvalue);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% stack cells
    pd_params = [pdparam; pdvalue2];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% Create distribution
pd = makedist(pdtype,  pd_params{:});
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MAKE TRUNCATE
switch istruncate
    case {'Yes', 'yes', '1', 'YES'}

        if isempty(trlimits)
            error('Truncated PDF but limits not provided');
        end

        pd_select =  truncate(pd,LB,UB);
    otherwise
        pd_select = pd;    
end 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LEGACY CODE:
% OLD prototype
%pd_select(i) = makedist(param(i).pdtype,'a',param(i).pdprop(1) ,'b',param(i).pdprop(2),'c',param(i).pdprop(3));   %  makedist('Triangular','a',a,'b',b,'c',c);


%tpd(i)  = truncate(pd(i),LB,UB);     % Truncated PDF

%pd_select(i) =  tpd(i);