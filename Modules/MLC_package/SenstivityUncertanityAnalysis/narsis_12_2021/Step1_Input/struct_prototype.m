%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STRUCTURE PROTOTYPE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INITIATE EMPTY STR - TO PRE-DEFINE ALL FIELDS - OTHERWISE CAN CREATE PROBLEMS
i=1;
param(i).num            = i;        % number of the parameter
param(i).name           = '';       % name 
param(i).fullname       = '';       % full name - long name 
param(i).descrip        = '';       % description
param(i).comment        = '';       % comment
param(i).note           = {''};     % notes
param(i).unit           = '';       % units
param(i).SOARCA         = [];       % SOARCA value, some value used in plotting 
param(i).MELCOR         = [];       % M2.2. Default Value, some value used in plotting
param(i).BEST           = [];       % Selected Best Estimate Value, some value used in plotting
param(i).special        = '';       % Special type, info about special type, has no effect
param(i).whyspec        = '';       % Comment why special, info has no effect
param(i).type           = '';       % Type of PDF 'Continous' or 'Disctete' or 'Equal' 
param(i).pdtype          = '';      % Type of PDF - which distribution 'Normal' 'Lognormal' etc. like in MATLAB. For Discrete we can sue PiecewiseLinear 
param(i).pdparam         = {''};    % Parameters of the distribution as used by makedist MATLAB function, e.g. mu, sigma, etc. Example: {'mu', 'sigma'}, for continous use cell, for discrete you can use matrix e.g. [1 2 3] or cell with strings
param(i).pdvalue         = [];      % Value of pdparam, in proper order. Example:  [0.0, 1.0] 
param(i).istruncate      = '';      % is PDF truncated ? 'Yes'   ;   'No' or [] no effect 
param(i).trlimits        = [];      % Lower and upper boundary of trunction  [0.0 5.0]
param(i).add_data         = [];     % additonal data - dedicated for discrete distributions. For discrete you can provide more details what is selected. E.g. Values of parameters etc. See Example
param(i).plotrange       =  [];      % range of parameters values for plots, works only if continous. For plotting purposes you have to define max and mix values
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%