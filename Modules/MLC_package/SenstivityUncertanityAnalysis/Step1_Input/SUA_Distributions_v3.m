%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SCRIPT DEDICATED TO DEFINE DISTRIBUTIONS OF UNCERTANITY PARAMETERS 
% DEVELOPED BY PIOTR DARNOWSKI FOR NARSIS PROJECT 2021 - ALL RIGHTS RESEVERD - SEE LICENSE PIOTR.DARNOWSKI@PW.EDU.PL, WARSAW UNIVERSITY OF TECHNOLOGY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% UNDER CONSTRUCTION - at this moment it is still not ready
%error('Under construction!')
% v2 = version 2 based on strucutres modified !
% version 1 without strucutres used for parameters  Distributions definition for matlab file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% USER CAN DEFINE DISTRIBUTIIN BELOW OR PROVIDE XML FILE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% READ FROM MATLAB INPUT AND SAVE STRUCUTRE
if flag_read_dist  == 0
    % READ MATLAB-TYPE INPUT FILE
    run struct_prototype.m 
    run INPUT_SUA_Distributions_v2.m 
    %write all param to single strucutre S, necesarry for writestruct to work
        S.param(:) = param(:);  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% READ STRUCUTRE
elseif flag_read_dist == 1
     
    %run struct_prototype.m
    % READ XML FILE 
    % S = readstruct("music.txt","FileType","xml")
    % allows to read and save distributions for parameters as structures
    % if read and 1st does not have all expected fields, it can cause problems
    % In case of problems first entry in the xml should contain all field with at least empty value
    S = readstruct([filename_input_parameters]);
    % Create separate param - array of strcutures - to simplfty our life
    for i=1:numel(S.param)
        param(i) = S.param(i);
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
else
    error('Unknown flag_read_dist');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SAVE ALL PARAMETERS TO XML FILE - IT IS A COPY OR NEW FILE
xml_file = ['Saved_', case_name, filename_input_parameters];
writestruct(S,xml_file);




nParams = numel(param);

% writestruct does not work is pdparam field is cell type with values
% it can be cell array with charcters only or matrix with numbers, it do nto know why, but it work this way
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE DISTRIBUTIONS
for i = 1:nParams 
    i
    
    if strcmp(param(i).type,'Continous')           
        pd_select(i) = create_dist(param(i).pdtype, param(i).pdparam, param(i).pdvalue,param(i).istruncate, param(i).trlimits);
    end
    if strcmp(param(i).type,'Discrete')
        pd_select(i) =  create_discrete_dist(param(i).pdtype,param(i).pdparam ,param(i).pdvalue);
    end
end
for i = 1:nParams 
    if strcmp(param(i).type,'Equal')
       % pd_select(i) = pd_select(param(i).pdtype); 
       pd_select(i) = pd_select(param(i).pdparam); 
    end    
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

