%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SCRIPT DECICATED TO FIT DISTIRUBTIONS, VISUALIZE DISTRIBUTIONS AND SAMPLES, COMPARE WITH OTHER DATA
% OLD PLOTTING WITH FITTING OF GAUNT DISTRIBUTIONS IS A PART OF VERSION 1 IN FOLDER v1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DEVELOPED BY PIOTR DARNOWSKI FOR NARSIS PROJECT 2021 - ALL RIGHTS RESEVERD - SEE LICENSE PIOTR.DARNOWSKI@PW.EDU.PL, WARSAW UNIVERSITY OF TECHNOLOGY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load X-Core package
% addpath(genpath('D:\NARSIS_DOCS\MODELE_AND_RESEARCH\SCRIPTS\MATLAB_XCore\x-core'));  % Add X-Core - Matlab Nuc. Eng. Material Databse
% Data generated with https://apps.automeris.io/wpd/
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% clear; clc; close all; format short g; %format long g;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
set(0,'defaulttextInterpreter','none')  %LATEX AXIS LABELS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NUMBERING OF ELEMENTS IN TABLE TYPE CAN BE DIFFERENT THAN PARAM_SELECT OR PARAM !!
% BECOUSE WE CAN HAVE EMPTY CELLS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%T2
%T1(1:2,:)
%Tbest
%T1.Properties.VariableNames
%T1.Rows
% name_numbers
% name
% param_data
% names_data
% param(i).type
% T = param_data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLOT SAMPLES AS SUBPLOT OF HISTOGRAMS - Alternative to use param_select  - 3 same
if flag_save ==1			% WORK ONLY IF ONLY IF FLAG_SAVE =1 
fig0 = plot_histogram_matrix('a1',param_data,case_name,flag_savefig,4,name);  %ALTERNATIVE 
%fig2 = plot_histogram_matrix('a2',T1,case_name,flag_savefig,4);				%THESMAE
%fig3 = plot_histogram_matrix('a2',param_data(:,1),case_name,flag_savefig);				%THESMAE
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLOT PDF AND CDF FUNCTIONS
fig1=plot_pdf_matrix('',param,param_select,pd_select,case_name,'pdf','subplot');
fig2=plot_pdf_matrix('',param,param_select,pd_select,case_name,'cdf','subplot');
fig3=plot_pdf_matrix('',param,param_select,pd_select,case_name,'pdf','normal');
fig4=plot_pdf_matrix('',param,param_select,pd_select,case_name,'cdf','normal');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLOT OF CDFs and PDFs and SINGLE PLOT

%i=5
%plot_pdf_matrix('add3',param(i),param_select(i),pd_select(i),case_name,'pdf','normal')
%plot_pdf_matrix('add3',param(i),param_select(i),pd_select(i),case_name,'cdf','normal')
%i=6
%plot_pdf_matrix('add3',param(i),param_select(i),pd_select(i),case_name,'pdf','normal')
%plot_pdf_matrix('add3',param(i),param_select(i),pd_select(i),case_name,'cdf','normal')