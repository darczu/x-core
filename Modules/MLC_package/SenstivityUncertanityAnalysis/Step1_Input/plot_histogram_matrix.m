%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FUNCTION TO PLOT HISTOGRAMS IN SUBPLOT OF A TABLE TYPE OR MATRIX DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T - can be matrix or table
% Matrix Rows: cases  Columns: subsequent parameters sampled
% folder can be string or []
% flag_savefing can be 1,0,[]
% nFigs number or []
% name can be vector of numbers or arrays of cells
% Example:
% plot_histogram_matrix(T11)
% plot_histogram_matrix(T11,[],[],[],[])
% plot_histogram_matrix(T1,case_name,flag_savefig,4);
% plot_histogram_matrix('a2',param_data(:,1),case_name,flag_savefig)
%
%fig1 = plot_histogram_matrix('a1',param_data,case_name,flag_savefig,4,name);  %ALTERNATIVE
%fig2 = plot_histogram_matrix('a2',T1,case_name,flag_savefig,4);				%THESMAE
%fig3 = plot_histogram_matrix('a2',param_data(:,1),case_name,flag_savefig);				%THESMAE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function fig=plot_histogram_matrix(no,T,folder,flag_savefig,nFigs,name)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
set(0,'defaulttextInterpreter','none')  % TURN OFF TEX INTERPETER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NUMBER OF ROWS AND COLUMNS
nRow = size(T,1);
nCol = size(T,2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CHECK IF THERE IS SMALLER NUMBER OF INPUT ARGMENTS
if nargin < 3   folder = '.';      end
if isempty(folder)       folder = '.';           end
if nargin < 4   flag_savefig = 1;   end
if nargin < 5 
    if  nCol < 4
        nFigs = nCol;
    else     
        nFigs = 4 ;         
    end
end
% IF name IS NOT GIVEN USE SUBSEQUENT NUMBERS AS NAMES OR IF IT IS ASO TABLE USE TABLE NAMES
if (nargin < 6)    
    % CHECK IF TABLE
    if istable(T)    
        name = [T.Properties.VariableNames];
    else
            name = 1:nCol;         
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CHECK IF INPUT IS EMPTY
if isempty(nFigs)        nFigs = 4;             end
if isempty(no)           no = 1;                end
if isempty(flag_savefig) flag_savefig = 1;      end
if isempty(folder)       folder = '';           end
if isempty(name)         name = 1:nCol;         end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GENERATE FIGURE
fig=figure; grid on; box on;
nFigRow = ceil(nCol/nFigs);
subplot(nFigRow,nFigs,1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% WINDOW STATE
%fig.Position =   [90, 90, 1600, 800];
fig.WindowState = 'maximized';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i = 1:nCol
    subplot(nFigRow,nFigs,i);

% pure data size(T1.Variables)
    if istable(T) == 1
        histogram(T.(i));
        %xlabel([T.Properties.VariableNames(i)]);
        xlabel([name(i)]);
    else
        histogram(T(:,i));

        if isnumeric(name)
            xlabel(['#', num2str(name(i))]);
        else
            xlabel(name{i});
        end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MAIN TITLE
sgtitle(['Histogram ',num2str(no),', Samples = ', num2str(nRow)]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SAVE TO TIFF AND FIG
if flag_savefig == 1
    figure_file = [folder,'/','Samples_Histogram_', num2str(no)]
    savefig(figure_file); 
    print('-dtiff',figure_file);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
