%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SCRIPT FOR PLOTTING OF SUBPLOT WITH ALL CDFs OR PDFs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% S - proper structure with data
% No                - number
% plot_type              - 'pdf' or 'cdf' or 'both'
% flag_savefig      - save fig/tiff
% nFigs             - number of columns in figure
% add_data - aditional data
% folder - save folder name
%  https://uk.mathworks.com/matlabcentral/answers/451708-use-a-structure-as-input-in-a-function
%
% plot_pdf_matrix(1,param,param_select,pd_select,'.','pdf','subplot')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function fig=plot_pdf_matrix(no,param,param_select,pd_select,folder,plot_type,plot_form,plot_sample,plot_default)
    %PROTOTYPE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
set(0,'defaulttextInterpreter','none')  % TURN OFF TEX INTERPETER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nRow = size(param_select{1},1);  %   number of samples
nCol = numel(param);  % number of parameters, size of the structure
 
if nargin < 6          plot_type ='pdf';        end   
if nargin < 7          plot_form ='subplot';    end   % or 'normal'
if nargin < 8          plot_sample = 'yes' ;    end   
if nargin < 5          folder = '.';           end
if isempty(folder)     folder = '.';           end

if strcmp(plot_type,'cdf')   
    plot_default = 'yes';
else
    plot_default = 'yes';
end    

nFigs = 4;
flag_savefig =1;

figure_file = [folder,'/',num2str(no),'_',plot_type,'_',plot_form,'_'];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GENERATE FIGURE
%fig(1)=figure; clf; grid on; box on;
%figure; clf; grid on; box on;
nFigRow = ceil(nCol/nFigs);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% WINDOW STATE
%fig.Position =   [90, 90, 1600, 800];
if strcmp(plot_form,'subplot')
    fig=figure; clf; grid on; box on;
    subplot(nFigRow,nFigs,1);
    fig.WindowState = 'maximized';
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
j = 1; %counter to take into account empty fields
for i = 1:nCol
    S = param(i);
    
    % Continous and Equal
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
% PLOT PDF, CDF ETC....   
    if(strcmp(S.type,'Continous') | strcmp(S.type,'Equal') | strcmp(S.type,'Discrete'))

            if isempty(S.plotrange) error('plotrange field cannot be empty'); end


        if strcmp(plot_form,'subplot')
            subplot(nFigRow,nFigs,j);     hold on; grid on; box on; 
            fig.WindowState = 'maximized';
        elseif strcmp(plot_form,'normal')
            fig(i)=figure; clf; hold on; grid on; box on;  
        end    

        

            xx = linspace(S.plotrange(1),S.plotrange(2),500);   
            file = S.verif_file;
            if ((~isempty(file)) & ~ismissing(file))
                data = csvread(file); 
                data = sort(data,1); x = data(:,1); y = data(:,2);
            end

            if(plot_type == 'pdf')
                pdff1 = pdf(pd_select(i),xx); plot(xx,pdff1,'-b'); lgd = {[S.pdtype]};
                
                if(strcmp(S.type,'Continous') | strcmp(S.type,'Equal')) 
                    xxx= param_select{i,1};  plot(xxx,pdf(pd_select(i),xxx),'.r','MarkerSize',7);   lgd = [lgd, ['Sample']]; 
                end

            elseif(plot_type == 'cdf')
                cpd1  = cdf(pd_select(i),xx); plot(xx,cpd1,'-b');  lgd = {[S.pdtype]};

                if(strcmp(S.type,'Continous') | strcmp(S.type,'Equal')) 
                    xxx= param_select{i,1};  plot(xxx,cdf(pd_select(i),xxx),'.r','MarkerSize',7);   lgd = [lgd, ['Sample']]; 
                end    
            end
        
        if(strcmp(S.type,'Continous') | strcmp(S.type,'Equal')) 
            if strcmp(plot_default,'yes')
                
                %isempty(S.SOARCA)
                %ismissing(S.SOARCA)

                if ( (~isempty(S.SOARCA)) & (~ismissing(S.SOARCA)))    xline(S.SOARCA,'Color','m','LineStyle','-','LineWidth',1.0);  lgd = [lgd, ['SOARCA']]; end
                if ( (~isempty(S.BEST)) & (~ismissing(S.BEST)))       xline(S.BEST,'Color','k','LineStyle','--','LineWidth',1.0);     lgd = [lgd, ['Best']]; end
                if ( (~isempty(S.MELCOR)) & (~ismissing(S.MELCOR)))     xline(S.MELCOR,'Color','c','LineStyle',':','LineWidth',1.0);  lgd = [lgd, ['Default']]; end

                    %line([S.SOARCA, S.SOARCA], [0, inf],'Color','m','LineStyle','-');
                    %  line([S.BEST, S.BEST], [0, inf],'Color','k','LineStyle','--'); 
                    % line([S.MELCOR, S.MELCOR], [0, inf],'Color','c','LineStyle',':');   
            end
        end

        if(strcmp(S.type,'Discrete'))

            if(plot_type == 'pdf')
                histogram(param_select{i,1},'Normalization','probability'); hold on; lgd = [lgd, ['Sample pdf']];
                bar(S.pdparam,S.pdvalue, 'FaceAlpha',0.5);  lgd = [lgd, ['PDF']];
            end
            if(plot_type == 'cdf')
                ecdf(param_select{i,1});  lgd = [lgd, ['Sample ecdf']];
            end
            %histogram(S.pdvalue,'Normalization','probability','BinWidth',1); 
                
        end
            
        
        
        legend(lgd,'FontSize',5);

   %     if isstring(S.num)
            xlabel(char(['#', char(num2str(S.num)),' ', char(S.name)]));
  %      else
 %           xlabel(['#', num2str(S.num),' ', S.name]);
%        end    

        j = j+1;

            % SAVE
            if (strcmp(plot_form,'normal')  & flag_savefig == 1)
                %savefig([figure_file, num2str(i)]); 
                savefig([figure_file, num2str(S.num)]); 
                print('-dtiff', [figure_file, num2str(S.num)]); 
                %print('-dtiff', [figure_file, num2str(i)]); 
            end
    end        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
% MAIN TITLE   
sgtitle([plot_type,num2str(no),', Samples = ', num2str(nRow)]);

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SAVE MAIN FIGURE TO TIFF AND FIG
    if (strcmp(plot_form,'subplot')  & (flag_savefig == 1))
        savefig([figure_file, 'sub']); 
        print('-dtiff',[figure_file, 'sub']);
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%