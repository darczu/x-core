%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MATLAB TYPE INPUT FILE FOR SUA_Distributions_v2
% IT CONTAINMENT DEFINITION OF STRUCUTRES WITH UNCERTANITY PARAMETERS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DEVELOPED BY PIOTR DARNOWSKI FOR NARSIS PROJECT 2021 - ALL RIGHTS RESEVERD - SEE LICENSE PIOTR.DARNOWSKI@PW.EDU.PL, WARSAW UNIVERSITY OF TECHNOLOGY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DEVELOPMENT
% STRUCUTRE PROTOTYPE IN struct_prototype.m file % IN FACT IT IS BETTER TO USE CLASSES. BUT MAYBE IN THE FUTURE
% https://uk.mathworks.com/help/stats/makedist.html % SEE https://uk.mathworks.com/help/stats/prob.uniformdistribution.html
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DEFINE CONTINOUS DISTRIBUTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = 1;
param(i).num            = i;
param(i).name           = 'SC1131(2)';
param(i).fullname = param(i).name;
param(i).descrip        = 'SC1131(2): Zr melt breakout temperature';
param(i).comment        = 'Based on PB SOARCA UA (SNL, 2015)';
param(i).note           = {'Triangular based on PB SOARCA UA; Molten Material Holdup Parameters' ...
                           ' Zr Melt Release; Parameter 1 % In Gauntt it is Figure 15 - Zr Melt Release % SC1131(2): Zircaloy melt breakout temperature (k))'};
param(i).unit           = '[K]';
param(i).SOARCA         =  2400;  % PB Uncertanity SOARCA default value
param(i).MELCOR         =  2400;  % code default
param(i).BEST           =  2400;  % best estimate
param(i).type           = 'Continous';
param(i).pdtype          =  'Triangular';
param(i).pdparam         =  {'a', 'b', 'c'};   
param(i).pdvalue         =  [2100, 2400, 2550];  %a, b, c
param(i).plotrange       = [2050,2700];
param(i).verif_file         = './DATA/Fig15.csv';   
% Create distribution
% pd_select(i) = create_dist(param(i).pdtype, param(i).pdparam, param(i).pdvalue,param(i).istruncate, param(i).trlimits);

%a	Lower limit	a≤b %b	Peak location	a≤b≤c %c	Upper limit	c≥b %a = 2100; b = 2400; c = 2550;  %a,c defined. b - found % b - is the mode of the distribution  
%param(i).pdvalue        =  {2100, 2400, 2550};  %a, b, c, writing does not work when it is the cell
%mean = (a+b+c)/3 %%mu =2400.0; sigma = 50.;  %Found manually %pd(i) = makedist('Normal',mu,sigma); % Normal fitted to Data in Gauntt H2 %OLD
%% =======================================================================
i = 2;
param(i).num            = i;
param(i).name           = 'SC1132(1)';
param(i).fullname = param(i).name;
param(i).descrip        = 'SC1132(1): Fuel Rod Failure/Collapse Temp';
param(i).comment        = 'Based on SOARCA UA (SNL, 2015)';   
param(i).note           = {'Gauntt data Figure 16 Core (Fuel) Component Failure Parameters' ...
                    	 'Fuel Rod Failure/Collapse temp	SC1132(1)  For INT model it should be equal to melting temperature'};
param(i).unit           = '[K]';
param(i).SOARCA         =  2800;  % SOARCA,
param(i).MELCOR         =  2500;  % code default
param(i).BEST           =  2500;  % best estimate
param(i).type           = 'Continous';
param(i).special        = 'No';             % Change to take into account other possiblities
param(i).pdtype         =  'Normal';
param(i).pdparam        =  {'mu' 'sigma'};   
param(i).plotrange       = [2200,2900];
param(i).verif_file         = './DATA/Fig16.csv'; 
% Define parameters
mu 		= 2479.0;  sigma = 83.;
param(i).pdvalue        =  [mu, sigma];  %a, b, c
% Create distribution
% pd_select(i) = create_dist(param(i).pdtype, param(i).pdparam, param(i).pdvalue,param(i).istruncate, param(i).trlimits);

%% Normal fitted to Data in Gauntt H2
%mu 		= 2575.0;  sigma = 70.;
%pd(i) = makedist('Normal','mu',mu,'sigma',sigma);
%% Normal from SOARCA SEQ
%mu 		= 2479.0;  sigma = 83.;
%pd1(i) 	= makedist('Normal','mu',mu,'sigma',sigma);
%% Revised for TMELT - based on SOARCA Surry UA. Effective eutectic reaction temperature %% In new model it is the same as TEUTECTIC == SC1132(1)
%% =======================================================================
% Parameter 14  
i = 14;
param(i).num            = i;
param(i).name           = 'TMLT';  %former TEUT
param(i).fullname = param(i).name;
param(i).descrip        = 'Eutectic Temperature for INT model, earlier TEUT Equal to SC1132(1)';
param(i).comment        = 'Based on SOARCA UA (SNL, 2015)';   
param(i).note           = {'For INT model it should be equal to melting temperature and SC1132(1)' ...
                            'In new model it is the same as TEUTECTIC == SC1132(1)' ...
                            'Revised for TMLT - based on SOARCA Surry UA. Effective eutectic reaction temperature'};
param(i).unit           = '[K]';
param(i).SOARCA         =  2800;  % SOARCA,
param(i).MELCOR         =  2500;  % code default
param(i).BEST           =  2500;  % best estimate
param(i).type           = 'Equal';
param(i).pdtype         = 'Normal';
param(i).pdparam          = 2;
param(i).special        = 'Yes'; 
param(i).whyspec        = 'Equal to param 2 SC1132(1)';             % Change to take into account other possiblities
param(i).plotrange       = [2200,2900];
% Other parameters not defined  
%param(i).pdparam        =  {'parameter'}        %Which paramter equal to
%param(i).pdvalue        =  2                     %Which paramter equal to

% Create distribution
%pd_select(i)            =  pd_select(2);

%% =======================================================================
% Parameter 3
i = 3;
param(i).num            = i;
param(i).name           = 'FUOZR';
param(i).fullname = param(i).name;
param(i).descrip        = 'COR_CMT(5) Secondary Material Transport Parameters';
param(i).comment        = 'Based on (Gantt,2004) Figure 17 ';    
param(i).note           = {'FUOZR	Secondary Material Transport Parameters	Secondary UO2 Content, dissolved UO2'};
param(i).unit           = '[-]';
param(i).SOARCA         =  [];  % SOARCA,
param(i).MELCOR         = 0.2;  % code default
param(i).BEST           = 0.2;  % best estimate
param(i).type           = 'Continous';
param(i).special        = 'No';             % Change to take into account other possiblities
param(i).istruncate      = 'Yes';            % is truncated ?
param(i).pdtype         =  'Normal';
param(i).pdparam        =  {'mu' 'sigma'};   
param(i).plotrange       = [0,1.0];
param(i).verif_file         = './DATA/Fig17.csv'; 
% Define parameters
mu =0.2;  sigma = 0.09;  LB =0.0; UB = 0.5; 
% has to be <0.0, 1.0>
param(i).pdvalue        =  [mu, sigma];  %a, b, c
param(i).trlimits        =  [LB, UB];  %Lower and upper boundary


% Create distribution 
% pd_select(i) = create_dist(param(i).pdtype, param(i).pdparam, param(i).pdvalue,param(i).istruncate, param(i).trlimits);
%% =======================================================================
i = 16;
param(i).num            = i;
param(i).name           = 'FSXSS';
param(i).fullname = param(i).name;
param(i).descrip        = 'COR_CMT(7) Secondary Material Transport Parameters';
param(i).comment        = 'Based on (Itoh, 2014)';    
param(i).note           = {'Transport parameter for steel oxide in molten steel'};
param(i).unit           = '[-]';
param(i).SOARCA         =  [];  % SOARCA,
param(i).MELCOR         = 1.0;  % code default
param(i).BEST           = 1.0;  % best estimate
param(i).type           = 'Continous';
param(i).special        = 'No';             % Change to take into account other possiblities
param(i).istruncate      = 'Yes';            % is truncated ?
%param(i).istruncate      = 'No';            % is truncated ?
param(i).pdtype         =  'Normal';
param(i).pdparam        =  {'mu' 'sigma'};
param(i).plotrange       = [0, 1.1];


% Define parameters
% has to be <0.0, 1.0>
mu =1.0;  sigma = 0.6/3; LB =0.6; UB = 1.0;    % Assumed that 0.6 is 3*sigma far from 1.0
param(i).pdvalue        =  [mu, sigma];  %a, b, c
param(i).trlimits        =  [LB, UB];  %Lower and upper boundary

%param(i).pdtype         =  'Uniform';
%param(i).pdparam        =  {'Lower' 'Upper'};    
%param(i).pdvalue        =  [LB], UB];  %a, b, c

% Create distribution 
% pd_select(i) = create_dist(param(i).pdtype, param(i).pdparam, param(i).pdvalue,param(i).istruncate, param(i).trlimits);
%% =======================================================================
% Paramter 17
i = 17;
param(i).num            = i;
param(i).name           = 'SC1141(2)';
param(i).fullname = param(i).name;
param(i).descrip        = 'GAMBRK - Core Melt Breakthrough Candling Parameters: Maximum melt flow rate per unit width after breakthrought';
param(i).comment        = 'Based on (Galushin, 2020) and (SOARCA-PB,2015)';    
param(i).note           = {'Molten cladding/pool drainage rate (SC1141-2), in SOARCA 0.2 was based on CORA exp'};
param(i).unit           = '[kg/m-s]';
param(i).SOARCA         = 0.2;  % SOARCA,
param(i).MELCOR         = 1.0;  % code default
param(i).BEST           = 1.0;  % best estimate
param(i).type           = 'Continous';
param(i).special        = 'No';         
param(i).istruncate      = 'No';        
param(i).pdtype         =  'Triangular';
param(i).pdparam        =  {'a' 'b' 'c'};
param(i).plotrange       = [0, 2];
% Define parameters
LB =0.1; UB = 2.0; mode = 1.0;   
param(i).pdvalue        =  [LB, mode, UB];  %a, b, c
% Create distribution 
% pd_select(i) = create_dist(param(i).pdtype, param(i).pdparam, param(i).pdvalue,param(i).istruncate, param(i).trlimits);
%% =======================================================================
% Paramter 4
i = 4;
param(i).num            = i;
param(i).name           = 'HFRZZR';
param(i).fullname = param(i).name;
param(i).descrip        = 'COR_CHT(1) Zr Freezing Coeff';
param(i).comment        = 'Based on (Gauntt, 2004) Figure 18, old COR00005 ';    
param(i).note           = {'Refreezing heat transfer coefficient for Zircaloy' ...
'GCandling Heat Transfer Coefficients Zr Freezing Coeff COR_CHT(1)'};
param(i).unit           = '[W/m2-K]';
param(i).SOARCA         = 7500;  % SOARCA,
param(i).MELCOR         = 7500;  % code default
param(i).BEST           = 7500;  % best estimate
param(i).type           = 'Continous';
param(i).special        = 'No';         
param(i).istruncate      = 'Yes';        
param(i).pdtype         =  'Lognormal';
param(i).pdparam        =  {'mu' 'sigma'};
param(i).plotrange       = [0, 25000];
param(i).verif_file         = './DATA/Fig18.csv'; 
% Define parameters
UB = 22000; LB=2000; %LB=1000
%LOG-Normal fitted to Gauntt Data
mu = log(7500.0)  ;% =   8.9227  %7500 ? can also be 8000
sigma = log(1.75) ;% =  0.55962
median = exp(mu);
param(i).pdvalue        =  [mu, sigma];  %a, b, c
param(i).trlimits        =  [LB, UB];  %Lower and upper boundary
% Create distribution 
% pd_select(i) = create_dist(param(i).pdtype, param(i).pdparam, param(i).pdvalue,param(i).istruncate, param(i).trlimits);
% Alternative
%% =======================================================================
% Parameter 15
i = 15;
param(i).num            = i;
param(i).name           = 'HFRZSS';
param(i).fullname = param(i).name;
param(i).descrip        = 'COR_CHT(4) SS Freezing Coeff';
param(i).comment        = 'Based on (Galushin, 2020), (Itoh, 2014) ';    
param(i).note           = {'Refreezing heat transfer coefficient for Steel' };
param(i).unit           = '[W/m2-K]';
param(i).SOARCA         = 2500;  % SOARCA,
param(i).MELCOR         = 2500;  % code default
param(i).BEST           = 2500;  % best estimate
param(i).type           = 'Continous';
param(i).special        = 'No';         
param(i).istruncate      = 'Yes';        
param(i).pdtype         =  'Lognormal';
param(i).pdparam        =  {'mu' 'sigma'};
param(i).plotrange       = [0, 5000];
% Define parameters
LB = 1000; UB=5000; %LB=1000  % 5000 is assumed
mu = log(2500); %median value
%sigma = log(1.75) ;  %To be fitted  ***REVISE***
sigma = log(1.5) ;  %To be fitted  ***REVISE***
median = exp(mu);
param(i).pdvalue        =  [mu, sigma];  %a, b, c
param(i).trlimits        =  [LB, UB];  %Lower and upper boundary
% Create distribution 
% pd_select(i) = create_dist(param(i).pdtype, param(i).pdparam, param(i).pdvalue,param(i).istruncate, param(i).trlimits);
%% =======================================================================
% Parameter 5
i = 5;
param(i).num            = i;
param(i).name           = 'DHYPD';
param(i).fullname = param(i).name;
param(i).descrip        = 'COR_EDR Core-Region Particulate Debris Diameter';
param(i).comment        = 'Based on  (Gauntt, 2004) Figure 19/20';    
param(i).note           = {'FARO based 2mm' };
param(i).unit           = '[m]';
param(i).SOARCA         = 0.01;  % SOARCA,2 mm
param(i).MELCOR         = [];  % code default
param(i).BEST           = 0.01;  % best estimate
param(i).type           = 'Continous';
param(i).special        = 'No';         
param(i).istruncate      = 'Yes';        
param(i).pdtype         =  'Lognormal';
param(i).pdparam        =  {'mu' 'sigma'};
param(i).plotrange       = [0, 0.05];
param(i).verif_file         = './DATA/Fig19.csv'; 
% Define parameters
LB = 0.002; UB=0.05; %
mu = log(0.012); %median value based on Gauntt
%sigma = log(1.8);  %To be fitted  fitted
%sigma = log(1.5);  %To be fitted  fitted
sigma = log(2.0);  %To be fitted  fitted
median = exp(mu);
param(i).pdvalue        =  [mu, sigma];  %a, b, c
param(i).trlimits        =  [LB, UB];  %Lower and upper boundary
% Create distribution 
% pd_select(i) = create_dist(param(i).pdtype, param(i).pdparam, param(i).pdvalue,param(i).istruncate, param(i).trlimits);
%mu = 0.002
%sigma = 0.014
%pd = makedist('HalfNormal',mu,sigma);
%a = 0.002;
%b = 0.0025;
%c = 0.04;
%pd = makedist('Triangular','a',a,'b',b,'c',c);
%% =======================================================================
% Parameter 6
i = 6;
param(i).num            = i;
param(i).name           = 'DHYPDLP';
param(i).fullname = param(i).name;
param(i).descrip        = 'COR_EDR Lower Plenum Debris Diamter';
param(i).comment        = 'Based on  (Gauntt, 2004a,b) Figure 20, also (Galushin, 2020), etc.';    
param(i).note           = {'Gauntt Figure 19/20 Not so obvious. Modfied Gauntt with lowered limit nad median.' ...
                           'Ditribution type as in Gaunt, Lower limit as in Galushin, Upper limit as Gauntt' ...
                           'Median based on upper limit in Galushin, as having experimental evidence' ...
};
param(i).unit           = '[m]';
param(i).SOARCA         = 0.002;  % SOARCA,2 mm
param(i).MELCOR         = [];  % code default
param(i).BEST           = 0.002;  % best estimate
param(i).type           = 'Continous';
param(i).special        = 'No';         
param(i).istruncate      = 'Yes';        
param(i).pdtype         =  'Lognormal';
param(i).pdparam        =  {'mu' 'sigma'};
param(i).plotrange       = [0, 0.06];
param(i).verif_file         = './DATA/Fig20.csv'; 
% Define parameters
LB = 0.002; UB=0.06; %
mu = log(0.005); %median value based on Gauntt
%sigma = log(1.55);  %To be fitted  fitted
sigma = log(3);  %To be fitted  fitted
median = exp(mu);
param(i).pdvalue        =  [mu, sigma];  %a, b, c
param(i).trlimits        =  [LB, UB];  %Lower and upper boundary

% Create distribution 
% pd_select(i) = create_dist(param(i).pdtype, param(i).pdparam, param(i).pdvalue,param(i).istruncate, param(i).trlimits);
%mu = 0.01
%sigma = 0.019
%pd = makedist('HalfNormal',mu,sigma);
%a = 0.01;
%b = 0.011;
%c = 0.06;
%pd = makedist('Triangular','a',a,'b',b,'c',c);
%% =======================================================================
% Parameter 7
i = 7;
param(i).num            = i;
param(i).name           = 'PORDP';
param(i).fullname = param(i).name;
param(i).descrip        = 'Debris porosity (PORDP)';
param(i).comment        = 'Based on  (Gauntt, 2004a,b)';    
param(i).note           = {'Gauntt Figure 21, in Galushin it is 0.3-0.5'};
param(i).unit           = '[-]';
param(i).SOARCA         = 0.4;  % SOARCA
param(i).MELCOR         = 0.4;  % code default
param(i).BEST           = 0.4;  % best estimate
param(i).type           = 'Continous';
param(i).special        = 'No';         
param(i).istruncate      = 'Yes';        
param(i).pdtype         =  'Normal';
param(i).pdparam        =  {'mu' 'sigma'};
param(i).plotrange       = [0, 0.5];
param(i).verif_file         = './DATA/Fig21.csv'; 
% Define parameters
LB = 0.1; UB=0.5; %
mu = 0.38; 
sigma = 0.1;  
median = exp(mu);
param(i).pdvalue        =  [mu, sigma];  %a, b, c
param(i).trlimits        =  [LB, UB];  %Lower and upper boundary
% Create distribution 
% pd_select(i) = create_dist(param(i).pdtype, param(i).pdparam, param(i).pdvalue,param(i).istruncate, param(i).trlimits);
%% =======================================================================
% Parameter 8 Radial PWR 
if flag_type == 'PWR'
    i = 8;
    param(i).num            = i;
    param(i).name           = 'FCELR';
    param(i).fullname = param(i).name;
    param(i).descrip        = 'COR_RF card FCELR field - Radial Radiation Exchange Factors FCELR for PWR NPP';
    param(i).comment        = 'Based on  (Gauntt, 2004a,b) Gauntt Figure 24 ';    
    param(i).note           = {'Radial Radiation Exchange Factors FCELR for PWR is lower than for experiments like FPT' ...
                                'old default was 0.25'};
    param(i).unit           = '[-]';
    param(i).SOARCA         = 0.1;  % SOARCA
    param(i).MELCOR         = 0.1;  % code default
    param(i).BEST           = 0.1;  % best estimate for PWR
    param(i).type           = 'Continous';
    param(i).special        = 'Yes';
    param(i).whyspec        = 'Depends on type of facility, see flag_type';                  
    param(i).istruncate      = 'Yes';        
    param(i).pdtype         =  'Normal';
    param(i).pdparam        =  {'mu' 'sigma'};
    param(i).plotrange       = [0, 0.3];
    param(i).verif_file         = './DATA/Fig24.csv'; 
    % Define parameters
    mu =0.1; 
    sigma = 0.035;
    % has to be <0.0, 1.0>
    LB=0.02;  UB = 0.3;     %UB = 0.18;     %0.18 based on Gauntt figures
    param(i).pdvalue        =  [mu, sigma];  %a, b, c
    param(i).trlimits        =  [LB, UB];  %Lower and upper boundary
    % Create distribution 
    % pd_select(i) = create_dist(param(i).pdtype, param(i).pdparam, param(i).pdvalue,param(i).istruncate, param(i).trlimits);
end
%else
 % i = 8;
 % run struct_prototype.m
%end
%% =======================================================================
% Parameter 9 Radial FPT 

if flag_type == 'FPT'
    i = 9;
    param(i).num            = i;
    param(i).name           = 'FCELR';
    param(i).fullname = param(i).name;
    param(i).descrip        = 'COR_RF card FCELR field - Radial Radiation Exchange Factors FCELR for FPT-1';
    param(i).comment        = 'Based on  (Gauntt, 2004a,b) Gauntt Figure 24 ';    
    param(i).note           = {'Radial Radiation Exchange Factors FCELR for PWR is lower than for experiments like FPT' ...
                                'According to Gannt 0.75- 1.0' };
    param(i).unit           = '[-]';
    param(i).SOARCA         = 0.1;  % SOARCA
    param(i).MELCOR         = 0.1;  % code default
    param(i).BEST           = 0.75;  % best estimate for FPT  %  According to Gannt 0.75- 1.0
    param(i).type           = 'Continous';
    param(i).special        = 'Yes';         
    param(i).whyspec        = 'Depends on type of facility, see flag_type';           
    param(i).istruncate      = 'Yes';        
    param(i).pdtype         =  'Normal';
    param(i).pdparam        =  {'mu' 'sigma'};
    param(i).plotrange       = [0, 1];
    % Define parameters
    mu =0.75;  %Gantt
    %mu = 0.5;
    sigma = 0.1;
    LB=0.5; UB = 1.0;
    %LB=0.4; UB = 0.6;
    % has to be <0.0, 1.0>
    param(i).pdvalue        =  [mu, sigma];  %a, b, c
    param(i).trlimits        =  [LB, UB];  %Lower and upper boundary
    % Create distribution 
    % pd_select(i) = create_dist(param(i).pdtype, param(i).pdparam, param(i).pdvalue,param(i).istruncate, param(i).trlimits);
end
    %else
  %  i = 9;
  %  run struct_prototype.m
%end    
%% =======================================================================
% Parameter 10 Axial both FPT and PWR
i = 10;
param(i).num            = i;
param(i).name           = 'FCELA';
param(i).fullname = param(i).name;
param(i).descrip        = 'COR_RF card FCELA field - Axial Radiation Exchange Factors';
param(i).comment        = 'Based on  (Gauntt, 2004a,b) Gauntt Figure 24 but also Gharari ';    
param(i).note           = {'Same as for PWR FCELR' ...
                            '' };
param(i).unit           = '[-]';
param(i).SOARCA         = 0.1;  % SOARCA
param(i).MELCOR         = 0.1;  % code default
param(i).BEST           = 0.1;  % best estimate for FPT  %  According to Gannt 0.75- 1.0
param(i).type           = 'Continous';
param(i).special        = 'Yes';         
param(i).whyspec        = 'Depends on type of facility, see flag_type';           
param(i).istruncate      = 'Yes';        
param(i).pdtype         =  'Normal';
param(i).pdparam        =  {'mu' 'sigma'};
param(i).plotrange       = [0, 0.3];
param(i).verif_file         = './DATA/Fig24.csv'; 
% Define parameters
mu = 0.1;  %default 
sigma = 0.035;
LB=0.02; UB = 0.3;    %LB-Gauntt, UB - Gharari  %UB = 0.18;     %Earlier: 0.18 based on Gauntt figures
param(i).pdvalue        =  [mu, sigma];  %a, b, c
param(i).trlimits        =  [LB, UB];  %Lower and upper boundary
% has to be <0.0, 1.0>
% Create distribution 
% pd_select(i) = create_dist(param(i).pdtype, param(i).pdparam, param(i).pdvalue,param(i).istruncate, param(i).trlimits);
%% =======================================================================
% Parameter 11
i = 11;
param(i).num            = i;
param(i).name           = 'HDBH2O';
param(i).fullname = param(i).name;
param(i).descrip        = ' In-Vessel Falling Debris HTC HDBH2O	';
param(i).comment        = 'Based on  (Gauntt, 2004a,b), also Gharari and Ghalishin 2018';    
param(i).note           = {' in Gauntt Figure 23 100-400 W/m2-K, based on FARO exp  '...
                            'in Ghalishin 200-2000'...
                            'Gharari 100-400'...
                            'SOARCA best is 2000' ...
};
param(i).unit           = '[W/m2-K]';
param(i).SOARCA         = 2000;  % SOARCA,2 mm
param(i).MELCOR         = 100;  % M2.2. code default
param(i).BEST           = 100;  % best estimate
param(i).type           = 'Continous';
param(i).special        = 'No';         
param(i).istruncate      = 'No';        
param(i).pdtype         =  'Triangular';
param(i).pdparam        =  {'a' 'b' 'c'};
param(i).plotrange       = [0, 3000];
param(i).verif_file         = './DATA/Fig23.csv'; 
% Define parameters
%LB = 125;  %Gauntt %UB = 400; %Gauntt %mode = 150;
a = 100;  %Code default, Gharari LB
b = 250;  % Guess   - center of Gharari
c = 2000;  %Galushin max
param(i).pdvalue        =  [a, b, c];  %a, b, c
% Create distribution 
% pd_select(i) = create_dist(param(i).pdtype, param(i).pdparam, param(i).pdvalue,param(i).istruncate, param(i).trlimits);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DISCRETE DISTRIBUTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% =======================================================================
% Parameter 12
%edges12 = [0.5:1:5.5];
% 1 - URBANIC-HEIDRICK DEFAULT
i = 12;                             
param(i).num            = i;        
param(i).name           = 'SC1001';
param(i).fullname           = {'SC1001(1,1)', 'SC1001(2,1)', 'SC1001(3,1)', 'SC1001(4,1)', 'SC1001(5,1)', 'SC1001(6,1)'}; 
param(i).descrip        = 'Oxidation Rate Coefficients SC1001(1..6,1)';       
param(i).comment        = 'Oxidation Rate Coefficients SC1001(1..6,1), Based on internal research, 5 options available 1,2,3,4,5; it covers six fields!!!';       
param(i).note           = {'It is in fact uniform but NonUniform allows using other approach'  ...
                           '0-1 is 1 1-2 is 2 .... 4-5 is 5'...
                        };   
param(i).SOARCA         = [1];  % Default Urbanic-Heidrick  
param(i).MELCOR         = [1];    
param(i).BEST           = [1];
param(i).type           = 'Discrete';     % if Discrete then ceil
param(i).special        = 'Yes';
param(i).whyspec        = 'Continous-Simulates Discrete with Picewise-Linear Dist.';   
param(i).istruncate     = 'No';        
param(i).pdtype         =  'PiecewiseLinear';  %if discrete has to be 'PiecewiseLinear'

param(i).pdparam      = [1, 2, 3, 4, 5];  % number of the model
param(i).pdvalue         = [0.2, 0.2, 0.2, 0.2, 0.2];   % probability
param(i).plotrange       = [1,5];

% or     = {'1', '2', '3', '4', '5'};   % or    = {[1:5]}; 
% pd_select(i) =  create_discrete_dist(param(i).pdtype,param(i).pdparam ,param(i).pdvalue);
%% =======================================================================
% DEFINE ADDITIONA DATA - DEFINITION OF CONTENTS OF THE DISCRETE DISTRIBUTIONS
%% =======================================================================
k = 1; 
param(i).add_data(k).pdparam    = k;  
%!29.6 × exp(-16820 / T), T < 1853 K 		%!87.9 × exp(-16610 / T), T >1873 K
    param(i).add_data(k).name     = 'URBANIC-HEIDRICK';
    param(i).add_data(k).value(1) = 29.6;
    param(i).add_data(k).value(2) = 16820.0;
    param(i).add_data(k).value(3) = 87.9;
    param(i).add_data(k).value(4) = 16610.0;
    param(i).add_data(k).value(5) = 1853.0;
    param(i).add_data(k).value(6) = 1873.0;  
%% =======================================================================
k = 2; 
param(i).add_data(k).pdparam    = k;  
param(i).add_data(k).name = 'BAKER-JUST';  
%3330 × exp(-22897 / T)
    param(i).add_data(k).value(1) = 3300.0 ; 
    param(i).add_data(k).value(2) = 22897.0;  
    param(i).add_data(k).value(3) = 3300.0 ; 
    param(i).add_data(k).value(4) = 22897.0;  
    param(i).add_data(k).value(5) = 1853.0 ; 
    param(i).add_data(k).value(6) = 1873.0 ; 
%% =======================================================================
k = 3; 
param(i).add_data(k).pdparam    = k;  
param(i).add_data(k).name = 'PRATER-COURTRIGHT AND LEISTIKOW-SCHANZ'; 
    %! 26763.6 × exp(-26440 / T), T > 1873K				%! 425.8 × exp(-20962 / T), T < 1853 K
     param(i).add_data(k).value(1) = 425.8  ;
     param(i).add_data(k).value(2) = 20962.0;
     param(i).add_data(k).value(3) = 26763.6;
     param(i).add_data(k).value(4) = 26440.0;
     param(i).add_data(k).value(5) = 1853.0 ;
     param(i).add_data(k).value(6) = 1873.0 ;
%% =======================================================================
k = 4; 
param(i).add_data(k).pdparam    = k;  
param(i).add_data(k).name ='PRATER-COURTRIGHT AND CATHCART-PAWEL';
      %! 26763.6 × exp(-26440 / T), T > 1873K		%294.2 × exp(-20100 / T) < 1853 K
       param(i).add_data(k).value(1) = 294.2 ;
       param(i).add_data(k).value(2) = 20100.0;  
       param(i).add_data(k).value(3) = 26763.6;  
       param(i).add_data(k).value(4) = 26440.0;  
       param(i).add_data(k).value(5) = 1853.0 ; 
       param(i).add_data(k).value(6) = 1873.0  ;    
%% =======================================================================
k = 5; 
param(i).add_data(k).pdparam    = k;  
param(i).add_data(k).name = 'URBANIC-HEIDRICK AND CATHCART-PAWEL';
    %!87.9 × exp(-16610 / T), T >1873 K 	%!294.2 × exp(-20100 / T) < 1853 K
       param(i).add_data(k).value(1) = 294.2   ; 
       param(i).add_data(k).value(2) = 20100.0 ;
       param(i).add_data(k).value(3) = 87.9    ;
       param(i).add_data(k).value(4) = 16610.0 ;
       param(i).add_data(k).value(5) = 1853.0  ;
       param(i).add_data(k).value(6) = 1873.0  ;
%% =======================================================================
%% =======================================================================
%% =======================================================================
% Parameter 13
%edges13 = [0.5:1:3.5];
i = 13;                             
param(i).num            = i;        
param(i).name           = 'IRODDAMAGE'; 
param(i).fullname       = param(i).name;
param(i).descrip        = 'IRODDAMAGE COR_ROD Rod Collapse Model ';       
param(i).comment        = 'Based on SOARCA UA, Fuel failure criterion function Fuel Rod Collapse Model Logic';       
param(i).note           = {'Field/card data dietals in this case are present in MELCOR block type comment' ...
                        };   
param(i).SOARCA         = [1];  % 
param(i).MELCOR         = [1];    
param(i).BEST           = [1];
param(i).type           = 'Discrete';     % if Discrete then ceil
param(i).special        = 'Yes';
param(i).whyspec        = 'Continous-Simulates Discrete with Picewise-Linear Dist.';   
param(i).istruncate     = 'No';        
param(i).pdtype         =  'PiecewiseLinear';  %if discrete has to be 'PiecewiseLinear'

param(i).pdparam      = [1, 2, 3];  
param(i).pdvalue         = [0.8, 0.1, 0.1]; 
param(i).plotrange       = [1,3];
%% =======================================================================
% DEFINE ADDITIONAL DATA - DEFINITION OF CONTENTS OF THE DISCRETE DISTRIBUTIONS
%% =======================================================================
k = 1; 
    param(i).add_data(k).pdparam    = k;  
    param(i).add_data(k).name     = 'ALTERNATIVE1';
    param(i).add_data(k).value = 'TIMETOFAIL1';
k = 2; 
    param(i).add_data(k).pdparam    = k;  
    param(i).add_data(k).name     = 'ALTERNATIVE2';
    param(i).add_data(k).value = 'TIMETOFAIL2';

k = 3; 
    param(i).add_data(k).pdparam    = k;  
    param(i).add_data(k).name     = 'ALTERNATIVE3';
    param(i).add_data(k).value = 'TIMETOFAIL3';
%% =======================================================================
% ALT1	SOARCA				ALT2					ALT3	
% 2090	6.00E+31			1990	6.00E+31		2190	6.00E+31
% 2100	3.60E+04			2000	1.80E+04		2200	7.20E+04
% 2500	3.60E+03			2400	1.80E+03		2600	7.20E+03
% 2600	3.00E+02			2500	1.50E+02		2700	6.00E+02
%% =======================================================================


%% =======================================================================
% LEGACY CODE
% Alternative pd = makedist('PieceWiselinear','x',[6 8 10],'Fx',[0 0.8 1]);
%param(i).pdparam        = {'1' '2' '3' '4' '5'}; 
%param(i).pdvalue        = [0.2, 0.2, 0.2, 0.2, 0.2];
% https://uk.mathworks.com/help/stats/prob.piecewiselineardistribution.html 
% [f,x] = ecdf(hospital.Weight);
% pd = makedist('PiecewiseLinear','x',x,'Fx',f)
% makedist('PiecewiseLinear', 'x', [1:1:100]  ,'Fx', linspace(0,1,100))
%edges12 = [0.5:1:5.5];
% Examples:
%param(i).pdparam      = {linspace(1,100,100)};   % can be cell or matrix 
%param(i).pdvalue       = 0.01.*ones(1,100);     % can be operation matlab type
%















