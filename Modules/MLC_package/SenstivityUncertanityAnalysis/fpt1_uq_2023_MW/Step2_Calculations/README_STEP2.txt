Developed by Piotr Darnowski, all rights reserved.
Short readme about Step2.
This step generates power-shell files which allows direct execution of the uncertanity inputs. 
Generated power-shell scripts execute melcor/melgen in parallel in several windows, but also in series. Flexible to user needs. See: Step2_Main_SUA.m file for details.
User has to provide folders/location for Step1 and melcor/melgen and working folders.



Files: 
clean_melcor_results.ps1    remove melcor results files - example
melcor_unc_1.ps1			based on this file automatic generator of power shell files was prepared
melcor_unc_execute_1.ps1	based on this file automatic generator of power shell files was prepared
parallel_melcor_unc_runner.ps1  alternative, simplier approach to rune files - without alliases in power-shell


other files were generated - examples:
melcor_execute_MLC_UNC_Test1.ps1
melcor_scripts_MLC_UNC_Test1.ps1


M-files:
Step2_Main_SUA.m     		file to generate automatically power shell scripts to run MELCOR results for files generated in Step1
SUA_Generate_PowerShell.m	script which actually generates file
