% Developed by Piotr Mazgaj for NARSIS Project
function graph_bar_2( X1,X2, Param_Name, MainTitle,XLabel, YLabel, Filename)



%  Auto-generated by MATLAB on 02-Dec-2014 11:48:39

% Create figure
figure1 = figure;
 
% nie wy?wietla si? okienko podczas uruchamiania skryptu
% if Visibility == 'OFF'
%     set(figure1,'visible','off');
% end


% Create axes
% n = 10;
% range = (Tmax - Tmin)/n ;
% Tick(1) = Tmin;
% 
% for i=2:(n+1)
%     Tick(i)=range * (i-1)+Tmin;
%     
% end


% axes1 = axes('Parent',figure1,'XTick',Tick,...
%     'XMinorTick','on',...
%     'FontSize',14);



% % Create axes
axes1 = axes('Parent',figure1,'XTick',[0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20],...
    'XMinorTick','on',...
    'FontSize',14);
%% Uncomment the following line to preserve the X-limits of the axes
xlim(axes1,[0 20]);
box(axes1,'on');
hold(axes1,'all');



%% Create line Experimental
% curve1 = line(X1,Y1,'Parent',axes1,'MarkerFaceColor',[0 0 1],...
%     'MarkerEdgeColor','none',...
%     'MarkerSize',3,...
%     'Marker','o',...
%     'LineStyle','none',...
%     'Color',[0 0 1],...
%     'DisplayName',name1);
% 


title('Parameters')
 
h1=  subplot(1,4,1)       % add first plot in 2 x 1 grid
 
 
 set(h1,'Visible','off');
 
title('Parameters')

% ----------------------------------------------
subplot(1,4,[2 3])       % add first plot in 2 x 1 grid
barh(X1)


%title('Pearson correlation coefficients for PCT')
title(MainTitle)

grid on ;

% tu tutu set(gca,'YTick',1:18)
set(gca,'YTickLabel',Param_Name,'FontSize',9, 'FontWeight', 'bold', 'GridAlpha',0.4)

set(gca,'xTick',-1:.2:1)
xlim([-1,1]);

%xlabel('Pearson Correlation Coefficient','FontSize',12);
xlabel(XLabel,'FontSize',14);

% ------------------------------------
subplot(1,4,4)       % add second plot in 2 x 1 grid
barh(X2)       % plot using + markers


% tu . utu set(gca,'YTick',1:18)


%set(gca,'ytick',[])

set(gca,'xTick',0:.2:1, 'GridAlpha',0.4,'FontWeight', 'bold')
% Create xlabel
xlabel('p-value','FontSize',14);

%axes1.GridAlpha = 0.4;  % Make grid lines less transparent.
%axes1.GridColor = [0.1, 0.1, 0.1]; % Dark Green.
grid on
% Create ylabel
%ylabel(YLabel,'FontSize',16);

% Create title
%title(Title,'FontSize',16);

% Create legend

%legend([curve1 f1 curve2 curve3 curve4],name1,'Exp. Data Uncertainty',name2,name3,name4)

% legend1 = legend(axes1,'show');
% set(legend1,'Location', LLocation);


% % Create textbox
% annotation(figure1,'textbox',...
%     [0.13927 0.838095238095239 0.441071428571429 0.0761904761904765],...
%     'String',description,...
%     'FitBoxToText','on');
% 

% Axe limit
%xlim([0,600]);
% Axe limit
xlim([0,1]);



print(['graphs/',Filename],'-dpng','-r200');
print(['graphs/',Filename],'-depsc');
savefig(['graphs/',Filename]);

% Graph in PDF
% fig = gcf;
% fig.PaperPositionMode = 'auto'
% fig_pos = fig.PaperPosition;
% fig.PaperSize = [fig_pos(3) fig_pos(4)];
% 
% print(fig,['graphs/',Filename],'-dpdf');

disp([Filename,' is created.']);
end

