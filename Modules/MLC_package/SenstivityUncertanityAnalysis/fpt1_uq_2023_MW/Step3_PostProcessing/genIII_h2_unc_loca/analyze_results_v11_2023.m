%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Developed by Piotr Darnowski, Warsaw University of Technology
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% https://uk.mathworks.com/matlabcentral/answers/498767-troubles-assigning-colorbar-values-in-a-bivariate-histogram-plot
% https://uk.mathworks.com/help/matlab/matlab_prog/create-a-table.html
% https://uk.mathworks.com/help/stats/gplotmatrix.html
% https://uk.mathworks.com/help/matlab/ref/plotmatrix.html
% https://uk.mathworks.com/help/matlab/ref/categorical.html
% SHADED AREA PLOTS:
% https://uk.mathworks.com/matlabcentral/fileexchange/58262-shaded-area-error-bar-plot
% https://uk.mathworks.com/matlabcentral/fileexchange/69203-shaded-plots-and-statistical-distribution-visualizations
% https://uk.mathworks.com/matlabcentral/answers/498767-troubles-assigning-colorbar-values-in-a-bivariate-histogram-plot
% https://uk.mathworks.com/matlabcentral/answers/83519-2d-scatter-plot-with-z-value-in-color
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SCRIPT FOR UNCERTANITY PLOTTING
% legend([p3(1)],'M2.2.18')
% legend([p1(1)],'Success')
% legend([p2(1)],'Partial')
% legend([p1 p3],{'First','Third'})
% legend({'cos(x)','cos(2x)','cos(3x)','cos(4x)'},'Location','northwest','NumColumns',2)
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  labels = [labels, [cases_list_names1{k_num} 'EFPD' ] ];
%		hLegend = findobj(gcf, 'Type', 'Legend');
%		%get text
%		
%		if isempty(hLegend)	
%			labels = {};
%		else
%			labels = hLegend.String;
%		end
% set(0, 'DefaulttextInterpreter', 'none'); 
%  l = legend(labels); 
% set(l, 'Interpreter','none');
% set(l,'FontSize',legend_font_size);
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T=readtable('data_cols.csv')
% Tbest=readtable('best_est_data_cols.csv')
% flag_read0 = 0; %Read case0
% set(0, 'DefaulttextInterpreter', 'none'); 
% mkdir([folder{:},'\',folder_name,'\',results_folder]);
% print('-dtiff',[folder{:},'\',folder_name,'\',results_folder,'\', name, '.tiff']);
% savefig([folder{:},'\',folder_name,'\',results_folder,'\', name, '.fig']);
% x = rand(100,1)*50;
% catnames = ["small","medium","large"];
% binnedData = discretize(x,[0 15 35 50],'categorical',catnames);
% figure('Renderer', 'painters', 'Position', [10 10 900 600])
%
% tcl.TileSpacing ='compact';
% openfig('fig1.fig');
% ax1=gca;
% openfig('fig2.fig')
% ax2=gca;
%  
% figure;
% tcl=tiledlayout(1,2);
% ax1.Parent=tcl;
% ax1.Layout.Tile=1;
% ax2.Parent=tcl;
% ax2.Layout.Tile=2;
%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear; clc; close all; format short g;
default_color = lines(100);
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
flag_plot = 0;  %Plot figure of merit
flag_save = 0;  %Save figure of merit
flag_reduced = 1;  %NOT ACTIVE
flag_normalization = 1;  %0 - normalizatio by division;  1 - normalization based on formula (X - mu) / sigma



root_folder15  = 'C:\CloudStation\ROOT\NARSIS-GenIII\EDFresults_NARSIS_UNC_PWR_015_INT_Results_M2218\'   %400, Best, LHS, fpt-1 like

root_folder14  = 'C:\CloudStation\ROOT\NARSIS-GenIII\EDFresults_NARSIS_UNC_PWR_014_INT_Results_M2218\'  %400, Best, LHS, peak profile
root_folder14a  = 'C:\CloudStation\ROOT\NARSIS-GenIII\EDFresults_NARSIS_UNC_PWR_014_WILKS_INT_Results_M2218\'  %120, Best, SRS, peak profile

root_folder14b  = 'C:\CloudStation\ROOT\NARSIS-GenIII\EDFresults_NARSIS_UNC_PWR_014_SRS_INT_Results_M2218\'  %400, Best, SRS, peak profile

root_folder3 = 'C:\CloudStation\ROOT\NARSIS-FPT1\EDFresults_NARSIS_UNC_FPT_E8_INT_Results_M2218\'		
root_folder3a = 'C:\CloudStation\ROOT\NARSIS-FPT1\EDFresults_NARSIS_UNC_FPT_E8_INT_Results_M2211\'

root_folder16a =  'C:\CloudStation\ROOT\NARSIS-GenIII\EDFresults_NARSIS_UNC_PWR_016_WILKS_BEST_INT_Results_M2218\'		%120, Best, SRS, peak profile
root_folder16b = 'C:\CloudStation\ROOT\NARSIS-GenIII\EDFresults_NARSIS_UNC_PWR_016_WILKS_UNIFORM_INT_Results_M2218\'    %120, Uniform, SRS, peak profile

root_folder17a = 'G:\ROOT_MELCOR\FPT1\fpt1_uq_test3_Results_M22_r2023'

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
%flag_type = 'PWR'
flag_type = 'FPT'
nPlots = 1  %Number of cases 1 or 2
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
% INPUT ANALYSIS
if flag_type == 'PWR'
	%root_folder1  = 'C:\CloudStation\ROOT\NARSIS-GenIII\EDFresults_NARSIS_UNC_PWR_014_INT_Results_M2218\'

	%case_name = {'PWR peak', 'PWR FPT-like'};
	case_name = {'PWR Best', 'PWR Uniform'};
	%case_name = {'PWR Uniform'};
	%case_name = {'PWR Best'};
	edffile = 'EDF_5_MH2CR'; colFOM = 2;
	partial_success = 2500;
	nCases = 400;
	%nCases = 120;
	str = 'DEBRIS EJECTION THROUGH FAILED VESSEL ABOUT TO START'
	file_diag = 'MELPWR.DIA';

    root_folder = root_folder14b
    %root_folder = root_folder14a
    %root_folder2 = root_folder14b
    %root_folder = root_folder14a
	%root_folder = root_folder14b
    
 %root_folder = root_folder14; %PWR peak    
 %root_folder2 = root_folder15; %PWR fpt-like
	
	%root_folder = root_folder16a;
	%root_folder = root_folder16b;
	%root_folder2 = root_folder16b;

end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
if flag_type == 'FPT'
    
	%root_folder = root_folder3;  %M2.2.18
    %root_folder2 = root_folder3a;   %M2.2.11
    
	root_folder = root_folder17;  %M2.2r2023
	
	partial_success = 16500;
	nCases = 400;
	edffile = 'EDF_5_MH2CR'; colFOM = 2; %edffile = 'EDF_H2';  colH2 = 3;
	str = 'USER INPUT TEND'
	file_diag = 'MELFPT1.DIA';
    %case_name = {'FPT-1'};
    
    %case_name = {'FPT M2.2.18', 'FPT M2.2.11'};
	case_name = {'FPT M2.2r2023'};
end

 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
	fig_folder = [root_folder,'Figs']; mkdir(fig_folder);
	
	[T_all, T_best, T_success, T_partial, T_failed, ...
	Tbl_calc_all , Tbl_names, Tbl_variables, Tbl_calc, Tbl_best, Tbl_all, Tbl_partial, Tbl_success, ...
	variables, A_success, A_partial, A_failed, A, A_all, A_best,...
	data,  data0, ...
	timeend_success, timeend_partial, ...
	FOM, FOM_partial, FOM_success, FOM_failed, mCases, ...
	isPartial, isBest, isSuccess, isFailed ...  
	T_MAX, T_MAX2, x, y, x0, y0, t_interp, y_interp, y_interp_mean, y_interp_mean_all  ...
	] = ...
	find_failed_runs_v9(root_folder,  edffile, nCases, partial_success, file_diag, colFOM, str);
%% 
if nPlots == 2
	root_folder = root_folder2;
    fig_folder = [root_folder,'FigsComp']; mkdir(fig_folder);
	%fig_folder = ['./FigsComp']; mkdir(fig_folder);

	[T_all2, T_best2, T_success2, T_partial2, T_failed2, ...
	Tbl_calc_all2 , Tbl_names2, Tbl_variables2, Tbl_calc2, Tbl_best2, Tbl_all2, Tbl_partial2, Tbl_success2, ...
	variables2, A_success2, A_partial2, A_failed2, A2, A_all2, A_best2,...
	data2,  data02, ...
	timeend_success2, timeend_partial2, ...
	FOM2, FOM_partial2, FOM_success2, FOM_failed2, mCases2, ...
	isPartial2, isBest2, isSuccess2, isFailed2 ...  
	T_MAX_2, T_MAX2_2, x2, y2, x02, y02, t_interp2, y_interp2, y_interp_mean2, y_interp_mean_all2  ...
	] = ...
	find_failed_runs_v9(root_folder,  edffile, nCases, partial_success, file_diag, colFOM, str);
	
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
%% PLOT OTHER CASES
if flag_plot == 1

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	 
k = 1;  f(k)=figure(k); hold on; 
%if flag_normalization == 1
%    y_interp_end = y_interp(end,:);
%	y_interp_mean = mean(y_interp_end)
%	y_interp_std = std(y_interp_end)
%
%	plot(t_interp, (y_interp - y_interp_mean)./y_interp_std );
%else
	plot(t_interp, y_interp./y_interp_mean);
%end

box on; grid on;
if (flag_save == 1) savefig([fig_folder,'/Fig',num2str(k)]);  print('-dtiff',[fig_folder,'/Fig',num2str(k)]); end;

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	 
k = 2; f(k)=figure(k); hold on;
plot(t_interp, y_interp);
box on; grid on;
if (flag_save == 1) savefig([fig_folder,'/Fig',num2str(k)]);  print('-dtiff',[fig_folder,'/Fig',num2str(k)]); end;

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  




%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
k = 3; f(k)=figure(k); hold on;
%pct = [68.27, 95.45, 99.73]; 
pct = [95 50 ]; 
labels = {};

%if flag_normalization == 1
%	plot_distribution_prctile((t_interp')./partial_success,(normalize(y_interp))','Prctile',pct); labels = [labels, case_name(1)  ];
%	if nPlots == 2
%		plot_distribution_prctile( (t_interp2')./partial_success,(normalize(y_interp2))','Prctile',pct,'Color',default_color(2,:));
%		labels = [labels, case_name(1)  ];
%	end
%	ylabel('H2 (Mass - Mean)/Sigma, [-]');
%
%else
	plot_distribution_prctile((t_interp')./partial_success,(y_interp./y_interp_mean)','Prctile',pct); labels = [labels, case_name(1)  ];
	if nPlots == 2
		%plot_distribution_prctile(t_interp2',(y_interp2./y_interp_mean2)','Prctile',pct,'Color',default_color(2,:));
		plot_distribution_prctile( (t_interp2')./partial_success,(y_interp2./y_interp_mean2)','Prctile',pct,'Color',default_color(2,:));
		labels = [labels, case_name(1)  ];
	end
	ylabel('H2 mass / mean, [-]');
%end
box on; grid on;






title('Norm. Hydrogen Generation','FontSize',9);
xlabel('Norm. Time, [-]');
%xlim([0 5000]);
xlim([0 1.5]);

if (flag_save == 1) savefig([fig_folder,'/Fig',num2str(k)]);  print('-dtiff',[fig_folder,'/Fig',num2str(k)]); end;
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
k = 4; f(k)=figure(k); hold on;
pct = [95 50 ]; labels = {};
%p2 = [68.27, 95.45, 99.73]; % p1 = [95 50 ]
plot_distribution_prctile(t_interp',(y_interp)','Prctile',pct);
box on; grid on;

if nPlots == 2
	plot_distribution_prctile(t_interp2',(y_interp2)','Prctile',pct,'Color',default_color(2,:));
end


%legend(labels{:});	
if (flag_save == 1) savefig([fig_folder,'/Fig',num2str(k)]);  print('-dtiff',[fig_folder,'/Fig',num2str(k)]); end;
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
% PLOT BEST ESTIMATE AND EXP DATA
if flag_type == 'FPT'
	k = 5; f(k)=figure(k); hold on;
	% PLOT BEST CASE
	plot_fom(f(k),x0,y0,'-','k',2);  % isSuccess(1));  
	load('C:\CloudStation\NARSIS\MODELE_AND_RESEARCH\SCRIPTS\MATLAB_XCore\x-core\Modules\MLC_package\PostProcessing\MATLAB_MELCOR_RESULTS_VISUALIZATION\fpt1\EXP_DATA6.mat')

  %  Zr_data =  importdata([folder,'\',edffile]) ;
    
	x1 = EXP_H2_TOT(:,1); 
	y1 = EXP_H2_TOT(:,2);
	x2 = EXP_H2_TOT_SNL2(:,1); 
	y2 = EXP_H2_TOT_SNL2(:,2);
	plot_fom(f(k),x1,y1,'none',default_color(1,:),0.5,'o',5);  
	plot_fom(f(k),x2,y2,'none',default_color(2,:),0.5,'<',5);  
	legend('Best Est.','EXP#1','EXP#2','Location','NorthWest');
	ax = f(k).CurrentAxes; 
	title(ax,'Hydrogen Generation');  xlabel(ax,'Time, [s]'); ylabel(ax,'Mass, [kg]'); xlim(ax,[0 inf]);
	if (flag_save == 1) savefig([fig_folder,'/Fig',num2str(k)]);  print('-dtiff',[fig_folder,'/Fig',num2str(k)]); end;
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
k = 6;
f(k) = figure(k); hold on; % ax1 = f.CurrentAxes; 
for i=1:nCases	
	if isSuccess(i+1) == 1
		p1(i) = plot_fom(f(k),x{i},y{i},'-',[0.5 0.5 0.5],1);	
		exampleSuccess = i;
	end			
end
for i=1:nCases	
	if (isSuccess(i+1) == 0) & (isPartial(i+1) == 1)
		p1(i) = plot_fom(f(k),x{i},y{i},'-','r',1);	
		examplePartial = i;
	end	
end		

% PLOT BEST CASE
p3 = plot_fom(f(k),x0,y0,'-','black',2,'none');  
p4 = plot(t_interp,y_interp_mean_all,'-b','LineWidth',1.5);  

ax(k) = f(k).CurrentAxes;
% OTHER OPTIONS	
xlim(ax(k),[0 inf]);   ylim(ax(k),[0 inf]); title(ax(k),'Hydrogen Generation'); 
xlabel(ax(k),'Time, [s]');  ylabel(ax(k),'Mass, [kg]');
%legend([p3(1), p1(1), p1(examplePartial), p4(1)],'Best Est.','Success','Partial','Mean')
legend([p3(1), p1(exampleSuccess), p1(examplePartial), p4(1)],'Best Est.','Success','Partial','Mean')
legend('Location','SouthEast')

%legend([p3(1), p1(1), p1(examplePartial)],'Best Est.','Success','Partial');
if flag_type == 'PWR'
    xlim([0 5000]); ylim([0 500]);
end
if flag_type == 'FPT'
    xlim([0 20000]); ylim([0 0.12]);
end


ax(k) = f(k).CurrentAxes;
%line([partial_success partial_success],[0 max(FOM)])
if (flag_save == 1) savefig([fig_folder,'/Fig',num2str(k)]);  print('-dtiff',[fig_folder,'/Fig',num2str(k)]); end;
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	fig15 = figure;
%X(:,i) = data{i}(:,1);
%shadedErrorBar(x0, y0,{@mean,@std},'lineprops','-b','transparent',1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
k = 7;
f(k) = figure(k); hold on;
	boxplot(FOM_partial); box on; grid on;
	ax(k) = gca;	
	if (flag_save == 1) savefig([fig_folder,'/Fig',num2str(k)]);  print('-dtiff',[fig_folder,'/Fig',num2str(k)]); end;
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
k = 8;
f(k) = figure(k); hold on;	
	boxchart(FOM_partial);  grid on; box on;
	%ylim([0 inf]);
	ylim([0 500]);
	ax(k) = gca;	
	xlabel('Partial'); 
	if (flag_save == 1) savefig([fig_folder,'/Fig',num2str(k)]);  print('-dtiff',[fig_folder,'/Fig',num2str(k)]); end;
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
k = 9;	
f(k) = figure(k); hold on;
	tiledlayout(1,2);
	nexttile;
	boxchart(FOM,'Notch','on'); ylim([0 inf]);  grid on; box on;
	xlabel('All')
	nexttile;
	boxchart(FOM_partial,'Notch','on'); ylim([0 inf]); grid on; box on;
	hold on; box on; grid on;
	xlabel('Partial')
	if (flag_save == 1) savefig([fig_folder,'/Fig',num2str(k)]);  print('-dtiff',[fig_folder,'/Fig',num2str(k)]); end;

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
k = 10;
f(k) = figure(k);
copyobj(f(6).Children,f(k));
hold on; box on; grid on;
ax(10) = gca;
hL10 = findobj(gcf, 'Type', 'Legend');
if (flag_save == 1) savefig([fig_folder,'/Fig',num2str(k)]);  print('-dtiff',[fig_folder,'/Fig',num2str(k)]); end;
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
k = 11
f(k) = figure(k); 
tcl = tiledlayout(1,6);
ax1 = ax(10);
ax1.Parent = tcl;
h1 = hL10;
h1.Parent = tcl;

ax1.Layout.TileSpan = [1, 5];
ax2 = ax(8);
ax2.Parent = tcl;
ax2.Layout.Tile=6;

if (flag_save == 1) savefig([fig_folder,'/Fig',num2str(k)]);  print('-dtiff',[fig_folder,'/Fig',num2str(k)]); end;

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
k = 12
f(k) = figure(k); 
x1 = ones(1,numel(FOM));
y1= FOM;
x2 = 2*ones(1,numel(FOM_partial));
y2 = FOM_partial;

swarmchart(x1,y1,5); hold on; box on; grid on;
swarmchart(x2,y2,5); hold on; box on; grid on;
if (flag_save == 1) savefig([fig_folder,'/Fig',num2str(k)]);  print('-dtiff',[fig_folder,'/Fig',num2str(k)]); end;
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot ECDF
k = 13
f(k) = figure(k); 		hold on;
labels = {};
%plot_ecdf(f(k),FOM_partial,'-',default_color(1,:))
plot_ecdf(f(k),FOM_partial,'-',default_color(1,:)); labels = [labels, case_name(1)  ];
	if nPlots == 2
		plot_ecdf(f(k),FOM_partial2,'-',default_color(2,:)); labels = [labels, case_name(2)  ];
    end
xlabel('H2 Mass, [kg]');     
legend(labels{:});	
if (flag_save == 1) savefig([fig_folder,'/Fig',num2str(k)]);  print('-dtiff',[fig_folder,'/Fig',num2str(k)]); end;

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
% Plot ECDF
k = 14
f(k) = figure(k); 		hold on;
labels = {};
%plot_ecdf(f(k),FOM_partial,'-',default_color(1,:))

if flag_normalization == 1
	plot_ecdf(f(k), normalize(FOM_partial),'-',default_color(1,:)); labels = [labels, case_name(1)  ];
		if nPlots == 2
			plot_ecdf(f(k),normalize(FOM_partial2),'-',default_color(2,:)); labels = [labels, case_name(2)  ];
		end
	xlabel('H2 (Mass-Mean)/Sigma');
else
	plot_ecdf(f(k),FOM_partial./(mean(FOM_partial)),'-',default_color(1,:)); labels = [labels, case_name(1)  ];
		if nPlots == 2
			plot_ecdf(f(k),FOM_partial2./(mean(FOM_partial2)),'-',default_color(2,:)); labels = [labels, case_name(2)  ];
		end
	xlabel('H2 Mass / Mean');     
end

legend(labels{:});	
if (flag_save == 1) savefig([fig_folder,'/Fig',num2str(k)]);  print('-dtiff',[fig_folder,'/Fig',num2str(k)]); end;
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  







%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
Tbl_all.Properties.VariableNames

% figALL1 =figure;
% Aplot=Tbl_success{:,2:17};
% plotmatrix(Aplot)
% title('Comparison of all parameters')



%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
k = 49
figALL2 = figure(k); hold on;
tiledlayout(4,4); 
%X = Tbl_success.("FoM");
if flag_normalization == 1
	Y = normalize(Tbl_success.("FoM"));
	if nPlots == 2
		 Y2 = normalize(Tbl_success2.("FoM"));
	end
else
	Y = Tbl_success.("FoM")./mean(Tbl_success.("FoM"));
	if nPlots == 2
		Y2 = Tbl_success2.("FoM")./mean(Tbl_success2.("FoM"));
	end

end


%fig.Position =   [90, 90, 1600, 800];
figALL2.WindowState = 'maximized';

for i=2:(1+numel(variables)) %17
	nexttile;

	X=Tbl_success{:,i};
	if nPlots == 2
		X2=Tbl_success2{:,i};
	end
	hold on;
	label = Tbl_success.Properties.VariableNames(i);

	if   ~sum(strcmp(label,{'SC1001','IRODDAMAGE'}))
		% https://uk.mathworks.com/matlabcentral/fileexchange/76467-trendline
		trends=trendline(X,Y,default_color(1,:));	
		if nPlots == 2
			trends2=trendline(X2,Y2,default_color(2,:));
		end
	end
	if   sum(strcmp(label,{'SC1001','IRODDAMAGE'}))
		if nPlots == 2
			X = X-0.05
			X2 = X2 + 0.05
		end
	end
	hold on;

		plot(X,Y,'.','Color',default_color(1,:));	%plotmatrix(X,Y); 

	if nPlots == 2
		%plot(X2,Y2,'.','Color',default_color(2,:));	%plotmatrix(X,Y); 
		plot(X2,Y2,'.','Color',default_color(2,:));	%plotmatrix(X,Y); 
	end

	box on; grid on; hold on;

	%title(num2str(i-1))
	%ylabel('FoM'); 
    %ylabel('FoM / Mean(FoM)'); 
    ylabel('Norm. FoM'); 
	%xlabel(Tbl_success.Properties.VariableNames(i))
	xlabel(['#', num2str(i-1),' ',Tbl_success.Properties.VariableNames{i}])
end
if (flag_save == 1) savefig([fig_folder,'/Fig',num2str(k)]);  print('-dtiff',[fig_folder,'/Fig',num2str(k)]); end;

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
k = 51
figALL2 = figure(k); hold on;
tiledlayout(4,4); 
%X = Tbl_success.("FoM");
Y = Tbl_success.("FoM");
if nPlots == 2
	Y2 = Tbl_success2.("FoM");
end
%fig.Position =   [90, 90, 1600, 800];
figALL2.WindowState = 'maximized';

%for i=2:  17
for i=2: (1+numel(variables))

	nexttile;

	X=Tbl_success{:,i};
	if nPlots == 2
		X2=Tbl_success2{:,i};
	end
	hold on;
	label = Tbl_success.Properties.VariableNames(i);

	if   ~sum(strcmp(label,{'SC1001','IRODDAMAGE'}))
		% https://uk.mathworks.com/matlabcentral/fileexchange/76467-trendline
		trends=trendline(X,Y,default_color(1,:));	
		if nPlots == 2
			trends2=trendline(X2,Y2,default_color(2,:));
		end
	end
	if   sum(strcmp(label,{'SC1001','IRODDAMAGE'}))
		if nPlots == 2
			X = X-0.05
			X2 = X2 + 0.05
		end
	end
	hold on;

		plot(X,Y,'.','Color',default_color(1,:));	%plotmatrix(X,Y); 

	if nPlots == 2
		%plot(X2,Y2,'.','Color',default_color(2,:));	%plotmatrix(X,Y); 
		plot(X2,Y2,'.','Color',default_color(2,:));	%plotmatrix(X,Y); 
	end

	box on; grid on; hold on;

	%title(num2str(i-1))
	%ylabel('FoM'); 
    %ylabel('FoM / Mean(FoM)'); 
    ylabel('FoM, [kg]'); 
	%xlabel(Tbl_success.Properties.VariableNames(i))
	xlabel(['#', num2str(i-1),' ',Tbl_success.Properties.VariableNames{i}])
end
if (flag_save == 1) savefig([fig_folder,'/Fig',num2str(k)]);  print('-dtiff',[fig_folder,'/Fig',num2str(k)]); end;


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If pval(a,b) is small (less than 0.05), then the correlation rho(a,b) is significantly different from zero.
% Because the p-value is less than the significance level of 0.05, it indicates rejection of the hypothesis that no correlation exists between the two columns.
k = 50
figBar1 = figure(k);
h1=  subplot(1,6,1) 


%X = Tbl_success.("FoM");
Y = Tbl_success.("FoM");
%Y = Tbl_success{:,2:17};
%X = Tbl_success{:,2:17};
X = Tbl_success{:,2:(1+numel(variables))};

label = Tbl_success.Properties.VariableNames(2:(1+numel(variables)));

[rho1,pval1] = corr(X,Y,'Type','Pearson');
[rho2,pval2] = corr(X,Y,'Type','Spearman');
[rho3,pval3] = corr(X,Y,'Type','Kendall');

rho = [rho1, rho2, rho3];
pval = [pval1, pval2, pval3];

(pval < 0.05).*(abs(rho)>0.2)


subplot(1,6,[1 4])  
bar1 = barh(rho);
yticks([1:1:numel(label)])
yticklabels(label);
set(gca,'xTick',-1:.2:1)
set(gca,'FontSize',8)
xlabel('\rho-value')
grid on; box on;
xlim([-0.4,0.6]);

subplot(1,6, [5 6])  
bar2 = barh(pval);
yticks([1:1:numel(label)])
set(gca,'xTick',0:.2:1)
set(gca,'FontSize',8)
xlabel('p-value')
grid on; box on;
xlim([0,1]);

figBar1.Position =  [10 10 600 700]

sgtitle(['Pearson, Spearman, Kendall Corr. Coeff.'],'FontSize',12)

if (flag_save == 1) savefig([fig_folder,'/Fig',num2str(k)]);  print('-dtiff',[fig_folder,'/Fig',num2str(k)]); end;



%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	  
%{
j = 100; 
f(j) = figure(j); hold off; ax(j)=gca;
p = scatterhistogram(Tbl_success,'FoM','TimeEnd','GroupVariable','IRODDAMAGE','HistogramDisplayStyle','smooth','LineStyle','-')
p.LegendVisible = 'on';
p.LegendTitle = 'IRODDAMAGE';

j= 101; f(j) = figure(j); 
hold off; ax(j)=gca;
p = scatterhistogram(Tbl_success,'FoM','TimeEnd','GroupVariable','SC1001','HistogramDisplayStyle','smooth','LineStyle','-')
p.LegendVisible = 'on';
p.LegendTitle = 'SC1001';

j= 102; f(j) = figure(j);
plotmatrix(A_success(:,[4 5]));

j= 103; f(j) = figure(j);
histogram(A_success(:,4));

%}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end	% END OF FLAG PLOT


%{
figure
hold on
histogram(y_interp(600,:)./mean(y_interp(600,:)))
histogram(y_interp(400,:)./mean(y_interp(400,:)))
histogram(y_interp(200,:)./mean(y_interp(200,:)))
histogram(y_interp(800,:)./mean(y_interp(800,:)))
histogram(y_interp(1000,:)./mean(y_interp(1000,:)))


figure
hold on
histogram(y_interp(600,:))
histogram(y_interp(400,:))
%histogram(y_interp(200,:))
histogram(y_interp(800,:))
histogram(y_interp(1000,:))
%}
%% 
% Comparison of different time moments CDFs
k = 60
f(k) = figure(k); 		hold on;
tt =[300 400 500 600 700 800 900 1000]
 t_interp(tt)
 labels = {}
 
if flag_normalization == 1
	 for i = 1:numel(tt)
		plot_ecdf(f(k),normalize(y_interp(tt(i),:))','-',default_color(i,:)); labels = [labels, num2str(t_interp(tt(i)))]
	 end
else 
	 for i = 1:numel(tt)
		plot_ecdf(f(k),y_interp(tt(i),:)'./mean(y_interp(tt(i),:))','-',default_color(i,:)); labels = [labels, num2str(t_interp(tt(i)))]
	 end
end
 
legend(labels{:});	
