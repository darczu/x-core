% Function to plot Figure of Merit 
% Check also https://www.mathworks.com/company/newsletters/articles/creating-specialized-charts-with-matlab-object-oriented-programming.html?s_eid=psm_ml&source=15308
% 
function p = plot_fom(numFig,xData, yData, LineType, LineColor, LineWidth,MarkerType,MarkerSize,LegendEntry,Title,xLabel,yLabel)  %, information)

if nargin < 8; MarkerSize = 6; end
if nargin < 7; MarkerType = '.'; end
if nargin < 6; LineWidth = 2; end
if nargin < 5; LineColor = 'k'; end	
if nargin < 4; LineType = 'none'; end	

%exist('LegendEntry')

	figure(numFig);
	grid on;   box on; 
	p = plot(xData,yData);
	p.LineWidth = LineWidth;
	p.Color = LineColor;
	p.LineStyle = LineType;
	p.Marker = MarkerType; 
	p.MarkerSize  = MarkerSize;

	fig = gcf;	
	ax = fig.CurrentAxes;
	ax.GridAlpha = 0.1;
	ax.Box = 'on';
   
   grid on;
   
   
	%ylabel('FoM');
	%
	%xlabel('')
	uistack(p,'top') ;
		

%legend(fig)

	%legend([p1(1), p2(1), p3(1) ],'MELCOR','EXP#1','EXP#2')