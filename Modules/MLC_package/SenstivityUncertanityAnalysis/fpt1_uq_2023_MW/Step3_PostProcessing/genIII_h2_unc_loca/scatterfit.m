% Function to plot Figure of Merit 
% Check also https://www.mathworks.com/company/newsletters/articles/creating-specialized-charts-with-matlab-object-oriented-programming.html?s_eid=psm_ml&source=15308
% 
%rng("default")
%x = randn(1000, 1);
%y = 2*x + 1 + randn(size(x));
%s = scatter(x, y, 6, "filled", "MarkerFaceAlpha", 0.5);
%m = fitlm(x, y);
%hold on
%plot(x, m.Fitted, "LineWidth", 2)

function scatterfit(varargin)
 
% Ensure 2 or 3 inputs.
narginchk(2, 3)
 
% We support the calling syntax scatterfit(x, y) or 
% scatterfit(f, x, y), where f is the parent graphics.
switch nargin
    case 2
        f = gcf;
        x = varargin{1};
        y = varargin{2};
    otherwise % case 3
        f = varargin{1};
        x = varargin{2};
        y = varargin{3};
end % switch/case
 
% Create the chart axes and scatter plot.
ax = axes("Parent", f);
scatter(ax, x, y, 6, "filled")
% Compute and create the best-fit line.       
m = fitlm(x, y);
hold(ax, "on")
plot(ax, x, m.Fitted, "LineWidth", 2)
hold(ax, "off")
 
end % scatterfit function