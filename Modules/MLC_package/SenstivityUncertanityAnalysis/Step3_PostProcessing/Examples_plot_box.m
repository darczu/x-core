%% BOX plots % https://uk.mathworks.com/help/stats/compare-grouped-data-using-box-plots.html
clear; close all; clc;
load fisheriris
s1 = meas(1:50,2);
s2 = meas(51:100,2);
%%
figure(1)
boxplot([s1 s2],'Notch','on', ...
        'Labels',{'setosa','versicolor'})

xlim([0.64 2.29])
ylim([2.02 4.20])
grid on

%% Box chart
figure(2)
speciesName = categorical(species(1:100));
sepalWidth = meas(1:100,2);
b = boxchart(speciesName,sepalWidth,'Notch','on');
b.WhiskerLineStyle = '--';

