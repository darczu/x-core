%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Developed by Piotr Darnowski, Warsaw University of Technology
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This file reads all MELCOR massage files.  It also checks EDF file and provides isSuccess about last timestep
% attempts to find failed cases. in this case it covers cases which are expected to cause debris ejection-but fails
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% USER INPUT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXAMPLE
%edffile = 'EDF_5_MH2CR'; 
%colH2 = 2;
%str = 'DEBRIS EJECTION THROUGH FAILED VESSEL ABOUT TO START'
%nCases = 100;
%partial_success = 2500;
%file_diag = 'MELPWR.DIA';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dfile = 'matlab-log.log';
if exist(dfile, 'file'); delete(dfile); end
	diary(dfile)
	diary on
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~exist('root_folder','var')
	root_folder = '.\';
end
root_folder
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~exist('edffile','var')
	edffile = 'EDF_5_MH2CR';
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~exist('nCases','var')
	nCases = 100;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~exist('partial_success','var')
	partial_success = 2500;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~exist('file_diag','var')
    file_diag = 'MELPWR.DIA'
end

%https://uk.mathworks.com/matlabcentral/answers/49414-check-if-a-file-exists
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% END OF USER INPUT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Check in output file if the following phrase is present.
isSuccess = []; idx = []; timeend =[]; FOM = []; isBest = [];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Read case0 - read best/default case
% if flag_read0 == 1
	i = 0; 
	folder = [ root_folder, 'case',num2str(i)];
    content  = fileread([folder,'\',file_diag]) ;
    content2 = importdata([folder,'\',edffile]) ;
    timeend = [timeend; content2(end,1)];   
    %FOM = [FOM; content2(end,2)];
	FOM = [FOM; content2(end,colH2)];
    token0  = regexp( content, str, 'match' )  ;
	token_check0 = ~isempty(token0);	
    isSuccess = [isSuccess;  token_check0];
    idx = [idx; i];    
	data0{1} = content2(:,[1,colH2]);
	isBest = 1;
%end	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
%% read other cases
for i=1:nCases
    content = []; content2 = [];
	folder = [ root_folder, 'case',num2str(i)];    
	content  = fileread([folder,'\',file_diag]) ;
	content2 = importdata([folder,'\',edffile]) ;
	timeend = [timeend; content2(end,1)];
	%FOM = [FOM; content2(end,2)];      
	FOM = [FOM; content2(end,colH2)];      
	%data{i} = content2(:,[1:2]);    	
	data{i} = content2(:,[1,colH2]);    	

	token{i}  = regexp( content, str, 'match' )  ;   
	token_check = ~isempty(token{i});
	isSuccess = [isSuccess;  token_check];
	idx = [idx; i];   
	isBest = [isBest; 0];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Logical controls	
isSuccess;
isPartial = (timeend > partial_success);
isFailed  = ~isSuccess;
isSuccessNotBest = (isSuccess & ~isBest)
isPartialNotBest = (isPartial & ~isBest)
isFailedNotBest  = (isFailed & ~isBest)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
A 		= [idx, isSuccess, isPartial, timeend, FOM]
%A_all  = A(2:end,:); %without best case
A_all = A;
A_best = A(1,:);

% without best
isPartial2 = isPartial(2:end,:);
isSuccess2 = isSuccess(2:end);
isFailed2  = isFailed(2:end);

nSuccess = sum(isSuccess2 == 1);
nFailed = sum(isSuccess2 == 0);
nPartial = sum(isPartial2 == 1); %Patial scuees

ratio = nSuccess/(numel(isSuccess2));
partial_ratio = nPartial/(numel(isSuccess2));
failed_cases = []; success_cases = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
% Arrays with number s of dailed and success casses
	A_failed  = A_all(isSuccess==0,:);
	A_success = A_all(isSuccess==1,:);
    A_partial = A_all(isPartial==1,:);

FOM_success = FOM(isSuccess==1);
FOM_partial = FOM(isPartial==1);
FOM_failed  = FOM(isFailed==1);

timeend_success = timeend(isSuccess==1);
timeend_partial = timeend(isPartial==1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
% Table with all cases
T_all 			= array2table(A_all, 'VariableNames',{'Case','Success','Partial','TimeEnd','FoM'})
    			writetable(T_all,[root_folder, 'Fail-to-success.dat']);

% Best estimate				
T_best 			= array2table(A_best, 'VariableNames',{'Case','Success','Partial','TimeEnd','FoM'})
				writetable(T_best,[root_folder, 'Best.dat']);

% Table with failed cases	
T_failed 		= array2table(A_failed, 'VariableNames',{'Case','Success','Partial','TimeEnd','FoM'})
				writetable(T_failed,[root_folder, 'Failed-cases.dat']);  

% Table with success cases
T_success 		= array2table(A_success, 'VariableNames',{'Case','Success','Partial','TimeEnd','FoM'})
				writetable(T_success,[root_folder, 'Success-cases.dat']);  


% Partial results
T_partial 		= array2table(A_partial, 'VariableNames',{'Case','Success','Partial','TimeEnd','FoM'})
				writetable(T_partial,[root_folder, 'Partial-success-cases.dat']); 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
disp(['Partial sccuess is for cases with t>Time limit:', num2str(partial_success)])
disp(['Estimations - without case0'])
disp(['N = ', num2str(nCases)])
disp(['success = ', num2str(nSuccess)])
disp(['partial success = ', num2str(nPartial)])
disp(['failure = ', num2str(nFailed)])
disp(['success/failure = ', num2str(ratio)])
disp(['partial success/failure = ', num2str(partial_ratio)])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	

% Number of loaded cases	
mCases = size(A_all,1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
diary off