%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Developed by Piotr Darnowski, Warsaw University of Technology
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This file reads all MELCOR massage files.  It also checks EDF file and provides info about last timestep
% attempts to find failed cases. in this case it covers cases which are expected to cause debris ejection-but fails
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% USER INPUT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dfile = 'matlab-log.log';
if exist(dfile, 'file'); delete(dfile); end
	diary(dfile)
	diary on
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~exist('root_folder','var')
	root_folder = '.\';
end
root_folder
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~exist('edffile','var')
	edffile = 'EDF_5_MH2CR';
end
edffile
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~exist('nCases','var')
	nCases = 100;
end
nCases
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~exist('partial_success','var')
	partial_success = 2500;
end
partial_success
str = 'DEBRIS EJECTION THROUGH FAILED VESSEL ABOUT TO START'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% END OF USER INPUT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Check in output file if the following phrase is present.
info = []; idx = []; timeend =[]; FOM = [];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Read case0 - read best/default case
i = 0; 
	folder = [ root_folder, 'case',num2str(i)];
    content  = fileread([folder,'\MELPWR.DIA']) ;
    content2 = importdata([folder,'\',edffile]) ;
    timeend = [timeend; content2(end,1)];   
    FOM = [FOM; content2(end,2)];
    token0  = regexp( content, str, 'match' )  ;
	token_check0 = ~isempty(token0);	
    info = [info;  token_check0];
    idx = [idx; i];    
	data0{1} = content2(:,[1:2]);
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
%% read other cases
for i=1:nCases
    content = []; content2 = [];
	folder = [ root_folder, 'case',num2str(i)];    
	content  = fileread([folder,'\MELPWR.DIA']) ;
	content2 = importdata([folder,'\',edffile]) ;
	timeend = [timeend; content2(end,1)];
	FOM = [FOM; content2(end,2)];      
	data{i} = content2(:,[1:2]);    	

	token{i}  = regexp( content, str, 'match' )  ;   
	token_check = ~isempty(token{i});
	info = [info;  token_check];
	idx = [idx; i];   
end
% FOM matrix without fialed cases
FOMreduced = FOM(info==1);
timeend_reduced = timeend(info==1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
disp('case number,  0 - failed;  1 - success')
A = [idx, info, timeend, FOM]

success = sum(info == 1);
failure = sum(info == 0);
partial = sum(timeend > partial_success); %Patial scuees
ratio = success/(numel(info));
pratio = partial/(numel(info));
failed_cases = []; success_cases = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
% Arrays with number s of dailed and success casses
	A_failed_cases  = A(info==0,:);
	A_success_cases = A(info==1,:);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
% Table with all cases
Tall 			= array2table(A, 'VariableNames',{'Case','Success','TimeEnd','FoM'})
    			writetable(Tall,[root_folder, 'Fail-to-success.dat']);
% Table with failed cases	
Tfailed 		= array2table(A_failed_cases, 'VariableNames',{'CasesFailed'})
				writetable(Tfailed,[root_folder, 'Failed-cases.dat']);  
% Table with success cases
Tsuccess 		= array2table(A_success_cases, 'VariableNames',{'CasesSuccess'})
				writetable(Tsuccess,[root_folder, 'Success-cases.dat']);  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
disp(['Partial scuess is for cases with t>2500s'])
disp(['All estimations include case0'])
disp(['success = ', num2str(success)])
disp(['partial success = ', num2str(partial)])
disp(['failure = ', num2str(failure)])
disp(['success/failure = ', num2str(ratio)])
disp(['partial success/failure = ', num2str(pratio)])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
diary off