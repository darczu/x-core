% Developed by Piotr Mazgaj for NARSIS project
Param =["Molten Material Holdup Parameters "
"Core(Fuel) Component Failure Parameters "
"Candling Heat Transfer Coefficients " 
"Core-Region Particulate Debris Diameter "
"Debris porosity (PORDP)  "
"Radiation Exchange Factors Radial  "
"Radiation Exchange Factors Axial   "
"Molten clad drainage rate "
"Secondary Material Transport Parameters "]

graph_bar_2(RHO_H2_S, PVAL_H2_S, Param, 'Spearman Corr. Coeff. - Total H_2 Production','\rho-value ', 'Coefficient - DP120',...
                'Spearman_H2-2000');
				
				