% ========================================================================
% Version 4 Developed in 06 2019
% Script to generate EDF input for MELCOR inputdeck
% Version for the core calculations is different - In-Vessel Early
% ========================================================================
 EDF_flag = 2;  % Do not change

 
 name = 'EDF_MELCOR_input';
% OutputPath= ['OutputData/',name,'/'];  %output for serpent 
% EDF_filename     = [OutputPath,'EDF_Output_11']; 
 EDF_filename1 = 'EDF_LH_MASS.INP'; %CURENTLY THE SAME DIRECTORY
 EDF_filename2 = 'EDF_LH_TEMP.INP'; %CURENTLY THE SAME DIRECTORY
 EDF_filename3 = 'EDF_LH_MPOOL.INP'; %CURENTLY THE SAME DIRECTORY
 EDF_filename4 = 'EDF_LH_HT.INP'; %CURENTLY THE SAME DIRECTORY
%  EDF_filename1 = 'EDF_INP_MASS'; %CURENTLY THE SAME DIRECTORY
% EDF_filename2 = 'EDF_INP_TEMP'; %CURENTLY THE SAME DIRECTORY
% EDF_filename3 = 'EDF_INP_MPOOL'; %CURENTLY THE SAME DIRECTORY
 % =======================================================================
 time_start = 0.0; %h
 %time_end =   53.0;  %h
 time_end =   30000.0;  %h
 %time_start = time_start*3600; %to seconds
% time_end = time_end*3600;
 time_step = 50.0;   %frequency
 time_step1 = 1e6;    %last step
 % ======================================================================= 
 %MODEL INFO: Narsis GenIII Lower Plenum
    IRMIN = 1;
    IRMAX = 6;
    IAMIN = 1;
    IAMAX = 4;
    NMAX = (IAMAX-IAMIN+1)*IRMAX; %4 for safety
    TYPE = 'PWR'; %Reactor Type
    %CVH = {'CVCORE1','CVCORE2','CVCORE3','CVCORE4','CVBYPASS','CVLOWERPLENUM'}; %CVHs 
	CVH = {'LP'}
	
% Lower Head Data
	EDF_flaf_LH = 1;  %Calculate lower head
	
	NLH = 8;  %Number of temperature nodes in the vessel lower head
	NLHT = 7;  %Number of lower head segments
	NLHTA = 7;	%Number of segments in noncylindrical (flat or hemispherical) portion of  the lower head.
	
	
 % ========================================================================
    Date = date;
    DateString = datestr(Date); %Save the date of the file generatorion
% =========================================================================
% COR PACKAGE IDENTIFICATORS
 MAT = {'CRP' , 'INC' , 'SS', 'SSOX', 'UO2', 'ZR', 'ZRO2'}; %7 materials
 MAT1 = {'CRP', 'SS', 'SSOX', 'UO2', 'ZR', 'ZRO2'}; %6 materials
 MAT_MP = {'OXI','MET'};  %Molten Pools Variables Materials
 KEY_MP = {'CH','BY'};     %Molten Pools Variables keys
 
%CMP = {'FU', 'CL', 'CB', 'CN', 'NS', 'SS', 'SVC', 'SVB', };%Components Short
 COMP_PWR = {'FU', 'CL', 'CB', 'CN', 'NS', 'SS', 'PB','PD',...
             'SH', 'FM' ...
             'MP1','MB1','MP2','MB2','SVC', 'SVB' };  %Components Long

 COMP_PWR1 = {'FU', 'CL', 'CB', 'CN', 'NS', 'SS','PD',...
              'MP1','MP2'};			 
			 
			 
 COMP_BWR = {'FU', 'CL', 'CB', 'CN', 'NS', 'SS', 'PB','PD',...
             'MP1','MB1','MP2','MB2','SVC', 'SVB' };  %Components Long
 
 COMP_BWR1 = {'FU', 'CL', 'CB', 'CN', 'NS', 'SS','PD',...
             'MP1','MP2' };  %Components Short - for DC conglomerate debris calculation
         
    if(strcmp(TYPE,'BWR'))       
        CMP = COMP_BWR;
        CMP1 = COMP_BWR1;
        
    elseif(strcmp(TYPE,'PWR'))   
        CMP = COMP_PWR;
        CMP1 = COMP_PWR1;
    else error('ONLY PWR AND BWR ALLOWED');
    end
    %MP1 - Oxidic molten pool; MP2 - Metallic molten pool
 COMP_MP = {'MP1','MB1','MP2','MB2'};
 COMP_MP1 = {'MP1','MP2'}; %only in-channel
 
%CVH PACK. IDENT.         
   VOL1 = {'FLU','FLUB','FLUC'};
   VOL2 = {'FL','FLB','FLC'};
   CVHF = {'ALL','POOL','ATM','H2O-VAP'}; %CVH fluids
   CVHF2 = {'ALL','POOL','ATM'}; %CVH fluids #2 
% =========================================================================   
   
% =========================================================================
% INTRO PART OF THE FILE
   edf  = fopen(EDF_filename1,'w');   %MASS file
        fprintf(edf,'! Date: %s \n',DateString);
        fprintf(edf,'! === MELCOR EDF OUTPUT FILE === \n');
        fprintf(edf,'! === VERSION 2019 === \n');
        fprintf(edf,'! === ADD TO MELCOR INPUT DECK === \n');
        fprintf(edf,'! Add following line to your MELCOR inputdeck: \n');
        fprintf(edf,'! if there is no EDF_INPUT CARD ===> EDF_INPUT \n');
        fprintf(edf,'! ==> INCLUDE ''FILE_NAME'' ''BLOCK_NAME'' \n\n');
        fprintf(edf,' \n');
% =========================================================================
% GENERATE ONE OUTPUT FILE - NOT USED 2019 ================================
%{   
 if (EDF_flag == 1) 
        %COR-M.m.k.n m - 
        fprintf(edf,'! ===========================================================\n') 
        fprintf(edf,'BLOCK: ''EDF_MASS'' \n\n');
        fprintf(edf,'EDF_ID     ''EDF_MASS''   WRITE ''EDF_MASS'' \n');
        fprintf(edf,'EDF_CHN %d \n\n', IRMAX*IAMAX*7);
        k = 0;
        for iMAT = 1:numel(MAT)
         fprintf(edf,'! ==== MATERIAL: %s \n\n', cell2mat(MAT(iMAT)));   
          for IR=1:IRMAX   %ring
               fprintf(edf,'! ==== RING: %d \n', IR);
               for IA=1:IAMAX  % axial
                 k=k+1;
                 fprintf(edf,'%d \t COR-CellMass(%d,%d,%s) \n', k, IA,IR, cell2mat(MAT(iMAT)) )       
               end
          end
           fprintf(edf,'\n');
        end
        fprintf(edf,'EDF_FMT  ''1P, 150E12.5''\n');
        fprintf(edf,'EDF_TIM            0.0\n');
        fprintf(edf,'EDF_DTW    2 !n  twedf   dtwedf \n');
        fprintf(edf,'     1       %e       %f    \n',time_start,time_step);
        fprintf(edf,'     2       %e       %f    \n',time_end,time_step1);
        end
%}
% =========================================================================
% MULTIPLE FILES MASSES=======================================================
    if (EDF_flag == 2) 
       %COR-M.m.k.n m -
       fprintf(edf,'! ===========================================================\n');
       fprintf(edf,'BLOCK: ''EDF_MASS'' \n');
       fprintf(edf,'! ===========================================================\n\n');
        for i=1:numel(MAT)
            k=0;
            name1 = [' ''EDF_MASS_',cell2mat(MAT(i)),''' '];
            fprintf(edf,'EDF_ID  %s  WRITE  %s \n',name1, name1); 
            fprintf(edf,'EDF_CHN %d \n', IRMAX*IAMAX);
              for IR=1:IRMAX   %ring
               fprintf(edf,'! ==== RING: %d \n', IR);
               for IA=1:IAMAX  % axial
                 k=k+1;
                 fprintf(edf,'%d \t COR-CellMass(%d,%d,%s) \n', k, IA,IR, cell2mat(MAT(i)) );       
               end
              end
           fprintf(edf,'EDF_FMT  ''1P, %sE12.5''\n', num2str(NMAX+1));
           fprintf(edf,'EDF_TIM            0.0\n');
           fprintf(edf,'EDF_DTW    2 !n  twedf   dtwedf \n'); 
           fprintf(edf,'     1       %e       %f    \n',time_start,time_step);
           fprintf(edf,'     2       %e       %f    \n',time_end,time_step1);
           fprintf(edf,'\n\n');
        end 
    end
%% =====================================================================
% 2019 04 ==============================================================
% ADD COR TOTAL MASS - 2019 - FOR BALANCE
% COR-TotMass(MAT) %COR-M<MAT>-TOT 
        fprintf(edf,'! ===========================================================\n');        
        fprintf(edf,'BLOCK: ''EDF_MASS_TOTAL'' \n');
        fprintf(edf,'! ===========================================================\n');
        fprintf(edf,'EDF_ID  ''EDF_MASS_TOTAL''  WRITE  ''EDF_MASS_TOTAL'' \n'); 
        fprintf(edf,'EDF_CHN %d \n', numel(MAT));      
        for i=1:numel(MAT)                  
            fprintf(edf,'%d \t COR-TotMass(%s) \n', i, cell2mat(MAT(i)));
        end
           fprintf(edf,'EDF_FMT  ''1P, %sE12.5''\n', num2str(numel(MAT)+1));
           fprintf(edf,'EDF_TIM            0.0\n');
           fprintf(edf,'EDF_DTW    2 !n  twedf   dtwedf \n'); 
           fprintf(edf,'     1       %e       %f    \n',time_start,time_step);
           fprintf(edf,'     2       %e       %f    \n',time_end,time_step1);
    fprintf(edf,'! ===========================================================\n'); 
    fprintf(edf,'\n\n');

%% =====================================================================   
%% =====================================================================   

%% =====================================================================   
% PARTICLATE DEBRIS MASSES IN CHANNEL
% COR-PD(IA,IR,MAT) Particulate debris mass for material MAT in cell
%(IA,IR). IN CHANNEL !!!
fprintf(edf,'! ===========================================================\n');
fprintf(edf,'BLOCK: ''EDF_MASS_PD'' \n');
fprintf(edf,'! ===========================================================\n');
    for i=1:numel(MAT)
            k=0;
            name1 = [' ''EDF_MASS_PD_',cell2mat(MAT(i)),''' '];
            fprintf(edf,'EDF_ID  %s  WRITE  %s \n',name1, name1); 
            fprintf(edf,'EDF_CHN %d \n', IRMAX*IAMAX);
              for IR=1:IRMAX   %ring
               fprintf(edf,'! ==== RING: %d \n', IR);
               for IA=1:IAMAX  % axial
                 k=k+1;
                 fprintf(edf,'%d \t COR-PD(%d,%d,%s) \n', k, IA,IR, cell2mat(MAT(i)) );       
               end
              end
           fprintf(edf,'EDF_FMT  ''1P, %sE12.5''\n', num2str(NMAX+1));
           fprintf(edf,'EDF_TIM            0.0\n');
           fprintf(edf,'EDF_DTW    2 !n  twedf   dtwedf \n'); 
           fprintf(edf,'     1       %e       %f    \n',time_start,time_step);
           fprintf(edf,'     2       %e       %f    \n',time_end,time_step1);
           fprintf(edf,'\n\n');
    end 
    
%% ===================================================================== 
% NEW 06 2019
% PARTICLATE DEBRIS MASSES IN BYPASS
% COR-PB(IA,IR,MAT) Particulate debris mass for material MAT in cell
%(IA,IR). IN BYPASS.
fprintf(edf,'! ===========================================================\n');
fprintf(edf,'BLOCK: ''EDF_MASS_PB'' \n');
fprintf(edf,'! ===========================================================\n');
    for i=1:numel(MAT)
            k=0;
            name1 = [' ''EDF_MASS_PB_',cell2mat(MAT(i)),''' '];
            fprintf(edf,'EDF_ID  %s  WRITE  %s \n',name1, name1); 
            fprintf(edf,'EDF_CHN %d \n', IRMAX*IAMAX);
              for IR=1:IRMAX   %ring
               fprintf(edf,'! ==== RING: %d \n', IR);
               for IA=1:IAMAX  % axial
                 k=k+1;
                 fprintf(edf,'%d \t COR-PB(%d,%d,%s) \n', k, IA,IR, cell2mat(MAT(i)) );       
               end
              end
           fprintf(edf,'EDF_FMT  ''1P, %sE12.5''\n', num2str(NMAX+1));
           fprintf(edf,'EDF_TIM            0.0\n');
           fprintf(edf,'EDF_DTW    2 !n  twedf   dtwedf \n'); 
           fprintf(edf,'     1       %e       %f    \n',time_start,time_step);
           fprintf(edf,'     2       %e       %f    \n',time_end,time_step1);
           fprintf(edf,'\n\n');
    end 



%% ===================================================================== 
% DEBRIS MASSES
%COR-CellMassDB(IA,IR,MAT) Total debris mass of material MAT in cell (IA,IR). 
%The choices allowed for MAT are SS, SSOX, UO2, ZR, ZRO2, and CRP.
%COR-CellMassDB
fprintf(edf,'! ===========================================================\n');
fprintf(edf,'BLOCK: ''EDF_MASS_DB_TOT'' \n');
fprintf(edf,'! ===========================================================\n');
    for i=1:numel(MAT1)
            k=0;
            name1 = [' ''EDF_MASS_DB_TOT_',cell2mat(MAT1(i)),''' '];
            fprintf(edf,'EDF_ID  %s  WRITE  %s \n',name1, name1); 
            fprintf(edf,'EDF_CHN %d \n', IRMAX*IAMAX);
              for IR=1:IRMAX   %ring
               fprintf(edf,'! ==== RING: %d \n', IR);
               for IA=1:IAMAX  % axial
                 k=k+1;
                 fprintf(edf,'%d \t COR-CellMassDB(%d,%d,%s) \n', k, IA,IR, cell2mat(MAT1(i)) ) ;      
               end
              end
           fprintf(edf,'EDF_FMT  ''1P, %sE12.5''\n', num2str(NMAX+1));
           fprintf(edf,'EDF_TIM            0.0\n');
           fprintf(edf,'EDF_DTW    2 !n  twedf   dtwedf \n'); 
           fprintf(edf,'     1       %e       %f    \n',time_start,time_step);
           fprintf(edf,'     2       %e       %f    \n',time_end,time_step1);
           fprintf(edf,'\n\n');
    end
    

     
    
%% =====================================================================
% === MOLTEN POOL MASSES
% COMP_MP = {'MP1','MB1','MP2','MB2'};  COMP_MP1 = {'MP1','MP2'}; %only channel
%MP1 - Oxidic molten pool; MP2 - Metallic molten pool
%COR-M(IA,IR,COMP,MAT)  
fprintf(edf,'! ===========================================================\n');
fprintf(edf,'BLOCK: ''EDF_MASS_MP+MB'' \n');
fprintf(edf,'! ===========================================================\n');
for j=1:numel(COMP_MP1)
    for i=1:numel(MAT)
            k=0;
            name1 = [' ''EDF_MASS_',cell2mat(COMP_MP1(j)),'_',cell2mat(MAT(i)),''' '];
            fprintf(edf,'EDF_ID  %s  WRITE  %s \n',name1, name1); 
            fprintf(edf,'EDF_CHN %d \n', IRMAX*IAMAX);
              for IR=1:IRMAX   %ring
               fprintf(edf,'! ==== RING: %d \n', IR);
               for IA=1:IAMAX  % axial
                 k=k+1;                   
                 fprintf(edf,'%d \t COR-M(%d,%d,%s,%s) \n', k, IA,IR, cell2mat(COMP_MP1(j)),cell2mat(MAT(i)) )  ;     
               end
              end
           fprintf(edf,'EDF_FMT  ''1P, %sE12.5''\n', num2str(NMAX+1));
           fprintf(edf,'EDF_TIM            0.0\n');
           fprintf(edf,'EDF_DTW    2 !n  twedf   dtwedf \n'); 
           fprintf(edf,'     1       %e       %f    \n',time_start,time_step);
           fprintf(edf,'     2       %e       %f    \n',time_end,time_step1);
           fprintf(edf,'\n\n');
     end 
end





%% =====================================================================
% NEW 2019 06 
% CONGLOMERATE DEBRIS
%COR-DC(IA,IR,COMP,MAT)  Conglomerate debris mass for material MAT on component COMP in cell (IA,IR).
% IT OCCURED THAT DC IS NOT ALLOWED FOR:
% COMPONENTS = PB, MB1,MB2,SVC,SVB
fprintf(edf,'! ===========================================================\n');
fprintf(edf,'BLOCK: ''EDF_MASS_DC'' \n');
fprintf(edf,'! ===========================================================\n');

for j=1:numel(CMP1)
    for i=1:numel(MAT)
            k=0;
            name1 = [' ''EDF_MASS_DC_',cell2mat(CMP1(j)),'_',cell2mat(MAT(i)),''' '];
            fprintf(edf,'EDF_ID  %s  WRITE  %s \n',name1, name1); 
            fprintf(edf,'EDF_CHN %d \n', IRMAX*IAMAX);
              for IR=1:IRMAX   %ring
               fprintf(edf,'! ==== RING: %d \n', IR);
               for IA=1:IAMAX  % axial
                 k=k+1;                   
                 fprintf(edf,'%d \t COR-DC(%d,%d,%s,%s) \n', k, IA,IR, cell2mat(CMP1(j)),cell2mat(MAT(i)) )  ;     
               end
              end
           fprintf(edf,'EDF_FMT  ''1P, %sE12.5''\n', num2str(NMAX+1));
           fprintf(edf,'EDF_TIM            0.0\n');
           fprintf(edf,'EDF_DTW    2 !n  twedf   dtwedf \n'); 
           fprintf(edf,'     1       %e       %f    \n',time_start,time_step);
           fprintf(edf,'     2       %e       %f    \n',time_end,time_step1);
           fprintf(edf,'\n\n');
     end 
end


% ========================================================================
fclose(edf);
% ========================================================================





 
%% ========================================================================
%Temperatures EDF part - additional - VERSION_v2
% 5.3  Core Temperature and Melt Fraction Variables
edf = fopen(EDF_filename2,'w');  %TEMP file
        fprintf(edf,'! Date: %s \n',DateString);
        fprintf(edf,'! === MELCOR EDF OUTPUT FILE === \n');
        fprintf(edf,'! === VERSION 2019 === \n');
        fprintf(edf,'! === ADD TO MELCOR INPUT DECK === \n');
        fprintf(edf,'! Add following line to your MELCOR inputdeck: \n');
        fprintf(edf,'! if there is no EDF_INPUT CARD ===> EDF_INPUT \n');
        fprintf(edf,'! ==> INCLUDE ''FILE_NAME'' ''BLOCK_NAME'' \n\n');
        fprintf(edf,' \n');
        
 %% ========================================================================      
fprintf(edf,'! ===========================================================\n');
fprintf(edf,'BLOCK: ''EDF_TEMP'' \n');
fprintf(edf,'! ===========================================================\n');
% COR-CellTemp (IA,IR,COMP)
  for i=1:numel(CMP)
      k=0;
      name1 = [' ''EDF_TEMP_',cell2mat(CMP(i)),''' '];
      fprintf(edf,'EDF_ID  %s  WRITE  %s \n',name1, name1);        
      fprintf(edf,'EDF_CHN %d \n', NMAX);
       for IR=1:IRMAX   %ring
         fprintf(edf,'! ==== RING: %d \n', IR);
         for IA=IAMIN:IAMAX  % axial
           k=k+1;
           fprintf(edf,'%d \t COR-CellTemp(%d,%d,%s) \n', k, IA,IR, cell2mat(CMP(i)) ) ;      
		   % COMP='SS'  supporting structure
         end
       end
       fprintf(edf,'EDF_FMT  ''1P, %sE12.5''\n', num2str(NMAX+1));
       fprintf(edf,'EDF_TIM            0.0\n');
       fprintf(edf,'EDF_DTW    2 !n  twedf   dtwedf \n'); 
       fprintf(edf,'     1       %e       %f    \n',time_start,time_step);
       fprintf(edf,'     2       %e       %f    \n',time_end,time_step1);
       fprintf(edf,'\n\n');     
  end
  
fprintf(edf,'\n\n');  
% ========================================================================
fclose(edf);
% ========================================================================

%% =========================================================================
%% =========================================================================
%% =========================================================================

%% =========================================================================
%5.9  Convecting Molten Pool Variables

% INTRO PART OF THE FILE
edf = fopen(EDF_filename3,'w');  %MP file
        fprintf(edf,'! Date: %s \n',DateString);
        fprintf(edf,'! === MELCOR EDF OUTPUT FILE === \n');
        fprintf(edf,'! === VERSION 2019 === \n');
        fprintf(edf,'! === ADD TO MELCOR INPUT DECK === \n');
        fprintf(edf,'! Add following line to your MELCOR inputdeck: \n');
        fprintf(edf,'! if there is no EDF_INPUT CARD ===> EDF_INPUT \n');
        fprintf(edf,'! ==> INCLUDE ''FILE_NAME'' ''BLOCK_NAME'' \n\n');
        fprintf(edf,' \n');


fprintf(edf,'! ===========================================================\n');
fprintf(edf,'BLOCK: ''EDF_CPOOL_VARIABLES'' \n');
fprintf(edf,'! ===========================================================\n');
        
% MAT_MP = {'OXI','MET'};  %Molten Pools Variables Materials
% KEY_MP = {'CH','BY'};     %Molten Pools Variables keys
%% =======================================================================
%COR-T-LP(MAT,key)

dummy = numel(KEY_MP)*numel(MAT_MP);
            k=0;
            name1 = [' ''EDF_MPOOL_T_LP'' '];
            fprintf(edf,'EDF_ID  %s  WRITE  %s \n',name1, name1); 
            fprintf(edf,'EDF_CHN %d \n', dummy);
            for j=1:numel(KEY_MP)
                for i=1:numel(MAT_MP)   
                 k=k+1;
                 fprintf(edf,'%d \t COR-T-LP(%s,%s) \n', k, cell2mat(MAT_MP(i)), cell2mat(KEY_MP(j)));       
                end 
            end 
           fprintf(edf,'EDF_FMT  ''1P, %sE12.5''\n', num2str(dummy+1));
           fprintf(edf,'EDF_TIM            0.0\n');
           fprintf(edf,'EDF_DTW    2 !n  twedf   dtwedf \n'); 
           fprintf(edf,'     1       %e       %f    \n',time_start,time_step);
           fprintf(edf,'     2       %e       %f    \n',time_end,time_step1);
           fprintf(edf,'\n\n');
fprintf(edf,'! ===========================================================\n');

%% =======================================================================
%COR-M-LP(MAT,key)
            k=0;
            name1 = [' ''EDF_MPOOL_M_LP'' '];
            fprintf(edf,'EDF_ID  %s  WRITE  %s \n',name1, name1); 
            fprintf(edf,'EDF_CHN %d \n', dummy);
            for j=1:numel(KEY_MP)
                for i=1:numel(MAT_MP)   
                 k=k+1;
                 fprintf(edf,'%d \t COR-M-LP(%s,%s) \n', k, cell2mat(MAT_MP(i)), cell2mat(KEY_MP(j))) ;      
                end 
            end 
           fprintf(edf,'EDF_FMT  ''1P, %sE12.5''\n', num2str(dummy+1));
           fprintf(edf,'EDF_TIM            0.0\n');
           fprintf(edf,'EDF_DTW    2 !n  twedf   dtwedf \n'); 
           fprintf(edf,'     1       %e       %f    \n',time_start,time_step);
           fprintf(edf,'     2       %e       %f    \n',time_end,time_step1);
           fprintf(edf,'\n\n');
fprintf(edf,'! ===========================================================\n');


%% =======================================================================
%COR-V-LP(MAT,key)
            k=0;
            name1 = [' ''EDF_MPOOL_V_LP'' '];
            fprintf(edf,'EDF_ID  %s  WRITE  %s \n',name1, name1); 
            fprintf(edf,'EDF_CHN %d \n', dummy);
            for j=1:numel(KEY_MP)
                for i=1:numel(MAT_MP)   
                 k=k+1;
                 fprintf(edf,'%d \t COR-V-LP(%s,%s) \n', k, cell2mat(MAT_MP(i)), cell2mat(KEY_MP(j)))     ;  
                end 
            end 
           fprintf(edf,'EDF_FMT  ''1P, %sE12.5''\n', num2str(dummy+1));
           fprintf(edf,'EDF_TIM            0.0\n');
           fprintf(edf,'EDF_DTW    2 !n  twedf   dtwedf \n'); 
           fprintf(edf,'     1       %e       %f    \n',time_start,time_step);
           fprintf(edf,'     2       %e       %f    \n',time_end,time_step1);
           fprintf(edf,'\n\n');
fprintf(edf,'! ===========================================================\n');


%% =======================================================================
%COR-RA-LP(MAT,key)
            k=0;
            name1 = [' ''EDF_MPOOL_RA_LP'' '];
            fprintf(edf,'EDF_ID  %s  WRITE  %s \n',name1, name1); 
            fprintf(edf,'EDF_CHN %d \n', dummy);
            for j=1:numel(KEY_MP)
                for i=1:numel(MAT_MP)   
                 k=k+1;
                 fprintf(edf,'%d \t COR-RA-LP(%s,%s) \n', k, cell2mat(MAT_MP(i)), cell2mat(KEY_MP(j)))       ;
                end 
            end 
           fprintf(edf,'EDF_FMT  ''1P, %sE12.5''\n', num2str(dummy+1));
           fprintf(edf,'EDF_TIM            0.0\n');
           fprintf(edf,'EDF_DTW    2 !n  twedf   dtwedf \n'); 
           fprintf(edf,'     1       %e       %f    \n',time_start,time_step);
           fprintf(edf,'     2       %e       %f    \n',time_end,time_step1);
           fprintf(edf,'\n\n');
fprintf(edf,'! ===========================================================\n');
% ========================================================================
% ========================================================================
% ========================================================================
% NEW 2019 additiona lower head output
% ========================================================================
% ========================================================================
% ========================================================================
% In the following three variables, ijj refers to node jj of segment i, where jj = 01 is on the 
% outside of the lower head. (Although nodal information on COR_LHN record is entered from 
% inside to outside, the nodes are actually numbered in the opposite direction.
COR-TLH.ijj











% ========================================================================
fclose(edf);
% ========================================================================
