% Matlab Script Dedicated to Merge files
function [T, a]=merge2(file, fileRST, fileMRG,var,format)

%[file var]
a1    =    importdata([file var]);
a1RST =    importdata([fileRST var]);
t1    =    a1(:,1);
t1RST =    a1RST(:,1);

T = [t1; t1RST];
a = [a1(:,2:end); a1RST(:,2:end)];
x = [T, a]';  %' musialo byc wstawione zeby dzialac!
size(x)
%x1 = x()
%fileID = fopen([fileMRG, var, '.dat'],'w');
fileID = fopen([fileMRG, var],'w');
%fprintf(fileID,'%6s %12s\n','x','exp(x)');

fprintf(fileID,format,x);


%fprintf(fileID,'',x);


fclose(fileID);

%{
if (flag2 == 1)
    print('-djpeg100','-r200',[file '/Fig1.jpg'])
end
xlabel('Time, [s]');
xlabel('Time, [h]');
%}