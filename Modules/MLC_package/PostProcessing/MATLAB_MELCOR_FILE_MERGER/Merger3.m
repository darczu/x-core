% Skrypt odpalania funkcji merge
close all;
clear; clc;
file = '1ST/EPR0205_F';
fileRST = 'RST/EPR0205_F_RST';
fileMRG = 'MRG/EPR0205_F_MRG'; 
var=[];
var{1} = '/1_P561';
var{2} = '/2_MWPS_MWPZ';
var{3} = '/4_MCMTPS'; %< to samo
var{4} = '/4_MWPS_MWPZ';
var{5} = '/4_MWPS_MWPZ'; %< to samo
var{6} = '/6_WH2CR';
var{7} = '/7a_TRICMAX';
var{8} = '/8_TGPZ_TWPZ';
var{9} = '/9_ZWUS_ZWBS';
var{10} = '/10_TGBS_TGUS';
var{11} = '/11_PBS_PUS';
var{12} = '/12_PRB';
var{13} = '/13_MSTRBTOT';
var{14} = '/14_MH2RBTOT';
var{15} = '/15_ZWPZ';
var{16} = '/16_CORELEV';
%var(17) = '/';
var2=[];
var1 = '/1_P561';

nend=16
for i=1:nend
    if(i~=4 & i~=5)
    
     format = '%12.5e %12.5e\n';
     if(i==7 | i==11 | i==8 | i==9 || i==10 ) format = '%12.5e %12.5e %12.5e\n'; end
     if(i==16) format = '%12.5e %12.5e %12.5e %12.5e\n'; end
         
    
     var2 = var{i};
     [t1, a1] = merge2(file,fileRST,fileMRG,var2,format);
     figure(i)
     plot(t1,a1,'LineWidth',2)
    end
end


%hold on
%b  =    importdata([fileMRG '/1_P561.dat'],' ');
%plot(b(:,1),b(:,2),'xr');