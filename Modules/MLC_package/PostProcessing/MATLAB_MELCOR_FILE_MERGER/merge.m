% Matlab Script Dedicated to Merge files
function [T, a]=merge(file, fileRST, fileMRG)

a1    =    importdata([file '/1_P561']);
a1RST =    importdata([fileRST '/1_P561']);
t1    =    a1(:,1);
t1RST =    a1RST(:,1);

T = [t1; t1RST];
a = [a1(:,2:end); a1RST(:,2:end)];
x = [T, a]';  %' musialo byc wstawione zeby dzialac!

fileID = fopen([fileMRG, '/1_P561.dat'],'w');
%fprintf(fileID,'%6s %12s\n','x','exp(x)');
fprintf(fileID,'%12.5e %12.5e\n',x);
fclose(fileID);

