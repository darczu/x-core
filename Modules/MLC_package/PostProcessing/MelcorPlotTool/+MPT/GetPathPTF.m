function [filepaths, listing] = GetPathPTF(pathPTF)

n = length(pathPTF);
%base_folder = cd;

for i=1:n
    folder = pathPTF{i};  % path cell to string

    if i == 1
        listingPTF = dir([folder, '\**\*.ptf']);
    else
        listingPTF0 = dir([folder, '\**\*.ptf']);

        listingPTF = [listingPTF; listingPTF0];
    end

end

listing = listingPTF;

filepaths = {};

m = numel(listing);

for j=1:m 
   % filepaths(j) = fullfile(listing(j).folder,listing(j).name)
   filepaths{j} = [ listing(j).folder, '\', listing(j).name];
   %filepaths(j) = fullfile(listing(j).folder,listing(j).name)
   
end



%[ listingPTF(1).folder, listingPTF(1).name]
 % https://ch.mathworks.com/matlabcentral/answers/32038-can-you-use-dir-to-list-files-in-subfolders