%% Provide path with melcor results

pathAPT = 'E:\DARO\APTPLOT\bin\AptPlot.exe';

% Provide cell with folders with results - can be several subfolders
pathPTF = { 'C:\ROOT_MELCOR\FPT1_2023\E9_M2221', ...
            'C:\ROOT_MELCOR\FPT1_2023\E9_2023r1', ...
            'C:\ROOT_MELCOR\FPT1_2023\E8_test_eutectic', ...
            };

% Provide MELCOR variable names            
melcorParams = { 'COR-DMH2-TOT' };



% ptf_filepath = {'xxx.ptf'};








