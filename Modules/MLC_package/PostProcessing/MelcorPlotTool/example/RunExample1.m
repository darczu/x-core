% Test for the MelPlotTool
clear all; close all; clc;
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% INPUT PART
% Provide input file with path or filepath and parameters to read

% Input file location
path = 'C:\OneDrive\OneDrive - Politechnika Warszawska\git\MY_PROJECTS\MATLAB_XCore\x-core\Modules\MLC_package\PostProcessing\MelcorPlotTool\Example\';
% Inputfile name
inputfile = 'InputExample1.m';




%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load inputfiles with info about folders and setup
[pathAPT, pathPTF, melcorParams] = MPT.LoadInputMPT([path, inputfile]);   %run([path, inputfile]);
% paths for APT exe, folders with PTF results, melcor parameters
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get path with ptf files inside the folder
[filepathPTF, listingPTF] = MPT.GetPathPTF(pathPTF); 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read PTF files in the folder
%MPT.ReadInput(filepath)

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate APTPLOT scripts to read data
%MPT.GenerateAptPlotScript()

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%MPT.ExecuteAptPlotScript()
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%