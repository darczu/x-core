clear; clc; close all;
format short g

% ==============================================================================
% FLAGS
flag1 = 0; %Read initial state data 1-YES 0-NO
flag2 = 1; %SAVE FIGURE 1-YES 0-NO

% Color maps - %cool lines, colorcube
	cmap5 = lines(5); cmap11 = hsv(11);

% ==============================================================================
% BASIC FOLDER
code1 = 'M2.2.11932';		%Code version
folder1 = 'RESULTS_MELCOR';	%Location of MELCOR results
READ_TESTS = [4, 13, 15, 21, 24]; %Read Exp Data

case004 = 'CFT04_0003';
case021 = 'CFT21_0003';


case0 = 21;
case1 = case021; 



labels_CV = { 'CV001' 'CV021' 'CV100' 'CV101' 'CV1021' 'CV300' };
labels_FL = {'FL020' 'FL021' 'FL100' 'FL1021' 'FL2021'};

% ==============================================================================
% READ DATA

 run INITIAL_READ;
 
% ==============================================================================
%ERRORS 
%	EXP_dP = ones(size(EXP_P1,1),1).*0.9E5; %Pressure Error

% ==============================================================================
% RESULTS 
if (flag1 == 0)
	a=0;
	run FIGURES.m
end

% ==============================================================================
%end

