

% ===================================================================
% FIGURE - Pressure
% ===================================================================
a = a+1;
figure(a)
hold on; set(0, 'DefaulttextInterpreter', 'none');
title('Pressure');
name = ['Fig_', num2str(a)];
% ===================================================================
% MELCOR

labels_CV = { 'CV001' 'CV002' 'CV021' 'CV100' 'CV101' 'CV1021' 'CV300' };

t1  = EDF_P(:,1);   %time
y11 = EDF_P(:,2);	%CV001 top
y12 = EDF_P(:,3);	%CV002 top
y13 = EDF_P(:,4);	%CV021 bottom vessel
y14 = EDF_P(:,5);	%CV100 discharge pipe
y15 = EDF_P(:,6);   %CV101 vavle
y16 = EDF_P(:,7);   %CV1021 nozzle
y17 = EDF_P(:,8);	%CV300 cont.


%%plot(t1,y11,'-r','LineWidth',1,'MarkerSize',2); labels = [labels, labels_CV(1)];
plot(t1,y11,'-r','LineWidth',1,'MarkerSize',2); labels = [labels, 'MELCOR'];
%plot(t1,y12,'-b','LineWidth',1,'MarkerSize',2); labels = [labels, labels_CV(2)];
% plot(t1,y13,'-g','LineWidth',1,'MarkerSize',2); labels = [labels, labels_CV(3)];
% plot(t1,y14,'-k','LineWidth',1,'MarkerSize',2); labels = [labels, labels_CV(4)];
%%% plot(t1,y15,'-c','LineWidth',1,'MarkerSize',2); labels = [labels, labels_CV(5)];
%plot(t1,y16,'-m','LineWidth',1,'MarkerSize',2); labels = [labels, labels_CV(6)];
% plot(t1,y17,'-y','LineWidth',1,'MarkerSize',2); labels = [labels, labels_CV(7)];
labels = [labels];

%TRACE Pawel
tT1 = EDFT_P(:,1);
yT1 = EDFT_P(:,2);

plot(tT1,yT1,'-b','LineWidth',1,'MarkerSize',2); labels = [labels, 'TRACE'];


labels = [labels];

if case0 == 21
    tM1 = EDFM1_P(:,1);
    yM1 = EDFM1_P(:,2);
    plot(tM1,yM1,'--g','LineWidth',1,'MarkerSize',2); labels = [labels, 'MELCOR REF'];
end

if case0 ~= 21
% TRACE NRC
    tT2 = EDFT2_P(:,1);
    yT2 = EDFT2_P(:,2);
    plot(tT2,yT2,'--c','LineWidth',1,'MarkerSize',2); labels = [labels, 'TRACE REF'];
end

% ===================================================================
%EXPERIMENTAL

%t2  = EXP_P0(:,1);
%y21 = EXP_P0(:,2);
%plot(t2,y21,'ko','LineWidth',1,'MarkerSize',2);
%errorbar(t2,y21,EXP_dP,'ko','LineWidth',0.5,'MarkerSize',1);


tP1 = EXP_P1(:,1);
tP2 = EXP_P2(:,1);
tP3 = EXP_P3(:,1);
tP4 = EXP_P4(:,1);
tP5 = EXP_P5(:,1);

yP1 = EXP_P1(:,2).*1e3;
yP2 = EXP_P2(:,2).*1e3;
yP3 = EXP_P3(:,2).*1e3;
yP4 = EXP_P4(:,2).*1e3;
yP5 = EXP_P5(:,2).*1e3;

% 001M101 	23.13
% 001M104 	0.53
% 002M107 	-0.63
% 003M108 	-2.73
% 004M109 	-4.868


%%%plot(tP1,yP1,'ko','LineWidth',1,'MarkerSize',2); labels = [labels, p_labels1(1)];

plot(tP1,yP1,'ko','LineWidth',1,'MarkerSize',2); labels = [labels, 'Exp'];

%plot(tP2,yP2,'ro','LineWidth',1,'MarkerSize',2); labels = [labels, p_labels1(2)];
%plot(tP3,yP3,'go','LineWidth',1,'MarkerSize',2); labels = [labels, p_labels1(3)];
%plot(tP4,yP4,'bo','LineWidth',1,'MarkerSize',2); labels = [labels, p_labels1(4)];
%%% plot(tP5,yP5,'mo','LineWidth',1,'MarkerSize',2); labels = [labels, p_labels1(5)];


%labels = [labels, 'file058', 'file059', 'file060', 'file061'];
%labels = [labels, fileP1, fileP2, fileP3, fileP4];
%labels = [labels, fileP1, fileP2, fileP3, fileP4];


% ===================================================================
xlabel('Time, [s]'); ylabel('Pressure, [Pa]');
legend('Location','northeast');
box on; %ylim([0 inf]);   
grid on;  
%ylim([0.0 inf]);
ylim([0.0 6e6]);
xlim([-10, t_end]);
legend(labels,'Interpreter','none');
annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
	print('-dtiff',[folder1,'/',case1,'/', name, code1, case1, '.tiff'])
    savefig([folder1,'/',case1,'/', name, code1, case1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end


%{
% ===================================================================
% ===================================================================
% ===================================================================
% FIGURE - Temperatures
% ===================================================================
a = a+1;
figure(a)
hold on; set(0, 'DefaulttextInterpreter', 'none');
title('Temperature');
name = ['Fig_', num2str(a)];
% ===================================================================
% MELCOR
t2  = EDF_T(:,1) ;   %time
y21 = EDF_T(:,2) ; %CV001 liquid top
y22 = EDF_T(:,9) ; %CV001 vapour top
y23 = EDF_T(:,16); 	%CV001 sat liq top
y24 = EDF_T(:,23); 	%CV001 sat vapour top
labels =[];
CV2_labels = {'CV001 Pool' 'CV001 Atm' 'CV001 Sat Pool' 'CV001 Sat atm'}
plot(t2,y21,'-','LineWidth',1,'MarkerSize',2); labels = [labels, CV2_labels(1)];
plot(t2,y22,'-','LineWidth',1,'MarkerSize',2); labels = [labels, CV2_labels(2)];
plot(t2,y23,'-','LineWidth',1,'MarkerSize',2); labels = [labels, CV2_labels(3)];
plot(t2,y24,'-','LineWidth',1,'MarkerSize',2); labels = [labels, CV2_labels(4)];







% ===================================================================
% EXPERIMENTAL

tT1 = EXP_T1(:,1); 
tT2 = EXP_T2(:,1);
%tT4 = EXP_T4.data(:,1);
%tT5 = EXP_T5.data(:,1);

yT1 = EXP_T1(:,2)+273.15;
yT2 = EXP_T2(:,2)+273.15;
%yT3 = EXP_T3.data(:,2)+273.15;
%yT4 = EXP_T4.data(:,2)+273.15;
%yT5 = EXP_T5.data(:,2)+273.15;


plot(tT1,yT1,'ko','LineWidth',1,'MarkerSize',2); labels = [labels, T_labels1(1)];
%plot(tT2,yT2,'ro','LineWidth',1,'MarkerSize',2); labels = [labels, T_labels1(2)];
%plot(tT3,yT3,'go','LineWidth',1,'MarkerSize',2); labels = [labels, T_labels1(3)];
%plot(tT4,yT4,'mo','LineWidth',1,'MarkerSize',2); labels = [labels, T_labels1(4)];
%plot(tT5,yT5,'co','LineWidth',1,'MarkerSize',2); labels = [labels, T_labels1(5)];



% ===================================================================
xlabel('Time, [s]'); ylabel('Temperature, [K]');
legend('Location','northeast');
box on; %ylim([0 inf]);   
grid on;  
%ylim([0.0 inf]);
xlim([-10, t_end]);
legend(labels,'Interpreter','none');
annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
	print('-dtiff',[folder1,'/',case1,'/', name, code1, case1, '.tiff'])
    savefig([folder1,'/',case1,'/', name, code1, case1,'.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end

%}
% ===================================================================
% ===================================================================
% ===================================================================
% FIGURE - Sub-Cooling
% ===================================================================




% ===================================================================
% ===================================================================
% ===================================================================
% FIGURE - Mass Flow Rate
% ===================================================================
a = a+1;
figure(a)
hold on; set(0, 'DefaulttextInterpreter', 'none');
title('Mass Flow');
name = ['Fig_', num2str(a)];
% ===================================================================
% MELCOR
t3  = EDF_M(:,1);   %time
%y31 = EDF_M(:,2);	%FL020
%y32 = EDF_M(:,3);	%FL021
y33 = EDF_M(:,4);	%FL100
y34 = EDF_M(:,5);	%FL1021
y35 = EDF_M(:,6);	%FL2021

%plot(t2,y31,'-r','LineWidth',1,'MarkerSize',2);
%plot(t2,y32,'-b','LineWidth',1,'MarkerSize',2);
%%% plot(t3,y33,'-g','LineWidth',1,'MarkerSize',2);
%%% plot(t3,y34,'-c','LineWidth',1,'MarkerSize',2);
plot(t3,y35,'-r','LineWidth',1,'MarkerSize',2);


%labels = {'FL020' 'FL021' 'FL100' 'FL1021' 'FL2021'};
%%% labels = {'FL100' 'FL1021' 'FL2021'};
labels = {'MELCOR'}; labels = [labels];

tT3 = EDFT_M(:,1);
yT3 = EDFT_M(:,2);

plot(tT3,yT3,'-b','LineWidth',1,'MarkerSize',2); labels = [labels, 'TRACE'];

labels = [labels];

if case0 == 21
	%EDFM1_P = importdata([folderM1,'/', case1, '/pressure.dat']);		%Pressure
    %EDFM1_M = importdata([folderM1,'/', case1, '/flow.dat']);		%Pressure
    
    tM1 = EDFM1_M(:,1);
    yM1 = EDFM1_M(:,2);
    
    plot(tM1,yM1,'--g','LineWidth',1,'MarkerSize',2); labels = [labels, 'MELCOR REF'];


    tT3 = EDFT3_M(:,1);
    yT3 = EDFT3_M(:,2);
    plot(tT3,yT3,'--m','LineWidth',1,'MarkerSize',2); labels = [labels, 'TRACE REF 2'];
    
end


if case0 ~= 21
    % TRACE NRC
    tT2 = EDFT2_M(:,1);
    yT2 = EDFT2_M(:,2);
    plot(tT2,yT2,'--c','LineWidth',1,'MarkerSize',2); labels = [labels, 'TRACE REF'];
    
end

% ===================================================================
% EXPERIMENTAL
tM1 = EXP_M1(:,1); 
yM1 = EXP_M1(:,2);

plot(tM1,yM1,'ko','LineWidth',1,'MarkerSize',2);


%labels = [labels, 'Pitot'];
labels = [labels, 'Exp'];

% ===================================================================
xlabel('Time, [s]'); ylabel('Mass Flow, [kg/s]');
legend('Location','northeast');
box on; %ylim([0 inf]);   
grid on;  
%ylim([0.0 inf]);
xlim([-10, t_end]); ylim([0.0 14000]);

legend(labels,'Interpreter','none');
annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
	print('-dtiff',[folder1,'/',case1,'/', name, code1, case1, '.tiff'])
    savefig([folder1,'/',case1,'/', name, code1, case1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end
% ===================================================================

%end
