clear; clc; close all;
format short g

% ==============================================================================
% FLAGS
flag1 = 0; %Read initial state data 1-YES 0-NO
flag2 = 1; %SAVE FIGURE 1-YES 0-NO

% Color maps - %cool lines, colorcube
	cmap5 = lines(5); cmap11 = hsv(11);

% ==============================================================================
% BASIC FOLDER
code1 = 'M2.2.11932';		%Code version
folder1 = 'RESULTS_MELCOR/CFT_0101/';	%Location of MELCOR results
folderT1 = 'RESULTS_TRACE/01/';  %TRACE results Pawel D
folderT2 = 'RESULTS_TRACE/TRACE_ASSESSMENT/';  %TRACE results Assessemnt Document
folderT3 = 'RESULTS_TRACE/TRACE_TOMASZ/';  %Wyniki Tomasza - narazei tylko CFT21

folderM1 = 'RESULTS_MELCOR/SNL_MELCOR/';  %MELCOR results
%folder1 = 'RESULTS_MELCOR/';	%Location of MELCOR results
%READ_TESTS = [4, 13, 15, 21, 24]; %Read Exp Data
READ_TESTS = [4, 15, 21, 24]; %Read Exp Data
%READ_TESTS = [21];
%READ_TESTS1 = {'CFT21'}
READ_TESTS1 = { 'CFT04' ...
 'CFT15' ...
 'CFT21' ...
 'CFT24'};
 

 
%TIME_END = [100, 200, 100, 100, 100];
%TIME_END = [100, 100, 100, 100, 100];
TIME_END = [70, 60, 60, 60];
%TIME_END = 60;

a=0;
for g=1:numel(READ_TESTS)
	  case0 = READ_TESTS(g);
	  case1 = READ_TESTS1{g}
		t_end = TIME_END(g);

	%READ_TESTS=1:1:27;
	% case004 = 'CFT04_0014';
	% case021 = 'CFT21_0012a';
	% case021 = 'CFT21_0014a';
	% case013 = 'CFT13_0014';
	% case015 = 'CFT15_0014';
	% case024 = 'CFT24_0014';
	% 
	% case0 = 4;
	% case1 = case004; 
	% 
	% %case0 = 21;
	% %case1 = case021; 
	% 
	% case0 = 13;
	% case1 = case013;
	% 
	% case0 = 15;
	% case1 = case015;
	% 
	% case0 = 24;
	% case1 = case024;


	% 108	93	42	49	148	146	55	60	48	69	54	88


	%labels_CV = { 'CV001' 'CV002' 'CV021' 'CV100' 'CV101' 'CV1021' 'CV300' };
	labels_FL = {'FL020' 'FL021' 'FL100' 'FL1000' 'FL2000'};

	% ==============================================================================
	% READ DATA

	 run INITIAL_READv3;
	 
	% ==============================================================================
	%ERRORS 
	%	EXP_dP = ones(size(EXP_P1,1),1).*0.9E5; %Pressure Error

	% ==============================================================================
	% RESULTS 
	if (flag1 == 0)
		
		run FIGURESv3.m
	end
 
% ==============================================================================
end

