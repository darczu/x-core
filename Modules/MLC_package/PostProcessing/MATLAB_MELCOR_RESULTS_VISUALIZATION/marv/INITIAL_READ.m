% Read file name and elevation to find temperature
load TEST_DATA; 
xlsx_file = 'CHANNELS.xlsx';
[T_elev, xx]    = xlsread(xlsx_file,'Basic','B2:B27');		%Read elevation from Channels file - Temps.
[xx, T_files]  = xlsread(xlsx_file,'Basic','E2:AE27');		%Read filenames from Channels file - Temps.
[xx, T_labels] = xlsread(xlsx_file,'Basic','A2:A27');		%Read Temperature Labels

[p_elev, xx]    = xlsread(xlsx_file,'Basic','B31:B35');		%Read elevation from Channels file - Flows.
[xx, p_files]  = xlsread(xlsx_file,'Basic','E31:AE35');		%Read filenames from Channels file - Pressures
[xx, p_labels] = xlsread(xlsx_file,'Basic','A31:A35');		%Read Pressure Labels

[xx, m_files]  = xlsread(xlsx_file,'Basic','E39:AE39');		%Read filenames from Channels file - Flow Rates

labels = {};
T_labels1 ={}; p_labels1={}; m_labels1 ={};
% ==============================================================================
% Read experimental data
delim = ' '; header = 16; %Initial file part - not to read
	
for j=1:numel(READ_TESTS)   %loop for initial 
	TEST = READ_TESTS(j);    %current test 	
	if(TEST<10) 	
		folder2 = ['TEST_0',num2str(TEST)]; 
	else 
		folder2 = ['TEST_',num2str(TEST)]; 
	end

% ==============================================================================		
	T_fileTEST 	= T_files(:,TEST); %file names with temperatures for test TEST 
	N_Tfiles = numel(T_fileTEST);
	T_initial = [];
	for i=1:N_Tfiles  %Loop for files with Temperature
		file =[]; filename1 =[]; filename2=[];
		filename1 = char(T_fileTEST(i));
		filename2 = [folder2,'/',filename1];
		
		file	  = importdata(filename2,delim,header);
		
		% Initial temperature data
		if (flag1 == 1)
			T_initial = [T_initial; file.data(1,2)+273.15 ];
		else
		% Normal data - Experimental Data for Temperatures
			if(case0 == TEST)
				switch i
					case	1		%T. +20.54
						EXP_T1 = file.data;
						T_labels1 = [T_labels1, T_labels(i)];
					case	2		%T. +19.56
						EXP_T2 = file.data;
						T_labels1 = [T_labels1, T_labels(i)];
					case    22		%T. +0.74m
						EXP_T3 = file.data;
						T_labels1 = [T_labels1, T_labels(i)];
					case	23		%T. INTRUMENTATION RING 1 -0.63m
						EXP_T4 = file.data;
						T_labels1 = [T_labels1, T_labels(i)];
					case	24		%INSTRUMENTATION RING 2  -2.73m
						EXP_T5 = file.data;
						T_labels1 = [T_labels1, T_labels(i)];
					case	25      %0.7 M UPSTREAM OF NOZZLE ENTRANCE -4.868m
						EXP_T6 = file.data;
						T_labels1 = [T_labels1, T_labels(i)];
					case	26		%-5.543m NOZZLE ENTRANCE 
						EXP_T7 = file.data;
						T_labels1 = [T_labels1, T_labels(i)];
					otherwise
					
				end
			end
		end
	end

% ==============================================================================			
	p_fileTEST 	= p_files(:,TEST); %file names with temperatures for test TEST 
	N_pfiles = numel(p_fileTEST);
	p_initial = [];
	
	for i=1:N_pfiles  %Loop for files with Temperature
		file =[]; filename1 =[]; filename2=[];
		filename1 = char(p_fileTEST(i));
		filename2 = [folder2,'/',filename1];
		file	  = importdata(filename2,delim,header);
		
		% Initial temperature data
		if (flag1 == 1)
			p_initial = [p_initial; file.data(1,2).*1e+3 ];
		else
		% Normal data - Experimental Data for Temperatures
			if(case0 == TEST)
				switch i
					case	1		% +23.13
						EXP_P1 = file.data;
						p_labels1 = [p_labels1, p_labels(i)];
					case	2		% +0.53
						EXP_P2 = file.data;
						p_labels1 = [p_labels1, p_labels(i)];
					case    3		% -0.63
						EXP_P3 = file.data;
						p_labels1 = [p_labels1, p_labels(i)];
					case	4		% -2.73
						EXP_P4 = file.data;
						p_labels1 = [p_labels1, p_labels(i)];
					case	5		% -4.86
						EXP_P5 = file.data;
						p_labels1 = [p_labels1, p_labels(i)];
					otherwise
				end
			end
		end
	end	
	
% ==============================================================================			
	m_fileTEST 	= m_files(:,TEST); %file names with temperatures for test TEST 
	N_mfiles = numel(m_fileTEST);
	m_initial = [];
	
	for i=1:N_mfiles  %Loop for files with mass flow
		file =[]; filename1 =[]; filename2=[];
		filename1 = char(m_fileTEST(i));
		filename2 = [folder2,'/',filename1];
		file	  = importdata(filename2,delim,header);
		
		% Initial temperature data
		if (flag1 == 1)
			m_initial = [m_initial; file.data(1,2) ];
		else
		% Normal data - Experimental Data for Temperatures
			if(case0 == TEST)
				switch i
					case	1		% +23.13
						EXP_M1 = file.data;
					otherwise
				end
			end
		end
	end	
	
	
% ==============================================================================	
% Plot Initial Conditions
% ==============================================================================
	if( flag1 == 1)
		% Initial Temperatures
		[T_elev, T_initial]
		Elev_MEL1 = Elev_MEL(1:end-1)
		Temp_MELCOR = interp1(T_elev,T_initial, Elev_MEL1,'linear','extrap'); %Interpolate initial T
		% Plot
		
		figure(999)
		plot(T_elev,T_initial,'o-','Color',cmap5(j,:))
		hold on; grid on; box on; ylim([430 550]);
		plot(Elev_MEL1,Temp_MELCOR,'*','Color',cmap5(j,:)) 	%Plot
		labels = {labels{:}, ['Test ' num2str(TEST)], ['Test ' num2str(TEST) ' Interp'] }
	    legend(labels);		
	else
		% Normal data
	end
	
end

% ==============================================================================



% ==============================================================================
% READ MELCOR RESULTS DATA

%for k=1:numel(READ_CASES)
%    case1 = CASES(k);

	EDF_P	= importdata([folder1,'/', case1, '/EDF_P']);		%Pressure
	EDF_T	= importdata([folder1,'/', case1, '/EDF_T']);		%Temp.
	EDF_M	= importdata([folder1,'/', case1, '/EDF_MFLOW']);	%Mass Flow
	EDF_V	= importdata([folder1,'/', case1, '/EDF_VOID']);		%Void

% ==============================================================================	
%ERRORS 
%EXP_dP = ones(size(EXP_P1,1),1).*0.9E5; %Pressure Error
	


