% labels={};
% labels = [labels, 'Fission Power'];
%
% ===================================================================
% FIGURE - Generated Hydrogen 
% ===================================================================
run standard_plot1.m;
title('Hydrogen Generation');

if k_num == 1
	x1 = EXP_H2_TOT(:,1);
	y1 = EXP_H2_TOT(:,2);
	plot(x1,y1,'ko','LineWidth',1,'MarkerSize',2);  labels = [labels, 'EXP' ];
end

x2 = EDF_H2(:,1);
y2 = EDF_H2(:,3);
plot(x2,y2,['-'],'LineWidth',1,'MarkerSize',2);  labels = [labels, ['MLC ' case1_name] ];

run standard_plot2.m

% ===================================================================
% FIGURE - Generated Hydrogen Rate
% ===================================================================
run standard_plot1.m
title('Hydrogen Generation Rate');
if k_num == 1
	x1 = EXP_H2_RATE(:,1);
	y1 = EXP_H2_RATE(:,2);
	plot(x1,y1,'ko','LineWidth',1,'MarkerSize',2); labels = [labels, 'EXP'];
end

x2 = EDF_H2(:,1);
y2 = EDF_H2(:,2);
plot(x2,y2,'-','LineWidth',1,'MarkerSize',2);  labels = [labels, ['MLC ' case1_name] ];

ylabel('Mass rate, [kg/s]'); 
run standard_plot2.m
legend('Location','northeast');

% ===================================================================
% FIGURE  - Core Inlet/Outlet Temperature
% ===================================================================
run standard_plot1.m
title('Core Inlet/Outlet Temperature');

labels1 = { 'EXP Inlet' 'EXP Outlet' 'Inlet' 'Core'  'Outlet-UP1'  };

if k_num == 1
	x1 = EXP_TVAP_CORE_INLET(:,1);
	y1 = EXP_TVAP_CORE_INLET(:,2);
	x2 = EXP_TVAP_CORE_EXIT(:,1);
	y2 = EXP_TVAP_CORE_EXIT(:,2);
	plot(x1,y1,'ok','LineWidth',1,'MarkerSize',2); labels = [labels, labels1{1}];
	plot(x2,y2,'<k','LineWidth',1,'MarkerSize',2); labels = [labels, labels1{2}];
end

x   = EDF_TVAP(:,1);
y10 = EDF_TVAP(:,2); %Inlet
y11 = EDF_TVAP(:,3); %Core
y12 = EDF_TVAP(:,4); %Up1

plot(x,y10,'-.','LineWidth',1,'MarkerSize',2); labels = [labels, ['MLC ',case1_name,' ',labels1{3}]];
plot(x,y11,'--','LineWidth',1,'MarkerSize',2); labels = [labels, ['MLC ',case1_name,' ',labels1{4}]];
plot(x,y12,'-','LineWidth',1,'MarkerSize',2);  labels = [labels, ['MLC ',case1_name,' ',labels1{5}]];

ylabel('Temperature, [K]');
run standard_plot2.m













%{
% ===================================================================
% FIGURE - Fuel Temperatures
% ===================================================================
for ring=1:2
	a=a+1;
	figure(a)
	name = ['Fig', num2str(a)];

	hold on; set(0, 'DefaulttextInterpreter', 'none');
	title(['Fuel Temperatures Ring', int2str(ring), ' COR6: 300mm and COR7: 400mm ' ]);
	%title('Fuel Temperatures');
	labels = {};

	x = EDF_TEMP_FU(:,1); %time
	col=1;

		%for level=1:14
		%for ring=1:2
	%	for level=4:13 %ALL CORE
	for level=5:8
		  offset = (ring - 1)*14;
		  y = EDF_TEMP_FU(:,1+ level + offset); 	  
		  plot(x,y,'-','LineWidth',1,'MarkerSize',2,'Color',cmap6(col,:)); %or cmap1
		  
		  if(level<10)
			labels = {labels{:}, ['COR-TFU ', int2str(ring), '0', int2str(level)]};
		  else
			labels = {labels{:}, ['COR-TFU ', int2str(ring), '', int2str(level)]};
		  end
		  
		  col = col+1;
		%end

		end

	xlabel('Time, [s]');
	ylabel('Temperature, [K]');
	legend('Location','northeast');
	box on; %ylim([0 inf]);  
	grid on; xlim([0, 30000]);

	%labels = {' '};
	
	% 300mm for MELCOR it is Level 6
	% 400mm for MELCOR it is Level 7
	if ring == 2
		x1 = EXP_T_AVG_OUT_FUEL_300mm(:,1);
		y1 = EXP_T_AVG_OUT_FUEL_300mm(:,2);
		x2 = EXP_T_AVG_OUT_FUEL_400mm(:,1);
		y2 = EXP_T_AVG_OUT_FUEL_400mm(:,2);
		
		plot(x1,y1,'ok','LineWidth',1,'MarkerSize',3);
		plot(x2,y2,'or','LineWidth',1,'MarkerSize',3);
		
		labels = {labels{:} ,  'EXP Out. 300mm',  'EXP Out. 400mm' };
	else
	
		labels = {labels{:}};
	
	end

	legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');
	if (flag2 == 1)
		print('-dtiff',[case1,'/', name, code1, '.tiff'])
		savefig([case1,'/', name, code1, '.fig'])
		%saveas(gcf,[Fig1.fig'],'fig');
	end

end



% ===================================================================
% FIGURE - Clad Temperatures
% ===================================================================
for ring=1:2
a=a+1;
figure(a)
name = ['Fig', num2str(a)];

hold on; set(0, 'DefaulttextInterpreter', 'none');
title(['Clad Temperatures Ring', int2str(ring), ' COR9: 600mm and COR10: 700mm' ]);
labels = {};

x = EDF_TEMP_CL(:,1); %time
col=1;

	%for level=1:14
	%for ring=1:2
	%for level=5:14
	for level=8:11
	  offset = (ring - 1)*14;
	  y = EDF_TEMP_CL(:,1+ level + offset); 	  
	  plot(x,y,'-','LineWidth',1,'MarkerSize',2,'Color',cmap6(col,:)); %or cmap1
	  
	  if(level<10)
		labels = {labels{:}, ['COR-TCL ', int2str(ring), '0', int2str(level)]};
      else
	    labels = {labels{:}, ['COR-TCL ', int2str(ring), '', int2str(level)]};
	  end
	  
	  col = col+1;
	%end

	end

xlabel('Time, [s]');
ylabel('Temperature, [K]');
legend('Location','northeast');
box on; %ylim([0 inf]);  
grid on; xlim([0, 30000]);

if ring ==2
	x1 = EXP_T_AVG_OUT_CLAD_600mm(:,1);
	y1 = EXP_T_AVG_OUT_CLAD_600mm(:,2);
	x2 = EXP_T_AVG_OUT_CLAD_700mm(:,1);
	y2 = EXP_T_AVG_OUT_CLAD_700mm(:,2);
	
	plot(x1,y1,'ok','LineWidth',1,'MarkerSize',3);
	plot(x2,y2,'or','LineWidth',1,'MarkerSize',3);
	
    labels = {labels{:} ,  'EXP Out. 600mm',  'EXP Out. 700mm' };
	
else
	
	 labels = {labels{:}};
end	


legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end

end

% ===================================================================
% FIGURE  - Shroud Temperatures 1
% ===================================================================
% Inside Shroud Zirconia Insulator 200mm - Level 6 COR6
a=a+1;
figure(a)
name = ['Fig', num2str(a)];

hold on; set(0, 'DefaulttextInterpreter', 'none');
title('Inner Zirconia Shroud Temp. COR4:100mm  COR5:200mm  COR6:300mm');

x = EDF_TEMP_SH(:,1);

labels={};

% ! Node 10 - Zirconia Inner Shroud Inner Node 
y1 = EDF_TEMP_SH(:,2);
y2 = EDF_TEMP_SH(:,3);
y3 = EDF_TEMP_SH(:,4);
plot(x,y1,'--','LineWidth',1,'MarkerSize',2,'Color',cmap11(1,:));
plot(x,y2,'--','LineWidth',1,'MarkerSize',2,'Color',cmap11(2,:));
plot(x,y3,'--','LineWidth',1,'MarkerSize',2,'Color',cmap11(3,:));

label = {'100mm Node 10' '200mm Node 10' '300mm Node 10'};
labels=[labels  label]

%! Node 11 - Zirconia Outer Shroud Inner Node
y1 = EDF_TEMP_SH(:,5);
y2 = EDF_TEMP_SH(:,6);
y3 = EDF_TEMP_SH(:,7);
plot(x,y1,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(4,:));
plot(x,y2,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(5,:));
plot(x,y3,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(6,:));
label = {'100mm Node 11' '200mm Node 11' '300mm Node 11'};
labels=[labels  label]

%! Node 12 - Inconel Inner Node
y1 = EDF_TEMP_SH(:,8);
y2 = EDF_TEMP_SH(:,9);
y3 = EDF_TEMP_SH(:,10);
plot(x,y1,'-.','LineWidth',1,'MarkerSize',2,'Color',cmap11(8,:));
plot(x,y2,'-.','LineWidth',1,'MarkerSize',2,'Color',cmap11(9,:));
plot(x,y3,'-.','LineWidth',1,'MarkerSize',2,'Color',cmap11(10,:));
label = {'100mm Node 12' '200mm Node 12' '300mm Node 12'}
labels=[labels  label]

% Experimental  Inner Shroud Temperature at 200mm and 300mm - inside shroud zirconia insulator
% For MELCOR MANUAL VOL3 it was HS node 14
	x1 = EXP_T_ZR_SH_INS_200mm(:,1);
	y1 = EXP_T_ZR_SH_INS_200mm(:,2);
	plot(x1,y1,'ok','LineWidth',1,'MarkerSize',3)
    labels = {labels{:} ,  'EXP 200mm' };
	
xlabel('Time, [s]');
ylabel('Temperature, [K]');
legend('Location','northeast');
box on; %ylim([0 inf]);  
grid on; xlim([0, 30000]);

legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end

% ===================================================================
% FIGURE  - Shroud Temperatures 600mm
% ===================================================================
% Inside Shroud Zirconia Insulator 600mm - Level 9 COR 9
a=a+1;
figure(a)
name = ['Fig', num2str(a)];

hold on; set(0, 'DefaulttextInterpreter', 'none');
title('Shroud Temp. COR8: 500mm COR9: 600mm COR10: 700mm');

x = EDF_TEMP_SH(:,1);

labels={};

% ! Node 10 - Zirconia Inner Shroud Inner Node 
y1 = EDF_TEMP_SH(:,11);
y2 = EDF_TEMP_SH(:,12);
y3 = EDF_TEMP_SH(:,13);
plot(x,y1,'--','LineWidth',1,'MarkerSize',2,'Color',cmap11(1,:));
plot(x,y2,'--','LineWidth',1,'MarkerSize',2,'Color',cmap11(2,:));
plot(x,y3,'--','LineWidth',1,'MarkerSize',2,'Color',cmap11(3,:));

label = {'500mm Node 10' '600mm Node 10' '700mm Node 10'};
labels=[labels  label];

%! Node 11 - Zirconia Outer Shroud Inner Node
y1 = EDF_TEMP_SH(:,14);
y2 = EDF_TEMP_SH(:,15);
y3 = EDF_TEMP_SH(:,16);
plot(x,y1,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(4,:));
plot(x,y2,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(5,:));
plot(x,y3,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(6,:));
label = {'500mm Node 11' '600mm Node 11' '700mm Node 11'};
labels=[labels  label];

%! Node 12 - Inconel Inner Node
y1 = EDF_TEMP_SH(:,17);
y2 = EDF_TEMP_SH(:,18);
y3 = EDF_TEMP_SH(:,19);
plot(x,y1,'-.','LineWidth',1,'MarkerSize',2,'Color',cmap11(8,:));
plot(x,y2,'-.','LineWidth',1,'MarkerSize',2,'Color',cmap11(9,:));
plot(x,y3,'-.','LineWidth',1,'MarkerSize',2,'Color',cmap11(10,:));
label = {'500mm Node 12' '600mm Node 12' '700mm Node 12'}
labels=[labels  label];

% Experimental  Inner Shroud Temperature at 600mm - inside shroud zirconia insulator
% For MELCOR MANUAL VOL3 it was HS node 14

	x1 = EXP_T_ZR_SH_INS_600mm(:,1);
	y1 = EXP_T_ZR_SH_INS_600mm(:,2);
	plot(x1,y1,'ok','LineWidth',1,'MarkerSize',3)
    labels = {labels{:} ,  'EXP 600mm' };
	


xlabel('Time, [s]');
ylabel('Temperature, [K]');
legend('Location','northeast');
box on; %ylim([0 inf]);  
grid on; xlim([0, 30000]);


legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end

% ===================================================================
% FIGURE  Core Energy Balance
% ===================================================================
a=a+1;
figure(a)
name = ['Fig', num2str(a)];

hold on; set(0, 'DefaulttextInterpreter', 'none');
title('Energy Balance');
labels={};

x = EDF_POWER(:,1);

y1 = EDF_POWER(:,8); %EFPD
y2 = EDF_POWER(:,9); %EMWR
y3 = EDF_POWER(:,10); %ECNV
y4 = EDF_POWER(:,11); %EBND
y5 = EDF_POWER(:,13); %DCH-TOTCLSPOW
y6 = EDF_POWER(:,14); %DCH-COREPOW
y7 = y1 + y2 - y3 - y4; %Balance

plot(x,y1,'-k','LineWidth',1,'MarkerSize',2);
plot(x,y2,'-g','LineWidth',1,'MarkerSize',2);
plot(x,y3,'-b','LineWidth',1,'MarkerSize',2);
plot(x,y4,'-r','LineWidth',1,'MarkerSize',2);
plot(x,y5,'-.c','LineWidth',1,'MarkerSize',4);
plot(x,y6,'-.m','LineWidth',1,'MarkerSize',4);
plot(x,y7,'-','LineWidth',1,'MarkerSize',4,'Color',[1.00,0.41,0.16]);
%labels = {'EFPD - Fission Power', 'EMWR - Oxidation Power',   ...
%           'ECNV - Convective Removal', 'EBND - Conduction Removal' };
labels = {'COR-EFPD', 'COR-EMWR',   ...
           'COR-ECNV', 'COR-EBND' ...
 'DCH-TOTCLSPOW', 'DCH-COREPOW', 'MELCOR BALANCE'};
 %'EFPD+EMWR-EBND-ECNV'
 
% labels = {'COR-EFPD - Fission Power', 'COR-EMWR - Oxidation Power',   ...
%           'COR-ECNV - Convective Removal', 'COR-EBND - Conduction Removal' ...
% 'DCH-TOTCLSPOW Decay Heat All RNs', 'DCH-COREPOW Total Decay Heat'};


x1 = EXP_POWER_NUCL(:,1);
y1 = EXP_POWER_NUCL(:,3);
x2 = EXP_POWER_OXDN(:,1);
y2 = EXP_POWER_OXDN(:,3);
x3 = EXP_POWER_CONV(:,1);
y3 = EXP_POWER_CONV(:,3);
x4 = EXP_POWER_SHRD(:,1);
y4 = EXP_POWER_SHRD(:,3);
x5 = EXP_POWER_STDHT(:,1);
y5 = EXP_POWER_STDHT(:,3);

plot(x1,y1,'--k','LineWidth',1,'MarkerSize',2);
plot(x2,y2,'--g','LineWidth',1,'MarkerSize',2);
plot(x3,y3,'--b','LineWidth',1,'MarkerSize',2);
plot(x4,y4,'--r','LineWidth',1,'MarkerSize',2);
plot(x5,y5,'--y','LineWidth',1,'MarkerSize',4,'Color',[1.00,0.41,0.16]);

ylim([0 40000])

labels1 = {'SNL NUCLEAR', 'SNL OXDN',   ...
           'SNL CONV', 'SNL SHRD', 'SNL STDHT'};

 labels = [labels{:} , labels1];

xlabel('Time, [s]');
ylabel('Power, [W]');
legend('Location','northeast');
box on; %ylim([0 inf]);
  grid on; xlim([0, 30000]);

legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end
%}