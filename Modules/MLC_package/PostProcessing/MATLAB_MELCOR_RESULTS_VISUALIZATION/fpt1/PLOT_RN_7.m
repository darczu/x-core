% ===================================================================
% ===================================================================
% ===================================================================
% ===================================================================
% Radionuclide Package
% ===================================================================
% ===================================================================
%  INVENTORY_NAME'
%     {'XE' }
%     {'CS' }
%     {'BA' }
%     {'I2' }
%     {'TE' }
%     {'RU' }
%     {'MO' }
%     {'CE' }
%     {'UO2'}
%     {'CD' }
% % ===================================================================
% INVENTORY =
%       0.03549
%       0.01837
%        0.0154
%       0.00198
%       0.00263
%       0.01729
%         0.025
%        0.0385
%         8.967
%       0.00023
% ===================================================================
% location in EDF_RELESE
% 1	RN1-XMRLSET		! Total Non and Rad mass from COR
% 2	RN1-TOTMAS('XE',TOT)
% 3	RN1-TOTMAS('CS',TOT)
% 4	RN1-TOTMAS('BA',TOT)
% 5	RN1-TOTMAS('I2',TOT)
% 6	RN1-TOTMAS('TE',TOT)
% 7	RN1-TOTMAS('RU',TOT)
% 8	RN1-TOTMAS('MO',TOT)
% 9	RN1-TOTMAS('CE',TOT)
% 10	RN1-TOTMAS('UO2',TOT)
% 11	RN1-TOTMAS('CD',TOT)
% 12	RN1-TOTMAS('AG',TOT)
% 13	RN1-TOTMAS('CSI',TOT)
% 14	RN1-TOTMAS('CSM',TOT)
% ! RAD
% 15	RN1-XMRLSER		! Total Rad mass from COR
% 16	RN1-TOTMAS('XE',RAD)
% 17	RN1-TOTMAS('CS',RAD)
% 18	RN1-TOTMAS('BA',RAD)
% 19	RN1-TOTMAS('I2',RAD)
% 20	RN1-TOTMAS('TE',RAD)
% 21	RN1-TOTMAS('RU',RAD)
% 22	RN1-TOTMAS('MO',RAD)
% 23	RN1-TOTMAS('CE',RAD)
% 24	RN1-TOTMAS('UO2',RAD)
% 25	RN1-TOTMAS('CD',RAD)
% 26	RN1-TOTMAS('AG',RAD)	
% 27	RN1-TOTMAS('CSI',RAD)	
% 28	RN1-TOTMAS('CSM',RAD)	
% ! AIC	
% 29	RN1-TOTMAS('AG-CR',TOT)
% 30	RN1-TOTMAS('AG-CR',RAD)
% 31	RN1-TOTMAS('IN-CR',TOT)
% 32	RN1-TOTMAS('IN-CR',RAD)
% 33	RN1-TOTMAS('CD-CR',TOT)
% 34	RN1-TOTMAS('CD-CR',RAD)	
% 35	RN1-TOTMAS('SNO2',TOT)
% 36	RN1-TOTMAS('SNO2',RAD)	

iXE = 16+1;
iCS = 17+1;
iBA = 18+1;
iI2 = 19+1;
iTE = 20+1;
iRU = 21+1;
iMO = 22+1;
iCE = 23+1;
iU = 24+1;
iCD = 25+1;
iAG = 26+1;
iCSI = 27+1;
iCSM = 28+1;


RELEASE_TABLE_TEXT = {'Final Mass' 'Initial Inv.' 'Release Frac.' 'Experimental Frac.'};
RELEASE_MASS = []; RELEASE_INITIAL =[]; RELEASE_FRAC  = []; RELEASE_FRAC_EXP  = []; RELEASE_MAT   = {};

% ===================================================================
% FIGURE XENON
% ===================================================================
run standard_plot1.m;
title('Xenon Class Release');
if k_num == 1
    x1 = EXP_XE(:,1);
    y1 = EXP_XE(:,2)./100;
    hCurve = plot(x1,y1,'ko','LineWidth',1,'MarkerSize',2); labels = [labels, 'EXP' ];
end
if k_num == 1
	if flagSNL3  == 1
		xx = SNL3_5_XE(:,1);
		yy = SNL3_5_XE(:,2);
		hCurve = plot(xx,yy,'--b','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#3 ORNL-Booth M1.85' ];
	end
end

if k_num == 1
	if flagSNLQL == 1
	% SNL Raport
		x4 = SNL_QL2_M22_XE_5(:,1);
		y4 = SNL_QL2_M22_XE_5(:,2);
		hCurve = plot(x4,y4,['--r'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#5 CB(-5) M2.2' ];
		x5 = SNL_QL2_M22_XE_7(:,1);
		y5 = SNL_QL2_M22_XE_7(:,2);
		hCurve = plot(x5,y5,['--m'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#5 CB(-7) M2.2' ];
	end
end


x2  = EDF_RELEASE(:,1); 
%y02 = EDF_RELEASE(:,3);
y02 = EDF_RELEASE(:,iXE);
y00 = INVENTORY(1);
y2  = y02./y00;
%hCurve = plot(x2,y2,'-r','LineWidth',1,'MarkerSize',2);
% [smap{k_num}]
hCurve = plot(x2,y2,'-','LineWidth',1,'MarkerSize',2); labels = [labels, [ case1_name] ];
hCurve.Color = cmapdyn(k_num,:);


ylabel('Fraction, [-]');
run standard_plot2.m;

RELEASE_MASS      = [RELEASE_MASS ;   y02(end)];
RELEASE_INITIAL   = [RELEASE_INITIAL;  y00(end)];
RELEASE_FRAC  	  = [RELEASE_FRAC  	;  y2(end)];
RELEASE_FRAC_EXP  = [RELEASE_FRAC_EXP; y1(end)];
RELEASE_TABLE     = [RELEASE_MASS,RELEASE_INITIAL, RELEASE_FRAC, RELEASE_FRAC_EXP ];
RELEASE_MAT       = {RELEASE_MAT{:}, 'XE'};
 
% ===================================================================
% FIGURE  CESIUM
% ===================================================================
run standard_plot1.m;
title('Cesium Class Release with CsOH & CsI & Cs2MoO4');
if k_num == 1
    x1 = EXP_CS(:,1);
    y1 = EXP_CS(:,2)./100;
    hCurve = plot(x1,y1,'ko','LineWidth',1,'MarkerSize',2); labels = [labels, 'EXP' ];
end


if k_num == 1
	if flagSNL3  == 1
		xx = SNL3_2_CS(:,1);
		yy = SNL3_2_CS(:,2);
		hCurve = plot(xx,yy,'--b','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#3 ORNL-Booth M1.85' ];
	end
end
if k_num == 1
	if flagSNLQL == 1
	% SNL Raport
		x4 = SNL_QL4_M22_CS_5(:,1);
		y4 = SNL_QL4_M22_CS_5(:,2);
		hCurve = plot(x4,y4,['--r'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#5 CB(-5) M2.2' ];
		x5 = SNL_QL4_M22_CS_7(:,1);
		y5 = SNL_QL4_M22_CS_7(:,2);
		hCurve = plot(x5,y5,['--m'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#5 CB(-7) M2.2' ];
	end
end


% Mass Fractions in CSI
mass0_CS = INVENTORY(2);  
mass0_I2 = INVENTORY(4);  

frac_CS_CSI = 0.511548789; 
frac_I2_CSI = 0.488451211;
frac_CS_CSOH = 0.88655;   
% OLD Mass Fractions in Cs2MoO4 - corrected
frac_CS_CS2MoO4 = 0.624334323; %Fraction of Cs in Cs2MoO4
frac_MO_CS2MoO4 = 0.225343035; %Fraction of Mo in Cs2MoO4

frac_CS_CSM = 0.734789879;  % Fraction of Cs in Cs2Mo
frac_MO_CSM = 0.265210121;  % Fraction of Mo in Cs2Mo


x2 = EDF_RELEASE(:,1);   %time
%y2 = EDF_RELEASE(:,4)./INVENTORY(2);
%y2 = (EDF_RELEASE(:,4)+frac_CS.*EDF_RELEASE(:,14))./INVENTORY(2);

% Changed from total  to fractional only-radioactive
% Artkyl 2020
%%% y2 = (frac_CS_CSOH.*EDF_RELEASE(:,4) + frac_CS_CSI.*EDF_RELEASE(:,14) +  frac_CS_CSM.*EDF_RELEASE(:,15))./INVENTORY(2);
% Wczesniej raczej tez bylo ok
% 2023
y2 = (1.*EDF_RELEASE(:,iCS) + frac_CS_CSI.*EDF_RELEASE(:,iCSI) +  frac_CS_CSM.*EDF_RELEASE(:,iCSM))./INVENTORY(2);



hCurve = plot(x2,y2,'-','LineWidth',1,'MarkerSize',2); labels = [labels, [ case1_name] ];
hCurve.Color = cmapdyn(k_num,:);


ylabel('Fraction, [-]');
run standard_plot2.m

% ===================================================================
% FIGURE BARIUM
% ===================================================================
run standard_plot1.m
title('Barium Class Release');
if k_num == 1
    x1 = EXP_BA(:,1);
    y1 = EXP_BA(:,2)./100;
    hCurve = plot(x1,y1,'ko','LineWidth',1,'MarkerSize',2); labels = [labels, 'EXP' ];
end

if k_num == 1
	if flagSNL3  == 1
		xx = SNL3_1_BA(:,1);
		yy = SNL3_1_BA(:,2);
		hCurve = plot(xx,yy,'--b','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#3 ORNL-Booth M1.85' ];
	end
end

if k_num == 1
	if flagSNLQL == 1
	% SNL Raport
		x4 = SNL_QL6_M22_BA_5(:,1);
		y4 = SNL_QL6_M22_BA_5(:,2);
		hCurve = plot(x4,y4,['--r'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#5 CB(-5) M2.2' ];
		x5 = SNL_QL6_M22_BA_7(:,1);
		y5 = SNL_QL6_M22_BA_7(:,2);
		hCurve = plot(x5,y5,['--m'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#5 CB(-7) M2.2' ];
	end
end


x2 = EDF_RELEASE(:,1);
y2 = EDF_RELEASE(:,5)./INVENTORY(3);
hCurve = plot(x2,y2,'-','LineWidth',1,'MarkerSize',2); labels = [labels, [ case1_name] ];
hCurve.Color = cmapdyn(k_num,:);


ylabel('Fraction, [-]');
run standard_plot2.m

% ===================================================================
% FIGURE Iodine
% ===================================================================
run standard_plot1.m
title('Iodine Class Release, including CsI & I2');
if k_num == 1
    x1 = EXP_I(:,1);
    y1 = EXP_I(:,2)./100;
    hCurve = plot(x1,y1,'ko','LineWidth',1,'MarkerSize',2); labels = [labels, 'EXP' ];
end

if k_num == 1
	if flagSNL3  == 1
		xx = SNL3_3_I(:,1);
		yy = SNL3_3_I(:,2);
		hCurve = plot(xx,yy,'--b','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#3 ORNL-Booth M1.85' ];
	end
end

if k_num == 1
	if flagSNLQL == 1
	% SNL Raport
		x4 = SNL_QL3_M22_I_5(:,1);
		y4 = SNL_QL3_M22_I_5(:,2);
		hCurve = plot(x4,y4,['--r'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#5 CB(-5) M2.2' ];
		x5 = SNL_QL3_M22_I_7(:,1);
		y5 = SNL_QL3_M22_I_7(:,2);
		hCurve = plot(x5,y5,['--m'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#5 CB(-7) M2.2' ];
	end
end



% Mass Fractions in CSI
mass0_CS = INVENTORY(2);  
mass0_I2 = INVENTORY(4);  
%frac_CS_CSI = 0.511548789; 
%frac_I2_CSI = 0.488451211;

% Mass Fractions in Cs2MoO4
%frac_CS_CSM = 0.624334323;
%frac_MO_CSM = 0.225343035;

x2 = EDF_RELEASE(:,1);
%y2 = EDF_RELEASE(:,6)./INVENTORY(4);
% artyluk 2020
% y2 = (EDF_RELEASE(:,6) + frac_I2_CSI.*EDF_RELEASE(:,14))./INVENTORY(4);
% 2023
y2 = (EDF_RELEASE(:,iI2) + frac_I2_CSI.*EDF_RELEASE(:,iCSI))./INVENTORY(4);

hCurve = plot(x2,y2,'-','LineWidth',1,'MarkerSize',2); labels = [labels, [ case1_name] ];
hCurve.Color = cmapdyn(k_num,:);

ylabel('Fraction, [-]');
run standard_plot2.m

% ===================================================================
% FIGURE Tellurium
% ===================================================================
run standard_plot1.m
title('Tellurium Class Release');
if k_num == 1
    x1 = EXP_TE(:,1);
    y1 = EXP_TE(:,2)./100;
    hCurve = plot(x1,y1,'ko','LineWidth',1,'MarkerSize',2); labels = [labels, 'EXP' ];
end

if k_num == 1
	if flagSNL3  == 1
		xx = SNL3_4_TE(:,1);
		yy = SNL3_4_TE(:,2);
		hCurve = plot(xx,yy,'--b','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#3 ORNL-Booth M1.85' ];
	end
end

if k_num == 1
	if flagSNLQL == 1
	% SNL Raport
		x4 = SNL_QL5_M22_TE_5(:,1);
		y4 = SNL_QL5_M22_TE_5(:,2);
		hCurve = plot(x4,y4,['--r'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#5 CB(-5) M2.2' ];
		x5 = SNL_QL5_M22_TE_7(:,1);
		y5 = SNL_QL5_M22_TE_7(:,2);
		hCurve = plot(x5,y5,['--m'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#5 CB(-7) M2.2' ];
	end
end


x2 = EDF_RELEASE(:,1);
% Artkyul 2020
%y2 = EDF_RELEASE(:,7)./INVENTORY(5);
% 2023 zmienione - bo TeO
y2 = EDF_RELEASE(:,iTE)./INVENTORY(5);


hCurve = plot(x2,y2,'-','LineWidth',1,'MarkerSize',2); labels = [labels, [ case1_name] ];
hCurve.Color = cmapdyn(k_num,:);



ylabel('Fraction, [-]');
run standard_plot2.m
 
% ===================================================================
% FIGURE  Ruthenium
% ===================================================================
run standard_plot1.m
title('Ruthenium Class Release');
if k_num == 1
    x1 = EXP_RU(:,1);
    y1 = EXP_RU(:,2)./100;
    hCurve = plot(x1,y1,'ko','LineWidth',1,'MarkerSize',2); labels = [labels, 'EXP' ];
end
if k_num == 1
	if flagSNL3  == 1
		xx = SNL3_6_RU(:,1);
		yy = SNL3_6_RU(:,2);
		hCurve = plot(xx,yy,'--b','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#3 ORNL-Booth mod. M1.85' ];
	end
end

if k_num == 1
	if flagSNLQL == 1
	% SNL Raport
		x4 = SNL_QL7_M22_RU_5(:,1);
		y4 = SNL_QL7_M22_RU_5(:,2);
		hCurve = plot(x4,y4,['--r'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#5 CB(-5) M2.2' ];
		x5 =  SNL_QL7_M22_RU_7(:,1);
		y5 =  SNL_QL7_M22_RU_7(:,2);
		hCurve = plot(x5,y5,['--m'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#5 CB(-7) M2.2' ];
	end
end
x2 = EDF_RELEASE(:,1);
y2 = EDF_RELEASE(:,8)./INVENTORY(6);
hCurve = plot(x2,y2,'-','LineWidth',1,'MarkerSize',2); labels = [labels, [ case1_name] ];
hCurve.Color = cmapdyn(k_num,:);

ylabel('Fraction, [-]');
run standard_plot2.m

% ===================================================================
% FIGURE  Molybdenu
% ===================================================================
run standard_plot1.m
title('Molybdenu Class Release, including CsM');
if k_num == 1
    x1 = EXP_MO(:,1);
    y1 = EXP_MO(:,2)./100; hCurve = plot(x1,y1,'ko','LineWidth',1,'MarkerSize',2); labels = [labels, 'EXP' ];
end

if k_num == 1
	if flagPS1ISP  == 1
		xx = ISP_PS1_MO_BEST(:,1);
		yy = ISP_PS1_MO_BEST(:,2);
		hCurve = plot(xx,yy,'--b','LineWidth',1,'MarkerSize',2); labels = [labels, 'PS1 CORSOR-M M1.85' ];
	end
end
if k_num == 1
	if flagSNL3  == 1
		xx = SNL3_7_MO(:,1);
		yy = SNL3_7_MO(:,2);
		hCurve = plot(xx,yy,'--r','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#3 ORNL-Booth M1.85 mod. (*)' ];
		% Options for Cs2MoO4
	end
end



% Mass Fractions in CSI
mass0_CS = INVENTORY(2);  
mass0_I2 = INVENTORY(4);  
%frac_CS_CSI = 0.511548789; 
%frac_I2_CSI = 0.488451211;
% Mass Fractions in Cs2MoO4
%frac_CS_CSM = 0.624334323;
%frac_MO_CSM = 0.225343035;

x2 = EDF_RELEASE(:,1);
% Artykul 2020
%y2 = (EDF_RELEASE(:,9) + frac_MO_CSM.*EDF_RELEASE(:,15)  )./INVENTORY(7);
% 2023
y2 = (EDF_RELEASE(:,iMO) + frac_MO_CSM.*EDF_RELEASE(:,iCSM)  )./INVENTORY(7);

hCurve = plot(x2,y2,'-','LineWidth',1,'MarkerSize',2);  labels = [labels, [ case1_name] ];
hCurve.Color = cmapdyn(k_num,:);

ylabel('Fraction, [-]');
run standard_plot2.m

% ===================================================================
% FIGURE CERIUM 
% ===================================================================
run standard_plot1.m
title('Cerium Class Release (Pu+Np+Zr)');
%x1 = EXP_SB(:,1);
%y1 = EXP_SB(:,2)./100;
%hCurve = plot(x1,y1,'ko','LineWidth',1,'MarkerSize',2);

if k_num == 1
	if flagSNL3  == 1
		xx = SNL3_8_CE(:,1);
		yy = SNL3_8_CE(:,2);
		hCurve = plot(xx,yy,'--b','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#3 ORNL M1.85' ];
	end
end


x2 = EDF_RELEASE(:,1);
y2 = EDF_RELEASE(:,10)./INVENTORY(8);
hCurve = plot(x2,y2,'-','LineWidth',1,'MarkerSize',2);  labels = [labels, [ case1_name] ];
hCurve.Color = cmapdyn(k_num,:);

ylabel('Fraction, [-]');
run standard_plot2.m
% ===================================================================
% FIGURE  URANIUM
% ===================================================================
run standard_plot1.m
title('Uranium Class Release');
if k_num == 1
    x1 = EXP_U(:,1);
    y1 = EXP_U(:,2)./100;
    hCurve = plot(x1,y1,'ko','LineWidth',1,'MarkerSize',2); labels = [labels, 'EXP' ];
end
if k_num == 1
	if flagSNL3  == 1
		xx = SNL3_10_U(:,1);
		yy = SNL3_10_U(:,2);
		hCurve = plot(xx,yy,'--b','LineWidth',1,'MarkerSize',2);  labels = [labels, 'SNL#3 ORNL scaled M1.85 (*)' ];
		%labels = [labels, 'SNL#3 ORNL scaled to FPT1 ' ];
	end
end


x2 = EDF_RELEASE(:,1);
% artykul 2020
%y2 = EDF_RELEASE(:,11)./INVENTORY(9);
% 2023
y2 = EDF_RELEASE(:,iU)./INVENTORY(9);

hCurve = plot(x2,y2,'-','LineWidth',1,'MarkerSize',2); labels = [labels, [ case1_name] ];
hCurve.Color = cmapdyn(k_num,:);

ylabel('Fraction, [-]');

run standard_plot2.m

% ===================================================================
% FIGURE Cadmium Class (Sb Fuel)
% ===================================================================
run standard_plot1.m
title('Cadmium  Class Release - Fuel Antimony (Sb)');
if k_num == 1
    x1 = EXP_SB(:,1);
    y1 = EXP_SB(:,2)./100;
    hCurve = plot(x1,y1,'ko','LineWidth',1,'MarkerSize',2); labels = [labels, 'EXP' ];
    %hCurve = plot(x11,y11,'ks','LineWidth',1,'MarkerSize',2);
end
if k_num == 1
	if flagSNL3  == 1
		xx = SNL3_11_CD(:,1);
		yy = SNL3_11_CD(:,2);
		hCurve = plot(xx,yy,'--b','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#3 ORNL M1.85' ];
	end
end


%x11 = EXP_CD(:,1);
%y11 = EXP_CD(:,2)./100;
%hCurve = plot(x1,y1,'ko','LineWidth',1,'MarkerSize',2); 
%hCurve = plot(x11,y11,'ks','LineWidth',1,'MarkerSize',2);
x3 = EDF_RELEASE(:,1);
y3 = EDF_RELEASE(:,12)./INVENTORY(10);     %From Fuel AG group
hCurve = plot(x3,y3,'-','LineWidth',1,'MarkerSize',2); labels = [labels, [ case1_name] ];
hCurve.Color = cmapdyn(k_num,:);

%hCurve = plot(x3,y2,'-b','LineWidth',1,'MarkerSize',2);

ylabel('Fraction, [-]');
%labels = { 'EXP Sb' 'Fuel Sb'};
run standard_plot2.m

% ===================================================================
% FIGURE Silver Class (from Fuel)
% ===================================================================
run standard_plot1.m
title('Silver Class Release - Fuel - no AIC model');

% Work only when there are no AIC model
x3 = EDF_RELEASE(:,1);
y3 = EDF_RELEASE(:,13)./INVENTORY_STR(1);  %From Control Rods relative
hCurve = plot(x3,y3,'-','LineWidth',1,'MarkerSize',2); labels = [labels, [ case1_name] ];
hCurve.Color = cmapdyn(k_num,:);

%hCurve = plot(x3,y2,'-b','LineWidth',1,'MarkerSize',2);

ylabel('Fraction of Ag in CR mass, [-]');
%ylabel('Fraction of Ag in CR mass, [-]');
%labels = { 'EXP Sb' 'Fuel Sb'};
run standard_plot2.m



% ===================================================================
% FIGURE Cadmium (from AIC only)
% ===================================================================
run standard_plot1.m
title('Cadmium  Release - From CRs');
%x1 = EXP_SB(:,1);
%y1 = EXP_SB(:,2)./100;
if k_num == 1
    x11 = EXP_CD(:,1);
    y11 = EXP_CD(:,2)./100;
    hCurve = plot(x11,y11,'ko','LineWidth',1,'MarkerSize',2); labels = [labels, 'EXP' ];
end


if k_num == 1
	if flagPS1ISP  == 1
		xx = ISP_PS1_CD_BEST_CR(:,1);
		yy = ISP_PS1_CD_BEST_CR(:,2);
		hCurve = plot(xx,yy,'--b','LineWidth',1,'MarkerSize',2); labels = [labels, 'PS1 CORSOR-M M1.85 (*)' ];
	end
end


%hCurve = plot(x1,y1,'ko','LineWidth',1,'MarkerSize',2);
x3 = EDF_RELEASE(:,1);
y3 = EDF_RELEASE(:,34)./INVENTORY_STR(2);  %From Control Rods
%y2 = EDF_RELEASE(:,12)./INVENTORY(10);     %From Fuel
hCurve = plot(x3,y3,'-','LineWidth',1,'MarkerSize',2); labels = [labels, [ case1_name] ];
hCurve.Color = cmapdyn(k_num,:);
%hCurve = plot(x3,y2,'-b','LineWidth',1,'MarkerSize',2);
%hCurve = plot(x3,y2+y3,'-k','LineWidth',1,'MarkerSize',2);

ylabel('Fraction, [-]');
%labels = { 'EXP CRs'   'CRs'};

run standard_plot2.m
% ===================================================================
% FIGURE Silver (from AIC)
% ===================================================================
run standard_plot1.m
title('Silver Release from Control Rods');

if k_num == 1
    x1 = EXP_AG(:,1);
    y1 = EXP_AG(:,2)./100;
    hCurve = plot(x1,y1,'ko','LineWidth',1,'MarkerSize',2); labels = [labels, 'EXP' ];
end

if k_num == 1
	if flagPS1ISP  == 1
		xx = ISP_PS1_AG_BEST_CR(:,1);
		yy = ISP_PS1_AG_BEST_CR(:,2);
		hCurve = plot(xx,yy,'--b','LineWidth',1,'MarkerSize',2); labels = [labels, 'PS1 CORSOR-M M1.85 (*)' ];
	end
end

x3 = EDF_RELEASE(:,1);
y3 = EDF_RELEASE(:,30)./INVENTORY_STR(1);  %From Control Rods
%y2 = 0.0;
%y2 = EDF_RELEASE(:,13)./INVENTORY();     %From Fuel
hCurve = plot(x3,y3,'-','LineWidth',1,'MarkerSize',2);  labels = [labels, [ case1_name] ];
hCurve.Color = cmapdyn(k_num,:);
%hCurve = plot(x3,y2,'-b','LineWidth',1,'MarkerSize',2);
%hCurve = plot(x3,y2+y3,'-k','LineWidth',1,'MarkerSize',2);
ylabel('Fraction, [-]');
%labels = { 'EXP' 'CRs' 'Fuel'  };
%labels = { 'EXP' 'CRs Only'};
run standard_plot2.m

% ===================================================================
% FIGURE Indium (from AIC)
% ===================================================================
run standard_plot1.m
title('Indium Release from Control Rods');

if k_num == 1
    x1 = EXP_IN(:,1);
    y1 = EXP_IN(:,2)./100;
    hCurve = plot(x1,y1,'ko','LineWidth',1,'MarkerSize',2); labels = [labels, 'EXP' ];
end 


if k_num == 1
	if flagPS1ISP  == 1
		xx = ISP_PS1_IN_BEST_CR(:,1);
		yy = ISP_PS1_IN_BEST_CR(:,2);
		hCurve = plot(xx,yy,'--b','LineWidth',1,'MarkerSize',2); labels = [labels, 'PS1 CORSOR-M M1.85 (*)' ];
	end
end


x3 = EDF_RELEASE(:,1);
%y3 = EDF_RELEASE(:,34)./INVENTORY_STR(3);  %From Control Rods
y3 = EDF_RELEASE(:,32)./INVENTORY_STR(3);  %From Control Rods  - był error
%y2 = 0.0;
%y2 = EDF_RELEASE(:,13)./INVENTORY();     %From Fuel
hCurve = plot(x3,y3,'-','LineWidth',1,'MarkerSize',2); labels = [labels, [ case1_name] ];
hCurve.Color = cmapdyn(k_num,:);
%hCurve = plot(x3,y2,'-b','LineWidth',1,'MarkerSize',2);
%hCurve = plot(x3,y2+y3,'-k','LineWidth',1,'MarkerSize',2);

ylabel('Fraction, [-]');

%set(gca, 'YScale', 'log');

%labels = { 'EXP' 'CRs' 'Fuel'  };
% labels = { 'EXP' 'CRs Only'};
run standard_plot2.m

% ===================================================================
% FIGURE Tin - from Cladding 
% ===================================================================

%if flagTin == 1

    run standard_plot1.m
    title('Tin Release from Cladding');
    if k_num == 1
        x1 = EXP_SN(:,1);
        y1 = EXP_SN(:,2)./100;
        hCurve = plot(x1,y1,'ko','LineWidth',1,'MarkerSize',2); labels = [labels, 'EXP' ];
    end
	
	if k_num == 1
		if flagPS1ISP  == 1
		xx = ISP_PS1_SN_BEST_CLAD(:,1);
		yy = ISP_PS1_SN_BEST_CLAD(:,2);
		hCurve = plot(xx,yy,'--b','LineWidth',1,'MarkerSize',2); labels = [labels, 'PS1 CORSOR-M M1.85' ];
		end
	end

	if k_num == 1
		if flagSNL3  == 1
			xx = SNL3_12_SN(:,1);
			yy = SNL3_12_SN(:,2);
			hCurve = plot(xx,yy,'--r','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#3 ORNL-Booth M1.85' ];
		end
	end

	
    MASS_SN = 49.48./1000;  %pure tin
    MASS_SNO2 = 62.82./1000;  %tin oxide
    x2 = EDF_RELEASE(:,1);
    y2 = EDF_RELEASE(:,36);
    y22 = y2./MASS_SNO2;   
    hCurve = plot(x2,y22,'-','LineWidth',1,'MarkerSize',2); labels = [labels, [ case1_name] ];
	hCurve.Color = cmapdyn(k_num,:);
    ylabel('Fraction, [-]');
    %labels = { 'EXP' 'CRs' 'Fuel'  };
    %labels = { 'EXP' 'CRs Only'};
    run standard_plot2.m
%end




% % ===================================================================
% % ===================================================================
% % ===================================================================
% % ===================================================================
% % ===================================================================
% % ===================================================================







% % ===================================================================
% % FIGURE Cadmium (including. Antimony Sb)
% % ===================================================================
% a=a+1
% figure(a)
% hold on; set(0, 'DefaulttextInterpreter', 'none');
% title('Cadmium Class Release - Antimony (Sb). From Fuel');
% name = ['Fig', num2str(a)];
% 
% x1 = EXP_SB(:,1);
% y1 = EXP_SB(:,2)./100;
% 
% hCurve = plot(x1,y1,'ko','LineWidth',1,'MarkerSize',2);
% 
% x2 = EDF_RELEASE(:,1);
% y2 = EDF_RELEASE(:,12)./INVENTORY(10);
% 
% hCurve = plot(x2,y2,'-r','LineWidth',1,'MarkerSize',2);
% 
% 
% xlabel('Time, [s]');
% ylabel('Fraction, [-]');
% legend('Location','northwest');
% grid on; xlim([0, 30000]); xlim([0, 30000]);
% box on; % ylim([0 inf]);
% 
% labels = { 'EXP' 'Only Fuel' };
% legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');
% 
% if (flag2 == 1)
% 	print('-dtiff',[case1,'/', name, code1, '.tiff'])
%     savefig([case1,'/', name, code1, '.fig'])
%     %saveas(gcf,[Fig1.fig'],'fig');
% end
% xlim([0, 30000]); 


% ===================================================================
% FIGURE Cadmium (from AIC+Fuel)
% ===================================================================
%a=a+1
%figure(a)
%hold on; set(0, 'DefaulttextInterpreter', 'none');
%title('Cadmium  Release - From CRs and Fuel as Antimony');
%name = ['Fig', num2str(a)];
%
%x1 = EXP_SB(:,1);
%y1 = EXP_SB(:,2)./100;
%
%x11 = EXP_CD(:,1);
%y11 = EXP_CD(:,2)./100;
%
%
%
%
%hCurve = plot(x1,y1,'ko','LineWidth',1,'MarkerSize',2);
%hCurve = plot(x11,y11,'ks','LineWidth',1,'MarkerSize',2);
%
%
%xt = EDF_RELEASE(:,1);
%
%y3 = EDF_RELEASE(:,34)
%y31 = y3./INVENTORY_STR(2);  %From Control Rods
%
%y2 = EDF_RELEASE(:,12)
%y21 = y2./INVENTORY(10);     %From Fuel
%
%
%y40 = (y2+y3)./(INVENTORY_STR(2)+INVENTORY(10));
%
%hCurve = plot(xt,y31,'-r','LineWidth',1,'MarkerSize',2);
%hCurve = plot(xt,y21,'-b','LineWidth',1,'MarkerSize',2);
%hCurve = plot(xt,y40,'-k','LineWidth',1,'MarkerSize',2);
%
%
%xlabel('Time, [s]');
%ylabel('Fraction, [-]');
%legend('Location','northwest');
%grid on; xlim([0, 30000]); xlim([0, 30000]);
%box on; % ylim([0 inf]);
%
%labels = { 'EXP Sb' 'EXP Cd CRs'   'CRs' 'Fuel' 'Fuel+CRs'};
%legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');
%
%if (flag2 == 1)
%	print('-dtiff',[case1,'/', name, code1, '.tiff'])
%    savefig([case1,'/', name, code1, '.fig'])
%    %saveas(gcf,[Fig1.fig'],'fig');
%end
%xlim([0, 30000]); 










