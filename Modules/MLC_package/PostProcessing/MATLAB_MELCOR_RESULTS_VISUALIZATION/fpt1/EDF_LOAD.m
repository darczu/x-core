    % IMPORT EDF FILES
    EDF_H2 = importdata([case1 '/EDF_H2']);
    EDF_COND = importdata([case1 '/EDF_COND']);
    EDF_POWER = importdata([case1 '/EDF_POWER']);
    EDF_PRESS = importdata([case1 '/EDF_PRESS']);
    EDF_RHUM = importdata([case1 '/EDF_RHUM']);
    EDF_TVAP = importdata([case1 '/EDF_TVAP']);
    EDF_TEMP_HS = importdata([case1 '/EDF_TEMP_HS']);
    EDF_TEMP_CL = importdata([case1 '/EDF_TEMP_CL']);
    EDF_TEMP_FU = importdata([case1 '/EDF_TEMP_FU']);
    %EDF_TEMP_SH = importdata([case1 '/EDF_TEMP_SH']);
    EDF_CONT_MASS = importdata([case1 '/EDF_CONT_MASS']);  % NEw nodes
    EDF_TEMP_SH = importdata([case1 '/EDF_TEMP_SH2']);  % NEw nodes
    if flagRN==1
        EDF_CONT_DEP    =  importdata([case1 '/EDF_CONT_DEP']);
        EDF_CONT_PARAMS =  importdata([case1 '/EDF_CONT_PARAMS']);
        EDF_RELEASE     =  importdata([case1 '/EDF_RELEASE']);
    end
    if flagATMG==1
        EDF_ATMG_400 = importdata([case1 '/EDF_ATMG_400']);
        EDF_TYCLAIR = importdata([case1 '/EDF_RN1_TYCLAIR-14-1_10']);
    end
	
	EDF_COR_TNS= [];
	if flagCORTNS == 1
		EDF_COR_TNS_ALL = importdata([case1 '/EDF_COR_TNS']);
		EDF_COR_TNS = EDF_COR_TNS_ALL.data;
	end
	
	EDF_COR_MASS = [];
	if flagCORMASS == 1
		EDF_COR_MASS_ALL = importdata([case1 '/EDF_COR_MASS']);
		EDF_COR_MASS = EDF_COR_MASS_ALL.data;
	end
	