% ===================================================================
% FIGURE  - Upper Plenum and Raiser Temperatures
% ===================================================================
a=a+1;
figure(a)
name = ['Fig', num2str(a)];

hold on; set(0, 'DefaulttextInterpreter', 'none');
title('Upper Plenum and Raiser Temperatures');

x1 = EXP_TVAP_UP_3530mm(:,1);
y1 = EXP_TVAP_UP_3530mm(:,3);
x2 = EXP_TVAP_UP_4330mm(:,1);
y2 = EXP_TVAP_UP_4330mm(:,3);
x3 = EXP_TVAP_UP_6192mm(:,1);
y3 = EXP_TVAP_UP_6192mm(:,3);

plot(x1,y1,'<k','LineWidth',1,'MarkerSize',3);
plot(x2,y2,'sk','LineWidth',1,'MarkerSize',3);
plot(x3,y3,'xk','LineWidth',1,'MarkerSize',3);

xt  = EDF_TVAP(:,1);
y10 = EDF_TVAP(:,4);
y11 = EDF_TVAP(:,5);

if (flagBEND ==1)
 y12 = EDF_TVAP(:,17);
end

plot(xt,y10,'-','LineWidth',1,'MarkerSize',2,'Color',cmap6(1,:));
plot(xt,y11,'-','LineWidth',1,'MarkerSize',2,'Color',cmap6(3,:));

if (flagBEND ==1)
 plot(xt,y12,'-','LineWidth',1,'MarkerSize',2,'Color',cmap6(6,:));
end

xlabel('Time, [s]');
ylabel('Temperature, [K]');
legend('Location','northeast');
box on; %ylim([0 inf]); 
grid on; xlim([0, 30000]);

if (flagBEND ==1)
    labels = { 'EXP TC59 -3530mm' 'EXP TC58 -4330mm' 'EXP TC55 -6192mm' ...
   'UP1' 'UP2' 'RISER-BEND'};
else

    labels = { 'EXP TC59 -3530mm' 'EXP TC58 -4330mm' 'EXP TC55 -6192mm' ...
   'UP1' 'UP2'};
end
   
legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end

% ===================================================================
% FIGURE  - Hot Leg Temperatures
% ===================================================================
a=a+1;
figure(a)
name = ['Fig', num2str(a)];

hold on; set(0, 'DefaulttextInterpreter', 'none');
title('Hot Leg Temperatures');

x1 = EXP_TVAP_HL_204mm(:,1);
y1 = EXP_TVAP_HL_204mm(:,3);
x2 = EXP_TVAP_HL_579mm(:,1);
y2 = EXP_TVAP_HL_579mm(:,3);
x3 = EXP_TVAP_HL_1079mm(:,1);
y3 = EXP_TVAP_HL_1079mm(:,3);
x4 = EXP_TVAP_HL_2079mm(:,1);
y4 = EXP_TVAP_HL_2079mm(:,3);
x5 = EXP_TVAP_HL_4079mm(:,1);
y5 = EXP_TVAP_HL_4079mm(:,3);
x6 = EXP_TVAP_HL_5068mm(:,1);
y6 = EXP_TVAP_HL_5068mm(:,3);
x7 = EXP_TVAP_HL_5143mm(:,1);
y7 = EXP_TVAP_HL_5143mm(:,3);

plot(x1,y1,'<k','LineWidth',1,'MarkerSize',3);
plot(x2,y2,'sk','LineWidth',1,'MarkerSize',3);
plot(x3,y3,'.k','LineWidth',1,'MarkerSize',3);
plot(x4,y4,'>k','LineWidth',1,'MarkerSize',3);
plot(x5,y5,'ok','LineWidth',1,'MarkerSize',3);
plot(x6,y6,'dk','LineWidth',1,'MarkerSize',3);
plot(x7,y7,'+k','LineWidth',1,'MarkerSize',3);

xt  = EDF_TVAP(:,1);
y10 = EDF_TVAP(:,6); %HL1
y11 = EDF_TVAP(:,7); %HL2
y12 = EDF_TVAP(:,8); %SG-IN

plot(xt,y10,'-','LineWidth',1,'MarkerSize',2,'Color',cmap6(1,:));
plot(xt,y11,'-','LineWidth',1,'MarkerSize',2,'Color',cmap6(3,:));
plot(xt,y12,'-','LineWidth',1,'MarkerSize',2,'Color',cmap6(6,:));

xlabel('Time, [s]');
ylabel('Temperature, [K]');
legend('Location','northeast');
box on; %ylim([0 inf]); 
grid on; xlim([0, 30000]);

labels = { 'EXP TEPF807 204mm' 'EXP TEPF810 579mm' 'EXP TEPF812 1079mm' 'EXP TEPF813 2079mm' 'EXP TEPF814 4079mm' 'EXP TEPF815 5068mm' 'EXP TEPF816 5143mm'...
   'HL1' 'HL2' 'SG-IN'};
legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end

% ===================================================================
% FIGURE  - SG Temperatures
% ===================================================================
a=a+1;
figure(a)
name = ['Fig', num2str(a)];

hold on; set(0, 'DefaulttextInterpreter', 'none');
title('SG Temperatures');

x1 = EXP_TVAP_SG_320(:,1);
y1 = EXP_TVAP_SG_320(:,2);
x2 = EXP_TVAP_SG_740(:,1);
y2 = EXP_TVAP_SG_740(:,2);
x3 = EXP_TVAP_SG_1500(:,1);
y3 = EXP_TVAP_SG_1500(:,2);
x4 = EXP_TVAP_SG_2000(:,1);
y4 = EXP_TVAP_SG_2000(:,2);
x5 = EXP_TVAP_SG_3000(:,1);
y5 = EXP_TVAP_SG_3000(:,2);
plot(x1,y1,'<k','LineWidth',1,'MarkerSize',3);
plot(x2,y2,'sk','LineWidth',1,'MarkerSize',3);
plot(x3,y3,'xk','LineWidth',1,'MarkerSize',3);
plot(x4,y4,'ok','LineWidth',1,'MarkerSize',3);
plot(x5,y5,'>k','LineWidth',1,'MarkerSize',3);

xt = EDF_TVAP(:,1);
y20 = EDF_TVAP(:,8);
y21 = EDF_TVAP(:,9);
y22 = EDF_TVAP(:,10);
y23 = EDF_TVAP(:,11);
y24 = EDF_TVAP(:,12);
y25 = EDF_TVAP(:,13);
y26 = EDF_TVAP(:,14);

%plot(xt,y20,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(1,:));
plot(xt,y21,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(2,:));
plot(xt,y22,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(3,:));
plot(xt,y23,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(4,:));
plot(xt,y24,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(5,:));
plot(xt,y25,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(6,:));
%plot(xt,y26,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(7,:));


xlabel('Time, [s]');
ylabel('Temperature, [K]');
legend('Location','northeast');
box on; %ylim([0 inf]); 
grid on; xlim([0, 30000]);

%labels = { 'EXP 320mm' 'EXP 740mm' 'EXP 1500mm' 'EXP 2000mm' 'EXP 3000mm' ...
%  'SG-IN' 'SG-U1' 'SG-U2' 'SG-U3' 'SG-U4' 'SG-D1'  'SG-CL1'};

labels = { 'EXP 320mm' 'EXP 740mm' 'EXP 1500mm' 'EXP 2000mm' 'EXP 3000mm' ...
'SG-U1' 'SG-U2' 'SG-U3' 'SG-U4' 'SG-D1' };
legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
    print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end

