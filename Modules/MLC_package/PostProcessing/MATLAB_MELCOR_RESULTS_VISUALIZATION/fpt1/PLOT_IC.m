% TH only results
% 
clear; clc; close all;
format short g

flag2 = 1;

% LOAD DATA
load EXP_DATA3;

labels={};
%yyaxis left
%yyaxis right
%INLET_STEAM
%INLET_ENTHALPY

EDF_POWER = importdata(['EDF_POWER']);
%x1 = EDF_POWER(:,1);
%y1 = EDF_POWER(:,8); %EFPD
x1 =  POWER_INPUT_MELCOR2(:,1);
y1 = POWER_INPUT_MELCOR2(:,2);

%x2 = INLET_STEAM(:,1);
%y2 = INLET_STEAM(:,2);
x2 = INLET_STEAM_ALT(:,1);
y2 = INLET_STEAM_ALT(:,2);


% ===================================================================
% Power and Steam Flow
% ===================================================================
a=1
figure(a); hold on; box on; grid on; labels={};
title('Inlet Steam and Fission Power');
xlabel('Time, [s]'); 
xlim([0 20000]);

yyaxis left;
plot(x1,y1,'-o','LineWidth',2,'MarkerSize',2); labels = [labels, 'Fission Power'];
ylabel('Power, [W]');

yyaxis right;
plot(x2,y2,'-o','LineWidth',2,'MarkerSize',2);  labels = [labels, 'Steam Flow Rate' ];
ylabel('Flow Rate, [kg/s]');

legend(labels);
legend('Location','northwest');

if (flag2 == 1)
	print('-dtiff',['IC', num2str(a),'.tiff'])
    savefig(['IC', num2str(a),'.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end

% ===================================================================
% Power Distribution
% ===================================================================
% Peaked power
x3 = EXP_POWER_DIST_RAW (:,1); %raw power
y3 = EXP_POWER_DIST_RAW (:,2);
xx3 = MLC_CORE_AXIAL(:,1); %MELCOR model central locations
yy3 = interp1(x3,y3,xx3);

% Reference power
x4 = EXP_POWER_DIST_RAW_NONPEAK(:,1); %MELCOR model central locations
y4 = EXP_POWER_DIST_RAW_NONPEAK(:,3); %Non-peaked power distribution
xx4 = MLC_CORE_AXIAL(:,1); %MELCOR model central locations
yy4 = interp1(x4,y4,xx4,'Linear','extrap');


a = a+1;
figure(a); hold on; box on; grid on; labels={}; 
title('Axial Power Distribution');
xlabel('Location, [m]'); ylabel('Relative Power, [-]');
plot(x3,y3,'xb','LineWidth',1,'Color',[0.00,0.45,0.74],'MarkerSize',5); labels = [labels, 'Peaked, March et. al (2013)'];
plot(xx3,yy3,'ob','LineWidth',1,'Color',[0.00,0.45,0.74],'MarkerSize',5); labels = [labels, 'Peaked, Interpolation'];
plot(x4,y4,'x','Color',[0.85,0.33,0.10],'LineWidth',1,'MarkerSize',5); labels = [labels, 'Reference, Haste et. al (2003)'];
plot(xx4,yy4,'o','Color',[0.85,0.33,0.10],'LineWidth',1,'MarkerSize',5); labels = [labels, 'Reference, Interpolation'];
legend(labels);
legend('Location','south');

if (flag2 == 1)
	print('-dtiff',['IC', num2str(a),'.tiff'])
    savefig(['IC', num2str(a),'.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end
% ===================================================================
% UO2 and ZRO2 Enthalpies
% ===================================================================
x5 = ENTHALPY_UO2(:,2);
y5 = ENTHALPY_UO2(:,3);
x6 = ENTHALPY_ZRO2(:,2);
y6 = ENTHALPY_ZRO2(:,3);

a = a+1;
figure(a); hold on; box on; grid on; labels={}; 
title('ZrO2 and UO2 Enthalpy');
xlabel('Temperature, [K]'); ylabel('Enthalpy, [J]');
plot(x5,y5,'-o','LineWidth',2,'MarkerSize',4); labels = [labels, 'UO2'];
plot(x6,y6,'-s','LineWidth',2,'MarkerSize',4); labels = [labels, 'ZrO2'];
legend(labels); legend('Location','northwest');

if (flag2 == 1)
	print('-dtiff',['IC', num2str(a),'.tiff'])
    savefig(['IC', num2str(a),'.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end

% ===================================================================
% GAP Closure
% ===================================================================
x7 = THC_GAP_NR1(:,2);
y7 = THC_GAP_NR1(:,3);
x8 = THC_GAP_SNL(:,2); %+273.15;
y8 = THC_GAP_SNL(:,3);
%x9 = THC_GAP_SNL(:,2)+273.15;
%y9 = THC_GAP_SNL(:,3);


a = a+1;
figure(a); hold on; box on; grid on; labels={}; 
title('Gap Closure Model');
xlabel('Temperature, [K]'); ylabel('Thermal Conductivity, [W/m/K]');
plot(x7,y7,'-o','LineWidth',2,'MarkerSize',4); labels = [labels, 'NR1'];
plot(x8,y8,'-s','LineWidth',2,'MarkerSize',4); labels = [labels, 'SNL'];
%plot(x9,y9,'-s','LineWidth',2,'MarkerSize',4); labels = [labels, 'SNL corrected'];
legend(labels); legend('Location','northwest');
xlim([0, 2000]); ylim([0 12])
if (flag2 == 1)
	print('-dtiff',['IC', num2str(a),'.tiff'])
    savefig(['IC', num2str(a),'.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end

