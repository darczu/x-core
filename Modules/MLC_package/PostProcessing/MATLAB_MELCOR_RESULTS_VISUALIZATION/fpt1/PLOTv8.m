% TH only results
%A = rand(5);
%B = magic(10);
% save myFile.mat A B -v7.3 -nocompression
%save('myFile.mat','A','B','-v7.3','-nocompression')
% fmt = '-ascii','-double','-tabs'
% save(filename,variables,fmt)
% save('test.mat','-nocompression','-v7.3','-ascii','-double','-tabs')
% new approach we will use CSV files
% To read CSV files
% csvread('./DATA/Default Dataset.csv')  older matlba
% use A = readmatrix(filename) 		matlab 2019
% 
clear; clc; close all;
format short g
% ===================================================================
% LOAD DATA
load EXP_DATA6;

flag2 = 1; %SAVE FIGURE 1-YES 0-NO; 
flag3 = 0;  % 0 -save to 1st folder; 1 - save to new folder

flagSS = 0; %Steady State Plot
flagTH = 0; %Thermal Hyd. Plot core
flagRN = 1;  %1 - RN results avaialble  0 - RN not avialalble
flagSS_EXP=0; %Experimental Data for Steady State
flagCONT_TH = 0; %Containment TH results
flagRCS_TH = 0; %RCS TH results
flagRN_DEP = 0; %Plot RN depositions
flagIntegralE = 1; % Integrate energy
% OLD flag - try to not change them
flag_OLD_TEMPS = 1; % 1 - old temperature plots for single case ; 1 - NEW with new plots allowing comparison
flagATMG = 0; % ATMG - odczytane manualnie z pliku - nowy melcor nei ptorzbuje
flagWET = 0;  %Other options nie uzywaj
flagKONDENSAT = 1; 
flagBEND = 1; %Added Bend
% flagTin = 1; %Tin calculation
flagCORMASS = 0; %COR-MASS ASCII FILE AVAILABLE
flagCORTNS  = 0; %COR-TNS ASCII FILE AVAILABLE
 

flagFPT21 = 1; %Show results for FPT21
flagFPT22 = 1; %Show results for FPT22
flagSNL2 = 1;  %Show results for SNL2
flagSNL3 = 1;  %Show results for SNL3
flagSNLISP = 1;
flagSNLQL  = 1; %Show results for  SNL new MELCOR release note
flagPS1ISP = 1; 

% ===================================================================
%code1 = 'MELCOR2.1.6341';
%code1 ='M2.2.9541';
code1 ='M2.2.11932';
%code1 ='M2.2';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AIC comparison
%{
cases = {'D3'  ...  %'D3H' 'D3I' ... 
		'D3AB' ...  %'D3AB2'  ...%'D3BB2' 'D3BC2' 'D3BD2' ... % ...%'D3BC3' 'D3BC4' 'D3BC5' 'D3BC6' 
		'D3BC7' ...
		...%'D3D' 'D3E' 'D3F' 'D3G'	 ...  %'D3BI' 'D3BJ' ...
		};
%}
%{
cases_names = {	'WUT#0  AIC on CB(-5) M2.2' ... %'WUT#0A AIC on CB(-7) M2.2' ...  %'WUT#0B AIC on CB(-3) M2.2' ...
 			    'WUT#0  AIC on CB(-5) M2.1' ... %'WUT#0B AIC on CB(-3) M2.1' ...
				...%'WUT#1  AIC off CB(-5)  M2.2' ...
				...%'WUT#1A AIC off CB(-7)  M2.2' ...
				...%'WUT#1B AIC off CB(-3)  M2.2'  ... 
				...%'WUT#1C AIC off CB(-7)  M2.2' 'WUT#1D AIC off CB(-7)  M2.2' 'WUT#1E AIC off CB(-7)  M2.2' 'WUT#1F AIC off CB(-7)  M2.2' 
				'WUT#1G AIC off CB(-7)  M2.2' ...
				%'WUT#3 Power Profile' 'WUT#4 Gap Model' 'WUT#5 CR fail -100K' 'WUT#6 CR fail +100K' ... 
  				%'D3BI M2.1 (-3) AIC off' 'D3BJ M2.1 (-5) AIC off' ... };
				};
%}

cases = {'D3'  'D3BB2' 'D3BC' 'D3BI2' 'D3BJ2'}
cases_names = {'WUT#0 AIC on [-5]' 'WUT#1 AIC Off [-5]'  'D3BC AIC Off [-7]' 'D3BD AIC Off [-3]' 'D3BI2 AIC Off [-3] M2.1' 'D3BJ2 AIC Off [-5] M2.1' }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AIC comparison
%cases = {'D3'  'D3BB' 'D3BC' 'D3BD' 'D3BE' 'D3BF' 'D3BG' 'D3BH'}
%cases_names = {'WUT#0' 'WUT#1 AIC Off [-5]'  'D3BC AIC Off [-7]' 'D3BD AIC Off [-3]' 'D3BE AIC Off [-1]' 'D3BF AIC Off [-2]' 'D3BG AIC Off [1]' 'D3BH AIC Off [2]'}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Normal Comparison of RNs
% %{
%cases = {'D3' 'D3H' 'D3I' 'D3AB' 'D3AB2' };
%cases_names = {'WUT#0 CB(-5) M2.2' 'WUT#7 CB(-7) M2.2' 'WUT#8 CB(-3) M2.2' 'WUT#9 CB(-5) M2.1' 'WUT#10 CB(-3) M2.1'  };
%% cases 		= {'D3' 'D3AB2' }; 
%% %'D3H' 'D3I' 'D3AB' 
%% cases_names = {'WUT#0_ CB(-5) M2.2' ...
%% 			  ...% 'WUT#0A CB(-7) M2.2' ... 
%% 			  ...% 'WUT#0B CB(-3) M2.2' ...
%% 			  ...% 'WUT#0_ CB(-5) M2.1' ...
%% 			   'WUT#0B CB(-3) M2.1' ...			   
%% 				};
% %}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Molybdenum comparison
% %{
% cases 		= {'D3'  'D3H' 'D3I' 'D3AB' 'D3AB2' 'D3C' 'D3CA' 'D3CB' 'D3CC' 'D3CD' };
% cases_names = {'WUT#0_ CB(-5) M2.2' ...
% 			   'WUT#0A CB(-7) M2.2' ... 
% 			   'WUT#0B CB(-3) M2.2' ...
% 			   'WUT#0_ CB(-5) M2.1' ...
% 			   'WUT#0B CB(-3) M2.1' ...
% 			   'WUT#2_ MO CB(-5) M2.2' ...
% 			   'WUT#2A MO CB(-7) M2.2' ...
% 			   'WUT#2A MO CB(-3) M2.2' ...
% 			   'WUT#2_ MO CB(-5) M2.1' ...
% 			   'WUT#2B MO CB(-3) M2.1' ...			   
% 				};
% %}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ALL
%{
 cases 		= {'D3'  'D3H' 'D3I'  'D3C' 'D3CA' 'D3CB'  ...
 		 'D3BB2' 'D3BC2' 'D3BD2' 'D3BC7'  ... 
		'D3D' 'D3E' 'D3F' 'D3G'	...
 };
 cases_names = {'#0_ CB(-5) M2.2' ...
 			   '#0A CB(-7) M2.2' ... 
 			   '#0B CB(-3) M2.2' ...
 			   '#2_ MO CB(-5) M2.2' ...
 			   '#2A MO CB(-7) M2.2' ...
 			   '#2B MO CB(-3) M2.2' ...
				'#1  AIC off CB(-5)  M2.2' ...
				'#1A AIC off CB(-7)  M2.2' ...
				'#1B AIC off CB(-3)  M2.2'  ... 
				'#1C AIC off CB(-7)  M2.2' ...
				'#3 Power Profile' ... 
				'#4 Gap Model' ...
				'#5 CR fail -100K' ...
				'#6 CR fail +100K' ... 	   
 				};
				%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%cases = {'D3' 'D3AB'}
%cases_names = {'M2.2' 'M2.1'}

		
if numel(cases) == 1 
    flag3 = 0;  % 0 -save to 1st folder; 1 - save to new folder
end

if numel(cases) > 1 
    flag3 = 1;  % 0 -save to 1st folder; 1 - save to new folder
end


%cases =  {'TEST0022_M22_B1'  'TEST0022_M22_B10'}; % 'TEST0022_M22_B2'}; 
%cases_names = {'B1' 'B10'}; 
% labels={};
% labels = [labels, 'Fission Power'];

% ===================================================================
cmap6 = hsv(6); cmap11 = hsv(11); cmap14 = hsv(14); cmap28 = hsv(28); cmap16 = hsv(16); %color maps
cmapT = ['k', 'r', 'b', 'm', 'y','c','k','b','r','m','c','y'];

smap = {'-', '--', '-.', '.','-x','-o','-s','<-','s-'};

% Color map with different color for different case
%cmapdyn = hsv(numel(cases));
%cmapdyn = [lines(7); hsv(numel(cases)-7)];
%cmapdyn = [lines(7); jet(numel(cases)-7+2)];
%cmapdyn = [lines(6); hsv(numel(cases)-7+2)];
%cmapdyn = [lines(7); prism(6); colorcube(10)];

cmapdyn = [lines(7); colorcube(15)]
%cmapdyn = [linespacer(22)]

%cmapdyn = [lines(7); prism(6); varycolor(20)];
%cmapdyn = [lines(7); prism(6); linespacer(20)]; %%
%cmapdyn = [lines(7);  linespacer(20)]; %%
%cmapdyn = [lines(7);  varycolor(20)]; %%
%cmapdyn = linespacer(numel(cases));
%cmapdyn = linespacer(numel(cases),'qualitative');
%cmapdyn = linespacer(numel(cases),'sequential');
% colormapTestImage(cmapdyn)

% ===================================================================
for k_num=1:numel(cases)
    case1 =  cases{k_num};  % current case folder
    case1_name = cases_names{k_num}; % current case name

	run EDF_LOAD.m 
% ===================================================================

    % ===================================================================
    if flagTH ==1
        a=0;
        run PLOT_TH.m
    end

    
    % ===================================================================
    if flagCONT_TH == 1
        a = 50;
        run PLOT_CONT_TH.m  
    end
    % ===================================================================
    if flagRCS_TH == 1
        a = 100 ;
        run PLOT_RCS_TH.m
    end
    % ===================================================================
    if flagRN == 1
        a=150;
        run PLOT_RN_6.m
    end
    % ===================================================================
    if flagRN_DEP == 1
        a = 200;
        run PLOT_DEP.m
    end
    % ===================================================================
    if flagSS == 1
        a = 300;
        run PLOT_SS.m
    end
    % ===================================================================
    
end    

% ===================================================================
%annotation(gcf,'textbox', [0.02 0.02 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none');
%
%annotation(figure1,'textbox',...
%    [0.02 0.02 0.02 0.02],...
%    'String',{'ssss'},...
%    'FitBoxToText','off',...
%    'EdgeColor','none');

% ===================================================================
% ===================================================================
