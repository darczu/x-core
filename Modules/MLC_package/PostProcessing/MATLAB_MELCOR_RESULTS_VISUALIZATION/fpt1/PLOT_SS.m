 %===================================================================
% ===================================================================
% ===================================================================


% ===================================================================
% FIGURE 
% ===================================================================


figure(a)
hold on; set(0, 'DefaulttextInterpreter', 'none');
title('Steady State Pressure');
name = ['Fig', num2str(a)];

x = EDF_PRESS(:,1);
y1 = EDF_PRESS(:,16); %Cont
y2 = EDF_PRESS(:,3); %Core
y3 = EDF_PRESS(:,17); %Raiser-Bend
y4 = EDF_PRESS(:,12); %SG-U4
y5 = EDF_PRESS(:,14); %SG-CL1



plot(x,y1,'-','LineWidth',1,'MarkerSize',2,'Color',cmap6(1,:));
plot(x,y2,'-','LineWidth',1,'MarkerSize',2,'Color',cmap6(2,:));
plot(x,y3,'-','LineWidth',1,'MarkerSize',2,'Color',cmap6(3,:));
plot(x,y4,'-','LineWidth',1,'MarkerSize',2,'Color',cmap6(4,:));
plot(x,y5,'-','LineWidth',1,'MarkerSize',2,'Color',cmap6(5,:));



xlabel('Time, [s]');
ylabel('Pressure, [Pa]');
legend('Location','southwest');
grid on; xlim([-500, 100.0]); 
box on;  ylim([1.8e5 2.3e5]);

labels = { 'Containment' 'Core ' 'Bundle Exit' 'SG Upper Part' 'Cold Leg'};
legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end


% ===================================================================
% FIGURE 
% % ===================================================================
% a = a+1;
% 
% figure(a)
% hold on; set(0, 'DefaulttextInterpreter', 'none');
% title('Steady State Containment HS Temperatures');
% name = ['Fig', num2str(a)];
% 
% x = EDF_PRESS(:,1);
% y1 = EDF_PRESS(:,16); %Cont
% y2 = EDF_PRESS(:,3); %Core
% y3 = EDF_PRESS(:,17); %Raiser-Bend
% y4 = EDF_PRESS(:,12); %SG-U4
% y5 = EDF_PRESS(:,14); %SG-CL1
% 
% 
% 
% plot(x,y1,'-','LineWidth',1,'MarkerSize',2,'Color',cmap6(1,:));
% plot(x,y2,'-','LineWidth',1,'MarkerSize',2,'Color',cmap6(2,:));
% plot(x,y3,'-','LineWidth',1,'MarkerSize',2,'Color',cmap6(3,:));
% plot(x,y4,'-','LineWidth',1,'MarkerSize',2,'Color',cmap6(4,:));
% plot(x,y5,'-','LineWidth',1,'MarkerSize',2,'Color',cmap6(5,:));
% 
% 
% 
% xlabel('Time, [s]');
% ylabel('Pressure, [Pa]');
% legend('Location','southwest');
% grid on; xlim([-500, 100.0]); 
% box on;  ylim([1.8e5 2.3e5]);
% 
% labels = { 'Containment' 'Core ' 'Bundle Exit' 'SG Upper Part' 'Cold Leg'};
% legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');
% 
% if (flag2 == 1)
% 	print('-dtiff',[case1,'/', name, code1, '.tiff'])
%     savefig([case1,'/', name, code1, '.fig'])
%     %saveas(gcf,[Fig1.fig'],'fig');
% end


% ===================================================================
% FIGURE 
% ===================================================================
a=a+1;

figure(a)
hold on; set(0, 'DefaulttextInterpreter', 'none');
title('Steady State Temperatures');
name = ['Fig', num2str(a)];

x  = EDF_TVAP(:,1);

y1 = EDF_TVAP(:,16); %Cont
y2 = EDF_TVAP(:,3); %Core
y3 = EDF_TVAP(:,4); %UP1
y4 = EDF_TVAP(:,5); %UP2
y5 = EDF_TVAP(:,17); %Raiser-Bend

y6 = EDF_TVAP(:,6); %HL1
y7 = EDF_TVAP(:,7); %HL2
y8 = EDF_TVAP(:,8); %SG-IN

y9 = EDF_TVAP(:,9); %SG-U1
y10 = EDF_TVAP(:,10); %SG-U2
y11 = EDF_TVAP(:,11); %SG-U3
y12 = EDF_TVAP(:,12); %SG-U4
y13 = EDF_TVAP(:,13); %SG-D1
y14 = EDF_TVAP(:,14); %SG-CL1
y15 = EDF_TVAP(:,15); %Cl2-CONT
y16 = EDF_TVAP(:,18); %SUMP LIQUID


k=1;
plot(x,y1,'-','LineWidth',1,'MarkerSize',2,'Color',cmap16(k,:)); k=k+1;
plot(x,y2,'-','LineWidth',1,'MarkerSize',2,'Color',cmap16(k,:)); k=k+1;
plot(x,y3,'-','LineWidth',1,'MarkerSize',2,'Color',cmap16(k,:)); k=k+1;
plot(x,y4,'-','LineWidth',1,'MarkerSize',2,'Color',cmap16(k,:)); k=k+1;
plot(x,y5,'-','LineWidth',1,'MarkerSize',2,'Color',cmap16(k,:)); k=k+1;
plot(x,y6,'-','LineWidth',1,'MarkerSize',2,'Color',cmap16(k,:)); k=k+1;
plot(x,y7,'-','LineWidth',1,'MarkerSize',2,'Color',cmap16(k,:)); k=k+1;
plot(x,y8,'-','LineWidth',1,'MarkerSize',2,'Color',cmap16(k,:)); k=k+1;
plot(x,y9,'-','LineWidth',1,'MarkerSize',2,'Color',cmap16(k,:)); k=k+1;
plot(x,y10,'-','LineWidth',1,'MarkerSize',2,'Color',cmap16(k,:)); k=k+1;
plot(x,y11,'-','LineWidth',1,'MarkerSize',2,'Color',cmap16(k,:)); k=k+1;
plot(x,y12,'-','LineWidth',1,'MarkerSize',2,'Color',cmap16(k,:)); k=k+1;
plot(x,y13,'-','LineWidth',1,'MarkerSize',2,'Color',cmap16(k,:)); k=k+1;
plot(x,y14,'-','LineWidth',1,'MarkerSize',2,'Color',cmap16(k,:)); k=k+1;
plot(x,y15,'-','LineWidth',1,'MarkerSize',2,'Color',cmap16(k,:)); k=k+1;
plot(x,y16,'-','LineWidth',1,'MarkerSize',2,'Color',cmap16(k,:)); k=k+1;

xlabel('Time, [s]');
ylabel('Temperature, [K]');
legend('Location','southwest');
grid on; xlim([-500, 100.0]); 
box on;  ylim([0 1000.0]);

labels = { 'CONT' 'CORE' 'UP1' 'UP2' 'RAISER-BEND' 'HL1' 'SG-IN'  'SG-U1'  'SG-U2'  'SG-U3' 'SG-U4' 'SG-D1' 'SG-CL1' 'CL2-CONT' 'SUMP LIQUID'};
%labels = { 'CONT' 'CORE' 'UP1' 'UP2' 'RAISER-BEND' 'HL1' 'SG-IN'  'SG-U1'  'SG-U2'  'SG-U3' 'SG-U4' 'SG-D1' 'SG-CL1' 'CL2-CONT'};
legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end




% ===================================================================
% FIGURE - Fuel Temperatures
% ===================================================================
for ring=1:2
   
   if(flagSS_EXP ==1)
	if (ring == 2)
	  flagSS_EXP2 = 1;
	else
	  flagSS_EXP2 = 0;
	end
   end
   
	a=a+1;
	figure(a)
	name = ['Fig', num2str(a)];

	hold on; set(0, 'DefaulttextInterpreter', 'none');
	title(['Steady State Fuel Temp. Ring', int2str(ring), ' COR6: 300mm and COR7: 400mm ' ]);
	%title('Fuel Temperatures');
	labels = {};

	x = EDF_TEMP_FU(:,1); %time
	col=1;

		%for level=1:14
		%for ring=1:2
	for level=4:13 %ALL CORE
	%for level=5:8
		  offset = (ring - 1)*14;
		  y = EDF_TEMP_FU(:,1+ level + offset); 	  
		  plot(x,y,'-','LineWidth',1,'MarkerSize',2,'Color',cmap14(col,:)); %or cmap1
		  
		  if(level<10)
			labels = {labels{:}, ['COR-TFU ', int2str(ring), '0', int2str(level)]};
		  else
			labels = {labels{:}, ['COR-TFU ', int2str(ring), '', int2str(level)]};
		  end
		  
		  col = col+1;
		%end

		end

	xlabel('Time, [s]');
	ylabel('Temperature, [K]');
	legend('Location','northwest');
	box on; %ylim([0 inf]);  
	grid on; xlim([-500, 100]);

	
	if flagSS_EXP2 == 1
		x1 = EXP_T_AVG_OUT_FUEL_300mm(:,1);
		y1 = EXP_T_AVG_OUT_FUEL_300mm(:,2);
		x2 = EXP_T_AVG_OUT_FUEL_400mm(:,1);
		y2 = EXP_T_AVG_OUT_FUEL_400mm(:,2);
		
		plot(x1,y1,'ok','LineWidth',1,'MarkerSize',3);
		plot(x2,y2,'or','LineWidth',1,'MarkerSize',3);
		
		labels = {labels{:} ,  'EXP Out. 300mm',  'EXP Out. 400mm' };
		
	
	else
		labels = {labels{:}};
	end
	%labels = {' '};
	
	% 300mm for MELCOR it is Level 6
	% 400mm for MELCOR it is Level 7

    labels = {labels{:}  };
	
	legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');
	if (flag2 == 1)
		print('-dtiff',[case1,'/', name, code1, '.tiff'])
		savefig([case1,'/', name, code1, '.fig'])
		%saveas(gcf,[Fig1.fig'],'fig');
	end
end

% ===================================================================
% FIGURE - Clad Temperatures
% ===================================================================
for ring=1:2

   if(flagSS_EXP ==1)
	if (ring == 2)
	  flagSS_EXP2 = 1;
	else
	  flagSS_EXP2 = 0;
	end
   end

a=a+1;
figure(a)
name = ['Fig', num2str(a)];

hold on; set(0, 'DefaulttextInterpreter', 'none');
title(['Steady State Clad Temp. Ring', int2str(ring), ' COR9: 600mm and COR10: 700mm' ]);
labels = {};

x = EDF_TEMP_CL(:,1); %time
col=1;

	%for level=1:14
	%for ring=1:2
	%for level=5:14
	for level=1:14
	  offset = (ring - 1)*14;
	  y = EDF_TEMP_CL(:,1+ level + offset); 	  
	  plot(x,y,'-','LineWidth',1,'MarkerSize',2,'Color',cmap14(col,:)); %or cmap1
	  
	  if(level<10)
		labels = {labels{:}, ['COR-TCL ', int2str(ring), '0', int2str(level)]};
      else
	    labels = {labels{:}, ['COR-TCL ', int2str(ring), '', int2str(level)]};
	  end
	  
	  col = col+1;
	%end

	end

xlabel('Time, [s]');
ylabel('Temperature, [K]');
legend('Location','northwest');
box on; %ylim([0 inf]);  
grid on; 
xlim([-500, 100]);


if flagSS_EXP2 == 1

	x1 = EXP_T_AVG_OUT_CLAD_600mm(:,1);
	y1 = EXP_T_AVG_OUT_CLAD_600mm(:,2);
	x2 = EXP_T_AVG_OUT_CLAD_700mm(:,1);
	y2 = EXP_T_AVG_OUT_CLAD_700mm(:,2);
	
	
	plot(x1,y1,'ok','LineWidth',1,'MarkerSize',3);
	plot(x2,y2,'or','LineWidth',1,'MarkerSize',3);
	
    labels = {labels{:} ,  'EXP Out. 600mm',  'EXP Out. 700mm' };

else
  labels = {labels{:}};

end	

	
	
%labels = {' '};
legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end

end

% ===================================================================
% FIGURE  - Shroud Temperatures 1
% ===================================================================
% Inside Shroud Zirconia Insulator 200mm - Level 6 COR6
a=a+1;
figure(a)
name = ['Fig', num2str(a)];

hold on; set(0, 'DefaulttextInterpreter', 'none');
title('Shroud Temp. COR4:100mm  COR5:200mm  COR6:300mm');

x = EDF_TEMP_SH(:,1);

labels={};

% ! Node 6 - Zirconia Inner Shroud Inner Node 
y1 = EDF_TEMP_SH(:,2);
y2 = EDF_TEMP_SH(:,3);
y3 = EDF_TEMP_SH(:,4);
plot(x,y1,'-','LineWidth',1,'MarkerSize',2,'Color',cmap14(1,:));
plot(x,y2,'-','LineWidth',1,'MarkerSize',2,'Color',cmap14(2,:));
plot(x,y3,'-','LineWidth',1,'MarkerSize',2,'Color',cmap14(3,:));

label = {'100mm Zirconia Inner' '200mm Zirconia Inner' '300mm Zirconia Inner'};
labels=[labels  label]

%! Node 14 - Zirconia Outer Shroud Inner Node
y1 = EDF_TEMP_SH(:,5);
y2 = EDF_TEMP_SH(:,6);
y3 = EDF_TEMP_SH(:,7);
plot(x,y1,'-','LineWidth',1,'MarkerSize',2,'Color',cmap14(4,:));
plot(x,y2,'--','LineWidth',1,'MarkerSize',2,'Color',cmap14(5,:));
plot(x,y3,'-','LineWidth',1,'MarkerSize',2,'Color',cmap14(6,:));
label = {'100mm Zirconia Outer' '200mm Zirconia Outer' '300mm Zirconia Outer'};
labels=[labels  label]

%! Node 16 - Inconel Inner Node
y1 = EDF_TEMP_SH(:,8);
y2 = EDF_TEMP_SH(:,9);
y3 = EDF_TEMP_SH(:,10);
plot(x,y1,'-','LineWidth',1,'MarkerSize',2,'Color',cmap14(8,:));
plot(x,y2,'-.','LineWidth',1,'MarkerSize',2,'Color',cmap14(9,:));
plot(x,y3,'-','LineWidth',1,'MarkerSize',2,'Color',cmap14(10,:));
label = {'100mm Inconel Inner' '200mm Inconel Inner' '300mm Inconel Inner'}
labels=[labels  label]


% Experimental  Inner Shroud Temperature at 200mm and 300mm - inside shroud zirconia insulator
% For MELCOR MANUAL VOL3 it was HS node 14

if flagSS_EXP == 1
	x1 = EXP_T_ZR_SH_INS_200mm(:,1);
	y1 = EXP_T_ZR_SH_INS_200mm(:,2);
	plot(x1,y1,'ok','LineWidth',1,'MarkerSize',3)
    labels = {labels{:} ,  'EXP 200mm' };
	

else

 labels = {labels{:}  };
 
end


xlabel('Time, [s]');
ylabel('Temperature, [K]');
legend('Location','northwest');
box on; %ylim([0 inf]);  
grid on; xlim([-500, 100]);


legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end

% ===================================================================
% FIGURE  - Shroud Temperatures 600mm
% ===================================================================
% Inside Shroud Zirconia Insulator 600mm - Level 9 COR 9
a=a+1;
figure(a)
name = ['Fig', num2str(a)];

hold on; set(0, 'DefaulttextInterpreter', 'none');
title('Shroud Temp. COR8: 500mm COR9: 600mm COR10: 700mm');

x = EDF_TEMP_SH(:,1);

labels={};

% ! Node 6 - Zirconia Inner Shroud Inner Node 
y1 = EDF_TEMP_SH(:,11);
y2 = EDF_TEMP_SH(:,12);
y3 = EDF_TEMP_SH(:,13);
plot(x,y1,'-','LineWidth',1,'MarkerSize',2,'Color',cmap14(1,:));
plot(x,y2,'-','LineWidth',1,'MarkerSize',2,'Color',cmap14(2,:));
plot(x,y3,'-','LineWidth',1,'MarkerSize',2,'Color',cmap14(3,:));

label = {'500mm Zirconia Inner' '600mm Zirconia Inner' '700mm Zirconia Inner'};
labels=[labels  label];

%! Node 14 - Zirconia Outer Shroud Inner Node
y1 = EDF_TEMP_SH(:,14);
y2 = EDF_TEMP_SH(:,15);
y3 = EDF_TEMP_SH(:,16);
plot(x,y1,'-','LineWidth',1,'MarkerSize',2,'Color',cmap14(4,:));
plot(x,y2,'--','LineWidth',1,'MarkerSize',2,'Color',cmap14(5,:));
plot(x,y3,'-','LineWidth',1,'MarkerSize',2,'Color',cmap14(6,:));
label = {'500mm Zirconia Outer' '600mm Zirconia Outer' '700mm Zirconia Outer'};
labels=[labels  label];

%! Node 16 - Inconel Inner Node
y1 = EDF_TEMP_SH(:,17);
y2 = EDF_TEMP_SH(:,18);
y3 = EDF_TEMP_SH(:,19);
plot(x,y1,'-','LineWidth',1,'MarkerSize',2,'Color',cmap14(8,:));
plot(x,y2,'-.','LineWidth',1,'MarkerSize',2,'Color',cmap14(9,:));
plot(x,y3,'-','LineWidth',1,'MarkerSize',2,'Color',cmap14(10,:));
label = {'500mm Inconel Inner' '600mm Inconel Inner' '700mm Inconel Inner'}
labels=[labels  label];

% Experimental  Inner Shroud Temperature at 600mm - inside shroud zirconia insulator
% For MELCOR MANUAL VOL3 it was HS node 14

if flagSS_EXP == 1
	x1 = EXP_T_ZR_SH_INS_600mm(:,1);
	y1 = EXP_T_ZR_SH_INS_600mm(:,2);
	plot(x1,y1,'ok','LineWidth',1,'MarkerSize',3)
    labels = {labels{:} ,  'EXP 600mm' };
else
 labels = {labels{:} };

end


xlabel('Time, [s]');
ylabel('Temperature, [K]');
legend('Location','northwest');
box on; %ylim([0 inf]);  
grid on; xlim([-500, 100]); 


legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end


% ===================================================================
% FIGURE  - Containment Temperatures
% ===================================================================

% ===================================================================
% FIGURE 
% ===================================================================
a=a+1;

figure(a)
hold on; set(0, 'DefaulttextInterpreter', 'none');
title('Steady State Containment Temperatures');
name = ['Fig', num2str(a)];

x  = EDF_TEMP_HS(:,1);
y1 = EDF_TEMP_HS(:,2);
y2 = EDF_TEMP_HS(:,3); 
y3 = EDF_TEMP_HS(:,4); 
y4 = EDF_TEMP_HS(:,5); 
y5 = EDF_TEMP_HS(:,6);
y6 = EDF_TEMP_HS(:,7); 
y7 = EDF_TEMP_HS(:,8); 
y8 = EDF_TEMP_HS(:,9); 
y9 = EDF_TEMP_HS(:,10); 
y10 = EDF_TEMP_HS(:,11); 

k=1;
plot(x,y1,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1;
plot(x,y2,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1;
plot(x,y3,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1;
plot(x,y4,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1;
plot(x,y5,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1;
plot(x,y6,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1;
plot(x,y7,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1;
plot(x,y8,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1;
plot(x,y9,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1;
plot(x,y10,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1;


xlabel('Time, [s]');
ylabel('Temperature, [K]');
legend('Location','southwest');
grid on; xlim([-500, 100.0]); 
box on;  %ylim([0 1000.0]);

%labels = { 'CONT' 'CORE' 'UP1' 'UP2' 'RAISER-BEND' 'HL1' 'SG-IN'  'SG-U1'  'SG-U2'  'SG-U3' 'SG-U4' 'SG-D1' 'SG-CL1' 'CL2-CONT' 'SUMP LIQUID'};
labels = {'CL2_CONT_IN', 'CONT_IN','CONT_SUMP_HEMI_B', 'CONT_SUMP_WALL', 'CONT_HEMI_B', 'CONT_WALL_OUT', 'COND_DRY', 'COND_WET', 'CONT_HEMI_T', 'CONT_TOP'  };
%labels = { 'CONT' 'CORE' 'UP1' 'UP2' 'RAISER-BEND' 'HL1' 'SG-IN'  'SG-U1'  'SG-U2'  'SG-U3' 'SG-U4' 'SG-D1' 'SG-CL1' 'CL2-CONT'};
legend(labels,'Interpreter','none'); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end













