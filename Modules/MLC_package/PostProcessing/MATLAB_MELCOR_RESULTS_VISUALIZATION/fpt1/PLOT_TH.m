% labels={};
% labels = [labels, 'Fission Power'];
%
% ===================================================================
% FIGURE - Generated Hydrogen 
% ===================================================================
run standard_plot1.m;
title('Hydrogen Generation');

if k_num == 1
	x1 = EXP_H2_TOT(:,1);
	y1 = EXP_H2_TOT(:,2);
	plot(x1,y1,'ko','LineWidth',2,'MarkerSize',3);  labels = [labels, 'EXP ISP46' ];
	
	x1a = EXP_H2_TOT_SNL2(:,1);
	y1a = EXP_H2_TOT_SNL2(:,2);
	plot(x1a,y1a,'rs','LineWidth',2,'MarkerSize',3);  labels = [labels, 'EXP SNL#2' ];
	
end

if k_num == 1
	if flagSNLISP == 1 
		x5 = SNL_ISP46_H2TOT(:,1);
		y5 = SNL_ISP46_H2TOT(:,2);
		%plot(x5,y5,['-'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL M1.85 ISP46' ];
		plot(x5,y5,['--'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#1 M1.85' ];
	end
	
	if  flagSNL2 == 1
		x6 = SNL2_5_H2(:,1);
		y6 = SNL2_5_H2(:,2);
		plot(x6,y6,['--'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#2 M2.1' ];
	end
	if  flagFPT21 == 1
		% Additional  FPT21 Article
		x3 = FPT21_Fig11_H2(:,1);
		y3 = FPT21_Fig11_H2(:,2);
		%plot(x3,y3,['-'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL Wang' ];
		plot(x3,y3,['--'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#4 M2.1' ];
	end
	if flagSNLQL == 1
	% Additional  FPT21 Article
		x4 = SNL_QL1_M22_H2(:,1);
		y4 = SNL_QL1_M22_H2(:,2)./1000;
		%plot(x4,y4,['-'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL M2.2.10966' ];
		plot(x4,y4,['--'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#5 M2.2' ];
	end
end


	x2 = EDF_H2(:,1);
	y2 = EDF_H2(:,3);
	plot(x2,y2,['-'],'LineWidth',1,'MarkerSize',2);  labels = [labels, ['WUT ' case1_name] ];

run standard_plot2.m

% ===================================================================
% FIGURE - Generated Hydrogen Rate
% ===================================================================
run standard_plot1.m
title('Hydrogen Generation Rate');
if k_num == 1
	x1 = EXP_H2_RATE(:,1);
	y1 = EXP_H2_RATE(:,2);
	plot(x1,y1,'ko','LineWidth',2,'MarkerSize',3); labels = [labels, 'EXP ISP46'];
	
		x1a = EXP_H2_RATE_SNL2(:,1);
	y1a = EXP_H2_RATE_SNL2(:,2);
	plot(x1a,y1a,'rs','LineWidth',2,'MarkerSize',3);  labels = [labels, 'EXP SNL#2' ];
end

if k_num == 1
	if  flagSNL2 == 1
		x6 = SNL2_4_H2RATE(:,1);
		y6 = SNL2_4_H2RATE(:,2);
		plot(x6,y6,['--'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#2 M2.1' ];
	end
	if  flagFPT22 == 1
		x6 = FPT22_Fig14_H2R(:,1);
		y6 = FPT22_Fig14_H2R(:,2);
		%plot(x6,y6,['-'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL Wang' ];
		plot(x6,y6,['--'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#4 M2.1' ];
	end
		
	if flagSNLQL == 1
	% SNL Raport
		x4 = SNL_QL9_M186_H2(:,1);
		y4 = SNL_QL9_M186_H2(:,2)./1000;
		%plot(x4,y4,['-'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL QL M186' ];
		plot(x4,y4,['--'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#5 M1.86' ];
		x5 = SNL_QL9_M22_H2(:,1);
		y5 = SNL_QL9_M22_H2(:,2)./1000;
		%plot(x5,y5,['-'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL QL M2.2.9496' ];
		plot(x5,y5,['--'],'LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#5 M2.2.9496' ];
	end

end

x2 = EDF_H2(:,1);
y2 = EDF_H2(:,2);
plot(x2,y2,'-','LineWidth',1,'MarkerSize',2);  labels = [labels, ['WUT ' case1_name] ];

ylabel('Mass rate, [kg/s]'); 
run standard_plot2.m
legend('Location','northeast');

% ===================================================================
% FIGURE  - Core Inlet/Outlet Temperature
% ===================================================================
run standard_plot1.m
title('Core Inlet/Outlet Temperature');
labels1 = { 'EXP Inlet' 'EXP Outlet' 'Inlet' 'Core'  'Outlet-UP1'  };

if k_num == 1
	x1 = EXP_TVAP_CORE_INLET(:,1);
	y1 = EXP_TVAP_CORE_INLET(:,2);
	x2 = EXP_TVAP_CORE_EXIT(:,1);
	y2 = EXP_TVAP_CORE_EXIT(:,2);
	plot(x1,y1,'ok','LineWidth',1,'MarkerSize',2); labels = [labels, labels1{1}];
	plot(x2,y2,'<k','LineWidth',1,'MarkerSize',2); labels = [labels, labels1{2}];
end

if k_num == 1
	if flagFPT21 == 1
		xxx1 = FPT21_Fig13_TempDome(:,1);
		yyy1 = FPT21_Fig13_TempDome(:,2);
		%plot(xxx1,yyy1,'--r','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL Wang'];
		plot(xxx1,yyy1,'--r','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#4'];
	end
end

x   = EDF_TVAP(:,1);
y10 = EDF_TVAP(:,2); %Inlet
y11 = EDF_TVAP(:,3); %Core
y12 = EDF_TVAP(:,4); %Up1
plot(x,y10,'-.','LineWidth',1,'MarkerSize',2); labels = [labels, ['WUT ',case1_name,' ',labels1{3}]];
plot(x,y11,'--','LineWidth',1,'MarkerSize',2); labels = [labels, ['WUT ',case1_name,' ',labels1{4}]];
plot(x,y12,'-','LineWidth',1,'MarkerSize',2);  labels = [labels, ['WUT ',case1_name,' ',labels1{5}]];



ylabel('Temperature, [K]');
run standard_plot2.m
%ylim([0 inf]);


% ===================================================================
% TEMPERATURES
% ===================================================================
if flag_OLD_TEMPS == 0
% ===================================================================
% ===================================================================
% ===================================================================
% FIGURE - Fuel Temperatures
% ===================================================================
	for ring=2:2
		run standard_plot1.m
		title(['Fuel Temp. Ring', int2str(ring), ' COR6: 300mm and COR7: 400mm ' ]);
		%labels = {};
		% 300mm for MELCOR it is Level 6   % 400mm for MELCOR it is Level 7
		if k_num == 1
			if ring == 2
				x1 = EXP_T_AVG_OUT_FUEL_300mm(:,1); y1 = EXP_T_AVG_OUT_FUEL_300mm(:,2);
				x2 = EXP_T_AVG_OUT_FUEL_400mm(:,1); y2 = EXP_T_AVG_OUT_FUEL_400mm(:,2);
			%plot(x1,y1,'ok','LineWidth',1,'MarkerSize',3); %plot(x2,y2,'or','LineWidth',1,'MarkerSize',3);
			%plot(x1,y1,'o','Color',cmapT(1),'LineWidth',1,'MarkerSize',3); %plot(x2,y2,'o','Color',cmapT(2),'LineWidth',1,'MarkerSize',3);
				plot(x1,y1,'ok','LineWidth',1,'MarkerSize',3);  labels = [labels, 'EXP Out. 300mm'];
				plot(x2,y2,'or','LineWidth',1,'MarkerSize',3); labels = [labels, 'EXP Out. 400mm'  ];
				%labels = {labels{:} ,  'EXP Out. 300mm',  'EXP Out. 400mm' };
			else
				%labels = {labels{:}};
			end
		end
		
				% 300 mm to COR 0214 0215
		%  400 mm to  COR 0216 0217		
		if k_num == 1
			if flagFPT21 == 1
				xxx1 = FPT21_Fig16a_FuelTemp300mm(:,1);
				yyy1 = FPT21_Fig16a_FuelTemp300mm(:,2);
				%plot(xxx1,yyy1,'--k','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL Wang 300mm'];
				plot(xxx1,yyy1,'--k','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#4 300mm'];
				xxx2 = FPT21_Fig16b_FuelTemp400mm(:,1);
				yyy2 = FPT21_Fig16b_FuelTemp400mm(:,2);
				%plot(xxx2,yyy2,'--r','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL Wang 400mm'];
				plot(xxx2,yyy2,'--r','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#4 400mm'];
			end
		end
		

		x = EDF_TEMP_FU(:,1); %time

		col=1; 
		for level=5:8
			offset = (ring - 1)*14;
			y = EDF_TEMP_FU(:,1+ level + offset); 	  
			plot(x,y,[smap{k_num}],'LineWidth',1,'MarkerSize',2,'Color',cmapT(col)); %or cmap1
			if(level<10)
				%labels = {labels{:}, ['MLC TFU ', int2str(ring), '0', int2str(level),' ', case1_name]};
				labels = {labels{:}, ['WUT ',case1_name, ' TFU', int2str(ring), '0', int2str(level)]};
			else
				%labels = {labels{:}, ['MLC TFU ', int2str(ring), '', int2str(level),' ', case1_name]};
				labels = {labels{:}, ['WUT ',case1_name, ' TFU', int2str(ring), '', int2str(level)]};
			end
			col = col+1;
		end
		

		
		%ylim([0 inf]);
		ylabel('Temperature, [K]');
		run standard_plot2.m
	end


% ===================================================================
% FIGURE - Clad Temperatures
% ===================================================================
	for ring=2:2
		run standard_plot1.m
		title(['Clad Temp. Ring', int2str(ring), ' COR9: 600mm and COR10: 700mm' ]);

		if k_num == 1
			if ring ==2
				x1 = EXP_T_AVG_OUT_CLAD_600mm(:,1); y1 = EXP_T_AVG_OUT_CLAD_600mm(:,2);
				x2 = EXP_T_AVG_OUT_CLAD_700mm(:,1); y2 = EXP_T_AVG_OUT_CLAD_700mm(:,2);
				plot(x1,y1,'ok','LineWidth',1,'MarkerSize',3); labels = [labels, 'EXP Out. 600mm' ];
				plot(x2,y2,'or','LineWidth',1,'MarkerSize',3); labels = [labels,  'EXP Out. 700mm'  ];
				
			
				%labels = {labels{:} ,  'EXP Out. 600mm',  'EXP Out. 700mm' };
			else
				%labels = {labels{:}};
			end	
		end
		
		if k_num == 1
			if flagSNL2 == 1
				xxx2 = SNL2_3_TCL600mm(:,1);
				yyy2 = SNL2_3_TCL600mm(:,2);
				plot(xxx2,yyy2,'--b','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#2 COR220 ~600mm'];
			end
		end
		if k_num == 1
			if flagFPT21 == 1
				xxx1 = FPT21_Fig15d_CladTemp0220(:,1);
				yyy1 = FPT21_Fig15d_CladTemp0220(:,2);
				%plot(xxx1,yyy1,'--k','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL Wang COR220 ~600m'];
				plot(xxx1,yyy1,'--k','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#4 COR220 ~600mm'];
				xxx2 = FPT21_Fig15d_CladTemp0223(:,1);
				yyy2 = FPT21_Fig15d_CladTemp0223(:,2);
				%plot(xxx2,yyy2,'--r','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL Wang COR223 ~700m'];
				plot(xxx2,yyy2,'--r','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#4 COR223 ~700mm'];
			end
		end


		x = EDF_TEMP_CL(:,1); %time

		col=1;
		for level=8:11
			offset = (ring - 1)*14;
			y = EDF_TEMP_CL(:,1+ level + offset); 	  
			plot(x,y,[smap{k_num}],'LineWidth',1,'MarkerSize',2,'Color',cmapT(col)); %or cmap1
			if(level<10)
				%labels = {labels{:}, ['MLC TCL ', int2str(ring), '0', int2str(level),' ', case1_name]};
				labels = {labels{:}, ['WUT ',case1_name,' TCL', int2str(ring), '0', int2str(level)]};
			else
				%labels = {labels{:}, ['MLC TCL ', int2str(ring), '', int2str(level),' ', case1_name]};
				labels = {labels{:}, ['WUT ',case1_name,' TCL', int2str(ring), '', int2str(level)]};
			end
			col = col+1;
		end
		
		% 300 mm to COR 0214 0215
		%  400 mm to  COR 0216 0217
		% - 7.95 to 0mm
		
		% 600mm  -7.35m  0221+0220   -> jest 0220 FPT21_Fig15d_CladTemp0220
		% 700mm -7.25 m  0223+0224   -> jest 0223 FPT21_Fig15d_CladTemp0223
				

		
		ylabel('Temperature, [K]');
		%ylim([0 inf]);
		run standard_plot2.m
		end

% ===================================================================
% FIGURE  - Shroud Temperatures 1
% ===================================================================
	% Inside Shroud Zirconia Insulator 200mm - Level 6 COR6
 	run standard_plot1.m		
	title('Inner Zirc. Shroud Temp. COR4:100mm  COR5:200mm  COR6:300mm');
	%	labels={};
	% Experimental  Inner Shroud Temperature at 200mm and 300mm - inside shroud zirconia insulator
	% For MELCOR MANUAL VOL3 it was HS node 14

	if k_num == 1
	x1 = EXP_T_ZR_SH_INS_200mm(:,1); y1 = EXP_T_ZR_SH_INS_200mm(:,2);
	plot(x1,y1,'ok','LineWidth',1,'MarkerSize',3)
	labels = {labels{:} ,  'EXP 200mm' };
	end
	
	if k_num == 1
			if flagSNL2 == 1
				xxx2 = SNL2_1_200mm(:,1);
				yyy2 = SNL2_1_200mm(:,2);
				plot(xxx2,yyy2,'-k','LineWidth',1.5,'MarkerSize',2); labels = [labels, 'SNL#2 200mm'];
			end
		end
	

	x = EDF_TEMP_SH(:,1);

	% ! Node 10 - Zirconia Inner Shroud Inner Node 
	y1 = EDF_TEMP_SH(:,2);
	y2 = EDF_TEMP_SH(:,3);
	y3 = EDF_TEMP_SH(:,4);
	plot(x,y1,'--','LineWidth',1,'MarkerSize',2,'Color',cmap11(1,:)); 
	plot(x,y2,'--','LineWidth',1,'MarkerSize',2,'Color',cmap11(2,:));
	plot(x,y3,'--','LineWidth',1,'MarkerSize',2,'Color',cmap11(3,:));

	label = {'100mm Node 10' '200mm Node 10' '300mm Node 10'};
	labels=[labels  label];

	%! Node 11 - Zirconia Outer Shroud Inner Node
	y1 = EDF_TEMP_SH(:,5);
	y2 = EDF_TEMP_SH(:,6);
	y3 = EDF_TEMP_SH(:,7);
	plot(x,y1,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(4,:));
	plot(x,y2,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(5,:));
	plot(x,y3,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(6,:));
	label = {'100mm Node 11' '200mm Node 11' '300mm Node 11'};
	labels=[labels  label];

	%! Node 12 - Inconel Inner Node
	y1 = EDF_TEMP_SH(:,8);
	y2 = EDF_TEMP_SH(:,9);
	y3 = EDF_TEMP_SH(:,10);
	plot(x,y1,'-.','LineWidth',1,'MarkerSize',2,'Color',cmap11(8,:));
	plot(x,y2,'-.','LineWidth',1,'MarkerSize',2,'Color',cmap11(9,:));
	plot(x,y3,'-.','LineWidth',1,'MarkerSize',2,'Color',cmap11(10,:));
	label = {'100mm Node 12' '200mm Node 12' '300mm Node 12'}
	labels=[labels  label];
		
	ylabel('Temperature, [K]');
	run standard_plot2.m

% ===================================================================
% FIGURE  - Shroud Temperatures 600mm
% ===================================================================
	% Inside Shroud Zirconia Insulator 600mm - Level 9 COR 9
	run standard_plot1.m
	title('Shroud Temp. COR8: 500mm COR9: 600mm COR10: 700mm');
%	labels={};
	% Experimental  Inner Shroud Temperature at 600mm - inside shroud zirconia insulator
	% For MELCOR MANUAL VOL3 it was HS node 14
	if k_num == 1
		x1 = EXP_T_ZR_SH_INS_600mm(:,1); y1 = EXP_T_ZR_SH_INS_600mm(:,2);
		plot(x1,y1,'ok','LineWidth',1,'MarkerSize',3)
		labels = {labels{:} ,  'EXP 600mm' };
	end
	x = EDF_TEMP_SH(:,1);

	% ! Node 10 - Zirconia Inner Shroud Inner Node 
	y1 = EDF_TEMP_SH(:,11);
	y2 = EDF_TEMP_SH(:,12);
	y3 = EDF_TEMP_SH(:,13);
	plot(x,y1,'--','LineWidth',1,'MarkerSize',2,'Color',cmap11(1,:));
	plot(x,y2,'--','LineWidth',1,'MarkerSize',2,'Color',cmap11(2,:));
	plot(x,y3,'--','LineWidth',1,'MarkerSize',2,'Color',cmap11(3,:));

	label = {'500mm Node 10' '600mm Node 10' '700mm Node 10'};
	labels=[labels  label];

	%! Node 11 - Zirconia Outer Shroud Inner Node
	y1 = EDF_TEMP_SH(:,14);
	y2 = EDF_TEMP_SH(:,15);
	y3 = EDF_TEMP_SH(:,16);
	plot(x,y1,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(4,:));
	plot(x,y2,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(5,:));
	plot(x,y3,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(6,:));
	label = {'500mm Node 11' '600mm Node 11' '700mm Node 11'};
	labels=[labels  label];

	%! Node 12 - Inconel Inner Node
	y1 = EDF_TEMP_SH(:,17);
	y2 = EDF_TEMP_SH(:,18);
	y3 = EDF_TEMP_SH(:,19);
	plot(x,y1,'-.','LineWidth',1,'MarkerSize',2,'Color',cmap11(8,:));
	plot(x,y2,'-.','LineWidth',1,'MarkerSize',2,'Color',cmap11(9,:));
	plot(x,y3,'-.','LineWidth',1,'MarkerSize',2,'Color',cmap11(10,:));
	label = {'500mm Node 12' '600mm Node 12' '700mm Node 12'}
	labels=[labels  label];
	
	ylabel('Temperature, [K]');
	run standard_plot2.m



% ===================================================================
% FIGURE  Core Energy Balance - Power Rate
% ===================================================================
	run standard_plot1.m
	title('Energy Balance - Power');
	%labels={};

	if flagSNLISP == 1
		if k_num == 1
		    %labels = {};
			x1 = EXP_POWER_NUCL(:,1);
			y1 = EXP_POWER_NUCL(:,3);
			x2 = EXP_POWER_OXDN(:,1);
			y2 = EXP_POWER_OXDN(:,3);
			x3 = EXP_POWER_CONV(:,1);
			y3 = EXP_POWER_CONV(:,3);
			x4 = EXP_POWER_SHRD(:,1);
			y4 = EXP_POWER_SHRD(:,3);
			x5 = EXP_POWER_STDHT(:,1);
			y5 = EXP_POWER_STDHT(:,3);
	
			plot(x1,y1,'--k','LineWidth',1,'MarkerSize',2);   labels = [labels, 'SNL#1 NUCL'];
			plot(x2,y2,'--g','LineWidth',1,'MarkerSize',2);   labels = [labels,  'SNL#1 OXDN'];
			plot(x3,y3,'--b','LineWidth',1,'MarkerSize',2);	  labels = [labels, 'SNL#1 CONV'];
			plot(x4,y4,'--r','LineWidth',1,'MarkerSize',2);   labels = [labels, 'SNL#1 SHRD'];
			plot(x5,y5,'--y','LineWidth',1,'MarkerSize',4,'Color',[1.00,0.41,0.16]); labels = [labels, 'SNL#1 STDHT'];
	
			%labels1 = {'SNL#1 NUCLEAR', 'SNL#1 OXDN',   ...
			%'SNL#1 CONV', 'SNL#1 SHRD', 'SNL#1 STDHT'};		
		end
	end
	
	x = EDF_POWER(:,1);
	y1 = EDF_POWER(:,8); %EFPD
	y2 = EDF_POWER(:,9); %EMWR
	y3 = EDF_POWER(:,10); %ECNV
	y4 = EDF_POWER(:,11); %EBND
	%y5 = EDF_POWER(:,13); %DCH-TOTCLSPOW
	%y6 = EDF_POWER(:,14); %DCH-COREPOW
	y7 = y1 + y2 - y3 - y4; %Balance

	plot(x,y1,'-k','LineWidth',1,'MarkerSize',2); labels = [labels, [case1_name,' EFPD']];
	plot(x,y2,'-g','LineWidth',1,'MarkerSize',2); labels = [labels, [case1_name,' EMWR']];
	plot(x,y3,'-b','LineWidth',1,'MarkerSize',2); labels = [labels, [case1_name,' ECNV']];
	plot(x,y4,'-r','LineWidth',1,'MarkerSize',2); labels = [labels, [case1_name,' EBND']];
	plot(x,y7,'-','LineWidth',1,'MarkerSize',4,'Color',[1.00,0.41,0.16]); labels = [labels, [case1_name,' BALANCE']];
	%plot(x,y5,'-.c','LineWidth',1,'MarkerSize',4);
	%plot(x,y6,'-.m','LineWidth',1,'MarkerSize',4);
	
	%labels = {'EFPD - Fission Power', 'EMWR - Oxidation Power',   ...
	%           'ECNV - Convective Removal', 'EBND - Conduction Removal' };
	%labels = {'COR-EFPD', 'COR-EMWR',   ...
	%		'COR-ECNV', 'COR-EBND' ...
	%		'BALANCE'};
	
	%'DCH-TOTCLSPOW', 'DCH-COREPOW', 
	%'EFPD+EMWR-EBND-ECNV'
	% labels = {'COR-EFPD - Fission Power', 'COR-EMWR - Oxidation Power',   ...
	%           'COR-ECNV - Convective Removal', 'COR-EBND - Conduction Removal' ...
	% 'DCH-TOTCLSPOW Decay Heat All RNs', 'DCH-COREPOW Total Decay Heat'};
	%labels1 = {''};
	
	
	ylim([0 40000])

			%labels = [labels{:} , labels1];
	ylabel('Power, [W]');
	run standard_plot2.m
	
% ===================================================================
% FIGURE  Core Energy Balance - Integrated
% ===================================================================
if flagIntegralE == 1
	run standard_plot1.m
	title('Energy Balance - Cumulative');
	%
% FOR OUR RESULTS
	if k_num == 1
	    %labels = {};
		x = EDF_POWER(:,1).*(x>0);
		y1 = EDF_POWER(:,8).*(x>0); %EFPD
		y2 = EDF_POWER(:,9).*(x>0); %EMWR
		y3 = EDF_POWER(:,10).*(x>0); %ECNV
		y4 = EDF_POWER(:,11).*(x>1); %EBND
		%y5 = EDF_POWER(:,13); %DCH-TOTCLSPOW
		%y6 = EDF_POWER(:,14); %DCH-COREPOW
		y5 = y1 + y2 - y3 - y4; %Balance

		%yI1 =[0]; yI2 =[0]; yI3 =[0]; yI4 =[0]; yI5 =[0];
		yI1 =[]; yI2 =[]; yI3 =[]; yI4 =[]; yI5 =[];

		for i=1:numel(x)
			dum =  trapz([0; x(1:i)],[0; y1(1:i)]) ;
			yI1 = [yI1;	dum];
		end	
		for i=1:numel(x)
			dum = trapz([0; x(1:i)],[0; y2(1:i)]);
			yI2 = [yI2; dum 	];
		end	
		for i=1:numel(x)
			dum = trapz([0; x(1:i)],[0; y3(1:i)]);
			yI3 = [yI3; dum 	];
		end	
		for i=1:numel(x)
			dum = trapz([0; x(1:i)],[0; y4(1:i)]);
			yI4 = [yI4;  dum	];
		end	
		for i=1:numel(x)
			dum = trapz([0; x(1:i)],[0; y5(1:i)]);
			yI5 = [yI5;  dum	];
		end	

		plot(x,yI1,'-k','LineWidth',1,'MarkerSize',2); labels = [labels, 'WUT EFPD' ];
		plot(x,yI2,'-g','LineWidth',1,'MarkerSize',2); labels = [labels, 'WUT EMWR' ];
		plot(x,yI3,'-b','LineWidth',1,'MarkerSize',2); labels = [labels, 'WUT ECNV' ];
		plot(x,yI4,'-r','LineWidth',1,'MarkerSize',2); labels = [labels, 'WUT EBND' ];
		plot(x,yI5,'-','LineWidth',1,'MarkerSize',4,'Color',[1.00,0.41,0.16]); labels = [labels, 'WUT BALANCE' ];
		
		%labels = [labels, 'EXP' ];
		%labels = {'EFPD', 'EMWR',   ...
		%		'ECNV', 'EBND', ...
		%'BALANCE'};
	end

% FOR MELCOR SNL RESULTS
% labels1 ={};
	if k_num == 1
		
		if flagSNLISP == 1
			%yI1 =[]; yI2 =[]; yI3 =[]; yI4 =[]; yI5 =[];
			yI1 =[0]; yI2 =[0]; yI3 =[0]; yI4 =[0]; yI5 =[0];
	 
			%x1  = EDF_POWER(:,1).*(x>0);
			x1 = linspace(0,20000,1000)';

			x11 = EXP_POWER_NUCL(:,1);
			y11 = EXP_POWER_NUCL(:,3);
			y1  = interp1(x11,y11,x1,'Linear','extrap');
			
			x22 = EXP_POWER_OXDN(:,1);
			y22 = EXP_POWER_OXDN(:,3);
			y2  = interp1(x22,y22,x1,'Linear','extrap');

			x33 = EXP_POWER_CONV(:,1);
			y33 = EXP_POWER_CONV(:,3);
			y3  = interp1(x33,y33,x1,'Linear','extrap');

			x44 = EXP_POWER_SHRD(:,1);
			y44 = EXP_POWER_SHRD(:,3);
			y4  = interp1(x44,y44,x1,'Linear','extrap');

			x55 = EXP_POWER_STDHT(:,1);
			x55(38) = 11189; %unique points
			y55 = EXP_POWER_STDHT(:,3);
			%y5 = y1 + y2 - y3 - y4; 
		
			y5  = interp1(x55,y55,x1,'Linear','extrap');


			for i=1:numel(x1)
				dum =  trapz([0; x1(1:i)],[0; y1(1:i)]) ;
				yI1 = [yI1;	dum];
			end	
			for i=1:numel(x1)
				dum = trapz([0; x1(1:i)],[0; y2(1:i)]);
				yI2 = [yI2; dum 	];
			end	
			for i=1:numel(x1)
				dum = trapz([0; x1(1:i)],[0; y3(1:i)]);
				yI3 = [yI3; dum 	];
			end	
			for i=1:numel(x1)
				dum = trapz([0; x1(1:i)],[0; y4(1:i)]);
				yI4 = [yI4;  dum	];
			end	
			for i=1:numel(x1)
				dum = trapz([0; x1(1:i)],[0; y5(1:i)]);
				yI5 = [yI5;  dum	];
			end	
			
			%plot([0; x1], yI1','--k','LineWidth',1,'MarkerSize',2);  labels = [labels, 'SNL ISP46 NUCLEAR' ];
			%plot([0; x1], yI2','--g','LineWidth',1,'MarkerSize',2);  labels = [labels, 'SNL ISP46 OXDN' ];
			%plot([0; x1], yI3','--b','LineWidth',1,'MarkerSize',2);  labels = [labels, 'SNL ISP46 CONV' ];
			%plot([0; x1], yI4','--r','LineWidth',1,'MarkerSize',2);  labels = [labels, 'SNL ISP46 SHRD' ];
			%plot([0; x1], yI5','--','LineWidth',1,'MarkerSize',4,'Color',[1.00,0.41,0.16]);  labels = [labels, 'SNL ISP46 STDHT' ];

			plot([0; x1], yI1','--k','LineWidth',1,'MarkerSize',2);  labels = [labels, 'SNL#1 NUCL' ];
			plot([0; x1], yI2','--g','LineWidth',1,'MarkerSize',2);  labels = [labels, 'SNL#1 OXDN' ];%labels1 = {'SNL ISP46 NUCLEAR', 'SNL ISP46 OXDN',   ...
			plot([0; x1], yI3','--b','LineWidth',1,'MarkerSize',2);  labels = [labels, 'SNL#1 CONV' ];%'SNL ISP46 CONV', 'SNL ISP46 SHRD', 'SNL ISP46 STDHT'};
			plot([0; x1], yI4','--r','LineWidth',1,'MarkerSize',2);  labels = [labels, 'SNL#1 SHRD' ];
			plot([0; x1], yI5','--','LineWidth',1,'MarkerSize',4,'Color',[1.00,0.41,0.16]);  labels = [labels, 'SNL#1 STDHT' ];
		end
	end
	if k_num == 1
		if flagFPT21 == 1
			xxx1 = FPT21_Fig8a_FisDecEnergy(:,1);
			yyy1 = FPT21_Fig8a_FisDecEnergy(:,2);			

			xxx2 = FPT21_Fig8a_HSEnergy(:,1);
			yyy2 = FPT21_Fig8a_HSEnergy(:,2);			

			xxx3 = FPT21_Fig8a_OxEnergy(:,1);
			yyy3 = FPT21_Fig8a_OxEnergy(:,2);			

			xxx4 = FPT21_Fig8a_ConvEnergy(:,1);
			yyy4 = FPT21_Fig8a_ConvEnergy(:,2);			
			
			%plot(xxx1,yyy1,'-.k','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL Wang EFPD' ];
			%plot(xxx2,yyy2,'-.r','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL Wang EBND' ];
			%plot(xxx3,yyy3,'-.g','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL Wang EMWR' ];
			%plot(xxx4,yyy4,'-.b','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL Wang ECNV' ];
			plot(xxx1,yyy1,'-.k','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#4 EFPD' ];
			plot(xxx2,yyy2,'-.r','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#4 EBND' ];
			plot(xxx3,yyy3,'-.g','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#4 EMWR' ];
			plot(xxx4,yyy4,'-.b','LineWidth',1,'MarkerSize',2); labels = [labels, 'SNL#4 ECNV' ];
			
            %plot(x,y10,'-.','LineWidth',1,'MarkerSize',2); labels = [labels, ['MLC ',case1_name,' ',labels1{3}]];
			
		end
	end
	
	%labels = [labels{:} , labels1];
%	labels = [labels{:}];
	ylabel('Energy, [J]');
	ylim([1e4 3e8]);
	legend('Location','southeast');
	set(gca, 'YScale', 'log');

	run standard_plot2.m
end
% ===================================================================


if flagCORTNS == 1

% ===================================================================
% FIGURE  - CR/GT Temperature
% ===================================================================
% CONTROL RODS TEMPERATURES 500, 600, 700, 800, 1000 mm
% COR LEVELS: 7 //   8, 9, 10, 11, 13, 14 (13,14 average) Ring 1
	run standard_plot1.m
	%title('CR/GT Temperature');
	title('Absorber Temperature');
	
	if k_num == 1
		x1 = EXP_T_CR_1000mm(:,1);  %
		y1 = EXP_T_CR_1000mm(:,2);  %
		
		x2 = EXP_T_CR_700mm(:,1);    %
		y2 = EXP_T_CR_700mm(:,2);   %
		
		x3 = EXP_T_GT_500mm(:,1);    %
		y3 = EXP_T_GT_500mm(:,2);  %
		
		x4 = EXP_T_GT_600mm(:,1);  
		y4 = EXP_T_GT_600mm(:,2); 
		
		x5 = EXP_T_GT_800mm(:,1); 
		y5 = EXP_T_GT_800mm(:,2);
		
		plot(x1,y1,'ok','LineWidth',1,'MarkerSize',3); labels = [labels, 'EXP CR 1000mm'];
		plot(x2,y2,'ob','LineWidth',1,'MarkerSize',3); labels = [labels, 'EXP CR 700mm'];
		%plot(x3,y3,'or','LineWidth',1,'MarkerSize',2); labels = [labels, 'EXP GT 500mm'];
		%plot(x4,y4,'om','LineWidth',1,'MarkerSize',2); labels = [labels, 'EXP GT 600mm'];
		%plot(x5,y5,'oc','LineWidth',1,'MarkerSize',2); labels = [labels, 'EXP GT 800mm'];
		
	end
	x0 = EDF_COR_TNS(:,1);
	
	y02 = EDF_COR_TNS(:,2);  %7 
	y03 = EDF_COR_TNS(:,3);  %8 500mm
	y04 = EDF_COR_TNS(:,4);  %9 600mm
	y05 = EDF_COR_TNS(:,5);  %10 700mm
	y06 = EDF_COR_TNS(:,6);  %11 800mm
	y07 = EDF_COR_TNS(:,7);  %13  
	y08 = EDF_COR_TNS(:,8);  %14
	
	
	%plot(x0,y02,'-','LineWidth',1,'MarkerSize',2,'Color','y'); labels = [labels, 'TNS_107'];
	%plot(x0,y03,'-','LineWidth',1,'MarkerSize',2,'Color','r'); labels = [labels, 'TNS_108 500mm'];
	plot(x0,y04,'-','LineWidth',1,'MarkerSize',2,'Color','m'); labels = [labels, 'TNS_109 600mm'];
	plot(x0,y05,'-','LineWidth',1,'MarkerSize',2,'Color','b'); labels = [labels, 'TNS_110 700mm'];
	plot(x0,y06,'-','LineWidth',1,'MarkerSize',2,'Color','c'); labels = [labels, 'TNS_111 800mm'];
	plot(x0,y07,'-.','LineWidth',1,'MarkerSize',2,'Color','k'); labels = [labels, 'TNS_113 ~975mm'];
	plot(x0,y08,'--','LineWidth',1,'MarkerSize',2,'Color','k'); labels = [labels, 'TNS_114 ~1041mm'];


	
	ylabel('Temperature, [K]');
	run standard_plot2.m
end



if flagCORMASS == 1
% ===================================================================
% FIGURE  - Mass Balance
% ===================================================================
	run standard_plot1.m
	title('Mass Balance');

	
	if k_num == 1
		x1 = FPT21_Fig9a_MassTot(:,1);
		y1 = FPT21_Fig9a_MassTot(:,2);
		x2 = FPT21_Fig9b_MassSS(:,1);
		y2 = FPT21_Fig9b_MassSS(:,2);
		x3 = FPT21_Fig9b_MassSSOX(:,1);
		y3 = FPT21_Fig9b_MassSSOX(:,2);
		x4 = FPT21_Fig9b_MassZR(:,1);
		y4 = FPT21_Fig9b_MassZR(:,2);
		x5 = FPT21_Fig9b_MassZRO2(:,1);
		y5 = FPT21_Fig9b_MassZRO2(:,2);
		
		
		%plot(x1,y1,'--k','LineWidth',1,'MarkerSize',3); labels = [labels, 'SNL#4 Total Mass'];
		plot(x2,y2,'--b','LineWidth',1,'MarkerSize',3); labels = [labels, 'SNL#4 SS Mass'];
		plot(x3,y3,'--c','LineWidth',1,'MarkerSize',3); labels = [labels, 'SNL#4 SSOX Mass'];
		plot(x4,y4,'--r','LineWidth',1,'MarkerSize',3); labels = [labels, 'SNL#4 ZR Mass'];
		plot(x5,y5,'--m','LineWidth',1,'MarkerSize',3); labels = [labels, 'SNL#4 ZRO2 Mass'];
	end
	
	
	x0 = EDF_COR_MASS(:,1);
	y02 = EDF_COR_MASS(:,2);  % COR-MAGINC-TOT      
	y03 = EDF_COR_MASS(:,3);  % COR-MCTOHS-TOT         
	y04 = EDF_COR_MASS(:,4) - 2.176401143 + 0.78442; % - 2.176401143; % - 0.78442;  % COR-MSS-TOT     
	y05 = EDF_COR_MASS(:,5);  % COR-MSSOX-TOT   
	y06 = EDF_COR_MASS(:,6);  % COR-MUO2-INT-TOT         
	y07 = EDF_COR_MASS(:,7);  % COR-MZR-TOT   
	y08 = EDF_COR_MASS(:,8);  % COR-MZRO2-INT-TOT
	y09 = y02 + y03 + y04 + y05 + y06 + y07 + y08;  %Total
	
	
		%plot(x0,y09,'-k','LineWidth',1,'MarkerSize',3); labels = [labels, [case1_name, ' Total Mass']];
		plot(x0,y04,'-b','LineWidth',1,'MarkerSize',3); labels = [labels, [case1_name, ' SS Mass']];
		plot(x0,y05,'-c','LineWidth',1,'MarkerSize',3); labels = [labels, [case1_name, ' SSOX Mass']];
		plot(x0,y07,'-r','LineWidth',1,'MarkerSize',3); labels = [labels, [case1_name, ' ZR Mass']];
		plot(x0,y08,'-m','LineWidth',1,'MarkerSize',3); labels = [labels, [case1_name, ' ZRO2 Mass']];
		%plot(x0,y02,'-y','LineWidth',1,'MarkerSize',3); labels = [labels, [case1_name, ' AIC Mass']];
		%plot(x0,y06,'-g','LineWidth',1,'MarkerSize',3); labels = [labels, [case1_name, ' UO2 Mass']];
	
	
	ylabel('Mass, [kg]');
	run standard_plot2.m
	
	
	
	% ===================================================================
% FIGURE  - Mass Balance Total
% ===================================================================
	run standard_plot1.m
	title('Mass Balance');
	
	y091 = y09; % - 2.176401143;
	plot(x1,y1,'--k','LineWidth',1,'MarkerSize',3); labels = [labels, 'SNL#4 Total Mass'];
	plot(x0,y091,'-k','LineWidth',1,'MarkerSize',3); labels = [labels, [case1_name, ' Total Mass']];
	
		ylabel('Mass, [kg]');
	run standard_plot2.m
end	
% ===================================================================
% ===================================================================
% ===================================================================
% ===================================================================
% ===================================================================
% ===================================================================
% ===================================================================
% ===================================================================
% ===================================================================
% ===================================================================
end
% ===================================================================
% ===================================================================
% ===================================================================
% ===================================================================
% ===================================================================
% ===================================================================
% ===================================================================
% ===================================================================
% ===================================================================
% ===================================================================
%{
if flag_OLD_TEMPS == 1

	% ===================================================================
	% FIGURE - Fuel Temperatures
	% ===================================================================
	for ring=1:2
		a=a+1;
		figure(a)
		name = ['Fig', num2str(a)];

		hold on; set(0, 'DefaulttextInterpreter', 'none');
		title(['Fuel Temperatures Ring', int2str(ring), ' COR6: 300mm and COR7: 400mm ' ]);
		%title('Fuel Temperatures');
		labels = {};

		x = EDF_TEMP_FU(:,1); %time
		col=1;

			%for level=1:14
			%for ring=1:2
		%	for level=4:13 %ALL CORE
		%for level=5:8
		for level=6:7
			offset = (ring - 1)*14;
			y = EDF_TEMP_FU(:,1+ level + offset); 	  
			plot(x,y,'-','LineWidth',1,'MarkerSize',2,'Color',cmap6(col,:)); %or cmap1
			
			if(level<10)
				labels = {labels{:}, ['COR-TFU ', int2str(ring), '0', int2str(level)]};
			else
				labels = {labels{:}, ['COR-TFU ', int2str(ring), '', int2str(level)]};
			end
			
			col = col+1;
			%end

			end

		xlabel('Time, [s]');
		ylabel('Temperature, [K]');
		legend('Location','northeast');
		box on; %ylim([0 inf]);  
		grid on; xlim([0, 30000]);

		%labels = {' '};
		
		% 300mm for MELCOR it is Level 6
		% 400mm for MELCOR it is Level 7
		if ring == 2
			x1 = EXP_T_AVG_OUT_FUEL_300mm(:,1);
			y1 = EXP_T_AVG_OUT_FUEL_300mm(:,2);
			x2 = EXP_T_AVG_OUT_FUEL_400mm(:,1);
			y2 = EXP_T_AVG_OUT_FUEL_400mm(:,2);
			
			plot(x1,y1,'ok','LineWidth',1,'MarkerSize',3);
			plot(x2,y2,'or','LineWidth',1,'MarkerSize',3);
			
			labels = {labels{:} ,  'EXP Out. 300mm',  'EXP Out. 400mm' };
		else
		
			labels = {labels{:}};
		
		end

		legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');
		if (flag2 == 1)
			print('-dtiff',[case1,'/', name, code1, '.tiff'])
			savefig([case1,'/', name, code1, '.fig'])
			%saveas(gcf,[Fig1.fig'],'fig');
		end

	end



	% ===================================================================
	% FIGURE - Clad Temperatures
	% ===================================================================
	for ring=1:2
	a=a+1;
	figure(a)
	name = ['Fig', num2str(a)];

	hold on; set(0, 'DefaulttextInterpreter', 'none');
	title(['Clad Temperatures Ring', int2str(ring), ' COR9: 600mm and COR10: 700mm' ]);
	labels = {};

	x = EDF_TEMP_CL(:,1); %time
	col=1;

		%for level=1:14
		%for ring=1:2
		%for level=5:14
		%for level=8:11
		for level=9:10
		offset = (ring - 1)*14;
		y = EDF_TEMP_CL(:,1+ level + offset); 	  
		plot(x,y,'-','LineWidth',1,'MarkerSize',2,'Color',cmap6(col,:)); %or cmap1
		
		if(level<10)
			labels = {labels{:}, ['COR-TCL ', int2str(ring), '0', int2str(level)]};
		else
			labels = {labels{:}, ['COR-TCL ', int2str(ring), '', int2str(level)]};
		end
		
		col = col+1;
		%end

		end

	xlabel('Time, [s]');
	ylabel('Temperature, [K]');
	legend('Location','northeast');
	box on; %ylim([0 inf]);  
	grid on; xlim([0, 30000]);

	if ring ==2
		x1 = EXP_T_AVG_OUT_CLAD_600mm(:,1);
		y1 = EXP_T_AVG_OUT_CLAD_600mm(:,2);
		x2 = EXP_T_AVG_OUT_CLAD_700mm(:,1);
		y2 = EXP_T_AVG_OUT_CLAD_700mm(:,2);
		
		plot(x1,y1,'ok','LineWidth',1,'MarkerSize',3);
		plot(x2,y2,'or','LineWidth',1,'MarkerSize',3);
		
		labels = {labels{:} ,  'EXP Out. 600mm',  'EXP Out. 700mm' };
		
	else
		
		labels = {labels{:}};
	end	


	legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

	if (flag2 == 1)
		print('-dtiff',[case1,'/', name, code1, '.tiff'])
		savefig([case1,'/', name, code1, '.fig'])
		%saveas(gcf,[Fig1.fig'],'fig');
	end

	end

	% ===================================================================
	% FIGURE  - Shroud Temperatures 1
	% ===================================================================
	% Inside Shroud Zirconia Insulator 200mm - Level 6 COR6
	a=a+1;
	figure(a)
	name = ['Fig', num2str(a)];

	hold on; set(0, 'DefaulttextInterpreter', 'none');
	title('Inner Zirconia Shroud Temp. COR4:100mm  COR5:200mm  COR6:300mm');

	x = EDF_TEMP_SH(:,1);

	labels={};

	% ! Node 10 - Zirconia Inner Shroud Inner Node 
	y1 = EDF_TEMP_SH(:,2);
	y2 = EDF_TEMP_SH(:,3);
	y3 = EDF_TEMP_SH(:,4);
	plot(x,y1,'--','LineWidth',1,'MarkerSize',2,'Color',cmap11(1,:));
	plot(x,y2,'--','LineWidth',1,'MarkerSize',2,'Color',cmap11(2,:));
	plot(x,y3,'--','LineWidth',1,'MarkerSize',2,'Color',cmap11(3,:));

	label = {'100mm Node 10' '200mm Node 10' '300mm Node 10'};
	labels=[labels  label];

	%! Node 11 - Zirconia Outer Shroud Inner Node
	y1 = EDF_TEMP_SH(:,5);
	y2 = EDF_TEMP_SH(:,6);
	y3 = EDF_TEMP_SH(:,7);
	plot(x,y1,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(4,:));
	plot(x,y2,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(5,:));
	plot(x,y3,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(6,:));
	label = {'100mm Node 11' '200mm Node 11' '300mm Node 11'};
	labels=[labels  label];

	%! Node 12 - Inconel Inner Node
	y1 = EDF_TEMP_SH(:,8);
	y2 = EDF_TEMP_SH(:,9);
	y3 = EDF_TEMP_SH(:,10);
	plot(x,y1,'-.','LineWidth',1,'MarkerSize',2,'Color',cmap11(8,:));
	plot(x,y2,'-.','LineWidth',1,'MarkerSize',2,'Color',cmap11(9,:));
	plot(x,y3,'-.','LineWidth',1,'MarkerSize',2,'Color',cmap11(10,:));
	label = {'100mm Node 12' '200mm Node 12' '300mm Node 12'}
	labels=[labels  label];

	% Experimental  Inner Shroud Temperature at 200mm and 300mm - inside shroud zirconia insulator
	% For MELCOR MANUAL VOL3 it was HS node 14
		x1 = EXP_T_ZR_SH_INS_200mm(:,1);
		y1 = EXP_T_ZR_SH_INS_200mm(:,2);
		plot(x1,y1,'ok','LineWidth',1,'MarkerSize',3)
		labels = {labels{:} ,  'EXP 200mm' };
		
	xlabel('Time, [s]');
	ylabel('Temperature, [K]');
	legend('Location','northeast');
	box on; %ylim([0 inf]);  
	grid on; xlim([0, 30000]);

	legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

	if (flag2 == 1)
		print('-dtiff',[case1,'/', name, code1, '.tiff'])
		savefig([case1,'/', name, code1, '.fig'])
		%saveas(gcf,[Fig1.fig'],'fig');
	end

	% ===================================================================
	% FIGURE  - Shroud Temperatures 600mm
	% ===================================================================
	% Inside Shroud Zirconia Insulator 600mm - Level 9 COR 9
	a=a+1;
	figure(a)
	name = ['Fig', num2str(a)];

	hold on; set(0, 'DefaulttextInterpreter', 'none');
	title('Shroud Temp. COR8: 500mm COR9: 600mm COR10: 700mm');

	x = EDF_TEMP_SH(:,1);

	labels={};

	% ! Node 10 - Zirconia Inner Shroud Inner Node 
	y1 = EDF_TEMP_SH(:,11);
	y2 = EDF_TEMP_SH(:,12);
	y3 = EDF_TEMP_SH(:,13);
	plot(x,y1,'--','LineWidth',1,'MarkerSize',2,'Color',cmap11(1,:));
	plot(x,y2,'--','LineWidth',1,'MarkerSize',2,'Color',cmap11(2,:));
	plot(x,y3,'--','LineWidth',1,'MarkerSize',2,'Color',cmap11(3,:));

	label = {'500mm Node 10' '600mm Node 10' '700mm Node 10'};
	labels=[labels  label];

	%! Node 11 - Zirconia Outer Shroud Inner Node
	y1 = EDF_TEMP_SH(:,14);
	y2 = EDF_TEMP_SH(:,15);
	y3 = EDF_TEMP_SH(:,16);
	plot(x,y1,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(4,:));
	plot(x,y2,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(5,:));
	plot(x,y3,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(6,:));
	label = {'500mm Node 11' '600mm Node 11' '700mm Node 11'};
	labels=[labels  label];

	%! Node 12 - Inconel Inner Node
	y1 = EDF_TEMP_SH(:,17);
	y2 = EDF_TEMP_SH(:,18);
	y3 = EDF_TEMP_SH(:,19);
	plot(x,y1,'-.','LineWidth',1,'MarkerSize',2,'Color',cmap11(8,:));
	plot(x,y2,'-.','LineWidth',1,'MarkerSize',2,'Color',cmap11(9,:));
	plot(x,y3,'-.','LineWidth',1,'MarkerSize',2,'Color',cmap11(10,:));
	label = {'500mm Node 12' '600mm Node 12' '700mm Node 12'}
	labels=[labels  label];

	% Experimental  Inner Shroud Temperature at 600mm - inside shroud zirconia insulator
	% For MELCOR MANUAL VOL3 it was HS node 14

		x1 = EXP_T_ZR_SH_INS_600mm(:,1);
		y1 = EXP_T_ZR_SH_INS_600mm(:,2);
		plot(x1,y1,'ok','LineWidth',1,'MarkerSize',3)
		labels = {labels{:} ,  'EXP 600mm' };
		


	xlabel('Time, [s]');
	ylabel('Temperature, [K]');
	legend('Location','northeast');
	box on; %ylim([0 inf]);  
	grid on; xlim([0, 30000]);


	legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

	if (flag2 == 1)
		print('-dtiff',[case1,'/', name, code1, '.tiff'])
		savefig([case1,'/', name, code1, '.fig'])
		%saveas(gcf,[Fig1.fig'],'fig');
	end

	% ===================================================================
	% FIGURE  Core Energy Balance
	% ===================================================================
	a=a+1;
	figure(a)
	name = ['Fig', num2str(a)];

	hold on; set(0, 'DefaulttextInterpreter', 'none');
	title('Energy Balance');
	labels={};

	x = EDF_POWER(:,1);

	y1 = EDF_POWER(:,8); %EFPD
	y2 = EDF_POWER(:,9); %EMWR
	y3 = EDF_POWER(:,10); %ECNV
	y4 = EDF_POWER(:,11); %EBND
	y5 = EDF_POWER(:,13); %DCH-TOTCLSPOW
	y6 = EDF_POWER(:,14); %DCH-COREPOW
	y7 = y1 + y2 - y3 - y4; %Balance

	plot(x,y1,'-k','LineWidth',1,'MarkerSize',2);
	plot(x,y2,'-g','LineWidth',1,'MarkerSize',2);
	plot(x,y3,'-b','LineWidth',1,'MarkerSize',2);
	plot(x,y4,'-r','LineWidth',1,'MarkerSize',2);
	plot(x,y5,'-.c','LineWidth',1,'MarkerSize',4);
	plot(x,y6,'-.m','LineWidth',1,'MarkerSize',4);
	plot(x,y7,'-','LineWidth',1,'MarkerSize',4,'Color',[1.00,0.41,0.16]);
	%labels = {'EFPD - Fission Power', 'EMWR - Oxidation Power',   ...
	%           'ECNV - Convective Removal', 'EBND - Conduction Removal' };
	labels = {'COR-EFPD', 'COR-EMWR',   ...
			'COR-ECNV', 'COR-EBND' ...
	'DCH-TOTCLSPOW', 'DCH-COREPOW', 'MELCOR BALANCE'};
	%'EFPD+EMWR-EBND-ECNV'
	
	% labels = {'COR-EFPD - Fission Power', 'COR-EMWR - Oxidation Power',   ...
	%           'COR-ECNV - Convective Removal', 'COR-EBND - Conduction Removal' ...
	% 'DCH-TOTCLSPOW Decay Heat All RNs', 'DCH-COREPOW Total Decay Heat'};


	x1 = EXP_POWER_NUCL(:,1);
	y1 = EXP_POWER_NUCL(:,3);
	x2 = EXP_POWER_OXDN(:,1);
	y2 = EXP_POWER_OXDN(:,3);
	x3 = EXP_POWER_CONV(:,1);
	y3 = EXP_POWER_CONV(:,3);
	x4 = EXP_POWER_SHRD(:,1);
	y4 = EXP_POWER_SHRD(:,3);
	x5 = EXP_POWER_STDHT(:,1);
	y5 = EXP_POWER_STDHT(:,3);

	plot(x1,y1,'--k','LineWidth',1,'MarkerSize',2);
	plot(x2,y2,'--g','LineWidth',1,'MarkerSize',2);
	plot(x3,y3,'--b','LineWidth',1,'MarkerSize',2);
	plot(x4,y4,'--r','LineWidth',1,'MarkerSize',2);
	plot(x5,y5,'--y','LineWidth',1,'MarkerSize',4,'Color',[1.00,0.41,0.16]);

	ylim([0 40000])

	labels1 = {'SNL NUCLEAR', 'SNL OXDN',   ...
			'SNL CONV', 'SNL SHRD', 'SNL STDHT'};

	labels = [labels{:} , labels1];

	xlabel('Time, [s]');
	ylabel('Power, [W]');
	%legend('Location','northwest');
	box on; %ylim([0 inf]);
	grid on; xlim([0, 30000]);

	legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

	if (flag2 == 1)
		print('-dtiff',[case1,'/', name, code1, '.tiff'])
		savefig([case1,'/', name, code1, '.fig'])
		%saveas(gcf,[Fig1.fig'],'fig');
	end


	%%%
end
%}