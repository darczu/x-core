% ===================================================================
% FIGURE - Containment Pressure
% ===================================================================
a=a+1;
figure(a)
name = ['Fig', num2str(a)];

hold on; set(0, 'DefaulttextInterpreter', 'none');
title('Containment Pressure');

x1 = EXP_P_CONT(:,1);
y1 = EXP_P_CONT(:,2);
x11 = EXP_P_STEAM_CONT(:,1);
y11 = EXP_P_STEAM_CONT(:,2); 
x12 = EXP_P_NCG_CONT(:,1);
y12 = EXP_P_NCG_CONT(:,2);


x2 = EDF_PRESS(:,1);
y2 = EDF_PRESS(:,16);
x = EDF_RHUM(:,1); 
y3 = EDF_RHUM(:,2); %Cont Vapour Pressure
y41=EDF_RHUM(:,5);
y42=EDF_RHUM(:,6);
y43=EDF_RHUM(:,7); 
y4 = y41+y42+y43; %NCG PRessure

plot(x1,y1,'o','LineWidth',1,'MarkerSize',2);
plot(x11,y11,'<','LineWidth',1,'MarkerSize',3);
plot(x12,y12,'x','LineWidth',1,'MarkerSize',3);

plot(x2,y2,'-','LineWidth',1,'MarkerSize',2);
plot(x,y3,'--','LineWidth',1,'MarkerSize',2);
plot(x,y4,'-.','LineWidth',1,'MarkerSize',2);

xlabel('Time, [s]');
ylabel('Pressure, [Pa]');
legend('Location','northeast');
box on; ylim([0 3.5e5]);  grid on; xlim([0, 30000]);

labels = { 'EXP Total' 'EXP Steam' 'EXP NCG' 'MELCOR' 'MELCOR Steam' 'MELCOR NCG'};
legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end

% ===================================================================
% FIGURE - Containment Relative Humidity
% ===================================================================
a=a+1;
figure(a)
name = ['Fig', num2str(a)];

hold on; set(0, 'DefaulttextInterpreter', 'none');
title('Containment Relative Humidity');

x1 = EXP_CONT_RHUM(:,1);
y1 = EXP_CONT_RHUM(:,2);

x10 = EDF_RHUM(:,1); %Time
y10 = EDF_RHUM(:,2); %Cont Vapour Pressure
y20 = EDF_RHUM(:,3); %Cont Saturation Pressure

y30= y10./y20;

plot(x1,y1,'ok','LineWidth',1,'MarkerSize',2);
plot(x10,y30,'-r','LineWidth',1,'MarkerSize',2);
%plot(x,y1,'--','LineWidth',1,'MarkerSize',2);
%plot(x,y4,'-.','LineWidth',1,'MarkerSize',2);

xlabel('Time, [s]');
ylabel('Humidity, [-]');
legend('Location','northwest');
box on; %ylim([0 1.2]);  
grid on; xlim([0, 30000]);

labels = { 'EXP' 'MELCOR RHUM' }
legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end

% ===================================================================
% FIGURE - Containment Condensation Rate
% ===================================================================
a=a+1;
figure(a)
name = ['Fig', num2str(a)];

hold on; set(0, 'DefaulttextInterpreter', 'none');
title('Containment Condensation Rate');

x1 = EXP_COND_RATE(:,1); 
y1 = EXP_COND_RATE(:,2).*1000; %kg/s

plot(x1,y1,'ko','LineWidth',1,'MarkerSize',2);


AREA = 3*pi*(0.15)*1.718;  %[m2]

x10 = EDF_COND(:,1); %Time
y10 = EDF_COND(:,2); %
y10 = y10.*AREA*1000; %to grams/s

plot(x10,y10,'-r','LineWidth',1,'MarkerSize',2);

xlabel('Time, [s]');
%ylabel('Mass Flux, [kg/m2/s]');
ylabel('Mass Flow, [grams/s]');
legend('Location','northwest');
box on; %ylim([-inf inf]);  
grid on; xlim([0, 30000]);

labels = { 'EXP Condensation' 'MELCOR Wet Condenser' }
legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end


% ===================================================================		
% ===================================================================		
% ===================================================================		
if flagKONDENSAT == 1
    % ===================================================================
    % FIGURE - Containment Condensation Integral
    % ===================================================================
        a=a+1;
        figure(a)
        name = ['Fig', num2str(a)];
    
        hold on; 
        set(0, 'DefaulttextInterpreter', 'none');
        title('Containment Integrated Condensation Rate');
    
        AREA = 3*pi*(0.15)*1.718;  %[m2]
    
        x10 = EDF_COND(:,1); %Time
        y10 = EDF_COND(:,2); %
        y10 = y10.*AREA*1000; %to grams/s

            KONDENSAT =0.0;
            for i=2:numel(x10)
                x =x10(1:i);
                y = y10(1:i);
                integral = trapz(x,y );
                KONDENSAT = [KONDENSAT; integral];
            end
    
            plot(x10,KONDENSAT./1000,'-r','LineWidth',1,'MarkerSize',2);
    
            TOTAL_KONDENSAT = trapz(x10,y10)
    
        xlabel('Time, [s]');
        %ylabel('Mass Flux, [kg/m2/s]');
        ylabel('Integrated Condensation Rate, [kg]');
        legend('Location','northwest');
        box on; %ylim([-inf inf]);  
        grid on; xlim([0, 30000]);
    
        labels = {  'MELCOR Wet Condenser' }
        legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');
    
            if (flag2 == 1)
                print('-dtiff',[case1,'/', name, code1, '.tiff'])
                savefig([case1,'/', name, code1, '.fig'])
                %saveas(gcf,[Fig1.fig'],'fig');
            end
            
    % ===================================================================
    % FIGURE - Containment Sump Water
    % ===================================================================		
        a=a+1;
        figure(a)
        name = ['Fig', num2str(a)];
        hold on; 
        set(0, 'DefaulttextInterpreter', 'none');
        title('Sump Water Mass');
    
       x100 = EDF_CONT_MASS(:,1);
       y100 = EDF_CONT_MASS(:,3);
       y101 = interp1(x10,KONDENSAT./1000,x100,'spline');
       y102 = y100 - y101;
       
        plot(x100,y100,'-r','LineWidth',1,'MarkerSize',2);
        plot(x100,y102,'-b','LineWidth',1,'MarkerSize',2);
        
        xlabel('Time, [s]');
        %ylabel('Mass Flux, [kg/m2/s]');
        ylabel('Mass, [kg]');
        legend('Location','northwest');
        box on; %ylim([-inf inf]);  
        grid on; xlim([0, 30000]);
    
        labels = {  'Sump Pool Mass' 'Sump Pool Mass - Condensation'}
        legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');
    
            if (flag2 == 1)
                print('-dtiff',[case1,'/', name, code1, '.tiff'])
                savefig([case1,'/', name, code1, '.fig'])
                %saveas(gcf,[Fig1.fig'],'fig');
            end     
    end
    % ===================================================================		
    % ===================================================================		
    % ===================================================================	


    % ===================================================================
% FIGURE - Steam Partial Pressure
% ===================================================================
a=a+1;
figure(a)
name = ['Fig', num2str(a)];

hold on; set(0, 'DefaulttextInterpreter', 'none');
title('Steam Partial Pressure');

x1 = EXP_P_STEAM_CONT(:,1);
y1 = EXP_P_STEAM_CONT(:,2);
x12 = EXP_P_NCG_CONT(:,1);
y12 = EXP_P_NCG_CONT(:,2);

plot(x1,y1,'o','LineWidth',1,'MarkerSize',2);
plot(x12,y12,'<','LineWidth',1,'MarkerSize',2);


x = EDF_RHUM(:,1);
y2 = EDF_RHUM(:,2); %Cont Vapour Pressure
y3 = EDF_RHUM(:,3); %Saturation Atm Pressure

y41=EDF_RHUM(:,5);
y42=EDF_RHUM(:,6);
y43=EDF_RHUM(:,7); 

y4 = y41+y42+y43; %NCG PRessure

plot(x,y2,'-','LineWidth',1,'MarkerSize',2);
plot(x,y3,'-','LineWidth',1,'MarkerSize',2);
plot(x,y4,'--','LineWidth',1,'MarkerSize',2);


xlabel('Time, [s]');
ylabel('Pressure, [Pa]');
legend('Location','southwest');
box on; ylim([0 1.8e5]);  
grid on; xlim([0, 30000]);

labels = { 'EXP Steam' 'EXP NCG' 'MELCOR' 'Saturation Pressure' 'NCG Pressure'};
legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end


% ===================================================================
% FIGURE - Containment Temperature
% ===================================================================
a=a+1;
figure(a)
name = ['Fig', num2str(a)];

hold on; set(0, 'DefaulttextInterpreter', 'none');
title('Containment Temperature');

x1 = EXP_TVAP_CONT(:,1);
y1 = EXP_TVAP_CONT(:,2);

x2 = EDF_TVAP(:,1);
y2 = EDF_TVAP(:,16);
%y2 = EDF_TVAP(:,16);  ZMIEN!!!
%y2 = EDF_RHUM(:,3); %Saturation Atm

plot(x1,y1,'ok','LineWidth',1,'MarkerSize',2);
plot(x2,y2,'-r','LineWidth',1,'MarkerSize',2);


xlabel('Time, [s]');
ylabel('Temperature, [K]');
%legend('Location','northwest');
legend('Location','northeast');
box on; ylim([370 390]);  
grid on; xlim([0, 30000]);

labels = { 'EXP' 'MELCOR' };
legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end