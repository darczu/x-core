% 
% 
clear; clc; close all;
format short g
load EXP_DATA;

flag2 = 1; %SAVE FIGURE 1-YES 0-NO

case1 = 'TEST0017_M21';
code1 = 'MELCOR2.1.6341';

%case1 = 'TEST0017_M22';
%code1 ='M2.2.9541';

cmap = hsv(11);
cmap1 = hsv(14);
cmap2 = hsv(28);

% plot(TimeStepSeconds./3600,keff_all(:),'o-','color',cmap(cb+1,:),'linewidth',1.0);
%	labels = {labels{:}, ['cb ', int2str(cb)]};
%		if first==1
%			hold on;
%			first=0;
%		end
%  legend('Location','northeastoutside')
%
%plot(x1,y1,'o','LineWidth',1,'MarkerSize',2,'Color',cmap(1));

EDF_H2 = importdata([case1 '/EDF_H2']);
EDF_COND = importdata([case1 '/EDF_COND']);
EDF_POWER = importdata([case1 '/EDF_POWER']);
EDF_PRESS = importdata([case1 '/EDF_PRESS']);
EDF_RHUM = importdata([case1 '/EDF_RHUM']);
EDF_TVAP = importdata([case1 '/EDF_TVAP']);
EDF_TEMP_CL = importdata([case1 '/EDF_TEMP_CL']);
EDF_TEMP_FU = importdata([case1 '/EDF_TEMP_FU']);
EDF_TEMP_SH = importdata([case1 '/EDF_TEMP_SH']);

%%
% ===================================================================
% FIGURE 1 - Generated Hydrogen
% ===================================================================
a=1
figure(a)
hold on
title('Generated Hydrogen');
name = ['Fig', num2str(a)];
x1 = EXP_H2_TOT(:,1);
y1 = EXP_H2_TOT(:,2);
x2 = EDF_H2(:,1);
y2 = EDF_H2(:,3);

plot(x1,y1,'o','LineWidth',1,'MarkerSize',2);
plot(x2,y2,'-','LineWidth',1,'MarkerSize',2);


xlabel('Time, [s]');
ylabel('Mass, [kg]');
legend('Location','northwest');
box on; grid on;

labels = { 'EXP' code1 };
legend(labels);

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end

% ===================================================================
% FIGURE 2 - Generated Hydrogen Rate
% ===================================================================
a = a+1;
figure(a)
hold on
title('Generated Hydrogen Rate');
name = ['Fig', num2str(a)];
x1 = EXP_H2_RATE(:,1);
y1 = EXP_H2_RATE(:,2);
x2 = EDF_H2(:,1);
y2 = EDF_H2(:,2);

plot(x1,y1,'o','LineWidth',1,'MarkerSize',2);
plot(x2,y2,'-','LineWidth',1,'MarkerSize',2);


xlabel('Time, [s]');
ylabel('Mass, [kg/s]');
legend('Location','northwest');
box on; grid on;

labels = { 'EXP' code1 };
legend(labels);

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end




% ===================================================================
% FIGURE 3 - Containment Pressure
% ===================================================================
a=a+1;
figure(a)
name = ['Fig', num2str(a)];

hold on
title('Containment Pressure');

x1 = EXP_P_CONT(:,1);
y1 = EXP_P_CONT(:,2);
x2 = EDF_PRESS(:,1);
y2 = EDF_PRESS(:,15);

plot(x1,y1,'o','LineWidth',1,'MarkerSize',2);
plot(x2,y2,'-','LineWidth',1,'MarkerSize',2);


xlabel('Time, [s]');
ylabel('Pressure, [Pa]');
legend('Location','northwest');
box on; grid on;

labels = { 'EXP' code1 };
legend(labels);

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end




% ===================================================================
% FIGURE 4 - Steam Partial Pressure
% ===================================================================
a=a+1;
figure(a)
name = ['Fig', num2str(a)];

hold on
title('Steam Partial Pressure');

x1 = EXP_P_STEAM_CONT(:,1);
y1 = EXP_P_STEAM_CONT(:,2);
x2 = EDF_RHUM(:,1);
y2 = EDF_RHUM(:,2);
x3 = EDF_RHUM(:,1);
y3 = EDF_RHUM(:,3); %Saturation Atm Pressure

plot(x1,y1,'o','LineWidth',1,'MarkerSize',2);
plot(x2,y2,'-','LineWidth',1,'MarkerSize',2);
plot(x3,y3,'-','LineWidth',1,'MarkerSize',2);


xlabel('Time, [s]');
ylabel('Pressure, [Pa]');
legend('Location','northwest');
box on; grid on;

labels = { 'EXP' code1 'Saturation Pressure'};
legend(labels);

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end



% ===================================================================
% FIGURE 5 - Containment Temperature
% ===================================================================
a=a+1;
figure(a)
name = ['Fig', num2str(a)];

hold on
title('Containment Temperature');

x1 = EXP_TVAP_CONT(:,1);
y1 = EXP_TVAP_CONT(:,2);
x2 = EDF_TVAP(:,1);
y2 = EDF_TVAP(:,end);
%y2 = EDF_TVAP(:,16);  ZMIEN!!!
%y2 = EDF_RHUM(:,3); %Saturation Atm

plot(x1,y1,'o','LineWidth',1,'MarkerSize',2);
plot(x2,y2,'-','LineWidth',1,'MarkerSize',2);


xlabel('Time, [s]');
ylabel('Temperature, [K]');
legend('Location','northwest');
box on; grid on;

labels = { 'EXP' code1 };
legend(labels);

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end


% ===================================================================
% FIGURE 6 - SG Temperatures
% ===================================================================
a=a+1;
figure(a)
name = ['Fig', num2str(a)];

hold on
title('SG Temperatures');

x1 = EXP_TVAP_SG_320(:,1);
y1 = EXP_TVAP_SG_320(:,2);
x2 = EXP_TVAP_SG_740(:,1);
y2 = EXP_TVAP_SG_740(:,2);
x3 = EXP_TVAP_SG_1500(:,1);
y3 = EXP_TVAP_SG_1500(:,2);
x4 = EXP_TVAP_SG_2000(:,1);
y4 = EXP_TVAP_SG_2000(:,2);
x5 = EXP_TVAP_SG_3000(:,1);
y5 = EXP_TVAP_SG_3000(:,2);
plot(x1,y1,'<k','LineWidth',1,'MarkerSize',3);
plot(x2,y2,'sk','LineWidth',1,'MarkerSize',3);
plot(x3,y3,'xk','LineWidth',1,'MarkerSize',3);
plot(x4,y4,'ok','LineWidth',1,'MarkerSize',3);
plot(x5,y5,'>k','LineWidth',1,'MarkerSize',3);




xt = EDF_TVAP(:,1);
y20 = EDF_TVAP(:,8);
y21 = EDF_TVAP(:,9);
y22 = EDF_TVAP(:,10);
y23 = EDF_TVAP(:,11);
y24 = EDF_TVAP(:,12);
y25 = EDF_TVAP(:,13);
y26 = EDF_TVAP(:,14);

plot(xt,y20,'-','LineWidth',1,'MarkerSize',2,'Color',cmap(1,:));
plot(xt,y21,'-','LineWidth',1,'MarkerSize',2,'Color',cmap(2,:));
plot(xt,y22,'-','LineWidth',1,'MarkerSize',2,'Color',cmap(3,:));
plot(xt,y23,'-','LineWidth',1,'MarkerSize',2,'Color',cmap(4,:));
plot(xt,y24,'-','LineWidth',1,'MarkerSize',2,'Color',cmap(5,:));
plot(xt,y25,'-','LineWidth',1,'MarkerSize',2,'Color',cmap(6,:));
plot(xt,y26,'-','LineWidth',1,'MarkerSize',2,'Color',cmap(7,:));


xlabel('Time, [s]');
ylabel('Temperature, [K]');
legend('Location','northwest');
box on; grid on;

labels = { 'EXP 320' 'EXP 740' 'EXP 1500' 'EXP 2000' 'EXP 3000' ...
  'SG-IN' 'SG-U1' 'SG-U2' 'SG-U3' 'SG-U4' 'SG-D1'  'SG-CL1'};
legend(labels);

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end



% ===================================================================
% FIGURE 7 - Fuel Temperatures
% ===================================================================
for ring=1:2
	a=a+1;
	figure(a)
	name = ['Fig', num2str(a)];

	hold on
	title(['Fuel Temperatures Ring', int2str(ring) ]);
	%title('Fuel Temperatures');
	labels = {};

	x = EDF_TEMP_FU(:,1); %time
	col=1;

		%for level=1:14
		%for ring=1:2
		for level=4:13
		  offset = (ring - 1)*14;
		  y = EDF_TEMP_FU(:,1+ level + offset); 	  
		  plot(x,y,'-','LineWidth',1,'MarkerSize',2,'Color',cmap1(col,:));
		  
		  if(level<10)
			labels = {labels{:}, ['COR-TFU ', int2str(ring), '0', int2str(level)]};
		  else
			labels = {labels{:}, ['COR-TFU ', int2str(ring), '', int2str(level)]};
		  end
		  
		  col = col+1;
		%end

		end

	xlabel('Time, [s]');
	ylabel('Temperature, [K]');
	legend('Location','northwest');
	box on; grid on;

	%labels = {' '};
	legend(labels);

	if (flag2 == 1)
		print('-dtiff',[case1,'/', name, code1, '.tiff'])
		savefig([case1,'/', name, code1, '.fig'])
		%saveas(gcf,[Fig1.fig'],'fig');
	end

end



% ===================================================================
% FIGURE 9 - Clad Temperatures
% ===================================================================
for ring=1:2
a=a+1;
figure(a)
name = ['Fig', num2str(a)];

hold on
title(['Clad Temperatures Ring', int2str(ring) ]);
labels = {};

x = EDF_TEMP_CL(:,1); %time
col=1;

	%for level=1:14
	%for ring=1:2
	for level=5:14
	  offset = (ring - 1)*14;
	  y = EDF_TEMP_CL(:,1+ level + offset); 	  
	  plot(x,y,'-','LineWidth',1,'MarkerSize',2,'Color',cmap1(col,:));
	  
	  if(level<10)
		labels = {labels{:}, ['COR-TCL ', int2str(ring), '0', int2str(level)]};
      else
	    labels = {labels{:}, ['COR-TCL ', int2str(ring), '', int2str(level)]};
	  end
	  
	  col = col+1;
	%end

	end

xlabel('Time, [s]');
ylabel('Temperature, [K]');
legend('Location','northwest');
box on; grid on;

%labels = {' '};
legend(labels);

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end

end

% ===================================================================
% FIGURE 10 - Shroud Temperatures 1
% ===================================================================
% Inside Shroud Zirconia Insulator 200mm - Level 6 COR6
a=a+1;
figure(a)
name = ['Fig', num2str(a)];

hold on
title('Shroud Temp. COR4: 100mm COR5: 200mm COR6: 300mm');

x = EDF_TEMP_SH(:,1);

labels={};

% ! Node 6 - Zirconia Inner Shroud Inner Node 
y1 = EDF_TEMP_SH(:,2);
y2 = EDF_TEMP_SH(:,3);
y3 = EDF_TEMP_SH(:,4);
plot(x,y1,'-','LineWidth',1,'MarkerSize',2,'Color',cmap1(1,:));
plot(x,y2,'-','LineWidth',1,'MarkerSize',2,'Color',cmap1(2,:));
plot(x,y3,'-','LineWidth',1,'MarkerSize',2,'Color',cmap1(3,:));

label = {'100mm Zirconia Inner' '200mm Zirconia Inner' '300mm Zirconia Inner'};
labels=[labels  label]

%! Node 14 - Zirconia Outer Shroud Inner Node
y1 = EDF_TEMP_SH(:,5);
y2 = EDF_TEMP_SH(:,6);
y3 = EDF_TEMP_SH(:,7);
plot(x,y1,'-','LineWidth',1,'MarkerSize',2,'Color',cmap1(4,:));
plot(x,y2,'-','LineWidth',1,'MarkerSize',2,'Color',cmap1(5,:));
plot(x,y3,'-','LineWidth',1,'MarkerSize',2,'Color',cmap1(6,:));
label = {'100mm Zirconia Outer' '200mm Zirconia Outer' '300mm Zirconia Outer'};
labels=[labels  label]

%! Node 16 - Inconel Inner Node
y1 = EDF_TEMP_SH(:,8);
y2 = EDF_TEMP_SH(:,9);
y3 = EDF_TEMP_SH(:,10);
plot(x,y1,'-','LineWidth',1,'MarkerSize',2,'Color',cmap1(8,:));
plot(x,y2,'-','LineWidth',1,'MarkerSize',2,'Color',cmap1(9,:));
plot(x,y3,'-','LineWidth',1,'MarkerSize',2,'Color',cmap1(10,:));
label = {'100mm Inconel Inner' '200mm Inconel Inner' '300mm Inconel Inner'}
labels=[labels  label]




xlabel('Time, [s]');
ylabel('Temperature, [K]');
legend('Location','northwest');
box on; grid on;


legend(labels);

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end

% ===================================================================
% FIGURE 10 - Shroud Temperatures 600mm
% ===================================================================
% Inside Shroud Zirconia Insulator 600mm - Level 9 COR 9
a=a+1;
figure(a)
name = ['Fig', num2str(a)];

hold on
title('Shroud Temp. COR8: 500mm COR9: 600mm COR10: 700mm');

x = EDF_TEMP_SH(:,1);

labels={};

% ! Node 6 - Zirconia Inner Shroud Inner Node 
y1 = EDF_TEMP_SH(:,11);
y2 = EDF_TEMP_SH(:,12);
y3 = EDF_TEMP_SH(:,13);
plot(x,y1,'-','LineWidth',1,'MarkerSize',2,'Color',cmap1(1,:));
plot(x,y2,'-','LineWidth',1,'MarkerSize',2,'Color',cmap1(2,:));
plot(x,y3,'-','LineWidth',1,'MarkerSize',2,'Color',cmap1(3,:));

label = {'500mm Zirconia Inner' '600mm Zirconia Inner' '700mm Zirconia Inner'};
labels=[labels  label]

%! Node 14 - Zirconia Outer Shroud Inner Node
y1 = EDF_TEMP_SH(:,14);
y2 = EDF_TEMP_SH(:,15);
y3 = EDF_TEMP_SH(:,16);
plot(x,y1,'-','LineWidth',1,'MarkerSize',2,'Color',cmap1(4,:));
plot(x,y2,'-','LineWidth',1,'MarkerSize',2,'Color',cmap1(5,:));
plot(x,y3,'-','LineWidth',1,'MarkerSize',2,'Color',cmap1(6,:));
label = {'500mm Zirconia Outer' '600mm Zirconia Outer' '700mm Zirconia Outer'};
labels=[labels  label]

%! Node 16 - Inconel Inner Node
y1 = EDF_TEMP_SH(:,17);
y2 = EDF_TEMP_SH(:,18);
y3 = EDF_TEMP_SH(:,19);
plot(x,y1,'-','LineWidth',1,'MarkerSize',2,'Color',cmap1(8,:));
plot(x,y2,'-','LineWidth',1,'MarkerSize',2,'Color',cmap1(9,:));
plot(x,y3,'-','LineWidth',1,'MarkerSize',2,'Color',cmap1(10,:));
label = {'500mm Inconel Inner' '600mm Inconel Inner' '700mm Inconel Inner'}
labels=[labels  label]




xlabel('Time, [s]');
ylabel('Temperature, [K]');
legend('Location','northwest');
box on; grid on;


legend(labels);

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end

% ===================================================================
% Core Energy Balance
% ===================================================================
a=a+1;
figure(a)
name = ['Fig', num2str(a)];

hold on
title('Energy Balance');
labels={};

x = EDF_POWER(:,1);

y1 = EDF_POWER(:,8); %EFPD
y2 = EDF_POWER(:,9); %EMWR
y3 = EDF_POWER(:,10); %ECNV
y4 = EDF_POWER(:,11); %EBND
%y5 = EDF_POWER(:,13); %DCH-TOTCLSPOW
%y6 = EDF_POWER(:,14); %DCH-COREPOW


plot(x,y1,'-k','LineWidth',1,'MarkerSize',2);
plot(x,y2,'-g','LineWidth',1,'MarkerSize',2);
plot(x,y3,'-b','LineWidth',1,'MarkerSize',2);
plot(x,y4,'-r','LineWidth',1,'MarkerSize',2);
%plot(x,y5,'-b','LineWidth',1,'MarkerSize',2);
%plot(x,y6,'-r','LineWidth',1,'MarkerSize',2);
labels = {'EFPD - Fission Power', 'EMWR - Oxidation Power',   ...
           'ECNV - Convective Removal', 'EBND - Conduction Removal' };
%labels = {'COR-EFPD - Fission Power', 'COR-EMWR - Oxidation Power',   ...
%           'COR-ECNV - Convective Removal', 'COR-EBND - Conduction Removal' ...
% 'DCH-TOTCLSPOW Decay Heat All Radionuclides', 'DCH-COREPOW Total Decay Heat'};



xlabel('Time, [s]');
ylabel('Power, [W]');
legend('Location','northwest');
box on; grid on;

legend(labels);

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end
% ===================================================================
% FIGURE 
% ===================================================================

% ===================================================================
% FIGURE 
% ===================================================================

% ===================================================================
% FIGURE 
% ===================================================================

% ===================================================================
% FIGURE 
% ===================================================================

% ===================================================================
% FIGURE 
% ===================================================================

% ===================================================================
% FIGURE 
% ===================================================================
%{
a=a+1
figure(a)
name = ['Fig', num2str(a)];

hold on
title('');

x1 = EXP_P_STEAM_CONT(:,1);
y1 = EXP_P_STEAM_CONT(:,2);
x2 = EDF_RHUM(:,1);
y2 = EDF_RHUM(:,2);
%y2 = EDF_RHUM(:,3); %Saturation Atm

plot(x1,y1,'o','LineWidth',1,'MarkerSize',2);
plot(x2,y2,'-','LineWidth',1,'MarkerSize',2);


xlabel('Time, [s]');
ylabel('');
legend('Location','northwest');
box on; grid on;

labels = { 'EXP' code1 };
legend(labels);

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end
%}