a = a+1;

fig = figure(a)
hold on; 
set(0, 'DefaulttextInterpreter', 'none'); 
%set(0, 'DefaulttextInterpreter', 'latex'); 

%set(fig, 'DefaultFigureColormap', cmapdyn)  %nie dziala
%colormap(cmapdyn);		%					nie dziala

if k_num == numel(cases)
    name = ['Fig', num2str(a)];
    for xxx = 1:numel(cases)
        %name = [name, '_', cases_names{xxx}];
        name = [name, '_', cases{xxx}];
    end
end

xlabel('Time, [s]');
ylabel('Mass, [kg]');
grid on; 
box on;
grid on;
xlim([0, 20000]); % ylim([0 inf]);


if k_num == 1 
    labels = {};
end


if k_num > 1
    hLegend = findobj(gcf, 'Type', 'Legend');
    %get text
    %labels = {}
    labels = hLegend.String;
end


legend('Location','northwest');