% ===================================================================
% ===================================================================
% ===================================================================
% ===================================================================
% AEROSOLS
% ===================================================================


% ===================================================================
% Total Aerosol Airborne Mass
% ===================================================================

a=a+1
figure(a)
hold on; 

%set(0, 'DefaulttextInterpreter', 'none');
% set(0, 'DefaulttextInterpreter', 'none');
%title([case1, ' Containment Airborne Mass']);
title('Containment Airborne Aerosol Mass');
name = ['Fig', num2str(a)];

x1 = EXP_AERO_AIRBORNE(:,1);
y1 = EXP_AERO_AIRBORNE(:,3);
%y1 = EXP_AERO_AIRBORNE(:,3)./max(EXP_AERO_AIRBORNE(:,3));

plot(x1,y1,'ko','LineWidth',1,'MarkerSize',4);


if flagATMG == 1
		xt = EDF_ATMG_400(:,1);
		y2 = EDF_ATMG_400(:,2); %ATMG aerosol gas total mass
		xt1 = EDF_TYCLAIR(:,1);
		y3 = EDF_TYCLAIR(:,2);
		
		plot(xt,y2,'--r','LineWidth',1,'MarkerSize',2);
		plot(xt,y3,'--b','LineWidth',1,'MarkerSize',2);
		plot(xt,y2-y3,'-k','LineWidth',1,'MarkerSize',2);
		
		labels = { 'Aerosol EXP' 'Aerosol TOT ATMG-400' 'Aerosol H2O' 'Aerosol Non-H2O'};
		%labels = { 'Aero EXP' 'Aero TOT ATMG 400'};
else

		xt = EDF_CONT_PARAMS(:,1);
		y2 = EDF_CONT_PARAMS(:,6); %ATMG aerosol gas total mass

		%y2 = EDF_CONT_PARAMS(:,6)./max(EDF_CONT_PARAMS(:,6));

		%y3 = EDF_CONT_PARAMS(:,7); %ARMG 

		y4 = EDF_CONT_PARAMS(:,8); %VTMG vapour gas total mass

		%y5 = EDF_CONT_PARAMS(:,9); %VRMG

		y6 = EDF_CONT_PARAMS(:,10); %ATML
		%y7 = EDF_CONT_PARAMS(:,11); %ARML
		y8 = EDF_CONT_PARAMS(:,12); %VTML
		%y9 = EDF_CONT_PARAMS(:,13); %VRML


		plot(xt,y2,'-r','LineWidth',1,'MarkerSize',2);
		%plot(xt,y3,'-b','LineWidth',1,'MarkerSize',2);
		plot(xt,y4,'-m','LineWidth',1,'MarkerSize',2);
		%plot(xt,y5,'-c','LineWidth',1,'MarkerSize',2);

		plot(xt,y6,'-g','LineWidth',1,'MarkerSize',2);
		%plot(xt,y7,'--y','LineWidth',1,'MarkerSize',2);
		plot(xt,y8,'-.c','LineWidth',1,'MarkerSize',2);
		%plot(xt,y9,'.c','LineWidth',1,'MarkerSize',2);

		
		xt2 = EDF_RELEASE(:,1); 
		y20  = EDF_RELEASE(:,2); %Total  Release RAD and NONRAD
		y21  = EDF_RELEASE(:,15); %Total  Release RAD

		plot(xt2,y20,'--k','LineWidth',1,'MarkerSize',2);
		plot(xt2,y21,'.-k','LineWidth',1,'MarkerSize',2);

		labels = { 'Aero EXP' 'Aero TOT' 'Vap TOT'  'Aero Sump TOT'  'Vap Sump TOT' 'Core Released TOT' 'Core Released RAD'};
end

xlabel('Time, [s]');
ylabel('Mass, [kg]');
legend('Location','northwest');
grid on; xlim([0, 30000]); 
box on; % ylim([0 inf]);

%labels = { 'Aero EXP' 'Aero TOT' 'Aero RAD' 'Vap TOT' 'Vap RAD' 'Aero Sump TOT' 'Aero Sump RAD' 'Vap Sump TOT' 'Vap Sump RAD' 'Released TOT' 'Released RAD'};

legend(labels);

legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end




% ===================================================================
% FIGURE 166
% total mass deposited on dry condenser
% m_min= 0.0797227g
% m_ave= 0.131716g
% m_max= 0.17331g
% 
% FIGURE 167
% total mass deposited on dry condenser
% m_min= 0.0863085g
% m_ave= 0.134142g
% m_max= 0.181282g


% ===================================================================
% Deposition Mass - Wet and Dry Condenser
% ===================================================================
a=a+1
figure(a)
hold on; 
set(0, 'DefaulttextInterpreter', 'none');
title('Condenser Deposited Mass');
name = ['Fig', num2str(a)];

x1 = EXP_AERO_DEP_WETCOND(:,1);
y1 = EXP_AERO_DEP_WETCOND(:,3);
plot(x1,y1,'ko','LineWidth',1,'MarkerSize',6);
%Dry Condenser Deposited Mass + uncertanities
x01 = 30000.0
y01 = 0.134142*1e-3;
y02 =  0.0863085*1e-3;
y03 = 0.181282*1e-3;
plot(x01,y01,'kd','LineWidth',1,'MarkerSize',6);
plot(x01,y02,'k>','LineWidth',1,'MarkerSize',6);
plot(x01,y03,'k+','LineWidth',1,'MarkerSize',6);

xt = EDF_CONT_DEP(:,1);
y2 = EDF_CONT_DEP(:,3); %TOT WET CONDESER
y3 = EDF_CONT_DEP(:,15); %RAD WET 
y4 = EDF_CONT_DEP(:,2); %TOT DRY CONDENSER
y5 = EDF_CONT_DEP(:,14); %RAD DRY

plot(xt,y2,'-r','LineWidth',1,'MarkerSize',2);
plot(xt,y3,'--r','LineWidth',1,'MarkerSize',2);
plot(xt,y4,'-b','LineWidth',1,'MarkerSize',2);
plot(xt,y5,'--b','LineWidth',1,'MarkerSize',2);

size(xt);
% INTEGRATING TOTAL MASS OF AEROSOLS
if flagWET == 1
	aeroWET = y2; %Mass of aersol at given time deposited at wet condesner
	%xt = [0.0; xt];
	dt = diff(xt);
	dy = diff(aeroWET);
	dydt = [dy./dt];
	dydt = [dydt; 0.0];
    dydt1 = abs(dydt);	
    WET_DEP_TOTAL = trapz(xt, dydt1)	
    WET_DEP =0.0;
	for i=2:numel(dt)
		x = xt(1:i);
		y = dydt1(1:i);
		integral = trapz(x,y );
		WET_DEP = [WET_DEP; integral];
	end
	
	plot(xt,[WET_DEP; WET_DEP(end)],'--m','LineWidth',1,'MarkerSize',2);
	labels = { 'EXP WET' 'EXP DRY' 'EXP DRY MAX' 'EDF DRY MIN' 'WET MELCOR TOT' 'WET MELCOR RAD'  'DRY MELCOR TOT' 'DRY MELCOR RAD'  'WET TOTAL'}
else

	labels = { 'EXP WET' 'EXP DRY' 'EXP DRY MAX' 'EDF DRY MIN' 'WET MELCOR TOT' 'WET MELCOR RAD'  'DRY MELCOR TOT' 'DRY MELCOR RAD' }
end

	xt1 =  EDF_CONT_PARAMS(:,1);
    y111 = EDF_CONT_PARAMS(:,10); %ATML sump liquid
    y1111 = y111 + y2;
  
    plot(xt1,y111,'-m','LineWidth',1,'MarkerSize',2); % SUMP MASS
	plot(xt1,y1111,'--m','LineWidth',1,'MarkerSize',2); % SUMP MASS

	
labels = {labels{:} ,  'SUMP LIQUID' 'SUMP LIQUID + WET COND.' };


%y4 =  EDF_CONT_DEP(:,25); %ALL TOT
%y5 =  EDF_CONT_DEP(:,26); %ALL RAD
xlabel('Time, [s]'); ylabel('Mass, [kg]');
legend('Location','northwest');
grid on; xlim([0, 30000]); 
box on; % ylim([0 inf]);
set(gca, 'YScale', 'log')
ylim([1e-6 0.1]);
%labels = { 'Aero EXP' 'Aero TOT' 'Aero RAD' 'Vap TOT' 'Vap RAD' 'Aero Sump TOT' 'Aero Sump RAD' 'Vap Sump TOT' 'Vap Sump RAD' 'Released TOT' 'Released RAD'};

legend(labels);
annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');
if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end


% ===================================================================
% Deposition Mass - Wet Condeser
% ===================================================================

labels = {};
a=a+1;
figure(a)
hold on; 
set(0, 'DefaulttextInterpreter', 'none');
title('Wet Condenser Deposited Mass');
name = ['Fig', num2str(a)];
plot(x1,y1,'ko','LineWidth',1,'MarkerSize',6); labels = [labels, 'EXP WET COND.'];
plot(xt,y2,'-r','LineWidth',1,'MarkerSize',2); labels = [labels, 'WET MELCOR TOT'];
plot(xt,y3,'--r','LineWidth',1,'MarkerSize',2); labels = [labels,  'WET MELCOR RAD'];

if flagWET == 1
	plot(xt,[WET_DEP; WET_DEP(end)],'--m','LineWidth',1,'MarkerSize',2);	labels = [labels, 'WET TOTAL'];
end

xt1 =  EDF_CONT_PARAMS(:,1);
y111 = EDF_CONT_PARAMS(:,10); %ATML sump liquid
y1111 = y111 + y2;

plot(xt1,y111,'-m','LineWidth',1,'MarkerSize',2); labels = [labels, 'SUMP LIQUID']; % SUMP MASS
plot(xt1,y1111,'--m','LineWidth',1,'MarkerSize',2); labels = [labels, 'SUMP LIQUID + WET COND.']; % SUMP MASS
xlabel('Time, [s]'); ylabel('Mass, [kg]');
legend('Location','northwest');
grid on; xlim([0, 30000]); 
box on; % ylim([0 inf]);

legend(labels);
annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');
if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end

% ===================================================================
% Deposition Mass - Other Walls
% ===================================================================
	a=a+1
	figure(a)
	hold on; 
	set(0, 'DefaulttextInterpreter', 'none');
	title('Containment Deposited Mass');
	name = ['Fig', num2str(a)];
	
	%Wet Condenser
	x1 = EXP_AERO_DEP_WETCOND(:,1);
	y1 = EXP_AERO_DEP_WETCOND(:,3);
	plot(x1,y1,'ko','LineWidth',1,'MarkerSize',6);
	
	%Dry Condenser
	x01 = 30000.0;  y01 = 0.134142*1e-3;
	y02 =  0.0863085*1e-3; y03 = 0.181282*1e-3;
	plot(x01,y01,'kd','LineWidth',1,'MarkerSize',6);
	plot(x01,y02,'k>','LineWidth',1,'MarkerSize',6);
	plot(x01,y03,'k+','LineWidth',1,'MarkerSize',6);
	
	% Containment Walls
	x10 = EXP_AERO_DEP_CONTWALLS(:,1);
	y10 = EXP_AERO_DEP_CONTWALLS(:,3);
	% Cont. Floor
	x20 = EXP_AERO_DEP_CONTFLOOR(:,1);
	y20 = EXP_AERO_DEP_CONTFLOOR(:,3);
	
	plot(x10,y10,'ks','LineWidth',1,'MarkerSize',6);
	plot(x20,y20,'kx','LineWidth',1,'MarkerSize',6);
	
	labels = { 'EXP WET' 'EXP DRY' 'EXP DRY MIN' 'EXP DRY MAX' 'EXP WALLS' 'EXP FLOOR' }
	
	% MELCOR calculations
	xt  = EDF_CONT_DEP(:,1);
	y30 = EDF_CONT_DEP(:,3); %TOT WET 
	y31 = EDF_CONT_DEP(:,2); %TOT DRY
	y32 = EDF_CONT_DEP(:,8); %TOT CONT  SUMP HEMI_BOT
	y33 = EDF_CONT_DEP(:,9); %TOT CONT  SUMP SUMP WALLS
	y34 = EDF_CONT_DEP(:,10); %TOT CONT HEMI BOT 
	y35 = EDF_CONT_DEP(:,11); %TOT WALLS
	y36 = EDF_CONT_DEP(:,12); %TOT HEMI TOP
	y37 = EDF_CONT_DEP(:,13); %TOT TOP SLAB
	
	
	% WALLS
	y38 = y33 + y35;
	
	%FLOORS 
	%y39 = y32 + y34 + y36 + y37;  
	%y39 = y32 + y34  + y37;  
%FLOORS 
	y39 = y32 + y34 ;  
	
	%y31 = EDF_CONT_DEP(:,15); %RAD WET 
	%y33 = EDF_CONT_DEP(:,14); %RAD DRY
	k=1;
	plot(xt,y30,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1;
	plot(xt,y31,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1;
	plot(xt,y32,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1;
	plot(xt,y33,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1;
	plot(xt,y34,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1;
	plot(xt,y35,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1;
	plot(xt,y36,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1;
	plot(xt,y37,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1;
	
	
	plot(xt,y38,'--','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1; % TOTAL WALLS
	plot(xt,y39,'--','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1; % TOTAL FLOORS
	
	
	labels = {labels{:} ,  'WET MELCOR TOT',  'DRY MELCOR TOT' 'SUMP HEMI BOT TOT' 'SUMP WALLS TOT' 'CONT HEMI BOT TOT' 'WALLS TOT' 'HEMI TOP TOT' 'TOP SLAB TOT'};
	labels = {labels{:} ,  'TOTAL WALLS',  'TOTAL FLOORS' };
	
	% ==== SUMP
	
		xt1 =  EDF_CONT_PARAMS(:,1);
		y111 = EDF_CONT_PARAMS(:,10); %ATML sump liquid
	
	
	%plot(xt1,y111,'-.','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1; % SUMP MASS
	plot(xt1,y111,'-m','LineWidth',1,'MarkerSize',2);; % SUMP MASS
	
	
	labels = {labels{:} ,  'SUMP LIQUID' };
	
	set(gca, 'YScale', 'log')
	ylim([1e-6 0.1]);
	%y4 =  EDF_CONT_DEP(:,25); %ALL TOT
	%y5 =  EDF_CONT_DEP(:,26); %ALL RAD
	xlabel('Time, [s]'); ylabel('Mass, [kg]');
	
	legend('Location','southeast'); 
	grid on; xlim([0, 30000]); box on; 
	
	
	%labels = { 'Aero EXP' 'Aero TOT' 'Aero RAD' 'Vap TOT' 'Vap RAD' 'Aero Sump TOT' 'Aero Sump RAD' 'Vap Sump TOT' 'Vap Sump RAD' 'Released TOT' 'Released RAD'};
	%labels = { 'EXP WET' 'EXP DRY' 'EXP DRY MAX' 'EDF DRY MIN' 'WET MELCOR TOT' 'WET MELCOR RAD'  }
	%  }
	
	
	legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');
	
	if (flag2 == 1)
		print('-dtiff',[case1,'/', name, code1, '.tiff'])
		savefig([case1,'/', name, code1, '.fig'])
		%saveas(gcf,[Fig1.fig'],'fig');
	end
	
	
	
% ===================================================================
% Deposition Mass - Dry Condenser
% ===================================================================
	a=a+1
	figure(a)
	hold on; 
	set(0, 'DefaulttextInterpreter', 'none');
	title('Dry Condenser Deposited Mass');
	name = ['Fig', num2str(a)];
		
	%Dry Condenser
	x01 = 30000.0;  y01 = 0.134142*1e-3;
	y02 =  0.0863085*1e-3; y03 = 0.181282*1e-3;
	plot(x01,y01,'kd','LineWidth',1,'MarkerSize',6);
	plot(x01,y02,'k>','LineWidth',1,'MarkerSize',6);
	plot(x01,y03,'k+','LineWidth',1,'MarkerSize',6);

	
	labels = { 'EXP DRY' 'EXP DRY MIN' 'EXP DRY MAX' 'EDF DRY'}
	
	% MELCOR calculations
	xt  = EDF_CONT_DEP(:,1);
	y31 = EDF_CONT_DEP(:,2); %TOT DRY
	
	plot(xt,y31,'-r','LineWidth',1,'MarkerSize',2);
	labels = {labels{:} ,  'DRY MELCOR TOT' };
	
	xlabel('Time, [s]'); ylabel('Mass, [kg]');
	
	legend('Location','northwest'); 
	grid on; xlim([0, 30000]); box on; 
	
	
	legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');
	
	if (flag2 == 1)
		print('-dtiff',[case1,'/', name, code1, '.tiff'])
		savefig([case1,'/', name, code1, '.fig'])
		%saveas(gcf,[Fig1.fig'],'fig');
	end
	

% ===================================================================
% Deposition Mass -Floor
% ===================================================================
	a=a+1
	figure(a)
	hold on; 
	set(0, 'DefaulttextInterpreter', 'none');
	title('Containment Floor Deposited Mass');
	name = ['Fig', num2str(a)];
	
	

	% Cont. Floor
	x20 = EXP_AERO_DEP_CONTFLOOR(:,1);
	y20 = EXP_AERO_DEP_CONTFLOOR(:,3);
	

	plot(x20,y20,'kx','LineWidth',1,'MarkerSize',6);
	
	labels = {  'EXP FLOOR' }
	
	% MELCOR calculations
	xt  = EDF_CONT_DEP(:,1);

	y32 = EDF_CONT_DEP(:,8); %TOT CONT  SUMP HEMI_BOT
	%y33 = EDF_CONT_DEP(:,9); %TOT CONT  SUMP SUMP WALLS
	y34 = EDF_CONT_DEP(:,10); %TOT CONT HEMI BOT 
	y35 = EDF_CONT_DEP(:,11); %TOT WALLS
	y36 = EDF_CONT_DEP(:,12); %TOT HEMI TOP
	y37 = EDF_CONT_DEP(:,13); %TOT TOP SLAB
	
	

	%FLOORS 
	y39 = y32 + y34 ;  
	

	k=1;
	plot(xt,y32,'-b','LineWidth',1,'MarkerSize',2);
	%plot(xt,y33,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1;
	plot(xt,y34,'-r','LineWidth',1,'MarkerSize',2);
	%plot(xt,y35,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1;
	%plot(xt,y36,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+1;
	%plot(xt,y37,'-','LineWidth',1,'MarkerSize',2,'Color',cmap11(k,:)); k=k+2;
	
	plot(xt,y39,'--k','LineWidth',1,'MarkerSize',2); %TOTAL
	
	
	labels = {labels{:} ,  'SUMP HEMI BOT TOT'  'CONT HEMI BOT TOT'  };
	labels = {labels{:} ,   'TOTAL FLOORS' };
	

	
	%set(gca, 'YScale', 'log')
	%ylim([1e-6 0.1]);
	%y4 =  EDF_CONT_DEP(:,25); %ALL TOT
	%y5 =  EDF_CONT_DEP(:,26); %ALL RAD
	xlabel('Time, [s]'); ylabel('Mass, [kg]');
	
	legend('Location','southeast'); 
	grid on; xlim([0, 30000]); box on; 
	
	
	%labels = { 'Aero EXP' 'Aero TOT' 'Aero RAD' 'Vap TOT' 'Vap RAD' 'Aero Sump TOT' 'Aero Sump RAD' 'Vap Sump TOT' 'Vap Sump RAD' 'Released TOT' 'Released RAD'};
	%labels = { 'EXP WET' 'EXP DRY' 'EXP DRY MAX' 'EDF DRY MIN' 'WET MELCOR TOT' 'WET MELCOR RAD'  }
	%  }
	
	
	legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');
	
	if (flag2 == 1)
		print('-dtiff',[case1,'/', name, code1, '.tiff'])
		savefig([case1,'/', name, code1, '.fig'])
		%saveas(gcf,[Fig1.fig'],'fig');
	end
	
	
	

% ===================================================================
% Deposition Mass - WALLS
% ===================================================================
	a=a+1
	figure(a)
	hold on; 
	set(0, 'DefaulttextInterpreter', 'none');
	title('Containment Walls and Ceiling Deposited Mass');
	name = ['Fig', num2str(a)];
	
	% Containment Walls
	x10 = EXP_AERO_DEP_CONTWALLS(:,1);
	y10 = EXP_AERO_DEP_CONTWALLS(:,3);

	plot(x10,y10,'ks','LineWidth',1,'MarkerSize',6);
	labels = {  'EXP WALLS'  }
	
	% MELCOR calculations
	xt  = EDF_CONT_DEP(:,1);
	y33 = EDF_CONT_DEP(:,9); %TOT CONT  SUMP SUMP WALLS
	y35 = EDF_CONT_DEP(:,11); %TOT WALLS
	y36 = EDF_CONT_DEP(:,12); %TOT HEMI TOP
	y37 = EDF_CONT_DEP(:,13); %TOT TOP SLAB
		
	
	y38 = y33 + y35; % WALLS
	y41 = y36+y37; %TOTAL CEILING
	y50 = y41+y38; %CEILING PLUS WALL
	

	plot(xt,y41,'r-','LineWidth',1,'MarkerSize',2); %TOTAL CEILINGS
	plot(xt,y38,'b-','LineWidth',1,'MarkerSize',2); %TOTAL WALLS
	plot(xt,y50,'--k','LineWidth',1,'MarkerSize',2); %TOTAL WALLS + CEILING
	
	labels = {labels{:} , 'TOTCAL CEILING' 'TOTAL WALLS' 'CEILING+WALLS'   };

	xlabel('Time, [s]'); ylabel('Mass, [kg]');
	legend('Location','northwest'); 
	grid on; xlim([0, 30000]); box on; 
	legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');
	
	if (flag2 == 1)
		print('-dtiff',[case1,'/', name, code1, '.tiff'])
		savefig([case1,'/', name, code1, '.fig'])
		%saveas(gcf,[Fig1.fig'],'fig');
	end
	
	
% ===================================================================
% FIGURE Aersolol Proproties MMD
% ===================================================================
a=a+1
figure(a)
hold on; set(0, 'DefaulttextInterpreter', 'none');
title('Aerosol Mass Median Diameter');
name = ['Fig', num2str(a)];

%x1 = EXP_SN(:,1);
%y1 = EXP_SN(:,2)./100;

%plot(x1,y1,'ko','LineWidth',1,'MarkerSize',2);

%x3 = EDF_RELEASE(:,1);
%y3 = EDF_RELEASE(:,34)./INVENTORY_STR(3);  %From Control Rods
%y2 = 0.0;
%y2 = EDF_RELEASE(:,13)./INVENTORY();     %From Fuel

%plot(x3,y3,'-r','LineWidth',1,'MarkerSize',2);
%plot(x3,y2,'-b','LineWidth',1,'MarkerSize',2);
%plot(x3,y2+y3,'-k','LineWidth',1,'MarkerSize',2);

x1 = EXP_AMMD(:,1);
y1 = EXP_AMMD(:,2);

plot(x1,y1,'ok','LineWidth',1,'MarkerSize',2);

x=EDF_CONT_PARAMS(:,1);
MMDW = EDF_CONT_PARAMS(:,2);
MMDD = EDF_CONT_PARAMS(:,4);

plot(x,MMDW,'-r','LineWidth',1,'MarkerSize',1);
plot(x,MMDD,'--b','LineWidth',1,'MarkerSize',1);

set(gca, 'YScale', 'log')

xlabel('Time, [s]');
ylabel('Value, [m]');
legend('Location','northwest');
grid on; xlim([0, 30000]); xlim([0, 30000]);
box on; % ylim([0 inf]);

labels = {'EXP AMMD' 'MMDW' 'MMDD'};
legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end
xlim([0, 30000]); 






% ===================================================================
% FIGURE Aersolol Proproties
% ===================================================================
a=a+1
figure(a)
hold on; set(0, 'DefaulttextInterpreter', 'none');
title('Geometric Standard Deviation');
name = ['Fig', num2str(a)];

%x1 = EXP_SN(:,1);
%y1 = EXP_SN(:,2)./100;

%plot(x1,y1,'ko','LineWidth',1,'MarkerSize',2);

%x3 = EDF_RELEASE(:,1);
%y3 = EDF_RELEASE(:,34)./INVENTORY_STR(3);  %From Control Rods
%y2 = 0.0;
%y2 = EDF_RELEASE(:,13)./INVENTORY();     %From Fuel

%plot(x3,y3,'-r','LineWidth',1,'MarkerSize',2);
%plot(x3,y2,'-b','LineWidth',1,'MarkerSize',2);
%plot(x3,y2+y3,'-k','LineWidth',1,'MarkerSize',2);

x1 = EXP_GSD(:,1);
y1 = EXP_GSD(:,2);

plot(x1,y1,'ok','LineWidth',1,'MarkerSize',2);

x=EDF_CONT_PARAMS(:,1);
GSDW = EDF_CONT_PARAMS(:,3);
GSDD = EDF_CONT_PARAMS(:,5);

plot(x,GSDW,'-r','LineWidth',1,'MarkerSize',1);
plot(x,GSDD,'--b','LineWidth',1,'MarkerSize',1);

%set(gca, 'YScale', 'log')

xlabel('Time, [s]');
ylabel('Value, [-]');
legend('Location','northwest');
grid on; xlim([0, 30000]); ylim([0, 10])
box on; % ylim([0 inf]);

labels = { 'EXP GSD' 'GSDW' 'GSDD'};
legend(labels); annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none','Interpreter','none');

if (flag2 == 1)
	print('-dtiff',[case1,'/', name, code1, '.tiff'])
    savefig([case1,'/', name, code1, '.fig'])
    %saveas(gcf,[Fig1.fig'],'fig');
end
xlim([0, 30000]); 






