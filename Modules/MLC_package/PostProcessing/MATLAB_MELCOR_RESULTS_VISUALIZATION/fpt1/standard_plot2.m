%set(0, 'DefaulttextInterpreter', 'none'); 

% set(gca, 'Color',cmapdyn(k_num,:));

set(0, 'DefaulttextInterpreter', 'none'); 
 l = legend(labels); 
set(l, 'Interpreter','none');
 l.FontSize = 9;

legend(labels); 

% New - set color of the curve
%if (exist('k_num'))

%else
%end



if k_num == numel(cases)
    annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',cases{1},'FitBoxToText','off','EdgeColor','none','Interpreter','none');
end

if k_num == numel(cases) 
    if (flag2 == 1)
        
        if flag3 == 0
            %print('-dtiff',[case1,'/', name, code1, '.tiff']);
            print('-dtiff',[cases{1},'/', name,'_', code1, '.tiff']);
            %savefig([case1,'/', name, code1, '.fig']);
            savefig([cases{1},'/', name, '_', code1, '.fig']);
        end
        
        if flag3 == 1
            %folder_name = ['COMPARISON_' cases_names{:}];
            folder_name = ['COMPARISON_' cases{:}];
            mkdir(folder_name);

            print('-dtiff','-r300',[folder_name,'/', name,'_', code1, '.tiff']);
            savefig([folder_name,'/', name, '_', code1, '.fig']);
        end
            


    end
end


hold on;



