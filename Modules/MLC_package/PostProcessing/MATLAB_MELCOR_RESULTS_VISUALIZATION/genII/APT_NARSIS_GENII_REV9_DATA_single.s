#-------- -- ._ --.. --. .- .--- --------
# Code: MELCOR
#-------- -- ._ --.. --. .- .--- --------

MELCOR	0	ptf		"E:\DARO\MELCOR\ROOT\NARSIS-GenII\FINAL_Rev1\CLL#1_annulus\PWR.PTF"
# #############################################################################
# FINAL RESULTS FOR NARSIS
# SG WIDE LEVEL 6,7 IN METERS, BROKEN INTACT
 MREAD 0 "CFVALU_2006"
 MREAD 0 "CFVALU_2007"
# PRZ PRESSURE
 MREAD 0 "CVH-P_400"
#  CORE EXIT TEMPERATURE
# CAUTION:  FOR MANY SCENARIOS 2499K IS NOT REACHED!!! 
 MREAD 0 "COR-TSVC_117"  
 
# TO BE EXCHANGED RPV WATER LEVEL 'RPV_LIQLEV'   2023    "CFVALU_2023"
 MREAD 0 "CFVALU_2023"
# MREAD 0 "CVH-LIQLEV_320"
# CONTAINMENT PRESSURE
 MREAD 0 "CVH-P_900"
#CONTAINMENT TEMPERATURE
 MREAD 0 "CVH-TVAP_900"
# RWST RELATIVE WATER LEVEL 
# absolute 2.146m	- 2.146m = relative level 0m is 0%
# absolute 17.236	- 2.146m = relative level 15.0900m is top of the tank
# Nominal water level relative 14.810548 m corresponda to 100% water mass
# ELEVATION-VOLUME TABLE FOR THE RWST:
#2.146	0.0
#17.236	1350.0
# INITIAL WATER VOLUME IS 1325m3 AND IT CORRESSPOND TO ABOUT 14.81m (relative)
 MREAD 0 "CFVALU_982"
# H2 CONCTRATION IN CONT 
 MREAD 0 "CVH-X.6_900"
# WATER LEVEL IN SUMP 
 MREAD 0 "CFVALU_992"
#OLD: MREAD 0 "CVH-LIQLEV_909"
# IT IS RELATIVE LEVEL. LEVEL 0 is bottom of sump 
# BOTTOM OF THE SUMP IS -4.744m (aboslute).
# ELEVATION-VOLUME TABLE FOR THE SUMP:
#-4.744	0.0
#-2.114	37.97
# #############################################################################


MELCOR	0	EXPORT ASCII	 ".\FINAL_Rev1\CLL#1_annulus\DATA_NARSIS_Rev4.apt"

# #############################################################################