% ===================================================================
% FIGURE - Containment Pressures
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
 %
	content = {'Containment Pressure'};
	run standard_plot1v4.m;
	mlcfile = EDF.file{2};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,2); %
		%	y2 = EDF.data{k}(:,3); %H2O
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:));  labels = [labels, [cases_list_names1{k_num}] 'Total'];
		%plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'part. H2O']  ];
	end
		ylabel(labelP); xlabel(labelTime);
		xlim([0 plottime]); ylim([0 inf]);
		legend('Location','southeast');
	run standard_plot2v4.m
end
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
 %
	content = {'Containment H2O Partial Pressure'};
	run standard_plot1v4.m;
	mlcfile = EDF.file{2};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		%y1 = EDF.data{k}(:,2); %
			y2 = EDF.data{k}(:,3); %H2O
		%plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:));  labels = [labels, [cases_list_names1{k_num}] 'Total'];
		plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'part. H2O']  ];
	end
		ylabel(labelP); xlabel(labelTime);
		xlim([0 plottime]); ylim([0 inf]);
		legend('Location','southeast');
	run standard_plot2v4.m
end

% ===================================================================
% FIGURE - Containment Pressures
% ===================================================================
% Where CVH.X.<number>:  X= 3 – H2O vap.;  4 – N2;  5 – O2;  6 – H2; 7 – CO2; 8 – CO
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
	content = {'Containment Partial Pressures'};
	run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{2};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			y1 = EDF.data{k}(:,6); %H2
		%	y2 = EDF.data{k}(:,3); %H2O
			y3 = EDF.data{k}(:,8); %CO
			y4 = EDF.data{k}(:,7); %CO2
		
			plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num}   'H2']];
		%	plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'H2O']  ];
			plot(x0,y3,lw,1,ms,2,cl,col2(k_num,:),ls,'-.');  labels = [labels, [cases_list_names1{k_num}  'CO'] ];
			plot(x0,y4,lw,1,ms,2,cl,col2(k_num,:),ls,':');  labels = [labels, [cases_list_names1{k_num}  'CO2'] ];			
		end
	end	
		ylabel(labelP); xlabel(labelTime);
		legend('Location','northwest');
		legend('Location','southeast');
		xlim([0 plottime]); ylim([0 inf]);	
	run standard_plot2v4.m
end
% ===================================================================
% FIGURE - Containment Temp.
% ===================================================================	
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
content = {'Containment Temperature'};
	run standard_plot1v4.m;
	mlcfile = EDF.file{2};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,9); %tvap
		y2 = EDF.data{k}(:,10); %sat
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'T']];
		plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'Sat.'] ];
	end
		ylabel(labelT); xlabel(labelTime);
		legend('Location','northwest');
		legend('Location','southeast');
		xlim([0 plottime]);
	run standard_plot2v4.m
end
% ===================================================================
% FIGURE - Containment
% ===================================================================	
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
content = {'Containment Dome H2 Conc.'};
	run standard_plot1v4.m;
	mlcfile = EDF.file{2};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,17); %H2
		%y1 = EDF.data{k}(:,16); %O2
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'H2'] ];
		%plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'O2' ] ];
	end
		ylabel('Concentration, [-]');  xlabel(labelTime);  %xlim([0 60]); %ylim([0 0.15])
		legend('Location','southeast');
		xlim([0 plottime]);
		ylim([0 inf]);
	run standard_plot2v4.m
end
% ===================================================================
% FIGURE - Containment
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit	
	content = {'Containment Dome O2 Conc.'};
	run standard_plot1v4.m;
	mlcfile = EDF.file{2};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		%y1 = EDF.data{k}(:,17); %H2
		y1 = EDF.data{k}(:,16); %O2
		%plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'H2'] ];
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'O2' ] ];
	end
		ylabel('Concentration, [-]'); xlabel(labelTime); %xlim([-1 60]); %ylim([0 ])
		legend('Location','southeast');
		xlim([0 plottime]);	
		ylim([0 inf]);
	run standard_plot2v4.m
end


% ===================================================================
% WATER mass in SUMP, CAV, RWST
% ===================================================================	
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
	content = {'Water Mass in Cavity and Sump'};
	run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{18};
		
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			y1 = EDF.data{k}(:,3); %sump
			%y2 = EDF.data{k}(:,8); %rwst
			y3 = EDF.data{k}(:,13); %cav
			
			%plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:));  		 labels = [labels, [cases_list_names1{k_num} 'Cavity']];
			plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'Sump']];
			%plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'RWST']];
			plot(x0,y3,lw,1,ms,2,cl,col2(k_num,:),ls,'-.');  labels = [labels, [cases_list_names1{k_num} 'Cavity']];
		end
	end
		ylabel('Mass, [kg]'); xlabel(labelTime); 
		legend('Location','northeast');
		xlim([0 longtime]);
	run standard_plot2v4.m
end
% ===================================================================	
% WATER levels in SUMP, CAV, RWST
% ===================================================================	
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
	content = {'Level in Cavity and Sump'};
	run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{18};
		
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			y1 = EDF.data{k}(:,2); %sump
			%y2 = EDF.data{k}(:,7); %rwst
			y3 = EDF.data{k}(:,12); %cav
			
			plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'Sump']];
			plot(x0,y3,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'Cavity']];
						
		end
	end
	
		if flag3 == 1
		 if k_num == numel(cases)	
			yline( -4.7440, '-' )	%Sump
			yline( -2.1140, '-')
			yline(  -3.69400 , '--' )
			yline( 1.5960, '--')
			
		 end			
	   else
			yline( -4.7440, '-' )
			yline( -2.1140, '-')
			yline(  -3.69400 , '--' )
			yline( 1.5960, '--')
	   end

	
		ylabel('Level, [m]'); xlabel(labelTime); 
		legend('Location','northeast');
		xlim([0 longtime]);
	run standard_plot2v4.m
end
% ===================================================================	
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
	content = {'Level in RWST'};
	run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{18};
		
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			y2 = EDF.data{k}(:,7); %rwst
			plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'RWST']];
		end
	end
	
	if flag3 == 1
		if k_num == numel(cases)	
			yline( 2.146 )
			yline(  17.236)
		end			
	else
			yline( 2.146 )
			yline(  17.236)
	end
	
		ylabel('Level, [m]'); xlabel(labelTime); 
		legend('Location','northeast');
		xlim([0 longtime]);
	run standard_plot2v4.m
end
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
	content = {'Water Mass in RWST'};
	run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{18};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			y2 = EDF.data{k}(:,8); %rwst
			plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'RWST']];
		end
	end
		ylabel('Mass, [kg]'); xlabel(labelTime); 
		legend('Location','northeast');
		xlim([0 longtime]);
	run standard_plot2v4.m
end
% ===================================================================	

% SUMP
% 1         -4.7440           0.0			
% 2         -2.1140         37.97			
% RWST
% 1        2.146      0.0				!! WUT  ASSMED THAT RWST BOTTOM LEVEL IS GROUND LEVEL, WAS 17.406
% 2        17.236      1350.0			!! WUT  ASSUMED 10m HEIGHT 
% CAVITY
% CAV
% 1         -3.69400           0.0   !! WUT
% 2         -2.11400          60.5   !! WUT
% 3         -1.55400          83.5   !! WUT
% 4         -0.15400         128.0   !! WUT
% 5          1.29600         160.4   !! WUT orignal
% 6		   1.5960		  164.41   ! 160.4 + 4.0100,  Added to cover whole lower head - AVR reduced by the same amout of volume

	%	if flag3 == 1
	%	if k_num == numel(cases)	
	%		yline( -4.7440, '--' )	%Sump
	%		yline( -2.1140, '--')
	%		yline(  -3.69400 , '--' )
	%		yline( 1.5960, '--')
			
	%	end			
	%else
	%			yline( -4.7440, '--' )
	%		yline( -2.1140, '--')
		%		yline(  -3.69400 , '--' )
	%		yline( 1.5960, '--')
	%end

	
% ===================================================================
% PARS
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit	
	content = {'PARs H2 Removel Rate'};
	run standard_plot1v4.m;
	mlcfile = EDF.file{19};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,6);
		
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'DMH2' ] ];
	end
		ylabel('Rate, [kg/s]'); xlabel(labelTime); %xlim([-1 60]); %ylim([0 ])
		legend('Location','northeast');
		xlim([0 plottime]);	
	run standard_plot2v4.m
end
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit	
	content = {'PARs H2 Removed'};
	run standard_plot1v4.m;
	mlcfile = EDF.file{19};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,7);
		
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'IMH2' ] ];
	end
		ylabel(labelM); xlabel(labelTime); %xlim([-1 60]); %ylim([0 ])
		legend('Location','northwest');
		xlim([0 plottime]);	
	run standard_plot2v4.m
end
% ===================================================================
% FCL
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit	
	content = {'Fans Heat Removal Rate'};
	run standard_plot1v4.m;
	mlcfile = EDF.file{19};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,14);
		
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'QFC-RAT' ] ];
	end
		ylabel('Heat Rate, [W]'); xlabel(labelTime); %xlim([-1 60]); %ylim([0 ])
		legend('Location','northeast');
		xlim([0 plottime]);	
	run standard_plot2v4.m
end
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit	
	content = {'Fans Heat Removed'};
	run standard_plot1v4.m;
	mlcfile = EDF.file{19};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,15);
		
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'QFC-TOT' ] ];
	end
		ylabel('Heat, [J]'); xlabel(labelTime); %xlim([-1 60]); %ylim([0 ])
		legend('Location','northeast');
		xlim([0 plottime]);	
	run standard_plot2v4.m
end
% ===================================================================
% SPRAYS
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit	
	content = {'Spray Flow Rate'};
	run standard_plot1v4.m;
	mlcfile = EDF.file{19};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,8);
		y2 = EDF.data{k}(:,9);
		
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'Active' ] ];
		plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'Recirc' ] ];
		
	end
		ylabel('Flow Rate, [kg/s]'); xlabel(labelTime); %xlim([-1 60]); %ylim([0 ])
		legend('Location','northeast');
		xlim([0 plottime]);	
	run standard_plot2v4.m
end
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit	
	content = {'Spray Water Temperature'};
	run standard_plot1v4.m;
	mlcfile = EDF.file{19};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,10);
		y2 = EDF.data{k}(:,11);
		
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'Active' ] ];
		plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'Recirc' ] ];
		
	end
		ylabel(labelT); xlabel(labelTime); %xlim([-1 60]); %ylim([0 ])
		legend('Location','northeast');
		xlim([0 plottime]);	
	run standard_plot2v4.m
end
% ===================================================================








