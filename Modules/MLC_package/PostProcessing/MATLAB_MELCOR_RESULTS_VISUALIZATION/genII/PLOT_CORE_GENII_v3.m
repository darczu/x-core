% ===================================================================
% FIGURE - Decay Heat Shot term
% ===================================================================	
a = a+1; if (sum(plot_omit == (a)) == 0)  %plot omit
		content = {'Decay Heat Short Time'};
		run standard_plot1v4.m;
			if  (cases_output_type{k_num} == 1)
				mlcfile = EDF.file{1};
				k = detectfile(mlcfile,EDF.file);  %MELCOR
				
				if ~sum(k == emptyfile)
					%x0 = EDF.data{k}(:,1);        
					x0 = EDF.data{k}(:,1)./Tconst;        
					y1 = EDF.data{k}(:,2); %
					semilogy(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'EFPD' ] ];
				end		
			end	
		ylabel(labelPower); xlabel(labelTime); %xlabel('Time, [s]');  %ylim([0 150e6]); xlim([-50 3600]);	
		legend('Location','northeast');
		xlim([0 3600./Tconst]);
		run standard_plot2v4.m;
end
% ===================================================================
% FIGURE - Decay Heat Long term
% ===================================================================	

a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
		content = {'Decay Heat Long'};
		run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{1};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			y1 = EDF.data{k}(:,2); %
			semilogy(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'EFPD' ] ];
		end
		% DHR
		mlcfile = EDF.file{12};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			y2 = EDF.data{k}(:,2); %CAV0DHR
			semilogy(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'CAV'] ];
		end
	end	
		ylabel(labelPower); xlabel(labelTime);  % xlabel('Time, [h]');  %ylim([0 2.5e7]); %xlim([0 66]);	
		legend('Location','northeast');
		xlim([0 mediumtime]);
		run standard_plot2v4.m;
end	

% ===================================================================
% Core Water Level
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
		content = {'Core Water Level'};
		run standard_plot1v4.m;
	
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{14};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
% #  # CVH CORE BOT = 1.544
% #  # CVH CORE TOP = 6.58686
% #  # BAF =  2.567
% #  # TAF = 6.2246
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;          
			y1 = EDF.data{k}(:,15) - 1.544; %
			y2 = EDF.data{k}(:,16) - 1.544; %
			
			plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num}		'Swollen'] ];
			plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'-.');  labels = [labels, [cases_list_names1{k_num}		'Collapsed'] ];
		end
		
	end	
		ylabel('Level, [m]'); xlabel(labelTime);  %xlim([-20 200]); 
		%ylim([0.0 6.5686-1.544]);  
		ylim([0.0 6]);  
		%xlabel('Time, [s]'); 
		legend('Location','northeast');
	
	if flag3 == 1
		if k_num == numel(cases)	
			yline( 2.567- 1.544);
			yline( 6.2246-1.544);
		end			
	else
		yline( 2.567- 1.544);
		yline( 6.2246-1.544);
	end
	
		xlim([0 shorttime]);
		run standard_plot2v4.m;
end
% ===================================================================
% Core Water Level
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
		content = {'Core Water Level'};
		run standard_plot1v4.m;
	
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{14};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
% #  # CVH CORE BOT = 1.544
% #  # CVH CORE TOP = 6.58686
% #  # BAF =  2.567
% #  # TAF = 6.2246
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;          
			y1 = EDF.data{k}(:,15) - 1.544; %
			y2 = EDF.data{k}(:,16) - 1.544; %
			
			plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num}		'Swollen'] ];
			plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'-.');  labels = [labels, [cases_list_names1{k_num}		'Collapsed'] ];
		end
		
	end	
		ylabel('Level, [m]'); xlabel(labelTime);  %xlim([-20 200]); 
		%ylim([0.0 6.5686-1.544]);  
		ylim([0.0 6]);  
		%xlabel('Time, [s]'); 
		legend('Location','northeast');
	
	if flag3 == 1
		if k_num == numel(cases)	
			yline( 2.567- 1.544)
			yline( 6.2246-1.544)
		end			
	else
		yline( 2.567- 1.544)
		yline( 6.2246-1.544)
	end
	
		xlim([0 mediumtime]);
		run standard_plot2v4.m;
end
% ===================================================================
% Core Exit Temperature
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
		content = {'Core Exit Temperature'};
		run standard_plot1v4.m;
	
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{1};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;          
			%y1 = EDF.data{k}(:,21); 		
			y3 = EDF.data{k}(:, 21); %TSVC 117 
			%y4 = EDF.data{k}(:, 22); %TSVC 108
			
			%plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num}		'TSVC_117'] ];
			
			plot(x0,y3,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'TSVC_117'] ];
			%plot(x0,y4,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'TSVC_108'] ];
			
		end	
	end	
		ylabel(labelT); xlabel(labelTime);  
		legend('Location','northeast');
	if flag3 == 1
		if k_num == numel(cases)	
			yline(2499);
			yline(650+273);
		end			
	else
			yline(2499);
			yline(650+273);
	end
	xlim([0 mediumtime]);
		run standard_plot2v4.m;
end
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
		content = {'Core Exit Temperature'};
		run standard_plot1v4.m;
	
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{1};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;          
			%y1 = EDF.data{k}(:,21); 		
			%y3 = EDF.data{k}(:, 21); %TSVC 117 
			y4 = EDF.data{k}(:, 22); %TSVC 108
			
			%plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num}		'TSVC_117'] ];
			
			%plot(x0,y3,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'TSVC_117'] ];
			plot(x0,y4,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'TSVC_108'] ];
			
		end	
	end	
		ylabel(labelT); xlabel(labelTime);  
		legend('Location','northeast');
	if flag3 == 1
		if k_num == numel(cases)	
			yline(2499);
			yline(650+273);
		end			
	else
			yline(2499);
			yline(650+273);
	end
	xlim([0 mediumtime]);
		run standard_plot2v4.m;
end



% ===================================================================
% Core masses
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
	content = {'Lower Plenum Mass'};
	run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile  ='DATA_LP_MASS_CRP.apt'; k = detectfile(mlcfile,EDF.file);       %MELCOR file	
			if ~sum(k == emptyfile)
			x111 = EDF.data{k}(:,1)./Tconst;
			y10 = sum((EDF.data{k}(:,2:end)),2);  
			end
		%mlcfile  ='DATA_LP_MASS_INC.apt'; k = detectfile(mlcfile,EDF.file);       %MELCOR file
		%	if ~sum(k == emptyfile)
		%	y11 = sum((EDF.data{k}(:,2:end)),2);  
		%	end
		mlcfile  ='DATA_LP_MASS_SS.apt'; k = detectfile(mlcfile,EDF.file);       %MELCOR file		
			if ~sum(k == emptyfile)
			y12 = sum((EDF.data{k}(:,2:end)),2);  
			end
		mlcfile  ='DATA_LP_MASS_SSOX.apt'; k = detectfile(mlcfile,EDF.file);       %MELCOR file		
			if ~sum(k == emptyfile)
			y13 = sum((EDF.data{k}(:,2:end)),2);  
			end
		mlcfile  ='DATA_LP_MASS_UO2.apt'; k = detectfile(mlcfile,EDF.file);       %MELCOR file		
			if ~sum(k == emptyfile)
			y14 = sum((EDF.data{k}(:,2:end)),2);   
			end
		mlcfile  ='DATA_LP_MASS_ZR.apt'; k = detectfile(mlcfile,EDF.file);       %MELCOR file		
			if ~sum(k == emptyfile)
			y15 = sum((EDF.data{k}(:,2:end)),2);    
			end
		mlcfile  ='DATA_LP_MASS_ZRO2.apt'; k = detectfile(mlcfile,EDF.file);       %MELCOR file		
			if ~sum(k == emptyfile)
			y16 = sum((EDF.data{k}(:,2:end)),2);  
			end
		y111 = [];
		%y111 = y10 + y11 + y12+y13+y14+y15+y16; 
		y111 = y10  + y12+y13+y14+y15+y16; 
			
		plot(x111,y111,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'LP_MASS'	]];
	end
	ylabel(labelM); xlabel(labelTime); 	%xlabel('Time, [s]');
	xlim([0 mediumtime]);
	legend('Location','northeast'); 
	run standard_plot2v4.m;
end