% ===================================================================
% FIGURE - Containment 
% ===================================================================	
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
	content = {'Combustible Gases Mass'};
	run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{2};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			y1 = EDF.data{k}(:,12); %H2 EX
			y2 = EDF.data{k}(:,13); %CO EX
			y3 = EDF.data{k}(:,11); %H2 IN
			plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');   labels = [labels, [cases_list_names1{k_num} 'EV_H2']];
			plot(x0,y3,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'IV_H2']];
		end
			ylabel('H2 Mass, [kg]');  xlabel(labelTime);
	end
		legend('Location','northeast');
		xlim([0 plottime]);
	run standard_plot2v4.m
end
% ===================================================================
% FIGURE - All gasses production
% ===================================================================	
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
content = {'MCCI Gas Mass Generation'};
	run standard_plot1v4.m;

if  (cases_output_type{k_num} == 1)
	mlcfile = EDF.file{12};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,5); %H2 EX
		y2 = EDF.data{k}(:,6); %CO EX
		y3 = EDF.data{k}(:,7); %CO2 EX
		y4 = EDF.data{k}(:,8); %H2O Ex
		
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  		 labels = [labels, [cases_list_names1{k_num} 'H2']];
		plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'-.');  labels = [labels, [cases_list_names1{k_num} 'CO']];
		plot(x0,y3,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'CO2']];
		plot(x0,y4,lw,1,ms,2,cl,col2(k_num,:),ls,':');  labels = [labels, [cases_list_names1{k_num} 'H2O']];
	end
end
		ylabel(labelM);  xlabel(labelTime);		
		legend('Location','northwest');
		xlim([0 plottime]);
	run standard_plot2v4.m
end

% ===================================================================	
% Corium Temperature
% ===================================================================	
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
content = {'Ex-Vessel Corium Temperature'};
	run standard_plot1v4.m;

if  (cases_output_type{k_num} == 1)
	mlcfile = EDF.file{12};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,11); %
		y2 = EDF.data{k}(:,12); %
		y3 = EDF.data{k}(:,13); %
		y4 = EDF.data{k}(:,14); %
		y5 = EDF.data{k}(:,15); %
		y6 = EDF.data{k}(:,16); %
		
	% # Temperatures 11,12,13,14,15,16
	% MREAD 0 "CAV-T-DEB_10"
	% MREAD 0 "CAV-T-HMX_10"
	% MREAD 0 "CAV-T-HOX_10"
	% MREAD 0 "CAV-T-LMX_10"
	% MREAD 0 "CAV-T-LOX_10"
	% MREAD 0 "CAV-T-MET_10"
		
		plot(x0,y1,lw,1,ms,1,cl,col2(k_num,:),ls,'-');   labels = [labels, [cases_list_names1{k_num} 'DEB']];
		plot(x0,y2,lw,1,ms,1,cl,col2(k_num,:),ls,'-.');  labels = [labels, [cases_list_names1{k_num} 'HMX']];
		plot(x0,y3,lw,1,ms,1,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'HOX']];
		plot(x0,y4,lw,1,ms,1,cl,col2(k_num,:),ls,'-' , msym,'o');   labels = [labels, [cases_list_names1{k_num} 'LMX']];
		plot(x0,y5,lw,1,ms,1,cl,col2(k_num,:),ls,'-.', msym,'<');  labels = [labels, [cases_list_names1{k_num} 'LOX']];
		plot(x0,y6,lw,1,ms,1,cl,col2(k_num,:),ls,'--', msym,'x');  labels = [labels, [cases_list_names1{k_num} 'MET']];
		
	end
end
		ylabel(labelT);  xlabel(labelTime);		
		legend('Location','northeast');
		xlim([0 plottime]);
	run standard_plot2v4.m
end


% ===================================================================	
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
content = {'Ex-Vessel Corium Mass'};
	run standard_plot1v4.m;

if  (cases_output_type{k_num} == 1)
	mlcfile = EDF.file{12};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,23); % Corium total Mass
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');   labels = [labels, [cases_list_names1{k_num} 'MTOT']];
	end
end
		ylabel(labelM);  xlabel(labelTime);		
		legend('Location','northwest');
		xlim([0 plottime]);
	run standard_plot2v4.m
end


% ===================================================================	
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
content = {'Maximum Radial Concrete Diemnsion'};
	run standard_plot1v4.m;

if  (cases_output_type{k_num} == 1)
	mlcfile = EDF.file{12};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
	%	y1 = EDF.data{k}(:,9); % MINALT
		y1 = EDF.data{k}(:,10); % MAXRAD
	%	plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');   labels = [labels, [cases_list_names1{k_num} 'MINALT']];
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');   labels = [labels, [cases_list_names1{k_num} 'MAXRAD']];
	end
end



	if flag3 == 1
		if k_num == numel(cases)	
			yline(3.49)
			yline(7.49)
		end			
	else
			yline(3.49)
			yline(7.49)
	end

		ylabel(labelDistance);  xlabel(labelTime);		
		legend('Location','northwest');
		xlim([0 plottime]);
		ylim([3 8])
	run standard_plot2v4.m
end

% ===================================================================	
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
content = {'Minimum Axial Concrete Depth'};
	run standard_plot1v4.m;

if  (cases_output_type{k_num} == 1)
	mlcfile = EDF.file{12};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,9); % MINALT
		%y1 = EDF.data{k}(:,10); % MAXRAD
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');   labels = [labels, [cases_list_names1{k_num} 'MINALT']];
		%plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'--');   labels = [labels, [cases_list_names1{k_num} 'MAXRAD']];
	end
end



	if flag3 == 1
		if k_num == numel(cases)	
			yline(4.99)
			yline(4.99+5.224)
		end			
	else
		yline(4.99)
		yline(4.99+5.224)
	end
		ylabel(labelDistance);  xlabel(labelTime);		
		legend('Location','northwest');
		xlim([0 plottime]);
			ylim([4 11])
	run standard_plot2v4.m
end






%===================================================================
% FIGURE - Containment
% ===================================================================
if(flag_additional1 == 1)
	a=a+1; 
	if (sum(plot_omit == (a)) == 0)%plot omit
		content = {'Gases Generation'};
		run standard_plot1v4.m;
		%ax(1) = axes('yaxislocation','left','color','none'); 
		%ax(2) = axes('yaxislocation','right','color','none'); 
		if  (cases_output_type{k_num} == 1)
			mlcfile = EDF.file{2};
			k = detectfile(mlcfile,EDF.file);  %MELCOR
			if ~sum(k == emptyfile)
				x00 = EDF.data{k}(:,1)./Tconst;        
				y11 = EDF.data{k}(:,11); %H2 IN
				yyaxis left
				p1=plot(x00,y11,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels1 = [labels1, [cases_list_names1{k_num} 'IV_H2']];
			end
			mlcfile = EDF.file{12};
			k = detectfile(mlcfile,EDF.file);  %MELCOR
			
			if ~sum(k == emptyfile)
				x0 = EDF.data{k}(:,1)./Tconst;        
				y1 = EDF.data{k}(:,5); %H2 EX
				y2 = EDF.data{k}(:,6); %CO EX
				y3 = EDF.data{k}(:,7); %CO2 EX
				y4 = EDF.data{k}(:,8); %H2O Ex
				yyaxis left
				p2=plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');    labels1 = [labels1, [cases_list_names1{k_num} 'EX_H2']];
				yyaxis right
				p3=plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'-.');  labels2 = [labels2, [cases_list_names1{k_num} 'CO']];
				p4=plot(x0,y3,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels2 = [labels2, [cases_list_names1{k_num} 'CO2']];
				p5=plot(x0,y4,lw,1,ms,2,cl,col2(k_num,:),ls,':');  labels2 = [labels2, [cases_list_names1{k_num} 'H2O']];
			end
			%labels = [labels1; labels2]
			yyaxis left
			ylabel('Hydrogen Mass, [kg]'); 
			yyaxis right
			ylabel('Gas Mass, [kg]'); 
			legend('Location','northwest'); 
			xlabel(labelTime);
			xlim([0 plottime]);
		end
		legend([labels1'; labels2']);
		run standard_plot2v4.m
	end
end	
	
	
% ===================================================================