% ===================================================================
% Fuel Temperatures
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
content = {'Fuel Temperatures All'};
run standard_plot1v4.m;
if  (cases_output_type{k_num} == 1)
	mlcfile  = EDF.file{3}; 
	k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
	cmapTFU = hsv(size(EDF.data{k},2));
	
	if ~sum(k == emptyfile) 
		x2 = EDF.data{k}(:,1)./Tconst;
		for j=1:(size(EDF.data{k},2)-1)
			y{j} = EDF.data{k}(:,j+1);
			% plot(x2,y{j},lw,1,ms,2,cl,cmapTFU(j,:),ls,smap2{k_num});  %labels = [labels, ['COR-TFU' ] ];
			if flag3 == 1
			 dummy_ls = smap2{k_num};
			else
			 dummy_ls = '-';
			end
			
		%	if j<5
				plot(x2,y{j},lw,1,ms,2,cl,cmapTFU(j,:),ls,dummy_ls);  %labels = [labels, ['COR-TFU' ] ];
		%	end 		
		end
	end
	TFUlab =  {'COR-TFU_107' 'COR-TFU_111' 'COR-TFU_114' 'COR-TFU_116' ...
	'COR-TFU_207' 'COR-TFU_211' 'COR-TFU_214' 'COR-TFU_216' ...
	'COR-TFU_307' 'COR-TFU_311' 'COR-TFU_314' 'COR-TFU_316' ...
	'COR-TFU_407' 'COR-TFU_411' 'COR-TFU_414' 'COR-TFU_416' ...
	'COR-TFU_507' 'COR-TFU_511' 'COR-TFU_514' 'COR-TFU_516' ...
	};
	labels = [labels, TFUlab];
end
	legend_font_size = 7;
	ylabel('Temperature, [K]');   xlabel(labelTime);
	ylim([0.0 3200]); 		
	xlim([0 mediumtime]);
	run standard_plot2v4.m;
end
% ===================================================================
% Ring 1
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
content = {'Fuel Temperatures Ring 1'};
run standard_plot1v4.m;
if  (cases_output_type{k_num} == 1)
	mlcfile  = EDF.file{3}; 
	k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
	
	cmapTFU = hsv(4);
	
	TFUlab =  {...
	 'COR-TFU_107' 'COR-TFU_111' 'COR-TFU_114' 'COR-TFU_116' ...
	};
	
	if ~sum(k == emptyfile) 
		x2 = EDF.data{k}(:,1)./Tconst;
		%for j=1:(size(EDF.data{k},2)-1)
		for j=1:4
			y{j} = EDF.data{k}(:,j+1);
			% plot(x2,y{j},lw,1,ms,2,cl,cmapTFU(j,:),ls,smap2{k_num});  %labels = [labels, ['COR-TFU' ] ];
			if flag3 == 1
			 dummy_ls = smap2{k_num};
			else
			 dummy_ls = '-';
			end
			jj = j ;
				plot(x2,y{j},lw,1,ms,2,cl,cmapTFU(jj,:),ls,dummy_ls);    labels = [labels, [cases_list_names1{k_num} TFUlab{jj}] ];
		end
	end
	%labels = [labels, TFUlab];
end
	legend_font_size = 7;
	ylabel('Temperature, [K]');   xlabel(labelTime);
	ylim([0.0 3200]); 		
	xlim([0 mediumtime]);
	run standard_plot2v4.m;
end
TFUlab = {};
% ===================================================================
% Ring 2
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
content = {'Fuel Temperatures Ring 2'};
run standard_plot1v4.m;
if  (cases_output_type{k_num} == 1)
	mlcfile  = EDF.file{3}; 
	k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
cmapTFU = hsv(4);
	
	TFUlab =  { ...
	'COR-TFU_207' 'COR-TFU_211' 'COR-TFU_214' 'COR-TFU_216' ...
	};
	
	if ~sum(k == emptyfile) 
		x2 = EDF.data{k}(:,1)./Tconst;
		%for j=1:(size(EDF.data{k},2)-1)
		for j=5:8
			y{j} = EDF.data{k}(:,j+1);
			% plot(x2,y{j},lw,1,ms,2,cl,cmapTFU(j,:),ls,smap2{k_num});  %labels = [labels, ['COR-TFU' ] ];
			if flag3 == 1
			 dummy_ls = smap2{k_num};
			else
			 dummy_ls = '-';
			end
			
		jj = j - 4;
				plot(x2,y{j},lw,1,ms,2,cl,cmapTFU(jj,:),ls,dummy_ls);  labels = [labels, [cases_list_names1{k_num} TFUlab{jj}] ];
		%	end 		
		end
	end

	%labels = [labels, TFUlab];
end
	legend_font_size = 7;
	ylabel('Temperature, [K]');   xlabel(labelTime);
	ylim([0.0 3200]); 		
	xlim([0 mediumtime]);
	run standard_plot2v4.m;
end
TFUlab = {};
% ===================================================================
% Ring 3
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
content = {'Fuel Temperatures Ring 3'};
run standard_plot1v4.m;
if  (cases_output_type{k_num} == 1)
	mlcfile  = EDF.file{3}; 
	k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
cmapTFU = hsv(4);
	TFUlab =  { ...
	'COR-TFU_307' 'COR-TFU_311' 'COR-TFU_314' 'COR-TFU_316' ...
	};
	if ~sum(k == emptyfile) 
		x2 = EDF.data{k}(:,1)./Tconst;
		%for j=1:(size(EDF.data{k},2)-1)
		for j=9:12
			y{j} = EDF.data{k}(:,j+1);
			% plot(x2,y{j},lw,1,ms,2,cl,cmapTFU(j,:),ls,smap2{k_num});  %labels = [labels, ['COR-TFU' ] ];
			if flag3 == 1
			 dummy_ls = smap2{k_num};
			else
			 dummy_ls = '-';
			end
			
		jj = j - 8;
				plot(x2,y{j},lw,1,ms,2,cl,cmapTFU(jj,:),ls,dummy_ls);  labels = [labels, [cases_list_names1{k_num} TFUlab{jj}] ];
		%	end 		
		end
	end
	
	%labels = [labels, TFUlab];
end
	legend_font_size = 7;
	ylabel('Temperature, [K]');   xlabel(labelTime);
	ylim([0.0 3200]); 		
	xlim([0 mediumtime]);
	run standard_plot2v4.m;
end
TFUlab = {};
% ===================================================================
% Ring 4
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
content = {'Fuel Temperatures Ring 4'};
run standard_plot1v4.m;
if  (cases_output_type{k_num} == 1)
	mlcfile  = EDF.file{3}; 
	k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
cmapTFU = hsv(4);
	
		TFUlab =  { ...
	'COR-TFU_407' 'COR-TFU_411' 'COR-TFU_414' 'COR-TFU_416' ...
	};
	
	if ~sum(k == emptyfile) 
		x2 = EDF.data{k}(:,1)./Tconst;
		%for j=1:(size(EDF.data{k},2)-1)
		for j=13:16
			y{j} = EDF.data{k}(:,j+1);
			% plot(x2,y{j},lw,1,ms,2,cl,cmapTFU(j,:),ls,smap2{k_num});  %labels = [labels, ['COR-TFU' ] ];
			if flag3 == 1
			 dummy_ls = smap2{k_num};
			else
			 dummy_ls = '-';
			end
			
	jj = j - 12;
				plot(x2,y{j},lw,1,ms,2,cl,cmapTFU(jj,:),ls,dummy_ls);  labels = [labels, [cases_list_names1{k_num} TFUlab{jj}] ];
		%	end 		
		end
	end

	%labels = [labels, TFUlab];
end
	legend_font_size = 7;
	ylabel('Temperature, [K]');   xlabel(labelTime);
	ylim([0.0 3200]); 		
	xlim([0 mediumtime]);
	run standard_plot2v4.m;
end
TFUlab = {};
% ===================================================================
% Ring 5
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
content = {'Fuel Temperatures Ring 5'};
run standard_plot1v4.m;
if  (cases_output_type{k_num} == 1)
	mlcfile  = EDF.file{3}; 
	k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
cmapTFU = hsv(4);
	
		TFUlab =  { ...
	'COR-TFU_507' 'COR-TFU_511' 'COR-TFU_514' 'COR-TFU_516' ...
	};
	
	if ~sum(k == emptyfile) 
		x2 = EDF.data{k}(:,1)./Tconst;
		%for j=1:(size(EDF.data{k},2)-1)
		for j=17:20
			y{j} = EDF.data{k}(:,j+1);
			% plot(x2,y{j},lw,1,ms,2,cl,cmapTFU(j,:),ls,smap2{k_num});  %labels = [labels, ['COR-TFU' ] ];
			if flag3 == 1
			 dummy_ls = smap2{k_num};
			else
			 dummy_ls = '-';
			end
			
		jj = j - 16;
				plot(x2,y{j},lw,1,ms,2,cl,cmapTFU(jj,:),ls,dummy_ls);  labels = [labels, [cases_list_names1{k_num} TFUlab{jj}] ];
		%	end 		
		end
	end

	%labels = [labels, TFUlab];
end
	legend_font_size = 7;
	ylabel('Temperature, [K]');   xlabel(labelTime);
	ylim([0.0 3200]); 		
	xlim([0 mediumtime]);
	run standard_plot2v4.m;
end
TFUlab = {};
% ===================================================================
% ===================================================================
% ===================================================================
% ===================================================================
% ===================================================================




% ===================================================================
% ===================================================================
% ===================================================================
% Cladding Temperatures
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
content = {'Cladding Temperatures All'};
run standard_plot1v4.m;

if  (cases_output_type{k_num} == 1)
	mlcfile  = EDF.file{13}; 
	k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
	
	
cmapTCL = hsv(size(EDF.data{k},2));
	
	if ~sum(k == emptyfile) 
		x2 = EDF.data{k}(:,1)./Tconst;
		for j=1:(size(EDF.data{k},2)-1)
			y{j} = EDF.data{k}(:,j+1);
			% plot(x2,y{j},lw,1,ms,2,cl,cmapTCL(j,:),ls,smap2{k_num});  %labels = [labels, ['COR-TCL' ] ];
			if flag3 == 1
			 dummy_ls = smap2{k_num};
			else
			 dummy_ls = '-';
			end
		%	if j<5
				plot(x2,y{j},lw,1,ms,2,cl,cmapTCL(j,:),ls,dummy_ls);  %labels = [labels, ['COR-TCL' ] ];
		%	end	
		end
	end
	TCLlab =  {'COR-TCL_107' 'COR-TCL_111' 'COR-TCL_114' 'COR-TCL_116' ...
	'COR-TCL_207' 'COR-TCL_211' 'COR-TCL_214' 'COR-TCL_216' ...
	'COR-TCL_307' 'COR-TCL_311' 'COR-TCL_314' 'COR-TCL_316' ...
	'COR-TCL_407' 'COR-TCL_411' 'COR-TCL_414' 'COR-TCL_416' ...
	'COR-TCL_507' 'COR-TCL_511' 'COR-TCL_514' 'COR-TCL_516' 
	};
	labels = [labels, TCLlab];
end
	legend_font_size = 7;
	ylabel('Temperature, [K]');   xlabel(labelTime);
	ylim([0.0 3200]); 		
	xlim([0 mediumtime]);
	run standard_plot2v4.m;
end
% ===================================================================
% Ring 1
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
content = {'Cladding Temperatures Ring 1'};
run standard_plot1v4.m;

if  (cases_output_type{k_num} == 1)
	mlcfile  = EDF.file{13}; 
	k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
	cmapTCL = hsv(4);
		TCLlab =  {...
	'COR-TCL_107' 'COR-TCL_111' 'COR-TCL_114' 'COR-TCL_116' ...
	};
	if ~sum(k == emptyfile) 
		x2 = EDF.data{k}(:,1)./Tconst;
		for j=1:4
			y{j} = EDF.data{k}(:,j+1);
			% plot(x2,y{j},lw,1,ms,2,cl,cmapTCL(j,:),ls,smap2{k_num});  %labels = [labels, ['COR-TCL' ] ];
			if flag3 == 1
			 dummy_ls = smap2{k_num};
			else
			 dummy_ls = '-';
			end
		
		jj = j;
				plot(x2,y{j},lw,1,ms,2,cl,cmapTCL(jj,:),ls,dummy_ls);  labels = [labels, [cases_list_names1{k_num} TCLlab{jj}] ];
		%	end	
		end
	end

	%labels = [labels, TCLlab];
end
	legend_font_size = 7;
	ylabel('Temperature, [K]');   xlabel(labelTime);
	ylim([0.0 3200]); 		
	xlim([0 mediumtime]);
	run standard_plot2v4.m;
end
TFUlab = {};
% ===================================================================
% Ring 2
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
content = {'Cladding Temperatures Ring 2'};
run standard_plot1v4.m;

if  (cases_output_type{k_num} == 1)
	mlcfile  = EDF.file{13}; 
	k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
	cmapTCL = hsv(4);
		TCLlab =  {...
	'COR-TCL_207' 'COR-TCL_211' 'COR-TCL_214' 'COR-TCL_216' ...
	};
	if ~sum(k == emptyfile) 
		x2 = EDF.data{k}(:,1)./Tconst;
		for j=5:8
			y{j} = EDF.data{k}(:,j+1);
			% plot(x2,y{j},lw,1,ms,2,cl,cmapTCL(j,:),ls,smap2{k_num});  %labels = [labels, ['COR-TCL' ] ];
			if flag3 == 1
			 dummy_ls = smap2{k_num};
			else
			 dummy_ls = '-';
			end
		jj = j - 4;
				plot(x2,y{j},lw,1,ms,2,cl,cmapTCL(jj,:),ls,dummy_ls);  labels = [labels, [cases_list_names1{k_num} TCLlab{jj}] ];
		%	end	
		end
	end

	%labels = [labels, TCLlab];
end
	legend_font_size = 7;
	ylabel('Temperature, [K]');   xlabel(labelTime);
	ylim([0.0 3200]); 		
	xlim([0 mediumtime]);
	run standard_plot2v4.m;
end
TFUlab = {};
% ===================================================================
% Ring 3
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
content = {'Cladding Temperatures Ring 3'};
run standard_plot1v4.m;

if  (cases_output_type{k_num} == 1)
	mlcfile  = EDF.file{13}; 
	k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
cmapTCL = hsv(4);
	
	TCLlab =  {...
	'COR-TCL_307' 'COR-TCL_311' 'COR-TCL_314' 'COR-TCL_316' ...
	};
	
	if ~sum(k == emptyfile) 
		x2 = EDF.data{k}(:,1)./Tconst;
		for j=9:12
			y{j} = EDF.data{k}(:,j+1);
			% plot(x2,y{j},lw,1,ms,2,cl,cmapTCL(j,:),ls,smap2{k_num});  %labels = [labels, ['COR-TCL' ] ];
			if flag3 == 1
			 dummy_ls = smap2{k_num};
			else
			 dummy_ls = '-';
			end
		
		jj = j - 8;
				plot(x2,y{j},lw,1,ms,2,cl,cmapTCL(jj,:),ls,dummy_ls);  labels = [labels, [cases_list_names1{k_num} TCLlab{jj}] ];
		%	end	
		end
	end

	%labels = [labels, TCLlab];
end
	legend_font_size = 7;
	ylabel('Temperature, [K]');   xlabel(labelTime);
	ylim([0.0 3200]); 		
	xlim([0 mediumtime]);
	run standard_plot2v4.m;
end
TFUlab = {};
% ===================================================================
% Ring 4
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
content = {'Cladding Temperatures Ring 4'};
run standard_plot1v4.m;

if  (cases_output_type{k_num} == 1)
	mlcfile  = EDF.file{13}; 
	k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
cmapTCL = hsv(4);
	
	TCLlab =  {...
	'COR-TCL_407' 'COR-TCL_411' 'COR-TCL_414' 'COR-TCL_416' ...
	};
	
	if ~sum(k == emptyfile) 
		x2 = EDF.data{k}(:,1)./Tconst;
		for j=13:16
			y{j} = EDF.data{k}(:,j+1);
			% plot(x2,y{j},lw,1,ms,2,cl,cmapTCL(j,:),ls,smap2{k_num});  %labels = [labels, ['COR-TCL' ] ];
			if flag3 == 1
			 dummy_ls = smap2{k_num};
			else
			 dummy_ls = '-';
			end
		
		
		jj = j - 12;
				plot(x2,y{j},lw,1,ms,2,cl,cmapTCL(jj,:),ls,dummy_ls);  labels = [labels, [cases_list_names1{k_num} TCLlab{jj}] ];
		%	end	
		end
	end

	%labels = [labels, TCLlab];
end
	legend_font_size = 7;
	ylabel('Temperature, [K]');   xlabel(labelTime);
	ylim([0.0 3200]); 		
	xlim([0 mediumtime]);
	run standard_plot2v4.m;
end
TFUlab = {};
% ===================================================================
% Ring 5
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
content = {'Cladding Temperatures Ring 5'};
run standard_plot1v4.m;

if  (cases_output_type{k_num} == 1)
	mlcfile  = EDF.file{13}; 
	k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
	cmapTCL = hsv(4);
	
	TCLlab =  {...
	'COR-TCL_507' 'COR-TCL_511' 'COR-TCL_514' 'COR-TCL_516' 
	};
	if ~sum(k == emptyfile) 
		x2 = EDF.data{k}(:,1)./Tconst;
		for j=17:20
			y{j} = EDF.data{k}(:,j+1);
			% plot(x2,y{j},lw,1,ms,2,cl,cmapTCL(j,:),ls,smap2{k_num});  %labels = [labels, ['COR-TCL' ] ];
			if flag3 == 1
			 dummy_ls = smap2{k_num};
			else
			 dummy_ls = '-';
			end
		
		
		jj = j - 16;
				plot(x2,y{j},lw,1,ms,2,cl,cmapTCL(jj,:),ls,dummy_ls);  labels = [labels, [cases_list_names1{k_num} TCLlab{jj}] ];
		%	end	
		end
	end

	%labels = [labels, TCLlab];
end
	legend_font_size = 7;
	ylabel('Temperature, [K]');   xlabel(labelTime);
	ylim([0.0 3200]); 		
	xlim([0 mediumtime]);
	run standard_plot2v4.m;
end
TFUlab = {};