% Ploting script version 27-08-2020. Attempt to make it as general as possbile
% Modified version of the NARSIS_EDF_READ_VER9
% Here it is for Forever epeirment orginally it was developed for Gen III plant
% NARSIS Project % Plot Forever code
% TO USE THIS FILE YOU NEED XCORE toolbox package installed
tic; clc; close all; clear; format short g
% FOLDERS ===================================================================
case_name = 'Gen II NPP Calculations'
root_folder = 'C:\CloudStation\ROOT\NARSIS-GenII'
post_folder = 'C:\CloudStation\NARSIS\MODELE_AND_RESEARCH\SCRIPTS\MATLAB_XCore\x-core\Database\MLC_package\PostProcessing\MATLAB_MELCOR_RESULTS_VISUALIZATION\genII'
%exp_file = 'EXP_FOREVER_DATA1.mat'
%code1 ='M2.2.15254'; %code1 ='M2.2.11932'; 
code1 ='M2.2.9541'; %code1 = 'MELCOR2.1.6341'; %code1 ='M2.2.9541';

% SETUP ===================================================================
flag1 		= 1; %PLOT 1-YES 0 - NO
flagPLOT = flag1; %Plot figures
flag2 		= 1; %SAVE FIGURE 1-YES 0-NO; 
flag3 		= 1;  
% Creates single plot
% flag3 NEW: 0 - save each to its own folder, 1 - save to new folder COMPARISON
% 0 - only plot no comparison, 1 - it is comparison

flag3a     = 0;  %0  - same figure numbers; 1 - new case increases figure numbers % 1- allows to compare matlab figures

%flag4    	= 1; %1 - Save FIG type file
flagTH 		= 1; % TH results
flagCases 	= 1; %1 - read from local folder or absolute, 0- read from given folder explicity in the file - file with list of folders
flagEDF 	= 1; %1 Use external file
flagEXP 	= 0; %Load expeirmental data
flag_hours  = 0; %1 - hours 0 - seconds - input data time scale
title_flag 	= 2 ; %0 - no title; 1 - title with file localization (subfolder) - helps to identificate file; 2 - title with information about what is on the plot 
%flag_info1  = 0;  %Add info about folder and case; 1 - old with folder; 2 - reduced with mlcfilename, 0 - no
flag_info1  = 3;  %Add case name when flag3 == 0


flag_additional = 0; %plot normal plot or additional plots



% EXPERIMENTAL DATA========================================================
% load experimental data - run mat file with experiment data
if flagEXP == 1
	load([post_folder,'\', exp_file]);
end

% CASES LIST ===================================================================
%cases_list 			= { 'case00028a' 'case00028b' 'case00028c'}
cases_list 			= { 'case00029b' 'case00029d2' 'case00029e' 'case00029f' 'case00029g'}
cases_output_type 	= {1 1 1 1 1}
cases_list_names 	= { 'HLL#1 ' 'CLL#1 ' 'SBO#1_noSeal ' 'SBO#1_Seal21 ' 'SBO#1_Seal250+21 '}

flag_accident 		= {0 0 1 1 1}  %0 - LOCA  1 - SBO



cases_control 		= [0 0 1 1 1] %Select cases to plot,   >= 1 draw. 0 - do not draw
%cases_comparison 	= {'case00029e' 'case00029f' 'case00029g'}
last_case = 5;
plot_omit 			= [];

if flag3 == 0	cases_list_names1 	= {'' '' '' '' ''};
else  cases_list_names1 	= cases_list_names; end
	
%cases_list = { 'case00025l'  'case00025m'} ;% 'case00026' 'case00026a'}
% cases_list = { 'case00025l'  'case00025m' 'case00026' 'case00026a'}
%cases_list = { 'case00025l' }

%cases_output_type = { 1 1 }; % 2 2}  %type 1 is apt file   type 2 is EDF file, 3 new, 4 newer to be selected by the user
%cases_output_type = { 1 1 2 2}  %type 1 is apt file   type 2 is EDF file, 3 new, 4 newer to be selected by the user

%cases_output_type = { 1 }
%cases_list_names = {'HLL#1_CFVoff' 'CLL#1_CFVoff'}; % 'HLL#1_CFVon'  'CLL#1_CFVon'}

%cases_list_names = {'HLL#1_CFVoff' 'CLL#1_CFVoff' 'HLL#1_CFVon'  'CLL#1_CFVon'}
%cases_list_names = {'CLL#1_CFVoff' }
%cases_list_names = {'HLL#1_CFVoff'  }
%cases_list_names =  cases_list; %{'test14' 'test11' 'test15' 'test12' 'test16'}



% CASES ===================================================================
if flagCases == 1
    folder = { root_folder }; %folder with results
	subfolder = cases_list;   %folder for particular case (result)
	for i=1:numel(cases_list)
		cases(i) = {[folder{1} '\' subfolder{i}]}; 
	end
    %cases_names = {[folder{1} '_' subfolder{1}]};  % 'C7E'  'C7F'}
	cases_names = cases_list_names;  
else
    %run FILES_DATA.m    %Read file names - FILE WITH EXPLICIT NAMES OF FILES WITH DATA
	%% file_final1 = 'Q:\ROOT\NARSIS-Forever\test14'; %Here it is given expicity but it should be in FILES_DATA file
%	file_final2 = {file_final1}; 
%    cases = file_final2; 
%    cases_names = cases_list_names;  
end

% if numel(cases) == 1 
%     flag3 = 0;  % 0 -save to 1st folder; 1 - save to new folder
% end
% if numel(cases) > 1 
%     flag3 = 1;  % 0 -save to 1st folder; 1 - save to new folder
% end

% ===================================================================
% COLORS AND LABELS SETUP
run colormaps_and_labels.m
if flag3 == 0
	col2 = col3;
end	


% TIME #1 ===================================================================
plottime = 250000		 %xlim([0 plottime./Tconst]); %60*3600;  %time end for plotting %timetime = 30000/3600; 
LOCAshorttime 	= 600; 	  	LOCAmediumtime 	= 10000; 	LOCAlongtime = 100000;
SBOshorttime 	= 20000;  	SBOmediumtime 	= 50000; 	SBOlongtime  = 100000;
% TIME #2 ===================================================================
%tend = plottime; % czas konca obliczen %8; %7 % 24 %66.0;  calculation time %<================================= TIME
if(flag_hours == 1)  
	Tconst = 3600;   labelTime = labelHours;
else 
	Tconst = 1.0;  labelTime= labelSeconds;
end  %time conversion from sec to hours

LOCAshorttime 	= LOCAshorttime./Tconst; 	  	
LOCAmediumtime 	= LOCAmediumtime./Tconst; 	  	 	
LOCAlongtime 	= LOCAlongtime./Tconst; 	  	
SBOshorttime 	= SBOshorttime./Tconst; 	  	
SBOmediumtime 	= SBOmediumtime./Tconst; 	  	
SBOlongtime  	= SBOlongtime./Tconst; 	  	

% ===================================================================


a  = 0; %initialization, a is plotted figure number, it does not increase when figure is not plotted
%a1 = -1; %global plot number
% PLOTTING ===================================================================
% This loop will allow to compre different results
for k_num=1:numel(cases)

	if cases_control(k_num) == 0
		%do not draw
	else
		% ===================================================================
		% Time setup	
		if flag_accident{k_num} == 0
			shorttime = LOCAshorttime; mediumtime = LOCAmediumtime; longtime = LOCAlongtime;
		elseif flag_accident{k_num} == 1
			shorttime = SBOshorttime; mediumtime = SBOmediumtime; longtime = SBOlongtime;
		end

		case1 =  cases{k_num};  % current case folder
		case1_name = cases_names{k_num}; % current case name
		% ===================================================================
		if flagEDF == 1 
			run EDF_LOAD_GENII_v1.m  
		end
		% ===================================================================
		if flagTH ==1
			if flag3a == 1   %plot with increasing figure numbers
				a = a ; 
				plot_omit = plot_omit + a
			else
				a = 0;		%plot with the same figure numbers
			end
			%	a1 = 0;
			if(flag1 == 1) %PLOT OR NOT
				%run PLOT_TH_GENII_v1.m
				run PLOT_CORE_GENII_v2.m
				run PLOT_RCS_GENII_v2.m
				run PLOT_CONT_GENII_v2.m
				run PLOT_CAV_GENII_v2.m
				
			end
		end
		% ===================================================================
	end
end    

% ===================================================================
toc