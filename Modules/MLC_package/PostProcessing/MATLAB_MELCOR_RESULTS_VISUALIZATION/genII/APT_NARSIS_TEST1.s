# #############################################################################
 MELCOR 0 ptf "E:\DARO\MELCOR\ROOT\NARSIS-GenII\FINAL_Rev1\SBO#1a_annulus\PWR.PTF"
 MELCOR 1 ptf "E:\DARO\MELCOR\ROOT\NARSIS-GenII\FINAL_Rev1\CLL#1.1a_annulus\PWR.PTF"
# #############################################################################
 MREAD 0 "CFVALU_2006"
 MREAD 0 "CFVALU_2007"
 MREAD 0 "CVH-P_400"
 MREAD 0 "COR-TSVC_117"  
 MREAD 0 "CFVALU_2023"
 MREAD 0 "CVH-P_900"
 MREAD 0 "CVH-TVAP_900"
 MREAD 0 "CFVALU_982"
 MREAD 0 "CVH-X.6_900" 
 MREAD 0 "CFVALU_992"
 # #############################################################################
 MREAD 1 "CFVALU_2006"
 MREAD 1 "CFVALU_2007"
 MREAD 1 "CVH-P_400"
 MREAD 1 "COR-TSVC_117"  
 MREAD 1 "CFVALU_2023"
 MREAD 1 "CVH-P_900"
 MREAD 1 "CVH-TVAP_900"
 MREAD 1 "CFVALU_982"
 MREAD 1 "CVH-X.6_900" 
 MREAD 1 "CFVALU_992"
# #############################################################################
# MELCOR	0	EXPORT ASCII	 ".\FINAL_Rev1\SBO#1a_annulus\DATA_NARSIS_Rev4.apt"
# MELCOR	1	EXPORT ASCII	 ".\FINAL_Rev1\CLL#1.1a_annulus\DATA_NARSIS_Rev4.apt"
 MELCOR	0	EXPORT ASCII	 ".\data1.apt"
 MELCOR	1	EXPORT ASCII	 ".\data2.apt"
# #############################################################################


