# #############################################################################
MELCOR 0 ptf ".\PWR.PTF"
# #############################################################################

# #################################################################################
# FILE 16
# LPIS 1 ACTIVE 2,3
 MREAD 0 "FL-MFLOW.1_862"; MREAD 0 "FL-I-H2O-MFLOW_862"
 # LPIS 2 ACTIVE 4,5,6,7
 MREAD 0 "FL-MFLOW.1_860"; MREAD 0 "FL-I-H2O-MFLOW_860"
 MREAD 0 "FL-MFLOW.1_861"; MREAD 0 "FL-I-H2O-MFLOW_861"
 # HPIS 1 ACTIVE 8,9
 MREAD 0 "FL-MFLOW.1_872"; MREAD 0 "FL-I-H2O-MFLOW_872"
 # HPIS 2 ACTIVE 10,11,12,13
 MREAD 0 "FL-MFLOW.1_870"; MREAD 0 "FL-I-H2O-MFLOW_870"
 MREAD 0 "FL-MFLOW.1_871"; MREAD 0 "FL-I-H2O-MFLOW_871"
 
 # RECIRC FLOWS - FOR MODEL WITH SOU_MODELoff
 # LPIS1 14, 15
 MREAD 0 "FL-MFLOW.1_865"; MREAD 0 "FL-I-H2O-MFLOW_865"; 
 # LPIS2 16, 17, 18, 19
 MREAD 0 "FL-MFLOW.1_863"; MREAD 0 "FL-I-H2O-MFLOW_863"; 
 MREAD 0 "FL-MFLOW.1_864"; MREAD 0 "FL-I-H2O-MFLOW_864"; 
 # HPIS1 20 ,21
 MREAD 0 "FL-MFLOW.1_875"; MREAD 0 "FL-I-H2O-MFLOW_875"; 
 # HPIS2 22, 23, 24, 25
 MREAD 0 "FL-MFLOW.1_873"; MREAD 0 "FL-I-H2O-MFLOW_873";
 MREAD 0 "FL-MFLOW.1_874"; MREAD 0 "FL-I-H2O-MFLOW_874";  

MELCOR	0	EXPORT ASCII	 ".\DATA_SIS_Rev4.apt"
# #################################################################################
# FILE 17
# LPIS 1 RECIRULATION 2
 MREAD 0 "CFVALU_1113"
# LPIS 2 RECIRULATION 3,4  x2 as two flows
 MREAD 0 "CFVALU_1111"
 MREAD 0 "CFVALU_1111"

# LPIS-TOTAL FLOW FOR HX MODEL 5
 MREAD 0 "CFVALU_1009"
# RHR HEAT REMOVAL 6 
 MREAD 0 "CFVALU_1018"
# RHR HX TEMPERATURE DROP 7
 MREAD 0 "CFVALU_1019"
# RHR HX TOUT 8
 MREAD 0 "CFVALU_1020"
# RHR Temperature dependand on mode 9
 MREAD 0 "CFVALU_1021"

MELCOR	0	EXPORT ASCII	 ".\DATA_SOU_Rev4.apt"
# #################################################################################
# FILE 4
#BREAK 2,3  4,5  
# 899 - B, 898 - I
 MREAD 0 "FL-MFLOW_899"; MREAD 0 "FL-I-H2O-MFLOW_899"; 
 MREAD 0 "FL-MFLOW_898"; MREAD 0 "FL-I-H2O-MFLOW_898"; 
#Accumulators 6,7,8,9
 MREAD 0 "ESF-ACC-RAT_1"; MREAD 0 "ESF-ACC-RAT_2"
 MREAD 0 "ESF-ACC-MAS_1"; MREAD 0 "ESF-ACC-MAS_2"
# RCS mass water 10,11
# 1998 - RCS water, 1999 RCS+SL+PRZ water
 MREAD 0 "CFVALU_1998"
 MREAD 0 "CFVALU_1999"
# HOT LEG CREEP BREAKS  890 - B    891  - I
# 12,13, 14,15
 MREAD 0 "FL-MFLOW_890"; MREAD 0 "FL-I-H2O-MFLOW_890";  
 MREAD 0 "FL-MFLOW_891"; MREAD 0 "FL-I-H2O-MFLOW_891";  
# SEAL LOCA - 16,17,18,19,20,21,22
 MREAD 0 "FL-MFLOW_894"; MREAD 0 "FL-I-H2O-MFLOW_894" 
 MREAD 0 "FL-MFLOW_895"; MREAD 0 "FL-I-H2O-MFLOW_895" 
 MREAD 0 "FL-MFLOW_896"; MREAD 0 "FL-I-H2O-MFLOW_896" 
 MREAD 0 "FL-MFLOW_897"; MREAD 0 "FL-I-H2O-MFLOW_897" 


MELCOR 0 EXPORT ASCII ".\DATA_BREAK_Rev4.apt"
###############################################################
# FILE 19
# ESFs 
# ACC 2,3,4,5
 MREAD 0 "ESF-ACC-RAT_1"; MREAD 0 "ESF-ACC-RAT_2"
 MREAD 0 "ESF-ACC-MAS_1"; MREAD 0 "ESF-ACC-MAS_2"
# PARs  6,7
 MREAD 0 "ESF-PAR-DMH2_1"
 MREAD 0 "ESF-PAR-IMH2_1" 
# SPRAYS Flow, Temperature  8,9,10,11
 MREAD 0 "SPR-FL_1";  MREAD 0 "SPR-FL_2"
 MREAD 0 "SPR-TP_1";  MREAD 0 "SPR-TP_2"
# FCL 12,13,14,15
 MREAD 0 "ESF-MFC-RAT_1"
 MREAD 0 "ESF-MFC-TOT_1"
 MREAD 0 "ESF-QFC-RAT_1"
 MREAD 0 "ESF-QFC-TOT_1"
 
MELCOR	0	EXPORT ASCII	 ".\DATA_ESF_Rev4.apt"
# #############################################################################
# FILE 17
# LPIS 1 RECIRULATION 2
 MREAD 0 "CFVALU_1113"
# LPIS 2 RECIRULATION 3,4  x2 as two flows
 MREAD 0 "CFVALU_1111"
 MREAD 0 "CFVALU_1111"

# LPIS-TOTAL FLOW FOR HX MODEL 5
 MREAD 0 "CFVALU_1009"
# RHR HEAT REMOVAL 6 
 MREAD 0 "CFVALU_1018"
# RHR HX TEMPERATURE DROP 7
 MREAD 0 "CFVALU_1019"
# RHR HX TOUT 8
 MREAD 0 "CFVALU_1020"
# RHR Temperature dependand on mode 9
 MREAD 0 "CFVALU_1021"

MELCOR	0	EXPORT ASCII	 ".\DATA_SOU_Rev4.apt"
# #################################################################################
# FILE 18
# SUMP 2,3,4,5 
 MREAD 0 "CVH-LIQLEV_909"
 MREAD 0 "CVH-MASS.1_909"
 MREAD 0 "CVH-MASS_909"
 MREAD 0 "CVH-TLIQ_909"
# RELATIVE WATER LEVEL IN SUMP  6
 MREAD 0 "CFVALU_992"
 
# RWST 7, 8, 9, 10
 MREAD 0 "CVH-LIQLEV_912"
 MREAD 0 "CVH-MASS.1_912"
 MREAD 0 "CVH-MASS_912"
 MREAD 0 "CVH-TLIQ_912"
# RELATIVE WATER LEVEL IN SUMP   11
 MREAD 0 "CFVALU_982"

# CAVITY 12, 13, 14, 15, 16
 MREAD 0 "CVH-LIQLEV_908"
 MREAD 0 "CVH-MASS.1_908"
 MREAD 0 "CVH-MASS_908"
 MREAD 0 "CVH-TLIQ_908"
 MREAD 0 "CVH-TVAP_908"
#
MELCOR	0	EXPORT ASCII	 ".\DATA_TANKS_Rev4.apt"
# #############################################################################
# FILE 1
# Time 1,  Decay Heat 2
 MREAD 0 "COR-EFPD-RAT" 
# PRZ p, T  3,4,5,6
 MREAD 0 "CVH-P_400"
 MREAD 0 "CVH-TLIQ_400"
 MREAD 0 "CVH-TVAP_400" 
 MREAD 0 "CVH-MASS.1_400" 
#  # SG p, T, liq mass,   7,8,9,10,11,12      2001  SG-B mass, 2000 SG-I mass
  MREAD 0 "CVH-P_730"    
  MREAD 0 "CVH-P_830" 
  MREAD 0 "CVH-TVAP_730"
  MREAD 0 "CVH-TVAP_830"  
#MREAD 0 "CFVALU_770" #MREAD 0 "CFVALU_771"
 MREAD 0 "CFVALU_2000"
 MREAD 0 "CFVALU_2001"
#  
# RCS - HL, CL  , Temp., Mass   13,14,15,16     # 1998 RCS mas without PZR, 1999 with PZR
  MREAD 0 "CVH-TVAP_500"
  MREAD 0 "CVH-TVAP_600"  
  MREAD 0 "CVH-TVAP_570"
  MREAD 0 "CVH-TVAP_670"
#  
# CORE   17-20
#  MREAD 0 "CVH-TLIQ_300"; MREAD 0 "CVH-TLIQ_340"; MREAD 0 "CVH-TLIQ_310"; MREAD 0 "CVH-TLIQ_320"   
#  MREAD 0 "CVH-TVAP_300"; MREAD 0 "CVH-TVAP_340"; MREAD 0 "CVH-TVAP_310"; MREAD 0 "CVH-TVAP_320"   
  MREAD 0 "CVH-TLIQ_340"; MREAD 0 "CVH-TLIQ_320"
  MREAD 0 "CVH-TVAP_340"; MREAD 0 "CVH-TVAP_320"
# MREAD 0 "CVH-LIQLEV_320"; MREAD 0 "CVH-CLIQLEV_320"
# CORE EXIT TEMPERATURE 21, 22
  MREAD 0 "COR-TSVC_117"  
  MREAD 0 "COR-TSVC_108" 
#
#  # CVH CORE BOT = 1.544  # CVH CORE TOP = 6.58686
#  # BAF =  2.567r  # TAF = 6.2246

MELCOR 0 EXPORT ASCII ".\DATA_RCS_Rev4.apt"
###############################################################
# FILE 14
# MEASURED LEVELS
#	'SG_CLEV-B'         2002
#	'SG_CLEV-I'         2003    
#	'SG_NRLEV-B'   		2004
#	'SG_NRLEV-I'   		2005 
#	'SG_WRLEV-B'          2006 
#	'SG_WRLEV-I'          2007
# 'PRZ-LVL'          2010   LEVEL IN RELATION TO BOTTOM OF THE CVH
# 'PRZ-LVL-FRAC'   2011	FRACTION OF THE CVH
# 'PRZ-NLVL-FRAC'   2012 	LEVEL IN FRACTION OF 0-100% NOMINAL LEVELS
# 11.76025	0.0
# 20.43825	28.4152
# SG COLLAPSED LEVEL B, I  2,3
 MREAD 0 "CFVALU_2002"
 MREAD 0 "CFVALU_2003"
# SG NARROW LEVEL FRAC 4,5
 MREAD 0 "CFVALU_2004"
 MREAD 0 "CFVALU_2005"
# SG WIDE LEVEL FRAC 6,7
 MREAD 0 "CFVALU_2006"
 MREAD 0 "CFVALU_2007"
# PRZ LEVEL  8,9, 10
 MREAD 0 "CFVALU_2010"
 MREAD 0 "CFVALU_2011"
 MREAD 0 "CFVALU_2012"
# CORE LIQUID LEVELS 11,12,13,14,15,16,17,18 
  MREAD 0 "CVH-LIQLEV_300"; MREAD 0 "CVH-CLIQLEV_300"
  MREAD 0 "CVH-LIQLEV_310"; MREAD 0 "CVH-CLIQLEV_310"
  MREAD 0 "CVH-LIQLEV_320"; MREAD 0 "CVH-CLIQLEV_320"
  MREAD 0 "CVH-LIQLEV_340"; MREAD 0 "CVH-CLIQLEV_340"
# ADDITIONAL RPV LIQUID LEVELS  
#  MREAD 0 "CFVALU_2023";  MREAD 0 "CFVALU_2024"  

MELCOR 0 EXPORT ASCII ".\DATA_LVL_Rev4.apt"
###############################################################
# FILE 20
# FW B,I, AFW B,I   2,3,4,5
 MREAD 0 "FL-MFLOW_700"
 MREAD 0 "FL-MFLOW_800"
 MREAD 0 "FL-MFLOW_750"
 MREAD 0 "FL-MFLOW_850"

MELCOR 0 EXPORT ASCII ".\DATA_FW_Rev4.apt"
###############################################################
# FILE 2
#  # Containment p, T, ppart H2, H2O, CO  2,3,4,5,6,7, 8, 9, 10
# Where CVH.X.<number>:  X= 3 – H2O vap.;  4 – N2;  5 – O2;  6 – H2; 7 – CO2; 8 – CO
  MREAD 0 "CVH-P_900"
  MREAD 0 "CVH-PPART.3_900"
  MREAD 0 "CVH-PPART.4_900"
  MREAD 0 "CVH-PPART.5_900"
  MREAD 0 "CVH-PPART.6_900"
  MREAD 0 "CVH-PPART.7_900"
  MREAD 0 "CVH-PPART.8_900"
  MREAD 0 "CVH-TVAP_900"
  MREAD 0 "CVH-TSAT(P)_900"    
#  #Hydrogen and CO and other gasses  11 - 19
  MREAD 0 "COR-DMH2-TOT"
  MREAD 0 "CAV-MEX-H2_10"
  MREAD 0 "CAV-MEX-CO_10"
# Concetrations 14 - 19 
  MREAD 0 "CVH-X.3_900"
  MREAD 0 "CVH-X.4_900"
  MREAD 0 "CVH-X.5_900"
  MREAD 0 "CVH-X.6_900"
  MREAD 0 "CVH-X.7_900"
  MREAD 0 "CVH-X.8_900"
# CFVenting open fraction 20
 MREAD 0 "CFVALU_932"
 
MELCOR 0 EXPORT ASCII ".\DATA_CONT_Rev4.apt"

###############################################################
# FILE 12
# CAVITY, 2
 MREAD 0 "CAV-DHR_10"
# WATER IN CAVITY AND SUMP 3,4
 MREAD 0 "CVH-MASS.1_908"
 MREAD 0 "CVH-MASS.1_909"
# CORIUM IN CAVITY  5,6, 7, 8
 MREAD 0 "CAV-MEX-H2_10"
 MREAD 0 "CAV-MEX-CO_10"
 MREAD 0 "CAV-MEX-CO2_10"
 MREAD 0 "CAV-MEX-H2O_10"
# Altitude 9,10
 MREAD 0 "CAV-MINALT_10"
 MREAD 0 "CAV-MAXRAD_10"
# Temperatures 11,12,13,14,15,16
 MREAD 0 "CAV-T-DEB_10"
 MREAD 0 "CAV-T-HMX_10"
 MREAD 0 "CAV-T-HOX_10"
 MREAD 0 "CAV-T-LMX_10"
 MREAD 0 "CAV-T-LOX_10"
 MREAD 0 "CAV-T-MET_10"
# Masses 17, 18, 19, 20, 21,22, 23
 MREAD 0 "CAV-M-DEB_10"
 MREAD 0 "CAV-M-HMX_10"
 MREAD 0 "CAV-M-HOX_10"
 MREAD 0 "CAV-M-LMX_10"
 MREAD 0 "CAV-M-LOX_10"
 MREAD 0 "CAV-M-MET_10"
 MREAD 0 "CAV-MTOT_10"
#
MELCOR 0 EXPORT ASCII ".\DATA_CAV_Rev4.apt"
# #################################################################################
# FILE 3
# Temperatures Fuel
MREAD 0 "COR-TFU_107"; MREAD 0 "COR-TFU_111"; MREAD 0 "COR-TFU_114"; MREAD 0 "COR-TFU_116" 
MREAD 0 "COR-TFU_207"; MREAD 0 "COR-TFU_211"; MREAD 0 "COR-TFU_214"; MREAD 0 "COR-TFU_216"
MREAD 0 "COR-TFU_307"; MREAD 0 "COR-TFU_311"; MREAD 0 "COR-TFU_314"; MREAD 0 "COR-TFU_316"
MREAD 0 "COR-TFU_407"; MREAD 0 "COR-TFU_411"; MREAD 0 "COR-TFU_414"; MREAD 0 "COR-TFU_416"
MREAD 0 "COR-TFU_507"; MREAD 0 "COR-TFU_511"; MREAD 0 "COR-TFU_514"; MREAD 0 "COR-TFU_516"

MELCOR 0 EXPORT ASCII ".\DATA_TFU_Rev4.apt"
# #################################################################################
# FILE 13
# Temperatures Cladding
MREAD 0 "COR-TCL_107"; MREAD 0 "COR-TCL_111"; MREAD 0 "COR-TCL_114"; MREAD 0 "COR-TCL_116" 
MREAD 0 "COR-TCL_207"; MREAD 0 "COR-TCL_211"; MREAD 0 "COR-TCL_214"; MREAD 0 "COR-TCL_216"
MREAD 0 "COR-TCL_307"; MREAD 0 "COR-TCL_311"; MREAD 0 "COR-TCL_314"; MREAD 0 "COR-TCL_316"
MREAD 0 "COR-TCL_407"; MREAD 0 "COR-TCL_411"; MREAD 0 "COR-TCL_414"; MREAD 0 "COR-TCL_416"
MREAD 0 "COR-TCL_507"; MREAD 0 "COR-TCL_511"; MREAD 0 "COR-TCL_514"; MREAD 0 "COR-TCL_516"

MELCOR 0 EXPORT ASCII ".\DATA_TCL_Rev4.apt"
# ################################################################################
# Masses in LP
# DEBRIS MASSES in LP, in cells  RINGAXIAL
# FILE 11 # CRP
MREAD 0 "COR-MCRP_101"; MREAD 0 "COR-MCRP_102"
MREAD 0 "COR-MCRP_201"; MREAD 0 "COR-MCRP_202"
MREAD 0 "COR-MCRP_301"; MREAD 0 "COR-MCRP_302"
MREAD 0 "COR-MCRP_401"; MREAD 0 "COR-MCRP_402"
MREAD 0 "COR-MCRP_501"; MREAD 0 "COR-MCRP_502"
MREAD 0 "COR-MCRP_601"; MREAD 0 "COR-MCRP_602"
MELCOR 0 EXPORT ASCII ".\DATA_LP_MASS_CRP.apt"
# FILE 10 # INC
MREAD 0 "COR-MINC_101"; MREAD 0 "COR-MINC_102"
MREAD 0 "COR-MINC_201"; MREAD 0 "COR-MINC_202"
MREAD 0 "COR-MINC_301"; MREAD 0 "COR-MINC_302"
MREAD 0 "COR-MINC_401"; MREAD 0 "COR-MINC_402"
MREAD 0 "COR-MINC_501"; MREAD 0 "COR-MINC_502"
MREAD 0 "COR-MINC_601"; MREAD 0 "COR-MINC_602"
MELCOR 0 EXPORT ASCII ".\DATA_LP_MASS_INC.apt"
# FILE 8 # SS
MREAD 0 "COR-MSS_101"; MREAD 0 "COR-MSS_102"
MREAD 0 "COR-MSS_201"; MREAD 0 "COR-MSS_202"
MREAD 0 "COR-MSS_301"; MREAD 0 "COR-MSS_302"
MREAD 0 "COR-MSS_401"; MREAD 0 "COR-MSS_402"
MREAD 0 "COR-MSS_501"; MREAD 0 "COR-MSS_502"
MREAD 0 "COR-MSS_601"; MREAD 0 "COR-MSS_602"
MELCOR 0 EXPORT ASCII ".\DATA_LP_MASS_SS.apt"
# FILE 9 # SSOX
MREAD 0 "COR-MSSOX_101"; MREAD 0 "COR-MSSOX_102"
MREAD 0 "COR-MSSOX_201"; MREAD 0 "COR-MSSOX_202"
MREAD 0 "COR-MSSOX_301"; MREAD 0 "COR-MSSOX_302"
MREAD 0 "COR-MSSOX_401"; MREAD 0 "COR-MSSOX_402"
MREAD 0 "COR-MSSOX_501"; MREAD 0 "COR-MSSOX_502"
MREAD 0 "COR-MSSOX_601"; MREAD 0 "COR-MSSOX_602"
MELCOR 0 EXPORT ASCII ".\DATA_LP_MASS_SSOX.apt"
# FILE 7 # UO2
MREAD 0 "COR-MUO2_101"; MREAD 0 "COR-MUO2_102"
MREAD 0 "COR-MUO2_201"; MREAD 0 "COR-MUO2_202"
MREAD 0 "COR-MUO2_301"; MREAD 0 "COR-MUO2_302"
MREAD 0 "COR-MUO2_401"; MREAD 0 "COR-MUO2_402"
MREAD 0 "COR-MUO2_501"; MREAD 0 "COR-MUO2_502"
MREAD 0 "COR-MUO2_601"; MREAD 0 "COR-MUO2_602"

MELCOR 0 EXPORT ASCII ".\DATA_LP_MASS_UO2.apt"
# FILE 6 #ZR
MREAD 0 "COR-MZR_101"; MREAD 0 "COR-MZR_102"
MREAD 0 "COR-MZR_201"; MREAD 0 "COR-MZR_202"
MREAD 0 "COR-MZR_301"; MREAD 0 "COR-MZR_302"
MREAD 0 "COR-MZR_401"; MREAD 0 "COR-MZR_402"
MREAD 0 "COR-MZR_501"; MREAD 0 "COR-MZR_502"
MREAD 0 "COR-MZR_601"; MREAD 0 "COR-MZR_602"

MELCOR 0 EXPORT ASCII ".\DATA_LP_MASS_ZR.apt"
# FILE 5 # ZRO2
MREAD 0 "COR-MZRO2_101"; MREAD 0 "COR-MZRO2_102"
MREAD 0 "COR-MZRO2_201"; MREAD 0 "COR-MZRO2_202"
MREAD 0 "COR-MZRO2_301"; MREAD 0 "COR-MZRO2_302"
MREAD 0 "COR-MZRO2_401"; MREAD 0 "COR-MZRO2_402"
MREAD 0 "COR-MZRO2_501"; MREAD 0 "COR-MZRO2_502"
MREAD 0 "COR-MZRO2_601"; MREAD 0 "COR-MZRO2_602"

MELCOR 0 EXPORT ASCII ".\DATA_LP_MASS_ZRO2.apt"


# #################################################################################
MELCOR 0 CLOSE
#MELCOR 1 CLOSE
# #################################################################################


# #############################################################################
# STEADY-STATE
## MREAD 0 "COR-EFPD-RAT" 
## MREAD 0 "CVH-P_400"; MREAD 0 "CVH-MASS_400"
## MREAD 0 "CVH-TLIQ_400"; MREAD 0 "CVH-TVAP_400"  
## MREAD 0 "CVH-P_730";    MREAD 0 "CVH-P_830"      	
## MREAD 0 "CVH-TVAP_730"; MREAD 0 "CVH-TVAP_830"  
## MREAD 0 "FL-MFLOW_740"; MREAD 0 "FL-MFLOW_840"	
## MREAD 0 "CVH-TLIQ_700"; MREAD 0 "CVH-TLIQ_800"  
## MREAD 0 "FL-MFLOW_700"; MREAD 0 "FL-MFLOW_800"   
## MREAD 0 "CVH-TLIQ_500"; MREAD 0 "CVH-TLIQ_600"  
## MREAD 0 "CVH-TLIQ_570"; MREAD 0 "CVH-TLIQ_670"  
## MREAD 0 "CVH-TLIQ_300"; MREAD 0 "CVH-TLIQ_340"; MREAD 0 "CVH-TLIQ_310"; MREAD 0 "CVH-TLIQ_320"   
## 
## MREAD 0 "FL-MFLOW_580"; MREAD 0 "FL-MFLOW_680"; MREAD 0 "FL-MFLOW_500"; MREAD 0 "FL-MFLOW_600"   
## MREAD 0 "FL-MFLOW_301"
## MELCOR 0 EXPORT CSV "SS_case00012k.csv"
## #EXIT
## MELCOR 0 CLOSE
## 
## 
## MREAD 1 "CFVALU_2000"; MREAD 1 "CFVALU_2001"
## MREAD 1 "CFVALU_2002"; MREAD 1 "CFVALU_2003"
## MREAD 1 "CFVALU_2004"; MREAD 1 "CFVALU_2005"
## MREAD 1 "CFVALU_2011"; MREAD 1 "CFVALU_2012"
## 
## MELCOR 0 EXPORT CSV "SS_case00012k_CF.csv"
## MELCOR 1 CLOSE
## #MELCOR 0 EXPORT ASCII "SS.apt" \
#
# #############################################################################
