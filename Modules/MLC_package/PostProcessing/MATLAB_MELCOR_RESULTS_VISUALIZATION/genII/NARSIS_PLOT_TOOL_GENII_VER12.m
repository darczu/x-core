% Ploting script version 27-08-2020. Attempt to make it as general as possbile
% Modified version of the NARSIS_EDF_READ_VER9
% Here it is for Forever epeirment orginally it was developed for Gen III plant
% NARSIS Project % Plot Forever code
% TO USE THIS FILE YOU NEED XCORE toolbox package installed
tic; clc; close all; clear; format short g
% FOLDERS ===================================================================
case_name = 'Gen II NPP Calculations'
%root_folder = 'C:\CloudStation\ROOT\NARSIS-GenII'
root_folder = 'C:\CloudStation\ROOT\NARSIS-GenII\FINAL_Rev1'
post_folder = 'C:\CloudStation\NARSIS\MODELE_AND_RESEARCH\SCRIPTS\MATLAB_XCore\x-core\Database\MLC_package\PostProcessing\MATLAB_MELCOR_RESULTS_VISUALIZATION\genII'

% Results sub_folder

%exp_file = 'EXP_FOREVER_DATA1.mat'
%code1 ='M2.2.15254'; %code1 ='M2.2.11932'; 
code1 ='M2.2.9541'; %code1 = 'MELCOR2.1.6341'; %code1 ='M2.2.9541';

% SETUP ===================================================================
flag1 		= 1; %PLOT 1-YES 0 - NO
flagPLOT = flag1; %Plot figures/charts in windows
flag2 		= 1; %SAVE FIGURE 1-YES 0-NO;  

flag3 		= 1;   %COMPARISON
%flag3 		= 0;   %SINGLE PLOT

% Creates single plot
% flag3 NEW: 0 - save each to its own folder, 1 - save to new folder COMPARISON
% 0 - only plot no comparison, 1 - it is comparison

flag3a     = 0;  %0  - same figure numbers; 1 - new case increases figure numbers % 1- allows to compare matlab figures
%flag4    	= 1; %1 - Save FIG type file

flag_hours  = 0; %1 - hours 0 - seconds - input data time scale
title_flag 	= 2 ; %0 - no title; 1 - title with file localization (subfolder) - helps to identificate file; 2 - title with information about what is on the plot 
%flag_info1  = 0;  %Add info about folder and case; 1 - old with folder; 2 - reduced with mlcfilename, 0 - no
flag_info1  = 3;  %Add case name when flag3 == 0

flag_additional1 = 0; %plot normal plot or additional plots
flag_additional = 1;

%flagTH 		= 1; % TH results
flagCases 	= 1; %1 - read from local folder or absolute, 0- read from given folder explicity in the file - file with list of folders
flagEDF 	= 1; %1 Use external file
flagEXP 	= 0; %Load expeirmental data
flagINJ  = 1;
flag_SOU = 1;  %Plot SOU output
flag_REC = 1;  %Plot normal recirculation output

%flag_allplottime = 1; % 1 - yes, plots only for plottime, 0 - plots with variable times set by user
flag_allplottime = 0; % 1 - yes, plots only for plottime, 0 - plots with variable times set by user
			if flag_allplottime  == 1
				results_folder = 'Results_long4';
			else
				results_folder = 'Results4';
			end

% EXPERIMENTAL DATA========================================================
% load experimental data - run mat file with experiment data
%if flagEXP == 1
%	load([post_folder,'\', exp_file]);
%end
%
% CASES LIST ===================================================================
%
%%% cases_list 			= {  'SBO#2.2b' 'SBO#2.3' 'SBO#2.4' 'CREEP#2.1'}
%%% cases_list_names 		= {  'SBO#2.2b' 'SBO#2.3' 'SBO#2.4' 'CREEP#2.1'}
%%% flag_accident 		= { 2 2 2 2 }  
%%% cases_output_type 	= { 1 1 1 1} 
%
% ===================================================================
% cases_list 			= {'SBO#2.2a'}
% cases_list_names 	= {'SBO#2.2a_r1'}
% flag_accident 		= { 2 }       %1 - LOCA  2 - SBO
% cases_output_type 	= { 1 }       %type 1 is apt file   type 2 is EDF file, 3 new, 4 newer to be selected by the user
%
% TWO CASES
% cases_list 			= {  'SBO#2.4a'  'SBO#2.4b'}
% cases_list_names 		= {  'SBO#2.4a_'  'SBO#2.4b_'}
% flag_accident 		= { 2 2 }  
% cases_output_type 	= { 1 1 } 

% cases_list 			= { 'SBO#1' 'SBO#2'  'SBO#2.1' 'SBO#2.2b'}
% cases_list_names 		= { 'SBO#1_' 'SBO#2_'  'SBO#2.1_' 'SBO#2.2b_'}
% flag_accident 		= { 2 2 2 2}  
% cases_output_type 	= { 1 1 1 1} 

 cases_list 			= { 'SBO#2.2b' 'SBO#2.2e'  'SBO#2.2e_M2211' 'SBO#2.2e_M2215'}
 cases_list_names 		= { 'SBO#2.2_M2291_old' 'SBO#2.2_M2291'  'SBO#2.2_M2211' 'SBO#2.2_M2215' }
 flag_accident 		= { 2 2 2 1}  
 cases_output_type 	= { 1 1 1 1} 



% cases_list 			= { 'SBO#2' 'SBO#2.3a'  'SBO#2.4a' }
% cases_list_names 		= { 'SBO#2_' 'SBO#2.3a_'  'SBO#2.4a_' }
% flag_accident 		= { 2 2 2 }  
% cases_output_type 	= { 1 1 1 } 
% ===================================================================
% =================================================================== 
% CASES LIST FOR COMPARISONS=========================================
% ===================================================================
%% cases_list 			= {  'SBO#1' 'SBO#1.1' 'SBO#1.2' 'SBO#1.3'}
%% cases_list_names 		= {  'SBO#1_' 'SBO#1.1_' 'SBO#1.2_' 'SBO#1.3_' }
%% flag_accident 		= { 2 2 2 2 }  
%% cases_output_type 	= { 1 1 1 1} 
% ===================================================================
%% cases_list 			= { 'CREEP#1' 'CREEP#2' 'CREEP#2.1'   'SBO#1.3'}
%% cases_list_names 			= { 'CREEP#1_' 'CREEP#2_'   'CREEP#2.1_'  'SBO#1.3_'}
%% flag_accident 		= { 2 2 2 2 }  
%% cases_output_type 	= { 1 1 1 1} 
% ===================================================================
%% cases_list 			= { 'CREEP#1' 'CREEP#2' 'CREEP#2.1'   }
%% cases_list_names 			= { 'CREEP#1_' 'CREEP#2_'   'CREEP#2.1_' }
%% flag_accident 		= { 2 2 2  }  
%% cases_output_type 	= { 1 1 1 } 
% ===================================================================
%% cases_list 			= {  'SBO#1' 'SBO#2' 'SBO#2.1' 'SBO#2.2b' 'SBO#2.3a' }
%% cases_list_names 		= {  'SBO#1_' 'SBO#2_' 'SBO#2.1_' 'SBO#2.2_' 'SBO#2.3_'}
%% flag_accident 		= { 2 2 2 2 2}  
%% cases_output_type 	= { 1 1 1 1 1} 
  
% cases_list 			= {   'SBO#2' 'SBO#2.1' 'SBO#2.2b' 'SBO#2.3a'  'SBO#2.4a' }
% cases_list_names 		= {  'SBO#2_' 'SBO#2.1_' 'SBO#2.2_' 'SBO#2.3_'  'SBO#2.4_'}
% flag_accident 		= { 2 2 2 2 2}  
% cases_output_type 	= { 1 1 1 1 1} 
% ===================================================================
 %% cases_list 			= {   'SBO#2'  'SBO#2.3a' 'SBO#2.4a'}
 %% cases_list_names 		= {   'SBO#2_' 'SBO#2.3_' 'SBO#2.4_'}
 %% flag_accident 		= { 2 2 2 }  
 %% cases_output_type 	= { 1 1 1 } 
  
% ===================================================================
% cases_list 			= {   'SBO#2'  'SBO#2.1' 'SBO#2.2b'}
% cases_list_names 		= {   'SBO#2_' 'SBO#2.1_' 'SBO#2.2_'}
% flag_accident 		= { 2 2 2 }  
% cases_output_type 	= { 1 1 1 } 


% ===================================================================
%% cases_list 			= { 'CLL#1' 'CLL#4' 'CLL#4.3'}
%% cases_output_type 	= {1 1 1}  
%% cases_list_names 	= { 'CLL#1_' 'CLL#4_' 'CLL#4.3_'}   
%% flag_accident 		= {1 1 1}  
% ===================================================================
%% cases_list 			= { 'CLL#1' 'CLL#1.1' 'HLL#1' 'HLL#1.1'}
%% cases_output_type 	= {1 1 1 1} 
%% cases_list_names    = { 'CLL#1_' 'CLL#1.1_' 'HLL#1_' 'HLL#1.1_'}
%% flag_accident 		= {1 1 1 1}  	
% ===================================================================
%% cases_list 		= {'CLL#2' 'CLL#2.1' 'CLL#2.2' 'CLL#2.3e'}
%% cases_output_type 	= {1 1 1 1 } 
%% cases_list_names    = {'CLL#2_' 'CLL#2.1_' 'CLL#2.2_' 'CLL#2.3_'}
%% flag_accident 		= {1 1 1 1 }  	
% ===================================================================
%% cases_list 			= { 'CLL#3e' 'CLL#3.1e' 'CLL#3.2e' 'CLL#3.3e'}
%% cases_output_type 	= {1 1 1 1} 
%% cases_list_names    = { 'CLL#3_' 'CLL#3.1_' 'CLL#3.2_' 'CLL#3.3_'}
%% flag_accident 		= {1 1 1 1}  
% ===================================================================
%% cases_list 		= { 'CLL#4' 'CLL#4.1' 'CLL#4.2' 'CLL#4.3'}
%% cases_output_type 	= {1 1 1 1} 
%% cases_list_names   = { 'CLL#4_' 'CLL#4.1_' 'CLL#4.2_' 'CLL#4.3_'}
%% flag_accident 		= {1 1 1 1}  
% ===================================================================
%% cases_list 			= { 'CLL#4.3' 'CLL#5a'}
%% cases_output_type 	= { 1 1  } 
%% cases_list_names    = { 'CLL#4.3_' 'CLL#5_'}
%% flag_accident 		= { 1 1  }  	
% ===================================================================
%%% cases_list 			= {'CLL#1' 'CLL#4.3' 'CLL#5a'}
%%% cases_output_type 	= { 1 1 1 } 
%%% cases_list_names    = {'CLL#1_' 'CLL#4.3_' 'CLL#5_'}
%%% flag_accident 		= { 1 1  1}  	
% ===================================================================
%%  cases_list 			= { 'CLL#5a' 'CLL#5.1a'}
%%  cases_output_type 	= { 1 1  } 
%%  cases_list_names    = { 'CLL#5_' 'CLL#5.1_'}
%%  flag_accident 		= { 1 1  } 
%
% ===================================================================
% ===================================================================
% ===================================================================
%%  % cases_list 			= { 'CREEP#1' 'CREEP#2'  'SBO#1.1' 'SBO#1.2' 'SBO#1.3' 'SBO#2' 'SBO#2.1' 'SBO#2.2'}
%%  % cases_list_names 	= {'CREEP#1rev1' 'CREEP#2rev1'  'SBO#1.1rev1' 'SBO#1.2rev1' 'SBO#1.3rev1' 'SBO#2rev1' 'SBO#2.1rev1' 'SBO#2.2rev1'}  
%%  % flag_accident 		= { 2 2 2 2 2 2 2 2 }  	%1 - LOCA  2 - SBO
%%  % cases_output_type 	= { 1 1 1 1 1 1 1 1 } 
%%  % ===================================================================
%%  
%%  %{'CLL#4' 'CLL#4.1' 'CLL#4.2' 'CLL#4.3' 'CLL#5a' 'CLL#5.1a' 'HLL#1' 'HLL#1.1'}
%%  %{ 'CLL#1.1' 'CLL#2' 'CLL#2.1' 'CLL#2.2' 'CLL#2.3e' 'CLL#3.1e' 'CLL#3.2e' 'CLL#3.3e'}
%%  
%%  %{'CREEP#1rev1' 'CREEP#2rev1' 'SBO#1rev1' 'SBO#1.1rev1' 'SBO#1.2rev1' 'SBO#1.3rev1' 'SBO#2rev1' 'SBO#2.1rev1' 'SBO#2.2rev1'}  
%%  %{'CLL#4rev1' 'CLL#4.1rev1' 'CLL#4.2rev1' 'CLL#4.3rev1' 'CLL#5rev1' 'CLL#5.1rev1' 'HLL#1rev1' 'HLL#1.1rev1'}
%%  %{ 'CLL#1.1rev1' 'CLL#2rev1' 'CLL#2.1rev1' 'CLL#2.2rev1' 'CLL#2.3rev1' 'CLL#3.1rev1' 'CLL#3.2rev1' 'CLL#3.3rev1'}   
%%  
%%  %cases_output_type 	= { 1 1 1 1 1 1 1 1 1}  %type 1 is apt file   type 2 is EDF file, 3 new, 4 newer to be selected by the user
%%  %flag_accident 		= { 1 1 1 1 1 1 1 1 1}  	%1 - LOCA  2 - SBO


% PLOT ALL
%%'CLL#1' 'CLL#1.1' 'HLL#1' 'HLL#1.1' 'CLL#2' 'CLL#2.1' 'CLL#2.2' 'CLL#2.3e' 'CLL#3e' 'CLL#3.1e' 'CLL#3.2e' 'CLL#3.3e' 'CLL#4' 'CLL#4.1' 'CLL#4.2' 'CLL#4.3' 'CLL#5a' 'CLL#5.1a'}
%%cases_list_names 	= {'CLL#1' 'CLL#1.1' 'HLL#1' 'HLL#1.1' 'CLL#2' 'CLL#2.1' 'CLL#2.2' 'CLL#2.3e' 'CLL#3e' 'CLL#3.1e' 'CLL#3.2e' 'CLL#3.3e' 'CLL#4' 'CLL#4.1' 'CLL#4.2' 'CLL#4.3' 'CLL#5a' 'CLL#5.1a'}
%%flag_accident 		= { 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1}  	%1 - LOCA  2 - SBO
%%cases_output_type 	= { 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1} 
%%%
%%cases_list 			= { 'CREEP#1' 'CREEP#2' 'CREEP#2.1'   'SBO#1'  'SBO#1.1' 'SBO#1.2' 'SBO#1.3' 'SBO#2' 'SBO#2.1' 'SBO#2.2b'  'SBO#2.3a'  'SBO#2.4a'   }
%%cases_list_names 	= { 'CREEP#1' 'CREEP#2' 'CREEP#2.1'    'SBO#1'  'SBO#1.1' 'SBO#1.2' 'SBO#1.3' 'SBO#2' 'SBO#2.1' 'SBO#2.2b'  'SBO#2.3a'  'SBO#2.4a'  }
%%flag_accident 		= { 1 1 1 1 1 1 1 1 1 1 1 1}  	%1 - LOCA  2 - SBO
%%cases_output_type 	= { 1 1 1 1 1 1 1 1 1 1 1 1} 






% ===================================================================

cases_control = ones(1,numel(cases_list)); 
%cases_control 		= [1 1 1 1 ] 	  %Use if you want to plot only some cases
last_case = numel(cases_list);
%last_case = 4;  %use value when it is necesarry
plot_omit = [];
%plot_omit = 1:1:60;
%plot_omit([45, 57, 58, 6, 15, 23, 31]) = []; %DO NOT OMIT THESE 
%plot_omit 			= [1:28,30:56]  %not omit   45, 57, 58, 6, 15, 23, 31
if flag3 == 0	
   cases_list_names1 = {};
   cases_list_names1(1:numel(cases_list)) = {''};
	%cases_list_names1 	= {'' };
	%cases_list_names1 	= {'' '' ''};
	%cases_list_names1 	= {'' '' '' '' '' '' '' '' ''};
else  cases_list_names1 	= cases_list_names; end


% ===================================================================
%cases_output_type = { 1 1 }; % 2 2}  %type 1 is apt file   type 2 is EDF file, 3 new, 4 newer to be selected by the user
%cases_output_type = { 1 1 2 2}  %type 1 is apt file   type 2 is EDF file, 3 new, 4 newer to be selected by the user

% CASES ===================================================================
if flagCases == 1
    folder = { root_folder }; %folder with results
	subfolder = cases_list;   %folder for particular case (result)
	for i=1:numel(cases_list)
		cases(i) = {[folder{1} '\' subfolder{i}]}; 
	end
    %cases_names = {[folder{1} '_' subfolder{1}]};  % 'C7E'  'C7F'}
	cases_names = cases_list_names;  
else
    %run FILES_DATA.m    %Read file names - FILE WITH EXPLICIT NAMES OF FILES WITH DATA
	%% file_final1 = 'Q:\ROOT\NARSIS-Forever\test14'; %Here it is given expicity but it should be in FILES_DATA file
%	file_final2 = {file_final1}; 
%    cases = file_final2; 
%    cases_names = cases_list_names;  
end

% if numel(cases) == 1 
%     flag3 = 0;  % 0 -save to 1st folder; 1 - save to new folder
% end
% if numel(cases) > 1 
%     flag3 = 1;  % 0 -save to 1st folder; 1 - save to new folder
% end

% ===================================================================
% COLORS AND LABELS SETUP
run colormaps_and_labels.m
if flag3 == 0
	col2 = col3;
end	


% TIME #1 ===================================================================
plottime = 250000		 %xlim([0 plottime./Tconst]); %60*3600;  %time end for plotting %timetime = 30000/3600; 
if flag_allplottime == 1
	LOCAshorttime 	= plottime; 	  	LOCAmediumtime 	= plottime; 	LOCAlongtime = plottime;	LOCAshort2time = plottime;
	SBOshorttime 	= plottime;  	SBOmediumtime 	= plottime; 	SBOlongtime  = plottime;
else
	%LOCAshorttime 	= 5000; 	%for 4 and 5  	
    LOCAshorttime 	= 600; 	  	
    %LOCAmediumtime 	= 15000; 	% for 4 and 5
    LOCAmediumtime 	= 20000; 	 
    LOCAlongtime = 100000;  LOCAshort2time = 5000;
	SBOshorttime 	= 20000;  	SBOmediumtime 	= 50000; 	SBOlongtime  = 100000;  
end



% TIME #2 ===================================================================
%tend = plottime; % czas konca obliczen %8; %7 % 24 %66.0;  calculation time %<================================= TIME
if(flag_hours == 1)  
	Tconst = 3600;   labelTime = labelHours;
else 
	Tconst = 1.0;  labelTime= labelSeconds;
end  %time conversion from sec to hours

LOCAshorttime 	= LOCAshorttime./Tconst; 	  	
LOCAmediumtime 	= LOCAmediumtime./Tconst; 	  	 	
LOCAlongtime 	= LOCAlongtime./Tconst; 	  	
SBOshorttime 	= SBOshorttime./Tconst; 	  	
SBOmediumtime 	= SBOmediumtime./Tconst; 	  	
SBOlongtime  	= SBOlongtime./Tconst; 	  	

% ===================================================================


a  = 0; %initialization, a is plotted figure number, it does not increase when figure is not plotted
%a1 = -1; %global plot number
% PLOTTING ===================================================================
% This loop will allow to compre different results
for k_num=1:numel(cases)

	if cases_control(k_num) == 0
		%do not draw
	else
		% ===================================================================
		% Time setup	
		if flag_accident{k_num} == 1
			shorttime = LOCAshorttime; mediumtime = LOCAmediumtime; longtime = LOCAlongtime;
		elseif flag_accident{k_num} == 2
			shorttime = SBOshorttime; mediumtime = SBOmediumtime; longtime = SBOlongtime;
		end

		case1 =  cases{k_num};  % current case folder
		case1_name = cases_names{k_num}; % current case name
		% ===================================================================
		if flagEDF == 1 
			run EDF_LOAD_GENII_v3.m; 
		end
		% ===================================================================
		%if flagTH ==1
			if flag3a == 1   %plot with increasing figure numbers
				a = a ; 
				plot_omit = plot_omit + a;
			else
				a = 0;		%plot with the same figure numbers
			end
			%	a1 = 0;
			if(flag1 == 1) %PLOT OR NOT
				run PLOT_CORE_GENII_v3.m;
				run PLOT_RCS_GENII_v3.m;
				run PLOT_CONT_GENII_v3.m;
				run PLOT_CAV_GENII_v3.m;
				run PLOT_SIS_GENII_v3.m;
				run PLOT_TEMPS_v3.m;
				
				if flag_additional == 1
					run PLOT_TH2.m;
				end
				
			end
		%end
		% ===================================================================
	end
end    

% ===================================================================
toc