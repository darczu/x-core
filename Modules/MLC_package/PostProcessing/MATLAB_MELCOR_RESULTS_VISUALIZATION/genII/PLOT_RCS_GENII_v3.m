% ===================================================================
% ===================================================================
% FIGURE - RCS Pressures
% ===================================================================
a=a+1; 
if (sum(plot_omit == (a)) == 0)%plot omit
		content = {'Pressurizer and SG Pressures'};
		run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{1};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		if ~sum(k == emptyfile)
			%x0 = EDF.data{k}(:,1)./Tconst;        
			x0 = EDF.data{k}(:,1)./Tconst;       
			y1 = EDF.data{k}(:,3); %
			y2 = EDF.data{k}(:,7); %
			y3 = EDF.data{k}(:,8); %
			% plot(x0,y1,lw,1,ms,2,cl,  cmapdyn(c,:));  labels = [labels, ['TVAP_LP_'  cases_list{k_num}] ];c = c+1
			% plot(x0,y1,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR' ];
			plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'PRZ'] ];
			plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'--'); labels = [labels, [cases_list_names1{k_num} 'SG-B'] ];
			plot(x0,y3,lw,1,ms,2,cl,col2(k_num,:),ls,':');  labels = [labels, [cases_list_names1{k_num} 'SG-I'] ];
		end			
	end	
		ylabel(labelP);  xlabel(labelTime); %xlabel('Time, [s]'); % ylim([0 16e6]); xlim([-20 200]);	
		legend('Location','southwest');
		legend('Location','southeast');
		xlim([0 mediumtime]);	
		run standard_plot2v4.m;
end
% ===================================================================
a=a+1; 
if (sum(plot_omit == (a)) == 0)%plot omit
		content = {'Pressurizer Pressure'};
		run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{1};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		if ~sum(k == emptyfile)
			%x0 = EDF.data{k}(:,1)./Tconst;        
			x0 = EDF.data{k}(:,1)./Tconst;       
			y1 = EDF.data{k}(:,3); %

			% plot(x0,y1,lw,1,ms,2,cl,  cmapdyn(c,:));  labels = [labels, ['TVAP_LP_'  cases_list{k_num}] ];c = c+1
			% plot(x0,y1,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR' ];
			plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'PRZ'] ];

		end			
	end	
		ylabel(labelP);  xlabel(labelTime); %xlabel('Time, [s]'); % ylim([0 16e6]); xlim([-20 200]);	
			legend('Location','southwest');
		xlim([0 shorttime]);	
		run standard_plot2v4.m;
end
% ===================================================================
a=a+1; 
if (sum(plot_omit == (a)) == 0)%plot omit
		content = {'SG Pressures'};
		run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{1};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		if ~sum(k == emptyfile)
			%x0 = EDF.data{k}(:,1)./Tconst;        
			x0 = EDF.data{k}(:,1)./Tconst;       
			y2 = EDF.data{k}(:,7); %
			y3 = EDF.data{k}(:,8); %
			plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'--'); labels = [labels, [cases_list_names1{k_num} 'SG-B'] ];
			plot(x0,y3,lw,1,ms,2,cl,col2(k_num,:),ls,':');  labels = [labels, [cases_list_names1{k_num} 'SG-I'] ];
		end			
	end	
		ylabel(labelP);  xlabel(labelTime); %xlabel('Time, [s]'); % ylim([0 16e6]); xlim([-20 200]);	
		legend('Location','southeast');
		xlim([0 shorttime]);	
		run standard_plot2v4.m;
end
% ===================================================================
% RCS - Temperatures
% ===================================================================	
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
	% plot(x0,y1,lw,1,ms,2,cl,  cmapdyn(c,:));  labels = [labels, ['TVAP_LP_'  cases_list{k_num}] ];c = c+1
		content = {'PRZ and SG Gas Temperatures'};
		run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{1};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			%x0 = EDF.data{k}(:,1); %time       
			y1 = EDF.data{k}(:,5); % PRZ T
			y2 = EDF.data{k}(:,9); % SG-B 
			y3 = EDF.data{k}(:,10); % SG-I
			%y4 = EDF.data{k}(:,22); % UP Exit
			%plot(x0,y1,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR' ];
			plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'PRZ'] ];
			plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'SG-B'] ];
			plot(x0,y3,lw,1,ms,2,cl,col2(k_num,:),ls,':');  labels = [labels, [cases_list_names1{k_num} 'SG-I'] ];
		%	plot(x0,y4,lw,1,ms,2);  labels = [labels, [cases_list_names1{k_num} 'Core_Exit'] ];
		end			
	end		
		ylabel(labelT);  xlabel(labelTime);
		%xlabel('Time, [s]');  %xlim([-20 200]);	
		legend('Location','southeast'); 
		xlim([0 shorttime]);
		run standard_plot2v4.m;
end
% ===================================================================	
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
	% plot(x0,y1,lw,1,ms,2,cl,  cmapdyn(c,:));  labels = [labels, ['TVAP_LP_'  cases_list{k_num}] ];c = c+1
		content = {'PRZ Gas Temperatures'};
		run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{1};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			y1 = EDF.data{k}(:,5); % PRZ T
			plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'PRZ'] ];
		end			
	end		
		ylabel(labelT);  xlabel(labelTime);
		legend('Location','southeast'); 
			xlim([0 mediumtime]);
		run standard_plot2v4.m;
end
% ===================================================================	
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
		content = {'SG Gas Temperatures'};
		run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{1};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			y2 = EDF.data{k}(:,9); % SG-B 
			y3 = EDF.data{k}(:,10); % SG-I
			plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'SG-B'] ];
			plot(x0,y3,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'SG-I'] ];
		end			
	end		
		ylabel(labelT);  xlabel(labelTime);
		legend('Location','northwest'); 
		legend('Location','southeast');
			xlim([0 mediumtime]);
		run standard_plot2v4.m;
end
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
		content = {'Core and Upper Plenum Gas Temperatures'};
		run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{1};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;          
			y1 = EDF.data{k}(:, 19); % UP
			y2 = EDF.data{k}(:, 20); %Core 
			
			plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'UP'] ];
			plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'Core'] ];
		end			
	end		
		ylabel(labelT);  xlabel(labelTime);
		legend('Location','northwest'); 
		legend('Location','northeast');
			xlim([0 mediumtime]);
		run standard_plot2v4.m;
end
% ===================================================================




% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
		content = {'Hot Leg Gas Temperatures'};
		run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{1};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;          
			y1 = EDF.data{k}(:, 13); % Broken
			y2 = EDF.data{k}(:, 14); % Intact
			plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'Broken'] ];
			plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'Intact'] ];
		end			
	end		
		ylabel(labelT);  xlabel(labelTime);
		legend('Location','northwest'); 
		legend('Location','northeast');
			xlim([0 mediumtime]);
		run standard_plot2v4.m;
end
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
		content = {'Core and Upper Plenum Liquid Temperatures'};
		run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{1};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;          
			y1 = EDF.data{k}(:, 17); % UP
			y2 = EDF.data{k}(:, 18); %Core 
			plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'UP'] ];
			plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'Core'] ];
		end			
	end		
		ylabel(labelT);  xlabel(labelTime);
		legend('Location','northwest'); 
		legend('Location','northeast');
			xlim([0 mediumtime]);
		run standard_plot2v4.m;
end

% ===================================================================
% Water inventory
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
	content = {'RCS Water Inventory and Integrated Flows'};
		run standard_plot1v4.m;
	
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{4}; k = detectfile(mlcfile,EDF.file);  %MELCOR
		
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			y1 = EDF.data{k}(:,3) + EDF.data{k}(:,5);	%HL Break Flow
			%y1 = sum(EDF.data{k}(:,4:9),2); %Break I flow
			y2 = EDF.data{k}(:,8) + EDF.data{k}(:,9); %Acc1 + Acc2	
			%y3 = EDF.data{k}(:,11);  % (10)1998 - RCS water, 1999 RCS+SL+PRZ water (11)
			y4 = EDF.data{k}(:,11); % (10) 1998 - RCS water, (11) 1999 RCS+SL+PRZ water 
			
			
			plot(x0,y4,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num}  'RCS+PRZ+SL'] ];
			plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num}  'ACC'] ];
			plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-.');  labels = [labels, [cases_list_names1{k_num}  'BREAK'] ];		
			%plot(x0,y3,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num}	  'RCS'] ];
		end
	end
	
		ylabel(labelM); xlabel(labelTime); %xlabel('Time, [s]');  %xlim([-20 200]);	
		legend('Location','northeast');
		xlim([0 shorttime]);
		
		run standard_plot2v4.m;
end
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
	content = {'RCS Water Inventory and ACC Water'};
		run standard_plot1v4.m;
	
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{4}; k = detectfile(mlcfile,EDF.file);  %MELCOR
		
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			%y1 = EDF.data{k}(:,3) + EDF.data{k}(:,5);	%HL Break Flow
			%y1 = sum(EDF.data{k}(:,4:9),2); %Break I flow
			y2 = EDF.data{k}(:,10); %RCS no PRZ  
			y3 = EDF.data{k}(:,11); %RCS 13 with PRZ
			
			plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num}	  'RCS'] ];
			plot(x0,y3,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num}  'RCS+PRZ+SL'] ];
			
		end
	end
	
		ylabel(labelM); xlabel(labelTime); %xlabel('Time, [s]');  %xlim([-20 200]);	
		legend('Location','northeast');
		xlim([0 mediumtime]);
		
		run standard_plot2v4.m;
end
% ===================================================================

% ===================================================================
% Pressurizer Water Level
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
		content = {'PRZ Water Level'};
		run standard_plot1v4.m;

	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{14};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;          
			y1 = EDF.data{k}(:,8); % IN RELATION TO THE BOTTOM OF THE PRZ			
			plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num}		'CVH'] ];
		end
		
	end	
		ylabel('Level, [m]'); xlabel(labelTime);
		ylim([0.0 9.0]);  
		legend('Location','northeast');
	
	if flag3 == 1
		if k_num == numel(cases)	
			yline(8.678); 	 
			yline(0.591750);
			yline(7.251750);
			
		end			
	else
		yline(8.678); %labels = [labels, [cases_list_names1{k_num}		'CVH Bottom'] ];
		yline(0.591750); %labels = [labels, [cases_list_names1{k_num}		'NL 0%'] ]; %0% %0%
		yline(7.251750); %labels = [labels, [cases_list_names1{k_num}		'NL 100%'] ]; %100%
	end
	
		xlim([0 mediumtime]);
		run standard_plot2v4.m;
end

% ===================================================================
% Pressurizer Water Level
% ===================================================================

a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
		content = {'PRZ Water Level'};
		run standard_plot1v4.m;

	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{14};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;          
			y1 = EDF.data{k}(:,8); % IN RELATION TO THE BOTTOM OF THE PRZ			
			plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num}		'CVH'] ];
		end
		
	end	
		ylabel('Level, [m]'); xlabel(labelTime);
		ylim([0.0 9.0]);  
		legend('Location','northeast');
	
	if flag3 == 1
		if k_num == numel(cases)	
			yline(8.678); 	 
			yline(0.591750);
			yline(7.251750);
			
		end			
	else
		yline(8.678); %labels = [labels, [cases_list_names1{k_num}		'CVH Bottom'] ];
		yline(0.591750); %labels = [labels, [cases_list_names1{k_num}		'NL 0%'] ]; %0% %0%
		yline(7.251750); %labels = [labels, [cases_list_names1{k_num}		'NL 100%'] ]; %100%
	end
	
		xlim([0 shorttime]);
		run standard_plot2v4.m;
end

% ===================================================================



% ===================================================================
% SG Level 57
a=a+1; 
if (sum(plot_omit == (a)) == 0)%plot omit
		content = {'SG Collapsed Water Level'};
		run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{14};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		if ~sum(k == emptyfile)
			%x0 = EDF.data{k}(:,1)./Tconst;        
			x0 = EDF.data{k}(:,1)./Tconst;       
			y2 = EDF.data{k}(:,2); %	'SG_CLEV-B'         2002   2 
			y3 = EDF.data{k}(:,3); % 'SG_CLEV-I'         2003    3  
			plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'-'); labels = [labels, [cases_list_names1{k_num} 'SG-B'] ];
			plot(x0,y3,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'SG-I'] ];
		end			
	end	
		ylabel('Level, [kg]');  xlabel(labelTime); %xlabel('Time, [s]'); % ylim([0 16e6]); xlim([-20 200]);	
		legend('Location','southeast');
		xlim([0 shorttime]);  ylim([0 inf]);		
		run standard_plot2v4.m;
end
% ===================================================================
% SG Mass 58
a=a+1; 
if (sum(plot_omit == (a)) == 0)%plot omit
		content = {'SG Water Mass'};
		run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{1};
		k = detectfile(mlcfile,EDF.file);  %MELCOR
		if ~sum(k == emptyfile)
			%x0 = EDF.data{k}(:,1)./Tconst;        
			x0 = EDF.data{k}(:,1)./Tconst;       
			y2 = EDF.data{k}(:,12); % 2001  SG-B mass (12), 2000 SG-I mass (11)  //   12 - 'SG_MASS_POOL-B'   2001   
			y3 = EDF.data{k}(:,11); % 11 - 'SG_MASS_POOL-I'   2000  
			plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'-'); labels = [labels, [cases_list_names1{k_num} 'SG-B'] ];
			plot(x0,y3,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'SG-I'] ];
		end			
	end	
		ylabel('Mass, [kg]');  xlabel(labelTime); %xlabel('Time, [s]'); % ylim([0 16e6]); xlim([-20 200]);	
		legend('Location','southeast');
		xlim([0 shorttime]); ylim([0 inf]);	
		run standard_plot2v4.m;
end
% ===================================================================








