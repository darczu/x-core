% ===================================================================
% Integrated Water inventory
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
	content = {'RCS Integrated Water Inventory'};
		run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{4}; k = detectfile(mlcfile,EDF.file);  %MELCOR
		
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			%y1 = EDF.data{k}(:,3) + EDF.data{k}(:,5);	%HL Break Flow
			%y1 = sum(EDF.data{k}(:,4:9),2); %Break I flow
			%y2 = EDF.data{k}(:,8) + EDF.data{k}(:,9); %Acc1 + Acc2	
			y3 = EDF.data{k}(:,11); 
			
			%plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-.');  labels = [labels, [cases_list_names1{k_num}  'BREAK'] ];
			plot(x0,y3,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num}	  'RCS+PRZ+SL'] ];
		end
	end
		ylabel(labelM); xlabel(labelTime); %xlabel('Time, [s]');  %xlim([-20 200]);	
		legend('Location','northeast');
		xlim([0 mediumtime]);
		run standard_plot2v4.m;
end

% ===================================================================
% Integrated Accumulators
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
	%content = {'RCS Water Inventory and Integrated Flows'};
	content = {'ACC Integrated Water Inventory'};
		run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{4}; k = detectfile(mlcfile,EDF.file);  %MELCOR
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			%y1 = EDF.data{k}(:,3) + EDF.data{k}(:,5);	%HL Break Flow
			y2 = EDF.data{k}(:,8) + EDF.data{k}(:,9); %Acc1 + Acc2	
			plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num}  'ACC'] ];
		end
	end
		ylabel(labelM); xlabel(labelTime); %xlabel('Time, [s]');  %xlim([-20 200]);	
		legend('Location','northeast');
		xlim([0 mediumtime]);
		run standard_plot2v4.m;
end





% ===================================================================
if flagINJ == 1
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit	
	content = {'LPIS Injection Rate - Active Mode'};
	run standard_plot1v4.m;
	mlcfile = EDF.file{16};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,2); 
		y2 = EDF.data{k}(:,4) + EDF.data{k}(:,6);
		
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'LPIS1 RPV' ] ];
		plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'LPIS2' ] ];
	end
		ylabel('Flow Rate, [kg/s]'); xlabel(labelTime); %xlim([-1 60]); %ylim([0 ])
		legend('Location','northeast');
		xlim([0 longtime]);	
	run standard_plot2v4.m;
end
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit	
	content = {'LPIS Integrated Flow - Active Mode'};
	run standard_plot1v4.m;
	mlcfile = EDF.file{16};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,3); 
		y2 = EDF.data{k}(:,5) + EDF.data{k}(:,7);
		
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'LPIS1 RPV' ] ];
		plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'LPIS2' ] ];
	end
		ylabel('Mass, [kg]'); xlabel(labelTime); %xlim([-1 60]); %ylim([0 ])
		legend('Location','northeast');
		xlim([0  longtime]);	
	run standard_plot2v4.m;
end
% ===================================================================
a=a+1; if(sum(plot_omit == (a)) == 0)%plot omit	
	content = {'HPIS Injection Rate - Active Mode'};
	run standard_plot1v4.m;
	mlcfile = EDF.file{16};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,8); 
		y2 = EDF.data{k}(:,10) + EDF.data{k}(:,12);
		
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'HPIS1 RPV' ] ];
		plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'HPIS2' ] ];
	end
		ylabel('Flow Rate, [kg/s]'); xlabel(labelTime); %xlim([-1 60]); %ylim([0 ])
		legend('Location','northeast');
		xlim([0 longtime]);	
	run standard_plot2v4.m;
end
% ===================================================================
a=a+1; if(sum(plot_omit == (a)) == 0)%plot omit	
	content = {'HPIS Integrated Flow - Active Mode'};
	run standard_plot1v4.m;
	mlcfile = EDF.file{16};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,9); 
		y2 = EDF.data{k}(:,11) + EDF.data{k}(:,13);
		
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'HPIS1 RPV' ] ];
		plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'HPIS2' ] ];
	end
		ylabel('Mass, [kg]'); xlabel(labelTime); %xlim([-1 60]); %ylim([0 ])
		legend('Location','northeast');
		xlim([0 longtime]);	
	run standard_plot2v4.m;
end
% ===================================================================
end % if flagINJ == 1
% ===================================================================


% ===================================================================
% ===================================================================
if flag_SOU == 1
% ===================================================================
% RECIRCULATION FOR SOU CASES

a=a+1; if(sum(plot_omit == (a)) == 0)   %plot omit	
	content = {'LPIS Inj. Rate - Recirc. Mode - SOU Model'};
	run standard_plot1v4.m;
	mlcfile = EDF.file{17};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,2); 
		y2 = EDF.data{k}(:,3).*2; 
		
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'LPIS1 RPV' ] ];
		plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'LPIS2' ] ];
	end
		ylabel('Flow Rate, [kg/s]'); xlabel(labelTime); %xlim([-1 60]); %ylim([0 ])
		legend('Location','northeast');
		xlim([0 plottime]);	
	run standard_plot2v4.m;
end
% ===================================================================
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit	
	content = {'RHR HX Heat Removal Rate - SOU Model'};
	run standard_plot1v4.m;
	mlcfile = EDF.file{17};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,5); 		
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} ' RHR HX' ] ];
	end
		ylabel('Power, [W]'); xlabel(labelTime); %xlim([-1 60]); %ylim([0 ])
		legend('Location','northeast');
		xlim([0 plottime]);	
	run standard_plot2v4.m;
end

% ===================================================================

a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit	
	content = {'RHR HX Outlet Water Temp. - SOU Model'};
	run standard_plot1v4.m;
	mlcfile = EDF.file{17};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,8); 
		
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} ' RHR HX' ] ];
	end
		ylabel(labelT); xlabel(labelTime); %xlim([-1 60]); %ylim([0 ])
		legend('Location','northeast');
		%xlim([0 longtime]);	
		xlim([0 plottime]);	
	run standard_plot2v4.m;
end
% ===================================================================
end 		%if flag_SOU == 1
% ===================================================================

% ===================================================================
if flag_REC == 1
% ===================================================================


% ===================================================================
% LPIS Recirculation Injection
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit	
	content = {'LPIS Injection Rate - Recirc. Mode'};
	run standard_plot1v4.m;
	mlcfile = EDF.file{16};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,14); 
		y2 = EDF.data{k}(:,16) + EDF.data{k}(:,18);
		
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'LPIS1 RPV' ] ];
		plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'LPIS2' ] ];
	end
		ylabel('Flow Rate, [kg/s]'); xlabel(labelTime); %xlim([-1 60]); %ylim([0 ])
		legend('Location','northeast');
		xlim([0 longtime]);	
	run standard_plot2v4.m;
end
% ===================================================================
% LPIS Integrated Recirculation Injection
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit	
	content = {'LPIS Integrated Flow - Recirc. Mode'};
	run standard_plot1v4.m;
	mlcfile = EDF.file{16};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,15); 
		y2 = EDF.data{k}(:,17) + EDF.data{k}(:,19);
		
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'LPIS1 RPV' ] ];
		plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'LPIS2' ] ];
	end
		ylabel('Mass, [kg]'); xlabel(labelTime); %xlim([-1 60]); %ylim([0 ])
		legend('Location','northeast');
		xlim([0  longtime]);	
	run standard_plot2v4.m;
end
% ===================================================================
a=a+1; if(sum(plot_omit == (a)) == 0)%plot omit	
	content = {'HPIS Injection Rate - Recirc. Mode'};
	run standard_plot1v4.m;
	mlcfile = EDF.file{16};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,20); 
		y2 = EDF.data{k}(:,22) + EDF.data{k}(:,24);
		
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'HPIS1 RPV' ] ];
		plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'HPIS2' ] ];
	end
		ylabel('Flow Rate, [kg/s]'); xlabel(labelTime); %xlim([-1 60]); %ylim([0 ])
		legend('Location','northeast');
		xlim([0 longtime]);	
	run standard_plot2v4.m;
end
% ===================================================================
a=a+1; if(sum(plot_omit == (a)) == 0)%plot omit	
	content = {'HPIS Integrated Flow - Recirc. Mode'};
	run standard_plot1v4.m;
	mlcfile = EDF.file{16};
	k = detectfile(mlcfile,EDF.file);  %MELCOR
	if ~sum(k == emptyfile)
        x0 = EDF.data{k}(:,1)./Tconst;        
		y1 = EDF.data{k}(:,21); 
		y2 = EDF.data{k}(:,23) + EDF.data{k}(:,24);
		
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num} 'HPIS1 RPV' ] ];
		plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'--');  labels = [labels, [cases_list_names1{k_num} 'HPIS2' ] ];
	end
		ylabel('Mass, [kg]'); xlabel(labelTime); %xlim([-1 60]); %ylim([0 ])
		legend('Location','northeast');
		xlim([0 longtime]);	
	run standard_plot2v4.m;
end

% ===================================================================
end % if flag_REC == 1
% ===================================================================

% ===================================================================


% ===================================================================


% ===================================================================



% ===================================================================




















