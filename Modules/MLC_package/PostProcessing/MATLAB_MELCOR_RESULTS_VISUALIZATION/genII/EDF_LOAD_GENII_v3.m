% ===================================================================
   file = case1;
% ===================================================================

% ===================================================================
% For CSV files add extension

if (cases_output_type{k_num} == 1)  | (cases_output_type{k_num} == 2)  
	EDF.file = { ...
				'DATA_RCS_Rev4.apt', ...		%1
				'DATA_CONT_Rev4.apt', ...		%2
				'DATA_TFU_Rev4.apt', ...		%3
				'DATA_BREAK_Rev4.apt', ...		%4
				'DATA_LP_MASS_ZRO2.apt',...		%5
				'DATA_LP_MASS_ZR.apt',...		%6
				'DATA_LP_MASS_UO2.apt',...		%7
				'DATA_LP_MASS_SS.apt',...		%8
				'DATA_LP_MASS_SSOX.apt',...		%9
				'DATA_LP_MASS_INC.apt',...		%10  - REMOVED	
				'DATA_LP_MASS_CRP.apt',...		%11  - for M2.2.11 and M2.2.15 it is MAGINC
				'DATA_CAV_Rev4.apt', ...		%12
				'DATA_TCL_Rev4.apt', ...		%13
				'DATA_LVL_Rev4.apt', ...		%14
				'DATA_NARSIS_Rev4.apt',...			%15
				'DATA_SIS_Rev4.apt',...			%16
				'DATA_SOU_Rev4.apt',...			%17
				'DATA_TANKS_Rev4.apt',...			%18
				'DATA_ESF_Rev4.apt',...				%19
				'DATA_FW_Rev4.apt',...				%20
				};

				
elseif ((cases_output_type{k_num} == 5)	| (cases_output_type{k_num} == 3)			)			
	EDF.file = {...
	
				};
				
elseif ((cases_output_type{k_num} == 4)			)			
	EDF.file = {...
				};
								
end	
	EDF.num = 1:1:numel(EDF.file);
	EDF.units = {};
	emptyfile = [];




% Check if file exists - if not make empty
for i=1:numel(EDF.file)
    filename = [file,'\',EDF.file{i}];
	
    if  isfile(fullfile(filename))
		[fPath,fName,fExt] = fileparts(filename);
		if strcmp(lower(fExt),'.csv')		
			tmp = importdata(filename,',',2); % in csv file skip first line
			EDF.data{i} = tmp.data;
		elseif strcmp(lower(fExt),'.apt')		%APLT PLOT ASCII FILES
			tmp = importdata(filename,' ',3);	% shift start of reading - due to 2 lines header
			EDF.data{i} = tmp.data;				
		else
			EDF.data{i} = importdata(filename);
		end
    else
        %EDF.data{i} = zeros(10,10);
        EDF.data{i} = [];
        emptyfile = [emptyfile, i];
    end
end
% ===================================================================


% % ===================================================================
% if flagMCCI == 1
%     EDF.fileMCCI = {'R_COREC' 'Z_COREC' 'R_RPIT' 'Z_RPIT'}
% end
% % ===================================================================