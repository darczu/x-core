% ===================================================================
% FLOWS HL/CL BREAKS
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
	content = {'Integrated Large Break Water Flow'};
		run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{4}; k = detectfile(mlcfile,EDF.file);  %MELCOR
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			y1 = EDF.data{k}(:,3) + EDF.data{k}(:,5);	%HL+CL Broken Loop Break Flow 
			%y1 = sum(EDF.data{k}(:,4:9),2); %Break I flow
			plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num}  'BREAK'] ];
		end
	end
		ylabel(labelM); xlabel(labelTime); %xlabel('Time, [s]');  %xlim([-20 200]);	
		legend('Location','northeast');
		xlim([0 longtime]);	
		run standard_plot2v4.m;
end
%{
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
	content = {'Large Break Water Flow'};
		run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{4}; k = detectfile(mlcfile,EDF.file);  %MELCOR
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			y1 = EDF.data{k}(:,2) + EDF.data{k}(:,4);	%HL+CL Broken Loop Break Flow 
			%y1 = sum(EDF.data{k}(:,4:9),2); %Break I flow
			plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num}  'BREAK'] ];
		end
	end
		ylabel('Mass Flow, [kg/s]'); xlabel(labelTime); %xlabel('Time, [s]');  %xlim([-20 200]);	
		legend('Location','northeast');
		xlim([0 longtime]);	
		run standard_plot2v4.m
end
%}
% ===================================================================
% FLOWS HL/CL BREAKS
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
	content = {'Integrated ACC Water Flows'};
		run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{4}; k = detectfile(mlcfile,EDF.file);  %MELCOR	
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			y2 = EDF.data{k}(:,8) + EDF.data{k}(:,9); %Acc1 + Acc2	
			plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num}  'ACC'] ];
		end
	end
		ylabel(labelM); xlabel(labelTime); %xlabel('Time, [s]');  %xlim([-20 200]);	
		legend('Location','northeast');
		xlim([0 shorttime]);
		run standard_plot2v4.m;
end
%{
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
	content = {'ACC Water Flows'};
		run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{4}; k = detectfile(mlcfile,EDF.file);  %MELCOR	
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			y2 = EDF.data{k}(:,6) + EDF.data{k}(:,7); %Acc1 + Acc2	
			plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num}  'ACC'] ];
		end
	end
		ylabel('Mass Flow, [kg/s]'); xlabel(labelTime); %xlabel('Time, [s]');  %xlim([-20 200]);	
		legend('Location','northeast');
		xlim([0 shorttime]);
		run standard_plot2v4.m
end
%}
% ===================================================================
% FLOWS CREEP RUPTURED HL
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
	content = {'Integrated Creep Rupture Break Water Flow'};
		run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{4}; k = detectfile(mlcfile,EDF.file);  %MELCOR	
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			y2 = EDF.data{k}(:,13) + EDF.data{k}(:,15); %Creep breaks 13,15
			plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num}  'CREEP-BREAK'] ];
		end
	end
		ylabel(labelM); xlabel(labelTime); %xlabel('Time, [s]');  %xlim([-20 200]);	
		legend('Location','northeast');
		xlim([0 plottime]);
		run standard_plot2v4.m;
end
%{
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
	content = {'Creep Rupture Break Water Flow'};
		run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{4}; k = detectfile(mlcfile,EDF.file);  %MELCOR	
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			y2 = EDF.data{k}(:,12) + EDF.data{k}(:,14); %Creep breaks 13,15
			plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num}  'CREEP-BREAK'] ];
		end
	end
		ylabel('Mass Flow, [kg/s]'); xlabel(labelTime); %xlabel('Time, [s]');  %xlim([-20 200]);	
		legend('Location','northeast');
		xlim([0 mediumtime]);
		run standard_plot2v4.m
end
%}
% ===================================================================
% SEAL LOCAS
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
	content = {'Integrated Seal LOCA Water Flows'};
		run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{4}; k = detectfile(mlcfile,EDF.file);  %MELCOR	
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			y2 = EDF.data{k}(:,17) + EDF.data{k}(:,19) + EDF.data{k}(:,21) + EDF.data{k}(:,23); %17,19,21,23
			plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num}  'SEAL'] ];
		end
	end
		ylabel(labelM); xlabel(labelTime); %xlabel('Time, [s]');  %xlim([-20 200]);	
		legend('Location','northeast');
		xlim([0 plottime]);
		run standard_plot2v4.m;
end

%{
a=a+1; if (sum(plot_omit == (a)) == 0)%plot omit
	content = {'Seal LOCA Water Flows'};
		run standard_plot1v4.m;
	if  (cases_output_type{k_num} == 1)
		mlcfile = EDF.file{4}; k = detectfile(mlcfile,EDF.file);  %MELCOR	
		if ~sum(k == emptyfile)
			x0 = EDF.data{k}(:,1)./Tconst;        
			y2 = EDF.data{k}(:,16) + EDF.data{k}(:,18) + EDF.data{k}(:,20) + EDF.data{k}(:,22); %17,19,21,23
			plot(x0,y2,lw,1,ms,2,cl,col2(k_num,:),ls,'-');  labels = [labels, [cases_list_names1{k_num}  'SEAL'] ];
		end
	end
		ylabel('Mass Flow, [kg/s]'); xlabel(labelTime); %xlabel('Time, [s]');  %xlim([-20 200]);	
		legend('Location','northeast');
		xlim([0 mediumtime]);
		run standard_plot2v4.m
end
%}
% ===================================================================








% ===================================================================