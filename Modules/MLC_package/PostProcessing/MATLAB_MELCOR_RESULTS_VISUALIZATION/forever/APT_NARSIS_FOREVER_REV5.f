
#MELCOR 0 ptf "Q:\ROOT\NARSIS-Forever\test41\FRV.PTF"
MELCOR 1 ptf "Q:\ROOT\NARSIS-Forever\test41\FRV.PTF"
MELCOR 2 ptf "Q:\ROOT\NARSIS-Forever\test41\FRV.PTF"
MELCOR 3 ptf "Q:\ROOT\NARSIS-Forever\test41\FRV.PTF"
MELCOR 4 ptf "Q:\ROOT\NARSIS-Forever\test41\FRV.PTF"
MELCOR 5 ptf "Q:\ROOT\NARSIS-Forever\test41\FRV.PTF"
MELCOR 6 ptf "Q:\ROOT\NARSIS-Forever\test41\FRV.PTF"
MELCOR 7 ptf "Q:\ROOT\NARSIS-Forever\test41\FRV.PTF"
MELCOR 8 ptf "Q:\ROOT\NARSIS-Forever\test41\FRV.PTF"
MELCOR 9 ptf "Q:\ROOT\NARSIS-Forever\test41\FRV.PTF"
MELCOR 10 ptf "Q:\ROOT\NARSIS-Forever\test41\FRV.PTF"
#MELCOR 3 ptf "Q:\ROOT\NARSIS-Forever\test41\FRV.PTF"

#MELCOR 2 ptf "Q:\ROOT\NARSIS-Forever\test11\FRV.PTF"
#MELCOR 3 ptf "Q:\ROOT\NARSIS-Forever\test11\FRV.PTF"


#
# examples:
# # Containment
#   # Drywell - DW
#     # MREAD 0 "CVH-MASS.6_102"
#     # MELCOR 0 EXPORT CSV "C:\Users\admin2\Documents\ROOT\MELCOR\CASE21\CSV_DATA_EXTRACT\VTT_1_NOSPR_NOPAR\DW_H2_MASS.csv" 
# 	 MELCOR 0 ptf "D:\NARSIS_DOCS\MODELE_AND_RESEARCH\git-narsis\narsis-calculations\INPUTS\MELCOR\1000_GenII_NARSIS\case00012k\PWR.PTF"
#   MELCOR 1 ptf "D:\NARSIS_DOCS\MODELE_AND_RESEARCH\git-narsis\narsis-calculations\INPUTS\MELCOR\1000_GenII_NARSIS\case00012k\PWR.PTF"
# DEBRIS MASSES in LP, in cells  RINGAXIAL
# CRP
# MREAD 0 "COR-MCRP_101"; MREAD 0 "COR-MCRP_102"; MREAD 0 "COR-MCRP_103"; MREAD 0 "COR-MCRP_104"
# MREAD 0 "COR-MCRP_201"; MREAD 0 "COR-MCRP_202"; MREAD 0 "COR-MCRP_203"; MREAD 0 "COR-MCRP_204"
# # #####################################################################
# # MREAD 0 "COR-DMH2-TOT"
# # MELCOR 0 EXPORT CSV "5_MH2CR.csv"
# # MELCOR 0 EXPORT ASCII "CORMassLP_CRP.apt"
# ######################################################################################################
# CORE-TLH.ijj   i - segment 1-10  jj - node 1-11  jj=01 outside jj=11 inside
# ######################################################################################################
# MREAD 0 "COR-TLH_101"

# LH Temperatures
##MREAD 0 "COR-TLH_101"; MREAD 0 "COR-TLH_201"; MREAD 0 "COR-TLH_301"; MREAD 0 "COR-TLH_401"; MREAD 0 "COR-TLH_501"; MREAD 0 "COR-TLH_601"; MREAD 0 "COR-TLH_701"; MREAD 0 "COR-TLH_801"; MREAD 0 "COR-TLH_901"; MREAD 0 "COR-TLH_1001"
##MREAD 1 "COR-TLH_111"; MREAD 1 "COR-TLH_211"; MREAD 1 "COR-TLH_311"; MREAD 1 "COR-TLH_411"; MREAD 1 "COR-TLH_511"; MREAD 1 "COR-TLH_611"; MREAD 1 "COR-TLH_711"; MREAD 1 "COR-TLH_811"; MREAD 1 "COR-TLH_911"; MREAD 1 "COR-TLH_1011"
#MREAD 2 "COR-TLH_101"; MREAD 2 "COR-TLH_201"; MREAD 2 "COR-TLH_301"; MREAD 2 "COR-TLH_401"; MREAD 2 "COR-TLH_501"; MREAD 2 "COR-TLH_601"; MREAD 2 "COR-TLH_701"; MREAD 2 "COR-TLH_801"; MREAD 2 "COR-TLH_901"; MREAD 2 "COR-TLH_1001"
#MREAD 3 "COR-TLH_111"; MREAD 3 "COR-TLH_211"; MREAD 3 "COR-TLH_311"; MREAD 3 "COR-TLH_411"; MREAD 3 "COR-TLH_511"; MREAD 3 "COR-TLH_611"; MREAD 3 "COR-TLH_711"; MREAD 3 "COR-TLH_811"; MREAD 3 "COR-TLH_911"; MREAD 3 "COR-TLH_1011"

##MELCOR 0 EXPORT ASCII "Q:\ROOT\NARSIS-Forever\test41\COR-TLH_i01.apt"
##MELCOR 1 EXPORT ASCII "Q:\ROOT\NARSIS-Forever\test41\COR-TLH_i11.apt"
#MELCOR 2 EXPORT ASCII "Q:\ROOT\NARSIS-Forever\test15\COR-TLH_i01.apt"
#MELCOR 3 EXPORT ASCII "Q:\ROOT\NARSIS-Forever\test15\COR-TLH_i11.apt"


# LH STRAINS
##MREAD 0 "COR-VSTRAIN_101"; MREAD 0 "COR-VSTRAIN_201"; MREAD 0 "COR-VSTRAIN_301"; MREAD 0 "COR-VSTRAIN_401"; MREAD 0 "COR-VSTRAIN_501"; MREAD 0 "COR-VSTRAIN_601"; MREAD 0 "COR-VSTRAIN_701"; MREAD 0 "COR-VSTRAIN_801"; MREAD 0 "COR-VSTRAIN_901"; MREAD 0 "COR-VSTRAIN_1001"
##MREAD 1 "COR-VSTRAIN_111"; MREAD 1 "COR-VSTRAIN_211"; MREAD 1 "COR-VSTRAIN_311"; MREAD 1 "COR-VSTRAIN_411"; MREAD 1 "COR-VSTRAIN_511"; MREAD 1 "COR-VSTRAIN_611"; MREAD 1 "COR-VSTRAIN_711"; MREAD 1 "COR-VSTRAIN_811"; MREAD 1 "COR-VSTRAIN_911"; MREAD 1 "COR-VSTRAIN_1011"


##MELCOR 0 EXPORT ASCII "Q:\ROOT\NARSIS-Forever\test41\COR-VSTRAIN_i01.apt"
##MELCOR 1 EXPORT ASCII "Q:\ROOT\NARSIS-Forever\test41\COR-VSTRAIN_i11.apt"
#MELCOR 2 EXPORT ASCII "Q:\ROOT\NARSIS-Forever\test15\COR-VSTRAIN_i01.apt"
#MELCOR 3 EXPORT ASCII "Q:\ROOT\NARSIS-Forever\test15\COR-VSTRAIN_i11.apt"



#
#
 MREAD 1 "COR-VSTRAIN_101";  MREAD 1 "COR-VSTRAIN_102"; MREAD 1 "COR-VSTRAIN_103"; MREAD 1 "COR-VSTRAIN_104"; MREAD 1 "COR-VSTRAIN_105"; MREAD 1 "COR-VSTRAIN_106"; MREAD 1 "COR-VSTRAIN_107"; MREAD 1 "COR-VSTRAIN_108"; MREAD 1 "COR-VSTRAIN_109"; MREAD 1 "COR-VSTRAIN_110"; MREAD 1 "COR-VSTRAIN_111"
                         
 MREAD 2 "COR-VSTRAIN_201";  MREAD 2 "COR-VSTRAIN_202"; MREAD 2 "COR-VSTRAIN_203"; MREAD 2 "COR-VSTRAIN_204"; MREAD 2 "COR-VSTRAIN_205"; MREAD 2 "COR-VSTRAIN_206"; MREAD 2 "COR-VSTRAIN_207"; MREAD 2 "COR-VSTRAIN_208"; MREAD 2 "COR-VSTRAIN_209"; MREAD 2 "COR-VSTRAIN_210"; MREAD 2 "COR-VSTRAIN_211"
 MREAD 3 "COR-VSTRAIN_301";  MREAD 3 "COR-VSTRAIN_302"; MREAD 3 "COR-VSTRAIN_303"; MREAD 3 "COR-VSTRAIN_304"; MREAD 3 "COR-VSTRAIN_305"; MREAD 3 "COR-VSTRAIN_306"; MREAD 3 "COR-VSTRAIN_307"; MREAD 3 "COR-VSTRAIN_308"; MREAD 3 "COR-VSTRAIN_309"; MREAD 3 "COR-VSTRAIN_310"; MREAD 3 "COR-VSTRAIN_311"
 MREAD 4 "COR-VSTRAIN_401";  MREAD 4 "COR-VSTRAIN_402"; MREAD 4 "COR-VSTRAIN_403"; MREAD 4 "COR-VSTRAIN_404"; MREAD 4 "COR-VSTRAIN_405"; MREAD 4 "COR-VSTRAIN_406"; MREAD 4 "COR-VSTRAIN_407"; MREAD 4 "COR-VSTRAIN_408"; MREAD 4 "COR-VSTRAIN_409"; MREAD 4 "COR-VSTRAIN_410"; MREAD 4 "COR-VSTRAIN_411"
 MREAD 5 "COR-VSTRAIN_501";  MREAD 5 "COR-VSTRAIN_502"; MREAD 5 "COR-VSTRAIN_503"; MREAD 5 "COR-VSTRAIN_504"; MREAD 5 "COR-VSTRAIN_505"; MREAD 5 "COR-VSTRAIN_506"; MREAD 5 "COR-VSTRAIN_507"; MREAD 5 "COR-VSTRAIN_508"; MREAD 5 "COR-VSTRAIN_509"; MREAD 5 "COR-VSTRAIN_510"; MREAD 5 "COR-VSTRAIN_511"
 MREAD 6 "COR-VSTRAIN_601";  MREAD 6 "COR-VSTRAIN_602"; MREAD 6 "COR-VSTRAIN_603"; MREAD 6 "COR-VSTRAIN_604"; MREAD 6 "COR-VSTRAIN_605"; MREAD 6 "COR-VSTRAIN_606"; MREAD 6 "COR-VSTRAIN_607"; MREAD 6 "COR-VSTRAIN_608"; MREAD 6 "COR-VSTRAIN_609"; MREAD 6 "COR-VSTRAIN_610"; MREAD 6 "COR-VSTRAIN_611"
 MREAD 7 "COR-VSTRAIN_701";  MREAD 7 "COR-VSTRAIN_702"; MREAD 7 "COR-VSTRAIN_703"; MREAD 7 "COR-VSTRAIN_704"; MREAD 7 "COR-VSTRAIN_705"; MREAD 7 "COR-VSTRAIN_706"; MREAD 7 "COR-VSTRAIN_707"; MREAD 7 "COR-VSTRAIN_708"; MREAD 7 "COR-VSTRAIN_709"; MREAD 7 "COR-VSTRAIN_710"; MREAD 7 "COR-VSTRAIN_711"
 MREAD 8 "COR-VSTRAIN_801";  MREAD 8 "COR-VSTRAIN_802"; MREAD 8 "COR-VSTRAIN_803"; MREAD 8 "COR-VSTRAIN_804"; MREAD 8 "COR-VSTRAIN_805"; MREAD 8 "COR-VSTRAIN_806"; MREAD 8 "COR-VSTRAIN_807"; MREAD 8 "COR-VSTRAIN_808"; MREAD 8 "COR-VSTRAIN_809"; MREAD 8 "COR-VSTRAIN_810"; MREAD 8 "COR-VSTRAIN_811"
 MREAD 9 "COR-VSTRAIN_901";  MREAD 9 "COR-VSTRAIN_902"; MREAD 9 "COR-VSTRAIN_903"; MREAD 9 "COR-VSTRAIN_904"; MREAD 9 "COR-VSTRAIN_905"; MREAD 9 "COR-VSTRAIN_906"; MREAD 9 "COR-VSTRAIN_907"; MREAD 9 "COR-VSTRAIN_908"; MREAD 9 "COR-VSTRAIN_909"; MREAD 9 "COR-VSTRAIN_910"; MREAD 9 "COR-VSTRAIN_911"
                         
 MREAD 10 "COR-VSTRAIN_1001";  MREAD 10 "COR-VSTRAIN_1002"; MREAD 10 "COR-VSTRAIN_1003"; MREAD 10 "COR-VSTRAIN_1004"; MREAD 10 "COR-VSTRAIN_1005"; MREAD 10 "COR-VSTRAIN_1006"; MREAD 10 "COR-VSTRAIN_1007"; MREAD 10 "COR-VSTRAIN_1008"; MREAD 10 "COR-VSTRAIN_1009"; MREAD 10 "COR-VSTRAIN_1010"; MREAD 10 "COR-VSTRAIN_1011"



 MELCOR 1 EXPORT ASCII "Q:\ROOT\NARSIS-Forever\test41\COR-STRAIN_1jj.apt"
 MELCOR 2 EXPORT ASCII "Q:\ROOT\NARSIS-Forever\test41\COR-STRAIN_2jj.apt"
 MELCOR 3 EXPORT ASCII "Q:\ROOT\NARSIS-Forever\test41\COR-STRAIN_3jj.apt"
 MELCOR 4 EXPORT ASCII "Q:\ROOT\NARSIS-Forever\test41\COR-STRAIN_4jj.apt"
 MELCOR 5 EXPORT ASCII "Q:\ROOT\NARSIS-Forever\test41\COR-STRAIN_5jj.apt"
 MELCOR 6 EXPORT ASCII "Q:\ROOT\NARSIS-Forever\test41\COR-STRAIN_6jj.apt"
 MELCOR 7 EXPORT ASCII "Q:\ROOT\NARSIS-Forever\test41\COR-STRAIN_7jj.apt"
 MELCOR 8 EXPORT ASCII "Q:\ROOT\NARSIS-Forever\test41\COR-STRAIN_8jj.apt"
 MELCOR 9 EXPORT ASCII "Q:\ROOT\NARSIS-Forever\test41\COR-STRAIN_9jj.apt"
 MELCOR 10 EXPORT ASCII "Q:\ROOT\NARSIS-Forever\test41\COR-STRAIN_10jj.apt"
#


# MREAD 0 "COR-TLH_101"; # MREAD 0 "COR-TLH_102"; MREAD 0 "COR-TLH_103"; MREAD 0 "COR-TLH_104"; MREAD 0 "COR-TLH_105"; MREAD 0 "COR-TLH_106"; MREAD 0 "COR-TLH_107"; MREAD 0 "COR-TLH_108"; MREAD 0 "COR-TLH_109"; MREAD 0 "COR-TLH_110"; MREAD 0 "COR-TLH_111"
#                         
# MREAD 0 "COR-TLH_201"; # MREAD 0 "COR-TLH_202"; MREAD 0 "COR-TLH_203"; MREAD 0 "COR-TLH_204"; MREAD 0 "COR-TLH_205"; MREAD 0 "COR-TLH_206"; MREAD 0 "COR-TLH_207"; MREAD 0 "COR-TLH_208"; MREAD 0 "COR-TLH_209"; MREAD 0 "COR-TLH_210"; MREAD 0 "COR-TLH_211"
# MREAD 0 "COR-TLH_301"; # MREAD 0 "COR-TLH_302"; MREAD 0 "COR-TLH_303"; MREAD 0 "COR-TLH_304"; MREAD 0 "COR-TLH_305"; MREAD 0 "COR-TLH_306"; MREAD 0 "COR-TLH_307"; MREAD 0 "COR-TLH_308"; MREAD 0 "COR-TLH_309"; MREAD 0 "COR-TLH_310"; MREAD 0 "COR-TLH_311"
# MREAD 0 "COR-TLH_401"; # MREAD 0 "COR-TLH_402"; MREAD 0 "COR-TLH_403"; MREAD 0 "COR-TLH_404"; MREAD 0 "COR-TLH_405"; MREAD 0 "COR-TLH_406"; MREAD 0 "COR-TLH_407"; MREAD 0 "COR-TLH_408"; MREAD 0 "COR-TLH_409"; MREAD 0 "COR-TLH_410"; MREAD 0 "COR-TLH_411"
# MREAD 0 "COR-TLH_501"; # MREAD 0 "COR-TLH_502"; MREAD 0 "COR-TLH_503"; MREAD 0 "COR-TLH_504"; MREAD 0 "COR-TLH_505"; MREAD 0 "COR-TLH_506"; MREAD 0 "COR-TLH_507"; MREAD 0 "COR-TLH_508"; MREAD 0 "COR-TLH_509"; MREAD 0 "COR-TLH_510"; MREAD 0 "COR-TLH_511"
# MREAD 0 "COR-TLH_601"; # MREAD 0 "COR-TLH_602"; MREAD 0 "COR-TLH_603"; MREAD 0 "COR-TLH_604"; MREAD 0 "COR-TLH_605"; MREAD 0 "COR-TLH_606"; MREAD 0 "COR-TLH_607"; MREAD 0 "COR-TLH_608"; MREAD 0 "COR-TLH_609"; MREAD 0 "COR-TLH_610"; MREAD 0 "COR-TLH_611"
# MREAD 0 "COR-TLH_701"; # MREAD 0 "COR-TLH_702"; MREAD 0 "COR-TLH_703"; MREAD 0 "COR-TLH_704"; MREAD 0 "COR-TLH_705"; MREAD 0 "COR-TLH_706"; MREAD 0 "COR-TLH_707"; MREAD 0 "COR-TLH_708"; MREAD 0 "COR-TLH_709"; MREAD 0 "COR-TLH_710"; MREAD 0 "COR-TLH_711"
# MREAD 0 "COR-TLH_801"; # MREAD 0 "COR-TLH_802"; MREAD 0 "COR-TLH_803"; MREAD 0 "COR-TLH_804"; MREAD 0 "COR-TLH_805"; MREAD 0 "COR-TLH_806"; MREAD 0 "COR-TLH_807"; MREAD 0 "COR-TLH_808"; MREAD 0 "COR-TLH_809"; MREAD 0 "COR-TLH_810"; MREAD 0 "COR-TLH_811"
# MREAD 0 "COR-TLH_901"; # MREAD 0 "COR-TLH_902"; MREAD 0 "COR-TLH_903"; MREAD 0 "COR-TLH_904"; MREAD 0 "COR-TLH_905"; MREAD 0 "COR-TLH_906"; MREAD 0 "COR-TLH_907"; MREAD 0 "COR-TLH_908"; MREAD 0 "COR-TLH_909"; MREAD 0 "COR-TLH_910"; MREAD 0 "COR-TLH_911"
#                         
# MREAD 0 "COR-TLH_1001"; # MREAD 0 "COR-TLH_1002"; MREAD 0 "COR-TLH_1003"; MREAD 0 "COR-TLH_1004"; MREAD 0 "COR-TLH_1005"; MREAD 0 "COR-TLH_1006"; MREAD 0 "COR-TLH_1007"; MREAD 0 "COR-TLH_1008"; MREAD 0 "COR-TLH_1009"; MREAD 0 "COR-TLH_1010"; MREAD 0 "COR-TLH_1011"




# MELCOR 0 EXPORT CSV "Q:\ROOT\NARSIS-Forever\test14\COR-TLH_1jj.csv"
# ######################################################################################################



# ######################################################################################################



# ######################################################################################################




# ######################################################################################################
#EXIT
#MELCOR 0 CLOSE
MELCOR 1 CLOSE
MELCOR 2 CLOSE
MELCOR 3 CLOSE
MELCOR 4 CLOSE
MELCOR 5 CLOSE
MELCOR 6 CLOSE
MELCOR 7 CLOSE
MELCOR 8 CLOSE
MELCOR 9 CLOSE
MELCOR 10 CLOSE
#MELCOR 2 CLOSE
#MELCOR 3 CLOSE
