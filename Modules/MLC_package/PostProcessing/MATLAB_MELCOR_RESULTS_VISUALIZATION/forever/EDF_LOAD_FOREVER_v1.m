% ===================================================================
   file = case1;
% ===================================================================

% ===================================================================
% For CSV files add extension

if cases_output_type{k_num} == 1
	EDF.file = {'COR-TLH_i01.apt', ...
				'COR-TLH_i11.apt', ...				
				'COR-VSTRAIN_i01.apt', ...
				'COR-VSTRAIN_i11.apt', ...
				};
elseif ((cases_output_type{k_num} == 2)	| (cases_output_type{k_num} == 3)			)			
	EDF.file = {'EDF_TLH_i01', ...
				'EDF_TLH_i11', ...				
				'EDF_VSTRAIN_i01', ...
				'EDF_VSTRAIN_i11', ...
				'EDF_TEMPS',...
				};
				
elseif ((cases_output_type{k_num} == 4)			)			
	EDF.file = {'EDF_TLH_i01', ...
				'EDF_TLH_i11', ...				
				'EDF_VSTRAIN_i01', ...
				'EDF_VSTRAIN_i11', ...
				'EDF_TEMPS',...
				'COR-STRAIN_1jj.apt', ...  %single segment data
				'COR-STRAIN_2jj.apt', ...
				'COR-STRAIN_3jj.apt', ...
				'COR-STRAIN_4jj.apt', ...
				'COR-STRAIN_5jj.apt', ...
				'COR-STRAIN_6jj.apt', ...
				'COR-STRAIN_7jj.apt', ...
				'COR-STRAIN_8jj.apt', ...
				'COR-STRAIN_9jj.apt', ...
				'COR-STRAIN_10jj.apt', ...  
				};
								
end	
	EDF.num = 1:1:numel(EDF.file);
	EDF.units = {};
	emptyfile = [];




% Check if file exists - if not make empty
for i=1:numel(EDF.file)
    filename = [file,'\',EDF.file{i}];
	
    if  isfile(fullfile(filename))
		[fPath,fName,fExt] = fileparts(filename);
		if strcmp(lower(fExt),'.csv')		
			tmp = importdata(filename,',',2); % in csv file skip first line
			EDF.data{i} = tmp.data;
		elseif strcmp(lower(fExt),'.apt')		%APLT PLOT ASCII FILES
			tmp = importdata(filename,' ',3);	% shift start of reading - due to 2 lines header
			EDF.data{i} = tmp.data;	
			
		else
			EDF.data{i} = importdata(filename);
		end
    else
        %EDF.data{i} = zeros(10,10);
        EDF.data{i} = [];
        emptyfile = [emptyfile, i];
    end
end
% ===================================================================


% % ===================================================================
% if flagMCCI == 1
%     EDF.fileMCCI = {'R_COREC' 'Z_COREC' 'R_RPIT' 'Z_RPIT'}
% end
% % ===================================================================