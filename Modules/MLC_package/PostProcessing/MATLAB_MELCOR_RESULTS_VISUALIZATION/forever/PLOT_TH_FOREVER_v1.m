if(flag1 == 1) %PLOT OR NOT

% ===================================================================
% FIGURE - Lower head temperatures
% ===================================================================
content = {'Lower Head Temperatures'};
	run standard_plot1v2.m;
	
	
	if k_num == 1
		if flagEXP == 1
			x = EXP_Tout_0deg_I48(:,1)./Tconst;;
			y = EXP_Tout_0deg_I48(:,2);
			plot(x,y,'ko');  labels = [labels, ['EXP'] ];
		end
	end
	
	
	%mlcfile  = 'COR-TLH_i01.apt';
	mlcfile = EDF.file{1}
	
	k = detectfile(mlcfile,EDF.file);  %MELCOR

    if ~sum(k == emptyfile) 
        x0 = EDF.data{k}(:,1)./Tconst;
        y1 = EDF.data{k}(:,2);
		%plot(x0,y1,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR' ];
		%plot(x0,y1,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, ['MELCOR_' cases_list{k_num}] ];
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:));  labels = [labels, ['MELCOR_' cases_list{k_num}] ];
    end 


	
		ylabel(labelT);
		
	run standard_plot2v2.m
legend('Location','northeast');


% ===================================================================
% FIGURE - Lower head temperatures
% ===================================================================
content = {'Lower Head Temperatures'};
	run standard_plot1v2.m;
	
	%mlcfile  = 'COR-TLH_i11.apt';
	mlcfile = EDF.file{2}
	
	k = detectfile(mlcfile,EDF.file);  %MELCOR

    if ~sum(k == emptyfile) 
        x0 = EDF.data{k}(:,1)./Tconst;
        y1 = EDF.data{k}(:,2);
		%plot(x0,y1,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR' ];
		%plot(x0,y1,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, ['MELCOR_' cases_list{k_num}] ];
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:));  labels = [labels, ['MELCOR_' cases_list{k_num}] ];
    end 

		ylabel(labelT);
		
	run standard_plot2v2.m
legend('Location','northeast');

% ===================================================================
% FIGURE
% ===================================================================	
content = {'Vstrain'};
	run standard_plot1v2.m;
	
	if k_num == 1
		if flagEXP == 1
			x = EXP_disp_0deg_onlyP_I25(:,1)./Tconst;;
			y = EXP_disp_0deg_onlyP_I25(:,2);
			plot(x,y,'ko');  labels = [labels, ['EXP onlyP'] ];
			
			x1 = EXP_disp_0deg_I22(:,1)./Tconst;;
			y1 = EXP_disp_0deg_I22(:,2);
			plot(x1,y1,'k<');  labels = [labels, ['EXP '] ];
			
		end
	end
	
	%mlcfile  = 'COR-VSTRAIN_i01.apt';
	mlcfile = EDF.file{3}
	k = detectfile(mlcfile,EDF.file);  %MELCOR

    if ~sum(k == emptyfile) 
        x0 = EDF.data{k}(:,1)./Tconst;
        y1 = EDF.data{k}(:,2).*1000*0.188./3; % -21;
		%plot(x0,y1,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR' ];
		%plot(x0,y1,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, ['MELCOR_' cases_list{k_num}] ];
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:));  labels = [labels, ['MELCOR_' cases_list{k_num}] ];
    end 

		ylabel(labelD);
		
	run standard_plot2v2.m
legend('Location','northeast')
	
% ===================================================================
% FIGURE
% ===================================================================	
content = {'Vstrain'};
	run standard_plot1v2.m;
	
	if k_num == 1
		if flagEXP == 1
			x = EXP_disp_0deg_onlyP_I25(:,1)./Tconst;;
			y = EXP_disp_0deg_onlyP_I25(:,2);
			plot(x,y,'ko');  labels = [labels, ['EXP onlyP'] ];
			
			x1 = EXP_disp_0deg_I22(:,1)./Tconst;;
			y1 = EXP_disp_0deg_I22(:,2);
			plot(x1,y1,'k<');  labels = [labels, ['EXP '] ];
		end
	end
	
	%mlcfile  = 'COR-VSTRAIN_i11.apt';
	mlcfile = EDF.file{4}
	
	
	k = detectfile(mlcfile,EDF.file);  %MELCOR

    if ~sum(k == emptyfile) 
        x0 = EDF.data{k}(:,1)./Tconst;
        y1 = EDF.data{k}(:,2).*1000*0.188./3; % -21;
		%plot(x0,y1,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR' ];
		%plot(x0,y1,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, ['MELCOR_' cases_list{k_num}] ];
		plot(x0,y1,lw,1,ms,2,cl,col2(k_num,:));  labels = [labels, ['MELCOR_' cases_list{k_num}] ];
    end 

		ylabel(labelD);
		
	run standard_plot2v2.m
legend('Location','northeast')
	
	
	
	
	
	
	
	
	
% ===================================================================
% FIGURE
% ===================================================================	
if (cases_output_type{k_num} == 3) | (cases_output_type{k_num} == 4)
	content = {'Temperatures'};
	a = k_num*10;
	run standard_plot1v2.m;
	
	
	%if k_num == 1
		if flagEXP == 1
			x = EXP_Tout_0deg_I48(:,1)./Tconst;;
			y = EXP_Tout_0deg_I48(:,2);
			plot(x,y,'ko');  labels = [labels, ['EXP LH'] ];
			
			x1 = EXP_Tmelt_02_I45(:,1)./Tconst;;
			y1 = EXP_Tmelt_02_I45(:,2);
			
			x2 = EXP_Tmelt_06_I45(:,1)./Tconst;;
			y2 = EXP_Tmelt_06_I45(:,2);
			
			x3 = EXP_Tmelt_095_I45(:,1)./Tconst;;
			y3 = EXP_Tmelt_095_I45(:,2);
			
			
			plot(x1,y1,'kx');  labels = [labels, ['EXP melt 0.2'] ];
			plot(x2,y2,'ks');  labels = [labels, ['EXP melt 0.6'] ];
			plot(x3,y3,'k<');  labels = [labels, ['EXP melt 0.95'] ];
			
		end
	%end
	
	%mlcfile  = 'COR-TLH_i01.apt';
	mlcfile = EDF.file{5}
	
	k = detectfile(mlcfile,EDF.file);  %MELCOR

    if ~sum(k == emptyfile) 
        x0 = EDF.data{k}(:,1)./Tconst;  %time
        y1 = EDF.data{k}(:,2);			%TVAP
		y2 = EDF.data{k}(:,3);
		y3 = EDF.data{k}(:,4);
		y4 = EDF.data{k}(:,5);
		y5 = EDF.data{k}(:,6);
		y6 = EDF.data{k}(:,7);
		y7 = EDF.data{k}(:,8);
		y8 = EDF.data{k}(:,9);
		y9 = EDF.data{k}(:,10);
		y10 = EDF.data{k}(:,11);
		
		%plot(x0,y1,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR' ];
		%plot(x0,y1,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, ['MELCOR_' cases_list{k_num}] ];
		c = 1;
		plot(x0,y1,lw,1,ms,2,cl,  cmapdyn(c,:));  labels = [labels, ['TVAP_LP_'  cases_list{k_num}] ];c = c+1
		plot(x0,y2,lw,1,ms,2,cl,  cmapdyn(c,:));  labels = [labels, ['TVAP_CY_'  cases_list{k_num}] ];c = c+1
		plot(x0,y3,lw,1,ms,2,cl,  cmapdyn(c,:));  labels = [labels, ['TVAP_EN_'  cases_list{k_num}] ];c = c+1
		plot(x0,y4,lw,1,ms,2,cl,  cmapdyn(c,:));  labels = [labels, ['TLH_101_'  cases_list{k_num}] ];c = c+1
		plot(x0,y5,lw,1,ms,2,cl,  cmapdyn(c,:));  labels = [labels, ['TLH_111_'  cases_list{k_num}] ];c = c+1
		plot(x0,y6,lw,1,ms,2,cl,  cmapdyn(c,:));  labels = [labels, ['TPD_' 	 cases_list{k_num}] ];c = c+1
		plot(x0,y7,lw,1,ms,2,cl,  cmapdyn(c,:));  labels = [labels, ['HS_LID_1_' cases_list{k_num}] ];c = c+1
		plot(x0,y8,lw,1,ms,2,cl,  cmapdyn(c,:));  labels = [labels, ['HS_LID_7_' cases_list{k_num}] ];c = c+1
		plot(x0,y9,lw,1,ms,2,cl,  cmapdyn(c,:));  labels = [labels, ['HS_VES_1_' cases_list{k_num}] ];c = c+1
		plot(x0,y10,lw,1,ms,2,cl, cmapdyn(c,:));  labels = [labels, ['HS_VES_7_' cases_list{k_num}] ];
		
    end 
		ylabel(labelT);
		
	run standard_plot2v2.m
	
	legend('Location','northeast');
	
end 
% ===================================================================
% FIGURE
% ===================================================================	
	if  (cases_output_type{k_num} == 4)
	
	%y = zeros(10,11); %segment node
	
	vol_frac = [0.093168624
				0.094655326
				0.096153796
				0.097664034
				0.09918604
				0.100719815
				0.102265357
				0.103822668
				0.105391747
				0.106972593
				];  %volume fraction for single layer 1...10 
	vol_frac = vol_frac.*0.1; %10 segments 
	
		for i = 1:10
			mlcfile = EDF.file{i+5}  %file shift
				k = detectfile(mlcfile,EDF.file);  %MELCOR	
				if ~sum(k == emptyfile) 
					x0 = EDF.data{k}(:,1)./Tconst;  %time
										
					for j = 1:11
						yy{i,j} = EDF.data{k}(:,1+j);	%vector with all data for single segment		
					end
				end
		end
		
	%	yy_sum2 = {};
		for i=1:10
		    yy_sum1{i} = 0;
			
			for j=1:11
				yy_sum1{i} = yy_sum1{i} + yy{i,j}; 	
			end
	%		yy_sum2 = yy_sum2 + yy_sum1{i}
		end	
		
	yy_sum2 = {0}
	for i=1:10
		yy_sum2{:} = yy_sum2{:} + yy_sum1{i}	
	end	
		
		yyy = yy_sum2{:}
		
		Vij = 3.27631E-05;  %volume divided by 110
		V = 0.00360394;
		
		epsV_avg = Vij*yyy/V
		epsR_avg = epsV_avg/3  %estimated linear eps
		
		R = 0.188
		disp = epsR_avg*R*1000 %to mm
		
		
	
	
		content = {'Vstrain2'};
	run standard_plot1v2.m;
	
	%if k_num == 1
		if flagEXP == 1
			x = EXP_disp_0deg_onlyP_I25(:,1)./Tconst;;
			y = EXP_disp_0deg_onlyP_I25(:,2);
			plot(x,y,'ko');  labels = [labels, ['EXP onlyP'] ];
			hold on;
			x1 = EXP_disp_0deg_I22(:,1)./Tconst;;
			y1 = EXP_disp_0deg_I22(:,2);
			plot(x1,y1,'k<');  labels = [labels, ['EXP '] ];
			hold on;
		end
	%end
	hold on
	

		plot(x0,disp,lw,1,ms,2,cl,col2(k_num,:));  labels = [labels, ['MELCOR_' cases_list{k_num}] ];

		ylabel(labelD);
		
	run standard_plot2v2.m
legend('Location','northeast')
	
	
	
	
	
	
	end
% ===================================================================
	end %END of flag1
% ===================================================================