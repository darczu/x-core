% Ploting script version 22-07-2020. Attempt to make it as general as possbile
% Modified version of the NARSIS_EDF_READ_VER9
% Here it is for Forever epeirment orginally it was developed for Gen III plant
% NARSIS Project % Plot Forever code
% TO USE THIS FILE YOU NEED XCORE toolbox package installed
tic
clc; close all; clear; format short g
% FOLDERS ===================================================================
case_name = 'Forever Experiment Calculations'

root_folder = 'Q:\ROOT\NARSIS-Forever'
post_folder = 'D:\NARSIS_DOCS\MODELE_AND_RESEARCH\SCRIPTS\MATLAB_XCore\x-core\Database\MLC_package\PostProcessing\MATLAB_MELCOR_RESULTS_VISUALIZATION\forever'
exp_file = 'EXP_FOREVER_DATA1.mat'
code1 ='M2.2.15254'; %code1 ='M2.2.11932'; code1 ='M2.2.9541'; %code1 = 'MELCOR2.1.6341'; %code1 ='M2.2.9541';


% SETUP ===================================================================
flag1 		= 1; %PLOT 1-YES 0 - NO
flagPLOT = flag1; %Plot figures
flag2 		= 0; %SAVE FIGURE 1-YES 0-NO; 
flag3 		= 0;  % 0 -save to 1st folder; 1 - save to new folder
flag4    	= 0; %Save FIG type file
flagTH 		= 1; % TH results
flagCases 	= 1; %1 - read from local folder or absolute, 0- read from given folder explicity in the file - file with list of folders
flagEDF 	= 1; %1 Use external file
flagEXP 	= 1; %Load expeirmental data
flag_hours  = 1; %1 - hours 0 - seconds - input data time scale
title_flag 	= 2 ; %0 - no title; 1 - title with file localization (subfolder) - helps to identificate file; 2 - title with information about what is on the plot 
flag_info1  = 2;  %Add info about folder and case; 1 - old with folder; 2 - reduced with mlcfilename, 0 - no
% TIME ===================================================================
plottime = 30;  %time end for plotting %timetime = 30000/3600; 
%tend = plottime; % czas konca obliczen %8; %7 % 24 %66.0;  calculation time %<================================= TIME
if(flag_hours == 1 )  Tconst = 3600;   else Tconst = 1.0;  end  %time conversion from sec to hours

% EXPERIMENTAL DATA========================================================
% load experimental data - run mat file with experiment data
if flagEXP == 1
	load([post_folder,'\', exp_file]);
end

% CASES LIST ===================================================================
%cases_list = {'test14' 'test11' 'test15' 'test12' 'test16' 'test17'}
%cases_list = {'test11' 'test17' 'test18' 'test19'}
cases_list = { 'test25'  'test37' 'test38' 'test39' 'test40' 'test41'}
cases_output_type = {  3  4 3 3 3 4}  %type 1 is apt file   type 2 is EDF file, 3 new, 4 newer to be selected by the user

cases_list_names =  cases_list; %{'test14' 'test11' 'test15' 'test12' 'test16'}

% CASES ===================================================================
if flagCases == 1
    folder = { root_folder }; %folder with results
	subfolder = cases_list;   %folder for particular case (result)
	for i=1:numel(cases_list)
		cases(i) = {[folder{1} '\' subfolder{i}]}; 
	end
    %cases_names = {[folder{1} '_' subfolder{1}]};  % 'C7E'  'C7F'}
	cases_names = cases_list_names;  
else
    %run FILES_DATA.m    %Read file names - FILE WITH EXPLICIT NAMES OF FILES WITH DATA
	file_final1 = 'Q:\ROOT\NARSIS-Forever\test14'; %Here it is given expicity but it should be in FILES_DATA file
	file_final2 = {file_final1}; 
    cases = file_final2; 
    cases_names = cases_list_names;  
end

if numel(cases) == 1 
    flag3 = 0;  % 0 -save to 1st folder; 1 - save to new folder
end
if numel(cases) > 1 
    flag3 = 1;  % 0 -save to 1st folder; 1 - save to new folder
end

% ===================================================================
% COLORS AND LABELS SETUP
run colormaps_and_labels.m
% ===================================================================



% PLOTTING ===================================================================
% This loop will allow to compre different results
for k_num=1:numel(cases)
    case1 =  cases{k_num};  % current case folder
    case1_name = cases_names{k_num}; % current case name
	
	% ===================================================================
    if flagEDF == 1 
		run EDF_LOAD_FOREVER_v1.m  
	end
	% ===================================================================

    % ===================================================================
    if flagTH ==1
         a=0;
        run PLOT_TH_FOREVER_v1.m
    end
	% ===================================================================

end    

% ===================================================================
toc