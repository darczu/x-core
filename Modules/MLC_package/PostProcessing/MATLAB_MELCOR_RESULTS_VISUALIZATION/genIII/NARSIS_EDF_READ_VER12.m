% New script to generate plots - NARSIS 2019 modifications - Piotr Darnowski
% Updated 2021
% ===================================================================
clc; close all; clear; format short g
% ===================================================================
% FLAGS 

flagMAAP = 0 %read MAAP  Total Loss of Ac Power results
flagMAAPfull = 0 % 1 - read full maap data  0 -reduced

flag1 = 1; 
flagPLOT = flag1; %Plot figures
flag2    = 1; %SAVE plots 1-YES 0-NO;  
flag3    = 1;  % 0 -save to the 1st folder; 1 - save to new folder - when compariosn
flag4    = 1; %Save FIG type file
flagEDF = 1; %1 - EDF files read; 0 - no
flagSS = 0; %Steady State Plot
flagRPV_TH  = 1;
flagRCS_TH  = 1;
flagCONT_TH = 1;
flagCORE = 1; %Core output data
flagRN = 0;
flagMCCI    = 0;
flag22=0; %MCCI JPEG saving
flag33=0; %MCCI MOVIE
flagCases = 1; %1 - read from local folder, 0- read absolute folder
title_flag = 0; %0 - no title; 1 - title with file localization (subfolder) - helps to identificate file; 2 - title with information about what is on the plot 

flag_unique = 1; %use data uniqness procedure

flag_info1 = 2;

%code1 ='M229_168h';%code1 ='M2.2.11932'; %code1 = 'MELCOR2.1.6341'; %code1 ='M2.2.9541';

% ===================================================================
% CASES
if flagCases == 1
    folder = {'C:\CloudStation\ROOT\NARSIS-GenIII'}
    %folder = {'LOOP'};
    %subfolder = {'206_I6_SERWER'};
	%subfolder = {'case0002_A4'};
	%subfolder = {'case0002_B4'};    
      
    %subfolder =  {'case0002_B20'}
    %subfolder =  {'case0002_B20a'}
    %subfolder =  {'case0002_B20b_2'}
    %subfolder =  {'case0002_B20c'} %%
    %subfolder =  {'case0002_B20d'}
    %subfolder =  {'case0002_B20e'}
    
    %subfolder =  {'case0002_B21a'}
    %subfolder =  {'case0002_B21b'}
    %subfolder =  {'case0002_B21c'}
    %subfolder =  {'case0002_B21d'}
    %subfolder =  {'case0002_B21e'}
   
    %subfolder =  {'case0002_B22'}
    %subfolder =  {'case0002_B23'}
    
    %subfolder =  {'case0002_B23b'} %%
   subfolder =  {'case0002_B23_18M'} %%


    cases = {[folder{1} '/' subfolder{1}]}; 
    %cases_names = {[folder{1} '_' subfolder{1}]};  % 'C7E'  'C7F'}
    %cases_names = {'case0002_B23'}
    %cases_names = {'case0002_B23b'}
    cases_names = subfolder
    
        %cases = {'LOOP/206_I6_SERWER'};   
        %cases_names = {'LOOP 206_I6_SERWER'}; 
else
    run FILES_DATA.m    %Read file names
    cases = file_final; % <================================ 
    cases_names = {'206'}     %cases_names = file_final;
end

% old flags - to be removed
flagSNAP = 1; %old 5_MH2CR % OLD - for all cases
flag_all = 0;  %MORE DATA - for LBLOCA - new plot s - OLD - for all cases
flagMANY = 0; %many plot series  1 / one plot series 0

% ===================================================================
% TIME
%tend = 8 %5 days
%tend = 25 %
%tend = 120 %5 days
tend = 240 %10 days
%tend = 168 %7 days
%tend=278  %1M
%tend=200; %720k
%tend=300; %12.5d
%tend = 500 %1.8M

%plottime = 1.8e6/3600;  %time end for plotting
code1 =num2str(tend); 
plottime = tend;

flag_hours = 1; %1 - hours 0 - seconds

timetime = 30000/3600; 
%plottime = 1200000;  %time end for plotting

time_label = 'Time, [h]';

tend = plottime; % czas konca obliczen %8; %7 % 24 %66.0;  calculation time %<================================= TIME
if(flag_hours == 1 ) 
    Tconst = 3600;  % time = time./Tconst; %time conversion from sec to hours
else
    Tconst = 1.0; % na godziny
end
% ===================================================================
%if flagMAAP == 1
    run MAAP_DATA.m     %Read MAAP results
%end
% ===================================================================

% ===================================================================
if numel(cases) == 1 
    flag3 = 0;  % 0 -save to 1st folder; 1 - save to new folder
end
if numel(cases) > 1 
    flag3 = 1;  % 0 -save to 1st folder; 1 - save to new folder
end


% ===================================================================
% COLOR MAPS
cmap6 = hsv(6); cmap8 = hsv(8); cmap11 = hsv(11); cmap14 = hsv(14); cmap28 = hsv(28); cmap16 = hsv(16); cmap18 = hsv(19); %color maps
cmapT = ['k', 'r', 'b', 'm', 'y','c','k','b'];
cmapcases = hsv(numel(cases));
col1 = { 'b','r', 'm', 'c'}; col = char(col1);
colL = lines(10);
colP = prism(10);
smap = {'-', '--', '-.', '.','-x','-o','-s'};
ls = char({'LineStyle'}); 
cl = char({'Color'}); %col = char(col1);
ms = char({'MarkerSize'});
lw = char({'LineWidth'});
% Color palletes: lines, colorcube, prism
% 
% ===================================================================
% MAIN LOOP
for k_num=1:numel(cases)
    case1 = cases{k_num}; % current case folder
    case1_name = cases_names{k_num};% current case name
    % ===================================================================
    if flagEDF == 1
        run EDF_LOAD.m    % IMPORT EDF FILES 
    end
    % ===================================================================
    if flagSS == 1
        a = 1000;
        run PLOT_SS.m       % STEADY-STATE
    end
    % ===================================================================
    if flagRPV_TH ==1
        a=0;
        run PLOT_TH.m         % TH
    end
    % ===================================================================
    if flagCORE == 1
        a = 100;
        run PLOT_CORE.m      % Core parameters - masses etc
    end

   % ===================================================================
   if flagRN == 1
        a=300;
        run PLOT_RN.m          % RN
   end
    % ===================================================================
    if flagMCCI == 1
        %a=400;
        %run PLOT_MCCI.m          % MCCI - use it in stand-alone mode
    end
    % ===================================================================
    % ===================================================================
    tfailure = EDF.data{1}(end,1);
    tfailureh = tfailure/Tconst; 
    tfailureh2= floor(tfailureh);
    tfailureh3 = (tfailureh - floor(tfailureh))*60;
    disp(['Time to fail [sec] :', num2str(tfailure)])
    disp(['Time to fail (or end of the problem) [h/min] :', num2str(tfailureh2),' [h] ', num2str(floor(tfailureh3)), ' [min]'])
    % ===================================================================

end

 
 
 
 
 
% ===================================================================
%{
if(flagMANY ==1)
        %filexxx = {file104, file105, file106} %<==========
        %filexxx = {file108, file109}
    filexxx={file235}
        N = 2; k=10; %ile kolorow
        cmap=[];
        for i=1:1
            cmap(1:k,1:3,i) = cool(k);  %hsv, hot, cool, etc
            %cmap(1:k,1:3,i) = i.*cool(k)./2.*i;  %hsv, hot, cool, etc
           col1 = cmap(:,:,i); %old
      col1 = {'c', 'g', 'k', 'y'}; 
        col1 = char(col1)
}
% ===================================================================
% NOTES
% load 
% save myFile.mat A B -v7.3 -nocompression
%save('myFile.mat','A','B','-v7.3','-nocompression')
% fmt = '-ascii','-double','-tabs'
% save(filename,variables,fmt)
% save('test.mat','-nocompression','-v7.3','-ascii','-double','-tabs')
% csvread('./DATA/Default Dataset.csv')  older matlba
% use A = readmatrix(filename) 		matlab 2019

% ===================================================================
%annotation(gcf,'textbox', [0.02 0.02 0.02 0.02],'String',case1,'FitBoxToText','off','EdgeColor','none');
%
%annotation(figure1,'textbox',...
%    [0.02 0.02 0.02 0.02],...
%    'String',{'ssss'},...
%    'FitBoxToText','off',...
%    'EdgeColor','none');
%}
%{
h=figure(1+a)
% ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])

    %}

    %{

    if exist(filename, 'file') == 2
        % File exists.
   else
        % File does not exist.
   end

   if isfile(filename)
    % File exists.
else
    % File does not exist.
end
isfile(fullfile(cd, 'filename.txt'))


% isfile(fullfile('LOOP/', 'filename.txt'))
if isfile(filename)
    % File exists.
else
    % File does not exist.
end
isfile(fullfile(cd, 'filename.txt'))
%}