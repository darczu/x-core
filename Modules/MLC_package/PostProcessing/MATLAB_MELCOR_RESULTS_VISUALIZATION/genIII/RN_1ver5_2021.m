%RN_READER
%Piotr Darnowski
%Post Processing Radionuclides
% set(gca, 'YScale', 'log')
%30.06.2014, updated 2021
%set(gca, 'YScale', 'log')
%ylim([1e-5 1000])
%ylim([1e-6 1000])
% ===================================================================
clc; close all; clear;


% COLOR MAPS
cmap6 = hsv(6); cmap8 = hsv(8); cmap11 = hsv(11); cmap14 = hsv(14); cmap28 = hsv(28); cmap16 = hsv(16); cmap18 = hsv(19); %color maps
cmapT = ['k', 'r', 'b', 'm', 'y','c','k','b'];
col1 = { 'b','r', 'm', 'c'}; col = char(col1);
col17 = lines(17);
col21 = lines(21);
colP = prism(10);
smap = {'-', '--', '-.', '.','-x','-o','-s'};
ls21 = {'-','-','-','-','-','-','-','--','--','--','--','--','--','--', ':', ':',':',':',':',':',':'};
ls = char({'LineStyle'}); 
mk = char({'Marker'}); 
cl = char({'Color'}); %col = char(col1);
ms = char({'MarkerSize'});
lw = char({'LineWidth'});
% Color palletes: lines, colorcube, prism

% ===================================================================
% 'XE','CS','BA','I2','TE','RU','MO','CE','LA','UO2','CD','AG','BO2','H2O','CON','CSI','CSM'
legend_all = {'XE','CS','BA','I2','TE','RU','MO','CE','LA','UO2','CD','AG','BO2','H2O','CON','CSI','CSM'};
legend_red = {'XE','CS','BA','I2','TE','RU','MO','CE','LA','UO2','CD','AG','CSI','CSM'};


%TIME XE	CS	BA	I2	TE	RU	MO	CE	LA	UO2	CD	AG	BO2	H2O	CON	CSI	CSM
%1	  2		3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18


%     'XE'    'CS'    'BA'    'I2'    'TE'    'RU'    'MO'    'CE'    'LA'    'UO2'    'CD'    'AG'    'BO2'    'H2O'    'CON'    'CSI'    'CSM'
%%'XE','CS', 'Ba','I2','TE','RU','MO','CE','LA','UO2','CD','AG','BO2','CON','CSI','CSM'

Cs_CsI = 0.511555957; %Mass fraction of Cs in CsI
I_CsI = 1 - Cs_CsI;   %Mass fraction of I2 in CsI
flag1 =1; %1 - new output

flag_unique = 1; %use data uniqness procedure


select_plot = [2,3,4,5,6,7,8,9,10,11,12,13,17,18]

fileINV = 'RNInventory.txt';


%file120 ='LUHS/217_A_LUHS_4_RST';
%file121 = 'LOOP/case0002_A4'
%file122 = 'LOOP/case0002_B7_results'
%file122 = 'C:\CloudStation\ROOT\NARSIS-GenIII\case0002_B23_18M'
%file122 = 'C:\CloudStation\ROOT\NARSIS-GenIII\case0002_B23b'

  folder = {'C:\CloudStation\ROOT\NARSIS-GenIII'}

    %subfolder =  {'case0002_B20'}
    %subfolder =  {'case0002_B20a'}
    %subfolder =  {'case0002_B20b_2'}
    
    %subfolder =  {'case0002_B20d'}
    %subfolder =  {'case0002_B20e'}
    
    %subfolder =  {'case0002_B21a'}
    %subfolder =  {'case0002_B21b'}
    %subfolder =  {'case0002_B21c'}
    %subfolder =  {'case0002_B21d'}
    %subfolder =  {'case0002_B21e'}
   
    %subfolder =  {'case0002_B23_18M'}
    %subfolder =  {'case0002_B20c'}   
    subfolder =  {'case0002_B23b'}


file122 = [folder{1} '/' subfolder{1}]

% ===================================================================
% TIME
time_scale = 3600;
%tend = 1.2e6/time_scale;
%tend = 170 %7 days
%tend = 120 %5 days
%tend=278  %1M
%tend=200; %720k
%tend=240; %10 days
tend = 300 
%tend = 500 %1.8M
time_label = 'Time, [h]';
case_name = num2str(tend) %'500h'
%xlabel(time_label)


file0 = file122; %<-=============
file = [file0 '/' num2str(tend)]
mkdir(file);

plot1 = 'RN_ENV_CVCLT';
plot2 = 'RN_RCS_TYCLAIR';
plot3 = 'RN_CONT_TYCLAIR';
plot4 = 'RN_RCS_TYCLT';



if(flag1 == 1)
plot5 = 'RN_CONT_TYCLT';
plot6 = 'RN_ENV_TYCLT';
end

plot7 = 'RN_ENV_CVCLT Fraction';
plot8 = 'RN_ENV_TYCLT Fraction';


a1 =    importdata([file0 '/' plot1]);
a2 =    importdata([file0 '/' plot2]);
a3 =    importdata([file0 '/' plot3]);
a4 =    importdata([file0 '/' plot4]);
if(flag1 == 1)
a5 =    importdata([file0 '/' plot5]);
a6 =    importdata([file0 '/' plot6]);
end

if flag_unique == 1
    a1 = data_unique(a1);
    a2 = data_unique(a2);
    a3 = data_unique(a3);
    a4 = data_unique(a4);
    a5 = data_unique(a5);
    a6 = data_unique(a6);
end

time =  a1(:,1);
time = time./time_scale;
a=0;

inv=importdata(fileINV)
RNi = inv.data(:,1)';
RNt = inv.textdata;


% ===================================================================
%% plot1 = 'RN_ENV_CVCLT';
figure(1+a)
hold on

%plot(a1(:,1)./time_scale,a1(:,select_plot),lw,1)
for i=select_plot
    hold on;
    plot(a1(:,1)./time_scale,a1(:,i),lw,1,cl,col21(i,:),ls,ls21{i})
end

%plot(time,a1(:,2)./RNi)

box on;
grid on;

xlabel(time_label)
legend(legend_red)
legend('Location','best');
ylabel('Mass, [kg]')
xlim([0 tend]);

set(0, 'DefaulttextInterpreter', 'none');



title(plot1);
    print('-dtiff',[file '/' plot1 '_' case_name '.tiff'])
    savefig([file '/' plot1 '_' case_name '.fig'])
	set(gca, 'YScale', 'log')
	ylim([1e-5 1000])
	print('-dtiff',[file '/' plot1 '_' case_name 'log.tiff'])
    savefig([file '/' plot1 '_' case_name 'log.fig'])
% ===================================================================
%% plot2 = 'RN_RCS_TYCLAIR';
figure(2+a)

for i=select_plot
	hold on
	plot(a2(:,1)./time_scale,a2(:,i),lw,1,cl,col21(i,:),ls,ls21{i})
end

box on;
grid on;

xlabel(time_label)
legend(legend_red) 
legend('Location','best');
ylabel('Mass, [kg]')
xlim([0 tend]);
 set(0, 'DefaulttextInterpreter', 'none');

title(plot2);
    print('-dtiff',[file '/' plot2 '_' case_name '.tiff'])
    savefig([file '/' plot2 '_' case_name '.fig'])
     set(gca, 'YScale', 'log')
	ylim([1e-5 1000])
	    print('-dtiff',[file '/' plot2 '_' case_name 'log.tiff'])
    savefig([file '/' plot2 '_' case_name 'log.fig'])
% ===================================================================    
%% plot3 = 'RN_CONT_TYCLAIR';
figure(3+a)
for i=select_plot
	hold on
	plot(a3(:,1)./time_scale,a3(:,i),lw,1,cl,col21(i,:),ls,ls21{i})
end	
box on;
grid on;

xlabel(time_label)
legend(legend_red)
legend('Location','best');
ylabel('Mass, [kg]')

xlim([0 tend]);
set(0, 'DefaulttextInterpreter', 'none');
title(plot3);
    print('-dtiff',[file '/' plot3 '_' case_name '.tiff'])
    savefig([file '/' plot3 '_' case_name '.fig'])
    set(gca, 'YScale', 'log')
ylim([1e-5 1000])
    print('-dtiff',[file '/' plot3 '_' case_name 'log.tiff'])
    savefig([file '/' plot3 '_' case_name 'log.fig'])
% ===================================================================    
%% plot4 = 'RN_RCS_TYCLT';
figure(4+a)
for i=select_plot
	hold on
	plot(a4(:,1)./time_scale,a4(:,i),lw,1,cl,col21(i,:),ls,ls21{i})
end	
box on;
grid on;

xlabel(time_label)
legend(legend_red)
legend('Location','best');
ylabel('Mass, [kg]')


xlim([0 tend]);
set(0, 'DefaulttextInterpreter', 'none');
title(plot4);
    print('-dtiff',[file '/' plot4 '_' case_name '.tiff'])
    savefig([file '/' plot4 '_' case_name '.fig'])
    set(gca, 'YScale', 'log')
	ylim([1e-5 1000])
	print('-dtiff',[file '/' plot4 '_' case_name 'log.tiff'])
    savefig([file '/' plot4 '_' case_name 'log.fig'])

% ===================================================================
if(flag1 == 1)

%% plot5 = 'RN_CONT_TYCLT';

figure(5+a)
for i=select_plot
	hold on
	plot(a5(:,1)./time_scale,a5(:,i),lw,1,cl,col21(i,:),ls,ls21{i})
end
box on;
grid on;

xlabel(time_label)
legend(legend_red)
legend('Location','best');
ylabel('Mass, [kg]')


xlim([0 tend]);
set(0, 'DefaulttextInterpreter', 'none');
title(plot5);
    print('-dtiff',[file '/' plot5 '_' case_name '.tiff'])
    savefig([file '/' plot5 '_' case_name '.fig'])
set(gca, 'YScale', 'log')
ylim([1e-5 1000])
    print('-dtiff',[file '/' plot5 '_' case_name 'log.tiff'])
    savefig([file '/' plot5 '_' case_name 'log.fig'])
% ===================================================================
%% plot6 = 'RN_ENV_TYCLT';

figure(6+a)
for i=select_plot
	hold on
	plot(a6(:,1)./time_scale,a6(:,i),lw,1,cl,col21(i,:),ls,ls21{i})
end
box on;
grid on;

xlabel(time_label)
legend(legend_red)
legend('Location','best');
ylabel('Mass, [kg]')
xlim([0 tend]);
 
set(0, 'DefaulttextInterpreter', 'none');

title(plot6);
    print('-dtiff',[file '/' plot6 '_' case_name '.tiff'])
    savefig([file '/' plot6 '_' case_name '.fig'])
   set(gca, 'YScale', 'log')
	ylim([1e-5 1000])
	print('-dtiff',[file '/' plot6 '_' case_name 'log.tiff'])
    savefig([file '/' plot6 '_' case_name 'log.fig'])
end

%% ===========================================================================
%plot7 Release fractions

%TIME XE	CS	BA	I2	TE	RU	MO	CE	LA	UO2	CD	AG	BO2	H2O	CON	CSI	CSM
%1	  2		3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18

figure(7+a)
hold on
%for i=2:13
%    plot(a1(:,1)./time_scale,a1(:,i)'./RNi(i-1)','LineWidth',1,'Color',[rand rand rand])
%end
release_fraction7 = [];
release_7 = [];
release_7 = [0; a1(:,1)./time_scale ]; 
for i=2:13  %select_plot
    
	i
    if(i==3) %Cs
        data = (a1(:,i)' + Cs_CsI.*a1(:,17)')./RNi(i-1)';
        plot(a1(:,1)./time_scale,data,lw,1,cl,col21(i,:),ls,ls21{i}); %,'Color',[rand rand rand])
    elseif(i==5) %I
        data = (a1(:,i)' + I_CsI.*a1(:,17)')./RNi(i-1)';
        plot(a1(:,1)./time_scale,data,lw,1,cl,col21(i,:),ls,ls21{i}); %,'Color',[rand rand rand])       
    else
	data = a1(:,i)'./RNi(i-1)';
        plot(a1(:,1)./time_scale,data,lw,1,cl,col21(i,:),ls,ls21{i});  %,'Color',[rand rand rand])
    end
    
    release_fraction7 = [release_fraction7 ; [i-1, data(end)]];
    release_time7 =  a1(end,1);
    
    release_7 = [release_7, [i;data']];
end

box on;
grid on;

xlabel(time_label)
legend(legend_red)
%legend('XE','CS','BA','I2','TE','RU','MO','CE','LA','UO2','CD','AG','CSI','CSM');

legend('Location','best');
ylabel('Release Fraction, [-]')
xlim([0 tend]);
set(0, 'DefaulttextInterpreter', 'none');

title(plot7);
    print('-dtiff',[file '/' plot7 '_' case_name '.tiff'])
    savefig([file '/' plot7 '_' case_name '.fig'])

set(gca, 'YScale', 'log')
ylim([1e-8 1])
print('-dtiff',[file '/' plot7 '_' case_name 'log.tiff'])
    savefig([file '/' plot7 '_' case_name 'log.fig'])



%% ===========================================================================
if(flag1 == 1)
%%plot8 Release fractions
% like plot6
figure(8+a)
hold on
release_fraction8 = [];
release_8 = [];
release_8 = [0; a6(:,1)./time_scale ]; 
for i=2:13  %select_plot
	i
        
    if(i==3) %Cs
        data = (a6(:,i)' + Cs_CsI.*a6(:,17)')./RNi(i-1)';
        plot(a6(:,1)./time_scale,data,lw,1,cl,col21(i,:),ls,ls21{i}); %,'Color',[rand rand rand])
    elseif(i==5) %I
        data = (a6(:,i)' + I_CsI.*a6(:,17)')./RNi(i-1)';
        plot(a6(:,1)./time_scale,data,lw,1,cl,col21(i,:),ls,ls21{i}); %,'Color',[rand rand rand])       
    else
		data = a6(:,i)'./RNi(i-1)';
        plot(a6(:,1)./time_scale,data,lw,1,cl,col21(i,:),ls,ls21{i}); %,'Color') %,[rand rand rand])
    end
    
    
	release_fraction8 = [release_fraction8 ; [i-1, data(end)]];
	release_time8 =  a6(end,1);
    
    release_8 = [release_8, [i;data']];
    
    
end
box on;
grid on;

xlabel(time_label)
legend(legend_red)
legend('Location','best');
ylabel('Release Fraction, [-]')
xlim([0 tend]);
set(0, 'DefaulttextInterpreter', 'none');

title(plot8);
    print('-dtiff',[file '/' plot8 '_' case_name '.tiff'])
    savefig([file '/' plot8 '_' case_name '.fig'])    
    set(gca, 'YScale', 'log')
	ylim([1e-8 1])
	print('-dtiff',[file '/' plot8 '_' case_name 'log.tiff'])
	savefig([file '/' plot8 '_' case_name 'log.fig'])    
	end
%% ===================================================================    
save([file0 '/' num2str(tend) 'h_fraction7.txt'], 'release_fraction7', '-ASCII');
save([file0 '/' num2str(tend) 'h_fraction8.txt'], 'release_fraction8', '-ASCII');

save([file0 '/' num2str(tend) 'h_fraction7_time.txt'], 'release_7', '-ASCII');
save([file0 '/' num2str(tend) 'h_fraction8_time.txt'], 'release_8', '-ASCII');
