%RN_READER
%Piotr Darnowski
%Post Processing Radionuclides
%30.06.2014
clc; close all; clear;
%set(gca, 'ColorOrder', ColorSet);

%ColorSet = colormap(hsv(128));
%ColorSet = colormap(jet(20));
%ColorSet = varycolor(20);

%ColorSet = varycolor(50);

fileINV = 'RNInventory.txt';

file1 = 'LOOP/210_A_SERWER';
file2 = 'LOOP/210_B_SERWER';
file3 = 'LOOP/212A2';
file4 = 'LOOP/212B2';
file5 = 'LOOP/213_B1';
file6 = 'LOOP/213_B2';
file7 = 'LOOP/213_B3';
file5 = 'SBO/EPR213_B1_SBO';
file6 = 'SBO/EPR213_B2_SBO';
file7 = 'SBO/EPR213_B3_SBO';
file100 = 'SBO/214_SBO_B8';
file101 = 'SBO/214_SBO_B4';
file104 = 'SBO/215_SBO_B1';
file105 = 'SBO/215_SBO_B2';
file106 = 'SBO/215_SBO_B3';
file107 = 'LBLOCA/225_LBLOCA_ver1_1';
file119 ='LUHS/217_A_LUHS_4';
file120 ='LUHS/217_A_LUHS_4_RST';





Cs_CsI = 0.511555957; %Mass fraction of Cs in CsI
I_CsI = 1 - Cs_CsI;   %Mass fraction of I2 in CsI
flag1 =1; %1 - new output
%tend = 24;
tend = 320;

file = file119; %<-=============
file2 = file120; %<====== drugi plik


plot1 = 'RN_ENV_CVCLT';
plot2 = 'RN_RCS_TYCLAIR';
plot3 = 'RN_CONT_TYCLAIR';
plot4 = 'RN_RCS_TYCLT';



if(flag1 == 1)
plot5 = 'RN_CONT_TYCLT';
plot6 = 'RN_ENV_TYCLT';
end

plot7 = 'RN_ENV_CVCLT Fraction';
plot8 = 'RN_ENV_TYCLT Fraction';

a1 =    importdata([file '/' plot1]);
a2 =    importdata([file '/' plot2]);
a3 =    importdata([file '/' plot3]);
a4 =    importdata([file '/' plot4]);

a12 =    importdata([file2 '/' plot1]);
a22 =    importdata([file2 '/' plot2]);
a32 =    importdata([file2 '/' plot3]);
a42 =    importdata([file2 '/' plot4]);
if(flag1 == 1)
a5 =    importdata([file '/' plot5]);
a6 =    importdata([file '/' plot6]);

a52 =    importdata([file2 '/' plot5]);
a62 =    importdata([file2 '/' plot6]);
end

time =  a1(:,1);
time = time./3600;
time2 = a12(:,1);
time2 = time2./3600;
time3 = [time; time2];

a=0;
%     'XE'    'CS'    'BA'    'I2'    'TE'    'RU'    'MO'    'CE'    'LA'    'UO2'    'CD'    'AG'    'BO2'    'H2O'    'CON'    'CSI'    'CSM'
%%'XE','CS', 'Ba','I2','TE','RU','MO','CE','LA','UO2','CD','AG','BO2','CON','CSI','CSM'
inv=importdata(fileINV);
RNi = inv.data(:,1)';
RNt = inv.textdata;

%% plot1 = 'RN_ENV_CVCLT';
figure(1+a)
hold on

data = a1(:,2:end);
data2 = a12(:,2:end);
data3 = [data; data2];
%set(gca, 'ColorOrder', ColorSet);
plot(time3, data3, 'LineWidth',2)

%plot(a1(:,1)./3600,a1(:,2:end),'LineWidth',2)
%plot(a12(:,1)./3600,a12(:,2:end),'LineWidth',2)
%semilogy(time,a1(:,2)./RNi)

box on;
grid on;
xlabel('Time, [s]');
xlabel('Time, [h]');
legend('XE','CS','BA','I2','TE','RU','MO','CE','LA','UO2','CD','AG','BO2','H2O','CON','CSI','CSM');
ylabel('Mass, [kg]')
xlim([0 tend]);

set(0, 'DefaulttextInterpreter', 'none');
title(plot1);
    print('-djpeg100','-r200',[file '/' plot1 '.jpg'])
    
    
%% plot2 = 'RN_RCS_TYCLAIR';
figure(2+a)
hold on
%plot(time,a1(:,2:end),'LineWidth',1);
%semilogy(a2(:,1)./3600,a2(:,2:end),'LineWidth',2)
%semilogy(a22(:,1)./3600,a22(:,2:end),'LineWidth',2)
%semilogy(time,a1(:,2));

data = a2(:,2:end);
data2 = a22(:,2:end);
data3 = [data; data2];
plot(time3, data3, 'LineWidth',2)

box on;
grid on;
xlabel('Time, [s]');
xlabel('Time, [h]');
legend('XE','CS','BA','I2','TE','RU','MO','CE','LA','UO2','CD','AG','BO2','H2O','CON','CSI','CSM');
ylabel('Mass, [kg]')
xlim([0 tend]);
 set(0, 'DefaulttextInterpreter', 'none');
title(plot2);
    print('-djpeg100','-r200',[file '/' plot2 '.jpg'])
    
    
%% plot3 = 'RN_CONT_TYCLAIR';
figure(3+a)
hold on

data = a3(:,2:end);
data2 = a32(:,2:end);
data3 = [data; data2];
plot(time3, data3, 'LineWidth',2)
%semilogy(a3(:,1)./3600,a3(:,2:end),'LineWidth',2)
%semilogy(a32(:,1)./3600,a32(:,2:end),'LineWidth',2)
box on;
grid on;
xlabel('Time, [s]');
xlabel('Time, [h]');
legend('XE','CS','BA','I2','TE','RU','MO','CE','LA','UO2','CD','AG','BO2','H2O','CON','CSI','CSM');
ylabel('Mass, [kg]')
xlim([0 tend]);
set(0, 'DefaulttextInterpreter', 'none');
title(plot3);
    print('-djpeg100','-r200',[file '/' plot3 '.jpg'])
    
    
%% plot4 = 'RN_RCS_TYCLT';
figure(4+a)
hold on
%semilogy(time,a4(:,2:end),'LineWidth',1)
%semilogy(a4(:,1)./3600,a4(:,2:end),'LineWidth',2)
%semilogy(a42(:,1)./3600,a42(:,2:end),'LineWidth',2)
data = a4(:,2:end);
data2 = a42(:,2:end);
data3 = [data; data2];
plot(time3, data3, 'LineWidth',2)
box on;
grid on;
xlabel('Time, [s]');
xlabel('Time, [h]');
legend('XE','CS','BA','I2','TE','RU','MO','CE','LA','UO2','CD','AG','BO2','H2O','CON','CSI','CSM');
ylabel('Mass, [kg]')
xlim([0 tend]);
set(0, 'DefaulttextInterpreter', 'none');
title(plot4);
    print('-djpeg100','-r200',[file '/' plot4 '.jpg'])
    


if(flag1 == 1)

%% plot5 = 'RN_CONT_TYCLT';

figure(5+a)
hold on
%semilogy(a5(:,1)./3600,a5(:,2:end),'LineWidth',2)
%semilogy(a52(:,1)./3600,a52(:,2:end),'LineWidth',2)
data = a5(:,2:end);
data2 = a52(:,2:end);
data3 = [data; data2];
plot(time3, data3, 'LineWidth',2)
box on;
grid on;
xlabel('Time, [s]');
xlabel('Time, [h]');
legend('XE','CS','BA','I2','TE','RU','MO','CE','LA','UO2','CD','AG','BO2','H2O','CON','CSI','CSM');
ylabel('Mass, [kg]')
xlim([0 tend]);
set(0, 'DefaulttextInterpreter', 'none');
title(plot5);
    print('-djpeg100','-r200',[file '/' plot5 '.jpg'])
    


%% plot6 = 'RN_ENV_TYCLT';

figure(6+a)
hold on
data = a6(:,2:end);
data2 = a62(:,2:end);
data3 = [data; data2];
plot(time3, data3, 'LineWidth',2)
%semilogy(a6(:,1)./3600,a6(:,2:end),'LineWidth',2)
%semilogy(a62(:,1)./3600,a62(:,2:end),'LineWidth',2)
box on;
grid on;
xlabel('Time, [s]');
xlabel('Time, [h]');
legend('XE','CS','BA','I2','TE','RU','MO','CE','LA','UO2','CD','AG','BO2','H2O','CON','CSI','CSM');
ylabel('Mass, [kg]')
xlim([0 tend]);
set(0, 'DefaulttextInterpreter', 'none');
title(plot6);
    print('-djpeg100','-r200',[file '/' plot6 '.jpg'])
    
    
end

%===========================================================================
%%plot7 Release fractions
% like plot1
figure(7+a)
hold on
%for i=2:13
%    plot(a1(:,1)./3600,a1(:,i)'./RNi(i-1)','LineWidth',2,'Color',[rand rand rand])
%end
rcol = abs([rand(18,1) rand(18,1) rand(18,1)]);
for i=2:13
 col = [rcol(i,1) rcol(i,2) rcol(i,3)];
    if(i==3) %Cs
        data  = (a1(:,i)' + Cs_CsI.*a1(:,17)')./RNi(i-1)';
        data2 = (a12(:,i)' + Cs_CsI.*a12(:,17)')./RNi(i-1)';
        data3 = [data, data2];  
        plot(time3,data3','LineWidth',2,'Color',col);
        %plot(a1(:,1)./3600,data,'LineWidth',2,'Color',col)
        %plot(a12(:,1)./3600,data2,'LineWidth',2,'Color',col)
    elseif(i==5) %I
        data = (a1(:,i)' + I_CsI.*a1(:,17)')./RNi(i-1)';
        data2 = (a12(:,i)' + I_CsI.*a12(:,17)')./RNi(i-1)';
        data3 = [data, data2];
        plot(time3,data3','LineWidth',2,'Color',col);
        %plot(a1(:,1)./3600,data,'LineWidth',2,'Color',col)       
        %plot(a12(:,1)./3600,data2,'LineWidth',2,'Color',col)   
    else
        data = a1(:,i)'./RNi(i-1)';
        data2 =a12(:,i)'./RNi(i-1)';
        data3 = [data, data2];  
        plot(time3,data3','LineWidth',2,'Color',col);
        %plot(a12(:,1)./3600,a12(:,i)'./RNi(i-1)','LineWidth',2,'Color',col)
    end
end

box on;
grid on;
xlabel('Time, [s]');
xlabel('Time, [h]');
legend('XE','CS','BA','I2','TE','RU','MO','CE','LA','UO2','CD','AG','BO2','H2O','CON','CSI','CSM');
ylabel('Release Fraction, [-]')
xlim([0 tend]);
set(0, 'DefaulttextInterpreter', 'none');
title(plot7);
    print('-djpeg100','-r200',[file '/' plot7 '.jpg'])

    if(flag1 == 1)
    %===========================================================================
%%plot8 Release fractions
% like plot6
figure(8+a)
hold on
%rcol = [rand(13,1) rand(13,1) rand(13,1)];
for i=2:13
    col = [rcol(i,1) rcol(i,2) rcol(i,3)];
    if(i==3) %Cs
        data = (a6(:,i)' + Cs_CsI.*a6(:,17)')./RNi(i-1)';
        data2 = (a62(:,i)' + Cs_CsI.*a62(:,17)')./RNi(i-1)';
        data3 = [data, data2];
        %plot(a6(:,1)./3600,data,'LineWidth',2,'Color',col)
         plot(time3,data3,'LineWidth',2,'Color',col)
        %plot(a62(:,1)./3600,data2,'LineWidth',2,'Color',col)
    elseif(i==5) %I
        data = (a6(:,i)' + I_CsI.*a6(:,17)')./RNi(i-1)';
        data2 = (a62(:,i)' + I_CsI.*a62(:,17)')./RNi(i-1)';
        data3 = [data, data2];
        %plot(a6(:,1)./3600,data,'LineWidth',2,'Color',col)             
        %plot(a62(:,1)./3600,data2,'LineWidth',2,'Color',col)     
         plot(time3,data3,'LineWidth',2,'Color',col) 
    else
        data1 = a6(:,i)'./RNi(i-1)';
        data2 = a62(:,i)'./RNi(i-1)';
        data3 = [data, data2];
        %plot(a6(:,1)./3600,a6(:,i)'./RNi(i-1)','LineWidth',2,'Color',col)
        %plot(a62(:,1)./3600,a62(:,i)'./RNi(i-1)','LineWidth',2,'Color',col)
        plot(time3,data3,'LineWidth',2,'Color',col)
    end
end
box on;
grid on;
xlabel('Time, [s]');
xlabel('Time, [h]');
legend('XE','CS','BA','I2','TE','RU','MO','CE','LA','UO2','CD','AG','BO2','H2O','CON','CSI','CSM');
ylabel('Release Fraction, [-]')
xlim([0 tend]);
set(0, 'DefaulttextInterpreter', 'none');
title(plot8);
    print('-djpeg100','-r200',[file '/' plot8 '.jpg'])
    
    end
    
    
