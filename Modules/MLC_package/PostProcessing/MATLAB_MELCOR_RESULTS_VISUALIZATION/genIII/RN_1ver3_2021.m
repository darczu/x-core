%RN_READER
%Piotr Darnowski
%Post Processing Radionuclides
% set(gca, 'YScale', 'log')
%30.06.2014, updated 2021
% ===================================================================
% COLOR MAPS
cmap6 = hsv(6); cmap8 = hsv(8); cmap11 = hsv(11); cmap14 = hsv(14); cmap28 = hsv(28); cmap16 = hsv(16); cmap18 = hsv(19); %color maps
cmapT = ['k', 'r', 'b', 'm', 'y','c','k','b'];
cmapcases = hsv(numel(cases));
col1 = { 'b','r', 'm', 'c'}; col = char(col1);
colL = lines(17*2);
colP = prism(10);
smap = {'-', '--', '-.', '.','-x','-o','-s'};
ls = char({'LineStyle'}); 
cl = char({'Color'}); %col = char(col1);
ms = char({'MarkerSize'});
lw = char({'LineWidth'});
% Color palletes: lines, colorcube, prism
% ===================================================================


clc; close all; clear;
fileINV = 'RNInventory.txt';

file1 = 'LOOP/210_A_SERWER';
file2 = 'LOOP/210_B_SERWER';
file3 = 'LOOP/212A2';
file4 = 'LOOP/212B2';
file5 = 'LOOP/213_B1';
file6 = 'LOOP/213_B2';
file7 = 'LOOP/213_B3';
file5 = 'SBO/EPR213_B1_SBO';
file6 = 'SBO/EPR213_B2_SBO';
file7 = 'SBO/EPR213_B3_SBO';
file100 = 'SBO/214_SBO_B8';
file101 = 'SBO/214_SBO_B4';
file104 = 'SBO/215_SBO_B1';
file105 = 'SBO/215_SBO_B2';
file106 = 'SBO/215_SBO_B3';
file107 = 'LBLOCA/225_LBLOCA_ver1_1';
file119 ='LUHS/217_A_LUHS_4';
file120 ='LUHS/217_A_LUHS_4_RST';


%file121 = 'LOOP/case0002_A4'
%file122 = 'LOOP/case0002_B7_results'

file122 = 'C:\CloudStation\ROOT\NARSIS-GenIII\case0002_B23'



Cs_CsI = 0.511555957; %Mass fraction of Cs in CsI
I_CsI = 1 - Cs_CsI;   %Mass fraction of I2 in CsI
flag1 =1; %1 - new output

flag_unique = 1; %use data uniqness procedure

time_scale = 3600;

tend = 1.2e6/time_scale;
%tend = 320;

file = file122; %<-=============

plot1 = 'RN_ENV_CVCLT';
plot2 = 'RN_RCS_TYCLAIR';
plot3 = 'RN_CONT_TYCLAIR';
plot4 = 'RN_RCS_TYCLT';



if(flag1 == 1)
plot5 = 'RN_CONT_TYCLT';
plot6 = 'RN_ENV_TYCLT';
end

plot7 = 'RN_ENV_CVCLT Fraction';
plot8 = 'RN_ENV_TYCLT Fraction';


a1 =    importdata([file '/' plot1]);
a2 =    importdata([file '/' plot2]);
a3 =    importdata([file '/' plot3]);
a4 =    importdata([file '/' plot4]);
if(flag1 == 1)
a5 =    importdata([file '/' plot5]);
a6 =    importdata([file '/' plot6]);
end

if flag_unique == 1
    a1 = data_unique(a1);
    a2 = data_unique(a2);
    a3 = data_unique(a3);
    a4 = data_unique(a4);
    a5 = data_unique(a5);
    a6 = data_unique(a6);
end

time =  a1(:,1);
time = time./time_scale;
a=0;
%     'XE'    'CS'    'BA'    'I2'    'TE'    'RU'    'MO'    'CE'    'LA'    'UO2'    'CD'    'AG'    'BO2'    'H2O'    'CON'    'CSI'    'CSM'
%%'XE','CS', 'Ba','I2','TE','RU','MO','CE','LA','UO2','CD','AG','BO2','CON','CSI','CSM'
inv=importdata(fileINV)
RNi = inv.data(:,1)';
RNt = inv.textdata;

%% plot1 = 'RN_ENV_CVCLT';
figure(1+a)
hold on
plot(a1(:,1)./time_scale,a1(:,2:18),'LineWidth',1)
%plot(time,a1(:,2)./RNi)

box on;
grid on;
xlabel('Time, [s]');
xlabel('Time, [h]');
legend('XE','CS','BA','I2','TE','RU','MO','CE','LA','UO2','CD','AG','BO2','H2O','CON','CSI','CSM');
ylabel('Mass, [kg]')
xlim([0 tend]);

set(0, 'DefaulttextInterpreter', 'none');
title(plot1);
    print('-dtiff',[file '/' plot1 '.tiff'])
    savefig([file '/' plot1 '.fig'])
    
%% plot2 = 'RN_RCS_TYCLAIR';
figure(2+a)
hold on
%plot(time,a1(:,2:18),'LineWidth',1);
plot(a2(:,1)./time_scale,a2(:,2:18),'LineWidth',1)

%plot(time,a1(:,2));

box on;
grid on;
xlabel('Time, [s]');
xlabel('Time, [h]');
legend('XE','CS','BA','I2','TE','RU','MO','CE','LA','UO2','CD','AG','BO2','H2O','CON','CSI','CSM');
ylabel('Mass, [kg]')
xlim([0 tend]);
 set(0, 'DefaulttextInterpreter', 'none');
title(plot2);
    print('-dtiff',[file '/' plot2 '.tiff'])
    savefig([file '/' plot2 '.fig'])
    
    
%% plot3 = 'RN_CONT_TYCLAIR';
figure(3+a)
hold on
plot(a3(:,1)./time_scale,a3(:,2:18),'LineWidth',1)
box on;
grid on;
xlabel('Time, [s]');
xlabel('Time, [h]');
legend('XE','CS','BA','I2','TE','RU','MO','CE','LA','UO2','CD','AG','BO2','H2O','CON','CSI','CSM');
ylabel('Mass, [kg]')
xlim([0 tend]);
set(0, 'DefaulttextInterpreter', 'none');
title(plot3);
    print('-dtiff',[file '/' plot3 '.tiff'])
    savefig([file '/' plot3 '.fig'])
    
    
%% plot4 = 'RN_RCS_TYCLT';
figure(4+a)
hold on
%plot(time,a4(:,2:18),'LineWidth',1)
plot(a4(:,1)./time_scale,a4(:,2:18),'LineWidth',1)
box on;
grid on;
xlabel('Time, [s]');
xlabel('Time, [h]');
legend('XE','CS','BA','I2','TE','RU','MO','CE','LA','UO2','CD','AG','BO2','H2O','CON','CSI','CSM');
ylabel('Mass, [kg]')
xlim([0 tend]);
set(0, 'DefaulttextInterpreter', 'none');
title(plot4);
    print('-dtiff',[file '/' plot4 '.tiff'])
    savefig([file '/' plot4 '.fig'])
    


if(flag1 == 1)

%% plot5 = 'RN_CONT_TYCLT';

figure(5+a)
hold on
plot(a5(:,1)./time_scale,a5(:,2:18),'LineWidth',1)
box on;
grid on;
xlabel('Time, [s]');
xlabel('Time, [h]');
legend('XE','CS','BA','I2','TE','RU','MO','CE','LA','UO2','CD','AG','BO2','H2O','CON','CSI','CSM');
ylabel('Mass, [kg]')
xlim([0 tend]);
set(0, 'DefaulttextInterpreter', 'none');
title(plot5);
    print('-dtiff',[file '/' plot5 '.tiff'])
    savefig([file '/' plot5 '.fig'])


%% plot6 = 'RN_ENV_TYCLT';

figure(6+a)
hold on
plot(a6(:,1)./time_scale,a6(:,2:18),'LineWidth',1)
box on;
grid on;
xlabel('Time, [s]');
xlabel('Time, [h]');
legend('XE','CS','BA','I2','TE','RU','MO','CE','LA','UO2','CD','AG','BO2','H2O','CON','CSI','CSM');
ylabel('Mass, [kg]')
xlim([0 tend]);
set(0, 'DefaulttextInterpreter', 'none');
title(plot6);
    print('-dtiff',[file '/' plot6 '.tiff'])
    savefig([file '/' plot6 '.fig'])
    
end

%===========================================================================
%%plot7 Release fractions
% like plot1
figure(7+a)
hold on
%for i=2:13
%    plot(a1(:,1)./time_scale,a1(:,i)'./RNi(i-1)','LineWidth',1,'Color',[rand rand rand])
%end

for i=2:13
    if(i==3) %Cs
        data = (a1(:,i)' + Cs_CsI.*a1(:,17)')./RNi(i-1)'
        plot(a1(:,1)./time_scale,data,'LineWidth',1,'Color',[rand rand rand])
    elseif(i==5) %I
        data = (a1(:,i)' + I_CsI.*a1(:,17)')./RNi(i-1)'
        plot(a1(:,1)./time_scale,data,'LineWidth',1,'Color',[rand rand rand])       
    else
        plot(a1(:,1)./time_scale,a1(:,i)'./RNi(i-1)','LineWidth',1,'Color',[rand rand rand])
    end
end

box on;
grid on;
xlabel('Time, [s]');
xlabel('Time, [h]');
legend('XE','CS','BA','I2','TE','RU','MO','CE','LA','UO2','CD','AG','BO2','H2O','CON','CSI','CSM');
ylabel('Release Fraction, [-]')
xlim([0 tend]);
set(0, 'DefaulttextInterpreter', 'none');
title(plot7);
    print('-dtiff',[file '/' plot7 '.tiff'])
    savefig([file '/' plot7 '.fig'])

    if(flag1 == 1)
    %===========================================================================
%%plot8 Release fractions
% like plot6
figure(8+a)
hold on
for i=2:13
    if(i==3) %Cs
        data = (a6(:,i)' + Cs_CsI.*a6(:,17)')./RNi(i-1)'
        plot(a6(:,1)./time_scale,data,'LineWidth',1,'Color',[rand rand rand])
    elseif(i==5) %I
        data = (a6(:,i)' + I_CsI.*a6(:,17)')./RNi(i-1)'
        plot(a6(:,1)./time_scale,data,'LineWidth',1,'Color',[rand rand rand])       
    else
        plot(a6(:,1)./time_scale,a6(:,i)'./RNi(i-1)','LineWidth',1,'Color',[rand rand rand])
    end
end
box on;
grid on;
xlabel('Time, [s]');
xlabel('Time, [h]');
legend('XE','CS','BA','I2','TE','RU','MO','CE','LA','UO2','CD','AG','BO2','H2O','CON','CSI','CSM');
ylabel('Release Fraction, [-]')
xlim([0 tend]);
set(0, 'DefaulttextInterpreter', 'none');
title(plot8);
    print('-dtiff',[file '/' plot8 '.tiff'])
    savefig([file '/' plot8 '.fig'])
    
    end
    
    
