% NARSIS 2019 modifications - Piotr Darnowski
%
% 
%%EDF Reader Skrypt Matlab
%%Projekt SARWUT Piotr Darnowski
%%27.11.2013 zmiany 4.12.2013
%rozwijany do 20.07.2014
%zmiany modiyfikacje 25.02.2015 Mod dla dedykowane pod artykul w Nukleonice i konferencj� ERMSAR
%Poprzednia wersja 6 byla dla NEDa
clc; close all; clear;
%fileMAAP =  'MAAP4_FULL.xlsx';  
%band = '8590'; %Ex-Vessel phase

band = '3603'; %zakres dla pliku MAAP4 z fazy in-vessel Total Loss of Ac Power
 ffff

%===========================================================
fileMAAP = 'MAAP4.xlsx';
flagMAAP = 0; %read MAAP  Total Loss of Ac Power results
flagSNAP = 1; %computed with snap 1 / lunchpad 0
flagMANY = 0; %many plot series  1 / one plot series 0
flag_hours = 1; %1 - hours 0 - seconds
timetime = 30000/3600;%8; %7 % 24 %66.0;  calculation time %<================================= TIME

flagPLOT = 1; %flaga rysowania  wykres�w
flagJPG = 1; %zapis do JPG 
flagH2 = 0; %1 - actvie 0 - nonactive - new calculaion of H2 release rate based on derivatives

title_flag=2; %0 - no title; 1 - title with file localization (subfolder) - helps to identificate file; 2 - title with information about what is on the plot 
flag_add = 0;  % Additional flag 1 - 2A-LOCA; 2 - SLLOCA; 0 no add flag %<=======
flag_add2 = 0;  %Dla drugiego wykresu  1 - 2A-LOCA; 2 - SLLOCA; 0 no add flag %<=======
% ==================

flag_all= 0;  %MORE DATA - for LBLOCA - new plots - 2015.02

%===========================================================
a = 0;     %figures numerbing a+1
run files  %file list
run MAAPdata %even if no MAAP files

%Vessel Breach Line and MAAP data read
if(flagMAAP==1) 
    tfailh = 6 + (50/60); tfail = (tfailh)*3600; 
    disp(['MAAP Vessel fails at 6 [h] 50 [min], it is: ', num2str(tfail), ' [s]']); 
end
flag3 = 0; %2 - LBLOCAcl
flag4 = 0; %1 - restart 0 - normal 
%%schematy kolor�w
col1 = {'r', 'b', 'm', 'c'}; 
%===========================================================
%ERMSAR 235, 239
fileMAIN = file302  %main folder
%===========================================================
%%

    a=0;
    read2_302(a, fileMAIN, flagPLOT, flagJPG, flag3, flagMAAP, flagSNAP, flagH2, title_flag, flag_all, flag_add,  flag_hours, timetime, char(col1),...
        MAAP4_T, MAAP4_Th, MAAP4_1,	MAAP4_2,	MAAP4_3_1,	MAAP4_3_2,	MAAP4_4,	MAAP4_5,	MAAP4_6, ...
        MAAP4_7_1,	MAAP4_7_2,	MAAP4_8_1,	MAAP4_8_2,	MAAP4_9_1,	MAAP4_9_2,	MAAP4_10_1,	MAAP4_10_2, ...
        MAAP4_11_1,	MAAP4_11_2,	MAAP4_12,	MAAP4_13,	MAAP4_14,	MAAP4_15 ); 

 %%===========================================================
    a = 0;

    % ============================================================
    % ADDITIONAL PLOTS 
    if(flagMANY ==1)
        %filexxx = {file104, file105, file106} %<==========
        %filexxx = {file108, file109}
    filexxx={file235}
        N = 2; k=10; %ile kolorow
        cmap=[];
        for i=1:1
            cmap(1:k,1:3,i) = cool(k);  %hsv, hot, cool, etc
            %cmap(1:k,1:3,i) = i.*cool(k)./2.*i;  %hsv, hot, cool, etc
           col1 = cmap(:,:,i); %old
      col1 = {'c', 'g', 'k', 'y'}; 
        col1 = char(col1)
        
        read2_302(a, char(filexxx(i)), flagPLOT, flagJPG, flag3, flagMAAP, flagSNAP, flagH2, title_flag, flag_all, flag_add2, flag_hours, timetime, col1,...
        MAAP4_T, MAAP4_Th, MAAP4_1,	MAAP4_2,	MAAP4_3_1,	MAAP4_3_2,	MAAP4_4,	MAAP4_5,	MAAP4_6, ...
        MAAP4_7_1,	MAAP4_7_2,	MAAP4_8_1,	MAAP4_8_2,	MAAP4_9_1,	MAAP4_9_2,	MAAP4_10_1,	MAAP4_10_2, ...
        MAAP4_11_1,	MAAP4_11_2,	MAAP4_12,	MAAP4_13,	MAAP4_14,	MAAP4_15 );  
           
        %a = a+20;cos - nowe wykresy!!!
        
       % read2_215_ver2(a, char(filexxx(i)), flagPLOT, flagJPG, flag3, flagMAAP, flagSNAP, timetime, col1,...
        %    MAAP4_T, MAAP4_Th, MAAP4_1,	MAAP4_2,	MAAP4_3_1,	MAAP4_3_2,	MAAP4_4,	MAAP4_5,	MAAP4_6, ...
         %   MAAP4_7_1,	MAAP4_7_2,	MAAP4_8_1,	MAAP4_8_2,	MAAP4_9_1,	MAAP4_9_2,	MAAP4_10_1,	MAAP4_10_2, ...
         %   MAAP4_11_1,	MAAP4_11_2,	MAAP4_12,	MAAP4_13,	MAAP4_14,	MAAP4_15 );  
        end
    end
    %===========================================================

%%===========================================================