%  PLOT_RPV_TH

% CF1504 and 1505 are broken !

if(flag1 == 1) %PLOT OR NOT
% ===================================================================
% FIGURE 3 - Core mass of material
% ===================================================================
content = {'Mass of Materials'};
run standard_plot1v1.m;
mlcfile  ='MOLTEN_CORE';
maapfile = 'MCR';
k = detectfile(mlcfile,EDF.file);       %MELCOR file
 m1 = detectfile(maapfile,MAAP4.file);     %MAAP4 file

        if(flagMAAP == 1)
            x1 = MAAP4.T;
            y1 = MAAP4.data{m1};;
            plot(x1,y1,lw,1,ms,2,cl,'r',ls,'-');    labels = [labels, 'MAAP4 MCR' ];
        end
		 if ~or(sum(k == emptyfile),k==0) 
			x2 = EDF.data{k}(:,1)./Tconst;
			y2 = EDF.data{k}(:,2);  % 2  1 CF-VALU('LP-TOT')	! CF1505   = 1501 + 1504 !fig4   LP debris+ Melt
			%%% plot(x2,y2,lw,1,ms,2,cl,col(3,:),ls,'-');  labels = [labels, 'MELCOR LP Debris and Melt' ];    %aMOLTEN
			x3 = EDF.data{k}(:,1)./Tconst;  
			y3 = EDF.data{k}(:,7);  % 7  6 CF-VALU('COR-TOT-MASS')	! CF1503 ! prawd fig 3 areva
			plot(x3,y3,lw,1,ms,2,cl,col(4,:),ls,'-');  labels = [labels, 'MELCOR Core Total Mass' ]; % aCORE    
		end
		ylabel('Mass, [kg]');
	
run standard_plot2v1.m



% ===================================================================
% FIGURE  - Core mass of material
% ===================================================================
content = {'Mass of Debris' };
run standard_plot1v1.m;
mlcfile  =  'MOLTEN_CORE';  %k = 20;    
k = detectfile(mlcfile,EDF.file);       %MELCOR file
if ~or(sum(k == emptyfile),k==0) 
        x1 = EDF.data{k}(:,1)./Tconst;
        y2 = EDF.data{k}(:,2);  % 2  1 CF-VALU('LP-TOT')	! CF1505   = 1501 + 1504 !fig4   LP debris+ Melt
        y3 = EDF.data{k}(:,3);  % 3  2 CF-VALU('UP-MELT')	! CF1500 ! fig 3    UP-MELT
        y4 = EDF.data{k}(:,4);  % 4  3 CF-VALU('LP-MELT')	! CF1501 
        y5 = EDF.data{k}(:,5);  % 5  4 CF-VALU('TOT-MELT')	! CF1502 
        y6 = EDF.data{k}(:,6);  % 6  5 CF-VALU('LP-DEBRIS')	! CF1504  - problems
        y7 = EDF.data{k}(:,7);  % 7  6 CF-VALU('COR-TOT-MASS')	! CF1503 ! prawd fig 3 areva
        
        %% plot(x1,y2,lw,1,ms,2,cl,cmap6(1,:),ls,'-');  labels = [labels, 'MELCOR LP Debris and Melt' ];    %aMOLTEN
        plot(x1,y3,lw,1,ms,2,cl,cmap6(2,:),ls,'-');  labels = [labels, 'MELCOR UP-MELT' ]; % aCORE    
        plot(x1,y4,lw,1,ms,2,cl,cmap6(3,:),ls,'-');  labels = [labels, 'MELCOR LP-MELT' ]; % aCORE    
        plot(x1,y5,lw,1,ms,2,cl,cmap6(4,:),ls,'-');  labels = [labels, 'MELCOR TOT-MELT' ]; % aCORE    
    % %   plot(x1,y6,lw,1,ms,2,cl,cmap6(5,:),ls,'-');  labels = [labels, 'MELCOR LP-Debris' ]; % aCORE    
        plot(x1,y7,lw,1,ms,2,cl,'k',ls,'-');  labels = [labels, 'MELCOR Core Total Mass' ]; % aCORE    
end
    ylabel('Mass, [kg]');
run standard_plot2v1.m



% ===================================================================
% FIGURE - Massess of materials
% ===================================================================
content = {'Material Mass in Core Region' };
run standard_plot1v1.m;
%k = 23;
mlcfile  ='COREMASS';
k = detectfile(mlcfile,EDF.file);       %MELCOR file
if ~or(sum(k == emptyfile),k==0) 
        x1 = EDF.data{k}(:,1)./Tconst;
        y1 = EDF.data{k}(:,2); 
        y2 = EDF.data{k}(:,3);
        y3 = EDF.data{k}(:,4);
        y4 = EDF.data{k}(:,5);
        y5 = EDF.data{k}(:,6);
        y6 = EDF.data{k}(:,7);
        y7 = EDF.data{k}(:,8);
        y8 = EDF.data{k}(:,9);
        plot(x1,y1,lw,1,ms,2,cl,'k',ls,'-');  labels = [labels, 'Total' ];    %aMOLTEN
        plot(x1,y2,lw,1,ms,2,cl,cmap8(1,:),ls,'-');  labels = [labels, 'UO2' ];    %aMOLTEN
        plot(x1,y3,lw,1,ms,2,cl,cmap8(2,:),ls,'-');  labels = [labels, 'ZR' ];    %aMOLTEN
        plot(x1,y4,lw,1,ms,2,cl,cmap8(3,:),ls,'-');  labels = [labels, 'SS' ];    %aMOLTEN
        plot(x1,y5,lw,1,ms,2,cl,cmap8(4,:),ls,'-');  labels = [labels, 'INC' ];    %aMOLTEN
        plot(x1,y6,lw,1,ms,2,cl,cmap8(5,:),ls,'-');  labels = [labels, 'CRP' ];    %aMOLTEN
        plot(x1,y7,lw,1,ms,2,cl,cmap8(6,:),ls,'-');  labels = [labels, 'ZRO2' ];    %aMOLTEN
        plot(x1,y8,lw,1,ms,2,cl,cmap8(7,:),ls,'-');  labels = [labels, 'SSOX' ];    %aMOLTEN
end
        ylabel('Mass, [kg]');
run standard_plot2v1.m



% ===================================================================
% FIGURE 4
% ===================================================================
%{
content = {'Mass of Debris and Melt in Lower Plenum'};
run standard_plot1v1.m;
mlcfile  = '4_MCMTPS';
maapfile = 'MCMTPS';
k = detectfile(mlcfile,EDF.file);       %MELCOR file
 m1 = detectfile(maapfile,MAAP4.file);     %MAAP4 file
%k = 4;
if(flagMAAP == 1)
    x1 = MAAP4.T;
    y1 = MAAP4.data{m1};;
    plot(x1,y1,lw,1,ms,2,cl,'r',ls,'-');    labels = [labels, 'MAAP4 MCNMTPS' ];
end
if ~or(sum(k == emptyfile),k==0) 
    x2 = EDF.data{k}(:,1)./Tconst;
    y2 = EDF.data{k}(:,2); 
    plot(x2,y2,lw,1,ms,2,cl,col(2,:),ls,'-');  labels = [labels, 'MELCOR CF1504' ];    %aMOLTEN
end
ylabel('Mass, [kg]');
run standard_plot2v1.m

%}
% ===================================================================
% Combined Lower Plenum and Core
% ===================================================================
content = {'Mass of Debris and Melt'};
run standard_plot1v1.m;
maapfile = 'MCMTPS';
m1 = detectfile(maapfile,MAAP4.file);     %MAAP4 file
%k = 4;
	if(flagMAAP == 1)
		x1 = MAAP4.T;
		y1 = MAAP4.data{m1};;
		plot(x1,y1,lw,1,ms,2,cl,'r',ls,'-');    labels = [labels, 'MAAP4 MCNMTPS' ];
	end

maapfile = 'MCR';
m1 = detectfile(maapfile,MAAP4.file);     %MAAP4 file
    if(flagMAAP == 1)
        x1 = MAAP4.T;
        y1 = MAAP4.data{m1};;
        plot(x1,y1,lw,1,ms,2,cl,'r',ls,'--');    labels = [labels, 'MAAP4 MCR' ];
    end
	
	mlcfile  ='MOLTEN_CORE';
	k = detectfile(mlcfile,EDF.file);       %MELCOR file
		if ~or(sum(k == emptyfile),k==0) 
			x2 = EDF.data{k}(:,1)./Tconst;
			y2 = EDF.data{k}(:,2);  % 2  1 CF-VALU('LP-TOT')	! CF1505   = 1501 + 1504 !fig4   LP debris+ Melt  LP+Molten Debris CF1505
			%%% plot(x2,y2,lw,1,ms,2,cl,col(3,:),ls,'-');  labels = [labels, 'MELCOR LP Debris and Melt' ];    %aMOLTEN

			x3 = EDF.data{k}(:,1)./Tconst;  
			y3 = EDF.data{k}(:,7);  % 7  6 CF-VALU('COR-TOT-MASS')	! CF1503 ! prawd fig 3 areva
			plot(x3,y3,lw,1,ms,2,cl,'k',ls,'-');  labels = [labels, 'MELCOR Core Total Mass' ]; % aCORE    
		end
		

	mlcfile  ='COREMASS';
		k = detectfile(mlcfile,EDF.file);       %MELCOR file
		if ~or(sum(k == emptyfile),k==0) 
			x2 = EDF.data{k}(:,1)./Tconst;
			y2 = EDF.data{k}(:,2);  % CF1503 - CORE TOTAL MASS %CF-VALU('COR-TOT-MASS')	! CF1503
			plot(x2,y2,lw,1,ms,2,cl,col(4,:),ls,'-');  labels = [labels, 'MELCOR Core Mass (no LP)' ];    %aMOLTEN	 
		end
		
ylabel('Mass, [kg]');
%xlim([0 10]);
run standard_plot2v1.m



% ===================================================================
%
% ===================================================================
content = {'Core Mass Balance'};
run standard_plot1v1.m;

 %'CORMassLP_ZRO2', 'CORMassLP_ZR', 'CORMassLP_UO2', 'CORMassLP_SSOX', 'CORMassLP_SS', 'CORMassLP_INC', 'CORMassLP_CRP', ...
%			'CORMassLP_TOT' ...
	mlcfile  ='MOLTEN_CORE'; k = detectfile(mlcfile,EDF.file);       %MELCOR file
		if ~or(sum(k == emptyfile),k==0) 
			x3 = EDF.data{k}(:,1)./Tconst;  
			y3 = EDF.data{k}(:,7);  % 7  6 CF-VALU('COR-TOT-MASS')	! CF1503 ! prawd fig 3 areva
			plot(x3,y3,lw,1,ms,2,cl,'k',ls,'-');  labels = [labels, 'MELCOR Core Total Mass' ]; % aCORE    
		end
		
	mlcfile  ='COREMASS'; k = detectfile(mlcfile,EDF.file);       %MELCOR file
		if ~or(sum(k == emptyfile),k==0) 
			x4 = EDF.data{k}(:,1)./Tconst;
			y4 = EDF.data{k}(:,2);  % CF1503 - CORE TOTAL MASS %CF-VALU('COR-TOT-MASS')	! CF1503
			plot(x4,y4,lw,1,ms,2,cl,'m',ls,'-');  labels = [labels, 'MELCOR Core Mass (no LP)' ];    %aMOLTEN	 
		end	
		
    %{
	mlcfile  ='CORMassTOT.apt'; k = detectfile(mlcfile,EDF.file);       %MELCOR file
		if ~or(sum(k == emptyfile),k==0) 
			x5 = EDF.data{k}(:,1)./Tconst;
			y5 = sum((EDF.data{k}(:,2:end)),2);   
			plot(x5,y5,lw,1,ms,2,cl,'m',ls,'--');  labels = [labels, 'MELCOR Core Mass' ];    
		end	
%}		
y10 =[]; y11 =[]; y12 =[]; y13=[]; y14=[]; y15=[]; y16=[]; x111=[];
	mlcfile  ='CORMassLP_ZR.apt'; k = detectfile(mlcfile,EDF.file);       %MELCOR file	
		if ~or(sum(k == emptyfile),k==0)
			x111 = EDF.data{k}(:,1)./Tconst;
			y10 = sum((EDF.data{k}(:,2:end)),2);  
		end
	
	mlcfile  ='CORMassLP_ZRO2.apt'; k = detectfile(mlcfile,EDF.file);       %MELCOR file
		if ~or(sum(k == emptyfile),k==0)
		y11 = sum((EDF.data{k}(:,2:end)),2);  
		end
	mlcfile  ='CORMassLP_SS.apt'; k = detectfile(mlcfile,EDF.file);       %MELCOR file		
		if ~or(sum(k == emptyfile),k==0)
		y12 = sum((EDF.data{k}(:,2:end)),2);  
		end
	mlcfile  ='CORMassLP_SSOX.apt'; k = detectfile(mlcfile,EDF.file);       %MELCOR file		
		if ~or(sum(k == emptyfile),k==0)
		y13 = sum((EDF.data{k}(:,2:end)),2);  
		end
	mlcfile  ='CORMassLP_UO2.apt'; k = detectfile(mlcfile,EDF.file);       %MELCOR file		
		if ~or(sum(k == emptyfile),k==0)
		y14 = sum((EDF.data{k}(:,2:end)),2);   
		end
	mlcfile  ='CORMassLP_INC.apt'; k = detectfile(mlcfile,EDF.file);       %MELCOR file		
		if ~or(sum(k == emptyfile),k==0)
		y15 = sum((EDF.data{k}(:,2:end)),2);    
		end
	mlcfile  ='CORMassLP_CRP.apt'; k = detectfile(mlcfile,EDF.file);       %MELCOR file		
		if ~or(sum(k == emptyfile),k==0)
			y16 = sum((EDF.data{k}(:,2:end)),2);  
		end
	
	
	y111 = [];
	y111 = y10 + y11 + y12+y13+y14+y15+y16;
 	
	plot(x111,y111,lw,1,ms,2,cl,'r',ls,'-');  labels = [labels, 'MELCOR LP Mass' ];    %aMOLTEN	 
	
ylabel('Mass, [kg]');
%xlim([0 10]);
run standard_plot2v1.m




% ===================================================================
%
% ===================================================================
content = {'Ex-Vessel Debris Mass'};
run standard_plot1v1.m;
	mlcfile  ='CAV_MassEnergy'; k = detectfile(mlcfile,EDF.file);       %MELCOR file
		if ~or(sum(k == emptyfile),k==0)
			x0 = EDF.data{k}(:,1)./Tconst;
			y2 = EDF.data{k}(:,2);		%CAV10
			y3 = EDF.data{k}(:,3);		%CAV20
			y4 = EDF.data{k}(:,4);		%CAV30
			
			plot(x0,y2,lw,1,ms,2,cl,colL(1,:),ls,'-');  labels = [labels, 'MELCOR CAV10 Mass' ];    
			plot(x0,y3,lw,1,ms,2,cl,colL(2,:),ls,'-');  labels = [labels, 'MELCOR CAV20 Mass' ];       
			plot(x0,y4,lw,1,ms,2,cl,colL(3,:),ls,'-');  labels = [labels, 'MELCOR CAV30 Mass' ];    
		end

ylabel('Mass, [kg]');
%xlim([0 10]);
run standard_plot2v1.m


% ===================================================================
%
% ===================================================================


















% ===================================================================
end
% ===================================================================


% aCORE k =23
% CF 1000 (TOT-CORE-MASS)
% CF 1006 (UO2MassCore)
% CF 1016 (ZrMassCore)
% CF 1026 (SSMassCore)
% CF 1036 (INCMassCore)
% CF 1046 (CRPMassCore)
% CF 1056 (ZrO2MassCore)
% CF 1066 (SSOXMassCore)


% MOLTEN_CORE
%{
              1 CF-VALU('LP-TOT')	! CF1505   = 1501 + 1504 !fig4 
              2 CF-VALU('UP-MELT')	! CF1500 ! fig 3
              3 CF-VALU('LP-MELT')	! CF1501 
              4 CF-VALU('TOT-MELT')	! CF1502   !TOTAL MOLTEN MASS
              5 CF-VALU('LP-DEBRIS')	! CF1504  !fig 4
	      6 CF-VALU('COR-TOT-MASS')	! CF1503 ! prawd fig 3 areva
		   	      7 COR-TotMass(CRP)
 	      8 COR-TotMass(INC)
 	      9 COR-TotMass(SS)
 	      10 COR-TotMass(SSOX)
 	      11 COR-TotMass(UO2)
 	      12 COR-TotMass(ZR)
 	      13 COR-TotMass(ZRO2)
		  
		  
!               edfnam          mode        filnam
EDF_ID      '4_MCMTPS'         WRITE    '4_MCMTPS'
!       size
EDF_CHN    1 !n           Value Name
              1 CF-VALU('LP-DEBRIS')
!                 ifmt
EDF_FMT   '1P, 8E12.5'
!       size
EDF_DTW    4 !n         twedf        dtwedf
              1       -3500.0          30.0
			  2			  0.0			2.0
			  3		  15000.0		   10.0
              4       80000.0          30.0
			  
			  
			  EDF_ID      'COREMASS'         WRITE    'COREMASS'
!       size
EDF_CHN    8 !n              Value Name
              1  CF-VALU(TOT-CORE-MASS)
              2  CF-VALU('UO2MassCore')
              3   CF-VALU('ZrMassCore')
              4   CF-VALU('SSMassCore')
              5  CF-VALU('INCMassCore')
              6  CF-VALU('CRPMassCore')
              7 CF-VALU('ZrO2MassCore')
              8 CF-VALU('SSOXMassCore')
		  
%}


