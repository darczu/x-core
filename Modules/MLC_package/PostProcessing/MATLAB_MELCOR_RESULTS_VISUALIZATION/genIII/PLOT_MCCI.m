% MCCI PLOTTING SCRIPT
clear; close all; clc;
    folder = ['C:\CloudStation\ROOT\NARSIS-GenIII']
    subfolder =  ['case0002_B10_2021']
%file =   'LOOP/case0002_A4';
file = [folder '\' subfolder]; 
flag33 = 1;
flag22 = 1;

R_RPIT = importdata([file,'/','R_RPIT']);
Z_RPIT = importdata([file,'/','Z_RPIT']);
R_CC = importdata([file,'/','R_COREC']);
Z_CC = importdata([file,'/','Z_COREC']);

time1 = [(1:1:numel(R_RPIT(:,1)))', Z_RPIT(:,1)];
time2 = [(1:1:numel(R_CC(:,1)))', Z_CC(:,1)];
t = R_RPIT(:,1);
t2 = R_CC(:,1);
N = size(t,1);

step0 = 70;
step1 = 130;

% ==== CORE CATCHER ======================================================
figure(500)
text=[];
text2 =[];
%for i=1:100:N
k=1;
colMCCI = hsv(25);
% rand(1,3)
% for i=1480:2:1500   sBO
% i=1400:10:1600
%for i=1:numel(R_CC(:,1)) 

n = 0;
for i=step0:3:step1 
  n=n+1
    hold on; grid on; axis([0 8  -6.5 -5.5 ]);  box on;
  plot(R_CC(i,2:end),-Z_CC(i,2:end),'o-','color',colMCCI(n,:),'LineWidth',0.5)
  
  text2=[text2; t2(i)];
  xlabel('Length, [m]'); ylabel('Length, [m]');

  if(flag33 == 1)  
    M(k) = getframe;   %MOVIE
    k=k+1;
  end
end



ylim([-6.3 -5.6])
Legend = cell(size(text,1),1);
j=1;

for i=step0:3:step1 
   Legend{j}=strcat(num2str(round(text2(j))),' s'); 
   j=j+1;
end
 legend(Legend,'Location','EastOutside')

 line([0 8],[-6.13730E+00 -6.13730E+00],'LineWidth',1,'Color','k','LineStyle','--')
line([0 8],[-6.23731E+00 -6.23731E+00],'LineWidth',1,'Color','k','LineStyle','--')
 
% ====FIG ====
if (flag22 == 1)
    print('-dtiff',['Fig_CC.tiff'])
    saveas(gcf,['Fig_CC'],'fig')
end

% MOVIE  =========================
if(flag33 == 1)
figure(511) 
movie(M,20) 
end
% MOVIE  =========================


% ========================================================================
% ========================================================================
% ========================================================================
% ========================================================================
%% ==== REACTOR PIT ====
step2 = 40;
step3 = 115;
 n= 120;
 colMCCI2 = hsv(23);
figure(501)
text=[];
nn = 0;
%for i=1:numel(R_RPIT(:,1)) 
for i=step2:4:step3 
 nn = nn + 1
  hold on; grid on;   box on;
  plot(R_RPIT(i,2:end),-Z_RPIT(i,2:end),'o-','Color',colMCCI2(nn,:),'LineWidth',0.5)
  
  % plot(R_CC(i,2:end),-Z_CC(i,2:end),'o-','color',colMCCI(n,:),'LineWidth',0.5)
  
  %%plot(R_RPIT(i,[2:21,23:end]),-Z_RPIT(i,[2:21,23:end] ),'o-','color',rand(1,3),'LineWidth',1)
  %plot(R_RPIT(i,2:21),-Z_RPIT(i,2:21),'o-','color',rand(1,3),'LineWidth',1)
  %plot(R_RPIT(i,23:end),-Z_RPIT(i,23:end),'o-','color',rand(1,3),'LineWidth',1)
  text=[text; t(i)];
  xlabel('Length, [m]'); ylabel('Length, [m]');
 ylim([-4.5 0.0])
 
end
Legend = cell(size(text,1),1);
j=1;
%for i=1:numel(R_RPIT(:,1))  
for i=step2:4:step3 
   Legend{j}=strcat(num2str(round(text(j))),' s'); 
   j=j+1;
end
 legend(Legend,'Location','EastOutside')

 
 line([0 5],[ -3.50000E+00   -3.50000E+00   ],'LineWidth',1,'Color','k','LineStyle','--')
line([0 5],[-4.00001E+00 -4.00001E+00],'LineWidth',1,'Color','k','LineStyle','--')
 
% ====FIG ====
if (flag22 == 1)
    %print('-djpeg100','-r200',['Fig_RPIT.jpg'])
    %saveas(gcf,['Fig_RPIT'],'fig')
	   print('-dtiff',['Fig_RPIT.tiff'])
    saveas(gcf,['Fig_RPIT'],'fig')
end


% MOVIE  =========================
if(flag33 == 1)
figure(510) 
movie(M,20) 
end
% MOVIE  =========================
