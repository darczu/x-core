% Funkcja wczytywania 
function [time, a1, a2, a3, a4, a5, a6, a7a, a8, a9, a10, a11, a12, a13, a14, a15, ...
    a16, aTCL, aTFU, aMOLTEN]=  ...
    read2_215(a, file, flag1, flag2, flag3, flagMAAP, flagSNAP, flagH2,  title_flag, flag_all, flag_add, flag_hours, timetime, col,...
    MAAP4_T, MAAP4_Th, MAAP4_1,	MAAP4_2,	MAAP4_3_1,	MAAP4_3_2,	MAAP4_4,	MAAP4_5,	MAAP4_6, ...
    MAAP4_7_1,	MAAP4_7_2,	MAAP4_8_1,	MAAP4_8_2,	MAAP4_9_1,	MAAP4_9_2,	MAAP4_10_1,	MAAP4_10_2, ...
    MAAP4_11_1,	MAAP4_11_2,	MAAP4_12,	MAAP4_13,	MAAP4_14, MAAP4_15)
 
ls = char({'LineStyle'});
cl = char({'Color'});
%col = char(col1);

%%%% Na godziny
MAAP4_T = MAAP4_Th; %zamniaa na godziny
%%%%
tend = timetime; % czas konca obliczen
a1 =    importdata([file '/1_P561']);
%time =  a1(:,1);

Tconst = 1.0;
%%%% na godziny
if(flag_hours == 1 ) 
    Tconst = 3600;
   % time = time./Tconst; %time conversion from sec to hours
end

%%%%
a2 =    importdata([file '/2_MWPS_MWPZ']);
a3 = 0;
a4 =    importdata([file '/4_MCMTPS']);
if(flagSNAP == 0)
    a5 =    importdata([file '/5_MH2CR']);
else
    a5 = 0;
end
a6 =    importdata([file '/6_WH2CR']);
a7a=    importdata([file '/7a_TRICMAX']);  %to poprawic
a8 =    importdata([file '/8_TGPZ_TWPZ']);
a9 =    importdata([file '/9_ZWUS_ZWBS']);
a10=    importdata([file '/10_TGBS_TGUS']); 
a11=    importdata([file '/11_PBS_PUS']);
a12=    importdata([file '/12_PRB']);
a13=    importdata([file '/13_MSTRBTOT']);
a14=    importdata([file '/14_MH2RBTOT']);
a15=    importdata([file '/15_ZWPZ']);
a16=    importdata([file '/16_CORELEV']);
%a1504=    importdata([file '/CF_1504']); %LP Debris mass + molten mass in LP
if(flagSNAP == 0)
aTCL =    importdata([file '/TCL']);
aTFU =    importdata([file '/TFU']);
   aMOLTEN = importdata([file '/MOLTEN_CORE']); %7 - CF-VALU('COR-TOT-MASS')
else
   aMOLTEN =0.0;
   aTCL = 0.0;
   aTFU = 0.0;
end

aCORE = importdata([file '/COREMASS']);  %NIEAKTYWNE DLA 95C_M 
size(aCORE)
%%
%{
CF 1000 (TOT-CORE-MASS)
CF 1006 (UO2MassCore)
CF 1016 (ZrMassCore)
CF 1026 (SSMassCore)
CF 1036 (INCMassCore)
CF 1046 (CRPMassCore)
CF 1056 (ZrO2MassCore)
CF 1066 (SSOXMassCore)
%}
%%
%=====new
%2015.02============================================================
if(flag_all)
    a20 = importdata([file '/20_PSG']);
    a21 = importdata([file '/21_SS_MASS']);
    a22 = importdata([file '/H_LBLOCA']);
    a23 = importdata([file '/H_SLLOCA']);
    a24 = importdata([file '/M_LBLOCA']);
    a25 = importdata([file '/M_SLLOCA']);
end
%================


tfailure = a1(end,1);
tfailureh = tfailure/Tconst; 
tfailureh2= floor(tfailureh);
tfailureh3 = (tfailureh - floor(tfailureh))*60;
disp(['Time to fail [sec] :', num2str(tfailure)])
disp(['Time to fail (or end of the problem) [h/min] :', num2str(tfailureh2),' [h] ', num2str(floor(tfailureh3)), ' [min]'])

%
% Wykresy =========================================
if(flag1 == 1)
   
 
%% Figure 1 PPS

h=figure(1+a)

% ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
 title(file);
elseif(title_flag==2)
 title('Pressurizer Pressure')   
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15 );

% ==
hold on
if(flagMAAP == 1)
    plot(MAAP4_T,MAAP4_1,'-r','LineWidth',2);
end
%plot(I161_1(:,1),I161_1(:,2),'-b','LineWidth',2);
%plot(time,a1(:,2),'-b','LineWidth',2);
plot(a1(:,1)./Tconst,a1(:,2),'LineWidth',2,cl,col(2,:),ls,'-');
 set(0, 'DefaulttextInterpreter', 'none');
box on;
grid on;
xlabel('Time, [s]','Interpreter','none','FontSize',15 );
if( flag_hours == 1)    xlabel('Time, [h]','Interpreter','none','FontSize',15 ); end
if(flagMAAP == 1)
legend('MAAP4','MELCOR','Location','NorthEast','Orientation','vertical');
else
 legend('MELCOR','Location','NorthEast','Orientation','vertical');
end

ylabel('Pressure, [Pa]','Interpreter','none','FontSize',15 );
xlim([0 tend]);
if (flag2 == 1)
    %print('-djpeg100','-r200',[file '/Figures/Fig1.jpg'])
    print('-dtiff','-r300',[file '/Figures/Fig1.tiff'])
    %savefig([file '/Figures/Fig1.fig'])
    saveas(gcf,[file '/Figures/Fig1.fig'],'fig');
end


%% Figure 2 
h=figure(2+a)

% ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
 title(file);
elseif(title_flag==2)
 title('Reactor Coolant System Water Inventory')    
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15 );

hold on
if(flagMAAP == 1)
    plot(MAAP4_T,MAAP4_2,'-r','LineWidth',2);
end
%plot(I161_2(:,1),I161_2(:,2),'-b','LineWidth',2);
plot(a2(:,1)./Tconst,a2(:,2),cl,col(2,:),'LineWidth',2);
 set(0, 'DefaulttextInterpreter', 'none');
 
 xlabel('Time, [s]','Interpreter','none','FontSize',15 );
 if( flag_hours == 1)  xlabel('Time, [h]','Interpreter','none','FontSize',15 );  end
box on;
grid on;



if(flagMAAP == 1)
    legend('MAAP4','MELCOR','Location','NorthEast','Orientation','vertical');
else
    legend('MELCOR','Location','NorthEast','Orientation','vertical');
end
xlim([0 tend]);
ylabel('Water Inventory, [kg]')
if (flag2 == 1)
    %print('-djpeg100','-r200',[file '/Figures/Fig2.jpg'])
    print('-dtiff','-r300',[file '/Figures/Fig2.tiff'])
    saveas(gcf,[file '/Figures/Fig2.fig'],'fig');
end


%% Figure 3
%Molten and Total mass of material in the core
%{       
         2 1 CF-VALU('LP-TOT')	! CF1505   = 1501 + 1504 !fig4   LP debris+ Melt
         3 2 CF-VALU('UP-MELT')	! CF1500 ! fig 3    UP-MELT
         4 3 CF-VALU('LP-MELT')	! CF1501 
         5 4 CF-VALU('TOT-MELT')	! CF1502   !TOTAL MOLTEN MASS
         6 5 CF-VALU('LP-DEBRIS')	! CF1504  !fig 4
	     7 6 CF-VALU('COR-TOT-MASS')	! CF1503 ! prawd fig 3 areva
 	     8 7 COR-TotMass(CRP)
 	     9 8 COR-TotMass(INC)
 	     10 9 COR-TotMass(SS)
 	     11 10 COR-TotMass(SSOX)
 	     12 11 COR-TotMass(UO2)
 	     13 12 COR-TotMass(ZR)
 	     14 13 COR-TotMass(ZRO2) 
%}
h=figure(3+a)


% ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
 title(file);
 elseif(title_flag==2)
     title('Core mass of material');
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15,...
%    'FontName','times');

hold on
if(flagMAAP==1)
    plot(MAAP4_T, MAAP4_3_1,'-r','LineWidth',2);
    %plot(MAAP4_T, MAAP4_3_2,'-g','LineWidth',2);
end
if(flagSNAP==0)
 %plot(aMOLTEN(:,1),aMOLTEN(:,3),'-b','LineWidth',2); %MOLTEN MASS IN UPPER PART 
 %plot(aMOLTEN(:,1),aMOLTEN(:,7),'-m','LineWidth',2);  %TOTAL MASS IN THE CORE
 plot(aMOLTEN(:,1)./Tconst,aMOLTEN(:,7),cl,col(3,:),'LineWidth',2);  %TOTAL MASS IN THE CORE
 %7 - CF-VALU('COR-TOT-MASS')   MOLTEN_CORE(7)  Cf1503 Total mass in AX5-19 R1-5
end
%plot(time, aCORE(:,2),'-k','LineWidth',2); %TOT
plot(aCORE(:,1)./Tconst,aCORE(:,2),cl,col(4,:),'LineWidth',2);
 set(0, 'DefaulttextInterpreter', 'none');

box on;
grid on;
 xlabel('Time, [s]','Interpreter','none','FontSize',15 );
 if( flag_hours == 1)  xlabel('Time, [h]','Interpreter','none','FontSize',15 );  end
if(flagMAAP==1)
    %legend('MAAP4 MCR','MAAP4 MLTCR','MELCOR CF1500','MELCOR CF1503','CORE TOT','Location','NorthEast','Orientation','vertical');
    legend('MAAP4 MCR','MELCOR COR+LP','MELCOR CORE TOTAL','Location','NorthEast','Orientation','vertical');
else
   %legend('MELCOR CF1500','MELCOR CF1503','CORE TOT','Location','NorthEast','Orientation','vertical'); 
   legend('Core MASS TOTAL','Core Mass IA5-19 IR1-5','CORE TOT','Location','NorthEast','Orientation','vertical'); 
end
%CF1000 - aCORE(:,2) CoreMass of all

xlim([0 tend]);
ylim([0 2.5e5]); %dodane
ylabel('Mass, [kg]');
if (flag2 == 1)
    %print('-djpeg100','-r200',[file '/Figures/Fig3.jpg'])
    print('-dtiff','-r300',[file '/Figures/Fig3.tiff'])
    saveas(gcf,[file '/Figures/Fig3.fig'],'fig');
end
hold off

%% Figure 4
h=figure(4+a)

% ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
 title(file);
elseif(title_flag==2)
 title('Mass of Material in Lower Plenum');
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15,...
   % 'FontName','times');

hold on
if(flagMAAP==1)
    plot(MAAP4_T,MAAP4_4,'-r','LineWidth',2);   
end
plot(a4(:,1)./Tconst,a4(:,2),cl,col(2,:),'LineWidth',2);     %1504 - LP-Debris
%plot(time,aMOLTEN(:,2),'--m','LineWidth',2); %LP debris+MELT
 set(0, 'DefaulttextInterpreter', 'none');
%title(file);
box on;
grid on;
 xlabel('Time, [s]','Interpreter','none','FontSize',15 );
 if( flag_hours == 1)  xlabel('Time, [h]','Interpreter','none','FontSize',15 );  end
%legend('MAAP4 MCNMTPS','MELCOR CF1504','MELCOR CF1505','Location','NorthEast','Orientation','vertical');
if(flagMAAP==1)
    legend('MAAP4','MELCOR','Location','NorthEast','Orientation','vertical');
else
    legend('MELCOR CF1504','Location','NorthEast','Orientation','vertical');
end
ylabel('Mass, [kg]')
xlim([0 tend]);
if (flag2 == 1)
    %print('-djpeg100','-r200',[file '/Figures/Fig4.jpg'])
    print('-dtiff','-r300',[file '/Figures/Fig4.tiff'])
    saveas(gcf,[file '/Figures/Fig4.fig'],'fig');
end
hold off

%% Figure 5

if (flagSNAP==0)
h=figure(5+a)

% ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
 title(file);
elseif(title_flag==2)
     title('In-Vessel Hydrogen Production');
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15,...
    %'FontName','times');


hold on
if(flagMAAP == 1)
    plot(MAAP4_T,MAAP4_5,'-r','LineWidth',2);
end
plot(a5(:,1)./Tconst,a5(:,2),cl,col(2,:),'LineWidth',2);
 set(0, 'DefaulttextInterpreter', 'none');

text5 = [num2str(a5(end,2)), ' kg'];
%text(text5);
annotation(figure(5+a),'textbox',[0.15, 0.75, 0.15, 0.05],'String',text5,'LineStyle','none');
box on;
grid on;
 xlabel('Time, [s]','Interpreter','none','FontSize',15 );
 if( flag_hours == 1)  xlabel('Time, [h]','Interpreter','none','FontSize',15 );  end

if(flagMAAP == 1)
    legend('MAAP4','MELCOR','Location','NorthEast','Orientation','vertical');
else
    legend('MELCOR','Location','NorthEast','Orientation','vertical');
end

xlim([0 tend]);
ylim([0 900]); %dodane
ylabel('Mass, [kg]')
if (flag2 == 1)
    %print('-djpeg100','-r200',[file '/Figures/Fig5.jpg'])
     print('-dtiff','-r300',[file '/Figures/Fig5.tiff'])
    saveas(gcf,[file '/Figures/Fig5.fig'],'fig');
end
hold off
end

%% Figure 6
 
h=figure(6+a)
 % ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
 title(file);
elseif(title_flag==2)
 title('In-Vessel Hydrogen Production Rate');
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15,...
    %'FontName','times');
    
    hold on
    if(flagMAAP == 1)
        plot(MAAP4_T,MAAP4_6,'-r','LineWidth',2);
    end
    %plot(time,a6(:,2),'-b','LineWidth',2);
    %plot(a6(:,1)./Tconst,a6(:,2),'-b','LineWidth',2);
    
    if(flagH2 == 1)

        MH2CR = a5;
        timeH2 = MH2CR(1:(end-1),1);
        dydx = diff(MH2CR);
        dwdx = dydx(:,2)./dydx(:,1);
        
        plot(timeH2./Tconst,dwdx,cl,col(2,:),'LineWidth',2)
        
    else
        plot(a6(:,1)./Tconst,a6(:,2),cl,col(2,:),'LineWidth',2);
    
    end
     set(0, 'DefaulttextInterpreter', 'none');
   
    box on;
    grid on;
 xlabel('Time, [s]','Interpreter','none','FontSize',15 );
 if( flag_hours == 1)  xlabel('Time, [h]','Interpreter','none','FontSize',15 );  end
    if(flagMAAP == 1)
        legend('MAAP4','MELCOR','Location','NorthEast','Orientation','vertical');
    else
        legend('MELCOR','Location','NorthEast','Orientation','vertical');
    end
    xlim([0 tend]);
    ylabel('Release Rate, [kg/s]');
    if (flag2 == 1)
        %print('-djpeg100','-r200',[file '/Figures/Fig6.jpg'])
         print('-dtiff','-r300',[file '/Figures/Fig6.tiff'])
        saveas(gcf,[file '/Figures/Fig6.fig'],'fig');
    end
    hold off
end
%% Figure 7
h=figure(7+a)

% ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
 title(file);
 title([file ' only outlet gas - 7A']);
elseif(title_flag==2)
 title('Core Outlet Gas Temperature');
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15,...
    %'FontName','times');

hold on
%plot(MAAP4_T,MAAP4_7_1,'-r','LineWidth',2);  %only gas
if(flagMAAP == 1)
    plot(MAAP4_T,MAAP4_7_2,'-r','LineWidth',2);
end
%plot(time,a7a(:,2),'-b','LineWidth',2);  %<  ==== usuniety CVH345
plot(a7a(:,1)./Tconst,a7a(:,3),cl,col(2,:),'LineWidth',2);
%plot(time,a7a(:,3),'-m','LineWidth',2);
 set(0, 'DefaulttextInterpreter', 'none');

box on;
grid on;
 xlabel('Time, [s]','Interpreter','none','FontSize',15 );
 if( flag_hours == 1)  xlabel('Time, [h]','Interpreter','none','FontSize',15 );  end
%legend('MAAP4 TRICMAX','MELCOR CVH345','MELCOR CVH347','Location','NorthEast','Orientation','vertical');
if(flagMAAP == 1)
    legend('MAAP4 TRICMAX','MELCOR CVH347','Location','NorthEast','Orientation','vertical');
else
    legend('MELCOR CVH347','Location','NorthEast','Orientation','vertical');
end
xlim([0 tend]);
ylabel('Temperature, [K]');
if (flag2 == 1)
    %print('-djpeg100','-r200',[file '/Figures/Fig7.jpg'])
     print('-dtiff','-r300',[file '/Figures/Fig7.tiff'])
    saveas(gcf,[file '/Figures/Fig7.fig'],'fig');
end
hold off
%% Figure 8
h=figure(8+a)

% ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
 title(file);
elseif(title_flag==2)
 title('Water nad Gas Temperature in Pressurizer');
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15,...
    %'FontName','times');

hold on
if(flagMAAP == 1)
    plot(MAAP4_T,MAAP4_8_1,'-r','LineWidth',2);
    plot(MAAP4_T,MAAP4_8_2,'-g','LineWidth',2);
end
plot(a8(:,1)./Tconst,a8(:,2),cl,col(2,:),'LineWidth',2);
plot(a8(:,1)./Tconst,a8(:,3),cl,col(3,:),'LineWidth',2);
 set(0, 'DefaulttextInterpreter', 'none');
%title(file);
box on;
grid on;
 xlabel('Time, [s]','Interpreter','none','FontSize',15 );
 if( flag_hours == 1)  xlabel('Time, [h]','Interpreter','none','FontSize',15 );  end
if(flagMAAP == 1)
    legend('MAAP4 TGPZ','MAAP4 TWPZ','MELCOR TWPZ','MELCOR TGPZ','Location','NorthEast','Orientation','vertical');
else
    legend('MELCOR TWPZ','MELCOR TGPZ','Location','NorthEast','Orientation','vertical');
end
xlim([0 tend]);
ylim([0 1000]); %dodane
ylabel('Temperature, [K]');
if (flag2 == 1)
    %print('-djpeg100','-r200',[file '/Figures/Fig8.jpg'])
     print('-dtiff','-r300',[file '/Figures/Fig8.tiff'])
    saveas(gcf,[file '/Figures/Fig8.fig'],'fig');
end
hold off
%% Figure 9
h=figure(9+a)

% ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
 title(file);
 elseif(title_flag==2)
 title('Steam Generator Water Level');
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15,...
    %'FontName','times');

hold on
if(flagMAAP == 1)
    plot(MAAP4_T,MAAP4_9_1,'-g','LineWidth',2);
    plot(MAAP4_T,MAAP4_9_2,'-r','LineWidth',2);
end
plot(a9(:,1)./Tconst,a9(:,2),cl,col(3,:),'LineWidth',2);
plot(a9(:,1)./Tconst,a9(:,3),cl,col(2,:),'LineWidth',2);
 set(0, 'DefaulttextInterpreter', 'none');
%title(file);
box on;
grid on;
 xlabel('Time, [s]','Interpreter','none','FontSize',15 );
 if( flag_hours == 1)  xlabel('Time, [h]','Interpreter','none','FontSize',15 );  end
if(flagMAAP == 1)
    legend('MAAP4 ZWUS','MAAP4 ZWBS','MELCOR ZWUS','MELCOR ZWBS','Location','NorthEast','Orientation','vertical');
else
    legend('MELCOR ZWUS','MELCOR ZWBS','Location','NorthEast','Orientation','vertical');
end
xlim([0 tend]);
ylim([0 18.0]); %dodane
ylabel('Water Level, [m]');
if (flag2 == 1)
    %print('-djpeg100','-r200',[file '/Figures/Fig9.jpg'])
     print('-dtiff','-r300',[file '/Figures/Fig9.tiff'])
    saveas(gcf,[file '/Figures/Fig9.fig'],'fig');
end
hold off
%% Figure 10
h=figure(10+a)

% ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
 title(file);
 elseif(title_flag==2)
 title('Steam Generator Gas Temperature');
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15,...
    %'FontName','times');

hold on
if(flagMAAP == 1)
    plot(MAAP4_T,MAAP4_10_1,'-r','LineWidth',2);
    plot(MAAP4_T,MAAP4_10_2,'-g','LineWidth',2);
end
plot(a10(:,1)./Tconst,a10(:,2),cl,col(2,:),'LineWidth',2);
plot(a10(:,1)./Tconst,a10(:,3),cl,col(3,:),'LineWidth',2);
 set(0, 'DefaulttextInterpreter', 'none');
%title(file);
box on;
grid on;
 xlabel('Time, [s]','Interpreter','none','FontSize',15 );
 if( flag_hours == 1)  xlabel('Time, [h]','Interpreter','none','FontSize',15 );  end
if(flagMAAP == 1)
    legend('MAAP4 TGUS','MAAP4 TBUS','MELCOR TGUS','MELCOR TBUS','Location','NorthEast','Orientation','vertical');
else
    legend('MELCOR TGUS','MELCOR TBUS','Location','NorthEast','Orientation','vertical');
end
xlim([0 tend]);
ylim([0 640]); %dodane
ylabel('Temperature, [K]');
if (flag2 == 1)
    %print('-djpeg100','-r200',[file '/Figures/Fig10.jpg'])
     print('-dtiff','-r300',[file '/Figures/Fig10.tiff'])
    saveas(gcf,[file '/Figures/Fig10.fig'],'fig');
end
hold off
%% Figure 11
h=figure(11+a)

% ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
 title(file);
 elseif(title_flag==2)
 title('Steam Generator Pressure');
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15,...
    %'FontName','times');

hold on
if(flagMAAP == 1)
plot(MAAP4_T,MAAP4_11_1,'-r','LineWidth',2);
plot(MAAP4_T,MAAP4_11_2,'-g','LineWidth',2);
end
plot(a11(:,1)./Tconst,a11(:,2),cl,col(2,:),'LineWidth',2);
plot(a11(:,1)./Tconst,a11(:,3),cl,col(3,:),'LineWidth',2);
 set(0, 'DefaulttextInterpreter', 'none');
%title(file);
box on;
grid on;
 xlabel('Time, [s]','Interpreter','none','FontSize',15 );
 if( flag_hours == 1)  xlabel('Time, [h]','Interpreter','none','FontSize',15 );  end
if(flagMAAP == 1)
    legend('MAAP4 PUS','MAAP4 PBS','MELCOR PUS','MELCOR PBS','Location','NorthEast','Orientation','vertical');
else
    legend('MELCOR PUS','MELCOR PBS','Location','NorthEast','Orientation','vertical');
end
xlim([0 tend]);
ylim([0 1e7]); %dodane
ylabel('Pressure, [Pa]')
if (flag2 == 1)
    %print('-djpeg100','-r200',[file '/Figures/Fig11.jpg'])
     print('-dtiff','-r300',[file '/Figures/Fig11.tiff'])
    saveas(gcf,[file '/Figures/Fig11.fig'],'fig');
end
hold off
%% Figure 12
h=figure(12+a)

% ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
 title(file);
 elseif(title_flag==2)
 title('Containment Pressure');
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15,...
    %'FontName','times');

hold on
if(flagMAAP == 1)
    plot(MAAP4_T,MAAP4_12,'-r','LineWidth',2);
end
%plot(time,a12(:,2),'-b','LineWidth',2);
plot(a12(:,1)./Tconst, a12(:,2),cl,col(2,:),'LineWidth',2);
 set(0, 'DefaulttextInterpreter', 'none');
%title(file);
box on;
grid on;
ylabel('Pressure, [Pa]');
 xlabel('Time, [s]','Interpreter','none','FontSize',15 );
 if( flag_hours == 1)  xlabel('Time, [h]','Interpreter','none','FontSize',15 );  end
if(flagMAAP == 1)
    legend('MAAP4','MELCOR','Location','NorthEast','Orientation','vertical');
else
    legend('MELCOR','Location','NorthEast','Orientation','vertical');
end
xlim([0 tend]);
ylim([0 5e5]); %dodane
%ylim([0 6e5]); %dodane
if (flag2 == 1)
    %print('-djpeg100','-r200',[file '/Figures/Fig12.jpg'])
     print('-dtiff','-r300',[file '/Figures/Fig12.tiff'])
    saveas(gcf,[file '/Figures/Fig12.fig'],'fig');
end
hold off
%% Figure 13
h=figure(13+a)

% ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
 title(file);
 elseif(title_flag==2)
 title('Steam Mass in Containment');
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15,...
    %'FontName','times');

hold on
if(flagMAAP == 1)
    plot(MAAP4_T,MAAP4_13,'-r','LineWidth',2);
end
plot(a13(:,1)./Tconst,a13(:,2),cl,col(2,:),'LineWidth',2);
 set(0, 'DefaulttextInterpreter', 'none');
%title(file);
box on;
grid on;
 xlabel('Time, [s]','Interpreter','none','FontSize',15 );
 if( flag_hours == 1)  xlabel('Time, [h]','Interpreter','none','FontSize',15 );  end
if(flagMAAP == 1)
    legend('MAAP4','MELCOR','Location','NorthEast','Orientation','vertical');
else
    legend('MELCOR','Location','NorthEast','Orientation','vertical');
end
xlim([0 tend]);
ylim([0 10e4]);

ylabel('Mass, [kg]');
if (flag2 == 1)
    %print('-djpeg100','-r200',[file '/Figures/Fig13.jpg'])
     print('-dtiff','-r300',[file '/Figures/Fig13.tiff'])
    saveas(gcf,[file '/Figures/Fig13.fig'],'fig');
end
hold off
%% Figure 14
h=figure(14+a)

% ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
 title(file);
 elseif(title_flag==2)
 title('Hydrogen Mass in Containment');
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15,...
    %'FontName','times');

hold on
if(flagMAAP == 1)
    plot(MAAP4_T,MAAP4_14,'-r','LineWidth',2);
end
plot(a14(:,1)./Tconst,a14(:,2),cl,col(2,:),'LineWidth',2);
 set(0, 'DefaulttextInterpreter', 'none');
%title(file);
box on;
grid on;
 xlabel('Time, [s]','Interpreter','none','FontSize',15 );
 if( flag_hours == 1)  xlabel('Time, [h]','Interpreter','none','FontSize',15 );  end
if(flagMAAP == 1)
    legend('MAAP4','MELCOR','Location','NorthEast','Orientation','vertical');
else
    legend('MELCOR','Location','NorthEast','Orientation','vertical');
end

xlim([0 tend]);
ylabel('Mass, [kg]');
if (flag2 == 1)
    %print('-djpeg100','-r200',[file '/Figures/Fig14.jpg'])
     print('-dtiff','-r300',[file '/Figures/Fig14.tiff'])
    saveas(gcf,[file '/Figures/Fig14.fig'],'fig');
end
hold off
%% Figure 15
h=figure(15+a)

% ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
 title(file);
 elseif(title_flag==2)
 title('Pressurizer Water Level');
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15,...
    %'FontName','times');

hold on
if(flagMAAP == 1)
    plot(MAAP4_T, MAAP4_15,'-r','LineWidth',2);
end
plot(a15(:,1)./Tconst,a15(:,2),cl,col(2,:),'LineWidth',2);
 set(0, 'DefaulttextInterpreter', 'none');
%title(file);
box on;
grid on;
 xlabel('Time, [s]','Interpreter','none','FontSize',15 );
 if( flag_hours == 1)  xlabel('Time, [h]','Interpreter','none','FontSize',15 );  end
if(flagMAAP == 1)
    legend('MAAP4','MELCOR','Location','NorthEast','Orientation','vertical');
else
    legend('MELCOR','Location','NorthEast','Orientation','vertical');
end
xlim([0 tend]);
ylabel('Water Level, [m]')
if (flag2 == 1)
    %print('-djpeg100','-r200',[file '/Figures/Fig15.jpg'])
     print('-dtiff','-r300',[file '/Figures/Fig15.tiff'])
    saveas(gcf,[file '/Figures/Fig15.fig'],'fig');
end
hold off


%% Figure 16 = 7B CLAD MAX
if (flagSNAP==0)
h=figure(16+a)

% ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
    title([file ' Cladding temperature in core ring 1 top axial level - 7B']);
 title(file);
 elseif(title_flag==2)
 title('Cladding temperature in the top of core ring 1');
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15,...
    %'FontName','times');

hold on
if(flagMAAP == 1)
    plot(MAAP4_T,MAAP4_7_1,'-r','LineWidth',2);  %only gas
end
%plot(MAAP4_T,MAAP4_7_2,'-g','LineWidth',2);
%plot(time,a7a(:,2),'-b','LineWidth',2);
%plot(time,a7a(:,3),'-m','LineWidth',2);
plot(aTCL(:,1)./Tconst,aTCL(:,15),'-c','LineWidth',2); %19
plot(aTCL(:,1)./Tconst,aTCL(:,14),'-b','LineWidth',2); %18
plot(aTCL(:,1)./Tconst,aTCL(:,13),'-m','LineWidth',2); %17
plot(aTCL(:,1)./Tconst,aTCL(:,12),'-k','LineWidth',2); %16
plot(aTCL(:,1)./Tconst,aTCL(:,11),'--y','LineWidth',2); %15
 set(0, 'DefaulttextInterpreter', 'none');

box on;
grid on;
 xlabel('Time, [s]','Interpreter','none','FontSize',15 );
 if( flag_hours == 1)  xlabel('Time, [h]','Interpreter','none','FontSize',15 );  end
ylabel('Temperature, [K]');
if(flagMAAP == 1)
    legend('MAAP4','MELCOR 19','MELCOR 18','MELCOR 17','MELCOR 16','MELCOR 15','Location','NorthEast','Orientation','vertical');
else
    legend('MELCOR 19','MELCOR 18','MELCOR 17','MELCOR 16','MELCOR 15','Location','NorthEast','Orientation','vertical');
end
xlim([0 tend]);
ylim([0 3200]); %dodane
if (flag2 == 1)
    %print('-djpeg100','-r200',[file '/Figures/Fig16.jpg'])
     print('-dtiff','-r300',[file '/Figures/Fig16.tiff'])
    saveas(gcf,[file '/Figures/Fig16.fig'],'fig');
end
hold off
end
%% Figure 17 Fuel Temperature
if(flagSNAP==0)
h=figure(17+a)

% ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
 title(file);
 title([file ' Fuel temperature Ring 1 Levels 15-19']);
 elseif(title_flag==2)
 title('Fuel temperature in the top of core ring 1');
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15,...
    %'FontName','times');

hold on
plot(aTFU(:,1)./Tconst,aTFU(:,15),'-c','LineWidth',2); %19
plot(aTFU(:,1)./Tconst,aTFU(:,14),'-b','LineWidth',2); %18
plot(aTFU(:,1)./Tconst,aTFU(:,13),'-m','LineWidth',2); %17
plot(aTFU(:,1)./Tconst,aTFU(:,12),'-k','LineWidth',2); %16
plot(aTFU(:,1)./Tconst,aTFU(:,11),'--y','LineWidth',2); %15
 set(0, 'DefaulttextInterpreter', 'none');

box on;
grid on;
ylabel('Temperature, [K]');
 xlabel('Time, [s]','Interpreter','none','FontSize',15 );
 if( flag_hours == 1)  xlabel('Time, [h]','Interpreter','none','FontSize',15 );  end
legend('MELCOR 19','MELCOR 18','MELCOR 17','MELCOR 16','MELCOR 15','Location','NorthEast','Orientation','vertical');
xlim([0 tend]);
if (flag2 == 1)
    %print('-djpeg100','-r200',[file '/Figures/Fig17.jpg'])
     print('-dtiff','-r300',[file '/Figures/Fig17.tiff'])
    saveas(gcf,[file '/Figures/Fig17.fig'],'fig');
end
hold off
end
%CORE LEVEL
%% Figure 18 Core Water Level
h=figure(18+a)

% ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
 title(file);
 title([file 'Core Swollen Water Level']);
 elseif(title_flag==2)
    title('Core Water Level Swollen');
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15,...
    %'FontName','times');

hold on
%plot(a16(:,1)./Tconst, a16(:,2),cl,col(2,:),'LineWidth',2); %347 collapsed
plot(a16(:,1)./Tconst, a16(:,3),cl,col(3,:),'LineWidth',2); %347 swollen
 set(0, 'DefaulttextInterpreter', 'none');
line([0 tend],[2.37 2.37 ],'Color','k','Linestyle','--','LineWidth',2); %2.37 
line([0 tend],[6.57 6.57 ],'Color','k','Linestyle','--','LineWidth',2); %6.57

box on; grid on;
ylabel('Water Level, [m]');
 xlabel('Time, [s]','Interpreter','none','FontSize',15 );
 if( flag_hours == 1)  xlabel('Time, [h]','Interpreter','none','FontSize',15 );  end
legend('MELCOR CVH347')
xlim([0 tend]);
ylim([0 7.0]); %dodane
if (flag2 == 1)
    %print('-djpeg100','-r200',[file '/Figures/Fig18.jpg'])
     print('-dtiff','-r300',[file '/Figures/Fig18.tiff'])
    saveas(gcf,[file '/Figures/Fig18.fig'],'fig');
end
hold off

%% Figure 25 Core Water Level Collapsed
h=figure(25+a)

% ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
 title(file);
 title([file 'Core Collapsed Water Level']);
 elseif(title_flag==2)
    title('Core Water Level Collapsed');
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15,...
    %'FontName','times');

hold on
plot(a16(:,1)./Tconst,a16(:,2),cl,col(2,:),'LineWidth',2); %347 collapsed
%plot(a16(:,1)./Tconst, a16(:,3),cl,col(3,:),'LineWidth',2); %347 swollen
 set(0, 'DefaulttextInterpreter', 'none');
line([0 tend],[2.37 2.37 ],'Color','k','Linestyle','--','LineWidth',2); %2.37 
line([0 tend],[6.57 6.57 ],'Color','k','Linestyle','--','LineWidth',2); %6.57

box on; grid on;
ylabel('Water Level, [m]');
 xlabel('Time, [s]','Interpreter','none','FontSize',15 );
 if( flag_hours == 1)  xlabel('Time, [h]','Interpreter','none','FontSize',15 );  end
legend('MELCOR CVH347')
xlim([0 tend]);
ylim([0 7.0]); %dodane
if (flag2 == 1)
    %print('-djpeg100','-r200',[file '/Figures/Fig18.jpg'])
     print('-dtiff','-r300',[file '/Figures/Fig25.tiff'])
    saveas(gcf,[file '/Figures/Fig25.fig'],'fig');
end
hold off

%% CORE MASS above Lower Plenum 
h=figure(19+a)

% ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
 title(file);
 title([file 'Core Materials LVL 5-19']);
 elseif(title_flag==2)
 title('Mass of Core Materials in LVL 5-19')
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15,...
    %'FontName','times');

hold on

plot(aCORE(:,1)./Tconst, aCORE(:,2),'-k','LineWidth',2); %TOT
plot(aCORE(:,1)./Tconst, aCORE(:,3),'-g','LineWidth',2); %UO2
plot(aCORE(:,1)./Tconst, aCORE(:,4),'-r','LineWidth',2); %Ze
plot(aCORE(:,1)./Tconst, aCORE(:,5),'-b','LineWidth',2); %SS
plot(aCORE(:,1)./Tconst, aCORE(:,6),'-y','LineWidth',2); %Inc
plot(aCORE(:,1)./Tconst, aCORE(:,7),'-m','LineWidth',2); %CRP
plot(aCORE(:,1)./Tconst, aCORE(:,8),'-c','LineWidth',2); %ZrO2
plot(aCORE(:,1)./Tconst, aCORE(:,9),'Color',[1, 0.4, 0.6],'LineWidth',2); %SSOX
 set(0, 'DefaulttextInterpreter', 'none');

box on; grid on;
ylabel('Mass, [kg]');
 xlabel('Time, [s]','Interpreter','none','FontSize',15 );
 if( flag_hours == 1)  xlabel('Time, [h]','Interpreter','none','FontSize',15 );  end
legend('Core Total Mass','UO2','Zr','SS','Inconel','Poison','ZrO2','SSOX');
xlim([0 tend]);
if (flag2 == 1)
    %print('-djpeg100','-r200',[file '/Figures/Fig19.jpg'])
     print('-dtiff','-r300',[file '/Figures/Fig19.tiff'])
    saveas(gcf,[file '/Figures/Fig19.fig'],'fig');
end
hold off

%==========================================================================
%LBLOCA additional

if(flag_all == 1)

%% Figure 20 PSS
%{
h=figure(20+a)
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
 title(file);
 elseif(title_flag==2)
  title('Steam Generator Pressure')
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15,%'FontName','times');
hold on
if(flagMAAP == 1) plot(MAAP4_T,MAAP4_1,'-r','LineWidth',2); end
plot(a20(:,1)./Tconst,a20(:,2),'LineWidth',2,cl,col(2,:),ls,'-');
plot(a20(:,1)./Tconst,a20(:,3),'LineWidth',2,cl,col(1,:),ls,'-');
 set(0, 'DefaulttextInterpreter', 'none');
grid on; box on;
xlabel('Time, [s]','Interpreter','none','FontSize',15 );
if(flag_hours == 1) xlabel('Time, [h]','Interpreter','none','FontSize',15 ); end
ylabel('Pressure, [Pa]','Interpreter','none','FontSize',15 );
if(flagMAAP == 1) legend('MAAP4','MELCOR','Location','NorthEast','Orientation','vertical');
else legend('SGx1','SGx3','Location','NorthEast','Orientation','vertical'); end
xlim([0 tend]);
ylim([0 1e7]); %dodane
if (flag2 == 1)
    print('-dtiff','-r300',[file '/Figures/Fig20.tiff'])
    saveas(gcf,[file '/Figures/Fig20.fig'],'fig');
end
%}
%% Figure 21 SG MASS
h=figure(21+a)
set(h,'PaperSize',[20.98404194812 29.67743169791],'InvertHardcopy','off','Color',[1 1 1])
if(title_flag==1)  title(file); 
elseif(title_flag==2)
    title('Secondary Side Water Inventory');
end
hold on
if(flagMAAP == 1) plot(MAAP4_T,MAAP4_1,'-r','LineWidth',2); end
plot(a21(:,1)./Tconst,a21(:,2),'LineWidth',2,cl,col(2,:),ls,'-');
plot(a21(:,1)./Tconst,a21(:,3),'LineWidth',2,cl,col(1,:),ls,'-');
 set(0, 'DefaulttextInterpreter', 'none');
grid on; box on;
xlabel('Time, [s]','Interpreter','none','FontSize',15 );
if(flag_hours == 1) xlabel('Time, [h]','Interpreter','none','FontSize',15 ); end
ylabel('Mass, [kg]','Interpreter','none','FontSize',15 );
if(flagMAAP == 1) legend('MAAP4','MELCOR','Location','NorthEast','Orientation','vertical');
else legend('SGx1','SGx3','Location','NorthEast','Orientation','vertical'); end
xlim([0 tend]);
ylim([0 4e5]); %dodane
if (flag2 == 1)
    print('-dtiff','-r300',[file '/Figures/Fig21.tiff']);
    saveas(gcf,[file '/Figures/Fig21.fig'],'fig');
end


  %% Figure 22 Break Energy flow
        h=figure(22+a)
        set(h,'PaperSize',[20.98404194812 29.67743169791],'InvertHardcopy','off','Color',[1 1 1])
        if(title_flag==1)  title(file); 
        elseif(title_flag==2)
         title('Break Cumulative Energy Flow');
        end
        hold on
        if(flagMAAP == 1) plot(MAAP4_T,MAAP4_1,'-r','LineWidth',2); end
        
        if(flag_add == 1) %2A-LOCA
            plot(a22(:,1)./Tconst,a22(:,2),'LineWidth',2,cl,col(2,:),ls,'-');
            plot(a22(:,1)./Tconst,a22(:,3),'LineWidth',2,cl,col(1,:),ls,'-');
            plot(a22(:,1)./Tconst,a22(:,3)+a22(:,2),'LineWidth',2,cl,'k',ls,'-');
        end
        if(flag_add == 2) %SLLOCA
            plot(a23(:,1)./Tconst,a23(:,2),'LineWidth',2,cl,col(2,:),ls,'-');
            plot(a23(:,1)./Tconst,a23(:,3),'LineWidth',2,cl,col(1,:),ls,'-');   
            plot(a23(:,1)./Tconst,a23(:,3)+a23(:,2),'LineWidth',2,cl,'k',ls,'-');
        end
         set(0, 'DefaulttextInterpreter', 'none');
        grid on; box on;
        xlabel('Time, [s]','Interpreter','none','FontSize',15 );
        if(flag_hours == 1) xlabel('Time, [h]','Interpreter','none','FontSize',15 ); end
        ylabel('Enthalpy, [J]','Interpreter','none','FontSize',15 );
        if(flagMAAP == 1) legend('MAAP4','MELCOR','Location','NorthEast','Orientation','vertical');
        else legend('ATM','POOL','TOTAL','Location','NorthEast','Orientation','vertical'); end
        xlim([0 tend]);
        ylim([0 8e11]);
        if (flag2 == 1)
            print('-dtiff','-r300',[file '/Figures/Fig22.tiff']);
            saveas(gcf,[file '/Figures/Fig22.fig'],'fig');
        end

  %% Figure 23 Break  flow mass water vapour
        h=figure(23+a)
        set(h,'PaperSize',[20.98404194812 29.67743169791],'InvertHardcopy','off','Color',[1 1 1])
        if(title_flag==1)  title(file);
               elseif(title_flag==2)
         title('Break Cumulative Mass Flow');
        end
        hold on
        if(flagMAAP == 1) plot(MAAP4_T,MAAP4_1,'-r','LineWidth',2); end
        
        if(flag_add == 1) %2A-LOCA
            plot(a24(:,1)./Tconst,a24(:,5),'LineWidth',2,cl,col(2,:),ls,'-');
            plot(a24(:,1)./Tconst,a24(:,6),'LineWidth',2,cl,col(1,:),ls,'-');
            plot(a24(:,1)./Tconst,a24(:,5)+a24(:,6),'LineWidth',2,cl,'k',ls,'-');
        end
        if(flag_add == 2) %SL-LOCA
            plot(a25(:,1)./Tconst,a25(:,5),'LineWidth',2,cl,col(2,:),ls,'-');
            plot(a25(:,1)./Tconst,a25(:,6),'LineWidth',2,cl,col(1,:),ls,'-');   
            plot(a25(:,1)./Tconst,a25(:,5)+a25(:,6),'LineWidth',2,cl,'k',ls,'-');
        end
         set(0, 'DefaulttextInterpreter', 'none');
        grid on; box on;
        xlabel('Time, [s]','Interpreter','none','FontSize',15 );
        if(flag_hours == 1) xlabel('Time, [h]','Interpreter','none','FontSize',15 ); end
        ylabel('Mass Flow - Cumulative, [kg]','Interpreter','none','FontSize',15 );
        if(flagMAAP == 1) legend('MAAP4','MELCOR','Location','NorthEast','Orientation','vertical');
        else legend('Water','Water Vapour','TOTAL','Location','NorthEast','Orientation','vertical'); end
        xlim([0 tend]);
        ylim([0 4.5e5]); %dodane
        if (flag2 == 1)
            print('-dtiff','-r300',[file '/Figures/Fig23.tiff']);
            saveas(gcf,[file '/Figures/Fig23.fig'],'fig');
        end
      
        
        
%% Figure 24 Break  flow hydrogen
        h=figure(24+a)
        set(h,'PaperSize',[20.98404194812 29.67743169791],'InvertHardcopy','off','Color',[1 1 1])
        if(title_flag==1)  title(file); 
               elseif(title_flag==2)
         title('Break Cumulative Hydrogen Mass Flow');
        end
        hold on
        if(flagMAAP == 1) plot(MAAP4_T,MAAP4_1,'-r','LineWidth',2); end
        
        if(flag_add == 1) %2A-LOCA
            plot(a24(:,1)./Tconst,a24(:,7),'LineWidth',2,cl,col(3,:),ls,'-');
        end
        if(flag_add == 2) %SL-LOCA  
            plot(a25(:,1)./Tconst,a25(:,7),'LineWidth',2,cl,col(3,:),ls,'-');
        end
         set(0, 'DefaulttextInterpreter', 'none');
        grid on; box on;
        xlabel('Time, [s]','Interpreter','none','FontSize',15 );
        if(flag_hours == 1) xlabel('Time, [h]','Interpreter','none','FontSize',15 ); end
        ylabel('Mass Flow - Cumulative, [kg]','Interpreter','none','FontSize',15 );
        if(flagMAAP == 1) legend('MAAP4','MELCOR','Location','NorthEast','Orientation','vertical');
        else legend('Hydrogen','Location','NorthEast','Orientation','vertical'); end
        xlim([0 tend]);
        if (flag2 == 1)
            print('-dtiff','-r300',[file '/Figures/Fig24.tiff']);
            saveas(gcf,[file '/Figures/Fig24.fig'],'fig');
        end

    
end %<===flag all     

   
%{
 a20 = importdata([file '/20_PSG']);
    a21 = importdata([file '/21_SS_MASS']);
    a22 = importdata([file '/H_LBLOCA']);
    a23 = importdata([file '/H_SLLOCA']);
    a24 = importdata([file '/M_LBLOCA']);
    a25 = importdata([file '/M_SLLOCA']);
%}





end 
%close all

 %KONIEC IF-A flagi rysowania

