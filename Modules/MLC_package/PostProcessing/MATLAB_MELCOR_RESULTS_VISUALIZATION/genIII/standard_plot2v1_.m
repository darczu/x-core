set(0, 'DefaulttextInterpreter', 'none'); 
 l = legend(labels); 
set(l, 'Interpreter','none');
set(l,'FontSize',legend_font_size)



if k_num == numel(cases)
    annotation(gcf,'textbox', [0.02 0.04 0.02 0.02],'String',cases{1},'FitBoxToText','off','EdgeColor','none','Interpreter','none','FontSize',9 );
   % annotation(gcf,'textbox', [0.80 0.04 0.02 0.02],'String',EDF.file{k},'FitBoxToText','off','EdgeColor','none','Interpreter','none','FontSize',9 );
end

%['MAAP4 ' EDF.file{k}] 

if k_num == numel(cases) 
    if (flag2 == 1)
        if flag3 == 0
            %print('-dtiff',[case1,'/', name, code1, '.tiff']);
            print('-dtiff',[cases{1},'/', name,'_', code1, '.tiff']);
            %savefig([case1,'/', name, code1, '.fig']);
            savefig([cases{1},'/', name, '_', code1, '.fig']);
             %savefig([file '/Figures/Fig1.fig'])
             %saveas(gcf,[file '/Figures/Fig1.fig'],'fig');
                %print('-djpeg100','-r200',[file '/Figures/Fig1.jpg'])
        end
        
        if flag3 == 1
            %folder_name = ['COMPARISON_' cases_names{:}];
            folder_name = ['COMPARISON_' cases{:}];
            mkdir(folder_name);

            print('-dtiff',[folder_name,'/', name,'_', code1, '.tiff']);
            savefig([folder_name,'/', name, '_', code1, '.fig']);
        end
    end
end


hold on;

%{
%% Figure 1 PPS

h=figure(1+a)

% ==
set(h,'PaperSize',[20.98404194812 29.67743169791],...
    'InvertHardcopy','off',...
    'Color',[1 1 1])
if(title_flag==1) 
 title(file);
elseif(title_flag==2)
 title('Pressurizer Pressure')   
end
%axes('Parent',h,'XTick',[0 1 2 3 4 5 6 7 8],'FontSize',15 );

% ==
hold on
if(flagMAAP == 1)
    plot(MAAP4_T,MAAP4_1,'-r','LineWidth',2);
end

plot(a1(:,1)./Tconst,a1(:,2),'LineWidth',2,cl,col(2,:),ls,'-');
 set(0, 'DefaulttextInterpreter', 'none');
box on;
grid on;
xlabel('Time, [s]','Interpreter','none','FontSize',15 );
if( flag_hours == 1)    xlabel('Time, [h]','Interpreter','none','FontSize',15 ); end
if(flagMAAP == 1)
legend('MAAP4','MELCOR','Location','NorthEast','Orientation','vertical');
else
 legend('MELCOR','Location','NorthEast','Orientation','vertical');
end

ylabel('Pressure, [Pa]','Interpreter','none','FontSize',15 );
xlim([0 tend]);
if (flag2 == 1)
    %print('-djpeg100','-r200',[file '/Figures/Fig1.jpg'])
    print('-dtiff','-r300',[file '/Figures/Fig1.tiff'])
    %savefig([file '/Figures/Fig1.fig'])
    saveas(gcf,[file '/Figures/Fig1.fig'],'fig');
end
%}

