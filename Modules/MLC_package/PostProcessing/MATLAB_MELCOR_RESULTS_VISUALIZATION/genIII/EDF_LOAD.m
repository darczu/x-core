   % IMPORT EDF FILES
   %{
   EDF_H2 = importdata([case1 '/EDF_H2']);
   EDF_COND = importdata([case1 '/EDF_COND']);
   EDF_POWER = importdata([case1 '/EDF_POWER']);
   EDF_PRESS = importdata([case1 '/EDF_PRESS']);
   EDF_RHUM = importdata([case1 '/EDF_RHUM']);
   EDF_TVAP = importdata([case1 '/EDF_TVAP']);
   EDF_TEMP_HS = importdata([case1 '/EDF_TEMP_HS']);
   EDF_TEMP_CL = importdata([case1 '/EDF_TEMP_CL']);
   EDF_TEMP_FU = importdata([case1 '/EDF_TEMP_FU']);
   %EDF_TEMP_SH = importdata([case1 '/EDF_TEMP_SH']);
   EDF_CONT_MASS = importdata([case1 '/EDF_CONT_MASS']);  % NEw nodes
   EDF_TEMP_SH = importdata([case1 '/EDF_TEMP_SH2']);  % NEw nodes
   if flagRN==1
       EDF_CONT_DEP    =  importdata([case1 '/EDF_CONT_DEP']);
       EDF_CONT_PARAMS =  importdata([case1 '/EDF_CONT_PARAMS']);
       EDF_RELEASE     =  importdata([case1 '/EDF_RELEASE']);
   end
   if flagATMG==1
       EDF_ATMG_400 = importdata([case1 '/EDF_ATMG_400']);
       EDF_TYCLAIR = importdata([case1 '/EDF_RN1_TYCLAIR-14-1_10']);
   end
   %}
% ===================================================================
   file = case1;
% ===================================================================
% ===================================================================
% For CSV files add extension
% EDF.file = {'1_P561', '2_MWPS_MWPZ', '3_MCR', '4_MCMTPS', '5_MH2CR', ...                      %1-5
%            '6_WH2CR', '7a_TRICMAX' , '8_TGPZ_TWPZ', '9_ZWUS_ZWBS', '10_TGBS_TGUS', ...      %6- 10
%              '11_PBS_PUS', '12_PRB', '13_MSTRBTOT', '14_MH2RBTOT', '15_ZWPZ', ...           %11-15
%              '16_CORELEV', '20_CAV_GAS','21_SS_MASS',   'CF_1504',  'MOLTEN_CORE', ...          %16-20
%            'TCL', 'TFU', 'COREMASS',  'H_LBLOCA' ,'H_SLLOCA',...                            %21-25
%             'M_LBLOCA', 'M_SLLOCA'  ... %                                %26-27
% 			'SG_WATERMASS', ...
% 			'TCL2', 'CAV_MassEnergy', 'CAV_Temp', 'IRWST_CC', ...
%            ...  %'SGx3_SSMass_CF706.csv', 'SGx1_SSMassCF705.csv', ...
% 		    'CORMassLP_ZRO2.apt', 'CORMassLP_ZR.apt', 'CORMassLP_UO2.apt', 'CORMassLP_SSOX.apt', 'CORMassLP_SS.apt', 'CORMassLP_INC.apt', 'CORMassLP_CRP.apt', ...
% 			'CORMassTOT.apt' ...
% 			} ;
			
			
EDF.file = {'1_P561', '2_MWPS_MWPZ',  '4_MCMTPS', '5_MH2CR', ...                      
           '6_WH2CR', '7_TRICMAX' , '8_TGPZ_TWPZ', '9_ZWUS_ZWBS', '10_TGBS_TGUS', ... 
             '11_PBS_PUS', '12_PRB', '13_MSTRBTOT', '14_MH2RBTOT', '15_ZWPZ', ...     
             '16_CORELEV', 'MOLTEN_CORE', ... 
             '20_CAV_GAS',...          
           'TCL', 'TFU', 'COREMASS', ...
           'TCL2', 'CAV_MassEnergy', 'CAV_Temp', 'IRWST_CC', ... 
 		    'CORMassLP_ZRO2.apt', 'CORMassLP_ZR.apt', 'CORMassLP_UO2.apt', 'CORMassLP_SSOX.apt', 'CORMassLP_SS.apt', 'CORMassLP_INC.apt', 'CORMassLP_CRP.apt', ...
 			'CORMassTOT.apt',  ...
            'SG_WATERMASS', ...
            '20_CAV_GAS_2', 'CC_TMAX' ...
			} ;			


% ADD EDF in front of data            
%EDF.file = strcat('EDF_',EDF.file)

		
EDF.num = 1:1:numel(EDF.file);
EDF.units = {};
emptyfile = [];

% Check if file exists - if not make empty
for i=1:numel(EDF.file)
    filename = [file,'/',EDF.file{i}];
    if  isfile(fullfile(filename))
		[fPath,fName,fExt] = fileparts(filename);
		if strcmp(lower(fExt),'.csv')		%Allow to use CSV files - try to not use it. Better use function mergeCSV and script MERGER_CSV
            
			%EDF.data{i} = tmp.data;
            if flag_unique == 1
                EDF.data{i} = data_unique(tmp.data);
            else
                tmp = importdata(filename,',',2);    
            end
		elseif strcmp(lower(fExt),'.apt')		%APLT PLOT ASCII FILES
			tmp = importdata(filename,' ',3);
            
            if flag_unique == 1
                EDF.data{i} = data_unique(tmp.data);	
            else
                EDF.data{i} = tmp.data;	   
            end
			
        else
            
            
%			EDF.data{i} = importdata(filename);

            if flag_unique == 1
                tmp = importdata(filename);
                EDF.data{i} = data_unique(tmp);
            else            
                 EDF.data{i} = importdata(filename);
            end
                
		end
    else
        %EDF.data{i} = zeros(10,10);
        
        EDF.data{i} = [];
        emptyfile = [emptyfile, i];
    end
end


% ===================================================================
if flagMCCI == 1
    EDF.fileMCCI = {'R_COREC' 'Z_COREC' 'R_RPIT' 'Z_RPIT'}
end
% ===================================================================

% ===================================================================
if flagSS == 1
    EDF.fileSS = {'S_S_PCS' 'S_S_SCS'};
end
% ===================================================================

% CORE MASS - 23
% CF 1000 (TOT-CORE-MASS)
% CF 1006 (UO2MassCore)
% CF 1016 (ZrMassCore)
% CF 1026 (SSMassCore)
% CF 1036 (INCMassCore)
% CF 1046 (CRPMassCore)
% CF 1056 (ZrO2MassCore)
% CF 1066 (SSOXMassCore)

% MOLTEN_CORE  - 20
%2  1 CF-VALU('LP-TOT')	! CF1505   = 1501 + 1504 !fig4   LP debris+ Melt
%3  2 CF-VALU('UP-MELT')	! CF1500 ! fig 3    UP-MELT
%4  3 CF-VALU('LP-MELT')	! CF1501 
%5  4 CF-VALU('TOT-MELT')	! CF1502   !TOTAL MOLTEN MASS
%6  5 CF-VALU('LP-DEBRIS')	! CF1504  !fig 4
%7  6 CF-VALU('COR-TOT-MASS')	! CF1503 ! prawd fig 3 areva
 % 8 7 COR-TotMass(CRP)
 % 9 8 COR-TotMass(INC)
 % 10 9 COR-TotMass(SS)
 % 11 10 COR-TotMass(SSOX)
 % 12 11 COR-TotMass(UO2)
 % 13 12 COR-TotMass(ZR)
 % 14 13 COR-TotMass(ZRO2) 


%{
    a1 =    importdata([file '/1_P561']);
    a2 =    importdata([file '/2_MWPS_MWPZ']);
    a3 =    0;
    a4 =    importdata([file '/4_MCMTPS']);
    if(flagSNAP == 0)
        a5 =    importdata([file '/5_MH2CR']);
    else
        a5 = 0;
    end
    a6 =    importdata([file '/6_WH2CR']);
    a7a=    importdata([file '/7a_TRICMAX']);  %to poprawic
    a8 =    importdata([file '/8_TGPZ_TWPZ']);
    a9 =    importdata([file '/9_ZWUS_ZWBS']);
    a10=    importdata([file '/10_TGBS_TGUS']); 
    a11=    importdata([file '/11_PBS_PUS']);
    a12=    importdata([file '/12_PRB']);
    a13=    importdata([file '/13_MSTRBTOT']);
    a14=    importdata([file '/14_MH2RBTOT']);
    a15=    importdata([file '/15_ZWPZ']);
    a16=    importdata([file '/16_CORELEV']);
    %a1504=    importdata([file '/CF_1504']); %LP Debris mass + molten mass in LP
    if(flagSNAP == 0)
        aTCL =    importdata([file '/TCL']);
        aTFU =    importdata([file '/TFU']);
        aMOLTEN = importdata([file '/MOLTEN_CORE']); %7 - CF-VALU('COR-TOT-MASS')
        aCORE = importdata([file '/COREMASS']);  %NIEAKTYWNE DLA 95C_M 
    else
       aMOLTEN =0.0;
       aTCL = 0.0;
       aTFU = 0.0;
       aCORE = [];
    end
%}
% ===================================================================
%{
CF 1000 (TOT-CORE-MASS)
CF 1006 (UO2MassCore)
CF 1016 (ZrMassCore)
CF 1026 (SSMassCore)
CF 1036 (INCMassCore)
CF 1046 (CRPMassCore)
CF 1056 (ZrO2MassCore)
CF 1066 (SSOXMassCore)
%}
%{
if(flag_all)
    a20 = importdata([file '/20_PSG']);
    a21 = importdata([file '/21_SS_MASS']);
    a22 = importdata([file '/H_LBLOCA']);
    a23 = importdata([file '/H_SLLOCA']);
    a24 = importdata([file '/M_LBLOCA']);
    a25 = importdata([file '/M_SLLOCA']);
end
%}



% ===================================================================
% ===================================================================
