%  PLOT_RPV_TH
if(flag1 == 1) %PLOT OR NOT

    %ls = char({'LineStyle'}); 
    %cl = char({'Color'}); %col = char(col1);
    %ms = char({'MarkerSize'});
    %lw = char({'LineWidth'});
    %legend('Location','northeast');
% ===================================================================
% FIGURE 1 - Pressure
% ===================================================================
    content = {'Pressurizer Pressure'};
    run standard_plot1v1.m;
    mlcfile  = '1_P561';               maapfile = 'PPS';
    k = detectfile(mlcfile,EDF.file);  %MELCOR
    m1 = detectfile(maapfile,MAAP4.file);     %MAAP4 file

    if(flagMAAP == 1)
        x1 = MAAP4.T;
        y1 = MAAP4.data{m1};
        plot(x1,y1,lw,1,ms,2,cl,'r',ls,'-');    labels = [labels, 'MAAP4' ];
    end
    if ~sum(k == emptyfile) 
        x2 = EDF.data{k}(:,1)./Tconst;
        y2 = EDF.data{k}(:,2);
        plot(x2,y2,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR' ];
    end   
        ylabel('Pressure, [Pa]');
    run standard_plot2v1.m
% ===================================================================
% FIGURE 2 - Pressure PZR + SG
% ===================================================================
    content = {'Pressurizer and SG Pressures'};
    run standard_plot1v1.m;
    mlcfile  = '1_P561';               maapfile = 'PPS';
    k = detectfile(mlcfile,EDF.file);  %MELCOR
    m1 = detectfile(maapfile,MAAP4.file);     %MAAP4 file

    if(flagMAAP == 1)
        x1 = MAAP4.T;
        y1 = MAAP4.data{m1};
        plot(x1,y1,lw,1,ms,2,cl,'r',ls,'-');    labels = [labels, 'MAAP4' ];
    end
    if ~sum(k == emptyfile) 
        x2 = EDF.data{k}(:,1)./Tconst;
        y2 = EDF.data{k}(:,2);
        plot(x2,y2,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR PZR' ];
    end   
	
	mlcfile  = '11_PBS_PUS'; 
maapfile1 = 'PBS'; maapfile2 = 'PUS';
	k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
m1 = detectfile(maapfile1,MAAP4.file);     %MAAP4 file
m2 = detectfile(maapfile2,MAAP4.file);     %MAAP4 file


if(flagMAAP == 1)
    x1 = MAAP4.T;
    y1 = MAAP4.data{m1};
    y11 = MAAP4.data{m2};
    plot(x1,y1,lw,1,ms,2,cl,'r',ls,'-');    labels = [labels, 'MAAP4 PBS' ];
    plot(x1,y11,lw,1,ms,2,cl,'g',ls,'-');    labels = [labels, 'MAAP4 PUS' ];
end
if ~sum(k == emptyfile) 
    x2 = EDF.data{k}(:,1)./Tconst;
    y2 = EDF.data{k}(:,2);
    y3 = EDF.data{k}(:,3);
    plot(x2,y2,lw,1,ms,2,cl,'r',ls,'-');  labels = [labels, 'MELCOR SGx1' ];
    plot(x2,y3,lw,1,ms,2,cl,'m',ls,'-');  labels = [labels, 'MELCOR SGx3' ];
end    

        ylabel('Pressure, [Pa]');
	
run standard_plot2v1.m
   
% ===================================================================
% FIGURE 2 - Water Inventory
% ===================================================================
content = {'Water Inventory' };
run standard_plot1v1.m;
mlcfile  = '2_MWPS_MWPZ';
maapfile = 'MWPS+MWPZ';
k = detectfile(mlcfile,EDF.file);       %MELCOR file
 m1 = detectfile(maapfile,MAAP4.file);     %MAAP4 file

if(flagMAAP == 1)
    x1 = MAAP4.T;
    y1 = MAAP4.data{m1};;
    plot(x1,y1,lw,1,ms,2,cl,'r',ls,'-');    labels = [labels, 'MAAP4' ];
end
if ~sum(k == emptyfile) 
    x2 = EDF.data{k}(:,1)./Tconst;
    y2 = EDF.data{k}(:,2);
    plot(x2,y2,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR' ];
end
 mlcfile = 'SG_WATERMASS'; 
 k = detectfile(mlcfile,EDF.file);       %MELCOR file
if ~sum(k == emptyfile) 
    x3 = EDF.data{k}(:,1)./Tconst;
    y3 = EDF.data{k}(:,2);
    plot(x3,y3,lw,1,ms,2,cl,'r',ls,'-');  labels = [labels, 'MELCOR SGx1' ];
	
    x4 = EDF.data{k}(:,1)./Tconst;
    y4 = EDF.data{k}(:,3);
    plot(x4,y4,lw,1,ms,2,cl,'m',ls,'-');  labels = [labels, 'MELCOR SGx3' ];
end

 
 
    ylabel('Mass, [kg]');
run standard_plot2v1.m
% ===================================================================
% FIGURE 5 
% ===================================================================
content = {'In-Vessel Hydrogen Production'};
run standard_plot1v1.m;
mlcfile  = '5_MH2CR';
maapfile = 'MH2CR';
k = detectfile(mlcfile,EDF.file);       %MELCOR file
 m1 = detectfile(maapfile,MAAP4.file);     %MAAP4 file

    %k  = 5;
    if(flagMAAP == 1)
        x1 = MAAP4.T;
        y1 = MAAP4.data{m1};;
        plot(x1,y1,lw,1,ms,2,cl,'r',ls,'-');    labels = [labels, 'MAAP4' ];
    end
    if ~sum(k == emptyfile) 
        x2 = EDF.data{k}(:,1)./Tconst;
        y2 = EDF.data{k}(:,2);
        plot(x2,y2,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR' ];
    end
    ylabel('Mass, [kg]');
run standard_plot2v1.m


% ===================================================================
% FIGURE 6
% ===================================================================
content = {'In-Vessel Hydrogen Production Rate'};
run standard_plot1v1.m;
mlcfile  = '6_WH2CR';
maapfile = 'WH2CR';
k = detectfile(mlcfile,EDF.file);       %MELCOR file   %k = 6;
 m1 = detectfile(maapfile,MAAP4.file);     %MAAP4 file
  
    if(flagMAAP == 1)
        x1 = MAAP4.T;
        y1 = MAAP4.data{m1};;
        plot(x1,y1,lw,1,ms,2,cl,'r',ls,'-');    labels = [labels, 'MAAP4' ];
    end
    if ~sum(k == emptyfile) 
        x2 = EDF.data{k}(:,1)./Tconst;
        y2 = abs(EDF.data{k}(:,2));
        plot(x2,y2,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR' ];
    end    
    ylabel('Release Rate, [kg/s]');
run standard_plot2v1.m

% ===================================================================
% FIGURE 7
% ===================================================================
content = {'Core Outlet Gas Temperature'};
run standard_plot1v1.m;
mlcfile  ='7a_TRICMAX';
maapfile = 'TRICMAX';
k = detectfile(mlcfile,EDF.file);       %MELCOR file  %k = 7;
 m1 = detectfile(maapfile,MAAP4.file);     %MAAP4 file

if(flagMAAP == 1)
    x1 = MAAP4.T;
    y1 = MAAP4.data{m1};; %TRICMAX
    %y1 = MAAP4.data{k}{1}; %TCLADMAX
    plot(x1,y1,lw,1,ms,2,cl,'r',ls,'-');    labels = [labels, 'MAAP4 TRICMAX' ];
end
if ~sum(k == emptyfile) 
    x2 = EDF.data{k}(:,1)./Tconst;
    y2 = EDF.data{k}(:,2);
    y3 = EDF.data{k}(:,3);
    plot(x2,y2,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR UP' ];
    plot(x2,y3,lw,1,ms,2,cl,col(2,:),ls,'-');  labels = [labels, 'MELCOR Core' ];
end
ylabel('Temperature, [K]');
run standard_plot2v1.m

% ===================================================================
% FIGURE 8
% ===================================================================
content = {'Water nad Gas Temperature in Pressurizer'};
run standard_plot1v1.m;
mlcfile  ='8_TGPZ_TWPZ'; 
maapfile1 = 'TGPZ'; maapfile2 = 'TWPZ';

k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
m1 = detectfile(maapfile1,MAAP4.file);     %MAAP4 file
m2 = detectfile(maapfile2,MAAP4.file);     %MAAP4 file

if(flagMAAP == 1)
    x1 = MAAP4.T;
    y1 = MAAP4.data{m1};
    y11 = MAAP4.data{m2};
    plot(x1,y1,lw,1,ms,2,cl,'r',ls,'-');    labels = [labels, 'MAAP4 TGPZ' ];
    plot(x1,y11,lw,1,ms,2,cl,'g',ls,'-');    labels = [labels, 'MAAP4 TWPZ' ];
end
if ~sum(k == emptyfile) 
    x2 = EDF.data{k}(:,1)./Tconst;
    y2 = EDF.data{k}(:,2);
    y3 = EDF.data{k}(:,3);
    plot(x2,y2,lw,1,ms,2,cl,col(2,:),ls,'-');  labels = [labels, 'MELCOR TGPZ' ];
    plot(x2,y3,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR TWPZ' ];
end

ylabel('Temperature, [K]');
run standard_plot2v1.m

% ===================================================================
% FIGURE 9
% ===================================================================
content = {'Steam Generator Water Level'};
run standard_plot1v1.m;
%k = 9;
mlcfile  = '9_ZWUS_ZWBS'; 
maapfile1 = 'ZWUS'; maapfile2 = 'ZWBS';

k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
m1 = detectfile(maapfile1,MAAP4.file);     %MAAP4 file
m2 = detectfile(maapfile2,MAAP4.file);     %MAAP4 file

if(flagMAAP == 1)
    x1 = MAAP4.T;
    y1 = MAAP4.data{m1};
    y11 = MAAP4.data{m2};
    plot(x1,y1,lw,1,ms,2,cl,'r',ls,'-');    labels = [labels, 'MAAP4 ZWUS' ];
    plot(x1,y11,lw,1,ms,2,cl,'g',ls,'-');    labels = [labels, 'MAAP4 ZWBS' ];
end
if ~sum(k == emptyfile) 
    x2 = EDF.data{k}(:,1)./Tconst;
    y2 = EDF.data{k}(:,2);  %
    y3 = EDF.data{k}(:,3);  % 
    plot(x2,y2,lw,1,ms,2,cl,col(2,:),ls,'-');  labels = [labels, 'MELCOR TGPZ' ];
    plot(x2,y3,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR TWPZ' ];
end
ylabel('Water Level, [m]');
run standard_plot2v1.m
% ===================================================================
% FIGURE 10
% ===================================================================
content = {'Steam Generator Gas Temperature'};
run standard_plot1v1.m;
%k = 10;
mlcfile  = '10_TGBS_TGUS'; 
maapfile1 = 'TGBS'; maapfile2 = 'TGUS';

k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
m1 = detectfile(maapfile1,MAAP4.file);     %MAAP4 file
m2 = detectfile(maapfile2,MAAP4.file);     %MAAP4 file


if(flagMAAP == 1)
    x1 = MAAP4.T;
    y1 = MAAP4.data{m1};
    y11 = MAAP4.data{m2};
    plot(x1,y1,lw,1,ms,2,cl,'r',ls,'-');    labels = [labels, 'MAAP4 TGUS' ];
    plot(x1,y11,lw,1,ms,2,cl,'g',ls,'-');    labels = [labels, 'MAAP4 TBUS' ];
end
if ~sum(k == emptyfile) 
    x2 = EDF.data{k}(:,1)./Tconst;
    y2 = EDF.data{k}(:,2);
    y3 = EDF.data{k}(:,3);
    plot(x2,y2,lw,1,ms,2,cl,col(2,:),ls,'-');  labels = [labels, 'MELCOR TGUS' ];
    plot(x2,y3,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR TBUS' ];
end
ylabel('Temperature, [K]');

run standard_plot2v1.m
% ===================================================================
% FIGURE 11
% ===================================================================
content = {'Steam Generator Pressure'};
run standard_plot1v1.m;
%k = 11;
mlcfile  = '11_PBS_PUS'; 
maapfile1 = 'PBS'; maapfile2 = 'PUS';
k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
m1 = detectfile(maapfile1,MAAP4.file);     %MAAP4 file
m2 = detectfile(maapfile2,MAAP4.file);     %MAAP4 file

if(flagMAAP == 1)
    x1 = MAAP4.T;
    y1 = MAAP4.data{m1};
    y11 = MAAP4.data{m2};
    plot(x1,y1,lw,1,ms,2,cl,'r',ls,'-');    labels = [labels, 'MAAP4 PBS' ];
    plot(x1,y11,lw,1,ms,2,cl,'g',ls,'-');    labels = [labels, 'MAAP4 PUS' ];
end
if ~sum(k == emptyfile) 
    x2 = EDF.data{k}(:,1)./Tconst;
    y2 = EDF.data{k}(:,2);
    y3 = EDF.data{k}(:,3);
    plot(x2,y2,lw,1,ms,2,cl,col(2,:),ls,'-');  labels = [labels, 'MELCOR PBS' ];
    plot(x2,y3,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR PUS' ];
end    
ylabel('Pressure, [Pa]');
run standard_plot2v1.m

% ===================================================================
% FIGURE 12
% ===================================================================
content = {'Containment Pressure'};
run standard_plot1v1.m;
%k = 12;
mlcfile  = '12_PRB'; 
maapfile1 = 'PRB'; 
k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
m1 = detectfile(maapfile1,MAAP4.file);     %MAAP4 file

if(flagMAAP == 1)
    x1 = MAAP4.T;
    y1 = MAAP4.data{m1};
    plot(x1,y1,lw,1,ms,2,cl,'r',ls,'-');    labels = [labels, 'MAAP4' ];
end
if ~sum(k == emptyfile) 
    x2 = EDF.data{k}(:,1)./Tconst;
    y2 = EDF.data{k}(:,2);
    plot(x2,y2,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR' ];
end
ylabel('Pressure, [Pa]');
ylim([0 inf]);
run standard_plot2v1.m

% ===================================================================
% FIGURE 13
% ===================================================================
content = {'Steam Mass in Containment'};
run standard_plot1v1.m;
%k = 13;
mlcfile  = '13_MSTRBTOT'; 
maapfile1 = 'MSTRBTOT'; 
k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
m1 = detectfile(maapfile1,MAAP4.file);     %MAAP4 file
if(flagMAAP == 1)
    x1 = MAAP4.T;
    y1 = MAAP4.data{m1};
    plot(x1,y1,lw,1,ms,2,cl,'r',ls,'-');    labels = [labels, 'MAAP4' ];
end
if ~sum(k == emptyfile) 
    x2 = EDF.data{k}(:,1)./Tconst;
    y2 = EDF.data{k}(:,2);
    plot(x2,y2,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR' ];
end
ylabel('Mass, [kg]');
run standard_plot2v1.m

% ===================================================================
% FIGURE 14
% ===================================================================
content = {'Hydrogen Mass in Containment'};
run standard_plot1v1.m;
mlcfile  = '14_MH2RBTOT'; 
maapfile1 = 'MH2RBTOT'; 
k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
m1 = detectfile(maapfile1,MAAP4.file);     %MAAP4 file
%k = 14;
if(flagMAAP == 1)
    x1 = MAAP4.T;
    y1 = MAAP4.data{m1};
    plot(x1,y1,lw,1,ms,2,cl,'r',ls,'-');    labels = [labels, 'MAAP4' ];
end
if ~sum(k == emptyfile) 
    x2 = EDF.data{k}(:,1)./Tconst;
    y2 = EDF.data{k}(:,2);
    plot(x2,y2,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR' ];
end
ylabel('Mass, [kg]');
run standard_plot2v1.m

% ===================================================================
% FIGURE 15
% ===================================================================
content = {'Pressurizer Water Level'};
run standard_plot1v1.m;
mlcfile  = '15_ZWPZ'; 
maapfile1 = 'ZWPZ'; 
k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
m1 = detectfile(maapfile1,MAAP4.file);     %MAAP4 file

if(flagMAAP == 1)
    x1 = MAAP4.T;
    y1 = MAAP4.data{m1};
    plot(x1,y1,lw,1,ms,2,cl,'r',ls,'-');    labels = [labels, 'MAAP4' ];
end
if ~sum(k == emptyfile) 
    x2 = EDF.data{k}(:,1)./Tconst;
    y2 = EDF.data{k}(:,2);
    plot(x2,y2,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR' ];
end
ylabel('Water Level, [m]');
run standard_plot2v1.m


% ===================================================================
% FIGURE 16 Core Water Level
% ===================================================================
content = {'Core Water Level'};
run standard_plot1v1.m;

mlcfile  = '16_CORELEV'; 
k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;

if ~sum(k == emptyfile) 
    x2 = EDF.data{k}(:,1)./Tconst;
    y2 = EDF.data{k}(:,3);
    y3 = EDF.data{k}(:,2);

    % CVH 347
    plot(x2,y2,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR Swollen'];
    plot(x2,y3,lw,1,ms,2,cl,col(2,:),ls,'-');  labels = [labels, 'MELCOR Collapsed' ];
    line([0 plottime],[2.37 2.37 ],'Color','k','Linestyle','--','LineWidth',2); %2.37 
    line([0 plottime],[6.57 6.57 ],'Color','k','Linestyle','--','LineWidth',2); %6.57

    ylim([0 8.0]); %dodane
end
ylabel('Water Level, [m]');
run standard_plot2v1.m


% ===================================================================
% FIGURE  Cladding Temperatures
% ===================================================================
content = {'Cladding temperature'};
%content = {'Cladding temperature in core ring 1 top axial level - 7B'};
run standard_plot1v1.m;
%k = 21;
mlcfile  = 'TCL2'; 
maapfile1 = 'TCLADMAX'; 
k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
m1 = detectfile(maapfile1,MAAP4.file);     %MAAP4 file

cmapTCL = hsv(size(EDF.data{k},2));

if(flagMAAP == 1)
    x1 = MAAP4.T;
    y1 = MAAP4.data{m1};
    plot(x1,y1,lw,1,ms,2,cl,'r',ls,'-');    labels = [labels, 'MAAP4 TCLADMAX' ];
end
if ~sum(k == emptyfile) 
    x2 = EDF.data{k}(:,1)./Tconst;

    for j=1:(size(EDF.data{k},2)-1)
        y{j} = EDF.data{k}(:,j+1);
        plot(x2,y{j},lw,1,ms,2,cl,cmapTCL(j,:),ls,'-');  %labels = [labels, ['COR-TCL' ] ];
    end
end

TCLlab =  {'COR-TCL_106' 'COR-TCL_109' 'COR-TCL_112' 'COR-TCL_115' 'COR-TCL_118'...
'COR-TCL_206' 'COR-TCL_209' 'COR-TCL_212' 'COR-TCL_215' 'COR-TCL_218'...
'COR-TCL_306' 'COR-TCL_309' 'COR-TCL_312' 'COR-TCL_315' 'COR-TCL_318'...
'COR-TCL_406' 'COR-TCL_409' 'COR-TCL_412' 'COR-TCL_415' 'COR-TCL_418'...
'COR-TCL_506' 'COR-TCL_509' 'COR-TCL_512' 'COR-TCL_515' 'COR-TCL_518'}

labels = [labels, TCLlab];
legend_font_size = 7;


ylim([0 3200]);    
ylabel('Water Level, [m]');
run standard_plot2v1.m

% ===================================================================
% FIGURE  Fuel Temperature
% ===================================================================
%{
content = {'Fuel temperature'};
%content = {'Cladding temperature in core ring 1 top axial level - 7B'};
run standard_plot1v1.m;
%k = 22;
mlcfile  = 'TFU'; 
k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
if ~sum(k == emptyfile) 
    x2 = EDF.data{k}(:,1)./Tconst;
    y2 = EDF.data{k}(:,2);

    plot(x2,y2,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'MELCOR' ];
end

ylabel('Water Level, [m]');
run standard_plot2v1.m
%}

% ===================================================================
% FIGURE  Cavity Gas Production
% ===================================================================

content = {'Cavity gas production'};
run standard_plot1v1.m;
mlcfile  = '20_CAV_GAS'; 
k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;

if ~sum(k == emptyfile) 
    % {'CAVH2_10.csv' 'CAVH2_20.csv' 'CAVH2_30.csv' 'CAVCO_10.csv' 'CAVCO_20.csv' 'CAVCO_30.csv'}
	x2 = EDF.data{k}(:,1)./Tconst;
    y2 = EDF.data{k}(:,2);
    y3 = EDF.data{k}(:,3);
    y4 = EDF.data{k}(:,4);
    y5 = EDF.data{k}(:,5);
    y6 = EDF.data{k}(:,6);
    y7 = EDF.data{k}(:,7);
	
	y8 = y2+y3+y4;
	y9 = y5+y6+y7;
	
	
    plot(x2,y2,lw,1,ms,2,cl,cmap6(1,:),ls,'-');  labels = [labels, 'H2 RPit' ];
    plot(x2,y3,lw,1,ms,2,cl,cmap6(2,:),ls,'-');  labels = [labels, 'H2 CC' ];
    plot(x2,y4,lw,1,ms,2,cl,cmap6(3,:),ls,'-');  labels = [labels, 'H2 CC2' ];
    plot(x2,y5,lw,1,ms,2,cl,cmap6(4,:),ls,'-');  labels = [labels, 'CO RPit' ];
    plot(x2,y6,lw,1,ms,2,cl,cmap6(5,:),ls,'-');  labels = [labels, 'CO CC' ];
    plot(x2,y7,lw,1,ms,2,cl,cmap6(6,:),ls,'-');  labels = [labels, 'CO CC2' ];
	
	plot(x2,y8,lw,1,ms,2,cl,'k',ls,'-');  labels = [labels, 'Total H2' ];
	plot(x2,y9,lw,1,ms,2,cl,'k',ls,'--');  labels = [labels, 'Total CO' ];

    ylabel('Mass, [kg]');
    run standard_plot2v1.m
end


% ===================================================================
% FIGURE  Cavity Gas Production
% ===================================================================
%{
content = {'Cavity gas production'};
run standard_plot1v1.m;
mlcfile  = '20_CAV_GAS'; 
k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;

if ~sum(k == emptyfile) 
    x2 = EDF.data{k}(:,1)./Tconst;
    y2 = EDF.data{k}(:,2);
    y3 = EDF.data{k}(:,3);
    y4 = EDF.data{k}(:,4);
    y5 = EDF.data{k}(:,5);
    y6 = EDF.data{k}(:,6);
    y7 = EDF.data{k}(:,7);


    plot(x2,y2,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'H2 Reactor Pit' ];
    plot(x2,y3,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'H2 Core Catcher' ];
    plot(x2,y4,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'H2 Core Catcher 2' ];
    plot(x2,y5,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'CO Reactor Pit' ];
    plot(x2,y6,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'CO Core Catcher' ];
    plot(x2,y7,lw,1,ms,2,cl,col(1,:),ls,'-');  labels = [labels, 'CO Core Catcher 2' ];
end


ylabel('Mass, [kg]');
run standard_plot2v1.m

%}

% ===================================================================
% FIGURE 
% ===================================================================
content = {'Combustible gases'};

run standard_plot1v1.m;
mlcfile  = '5_MH2CR';
maapfile = 'MH2CR';
k = detectfile(mlcfile,EDF.file);       %MELCOR file
 m1 = detectfile(maapfile,MAAP4.file);     %MAAP4 file

    %k  = 5;
    if(flagMAAP == 1)
        x1 = MAAP4.T;
        y1 = MAAP4.data{m1};;
        plot(x1,y1,lw,1,ms,2,cl,'r',ls,'-');    labels = [labels, 'MAAP4' ];
    end
    if ~sum(k == emptyfile) 
        x2 = EDF.data{k}(:,1)./Tconst;
        y2 = EDF.data{k}(:,2);
        plot(x2,y2,lw,1,ms,2,cl,'b',ls,'-');  labels = [labels, 'H2 In-Vessel Production' ];
    end
    
mlcfile  = '14_MH2RBTOT'; 
maapfile1 = 'MH2RBTOT'; 
k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;
m1 = detectfile(maapfile1,MAAP4.file);     %MAAP4 file

if(flagMAAP == 1)
    x1 = MAAP4.T;
    y1 = MAAP4.data{m1};
    plot(x1,y1,lw,1,ms,2,cl,'r',ls,'-');    labels = [labels, 'MAAP4' ];
end
if ~sum(k == emptyfile) 
    x2 = EDF.data{k}(:,1)./Tconst;
    y2 = EDF.data{k}(:,2);
    plot(x2,y2,lw,1,ms,2,cl,'m',ls,'-');  labels = [labels, 'H2 Containment Atm.' ];
end

mlcfile  = '20_CAV_GAS'; 
k  = detectfile(mlcfile,EDF.file);       %MELCOR file k = 8;

if ~sum(k == emptyfile) 

    x2 = EDF.data{k}(:,1)./Tconst;
    y2 = EDF.data{k}(:,2);
    y3 = EDF.data{k}(:,3);
    y4 = EDF.data{k}(:,4);
    y5 = EDF.data{k}(:,5);
    y6 = EDF.data{k}(:,6);
    y7 = EDF.data{k}(:,7);


    plot(x2,y2,lw,1,ms,2,cl,colL(1,:),ls,'-');  labels = [labels, 'H2 Reactor Pit' ];
    plot(x2,y3,lw,1,ms,2,cl,colL(2,:),ls,'-');  labels = [labels, 'H2 Core Catcher' ];
    plot(x2,y4,lw,1,ms,2,cl,colL(3,:),ls,'-');  labels = [labels, 'H2 Core Catcher 2' ];
    plot(x2,y5,lw,1,ms,2,cl,colL(1,:),ls,'--');  labels = [labels, 'CO Reactor Pit' ];
    plot(x2,y6,lw,1,ms,2,cl,colL(2,:),ls,'--');  labels = [labels, 'CO Core Catcher' ];
    plot(x2,y7,lw,1,ms,2,cl,colL(3,:),ls,'--');  labels = [labels, 'CO Core Catcher 2' ];

end
ylabel('Mass, [kg]');
run standard_plot2v1.m

% ===================================================================
% FIGURE 
% ===================================================================
content = {'IRWST and Core Catcher'};
run standard_plot1v1.m;
mlcfile  = 'IRWST_CC';
k = detectfile(mlcfile,EDF.file);       %MELCOR file

if ~sum(k == emptyfile) 
x2 = EDF.data{k}(:,1)./Tconst;	  %time
y2 = EDF.data{k}(:,2);			%SR mass
y3 = EDF.data{k}(:,3);			%SR temp
y4 = EDF.data{k}(:,4);			%SR sat temp
y5 = EDF.data{k}(:,5);			%IRWST mass 
y6 = EDF.data{k}(:,6);			%IRWST temp
y7 = EDF.data{k}(:,7);			%IRWST sat	

	yyaxis left;
	   plot(x2,y2,lw,1,ms,2,cl,colL(1,:),ls,'-');  labels = [labels, 'SR Water Mass' ];
	   plot(x2,y5,lw,1,ms,2,cl,colL(2,:),ls,'-');  labels = [labels, 'IRWST Water Mass' ];
	ylabel('Mass, [kg]');    
		
	yyaxis right;
		plot(x2,y3,lw,1,ms,2,cl,colL(3,:),ls,'-');  labels = [labels, 'SR Water Temp.' ];
		plot(x2,y4,lw,1,ms,2,cl,colL(4,:),ls,':');  labels = [labels, 'SR Water Sat. Temp.' ];
		plot(x2,y6,lw,1,ms,2,cl,colL(5,:),ls,'-');  labels = [labels, 'IRWST Water Temp.' ];
		plot(x2,y7,lw,1,ms,2,cl,colL(6,:),ls,'--');  labels = [labels, 'IRWST Sat. Temp.' ];
	ylabel('Temperature, [K]');    	
end		
			
			
%{			
	mlcfile  = 'CAV_Temp';
	k = detectfile(mlcfile,EDF.file);       %MELCOR file
if ~sum(k == emptyfile) 
	x2 = EDF.data{k}(:,1)./Tconst;
	
	%for size(EDF.data)
	%sum(EDF.data{k}(:,18)) > 0
	
	y8 = EDF.data{k}(:,19);  %LMX non-zero
	plot(x2,y8,lw,1,ms,1,cl,'r',ls,'-');  labels = [labels, 'Corium Temperature' ];
	
	
end	
			
line([0 x2(end)],[1420 1420],'LineWidth',1,'Color','k','LineStyle','--');  labels = [labels, 'Plate Melting T' ];
	%}		
run standard_plot2v1.m

%% ===================================================================
% FIGURE 
% ===================================================================

content = {'Corium Temperatures'};
run standard_plot1v1.m;
mlcfile  = 'CAV_Temp';
k = detectfile(mlcfile,EDF.file);       %MELCOR file

if ~sum(k == emptyfile) 
	x2 = EDF.data{k}(:,1)./Tconst;
		
	y7 = EDF.data{k}(:,4);  %TSURF 30
	y8 = EDF.data{k}(:,19);  %LMX non-zero
	plot(x2,y8,lw,1,ms,1,cl,'r',ls,'-');  labels = [labels, 'Corium Temperature' ];
	%plot(x2,y7,lw,1,ms,1,cl,'b',ls,'-');  labels = [labels, 'Corium Temperature' ];
	
end	

line([0 x2(end)],[1420 1420],'LineWidth',1,'Color','k','LineStyle','--');  labels = [labels, 'Plate Melting T' ];
	ylabel('Temperature, [K]');   		
run standard_plot2v1.m

%% ===================================================================
% FIGURE 
% ===================================================================




% ===================================================================
% FIGURE 
% ===================================================================



end
% flag1 ===============================================================