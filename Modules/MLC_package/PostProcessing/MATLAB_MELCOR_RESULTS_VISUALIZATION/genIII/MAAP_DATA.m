%MAAP read
% LOAD MAAP RESULTS
fileMAAP = 'MAAP4.xlsx';

band = '3603'; %zakres dla pliku MAAP4 z fazy in-vessel Total Loss of Ac Power
if flagMAAPfull == 1
    band = '8590';
    fileMAAP = 'MAAP4_FULL.xlsx';
end

% ===================================================================
tfailh = 6 + (50/60); tfail = (tfailh)*3600; 
disp(['MAAP Vessel fails at 6 [h] 50 [min], it is: ', num2str(tfail), ' [s]']); 

MAAP4.file = {};


if flagMAAP==1
   % MAAP4_T    = xlsread(fileMAAP ,'Arkusz1',['B5:B' band]); %Time in sec
   % MAAP4_Th    = xlsread(fileMAAP ,'Arkusz1',['A5:A' band]); %Time in hours

    
   MAAP4.file = {'PPS' 'MWPS+MWPZ' 'MCR' 'MLTCR' 'MCMTPS' 'MH2CR' 'WH2CR'...
                 'TCLADMAX' 'TRICMAX' 'TGPZ' 'TWPZ' 'ZWBS' 'ZWUS' 'TGUS' 'TGBS' 'PBS' 'PUS' ...
                 'PRB' 'MSTRBTOT' 'MH2RBTOT' 'ZWPZ' };
   
%    EDF.file = {'1_P561', '2_MWPS_MWPZ', '3_MCR', '4_MCMTPS', '5_MH2CR', ...                      %1-5
%    '6_WH2CR', '7a_TRICMAX' , '8_TGPZ_TWPZ', '9_ZWUS_ZWBS', '10_TGBS_TGUS', ...      %6- 10
%      '11_PBS_PUS', '12_PRB', '13_MSTRBTOT', '14_MH2RBTOT', '15_ZWPZ', ...           %11-15
%      '16_CORELEV', '20_PSG','21_SS_MASS',   'CF_1504',  'MOLTEN_CORE', ...          %16-20
%    'TCL', 'TFU', 'COREMASS',  'H_LBLOCA' ,'H_SLLOCA',...                            %21-25
%     'M_LBLOCA', 'M_SLLOCA'};                                %26-27
      
      
      


    MAAP4.T    = xlsread(fileMAAP ,'Arkusz1',['B5:B' band]); %Time in sec
    MAAP4.Th    = xlsread(fileMAAP ,'Arkusz1',['A5:A' band]); %Time in hours
    MAAP4.data{1} = xlsread(fileMAAP ,'Arkusz1',['D5:D' band]);  %PPS
    MAAP4.data{2} = xlsread(fileMAAP ,'Arkusz1',['E5:E' band]);  %MWPS+MWPZ
    MAAP4.data{3} = xlsread(fileMAAP ,'Arkusz1',['F5:F' band]);  %MCR
    MAAP4.data{4} = xlsread(fileMAAP ,'Arkusz1',['G5:G' band]); %MLTCR
    MAAP4.data{5} = xlsread(fileMAAP ,'Arkusz1',['H5:H' band]);  %MCMTPS
    MAAP4.data{6} = xlsread(fileMAAP ,'Arkusz1',['I5:I' band]);  %MH2CR
    MAAP4.data{7} =   xlsread(fileMAAP ,'Arkusz1',['J5:J' band]);  %WH2CR
    MAAP4.data{8} = xlsread(fileMAAP ,'Arkusz1',['K5:K' band]);  %TCLADMAX
    MAAP4.data{9} = xlsread(fileMAAP ,'Arkusz1',['L5:L' band]);  %TRICMAX ?? napewno ?
    MAAP4.data{10} = xlsread(fileMAAP ,'Arkusz1',['M5:M' band]);  %TGPZ
    MAAP4.data{11} = xlsread(fileMAAP ,'Arkusz1',['N5:N' band]);  %TWPZ
    MAAP4.data{12} = xlsread(fileMAAP ,'Arkusz1',['O5:O' band]);  %ZWBS
    MAAP4.data{13} = xlsread(fileMAAP ,'Arkusz1',['P5:P' band]);  %ZWUS
    MAAP4.data{14} = xlsread(fileMAAP ,'Arkusz1',['Q5:Q' band]);  %TGUS
    MAAP4.data{15} = xlsread(fileMAAP ,'Arkusz1',['R5:R' band]);  %TGBS
    MAAP4.data{16} = xlsread(fileMAAP ,'Arkusz1',['S5:S' band]);  %PBS
    MAAP4.data{17} = xlsread(fileMAAP ,'Arkusz1',['T5:T' band]);  %PUS
    MAAP4.data{18} =   xlsread(fileMAAP ,'Arkusz1',['U5:U' band]);  %PRB
    MAAP4.data{19} =   xlsread(fileMAAP ,'Arkusz1',['V5:V' band]);  %MSTRBTOT
    MAAP4.data{20} =   xlsread(fileMAAP ,'Arkusz1',['W5:W' band]);  %MH2RBTOT
    MAAP4.data{21} =   xlsread(fileMAAP ,'Arkusz1',['Y5:Y' band]);  %ZWPZ
 
else
    MAAP4.T    = []; %Time in sec
    MAAP4.Th    = [];
    MAAP4.data{1}  =[];
    MAAP4.data{2}  =[];
    MAAP4.data{3}  =[];
    MAAP4.data{4}  =[];
    MAAP4.data{5}  =[];
    MAAP4.data{6}  =[];
    MAAP4.data{7}  =[];
    MAAP4.data{8}  =[];
    MAAP4.data{9}  =[];
    MAAP4.data{10} =[];
    MAAP4.data{11} =[];
    MAAP4.data{12} =[];
    MAAP4.data{13} =[];
    MAAP4.data{14} =[];
    MAAP4.data{15} =[];
    MAAP4.data{16} =[];
    MAAP4.data{17} =[];
    MAAP4.data{18} =[];
    MAAP4.data{19} =[];
    MAAP4.data{20} =[];
    MAAP4.data{21} =[];
end


if(flag_hours == 1 ) 
    MAAP4.T = MAAP4.Th; %zamniaa na godziny
else
    %MAAP4_T =; %zamniaa na godziny
end


%{

MAAP4.T    = xlsread(fileMAAP ,'Arkusz1',['B5:B' band]); %Time in sec
MAAP4.Th    = xlsread(fileMAAP ,'Arkusz1',['A5:A' band]); %Time in hours
MAAP4.data{1} = xlsread(fileMAAP ,'Arkusz1',['D5:D' band]);  %PPS
MAAP4.data{2} = xlsread(fileMAAP ,'Arkusz1',['E5:E' band]);  %MWPS+MWPZ
MAAP4.data{3}{1} = xlsread(fileMAAP ,'Arkusz1',['F5:F' band]);  %MCR
MAAP4.data{3}{2} = xlsread(fileMAAP ,'Arkusz1',['G5:G' band]); %MLTCR
MAAP4.data{4} = xlsread(fileMAAP ,'Arkusz1',['H5:H' band]);  %MCMTPS
MAAP4.data{5} = xlsread(fileMAAP ,'Arkusz1',['I5:I' band]);  %MH2CR
MAAP4.data{6} =   xlsread(fileMAAP ,'Arkusz1',['J5:J' band]);  %WH2CR
MAAP4.data{7}{1} = xlsread(fileMAAP ,'Arkusz1',['K5:K' band]);  %TCLADMAX
MAAP4.data{7}{2} = xlsread(fileMAAP ,'Arkusz1',['L5:L' band]);  %TRICMAX ?? napewno ?
MAAP4.data{8}{1} = xlsread(fileMAAP ,'Arkusz1',['M5:M' band]);  %TGPZ
MAAP4.data{8}{2} = xlsread(fileMAAP ,'Arkusz1',['N5:N' band]);  %TWPZ
MAAP4.data{9}{1} = xlsread(fileMAAP ,'Arkusz1',['O5:O' band]);  %ZWBS
MAAP4.data{9}{2} = xlsread(fileMAAP ,'Arkusz1',['P5:P' band]);  %ZWUS
MAAP4.data{10}{1} = xlsread(fileMAAP ,'Arkusz1',['Q5:Q' band]);  %TGUS
MAAP4.data{10}{2} = xlsread(fileMAAP ,'Arkusz1',['R5:R' band]);  %TGBS
MAAP4.data{11}{1} = xlsread(fileMAAP ,'Arkusz1',['S5:S' band]);  %PBS
MAAP4.data{11}{2} = xlsread(fileMAAP ,'Arkusz1',['T5:T' band]);  %PUS
MAAP4.data{12} =   xlsread(fileMAAP ,'Arkusz1',['U5:U' band]);  %PRB
MAAP4.data{13} =   xlsread(fileMAAP ,'Arkusz1',['V5:V' band]);  %MSTRBTOT
MAAP4.data{14} =   xlsread(fileMAAP ,'Arkusz1',['W5:W' band]);  %MH2RBTOT
MAAP4.data{15} =   xlsread(fileMAAP ,'Arkusz1',['Y5:Y' band]);  %ZWPZ


  MAAP4.T    = []; %Time in sec
    MAAP4.Th    = [];
    MAAP4.data{1} = [];
    MAAP4.data{2} = [];
    MAAP4.data{3}{1} = [];
    MAAP4.data{3}{2} = [];
    MAAP4.data{4} =   [];
    MAAP4.data{5} =   [];
    MAAP4.data{6} =   [];
    MAAP4.data{7}{1} = [];
    MAAP4.data{7}{2} = [];
    MAAP4.data{8}{1} = [];
    MAAP4.data{8}{2} = [];
    MAAP4.data{9}{1} = [];
    MAAP4.data{9}{2} = [];
    MAAP4.data{10}{1} = [];
    MAAP4.data{10}{2} = [];
    MAAP4.data{11}{1} = [];
    MAAP4.data{11}{2} = [];
    MAAP4.data{12} =   [];
    MAAP4.data{13} =   [];
    MAAP4.data{14} =   [];
    MAAP4.data{15} =   [];
%}