
%{
function [hours, mins, secs] = sec2hms(t)
    hours = floor(t ./ 3600);
    t = t - hours .* 3600;
    mins = floor(t ./ 60);
    secs = t - mins .* 60;
end
%}
%If you also want to have it formatted, use a printf:

function [hours, mins, hms] = sec2hms(t)
    hours = floor(t./3600);
    t = t - hours.*3600;
    mins = floor(t./60);
    %secs = t - mins .* 60;
    %hms = sprintf('%02d:%02d:%05.2f\n', hours, mins, secs);
    %hms=zeros(1,size(hours,1))';
    hms=[];
    for i=1:size(hours,1)
      %hms(i) = sprintf('%02d h %02d min\n', hours(i), mins(i));
      hms = [hms; sprintf('%02d h %02d min', hours(i), mins(i)) ]
      %hms
    end
    hms
end

%{
sec2hms(69.9904)
ans =
    00:01:09.99
%}