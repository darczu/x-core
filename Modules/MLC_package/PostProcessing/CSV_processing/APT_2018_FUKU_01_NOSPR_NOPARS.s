# #mazgaj 08 AUG 2018
#-------- -- ._ --.. --. .- .--- --------
# FUKU calculations for PhD 2018
# Code: MELCOR
#-------- -- ._ --.. --. .- .--- --------
#
#
MELCOR 0 ptf "C:\Users\admin2\Documents\ROOT\MELCOR\CASE21\VTT_1_NOSPR_NOPARS\MC_Step\MC_Step.ptf"
#
# rkt pow only when one point kinetics is active
# RREAD 0 "rktpow-0"
#RELAP 0 EXPORT CSV "REACT_POW.csv"
#
#-------- -- ._ --.. --. .- .--- --------
#
#
## core mass flow

# # core power

## Containment
  ## Drywell - DW
    ## MREAD 0 "CVH-MASS.6_102"
    ## MELCOR 0 EXPORT CSV "C:\Users\admin2\Documents\ROOT\MELCOR\CASE21\CSV_DATA_EXTRACT\VTT_1_NOSPR_NOPAR\DW_H2_MASS.csv" 
	 
	## MREAD 0 "CVH-MASS.5_102"
    ##MELCOR 0 EXPORT CSV "DW_O2_MASS.csv" 

     MREAD 0 "CVH-X.6_102"
     MELCOR 0 EXPORT CSV "C:\Users\admin2\Documents\ROOT\MELCOR\CASE21\CSV_DATA_EXTRACT\VTT_1_NOSPR_NOPARS\DW_H2_FRAC.csv" 
	 
	 MREAD 0 "CVH-X.5_102"
     MELCOR 0 EXPORT CSV "C:\Users\admin2\Documents\ROOT\MELCOR\CASE21\CSV_DATA_EXTRACT\VTT_1_NOSPR_NOPARS\DW_O2_FRAC.csv" 
	 
	 MREAD 0 "CVH-P_102"
     MELCOR 0 EXPORT CSV "C:\Users\admin2\Documents\ROOT\MELCOR\CASE21\CSV_DATA_EXTRACT\VTT_1_NOSPR_NOPARS\CVH-P_102.csv" 
	 
	  MREAD 0 "SPR-FL_1"
     MELCOR 0 EXPORT CSV "C:\Users\admin2\Documents\ROOT\MELCOR\CASE21\CSV_DATA_EXTRACT\VTT_1_NOSPR_NOPARS\SPR_FL_1.csv" 
	 
	 MREAD 0 "SPR-FL_2"
     MELCOR 0 EXPORT CSV "C:\Users\admin2\Documents\ROOT\MELCOR\CASE21\CSV_DATA_EXTRACT\VTT_1_NOSPR_NOPARS\SPR_FL_2.csv"
	 
	 MREAD 0 "ESF-PAR-DMH2_1"
     MELCOR 0 EXPORT CSV "C:\Users\admin2\Documents\ROOT\MELCOR\CASE21\CSV_DATA_EXTRACT\VTT_1_NOSPR_NOPARS\PAR_RATE.csv"
	 
	 MREAD 0 "TIME"
     MELCOR 0 EXPORT CSV "C:\Users\admin2\Documents\ROOT\MELCOR\CASE21\CSV_DATA_EXTRACT\VTT_1_NOSPR_NOPARS\TIME.csv"

  ## Wetwell - WW

      MREAD 0 "CVH-X.6_103"
      MELCOR 0 EXPORT CSV "C:\Users\admin2\Documents\ROOT\MELCOR\CASE21\CSV_DATA_EXTRACT\VTT_1_NOSPR_NOPARS\WW_H2_FRAC_NOSPR_NOPARS.csv" 
	  
	   MREAD 0 "CVH-P.6_103"
      MELCOR 0 EXPORT CSV "C:\Users\admin2\Documents\ROOT\MELCOR\CASE21\CSV_DATA_EXTRACT\VTT_1_NOSPR_NOPARS\WW_P_NOSPR_NOPARS.csv" 
	 
	 ## MREAD 0 "CVH-MASS.5_103"
   ##   MELCOR 0 EXPORT CSV "WW_O2_MASS.csv" 

  ##    MREAD 0 "CVH-X.6_103"
  ##    MELCOR 0 EXPORT CSV "WW_H2_FRAC.csv" 
	 
 ##	 MREAD 0 "CVH-X.5_103"
  ##    MELCOR 0 EXPORT CSV "WW_O2_FRAC.csv" 
	 
	  ## PEDESTAL
	 
 ##	    MREAD 0 "CVH-MASS.6_100"
 ##     MELCOR 0 EXPORT CSV "PD_H2_MASS.csv" 
	 
 ##	 MREAD 0 "CVH-MASS.5_100"
 ##     MELCOR 0 EXPORT CSV "PD_O2_MASS.csv" 

 ##     MREAD 0 "CVH-X.6_100"
  ##    MELCOR 0 EXPORT CSV "PD_H2_FRAC.csv" 
	 
 ##	 MREAD 0 "CVH-X.5_100"
  ##    MELCOR 0 EXPORT CSV "PD_O2_FRAC.csv" 
	 
	 	 
	  ## REACTOR HALL
	 
 ##	    MREAD 0 "CVH-MASS.6_203"
     MELCOR 0 EXPORT CSV "RH_H2_MASS.csv" 
	 
 ##	 MREAD 0 "CVH-MASS.5_203"
     MELCOR 0 EXPORT CSV "RH_O2_MASS.csv" 

 ##     MREAD 0 "CVH-X.6_203"
     MELCOR 0 EXPORT CSV "RH_H2_FRAC.csv" 
	 
 ##	 MREAD 0 "CVH-X.5_203"
     MELCOR 0 EXPORT CSV "RH_O2_FRAC.csv" 
	 

# #
#EXIT
MELCOR 0 CLOSE
