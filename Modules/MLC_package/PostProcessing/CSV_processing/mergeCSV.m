%Function dedicated to merge CSV files - tkaen from the same MELCOR output
% folder = 'LOOP/case0002_A1/'
% 20_CAV_GAS
% InFiles ={[folder, '/','CAVH2_10.csv'] }
%  InFiles ={'LOOP/case0002_A1/CAVH2_10.csv' 'LOOP/case0002_A1/CAVH2_20.csv'}
% InFiles ={'LOOP/case0002_A1/CAVH2_10.csv' 'LOOP/case0002_A1/CAVH2_20.csv' 'LOOP/case0002_A1/CAVH2_30.csv' 'LOOP/case0002_A1/CAVCO_10.csv' 'LOOP/case0002_A1/CAVCO_20.csv' 'LOOP/case0002_A1/CAVCO_30.csv'}
% InFile = {'CAVH2_10.csv' 'CAVH2_20.csv' 'CAVH2_30.csv' 'CAVCO_10.csv' 'CAVCO_20.csv' 'CAVCO_30.csv'}
%  OutFile =    '20_xx'
% OutFile =     [folder,'20_CAV_GAS']
% Outfile = '20_CAV_GAS'
% mergeCSV({'5_MH2CR.csv'},'5_MH2CR','LOOP/case0002_A1/')
% mergeCSV(InFiles,OutFile,folder)
function result=mergeCSV(InFiles,OutFile,folder)

%InFiles = char(InFiles)

if(nargin <3)
		folder =''; 
	end


%files - filenames
NFiles=numel(InFiles);

for i=1:NFiles
  tmp = importdata([folder,InFiles{i}],',',2);
  CSV.data{i} =  tmp.data;
  numel(CSV.data{i}(:,1))
end

time = CSV.data{1}(:,1);
result = time;

for j=1:NFiles
	result = [result, CSV.data{j}(:,2)];
end

save([folder, OutFile],'result','-nocompression','-v7.3','-ascii','-double','-tabs')





% ===================================================================
% NOTES
% load 
% save myFile.mat A B -v7.3 -nocompression
%save('myFile.mat','A','B','-v7.3','-nocompression')
% fmt = '-ascii','-double','-tabs'
% save(filename,variables,fmt)
% save('test.mat','-nocompression','-v7.3','-ascii','-double','-tabs')
% csvread('./DATA/Default Dataset.csv')  older matlba
% use A = readmatrix(filename) 		matlab 2019

%{
 if  isfile(fullfile(filename))
		[fPath,fName,fExt] = fileparts(filename);
		if strcmp(lower(fExt),'.csv')		%Allow to use CSV files
			tmp = importdata(filename,',',2);
			EDF.data{i} = tmp.data;
		else
			EDF.data{i} = importdata(filename);
			
end
%}
