# Power Shell Script to execute several inputs in different windows
# http://tymoteuszkestowicz.com/2013/11/powershell-mini-kompendium/
# https://4sysops.com/archives/strings-in-powershell-replace-compare-concatenate-split-substring/
#https://www.varonis.com/blog/powershell-array/
# https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_arrays?view=powershell-7.1
# https://superuser.com/questions/237902/how-can-one-show-the-current-directory-in-powershell
# https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.management/start-process?view=powershell-7.1
# https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/invoke-expression?view=powershell-7.1
echo "Power Shell for MELCOR parallel execution"

#$cases = @(
#'cd C:\CloudStation\ROOT\NARSIS-GenIII\case0004_B3_PowerShellTest\calculations\case1'
#'cd C:\CloudStation\ROOT\NARSIS-GenIII\case0004_B3_PowerShellTest\calculations\case2'
#'cd C:\CloudStation\ROOT\NARSIS-GenIII\case0004_B3_PowerShellTest\calculations\case3'
#'cd C:\CloudStation\ROOT\NARSIS-GenIII\case0004_B3_PowerShellTest\calculations\case4'	 
#)

$folder = "C:\CloudStation\ROOT\NARSIS-GenIII\case0004_B3_PowerShellTest\calculations";
$folder2 = ".\MLC_UNC_Test1"


#$folder1 = @(
#'1'
#'2'
#'3'
#'4'
#)
# Cases od 1 do 4

$folder1 = 1..5


for ($i = 1; $i -lt 6; $i++)
{
echo $i	
echo $folder
echo "$PWD"

#$location =  $cases[$i-1]
#echo $location	

# JEzeli folder1 jest tablica liczb
$location2 = [string]$folder1[$i-1]

# Jezeli folder1 jest tablica stringow
#$location2 = $folder1[$i-1]


# echo "cd $folder\case$location2"
#echo "cd .\case$location2"
echo "cd $folder2\case$location2"


# Open new power-shell terminal
#Start-Process powershell.exe "$location; .\melgen.exe i=INPUT.INP ow=o; .\melcor.exe i=INPUT.INP ow=o; Pause"  # JEZELI LOKACJA JEST PODANA W CALOSCI
#Start-Process powershell.exe "cd $folder\case$location2; .\melgen.exe i=INPUT.INP ow=o; .\melcor.exe i=INPUT.INP ow=o; Pause"
#Start-Process powershell.exe "cd .\case$location2; ..\melgen.exe i=INPUT.INP ow=o; ..\melcor.exe i=INPUT.INP ow=o; Pause"
Start-Process powershell.exe "cd $folder2\case$location2; ..\..\melgen.exe i=INPUT.INP ow=o; ..\..\melcor.exe i=INPUT.INP ow=o; Pause"
echo "$PWD"
}

 Pause
'Finished Calculations...'







#################################################################################################
# SMIECI ROBOCZE
#################################################################################################





#'cd $folder\case1; melgen.exe i=INPUT.INP ow=o; melcor.exe i=INPUT.INP ow=o; '
#'cd $folder\case2; melgen.exe i=INPUT.INP ow=o; melcor.exe i=INPUT.INP ow=o; '
#'cd $folder\case3; melgen.exe i=INPUT.INP ow=o; melcor.exe i=INPUT.INP ow=o; '
#'cd $folder\case4; melgen.exe i=INPUT.INP ow=o; melcor.exe i=INPUT.INP ow=o; '


#$cases = @(
#'cd ' + $folder +'\case1; .\melgen.exe i=INPUT.INP; '
#'cd ' + $folder +'\case2; .\melgen.exe i=INPUT.INP; '
#'cd ' + $folder +'\case3; .\melgen.exe i=INPUT.INP; '
#'cd ' + $folder +'\case4; .\melgen.exe i=INPUT.INP; '	 
#)

#$cases = @(
# $folder + 'case1\melgen.exe i=INPUT.INP ow=o; ' 
# $folder + 'case2\melgen.exe i=INPUT.INP ow=o; ' 
# $folder + 'case3\melgen.exe i=INPUT.INP ow=o; ' 
# $folder + 'case4\melgen.exe i=INPUT.INP ow=o; ' 	 
#)

#$cases = @(
#$folder +'\case1\melgen.exe i=INPUT.INP ow=o '
#$folder +'\case2\melgen.exe i=INPUT.INP ow=o '
#$folder +'\case3\melgen.exe i=INPUT.INP ow=o '
#$folder +'\case4\melgen.exe i=INPUT.INP ow=o '	 
#)
#  cmd.exe /c dir /w

#$cases = @(
#'C:\CloudStation\ROOT\NARSIS-GenIII\case0004_B3_PowerShellTest\calculations\case1\melgen.exe i=INPUT.INP ow=o; '
#'C:\CloudStation\ROOT\NARSIS-GenIII\case0004_B3_PowerShellTest\calculations\case2\melgen.exe i=INPUT.INP ow=o; '
#'C:\CloudStation\ROOT\NARSIS-GenIII\case0004_B3_PowerShellTest\calculations\case3\melgen.exe i=INPUT.INP ow=o; '
#'C:\CloudStation\ROOT\NARSIS-GenIII\case0004_B3_PowerShellTest\calculations\case4\melgen.exe i=INPUT.INP ow=o; '	 
#)

# cd  ".\calculations\case$i"
# .\melgen.exe i=INPUT.INP ow=o
# .\melcor.exe i=INPUT.INP ow=o
#  -NoExit
# Invoke-Expression -Command Get-Location
#Start-Process "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Powershell.exe" -ArgumentList '-File C:\scripts\scriptx86.ps1' -Wait
#start-process powershell.exe -argument "-noexit -nologo -noprofile -command write-host $var"
#foreach($var in $cases) {	 
 #Start-Process powershell.exe -ArgumentList '-File .\run.ps1' -Wait
 #Invoke-Expression -Command "Start-Process powershell.exe .\run.ps1"
 #Invoke-Expression -Command "powershell.exe .\run.ps1"
 
# #cd C:\CloudStation\ROOT\NARSIS-GenIII\case0004_B3_PowerShellTest\calculations\case1
# 
# 
# 
# #Invoke-Expression -Command "Get-Location"
# #Invoke-Expression -Command ".\melgen.exe i=INPUT.INP ow=o"
# echo "$PWD"
# 
# 
# .\melgen.exe i=INPUT.INP ow=o
# 
# #.\melgen.exe i=INPUT.INP ow=o
# # .\melcor.exe i=INPUT.INP ow=o
# #echo $folder
# 
# # Pause
# 
# Pause
 

#Invoke-Expression -Command "start-process powershell.exe $location; .\run.ps1"


#echo "$PWD"
#echo $cases[$i-1]


#Invoke-Expression -Command "start-process powershell.exe; $location; "  #  .\run.ps1"



#$argument = "powershell.exe .\run.ps1"

#Invoke-Expression -Command start-process "$argument"



#Invoke-Expression -Command "$cases[$i]




#Invoke-Expression -Command "powershell.exe"
#Invoke-Expression -Command {"Start-Process -argument "powershell.exe; cd $var" "}

# for ($i = 1; $i -lt 5; $i++)
# {	
# }


#foreach($var in $cases) {	
#start-process powershell.exe -argument "-noexit -nologo -noprofile -command write-host $var"
#$var
#}
#
# Pause
#'Run Calculations...'

# foreach($var in $cases) {	
# start-process powershell.exe -argument "-noexit -nologo -noprofile -command write-host $var"
# $var
# }


#foreach($var in $cases) {
#	start powershell.exe {'echo hello'; Read-Host}
#}

# start-process powershell.exe -argument "-noexit -nologo -noprofile -command write-host $test"

#start-process powershell {$var}
#Start-Process powershell {$var}
	#Write-Host $var

#Pause
# Write-Host $i
#Start-Process Powershell.exe  $i

#cd  .\calculations\case$i
#cd C:\CloudStation\ROOT\NARSIS-GenIII\case0004_B3_PowerShellTest\calculations\case$i ;
#\case$i\melgen.exe i=INPUT.INP ow=o;
#C:\CloudStation\ROOT\NARSIS-GenIII\case0004_B3_PowerShellTest\calculations\case$i\melcor.exe i=INPUT.INP ow=o;


#Start-Process Powershell.exe -Argumentlist "-file C:\myscript.ps1"
#Start-Process Powershell.exe -Argumentlist "-file C:\myscript2.ps1"


# start powershell.exe C:\scriptPath\script1.ps1
# timeout 5
# start powershell.exe C:\scriptPath\script2.ps1
# timeout 5
# start powershell.exe C:\scriptPath\script3.ps1
# timeout 5
# start powershell.exe C:\scriptPath\script4.ps1
# timeout 5
# powershell.exe C:\scriptPath\script5.ps1
# REM ###############
# start powershell.exe C:\scriptPath\script6.ps1
# timeout 5
# start powershell.exe C:\scriptPath\script7.ps1
# timeout 5
# start powershell.exe C:\scriptPath\script8.ps1
# timeout 5
# start powershell.exe C:\scriptPath\script9.ps1
# timeout 5