# Power Shell Script to execute several inputs in different windows
echo "Power Shell for MELCOR parallel execution"


$cases = @(
'case1'
'case2'
)


foreach($case in $cases) {

echo $case

#Start-Process powershell.exe "cd .\$case; ..\melgen.exe i=INPUT.INP ow=o; cd .\$case; ..\melcor.exe i=INPUT.INP ow=o; Pause "
Start-Process powershell.exe "cd .\$case; ..\melgen.exe i=INPUT.INP ow=o; ..\melcor.exe i=INPUT.INP ow=o; Pause "
echo "$PWD"
}

-NoExit
 Pause
'Finished Calculations...'


