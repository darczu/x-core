﻿ echo "NARSIS MELCOR PARALLEL EXECUTION ENGINE"
 
 
#############################################################################################
# FOLDER GLOWNY + FOLDER WYNIKOWY + NAZWA FOLDERU DLA PRZYPADKOW
# Main Folder + Folder with Results + Cases names
$global:main_folder = 'C:\CloudStation\ROOT\NARSIS-GenIII\case0004_B3_PowerShellTest'
$global:results = '\calculations'
$global:case_folder = '\case'
#############################################################################################
# Based on Piotr Mazgaj Scripts
#Set-ExecutionPolicy Unrestricted
# powershell.exe -executionpolicy bypass -file .\Script.ps1
# powershell.exe -executionpolicy bypass -file .\Script.ps1

# In case of problems with execution EXECUTION POLICY
# powershell.exe -executionpolicy bypass -file .\serial_runner.ps1
# https://4sysops.com/archives/how-to-create-a-powershell-alias/




function getUsername {
Param ([string]$name=(Read-Host "Enter a user name"))
write-host "You entered$name"
}
Set-Alias ja getUsername -Scope Global

# RUN MELGEN
function global:m1 {
	Param ([string]$name)
	#C:\CloudStation\ROOT\NARSIS-GenIII\case0004_B3_PowerShellTest\melgen.exe i=$name ow=o
	Invoke-Expression -Command	"$main_folder\melgen.exe i=$name ow=o"
}
Set-alias melgen m1 -Scope Global

# RUN MELCOR
function global:m2 {
	Param ([string]$name)
	#C:\CloudStation\ROOT\NARSIS-GenIII\case0004_B3_PowerShellTest\melcor.exe  i=$name ow=o
	Invoke-Expression -Command "$main_folder\melcor.exe  i=$name ow=o"
} 
Set-alias melcor m2 -Scope Global



# ============================
# UNCERTANITY RUN
function global:m100 {
	Param ([string]$name)
	$name_folder = $results + $case_folder +$name 
	
	
	echo "Main: $main_folder"
	$folderX = $main_folder+$name_folder 
	
	echo "Runfolder: $folderX"

	Invoke-Expression -Command "cd $folderX"
	echo "Current: $PWD"

#	#cd C:\CloudStation\ROOT\NARSIS-GenIII\case0004_B3_PowerShellTest\$name_folder
#	echo "$PWD"
#	
  melgen INPUT.INP	
  melcor INPUT.INP    
}
Set-alias melcor_unc m100 -Scope Global











