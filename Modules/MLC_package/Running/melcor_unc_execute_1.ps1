## ============================
# EXECUTES MELCOR CALCULATIONS
# VERSION ALLOWING PARALLEL TERMINAL WITH SEVERAL CASES PER TERMINAL
# Allows to control batches for serial run in multiple windows.
# e.g. First row is list of cases executed in series in terminal 1, Seonc row list of cases exectued in terminal 2, etc.

# YOU CAN GENERATE scripts array entries with generate_list_for_parallel_calc.m
$scripts = @(
"melcor_unc 1; melcor_unc 2; Pause"
"melcor_unc 3; melcor_unc 4; Pause"
"melcor_unc 5; Pause"
)
# 3 rows == 3 windows and 5 code runs

# ============================
foreach($script in $scripts) {	
# Wczytuje funkcje i aliasy i odpala komendy wyzej
	Start-Process powershell.exe ".\melcor_unc_1.ps1; $script"
#	echo "$PWD" 
    #Pause
  }  
# ============================

 Pause
'Finished Calculations...'

