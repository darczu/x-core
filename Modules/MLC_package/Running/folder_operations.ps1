# Power Shell script to copy  and rename folder
for ($i = 1; $i -lt 5; $i++)
{
# Creates directories 
 New-Item -Path ".\calculations" -Name "case$i" -ItemType "directory"
 
 # opy directory contents to an existing directory
 Copy-Item -Path ".\base_case\*" -Destination ".\calculations\case$i\" -Recurse 
 
 Copy-Item -Path ".\*.exe" -Destination ".\calculations\case$i\" -Recurse 
 Copy-Item -Path ".\*.key" -Destination ".\calculations\case$i\" -Recurse 
 Copy-Item -Path ".\*.dll" -Destination ".\calculations\case$i\" -Recurse 
 

}

"Press Enter..."
Pause
