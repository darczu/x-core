# Power Shell Script to execute several inputs in different windows
echo "Power Shell for MELCOR parallel execution"


$cases = @(
'case0004_C4_unc_include_test'
)


foreach($case in $cases) {

echo $case

#Start-Process powershell.exe "cd .\$case; ..\melgen.exe i=INPUT.INP ow=o; cd .\$case; ..\melcor.exe i=INPUT.INP ow=o; Pause "
Start-Process powershell.exe "cd .\$case; ..\MELGEN_11932.exe i=INPUT.INP ow=o; cd .\$case; ..\MELCOR_11932.exe i=INPUT.INP ow=o; Pause "
echo "$PWD"
}


 Pause
'Finished Calculations...'


