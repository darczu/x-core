# Power Shell Script to execute several inputs in different windows
echo "Power Shell for MELCOR parallel execution"


$cases = @(

'gemini_plus\test0001'


)



foreach($case in $cases) {


echo $case

$melgen_inp = 'DLOFC.GEN'
$melcor_inp = 'DLOFC.COR'


#Start-Process powershell.exe  "-NoExit cd .\$case; Start-Transcript -Path .\console_log.txt; ..\..\melgen_15254.exe i=INPUT.INP ow=o; ..\..\melcor_15254.exe i=INPUT.INP ow=o; Stop-Transcript " 
Start-Process powershell.exe  "-NoExit cd .\$case; Start-Transcript -Path .\console_log.txt; ..\..\melgen_6342.exe i=$melgen_inp ow=o; ..\..\melcor_6342.exe i=$melcor_inp ow=o; Stop-Transcript " 
echo "$PWD"
}
