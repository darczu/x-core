# Power Shell Script to execute several inputs in different windows
echo "Power Shell for MELCOR parallel execution"




$cases = @(
#'case0004_C7noEDF'
#'case0004_M2218\case0004_C7_unc'
#'case0004_M2218\case0004_C7a_unc'
#'case0004_M2218\case0004_C7b_unc'
#'case0004_M2218\case0004_C7d_unc'
#'case0004_M2218\case0004_C8a_unc'
#'case0004_M2218\case0004_C8_unc'
#'case0004_M2218\case0004_C8_2_unc'
#'case0004_M2218\case0004_C13'
#'case0004_M2218\case0004_C14_LHF'
#'case0004_M2218\case0004_C14c_LHF'
#'case0004_M2218\case0004_C14d_LHF'
#'case0004_M2215\case0004_C14aa_LHF'
#'case0004_M2215\case0004_C14a_LHF'
#'case0004_M2215\case0004_C14a'
#'case0004_M2215\case0004_C14a8_newLP_LHF'
#'case0004_M2218\case0004_C14a8_newLP_LHF'
#'case0004_M2218\case0'
#'case0004_M2218\case0004_C19c'
#'case0004_M2218\case0004_C20'
'wersja3\test'
#'case0004_M2218\case0004_C15c'
#'case0004_M2218\case0004_C14j2_LHF'

#'case0004_M2218\test'


)



foreach($case in $cases) {

echo $case

#Start-Process powershell.exe "cd .\$case; ..\melgen.exe i=INPUT.INP ow=o; cd .\$case; ..\melcor.exe i=INPUT.INP ow=o; Pause "
#Start-Process powershell.exe "cd .\$case; ..\melgen.exe i=INPUT.INP ow=o; ..\melcor.exe i=INPUT.INP ow=o; Pause " 
#Start-Process powershell.exe "cd .\$case; Start-Transcript -Path .\console_log.txt; ..\melgen.exe i=INPUT.INP ow=o; ..\melcor.exe i=INPUT.INP ow=o; Pause; Stop-Transcript " 
#Start-Process powershell.exe "cd .\$case; Start-Transcript -Path .\console_log.txt; ..\..\melgen_11932.exe i=INPUT.INP ow=o; ..\..\melcor_11932.exe i=INPUT.INP ow=o; Pause; Stop-Transcript " 
#Start-Process powershell.exe "cd .\$case; Start-Transcript -Path .\console_log.txt; ..\..\melgen_9541.exe i=INPUT.INP ow=o; ..\..\melcor_9541.exe i=INPUT.INP ow=o; Pause; Stop-Transcript " 

#Start-Process powershell.exe  "-NoExit cd .\$case; Start-Transcript -Path .\console_log.txt; ..\..\melgen_15254.exe i=INPUT.INP ow=o; ..\..\melcor_15254.exe i=INPUT.INP ow=o; Stop-Transcript " 
Start-Process powershell.exe  "-NoExit cd .\$case; Start-Transcript -Path .\console_log.txt; ..\..\melgen_18019.exe i=INPUT.INP ow=o; ..\..\melcor_18019.exe i=INPUT.INP ow=o; Stop-Transcript " 
echo "$PWD"
}

#$(
#) *>&1 > ".\out.log"
# Pause
#'Finished Calculations...'
#Start-Transcript -Path ".\console_log.txt" -NoClobber
#Stop-Transcript
