%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Warsaw University of Technology Institute of Heat Engineering - Piotr Darnowski
%Example Class 7 - Reactor Physics Tutorials  Rev 22-01-2019
% Right Hand Side for the PDE Solver
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function dx = RHS_PDE_EXPLICIT(t,x,data,opt,opt1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dx = zeros(4,1);  %cleaning previous solution at the beginning of ODE solver time step
%        1    2      3    4    5    6   7   8    9   10   11  12  13  14  15   16
%data: beta,LAMBDA,lambda,t0,rho0,cp_c,cp_f,m_f,m_c,w_c,T_cin,hA,r_f,r_c,T_c0,T_f0

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% non reactivity controlled transients
if(t < data(4))   %initial time
       
elseif(t >= data(4)) %transient initation time
	if(opt == 'e')
	%(e) Cold water injection at Hot Full Power T_cin=530K 
	   data(11) = opt1;
	   
	elseif(opt == 'f')
	%(f) One reactor coolant pump trip (reactor has 4 of them). Reactor coolant pump coast-down time is 60s. Initial State: HFP. 
	% Initial flow 17000 kg/s - total
	   data(10) = ((4-opt1)/4)*17000.0 + (opt1/4)*17000.0*exp(-(t-data(4))/60.0);   
       
	end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rho= IN_REACTIVITY(t,x,data,opt,opt1); %function to calculate reactivity

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Point Dynamics Model ODE system:
dx(1) = ((rho-data(1))/data(2))*x(1) + (data(3))*x(2);
dx(2) = (data(1)/data(2))*x(1) - (data(3))*x(2); 
dx(3) = (x(1) - data(12)*(x(3)-x(4)))/(data(8)*data(7));
dx(4) = (data(12)*(x(3)-x(4)) - 2*data(10)*data(6)*(x(4)-data(11)))/(data(9)*data(6));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%dx(1) = ((rho-beta)/LAMBDA)*x(1) + (lambda)*x(2);
%dx(2) = (beta/LAMBDA)*x(1) - (lambda)*x(2); 
%dx(3) = (x(1) - hA*(x(3)-x(4)))/(m_f*cp_f);
%dx(4) = (hA*(x(3)-x(4)) - 2*w_c*cp_c*(x(4)-T_cin))/(m_c*cp_c);

% x(1) - Power
% x(2) - Prec. Concentration
% x(3) - Fuel Temp
% x(4) - Mod Temp
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%