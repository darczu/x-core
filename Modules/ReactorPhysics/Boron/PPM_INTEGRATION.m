% Calculates the  average value of the function
%USE TRAPZ FMATLAB FUNCTION TO INTEGRATE FUNCTION
% F�r= (1/(b-a)) * ca�ka oznaczona z funkcji w przedziale a-b.

%X = xlsread('boron_letdown','Sheet1','C4:C32'); BEAVRS
%Y = xlsread('boron_letdown','Sheet1','D4:D32'); BEAVRS

X = xlsread('boron_letdown','US-EPR','C3:C55');
Y = xlsread('boron_letdown','US-EPR','D3:D55'); 
%przedzia�
a = 0; 
b = 550.34314;

I = trapz(X,Y); 
Fav = I/(b-a)