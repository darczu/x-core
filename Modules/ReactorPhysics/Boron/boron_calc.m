% Rev 29-03-2017
% function to calculate boron atomic density - method based on the BEAVRS methodology
% 21-01-2017
% sb_boron - 'ppm' of boron in water
% rho_pure - density of pure water (without boron)
%
function [N, Ni, rho] = boron_calc(sb_boron,rho_pure)
format long e

M_B	= 10.81102817;  %Boron atomic weight [g/mol]  

% 10.0129370*0.199+0.801*11.0093055  =   10.8110281685;  
% B elemental 10.8135 BEAVRS 2017
% B-10 		10.0129370   BEAVRS 2017          
% B-10	     10.012937  XSDATA ENDF
% B-10 		10.0129370(4)        https://www.webelements.com/boron/     
% B-11  		11.0093054  BEAVRS 2017			  
% B-11 		11.009276  XSDATA ENDF
% B-11 		11.0093055(5)       https://www.webelements.com/boron/     
% Boron 10.811 g/mole  %IAEA THERPRO
% rho=10.811 (7)   https://www.webelements.com/boron/     


M_H	= 1.00794;      %atomic weight [g/mol]
M_O	= 15.9994;      %atomic weight [g/mol]
N_A = 0.60221415;  %[atom/mol/10^24]
wb_boron = sb_boron.*1e-6;  %mass fraction

M_H2O = 2*M_H + M_O; 			%water atomic weight [g/mol]
N_H2O = N_A*rho_pure/M_H2O;     %water atomic/nuclear density [at/barn/cm]
N_H = 2*N_H2O;
N_O = N_H2O;
rho_borated = rho_pure/(1-wb_boron);
N_B = rho_borated*wb_boron*N_A/M_B;   %atomic/nuclear density [at/barn/cm]

N_H1 =  0.99985*N_H;
N_H2 =  0.00015*N_H;
N_B10 = 0.199*N_B;
N_B11 = 0.801*N_B;
N_O16 = 0.99757*N_O;   % 1-0.00038, in reality there is  0.99757
N_O17 = 0.00038*N_O;
N_O18 = 0.00205*N_O;
N_O16 = N_O16 + N_O18;  %O-16 = O16+O18
%N_O17 = N_O17 + N_O18;       %O-17 = O17+O18 - second option

N = [N_H2O; N_H; N_B ;N_O];
Ni = [N_H1; N_H2; N_B10; N_B11; N_O16; N_O17];
rho = [rho_pure; rho_borated];




