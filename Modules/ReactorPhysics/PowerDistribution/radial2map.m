%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rev 14-01-2020
% Function takes radial vector with average values for zones and transforms it to 2D MAP
% Creates 2D radial core map  (matrix) based on input pattern matrix which containts zones number
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% radial    - vector with radial values of power or other parameter - like in RZ model
% zones_map - 2D map with zone numbering. 0 - is no-zone - outer core
% REMOVED: normalization  -  normalize input 2D map radially - sum all values and divide by number of non-zero elements 1 - yes and Default; 0 no
% 
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% map - produces normalized 2D map of the core
%
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% radial2map(radial,zones_map, normal) 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [map] = radial2map(radial,zones_map, normal) 

if(nargin < 3) 
	normal = 1; % Normalize output, as 2D core radial maps should be normalized to 1
else
	normal = 0;	%do not normalize the map
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% check if matrix is square
n = size(zones_map,1); 
m = size(zones_map,2);

if (n ~= m) 
	error('matrix is not a square');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Take data from zones_map
N = sum(sum((zones_map > 0))); %number as assamblies
zones = unique(zones_map); 		%matrix with unique values in the matrix - number of zones. Zone 0 is empty zone
zones = zones(2:end); %only core regions
% Check if radial has the same number of zones as zones map
%zones
%radial
if(numel(zones) ~= numel(radial) )
	error('number of zones in zones_map has to be the same as number of radial input vector');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create empty map

% Empty/zeros matrix n x n x number of zones+1
zones_map_separated = zeros(n,n); 

% Locations out of the core.
zones_map_outer = (zones_map == 0); 

for i=1:numel(zones)
	zones_map_separated_value(:,:,i) = zones(i).*(zones_map == zones(i)); %value of the zone
	zones_map_separated(:,:,i) 		 = (zones_map == zones(i));  %only 1
end

% number of zones of i-th type
N_zones = sum(sum((zones_map_separated > 0)));
N_zones = reshape(N_zones,[numel(N_zones) 1]);

power_radial_zones = radial;

for i=1:numel(zones)
	map_level(:,:,i) =  zones_map_separated(:,:,i).*power_radial_zones(i);
end

map = sum(map_level,3);

if normal == 1
	map = normalization(map); 
end

% Power distribution of a single zone as a sub-matrix
 %power_zones_map_separated =  zones_map_separated.*;

