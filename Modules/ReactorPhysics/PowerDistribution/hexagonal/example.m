% my
% use HexGrid 
clc; clear all; close all;

%run input14e_core0.m; 
run oxide_core0.m; 

N = 25;
N2 = N^2;
num_cols = N;
num_rows = N;

n0; 

lvl0_1 = zeros(N2,1);
lvl0_2 = zeros(N2,1);
lvl0_3 = zeros(N2,1);
lbl0_11 = zeros(N,N);
lbl0_22 = zeros(N,N);
lbl0_33 = zeros(N,N);

for j =1:N2
    for i = 1:n0      
        if lvl0(i,1) == j
            lvl0_1(j) = lvl0(i,1);
            lvl0_2(j) = lvl0(i,2);
            lvl0_3(j) = lvl0(i,3);
        end
    end
end

lvl0_11 = reshape(lvl0_1,[N,N]);
lvl0_22 = reshape(lvl0_2,[N,N]);
lvl0_33 = reshape(lvl0_3,[N,N]);

%lvl0_22_MAX = lvl0_22./ (mean(lvl0_2 ))  %or max
lvl0_22_MAX = lvl0_22./ (mean(lvl0(:,2)))  %or max

C = lvl0_22_MAX; 

[XX,YY] = meshgrid(1:N,1:N);


% side length
n=1; 

% transformation of X,Y - shift
XT=zeros(N,N);

for i=1:N
    for j=1:N
        if(i<(N/2-1))
            XT(i,j) = -(13-i)*n*(sqrt(3)/2);
        
        elseif(i> (N/2-1))
            XT(i,j) = +(i-13)*n*(sqrt(3)/2);

        end    
    end
end


for i=1:N
    for j=1:N
        if(mod(i,2)==1)  %even
          XT(i,j) = XT(i,j)-sqrt(3)/2
            
        else  %odd
            XT(i,j) = XT(i,j)-sqrt(3)
        end
    end
end

%XT = 0 



x_offset = n*sqrt(3); 
y_offset = n+n/2;

% determine the center locations
X_centers=repmat((1:num_cols),num_rows,1);  %repmat = repeat copy of the matrix
X_centers = X_centers*x_offset*1.0; 

Y_centers = repmat((1:num_rows)',1,num_cols);
Y_centers = Y_centers*y_offset*1.0;

% now shift odd rows over
odd_offset=n*sqrt(3)/2;
X_centers(2:2:end,:)=X_centers(2:2:end,:)+odd_offset;

%[XT, YT] = meshgrid(     ,[0, 0])

X_centers = X_centers + XT  %sqrt(3)/2;

X1 = reshape(X_centers,[N2,1]);
Y1 = reshape(Y_centers,[N2,1]);
C1 = reshape(lvl0_22_MAX,[N2,1]);
%%




xData = X1;
yData = Y1;
cData = C1;

% x and y limits
xLim = [8.5 36.25] ;
yLim = [6.5 33.5] ;

% regular scatter plot
f = figure
scatter(xData,yData,125,cData,'marker','o')
title('Regular scatter plot')
axis equal ; box on
colormap jet;
colorbar ;


% hexScatter plot with rHex = 1.0 without filling empty hexagons
rHex = 1.0 ;
ifFillEmptyHex = 1 ;
[figHndl,patchHndl,cbarHndl,xDataHex,yDataHex,cDataHex] = ...
    hexScatter(xData,yData,cData,xLim,yLim,rHex,ifFillEmptyHex) ;
%set(patchHndl,'EdgeColor',[1,1,1]) ;

 set(patchHndl,'EdgeColor',[0, 0, 0.51563]);
set(gca,'XTick',[])
set(gca,'YTick',[])
%set(cbarHndl,'jet') ;

set(gcf, 'Position',  [100, 100, 700, 700])


for i=1:N
    for j=1:N
        if(lvl0_22_MAX(i,j)>0)
            txt = [num2str(lvl0_22_MAX(i,j),'%2.3f')];
            x_text = X_centers(i,j)+(1/6)*sqrt(3)*n;
            y_text = Y_centers(i,j)+0.5*n
            text(x_text,y_text,txt,'FontSize',6,'Color','black');
        end
    end
end


%title('hexScatter plot with rHex = 1.0 without filling empty hexagons')

%{
% hexScatter plot with rHex = 0.3 without filling empty hexagons
rHex = 0.3 ;
ifFillEmptyHex = 0 ;
[figHndl,patchHndl,cbarHndl,xDataHex,yDataHex,cDataHex] = ...
    hexScatter(xData,yData,cData,xLim,yLim,rHex,ifFillEmptyHex) ;
title('hexScatter plot with rHex = 1.0 without filling empty hexagons')
set(patchHndl,'EdgeColor',[0.8,0.8,0.8]) ;
%}
%{
% hexScatter plot with rHex = 0.3 with filling empty hexagons
rHex = 0.3 ;
ifFillEmptyHex = 1 ;
[figHndl,patchHndl,cbarHndl,xDataHex,yDataHex,cDataHex] = ...
    hexScatter(xData,yData,cData,xLim,yLim,rHex,ifFillEmptyHex) ;
title('hexScatter plot with rHex = 1.0 without filling empty hexagons')
set(patchHndl,'EdgeColor',[0.8,0.8,0.8]) ;
%}
