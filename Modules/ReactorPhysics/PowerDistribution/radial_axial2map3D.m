%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rev 14-01-2020
% Calculate 3D Power Distribution with given axial and radial power profile vectors or 2D maps 
% Calculats fraction of power/flux/radionuclide etc.
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% radial 	- input vector with radial power/flux profile; 
%   alternatively matrix with 2D core map. Code detects it automatically.
% if radial is vector or 2D matrix
% THIS FUNCTION DOES NOT CONSIDER RELATIVE LOCATIONS OF RADIAL AND AXIAL. HENCE, USER HAVE TO CALCULATE IT SEPERATELY
% axial 	- input vector with axial power/flux profile with size Mx1
% THIS FUNCTION DOES NOT CONSIDER RELATIVE LOCATIONS OF RADIAL AND AXIAL. HENCE, USER HAVE TO CALCULATE IT SEPERATELY
% REMOVED: map   	- NOT USED; radial 2D map of the core with zones, numbering  starts with zone no. 1 outwards - number of zones has to have the same size as radial
% REMOVED: N 		- no. axial zones to be calculated, it is implicity assumed that it is active part of the core with all axial levels having the same height
% REMOVED: 			  if no input it is automatically the same as lenght of axial input vector
%
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% dist      	- distribution 3D - not normalized
% dist_norm 	- distribution 3D normalized to 1
% dist_RZ   	- matrix with size equal to number of radial rings x number of axial levels. It contains values
% dist_RZ_norm  - dist_RZ normalized to 1.0
% In any case level 1 is bottom. For RZ calcs (1,1) is bottom, center of the core
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% run Examples1.m
% Vector input: 
% [dist, dist_norm, dist_RZ , dist_RZ_norm] = radial_axial2map3D(radial,axial,zones_map)  
% Matrix input:
% [dist, dist_norm, dist_RZ , dist_RZ_norm] = radial_axial2map3D(map_EOL,axial,zones_map)  
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [dist, dist_norm, dist_RZ , dist_RZ_norm] = radial_axial2map3D(radial,axial,map)  

N = numel(axial); 	% Number of axial levels

% Check is radial is not multi-dim matrix
if(numel(size(radial)) > 2)
	error('only NxN matrix or vector');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3D distribution for input radial being a vector
if isvector(radial)
	%R = numel(radial);  %number of elements of radial matrix
	% multiplication of radial * axial
	% tensor product of two vectors column x row  ==  axial x radial 

	% 3D map generated
	radial_map = radial2map(radial, map);
	
	dist = [];
	for i=1:N
	   dist(:,:,i) = radial_map.*axial(i);
	end
	
	N_non_zero = sum(sum(sum(dist > 0)));
	avg1 = sum(sum(sum(dist)))./N_non_zero;   
	dist = dist./avg1;
	
	radial_R = radial;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
% it creates 3D map of core 2d maps for N axial levels
% to ahive the same with vector
% 3D distribution for matrix map
elseif ismatrix(radial)

	dist = [];
	for i=1:N
	    dist(:,:,i) = radial.*axial(i);
	end
	N_non_zero = sum(sum(sum(dist > 0)));
	avg1 = sum(sum(sum(dist)))./N_non_zero;   
	dist = dist./avg1;
	
%	radial_R = map2radial(radial,zones_map)';	
	radial_R = map2radial(radial,map);	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
else
	error('input radial is not matrix nor vector')
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if iscolumn(radial_R)
		radial_R = radial_R';
	else
		radial_R = radial_R;
	end

	if iscolumn(axial)
		%axial = axial; no change
	else
		axial = axial';
	end
	
	
	% normalize 3D map to 1.0 - sum of all elements in 3D matrix
	dist_norm = normalization(dist);
	
	dist_RZ = axial*radial_R;
	avg = sum(sum(dist_RZ))./(numel(dist_RZ));  %averaged	
	dist_RZ = dist_RZ./avg;  %distribution for RZ normalized to average value in RZ
	dist_RZ_norm = normalization(dist_RZ);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	%,N, interp) interpolation removed
% Default interpolation method
%if(nargin < 5)
%	interp = 'linear';
%end
% Default value if no N
%if(nargin < 4)
%	 N = M; 		% if not 4-th input make it the same as axial M. Default option.
%end
%M = numel(axial); 	% Number of axial levels

% Default values if no map
%if(nargin <3)
%   map = [];
%	if ismatrix(radial)
%	 map = ones(size(radial,1),size(radial,1));
%	end
%end   

