%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rev 14-01-2020
% Function takes 2D radial core map (matrix) and provides vector with relative average values for zones (defined in zones_map).
% User provides map with 2D power distribution (or other values) and 2D map of core zones
% The, function calculates (normalizes) relative average power (other) for each zone
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% map 		- 2D map with values (power etc.) for given core
% zones_map - 2D map with zone numbering. 0 - is no-zone - outer core
% normalization  -  normalize input 2D map radially - sum all values and divide by number of non-zero elements 1 - yes and Default; 0 no
% 
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% radial_value 	- vector with average values for given zone 
% average value per zone. It is sum of all values in zone divided by number of non-zero elements in zone (assamblies)
% radial_number - number of the zone 
% radial_norm   - radial vector normalized to 1, outcoming vector does not have to be normalized at all as it is 
% % Output radial_value is not normalized the third output variable is normalized
% radial maps are usually not normalized
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% run Examples1.m
% map = quarter2map(quarter_map_EOL)
% map = quarter2map(quarter_map_BOL) or map_2D
% new_radial = map2radial(map,zones_map) 
% [a,b,c] = map2radial(map_2D,zones_map)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function [radial_value, radial_number, randial_norm] = map2radial(map,zones_map,normal) 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [radial_value, radial_number, radial_norm] = map2radial(map,zones_map,normal) 


if(nargin < 3)  %Input map in general should be normalized to 1- user can select. By default it is normalzied
	normal = 1; % normalize input map radially - sum all values and divide by number of non-zero elements % dEfault
else
	normal = 0;	%do not normalize the map
end

% check if matrix is square
n = size(map,1); m = size(map,2);
if n ~= m
	error('matrix is not square');
end
% check if input matrices have the same size
if numel(map) ~= numel(zones_map)
	error('two input matrices should have the same size');
end

N = sum(sum((zones_map > 0))); %number as assamblies
zones = unique(zones_map); 		%matrix with unique values in the matrix - number of zones. Zone 0 is empty zone
zones = zones(2:end); %only core regions

% Normalization of the power distribution in relation to the average
sum_power_zones = sum(sum(map));  % sum of all powers
average_power =  sum_power_zones./N; % It is normalized if equal to one
normalized_power_zones = map./average_power;  % Normalized power distribution

% Map normalization  on/off
if normal == 1
	map =  normalized_power_zones; %Map normalization acrtive. Total sum of map is 1.0
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calcs with serpation
%
% Empty/zeros matrix n x n x number of zones+1
zones_map_separated = zeros(n,n,numel(zones)); 
% Locations out of the core.
zones_map_outer = (zones_map == 0); 
%zones_map_outer = (zones_map == 0); 

% Matrix with separater zones - single sub-matrix per single zone 
for i=1:numel(zones)
	zones_map_separated_value(:,:,i) = zones(i).*(zones_map == zones(i)); %value of the zone
	zones_map_separated(:,:,i) 		 = (zones_map == zones(i));  %only 1
end
% number of zones of i-th type
N_zones = sum(sum((zones_map_separated > 0)));
N_zones = reshape(N_zones,[numel(N_zones) 1]);

% Check if number of assamlies is correct
	if sum(N_zones) ~= N
		error('sth not ok with number fo zones');
	end

	
% Power distribution of a single zone as a sub-matrix
 power_zones_map_separated =  zones_map_separated.*map; %multuply each matrix level with matrix elements
 
sum_power_zones_map_separated =  sum(sum(power_zones_map_separated ));
h = size(sum_power_zones_map_separated,3); %size
sum_power_zones_map_separated_1 = reshape(sum_power_zones_map_separated,[h,1]); %vector with sum of values per zones

avgerage_power_zones_map_separated = sum_power_zones_map_separated_1./N_zones; %averaged of power zone = sum of powers in zone / number of assablies in zone


% Average value per given zone based on the input matrix values
radial_number = zones;
radial_value = avgerage_power_zones_map_separated;  % Values averaged per zone

radial_norm = radial_value./sum(radial_value); 

%radial_norm = normalization(radial_value)  %alternative

%radial_value_core = 

% plot(new_radial); hold on; plot(radial_power(:,2))



