%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rev 14-01-2020
% Function to create full core 2d map taking quarter core map.
% Input data is quarter core map. If the size is odd number code takes it as central column/row. If it is even it is the opposite
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% map - quarter map of the core - only bottom right quarter
% quarter - information about   IN current version only BR  ;BR - Bottom Right; BL - Bottom Left; TR - Top Right; TL - Top Left
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%
%
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see Examples1.m file 
% new_map = quarter2map(small) 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function new_map = quarter2map(map) 
%function [fraction] = quarter2map(map,quarter)

% No info about quarter
if(nargin < 2)
	% then no ifnromation about quadrant - it is assume that it is Bottom-Right
	quarter = 'BR'; 
end
% Check if square matrix
n = size(map,1); m = size(map,2);
if n ~= m
	error('matrix is not square');
end
% Check if odd or even
if mod(n,2) == 1  
% odd (nieparzysty)
	central_value = map(1,1);
	central_row_R = map(1,2:end);
	central_row_L = fliplr(central_row_R);
	central_col_B = map(2:end,1);
	central_col_T = flipud(central_col_B);
	
	map_BR = map(2:end,2:end);
	map_BL = fliplr(map_BR);
	map_TR = flipud(map_BR);
	map_TL = fliplr(map_TR);
	new_map = zeros(2*n-1,2*n-1); %new empty map
	
	new_map = [map_TL, central_col_T, map_TR ; ...
				central_row_L, central_value, central_row_R; ...	
				map_BL,central_col_B, map_BR ];
				
	% imagesc(new_map)
			
else
% even (parzysty)
	map_BR = map(1:end,1:end);
	map_BL = fliplr(map_BR);
	map_TR = flipud(map_BR);
	map_TL = fliplr(map_TR);
	new_map = zeros(2*n,2*n); %new empty map
	new_map = [map_TL, map_TR; map_BL, map_BR];
end
