%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates Density with Thermal Strain   Rev 04-05-2019
% General relation to calculate density with known thermal expansion data.
% Based on [2] MATPRO function FTHEXP (and others)
% it uses relation rho = rho0*(1-3*eps)
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho0           - reference density, [g/cc]
% 2: eps            - linear thermal expansion strain calculated for material at reference state (zero strain), [m/m]
% 3: mode           - option
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			      - density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function rho = DEN_THEXP(rho0,eps,mode)

    if(nargin < 3)
        mode ='Default';  %Default 'Kolev' ; 'Powers'
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch mode
    case {1,'single', 'Linear', 'linear', 'Default'}
        rho = rho0*(1.0 - 3.0*eps); %Based on FTHEXP [2]
        
        
    case {2,'triple', '3Linear', 'Orto', 'Orthogonal'}
        if numel(eps) == 3
            epsx = eps(1); epsy = eps(2); epsz = eps(3);
        else
            epsx = eps(1); epsy = eps(1); epsz = eps(1);
        end

        rho = rho0*(1.0 - epsx - epsy - epsz); %Based on CTHEXP [2]
    otherwise 
        error('No function');
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%rho = rho0.*((1+ (dLL0)./100).^(-3));
%rho(i) = rho0.*((1+ (dLL0)./100).^(-3));
%rho = rho0.*((1+ (dLL0)./100).^(-3));


%rho = rho0.*((1+ (dLL0)./100).^(3)); % NIE PODOBA MI SIE TA KORELACJA
%moim zdaniem powinono byc

% rho =m/V  rho0=m/V0 V=L*L*L  V0 = L0^3   rho=rho0*(L0/L)^3 = rho0*(L/L0)^-3 = rho0*(1+dL/L0)^-3
% w [2] jest ze rho=rho0*(1-3*epsilon)  gdzie epsilon to linear thermal expansion strain. To powinno malec a rosnie
% http://scholarsmine.mst.edu/cgi/viewcontent.cgi?article=8264&context=masters_theses
% http://onlinelibrary.wiley.com/doi/10.1111/jace.12893/references
% http://onlinelibrary.wiley.com/doi/10.1111/jace.12893/epdf
% rho = rho0.*(1+ (dLL0)./100).^(3);
% dLL0 = -0.135 + (3.940e-4).*T + (2.390e-7).*(T.^2)-(3.976e-11).*(T.^3);