%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plutonium DiOxide Density    Rev 21-02-2021
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				- temperature, [K]
% 2: mode			- mode, see below 
% 3: porosity		- porosity, [%/100] 
% 4: p				- pressure, [Pa]   
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			- density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mode  			- 'Carbajo'
% porosity 			- 0.0  
% p 				- 1E5
% T					- 300.0
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 'Carbajo' or  1 	- more complex model; includes melted uranium [3]
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% den = rho_PuO2(300,'Carbajo',0.05,1E5) 	% Calcs density with Carbajo with given all parameters
% den = rho_PuO2(300,'Carbajo',0.05)	
% den = rho_PuO2()
% den = rho_PUO2(500) 						
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out = rho_PuO2(T,mode,porosity,p)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	NT = numel(T); out = zeros(1,NT);
	
	if(nargin<1) 
		T = 300.0;
	 end
	
	if (nargin < 2)
		mode = 'Carbajo';
	end	

	% Porosity check
	if (nargin < 3)	
		porosity = 0.0;
	elseif( or(porosity>1.0,porosity<0.0 ) )
		error('Porosity has to be: 0.0<=p<=1.0');
	end

	if(nargin < 4)
		p = 1e5;
	end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch mode

	case {1, 'Carbajo'}
		rho0 = 11460; % +-80  density @273K [3]
		for i=1:numel(T)
			if (T(i)< 973)
				out(i) = rho0.*( (  9.9734e-1  + T(i).*9.802e-6 +  (-2.705e-10).*(T(i).^2) +  (4.391e-13).*(T(i).^3)).^(-3)); %[kg/m3]

			elseif(and(T(i)< 3120,T(i)>=973))
				out(i) = rho0.*( (  9.9672e-1  + T(i).*1.179e-5 +  (-2.429e-9).*(T(i).^2) + (1.219e-12).*(T(i).^3)).^(-3)); %[kg/m3]
				
			else	%fixed melting point
				out(i) = 8860 - (0.9285).*(T(i)-3120);  %[3]

					%BOTH FOR liquid UO2 and PuO2
					%rho = 8.74 - (9.18e-4).*(T-3120);  %REF ?	
			end

			if(T(i)>4500)
				disp('WARNING: Temperature beyond liquid uranium density correlation validity range 3120K<T<4500K');
			end
		end
		out = out./1000;  %kg/m3 -> g/cm3
		out = out.*(1-porosity);

	otherwise
		error('No function');
	end	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
