%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ag-In-Cd Density  Rev 04-05-2019, update 21-02-2021
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				      - temperature, [K]
% 2: mode			      - mode, see below - options 
% 3: p				      - pressure, [Pa]  
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			      - density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T                 			- 300 K
% mode              			- 'Default'
% p 							- 1e5 Pa
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1 or 'MATPRO' 						% Not actvie
% 2 or 'MELCOR' or 'Default'			% MELCOR based [7]
% 3 or 'BEAVRS'							% BEAVRS based [4]
% 4 or 'NENE'							% NENE-2014 Krshko NPP Conference Paper	
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% rho_AIC(1000.0);
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out = rho_AIC(T,mode,p)

	NT = numel(T); out = zeros(1,NT);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
T_melt = 1075.0; %MELCOR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(nargin < 1)
		T =300.0;  % Default Temperature 
	end
	if(nargin < 2)
		mode ='MELCOR';  
	end
	if(nargin < 3)
		p = 1e5;
	end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch mode
	case {1, 'MATPRO'}
		error('No function');
		
	case {2, 'MELCOR', 'Default'}
		out(:) = 9.6894;  %[4] MELCOR manual @ 1000K
		
	case {3, 'BEAVRS'}
		out(:) = 10.16;   % [7] BEAVRS

	case {4, 'NENE'}
		out(:) = 10158.52502/1000;  %Based on Westinghouse model of Krshko NPP presented in NENE2014 conf

	otherwise
		error('No function');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

