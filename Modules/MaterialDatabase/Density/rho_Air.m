% Air   - Rev 28-09-2018
% Density [g/cc]
% Temperature [K] 
% Pressure  [Pa]   -  removed
% Modify - use Ideal Gas Law
% function rho = rho_Air(T)

function out = rho_AIR(T)
    NT = numel(T); out = zeros(1,NT);

    out(:) = 0.001205; %[5]   Dry AIR Density  @ Normal Conditions T [K] and rho [g/cc]


