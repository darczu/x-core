%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Thorium DiOxide with Uranium DiOxide Density   Rev 28-03-2019
% General Thorium ThO2 and (Th,U)O2 
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				      - temperature, [K]
% 2: mode			      - mode, see below - options 
% y: mass fraction  - [wt%/100] content/ mass fraction of UO2. y = 0.0 - Pure Thoria (Default option); 
% x: atomic fraction - [at%/100] uranium atomic fraction for Th x U (1-x) O2
% 3: porosity		    - porosity, [%/100] 
% 4: p				      - pressure, [Pa]  
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			      - density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T                 - 300 K
% mode              - 'Ghosh'
% y                 - 0.0 - pure thoriua ThO2
% porosity 			    - 0.0 
% p 				        - 1E5 Pa 
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 'Ghosh' or  1    - Ghosh paper [604]
% 'IAEA'  or  2    - IAEA [013] 
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% rho_ThUO2(300.0,'IAEA',0.0);  rho_ThUO2(300.0,1,0.0);
% rho_ThUO2(300.0,'Ghosh',0.0)
% rho_ThUO2(300.0,1,0.5); rho_ThUO2(300.0,'IAEA',0.505);  % Tested with [013] data
%
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main References 013 and 060
% see REFERENCES.m file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function rho = rho_ThUO2(T,mode,y,porosity,p)


  if(nargin < 1) 
		T = 300.0;  % Default Temperature 
  end
  if (nargin < 2)
		mode = 'Ghosh';  % Default mode
  end
  if(nargin < 3) % content of UO2 
    y = 0.0; 
  end
	if (nargin < 4)
		porosity = 0.0; % Default porosity
	elseif( or(porosity>1.0,porosity<0.0 ) )
		error('Porosity has to be: 0.0<=p<=1.0');
  end    
	if(nargin < 5)
    p = 1e5;    % Default pressure
  end



if(or(strcmp(mode,'IAEA'),(mode==2)))
		for i=1:numel(T)
      % Theoretical density of (Th,U)O2 
      % Uncertanity of this equation is 0.28%
      yy = y.*100; %mass fraction
      rho(i) = 10.087 - T(i).*(2.891e-4) - T(i).*(yy).*(6.354e-7) + (yy).*(9.279e-3) + (yy.^2).*(5.111e-6);
    end
 

elseif(or(strcmp(mode,'Ghosh'),(mode==1))) % DEFAULT
    for i=1:numel(T)
      
        x = y;  % uranium atomic fraction 

      % Correlation based on correlations presented in Table 5 in  [604] - Melting behavior of (Th,U)O2and (Th,Pu)O2mixed oxides  Journal of Nuclear Materials 479 (2016) 112-122
      % It was developed for this code only. This function interpolates densities using correlations available in publication.
      % Solid phase density for 300K - melting point - be careful function does not take these limits into account
      % rho(T) = C1 + C2*T + C3*T^2 + C4*T^3
      %C1, C2, C3, C4 for 
      % Added UO2 based on Kolev [10] and it is based on Fink
     rho0 = 10.970; %UO2
     % rho(i) = rho0.*( (1.0056) + (-1.6324e-5).*(T(i)) + (-8.328e-9).*(T(i).^2)  + (2.0176e-13).*(T(i).^3)  );

      %ThO2, Th0.9375 U0.0625 O2 , Th U O2, Th U O2, Th U O2, Th U O2 
      C00 =[ 1          , 2             , 3           , 4             , 5           , 6               , 7       ];
      C0 = [0.0         , 0.0625        , 0.125       , 0.1875        , 0.25        , 0.3125          , 1.0     ];
      C1 = [10.0912     , 10.1478       , 10.2002     , 10.2492       , 10.3102     , 10.357          , rho0*1.0056     ];
      C2 = [-2.66141e-4 , -2.582e-4     , -2.4073e-4  , -2.09567e-4   ,-2.1423e-4   , -1.85017e-4     , rho0*(-1.6324e-5)     ];
      C3 = [-0.456978e-8, -0.52999e-8   , -2.1128e-8  , -4.77534e-8   ,-4.9352e-8   , -7.20112e-8     , rho0*(-8.328e-9)    ];
      C4 = [1.12e-11    , 0.94666e-11   , 0.66745e-11 , -1.6402e-12   ,  -1.4689e-12, -2.5823e-12     , rho0*(2.0176e-13)    ];
      
  
      % Check where is your x
      ind_bot= BisecSearch(x, C0); %BiSection search to find interval of Pu fraction (C0) for C00
      ind_top = ind_bot + 1;
  
     
      rho_bot(i) = C1(ind_bot) + C2(ind_bot).*T(i) + C3(ind_bot).*(T(i).^2) + C4(ind_bot).*(T(i).^3); %Density Bottom
      rho_top(i) = C1(ind_top) + C2(ind_top).*T(i) + C3(ind_top).*(T(i).^2) + C4(ind_top).*(T(i).^3); %Density Top
  
      %INTERPOLATION WITH INTERP1 MATLAB FUNCTION
      X = [C0(ind_bot), C0(ind_top)];
      V = [rho_bot(i),  rho_top(i)];
      rho(i) = interp1(X, V, x); 
  
    end
      



else	
		error('Incomplete input.');
end
  %Under Development ....



rho = rho.*(1.0-porosity);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MELCOR based data
% % FPT-1 model based data
% !                 tfname        tfscal        tfadcn
% TF_ID        'RHO_THORIA_OUTER'           1.0           0.0
% !      size
% TF_TAB    9 !n             x             y
% 1	293.0	10000.0
% 2	673.0	9932.0
% 3	1273.0	9818.0
% 4	1770.0	9716.0
% 5	2270.0	9601.0
% 6	2500.0	9548.0
% 7	3070.0	9406.0
% 8	3570.0	9275.0
% 9	3640.0	9258.0
% !
% % FPT-1 model based data
% !                 tfname        tfscal        tfadcn
% TF_ID        'RHO_THORIA_INNER'           1.0           0.0
% !      size
% TF_TAB    9 !n             x             y
% 1	293.0	10000.0
% 2	673.0	9898.0
% 3	1273.0	9728.0
% 4	1770.0	9577.0
% 5	2270.0	9407.0
% 6	2500.0	9330.0
% 7	3070.0	9122.0
% 8	3570.0	8933.0
% 9	3640.0	8908.0