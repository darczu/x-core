%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ZZirconium Di-Oxide Density  Rev 30-04-2019
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				      - temperature, [K]
% 2: mode			      - mode, see below - options 
% 3: p				      - pressure, [Pa]  
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			      - density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T                 - 300 K
% mode              - 'Default'
% p 				- 1e5 Pa
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 'Kolev' 
% 'Power'  
% 'MELCOR'
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% rho_ZRO2(500)
% rho_ZRO2(1000,'Kolev')
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function rho = rho_ZRO2(T,mode,p)

	if(nargin <1)
		T =300.0;  % Default Temperature 
	end
	if(nargin <2) 
		mode = 'MELCOR';  % Default mode
	end
	if(nargin <3)
		p = 1e5;    % Default pressure
	end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  T_melt = 2973.0;  %Kolev
  %T_melt = 2990.0; %MELCOR  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(strcmp(mode,'Kolev'))
  rho0s = 5800.0;

  c = [7.8e-6 -2.34e-3; ...
      1.302e-5 -3.338e-2]; % C matrix
  
  [id, T0, rho0l, beta, kappa, cpliq,INFO] = MAT_MELT_DATABASE(4);
  
		for i=1:numel(T)
		 	%if( T(i)<=300) 		
		 	%elseif(and(T(i)>300,T(i)<= 1478.0))

			if(T(i)<= 1478.0) % Limited to 300K %Checked with MATPRO - Kolev contains error
				epsilon = c(1,2) + c(1,1).*T(i);
				rho(i) = rho0s.*(1.0-3.*epsilon);
				rho(i) = rho(i)./1000;
				
		    elseif(and(T(i)>1478.0,T(i)<= T_melt)) %Checked with MATPRO - Kolev contains error
				epsilon = c(2,2) + c(2,1).*T(i);
				rho(i) = rho0s.*(1.0-3.*epsilon);
				rho(i) = rho(i)./1000;
				
			elseif( T(i)>T_melt) 	
%				[id, T0, rho0l, beta, kappa, cpliq,INFO] = MAT_MELT_DATABASE(4);
	   			rho(i) = rho_liq_pT(T(i),T0,p,beta,kappa,rho0l)/1000;			%[10]
				
		  end	
		end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
elseif(strcmp(mode,'Powers'))
	for i=1:numel(T)
		rho(i) = 5.863./(1+  T(i).*7.0e-5);  %[1]
	end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
elseif(strcmp(mode,'MELCOR'))	
	for i=1:numel(T)	
		rho(i) = 5.6; 						%[4] MELCOR 
	end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
else
	error('No such mode');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


