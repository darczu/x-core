%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Boron Oxide Density Rev 21-02-2021  % Rev 15-07-2017
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				- temperature, [K]
% 2: mode			- mode, see below - options 
% 3: p				- pressure, [Pa]     
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			- density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mode  			- 'MELCOR'
% porosity 			- 0.0  
% p 				- 1E5
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 'Carbajo' or  1 	- more complex model; includes melted uranium [3]
% 'Powers'  or	2 	- simple correlation based on [1] 
% 'MELCOR'  or	3 	- MELCOR based [4]
% 'Kolev'	or  4 	- Kolev base solid and liquid [10]
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m file
% COMMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out = rho_B2O3(T,mode,p)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%NT = numel(T); out = zeros(1,NT);
% option = 'Kolev' or 'Exp'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(nargin < 2)
 	mode = 'Kolev'; %Default
end

if(nargin < 3)
 p = 1e5;
end

rho0s = 2.46; %density fo 298.15 K [10]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch mode

% Kolev based
case {1, 'Kolev'}
	for i=1:numel(T)
			if( T(i)<298.15) 																			
				rho(i) = rho0s;	%assumption
			
			elseif(and(T(i)>=298.15,T(i)<=723.0))   												
				rho(i) = (2.477e3) + (5.296e-2).*T(i) + (-3.783e-4).*T(i).^2;	%[10] 	%assumption % Melting 723 K
				rho(i) = rho(i)/1000;
				
			elseif(T(i)>723.0)
				[id, T0, rho0l, beta, kappa, cpliq,INFO] = MAT_MELT_DATABASE(11);
				rho(i) = rho_liq_pT(T(i),T0,p,beta,kappa,rho0l)/1000;			%[10]
			end 
			
		   %if(T(i)>1953.15) 
			%	flag_boiling= 1;
			%end
			
	end	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Experimental based	
case {2, 'Exp'}
		for i=1:numel(T)
			
			if( T(i)<298.15) 																			
				rho(i) = rho0s;	%assumption			
			
			elseif(and(T(i)>=298.15,T(i)<=723.0))   
				rho(i) = (2.477e3) + (5.296e-2).*T(i) + (-3.783e-4).*T(i).^2;	%[10] Kolev based
				rho(i) = rho(i)/1000;
				
			%elseif(and(T(i)>723.0,T(i)<873.0))  	
				%rho(i) = 2.3175416993;	%[10]	assumed
				
			%elseif(and(T(i)<1273,T(i)>=873))
			elseif(and(T(i)<1273,T(i)>=723)) %assumed!!!
				rho(i) = 1.3388 + 228./T(i);  %[8] experimental
			
			else	%fixed melting point % T>=1273
				rho(i) = 1.3947 + 157.9./T(i); %[8] experimental
			end
		
		   %if(T(i)>1953.15) flag_boiling= 1;end
			
		end	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	otherwise
		error('no such options');
end  


out = rho;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%if(flag_boiling == 1) 
 % disp('WARNING (rho_B2O3): B2O3 boiling temperature 1953.15K exceeded');
%end

		
	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%rho = 5.863./(1+  T.*7.0e-5);  %[1]
 %dLL0 =  (3E-14).*T(i).^6 - (7E-11).*T(i).^5 + (7E-08).*T(i).^4 - (3E-05).*T(i).^3 + (0.0089).*T(i).^2 -  (1.2762).*T(i)+74.701;
%rho(i) = rho0.*((1+ (dLL0)./100).^(-3));
%rho = rho0.*((1+ (dLL0)./100).^(-3));
% Polynominal was fitted for T<600+273K
%y = 3E-14x6 - 7E-11x5 + 7E-08x4 - 3E-05x3 + 0.0089x2 - 1.2762x + 74.701    R² = 0.9891
%NIEWYKORZYSTANE
% DANE W EXCELU DLA B2O3 Ref .[9]

% [8] - High-Temperature Density Determination of Boron Oxide and Binary Rubidium and Cesium Borates  >600degC
% [9] - Thermal Expansion - Nonmetallic Solids <600degC    PAGE 1352
% [50] https://en.wikipedia.org/wiki/Boron_trioxide
%  2.460 g/cm3, liquid;
%2.55 g/cm3, trigonal;
% 3.11–3.146 g/cm3, monoclinic




% it is accurate beetwen 273<T[K]<557
%{
Temp [K]	dL/L0
273	-0.03
323	0.046
373	0.124
423	0.2
456	0.23
473	0.227
496	0.207
511	0.171
523	0.166
532	0.194
553	0.34
557	0.342
%}