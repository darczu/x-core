%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Helium density   Rev 14-06-2019. update 22-02-2021
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				      - temperature, [K]
% 2: p				      - pressure, [Pa]
% 3: mode			      - mode, see below - options 
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			      - density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T                 - 300 K
% p                 - 1e5 Pa
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: MHTGR             - [50]
% 2: MHTGR_simple       - [50]
% 3: NENE              - [NENE article for Krshko]
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main reference [50]
% see REFERENCES.m file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out = rho_He(T,mode,p)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  NT = numel(T); out = zeros(1,NT);

  if(nargin < 1) 
		T = 300.0;  % Default Temperature 
  end
  if(nargin < 2)
    p = 1e5;    % Default pressure
  end
  if (nargin < 3)
		mode = 'MHTGR';  % Default mode
  end
  %if or(numel(p)==1,numel(p)==numel(T))
  %else
  %  error('Only single pressure value allowed');
  %end
  if (numel(p)>1) 
    error('Only single pressure value allowed'); 
  end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch mode
  case{1,'MHTGR', 'Default'}
  % They are valid in the region 0.1 MPa-  10 MPa; 293 K1773 K
  %Results was compared with [50] and it is very close.
    pp = p./1e5;  %Pa -> bar
    for i=1:numel(T)
      rho(i) = 48.14.*((pp./T(i)).*(1./(1.0+0.4446.*pp.*(T(i).^(-1.2)))));  %[kg/m3]
      rho(i) = rho(i)/1000; %kg/m3 -> g/cc
    end	
  case{2,'MHTGR_simple'}
    pp = p./1e5;  %Pa -> bar
    for i=1:numel(T)
      p0 = 1e5/1e5;
      T0 = 273.16;
      rho0 = 0.18;
      rho(i) =  rho0.*(pp./p0).*(T0./T(i));
      rho(i) = rho(i)/1000; %kg -> g
      %Results was compared with [50] and it is very close.
    end
  case {3, 'NENE'}
    rho(:) = 0.0001786; %Based on Westinghouse model of Krshko NPP presented in NENE2014 conf
otherwise	
		error('Incomplete input.');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
out = rho;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
