%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Uranium DiOxide Density  Rev 20-02-2021
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				- temperature, [K]
% 2: mode			- mode, see below - options 
% 3: porosity		- porosity, [%/100] 
% 4: p				- pressure, [Pa]     
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			- density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mode  			- 'MELCOR'
% porosity 			- 0.0  
% p 				- 1E5
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 'Carbajo' or  1 	- more complex model; includes melted uranium [3]
% 'Powers'  or	2 	- simple correlation based on [1] 
% 'MELCOR'  or	3 	- MELCOR based [4]
% 'Kolev'	or  4 	- Kolev base solid and liquid [10]
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% den = rho_UO2(300,'Carbajo')   			% Calcs density with Carbajo with default pressure and porosity 1E5 Pa and 0.0 porosity
% den = rho_UO2(300,'Carbajo',0.05,1E5) 	% Calcs density with Carbajo with given all parameters
% den = rho_UO2(300,'Carbajo',0.05)		% Calcs density with Carbajo with default pressure and 5% porosity
% den = rho_UO2(500,'Powers') 
% den = rho_UO2(300,'Powers')
% den = rho_UO2(300,'Kolev')
% Default mode is 'MELCOR' - it works with one or no argument,  other are ignored. It returns constant value 10.96 g/cc.
% den = rho_UO2()
% den = rho_UO2(300) 
% den = rho_UO2(300,'MELCOR') 
% REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m file
% COMMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% REVIEW POWERS!!! - it is the same as in report but provides too high value !!!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out = rho_UO2(T,mode,porosity,p)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
NT = numel(T); out = zeros(1,NT);
	
	T_melt = 3120.0; %Kolev
	%T_melt=3113; %MELCOR
	if(nargin <1)
		T =300.0;  % Default Temperature 
	end
	
	if (nargin <2)
		mode = 'MELCOR';  %Default
	end

	if (nargin < 3)
		porosity = 0.0;
	elseif( or(porosity>1.0,porosity<0.0 ) )
		error('Porosity has to be: 0.0<=p<=1.0');
	end

	if(nargin < 4)
	 p = 1e5;
	end
	
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
switch mode
	case {1, 'Powers'}
	% Powers correlation [1]
		for i=1:NT
			out(i) = 12.255./(1+  T(i).*13.1015e-5);  %Powers NRC
		end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%			
	case{2, 'Carbajo'}
			% Carbajo correlation [3]
			% It is possbile that Carbajo made an error,, he cites Fink 2000 J. Nuc Mat and there is slightly different correlation. Otherwise when you check Martin work you will find
			% that Fink made a mistake [14]
			
			rho0 = 10970; %UO2
				for i=1:NT			
					if (T(i)< 973)
						out(i) = rho0.*( (  9.9734e-1  + T(i).*9.802e-6 +  (-2.705e-10).*(T(i).^2) +  (4.391e-13).*(T(i).^3)).^(-3)); %[kg/m3]
					elseif(and(T(i)< T_melt,T(i)>=973))
						out(i) = rho0.*( (  9.9672e-1  + T(i).*1.179e-5 +  (-2.429e-9).*(T(i).^2) + (1.219e-12).*(T(i).^3)).^(-3)); %[kg/m3]
					else  %fixed melting point
						out(i) = 8860.0 - (0.9285).*(T(i)-T_melt);  %[3]
					 
						%BOTH FOR liquid UO2 and PuO2
						%rho = 8.74 - (9.18e-4).*(T-3120);  %REF ?
						if(T(i)>4500)
							disp('WARNING: Temperature beyond liquid uranium density correlation validity range 3120K<T<4500K');
						end
					end
				end
		out = out./1000;  %kg/m3 -> g/cm3
		
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%			
%	case{3, 'Fink'}
%			% Like Carbajo but Fink
%			% It is possbile that Carbajo made an error,, he cites Fink 2000 J. Nuc Mat and there is slightly different correlation
%			
%			rho0 = 10970; %UO2
%				for i=1:NT			
%					if (T(i)< 973)
%						rho(i) = rho0.*( (  9.9734e-1  + T(i).*9.802e-6 +  (-2.705e-10).*(T(i).^2) +  (4.391e-13).*(T(i).^3)).^(-3)); %[kg/m3]
%					elseif(and(T(i)< T_melt,T(i)>=973))
%						rho(i) = rho0.*( (  9.9672e-1  + T(i).*1.179e-5 +  (-2.429e-9).*(T(i).^2) + (1.219e-12).*(T(i).^3)).^(-3)); %[kg/m3]
%					else  %fixed melting point
%						rho(i) = 8860.0 - (0.9285).*(T(i)-T_melt);  %[3]
%					 
%						%BOTH FOR liquid UO2 and PuO2
%						%rho = 8.74 - (9.18e-4).*(T-3120);  %REF ?
%						if(T(i)>4500)
%							disp('WARNING: Temperature beyond liquid uranium density correlation validity range 3120K<T<4500K');
%						end
%					end
%				end
%		rho = rho./1000;  %kg/m3 -> g/cm3	
%		
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
	case{4, 'MELCOR'}
			out(:) = 10.96;  %[4]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
	case{5, 'Kolev'}
		% Kolev provided  [10] - based on Fink
		
		[id, T0, rho01, beta, kappa, cpliq,INFO ] = MAT_MELT_DATABASE(1);
		
		rho0 = 10970; %UO2
		
			for i=1:NT			
					%if (T(i)< 3113.15)
					if (T(i)< T_melt)
						out(i) = rho0.*( (1.0056) + (-1.6324e-5).*(T(i)) + (-8.328e-9).*(T(i).^2)  + (2.0176e-13).*(T(i).^3)  );
						out(i) = out(i)./1000;
					else
						out(i) = rho_liq_pT(T(i),T0,p,beta,kappa,rho01)/1000;
					end
			end       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
	case{6, 'NARSIS_1'}
	% NARSIS Gen II Plant UO2 density		
		out(:) = 10.412;		
		
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
	otherwise
		error('Incomplete input.');
	end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
out = out.*(1.0-porosity);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% OLD
% %{
% 
% function rho = rho_UO2liq(T)
% 
% %Ronchi et al. [3] calculated the liquid density of UO2 from their least-squares fit to the data of
% %Christensen [16] and Drotning [17]
% %rho = 10.970/(1+  (T-273).*9.3e-5);
% 
% 
% %{
% Values calculated with this equation differ significantly from those obtained from the
% equation recommended by Drotning and the recent equation of Breitung and Reil [18] which is based
% on in reactor measurements of the density and thermal expansion from the melting point to 8000 K.
% The equation of Breitung and Reil [18] is: 
% %}
% 
% %Given in Carbajo
% %Woorks beetwen 3120-4500 K
% 
% %rho = 8.860 - (0.9285./1000).*(T-3120);
% 
% %if(or(T>4500,T<3120))
% % disp('WARNING: Temperature beyond liquid uranium density correlation validity range 3120K<T<4500K');
% %end
% 
% %BOTH FOR liquid UO2 and PuO2
% %rho = 8.74 - (9.18e-4).*(T-3120);
% 
% 
% %}
