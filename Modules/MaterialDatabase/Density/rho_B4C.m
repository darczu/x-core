%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Boron Carbide Density  Rev 20-02-2021
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				- temperature, [K]
% 2: mode			- mode, see below - options 
% 3: p				- pressure, [Pa]     
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			- density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mode  			- 'MELCOR'
% p 				- 1E5
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 'MELCOR'	or	1	
% 'MATPRO'	or	2		
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% XCore('rho_B4C', [1000 600], 'MATPRO')
% REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m file
% 
% [1] - Density Stratification and Fission Product Partitioning in Molten Corium Phases
% [2] - MATPRO RELAP5 3D Vol 4
% [3] - Carbajo  A review of the thermophysical proporties of MOX and UO2 fuels.
% [4] - MELCOR Reference Manual
% [5] - PNNL-15870rev1
% COMMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function rho = rho_B4C(T,mode,p)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
NT = numel(T);

T_melt = 2620.0; %MELCOR

if(nargin <2)
  mode = 'MELCOR';  %Default
end 

if(nargin < 3)
 p = 1e5;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
switch mode
	case{1, 'MATPRO'}
		for i=1:NT
			rho0=2.5; 			%[2]
			eps_a(i) = -1.10e-3 + (3.09e-6).*T(i) + (1.88e-9).*(T(i).^2);
			rho(i) = rho0.*((1+eps_a(i)).^(-3));
		end	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
	case{2, 'MELCOR'}	
		rho(1:NT) =  2.52; 				% [4] MELCOR

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
otherwise
	error('Incomplete input.');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	

%rho = 2.52;  %[4] MELCOR manual and [5]
%[2]
% rho0 = 2.5;  %it is density at 300K 
% rho = rho0.*(1 + dLL0).^(-3)   or (1-3*dLL0)
