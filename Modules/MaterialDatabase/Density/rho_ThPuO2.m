%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Thorium DiOxide with Plutonium DiOxide Density   Rev 28-03-2019
% ThO2, PuO2 and Thorium Plutonium MOX (Th,Pu)O2 
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				      - temperature, [K]
% 2: mode			      - mode, see below - options 
% x: atomic fraction  - (at%/100) atomic fraction of PuO2. x = 0.0 - Pure Thoria (Default option); 
% 3: porosity		    - porosity, [%/100] 
% 4: p				      - pressure, [Pa]  
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			      - density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T                 - 300 K
% mode              - 'Default'
% x                 - 0.0 - pure thoriua ThO2
% porosity 			    - 0.0 
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 'Ghosh' or 1    - Ghosh paper [604]
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Example 1
% x=linspace(0.0, 1.0, 10); T = 300.0; 
% for i=1:numel(x)  
%  rho(i) = rho_ThPuO2(T,1,x(i)); 
% end
% plot(x,rho) 
%
% Example 2
% T=linspace(300,2000, 50);
% rho = rho_ThPuO2(T, 1, 0.5);
% plot(T,rho)
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main References 604
% see REFERENCES.m file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function rho = rho_ThPuO2(T,mode,x,porosity,p)

  if(nargin < 1) 
		T = 300.0;  % Default Temperature 
  end
  if (nargin < 2)
		mode = 'Ghosh';  % Default mode
  end
  if(nargin < 3) % content of UO2 
    x = 0.0; 
  end
	if (nargin < 4)
		porosity = 0.0; % Default porosity
	elseif( or(porosity>1.0,porosity<0.0 ) )
		error('Porosity has to be: 0.0<=p<=1.0');
  end    
	if(nargin < 5)
    p = 1e5;    % Default pressure
  end


	if(or(strcmp(mode,'Ghosh'),(mode==1)))
		for i=1:numel(T)
    
    % Correlation based on correlations presented in Table 5 in  [604] - Melting behavior of (Th,U)O2and (Th,Pu)O2mixed oxides  Journal of Nuclear Materials 479 (2016) 112-122
    % It was developed for this code only. This function interpolates densities using correlations available in publication.
    % Solid phase density for 300K - melting point - be careful function does not take these limits into account
    % rho(T) = C1 + C2*T + C3*T^2 + C4*T^3
    %C1, C2, C3, C4 for 
    %ThO2, Th0.97Pu0.03 , Th0.95Pu0.05, Th0.92Pu0.08 , Th0.80Pu0.20, Th0.7Pu0.30 , PuO2
    C00 =[ 1           ,2            , 3          , 4           , 5          , 6          , 7           ];
    C0 = [0.0          , 0.03        , 0.05       , 0.08        , 0.20       , 0.30       , 1.0         ];
    C1 = [10.0912      , 10.1003     , 10.1777    , 10.2065     , 10.3781    , 10.5298    , 11.7581     ];
    C2 = [-2.66141e-4  , -2.2928e-4  , -2.5411e-4 , -2.4231e-4  , -2.0886e-4 , -1.8369e-4 , -3.229e-4   ];
    C3 = [-0.456978e-8 , -2.2473e-8  , -7.7185e-8 , -1.9516e-8  , -5.822e-8  , -8.9812e-8 , -1.0469e-8  ];
    C4 = [ 1.12e-11    , 0.6232e-11  , 0.9219e-11 ,  0.6956e-11 , 0.0459e-11 , 0.7064e-11 , -1.6744e-11 ];
    

    % Check where is your x
    ind_bot= BisecSearch(x, C0); %BiSection search to find interval of Pu fraction (C0) for C00
    ind_top = ind_bot + 1;

   
    rho_bot(i) = C1(ind_bot) + C2(ind_bot).*T(i) + C3(ind_bot).*(T(i).^2) + C4(ind_bot).*(T(i).^3); %Density Bottom
    rho_top(i) = C1(ind_top) + C2(ind_top).*T(i) + C3(ind_top).*(T(i).^2) + C4(ind_top).*(T(i).^3); %Density Top

    %INTERPOLATION WITH INTERP1 MATLAB FUNCTION
    X = [C0(ind_bot), C0(ind_top)];
    V = [rho_bot(i),  rho_top(i)];
    rho(i) = interp1(X, V, x); 

    end
    

	else	
		error('Incomplete input.');
  end
  %Under Development ....



rho = rho.*(1.0-porosity);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MELCOR based data
% % FPT-1 model based data
% !                 tfname        tfscal        tfadcn
% TF_ID        'RHO_THORIA_OUTER'           1.0           0.0
% !      size
% TF_TAB    9 !n             x             y
% 1	293.0	10000.0
% 2	673.0	9932.0
% 3	1273.0	9818.0
% 4	1770.0	9716.0
% 5	2270.0	9601.0
% 6	2500.0	9548.0
% 7	3070.0	9406.0
% 8	3570.0	9275.0
% 9	3640.0	9258.0
% !
% % FPT-1 model based data
% !                 tfname        tfscal        tfadcn
% TF_ID        'RHO_THORIA_INNER'           1.0           0.0
% !      size
% TF_TAB    9 !n             x             y
% 1	293.0	10000.0
% 2	673.0	9898.0
% 3	1273.0	9728.0
% 4	1770.0	9577.0
% 5	2270.0	9407.0
% 6	2500.0	9330.0
% 7	3070.0	9122.0
% 8	3570.0	8933.0
% 9	3640.0	8908.0