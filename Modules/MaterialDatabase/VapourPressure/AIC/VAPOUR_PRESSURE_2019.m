% 1 7110 AG-CR     1000.0         1 1  !T 
% 2 7110 AG-CR     12600.0        1 2	! A  - CHECK PLUS/MINUS
% 3 7110 AG-CR     7.989          1 3	! B
% 4 7110 AG-CR     0.0            1 4	! C
% 
% 
% 1 7110 IN-CR     400.0          1 1  !T 
% 2 7110 IN-CR     127000.0       1 2	! A  - CHECK PLUS/MINUS
% 3 7110 IN-CR     8.284          1 3	! B
% 4 7110 IN-CR     0.0            1 4	! C
%
%
%1 7110 CD-CR     500.0          1 1  !T 
%2 7110 CD-CR     5310.0         1 2	! A  - CHECK PLUS/MINUS
%3 7110 CD-CR     7.99           1 3	! B
%4 7110 CD-CR     0.0            1 4	! C
%

% CR2
%David R. Lide (ed), CRC Handbook of Chemistry and Physics, 84th Edition, online version. CRC Press. Boca Raton, Florida, 2003; Section 4, Properties of the Elements and Inorganic Compounds; Vapor Pressure of the Metallic Elements

%SMI.c,m (
% 

clear; clc; close all;

labels = {};
% labels = [labels, 'TRACE REF'];
%legend({'Ag Default' 'Cd Default' 'Ag-CR' 'In-CR' 'Cd-CR'  'SNL AG' 'SNL CD' 'SNL IN' 'IN-WIKI' 'CD-WIKI' 'AG-WIKI'})

figure(1)
hold on;



T = linspace(500,2000,100); %[K]

% ==============================================================================================================
%AG DEFAULT
A = 15400.0; 
B = 8.15;
C = 0.0;
log10_P_Ag_Hg = -A./T + B + C.*log10(T);
P_Ag_Hg =   10.^(log10_P_Ag_Hg);
P_Ag = P_Ag_Hg .*133.322368;  % 1mmHg = 133.322368 Pas
plot(T,P_Ag,'m-','LineWidth',2);
labels = [labels, 'Ag Default'];
% ==============================================================================================================
% CD DEFAULT CLASSs
A = 13730.0; 
B = 8.43 ;
C = 0.0;
log10_P_Cd_Hg = -A./T + B + C.*log10(T);
P_Cd_Hg =   10.^(log10_P_Cd_Hg);
P_Cd = P_Cd_Hg .*133.322368;  % 1mmHg = 133.322368 Pas
%plot(T,log10_P_Cd_Hg,'y-');
plot(T,P_Cd,'g-','LineWidth',2);
labels = [labels, 'Cd Default'];

% ==============================================================================================================
%AG-CR   - 2000K   https://www.iap.tuwien.ac.at/www/surface/vapor_pressure  zgadza sie
A = 12600.0; 
B = 7.989;
C = 0.0;
log10_P_Ag_Hg = -A./T + B + C.*log10(T);
P_Ag_Hg =   10.^(log10_P_Ag_Hg);
P_Ag = P_Ag_Hg .*133.322368;  % 1mmHg = 133.322368 Pas
%P_Ag = P_Ag_Hg;
%plot(T,log10_P_Ag_Hg,'r-');
plot(T,P_Ag,'r--','LineWidth',2);
labels = [labels, 'AG-CR'];
% ==============================================================================================================
 %IN-CR 
A = 12700.0; %It was 12700 -> changed from 127000.0 
B = 8.284;
C = 0.0;
log10_P_In_Hg = -A./T + B + C.*log10(T);
P_In_Hg =   10.^(log10_P_In_Hg);
P_In = P_In_Hg .*133.322368;  % 1mmHg = 133.322368 Pas
%P_In = P_In_Hg;

%plot(T,log10_P_In_Hg,'m-');
plot(T,P_In,'b--','LineWidth',2);
labels = [labels, 'IN-CR'];

dummy = P_In;
%{
%7.983 
 %IN-CR 
A = 12700.0; %It was 12700 -> changed from 127000.0 
%B = 8.284;
%B = 8.284 + log10(0.5);
B = 7.284;
C = 0.0;
log10_P_In_Hg = -A./T + B + C.*log10(T);
P_In_Hg =   10.^(log10_P_In_Hg);
P_In = P_In_Hg .*133.322368;  % 1mmHg = 133.322368 Pas
%P_In = P_In_Hg;

%plot(T,log10_P_In_Hg,'m-');
plot(T,P_In,'k--','LineWidth',2);
labels = [labels, 'Corrected IN-CR'];

ratio = dummy./P_In;
%}

% ==============================================================================================================
 %CD-CR ZGODAZ z : https://www.iap.tuwien.ac.at/www/surface/vapor_pressure  
A = 5310.0; 
B = 7.99 ;
C = 0.0;
log10_P_Cd_Hg = -A./T + B + C.*log10(T);
P_Cd_Hg =   10.^(log10_P_Cd_Hg);
P_Cd = P_Cd_Hg .*133.322368;  % 1mmHg = 133.322368 Pas
%P_Cd = P_Cd_Hg; 
%plot(T,log10_P_Cd_Hg,'y-');
plot(T,P_Cd,'c--','LineWidth',2);
labels = [labels, 'CD-CR'];

% ==============================================================================================================
% SNL VAPOUR CURVES
load SNL_CONTROL_ROD;
x1 = SNL_AG(:,1);
y1 = SNL_AG(:,2).*133.322368;

x2 = SNL_CD(:,1);
y2 = SNL_CD(:,2).*133.322368;

x3 = SNL_IN(:,1);
y3 = SNL_IN(:,2).*133.322368;

plot(x1,y1,'ko');
plot(x2,y2,'kx');
plot(x3,y3,'ks');

%legend({'Ag Default' 'Cd Default' 'Ag-CR' 'In-CR' 'Cd-CR'  'SNL AG' 'SNL CD' 'SNL IN' 'IN-WIKI' 'CD-WIKI' 'AG-WIKI'})

labels = [labels, 'AG SNL'];
labels = [labels, 'CD SNL'];
labels = [labels, 'IN SNL'];

% ==============================================================================================================
% INDUM WIKIPEDIA
% https://en.wikipedia.org/wiki/Vapor_pressures_of_the_elements_(data_page)
%  log (P/Pa) = 10.380 - 12276 / (T/K)  %liquid       CR2 (T/K)
%  log (P/Pa) = 10.055 - 12150 / (T/K)  %liquid    SMI.c,m (T/K)
%   log (P/Pa) = 10.997 - 12548 / (T/K) %solid    CR2
%A = 12276.0
%B = 10.380 ;
%C = 0.0;
A =  12150.
B =  10.055;
C =0.0;
% A =  12548.
% B =  10.997
% C = 0.0

log10_P_InW_Pa = -A./T + B + C.*log10(T);
P_InW_Pa =   10.^(log10_P_InW_Pa);
%P_InW = P_InW_Hg .*133.322368;  % 1mmHg = 133.322368 Pas
P_InW = P_InW_Pa;
plot(T, P_InW, 'r-');
labels = [labels, 'IN SMI'];

% ==============================================================================================================
% CD WIKI
% log (P/Pa) = 10.248 - 5392 / (T/K)  %liquid   CR2  (T/K)
% log (P/Pa) = 10.945 - 5799 / (T/K) %solid   CR2 (T/K)
A =  5392.
B =  10.248;
C =  0.0;

%A =  5799
%B = 10.945
%C =  0.0;

log10_P_CdW_Pa = -A./T + B + C.*log10(T);
P_CdW_Pa =   10.^(log10_P_CdW_Pa);
%P_CdW = P_CdW_Hg .*133.322368;  % 1mmHg = 133.322368 Pas
P_CdW = P_CdW_Pa;
plot(T, P_CdW, 'b-');
labels = [labels, 'CD CRC'];

% ==============================================================================================================
% AG WIKI
%log (P/Pa) = 10.758 - 13827 / (T/K)   CR2 (T/K)
A =  13827.0
B = 10.758 
C = 0.0

%A = 
%B = 
%C = 

log10_P_AgW_Pa = -A./T + B + C.*log10(T);
P_AgW_Pa =   10.^(log10_P_AgW_Pa);
%P_AgW = P_AgW_Hg .*133.322368;  % 1mmHg = 133.322368 Pas
P_AgW = P_AgW_Pa;
plot(T, P_AgW, 'k-');
labels = [labels, 'AG CRC'];

% ==============================================================================================================
% CsM
%A = 12100.0; 
%B = 7.675 ;
%C = 0.0;
%log10_P_CsM_Hg = -A./T + B + C.*log10(T);
%P_CsM_Hg =   10.^(log10_P_CsM_Hg);
%P_CsM = P_CsM_Hg .*133.322368;  % 1mmHg = 133.322368 Pas
%plot(T, P_CsM, '--');

box on; grid on;
set(gca, 'YScale', 'log')
title('Vapour Pressure of AIC Materials');
ylim([1e-20 Inf])

xlabel('Temperature, [K]'); ylabel('Vapour Pressure, [Pa]');

legend(labels,'Interpreter','none');

%legend({'Ag Default' 'Cd Default' 'Ag-CR' 'In-CR' 'Cd-CR'  'SNL AG' 'SNL CD' 'SNL IN' 'IN-WIKI' 'CD-WIKI' 'AG-WIKI'})
legend('Location','southeast'); 



 
