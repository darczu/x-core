% Helium thermal conductivity  Rev 14-06-2019
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				      - temperature, [K]
% 2: p				      - pressure, [Pa]
% 3: mode			      - mode, see below - options 
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: k			      - thermal conductivity, [W/m/K]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T                 - 300 K
% p                 - 1e5 Pa
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1:	MHTGR [50]
% 2:	MELCOR [4]
% 3:	MELCOR_simple [4]
% 4:	Wallenius [60]
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T=273.15:100:1273.15; p=6e6;  
% k1 = k_He(T,p,1); k2 = k_He(T,p,2); k3 = k_He(T,p,3);  k4 = k_He(T,p,4); 
% plot(T,k1,T,k2,T,k3,T,k4) 
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main reference 
% see REFERENCES.m file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function k = k_He(T,p,mode)

	if(nargin < 1) 
		T = 300.0;  % Default Temperature 
  	end
  	if(nargin < 2)
    	p = 1e5;    % Default pressure
  	end
  	if (nargin < 3)
		mode = 'MELCOR';  % Default mode
	end

%if or(numel(p)==1,numel(p)==numel(T))
%else
%  error('Only single pressure value allowed');
%end
	if (numel(p)>1) 
  		error('Only single pressure value allowed'); 
	end
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch mode
	case{1,'MHTGR', 'Default'}
		pp = p./1e5;
		for i=1:numel(T)
			%The same as MELCOR
			k(i) = (2.682e-3).*(T(i).^(0.71.*(1.0-(pp).*2e-4))).*(1.0+1.23e-3.*pp);


		end
	case{2,'MELCOR_KTA','MELCOR','KTA','MELCOR1'}
		%Based on MECLOR Refernce Manual
		for i=1:numel(T)
			k(i) = (2.682e-3).*((1.0 + (1.123e-8).*p)).*(T(i).^(0.71)).*(1.0-(2.0e-9).*p);  						%[4] MELCOR 
		end

%There is a small variation of thermal conductivity over the range 0.1 MPa to 10 MPa, our 
%range of interest for HTGRs, but not enough to be significant. MELCOR coding 
%presently allows thermal conductivity as a function of temperature only, so the KTA 
%formula is used at a pressure of 0.1 MPa

	case{3, 'MELCOR_power', 'MELCOR_simple','MELCOR2'}
		for i=1:numel(T)
			k(i) = (2.685e-3).*T(i).^(0.71);   %Alternative Power law fit for MELCOR applications
		end
	
	case{4,'Wallenius'}	
		for i=1:numel(T)
			k(i) = 0.0574 + (3.45e-4).*T(i) - (4.05e-8).*(T(i).^2); %[60]Wallenius book about Transmutation Chapter 6
		end

	otherwise	
		error('Incomplete input.');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



