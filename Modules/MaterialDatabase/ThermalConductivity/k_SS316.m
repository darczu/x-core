% Rev 15-10-2018
% SS316 th. cond.  T [K] and rho [g/cc]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				      - temperature, [K]
% 2: p				      - pressure, [Pa]
% 3: mode			      - mode, see below - options 
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			      - density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T                 - 300 K
% p                 - 1e5 Pa
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1:
% 2
% 3: 
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out = k_SS316(T,option)

if(nargin < 2)
 option ='MELCOR';  %Default 'MELCOR'
end

if(strcmp(option,'MELCOR'))
% Reference - MELCOR Reference Manual [4]
out = k_SS(T);


elseif(strcmp(option,'316L'))	
		% Stainles Steel AISI 316L
		%Ref: A. Kontautas et. al. Nuc Eng. and Des. 239 2009 1267-1274
	T_1 = [
	60+273.15
    100+273.15
	300+273.15	
	];
	k_1 = [
	14.5
	15.3
	18.74
		];


	% Linear inteprolation %vq = interp1(x,y,xi)
	for i=1:numel(T)
		out(i) = interp1(T_1,k_1,T(i),'linear','extrap');
		% No extrapolation is allowed
		%if(T(i)>T_1(end))
		%	error('ERROR(#1): Material temperature above allowed,  273.15<T<5000');
		%elseif (T(i)<T_1(1))
		%	error('ERROR(#2): Material temperature below allowed, 273.15<T<5000');
		%end	
	end



else
	error('no such options');
end  
