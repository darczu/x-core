% Helium specific heat capacity for constant pressure (also for constant V) Rev 14-06-2019
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				      - temperature, [K]
% 2: p				      - pressure, [Pa]
% 3: mode			      - mode, see below - options 
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: Cp			      - Specific Heat Capacity, [J/kg/K]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T                 - 300 K
% p                 - 1e5 Pa
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MHTGR             - [50]
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T=273.15:100:1273.15; p=6e6;  rho1 = rho_He(T,p,2); rho2 = rho_He(T,p,3); plot(T,rho1,T,rho2)
% 
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out = cp_He(T,mode)

    if(nargin < 1)  		T = 300.0;  % Default Temperature 
        end
    if(nargin < 2)
        p = 1e5;    % Default pressure
    end
        if (nargin < 3) 		mode = 'MHTGR';  % Default mode
    end
    if (numel(p)>1) 
        error('Only single pressure value allowed'); 
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
switch mode
    case{1,'MHTGR', 'Default','MHTGR_Cp','Cp'}
        out = 5195; %[50] [J/kg/K]

    case{2,'MHTGR2','MHTGR_Cv','Cv'}
        out = 3117; %[50] [J/kg/K]

    otherwise	
		error('Incomplete input.');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    

