% Helium Dyn Visc   Rev 14-06-2019
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				      - temperature, [K]
% 2: p				      - pressure, [Pa]
% 3: mode			      - mode, see below - options 
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			      - density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T                 - 300 K
% p                 - 1e5 Pa
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NENE              - [NENE article for Krshko]
% MHTGR             - [50]
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T=273.15:100:1273.15; p=6e6;  
% mi1 = mi_He(T,p,1); mi2 = mi_He(T,p,2); 
% plot(T,mi1,T,mi2)
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main reference [50]
% see REFERENCES.m file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function mi = mi_He(T,mode)

    if(nargin < 1) 
		T = 300.0;  % Default Temperature 
    end
    if(nargin < 2)
        p = 1e5;    % Default pressure
    end
    if (nargin < 3)
		mode = 'MHTGR';  % Default mode
    end
    if (numel(p)>1) 
        error('Only single pressure value allowed'); 
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  switch mode
    case{1,'MHTGR', 'Default'}
        mi =    (3.674e-7).*T.^(0.7);   %[kg/m/s]       %[Pa*s] 

    case{2,'MELCOR'}
        %Dynamic Viscosity for Helium - based on MELCOR Power Lw Fit   Dynamic Visc. = [kg/s/m] = [Pa*s]
        mi =    (3.674e-7).*T.^(0.7);   %[kg/m/s]       %[Pa*s] 

    otherwise	
		error('Incomplete input.');
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
