%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Steel (Stainless and Carbon) Young Modulus E  Rev 20-07-2020
% 
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				      - temperature, [K]
% 2: mode			      - mode, see below - options 
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: E			      - Young Modulus, [Pa]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T                 - 300 K
% mode              - 'Default'
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1 'MELCOR' 'SA533B1' 'MELCOR_CS' 		 SA533B1 Carbon Steel			% MELCOR Manual based [4]
% 2 'FOREVER' '15Mo3' 
% 
%
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% E_SS(500)
% E_SS(600.0,2)
% E_SS(linspace(300,700,10),1)
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%function sigmaY = sigmaY_SS(T,mode, flag_validation)
function E = E_SS(T,mode)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(nargin <1)
	T =300.0;  % Default Temperature 
end
if(nargin < 2)
	mode ='MELCOR';  %Default 'Kolev' ; 'Powers'
end
%if(nargin <3)
%	flag_validation = 0;
%end	


%if flag_validation == 1
%	disp('WARNING!!! Input data will be changed automatically')
%end





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch mode
	case {1 'MELCOR' 'SA533B1' 'MELCOR_CS' 'Default'}  %Ref. [4]
		for i=1:numel(T)
		%SC1602(X) = SCX  i.e. SC1602(1) == SC1
			SC1 = 2.0E11;		% Leading multipl factor 
			SC2 = 1800.0;		% Temp. at which E vanishes 
			SC3 = 900.0;		% Temp. at which E is halved 
			SC4 = 6.0; 			% Exponent scaled temp.
			E(i) = SC1.*( ( 1 + (T(i)./SC3).^(SC4)   ).^(-1)   - ( 1 + (SC2./SC3).^(SC4) ).^(-1) );
		end
			% CFT toolbox:
			% = a*( ( 1 + (x/c).^(d)   ).^(-1)   - ( 1 + (b/c).^(d) ).^(-1) )
		
	case {2 , '15Mo3_1', 'FOREVER1'} %
		%	data1 = [
		%	-97.53813180654637, 217.017701059008
		%	19.302330571585884, 212.0846940776016
		%	49.61240867804699, 211.5934402703246
		%	99.4773758854507, 207.09028037028554
		%	149.4175542047811, 205.75980130891037
		%	199.97553808636658, 199.214136739925
		%	250.050021962709, 197.86318876991328
		%	299.9149891701127, 191.590930337716
		%	349.77995637751644, 189.30718495983908
		%	399.6449235849201, 184.1651026973594
		%	449.50989079232386, 180.9924218586955
		%	500.3526024547747, 175.11784508000818
		%	550.2175696621784, 171.94516424134432
		%	600.0825368695821, 163.57484267390154
		%	];	
		%T1= data1(:,1) + 273.15;
		%E1= data1(:,2).*1E6;
		%
		%	for i=1:numel(T)
		%		E(i) = interp1(T1,E1,T(i));
		%	end	
		%end
		
		
		
% 		General model:
%      f(x) = a*( ( 1 + (x/c)^(d)   )^(-1)   - ( 1 + (b/c)^(d) )^(-1) )
% Coefficients (with 95% confidence bounds):
%        a =   3.405e+11  (-1.135e+12, 1.816e+12)
%        b =        2674  (-4845, 1.019e+04)
%        c =        1999  (-3997, 7995)
%        d =       2.016  (0.7325, 3.299)
% 
% Goodness of fit:
%   SSE: 1.753e+19
%   R-square: 0.9951
%   Adjusted R-square: 0.9936
%   RMSE: 1.324e+09

		for i=1:numel(T)
		%SC1602(X) = SCX  i.e. SC1602(1) == SC1
			SC1 = 3.405E11;		% Leading multipl factor 
			SC2 = 2674.0;		% Temp. at which E vanishes 
			SC3 = 1999.0;		% Temp. at which E is halved 
			SC4 = 2.016; 			% Exponent scaled temp.
			E(i) = SC1.*( ( 1 + (T(i)./SC3).^(SC4)   ).^(-1)   - ( 1 + (SC2./SC3).^(SC4) ).^(-1) );
		end

	case {3 , '15Mo3_2', 'FOREVER2'} %		

	
%	General model:
%     f(x) = a*( ( 1 + (x/c)^(d)   )^(-1)   - ( 1 + (b/c)^(d) )^(-1) )
%Coefficients (with 95% confidence bounds):
%       a =   2.195e+11  (1.559e+11, 2.832e+11)
%       b =        3000  (-4713, 1.071e+04)
%       c =        1200  (fixed at bound)
%       d =       3.577  (2.524, 4.631)
%
%Goodness of fit:
%  SSE: 1.821e+20
%  R-square: 0.9491
%  Adjusted R-square: 0.9398
%  RMSE: 4.069e+09

	
		for i=1:numel(T)
		%SC1602(X) = SCX  i.e. SC1602(1) == SC1
			SC1 = 2.195E11;		% Leading multipl factor 
			SC2 = 3000.0;		% Temp. at which E vanishes 
			SC3 = 1200.0;		% Temp. at which E is halved 
			SC4 = 3.577; 			% Exponent scaled temp.
			E(i) = SC1.*( ( 1 + (T(i)./SC3).^(SC4)   ).^(-1)   - ( 1 + (SC2./SC3).^(SC4) ).^(-1) );
		end	


case {4 , '15Mo3_3', 'FOREVER3'} %	
%	General model:
%     f(x) = a*( ( 1 + (x/c)^(d)   )^(-1)   - ( 1 + (b/c)^(d) )^(-1) )
%Coefficients (with 95% confidence bounds):
%       a =   2.138e+11  (-1.568e+12, 1.996e+12)
%       b =        2463  (-1.654e+05, 1.704e+05)
%       c =        1100  (-1984, 4184)
%       d =       4.526  (-3.318, 12.37)
%
%Goodness of fit:
%  SSE: 4.548e+20
%  R-square: 0.8728
%  Adjusted R-square: 0.8346
%  RMSE: 6.744e+09
%
		for i=1:numel(T)
		%SC1602(X) = SCX  i.e. SC1602(1) == SC1
			SC1 =  2.138e+11;		% Leading multipl factor 
			SC2 =   2463;		% Temp. at which E vanishes 
			SC3 = 1100;		% Temp. at which E is halved 
			SC4 = 4.526; 			% Exponent scaled temp.
			E(i) = SC1.*( ( 1 + (T(i)./SC3).^(SC4)   ).^(-1)   - ( 1 + (SC2./SC3).^(SC4) ).^(-1) );
		end	

	
		% More alternative data	- see also Examples 1
% https://steelselector.sij.si/data/pdf/15MO3.pdf  for diameter <60 mm
% https://virgamet.com/16mo3-15mo3-15d3-a516-grade-65-a335-grade-p1-p2-boiler-steel
% http://www.htsteelmill.com/mat-no-1-5415-din-15mo3-aisi-a182-grade-f1.html?gclid=Cj0KCQjwjer4BRCZARIsABK4QeUmAVZoc-nL_C_9Tkx3-O3SqnG11Otzfbl0yHNKJM6M9psuprfbDrUaAnkfEALw_wcB
% https://matmatch.com/materials/minfm32015-din-17243-grade-15mo3?utm_term=&utm_medium=ppc&utm_campaign=%5BSD%5D+Materials+Generic&utm_source=adwords&hsa_grp=61532612146&hsa_cam=918492904&hsa_src=g&hsa_net=adwords&hsa_tgt=aud-421245424252:dsa-19959388920&hsa_ad=311442906172&hsa_mt=b&hsa_kw=&hsa_ver=3&hsa_acc=2813184419&gclid=Cj0KCQjw6uT4BRD5ARIsADwJQ19_6P9SetIpT4pHUC5vWD2Vkl4b0TyYNoHHrWOoJAo6no3ldKrTYGsaApOxEALw_wcB

	% http://www.htsteelmill.com/mat-no-1-5415-din-15mo3-aisi-a182-grade-f1.html?gclid=Cj0KCQjwjer4BRCZARIsABK4QeUmAVZoc-nL_C_9Tkx3-O3SqnG11Otzfbl0yHNKJM6M9psuprfbDrUaAnkfEALw_wcB
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	otherwise
		error('no such options');
	end  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%