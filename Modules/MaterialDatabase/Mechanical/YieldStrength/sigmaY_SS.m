%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Steel (Stainless and Carbon) Yield Strength  Rev 20-07-2020
% Insp[ired on MELCOR SC1603 – Vessel Steel Yield Stress Parameters - see [4]
% MELCOR Default setup for Vessel Steel is "probably" based on LHF experiment. It was SA533B1 Carbon Steel
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				      - temperature, [K]
% 2: mode			      - mode, see below - options 
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: sigmaY			      - yield strenght, [Pa]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T                 - 300 K
% mode              - 'Default'
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1 'MELCOR' 'SA533B1' 'MELCOR_CS' 		 SA533B1 Carbon Steel			% MELCOR Manual based [4]
% 2 'FOREVER' '15Mo3' 
% 
%
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% sigmaY_SS(500)
% sigmaY_SS(600.0,2)
% sigmaY_SS(linspace(300,700,10),1)
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%function sigmaY = sigmaY_SS(T,mode, flag_validation)
function sigmaY = sigmaY_SS(T,mode)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(nargin <1)
	T =300.0;  % Default Temperature 
end
if(nargin < 2)
	mode ='MELCOR';  %Default 'Kolev' ; 'Powers'
end
%if(nargin <3)
%	flag_validation = 0;
%end	


%if flag_validation == 1
%	disp('WARNING!!! Input data will be changed automatically')
%run Examples1_15Mo3.m
%end

% 15Mo3 Data		 - ref. [61]
% Data taken from plot
%Only EXP
%data2_1 = [
%300.8403361	265.3179191
%473.7094838	254.9132948
%570.9483794	204.6242775
%621.3685474	194.2196532
%675.3901561	178.6127168
%718.607443	157.8034682
%765.4261705	145.6647399
%]; 
%
%		
%		T2= data2_1(:,1);
%		sigma2= data2_1(:,2).*1E6;;
%
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch mode
	case {1 'MELCOR' 'SA533B1' 'MELCOR_CS' 'Default'}  %Ref. [4]
	
		for i=1:numel(T)
		%SC1604(X) = SCX  i.e. SC1604(1) == SC1
			SC1 = 4.E8;  %Leading multiplicative constant
			SC2 = 1700.0; % Temperature at which yield stress vanishes
			SC3 = 900.0; %Temperature at which yield stress is approximately halved
			SC4 = 6.0; % Exponent of scaled temperatures
			
			sigmaY(i) = SC1.*( ( 1 + (T(i)./SC3).^(SC4)   ).^(-1)   - ( 1 + (SC2./SC3).^(SC4) ).^(-1) );
			% CFT toolbox:
			% = a*( ( 1 + (x/c).^(d)   ).^(-1)   - ( 1 + (b/c).^(d) ).^(-1) )

		end
	

		
		
	case {2 , 'SA533B1_Data', 'FOREVER1'  } %Ref. [61]

	
			data1 = [
				286.8712836	358.0813936
				310.9751049	360.9697318
				340.9680524	362.8039319
				375.9087416	364.8700425
				404.2086566	366.3950288
				436.0259926	368.3065324
				466.6319346	368.448841
				487.167252	364.0899803
				510.5972658	351.7026873
				534.8446057	340.5522496
				563.1785759	327.0897357
				574.2124777	320.8815656
				601.0480168	312.4793828
				628.0197769	301.8536715
				656.353747	290.6376429
				684.6877172	279.4216144
				711.6594773	268.7959031
				733.999723	259.9411437
				758.791947	247.4351626
				776.5006783	231.4878502
				788.7605693	218.1466794
				801.0204602	205.0416355
				812.0543621	192.1530413
				826.6436323	172.5381095
				852.5120022	155.7079761
				869.6758495	141.8776853
				885.6137078	128.989091
				904.3538268	110.2160641
				913.3210613	104.6139062
				940.510775	95.89852726
				967.2645815	84.0265906
				994.7267372	71.03961016
				1004.53465	64.97544162
				1035.184377	59.52842296
				1062.5648	51.69032114
				1091.674183	42.2502899
				1115.223951	39.86523506
				1145.216899	33.84962074
				1170.836465	27.46030417
				1196.197612	28.14322025
				1224.293195	26.32307526
				1252.53473	24.74186823
				1283.140672	22.78117151
				1311.382206	21.19996448
				1341.988148	19.09168843
				1369.8794	15.77115366
				1396.85116	15.12812947
				1423.332525	12.3620713
				1454.584101	7.714664252
				1474.868648	6.534029667
			];
		
		T1= data1(:,1);
		sigma1= data1(:,2).*1E6;
			
		%if flag_validation ==1
		%	T = T1;
		%	sigmaY = sigma1;		
		
		%else
			for i=1:numel(T)
				sigmaY(i) = interp1(T1,sigma1,T(i));
			end	
		%end
	
% More alternative data	- see also Examples 1
	%https://steelselector.sij.si/data/pdf/15MO3.pdf  for diameter <60 mm
% https://virgamet.com/16mo3-15mo3-15d3-a516-grade-65-a335-grade-p1-p2-boiler-steel
	
	%https://matmatch.com/materials/minfm32015-din-17243-grade-15mo3?utm_term=&utm_medium=ppc&utm_campaign=%5BSD%5D+Materials+Generic&utm_source=adwords&hsa_grp=61532612146&hsa_cam=918492904&hsa_src=g&hsa_net=adwords&hsa_tgt=aud-421245424252:dsa-19959388920&hsa_ad=311442906172&hsa_mt=b&hsa_kw=&hsa_ver=3&hsa_acc=2813184419&gclid=Cj0KCQjw6uT4BRD5ARIsADwJQ19_6P9SetIpT4pHUC5vWD2Vkl4b0TyYNoHHrWOoJAo6no3ldKrTYGsaApOxEALw_wcB
	case {3 , '15Mo3_Data', '15Mo3', 'FOREVER2'}	 %Ref. [61]
			data1 = [ 
			303.2688878	265.2003342
			343.3178649	262.4991853
			389.0881244	259.9411437
			424.0288136	257.5798746
			458.5608398	255.2186054
			478.8918256	253.5952328
			508.0090666	237.5090866
			533.7548376	225.1124235
			569.0633235	207.0487143
			589.3330099	204.0577734
			622.0260524	195.5965589
			645.7285082	186.545027
			671.0656162	176.7064055
			693.7464144	168.1468048
			722.5571582	156.6356176
			755.4136659	148.7253658
			775.2746892	146.6002236
			];
	
		
		T1= data1(:,1);
		sigma1= data1(:,2).*1E6;
			
		%if flag_validation ==1
		%	T = T1;
		%	sigmaY = sigma1;		
		%
		%else
			for i=1:numel(T)
				sigmaY(i) = interp1(T1,sigma1,T(i));
			end	
		%end
		
	case {4 '15Mo3_CFT_Fit' } 
	
		for i=1:numel(T)
		%SC1604(X) = SCX  i.e. SC1604(1) == SC1
			
			%The Best Fit with CFT
			SC1 = 2.77e+08 ;  %Leading multiplicative constant
			SC2 =  3000.0; % Temperature at which yield stress vanishes
			SC3 =  780.2  ; %Temperature at which yield stress is approximately halved
			SC4 = 3.994  ; % Exponent of scaled temperatures
		
			sigmaY(i) = SC1.*( ( 1 + (T(i)./SC3).^(SC4)   ).^(-1)   - ( 1 + (SC2./SC3).^(SC4) ).^(-1) );
			% CFT toolbox:
			% = a*( ( 1 + (x/c).^(d)   ).^(-1)   - ( 1 + (b/c).^(d) ).^(-1) )
		end	
			

	case {5 '15Mo3_Manual_Fit' }  %It is in fact CFT fit
		
				for i=1:numel(T)
		%SC1604(X) = SCX  i.e. SC1604(1) == SC1
			
			%The Best Fit with CFT
			SC1 = 3e+08 ;  %Leading multiplicative constant
			SC2 = 1525.0; % Temperature at which yield stress vanishes
			SC3 = 809.9; %Temperature at which yield stress is approximately halved
			SC4 = 3.852; % Exponent of scaled temperatures
		
			sigmaY(i) = SC1.*( ( 1 + (T(i)./SC3).^(SC4)   ).^(-1)   - ( 1 + (SC2./SC3).^(SC4) ).^(-1) );
			% CFT toolbox:
			% = a*( ( 1 + (x/c).^(d)   ).^(-1)   - ( 1 + (b/c).^(d) ).^(-1) )
                end
case {6, '15Mo3_Manual_Fit2' }  %It is in fact CFT fit
		
				for i=1:numel(T)
		%SC1604(X) = SCX  i.e. SC1604(1) == SC1
			
			%The Best Fit with CFT
			SC1 = 3.087e+08 ;  %Leading multiplicative constant
			SC2 = 1400.0; % Temperature at which yield stress vanishes
			SC3 = 800.0; %Temperature at which yield stress is approximately halved
			SC4 = 4.0; % Exponent of scaled temperatures
		
			sigmaY(i) = SC1.*( ( 1 + (T(i)./SC3).^(SC4)   ).^(-1)   - ( 1 + (SC2./SC3).^(SC4) ).^(-1) );
			% CFT toolbox:
			% = a*( ( 1 + (x/c).^(d)   ).^(-1)   - ( 1 + (b/c).^(d) ).^(-1) )		
                end
                
case {7, '15Mo3_Manual_Fit3' }  %It is in fact CFT fit
		for i=1:numel(T)
				%SC1604(X) = SCX  i.e. SC1604(1) == SC1
			
			%The Best Fit with CFT
			SC1 = 3.0e+08 ;  %Leading multiplicative constant
			SC2 = 1400.0; % Temperature at which yield stress vanishes
			SC3 = 800.0; %Temperature at which yield stress is approximately halved
			SC4 = 4.0; % Exponent of scaled temperatures
		
			sigmaY(i) = SC1.*( ( 1 + (T(i)./SC3).^(SC4)   ).^(-1)   - ( 1 + (SC2./SC3).^(SC4) ).^(-1) );
			% CFT toolbox:
			% = a*( ( 1 + (x/c).^(d)   ).^(-1)   - ( 1 + (b/c).^(d) ).^(-1) )		
        end
case {8, 'All_SQUAL_EXP_Data' }  

		% General model:
		%      f(x) = a*( ( 1 + (x/c)^(d)   )^(-1)   - ( 1 + (b/c)^(d) )^(-1) )
		% Coefficients (with 95% confidence bounds):
		%        a =       3e+08  (-1.023e+09, 1.623e+09)
		%        b =        1404  (-5858, 8665)
		%        c =       805.6  (-919.6, 2531)
		%        d =       3.326  (-6.127, 12.78)
		% 
		% Goodness of fit:
		%   SSE: 6.256e+15
		%   R-square: 0.8422
		%   Adjusted R-square: 0.8216
		%   RMSE: 1.649e+07
	for i=1:numel(T)
		SC1 = 3.0e+08 ;  %Leading multiplicative constant
		SC2 = 1404; % Temperature at which yield stress vanishes
		SC3 =  805.6; %Temperature at which yield stress is approximately halved
		SC4 = 3.326 ; % Exponent of scaled temperatures
		sigmaY(i) = SC1.*( ( 1 + (T(i)./SC3).^(SC4)   ).^(-1)   - ( 1 + (SC2./SC3).^(SC4) ).^(-1) );
	end
		
case {9, 'All_EXP_Data'}
		
		%	General model:
		%     f(x) = a*( ( 1 + (x/c)^(d)   )^(-1)   - ( 1 + (b/c)^(d) )^(-1) )
		%Coefficients (with 95% confidence bounds):
		%       a =       3e+08  (-6.121e+08, 1.212e+09)
		%       b =        1400  (-4306, 7106)
		%       c =         800  (-362, 1962)
		%       d =       3.495  (-2.987, 9.977)
		%
		%Goodness of fit:
		%  SSE: 9.118e+15
		%  R-square: 0.8296
		%  Adjusted R-square: 0.8126
		%  RMSE: 1.743e+07
		%
	for i=1:numel(T)
		SC1 = 3.0e+08 ;  %Leading multiplicative constant
		SC2 = 1400; % Temperature at which yield stress vanishes
		SC3 =  805.6; %Temperature at which yield stress is approximately halved
		SC4 = 3.495 ; % Exponent of scaled temperatures
		sigmaY(i) = SC1.*( ( 1 + (T(i)./SC3).^(SC4)   ).^(-1)   - ( 1 + (SC2./SC3).^(SC4) ).^(-1) );
	end
		
		
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	otherwise
		error('no such options');
	end  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%