%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 07 and 08 2020 testing of LMP parameters for Forover
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RAW DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%
% http://www.htsteelmill.com/mat-no-1-5415-din-15mo3-aisi-a182-grade-f1.html?gclid=Cj0KCQjwjer4BRCZARIsABK4QeUmAVZoc-nL_C_9Tkx3-O3SqnG11Otzfbl0yHNKJM6M9psuprfbDrUaAnkfEALw_wcB
% 	https://matmatch.com/materials/minfm32015-din-17243-grade-15mo3?utm_term=&utm_medium=ppc&utm_campaign=%5BSD%5D+Materials+Generic&utm_source=adwords&hsa_grp=61532612146&hsa_cam=918492904&hsa_src=g&hsa_net=adwords&hsa_tgt=aud-421245424252:dsa-19959388920&hsa_ad=311442906172&hsa_mt=b&hsa_kw=&hsa_ver=3&hsa_acc=2813184419&gclid=Cj0KCQjw6uT4BRD5ARIsADwJQ19_6P9SetIpT4pHUC5vWD2Vkl4b0TyYNoHHrWOoJAo6no3ldKrTYGsaApOxEALw_wcB	
% More data: http://www.iso-iran.ir/standards/din/DIN%2017155-1983%20%2C%20Creep%20Resistant%20Steel%20Plate%20and%20Strip.pdf
%
% 0.2 % Proof Stress (N/mm2) vs. Temperature
% 100oC	150oC	200oC	250oC	300oC	350oC	400oC	450oC	500oC
% 264	245	225	205	180	170	160	155	150
% 250	230	210	195	170	160	150	145	140
% 240	220	200	185	160	155	145	140	135
%
% 1 % Creep Limit (N/mm2) vs. Temperature (oC) 
% Hours	   450oC	460oC	480oC	500oC	520oC	540oC	560oC
% 10 000	298	273	222	171	125	-	-
% 100 000	245	209	143	93	59	-	-
% 200 000	228	189	121	75	45	-	-
%
% Creep Rapture Strength (N/mm2) vs. Temperature (oC)
% Hours	450oC	460oC	480oC	500oC	520oC	540oC	560oC
% 10000	    370	348	304	239	179	129	91
% 100000	285	251	190	137	94	61	40
% 200000	260	226	167	115	76	50	32
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ATTEMPT TO FIT GLOBAL MODEL 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OLD MODEL
%General model:
%     f(x,y) = (b + a*log10(x) )/(log10(y)-C)
%Coefficients (with 95% confidence bounds):
%       C =      -14.19  (-17.89, -10.49)
%       a =       -2959  (-3396, -2523)
%       b =   4.157e+04  (3.534e+04, 4.78e+04)
%Goodness of fit:
%  SSE: 704.6
%  R-square: 0.9767
%  Adjusted R-square: 0.9741
%  RMSE: 6.257
%
% P = b + a*log10(sigma)
% tr = 10^ (P/T - C) 
% log10(tr) = P/T - C  -> 	log10(tr) + C = P/T  -		> P = T*(log10(tr)+C)
% b + a*log10(sigma) = T*(log10(tr)+C)
% T =  b + a*log10(sigma)/(log10(tr)+C)
% Musi byc (b + a*log10(x) )/(log10(y)+C)
% (b + a*log10(x) )/(log10(y)+C)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TESTS ANALITICAL FOR MODEL RESULTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc; close all; clear;

dP = (2.5 - 0.1)*1e6;
Ri = 0.188
Ro = 0.2030
sigma_e = (dP+2500*9.81*0.188)*(Ri^2)/(Ro^2-Ri^2)
sigma=sigma_e
 T=1000
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %Default MELCOR model
 a1 = -4.725E3
 b1 = 4.812E4
 C1 = 7.042
 P1 = b1 + a1*log10(sigma)
 tr1 = 10^(P1/T - C1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OLD
 a2 = -2959
 b2 = 4.157e+04
 C2 = 14.19
P2 = b2 + a2*log10(sigma)
tr2 = 10^(P2/T - C2)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NEW 2 - NEW MODEL
% General model:
%      f(x,y) = (b + a*log10(x) )/(log10(y)+C)
% Coefficients (with 95% confidence bounds):
%        C =        21.8  (18.42, 25.18)
%        a =       -4000  (fixed at bound)
%        b =   5.591e+04  (5.329e+04, 5.854e+04)
% 
% Goodness of fit:
%   SSE: 1549
%   R-square: 0.9488
%   Adjusted R-square: 0.9461
%   RMSE: 9.03
% 
% NEW 2
a3 = -4000
b3 = 5.591e4
C3 = 21.8
P3 = b3 + a3*log10(sigma)
tr3 = 10^(P3/T - C3)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ONLY 10k hours model
% General model:
%      f(x,y) = (b + a*log10(x) )/(log10(y)+C)
% Coefficients (with 95% confidence bounds):
%        C =       17.03  (-5.1e+07, 5.1e+07)
%        a =       -4317  (-8.953e+09, 8.953e+09)
%        b =     5.5e+04  (-1.141e+11, 1.141e+11)
% 
% Goodness of fit:
%   SSE: 310.5
%   R-square: 0.9692
%   Adjusted R-square: 0.9538
%   RMSE: 8.81
a4 =  -4317
b4 =   5.5e+04
C4 =    17.03

% estimation of displacement
e_pl1 = 0.18*28*3600/tr1
e_pl2 = 0.18*28*3600/tr2
e_pl3 = 0.18*28*3600/tr3










%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DATA FOR 15Mo3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Time = [
10000
10000
10000
10000
10000
10000
10000
100000
100000
100000
100000
100000
100000
100000
200000
200000
200000
200000
200000
200000
200000
];
Time = Time*3600;
Time0 = reshape(Time,7,3)

Time1 = Time(1:7);
Time2 = Time(8:14);
Time3 = Time(15:21);
Time560 = [Time1(end), Time2(end), Time3(end) ] ;

Time10k = Time0(:,1);

Temp = [
450
460
480
500
520
540
560
450
460
480
500
520
540
560
450
460
480
500
520
540
560
];
Temp = Temp + 273.15;
Temp0 = reshape(Temp,7,3)

Temp1 = Temp(1:7);
Temp2 = Temp(8:14);
Temp3 = Temp(15:21);
Temp560 = [Temp1(end), Temp2(end), Temp3(end) ] ;

Temp10k = Temp0(:,1)

Sigma= [
370	
348	
304	
239	
179	
129	
91
285	
251	
190	
137	
94	
61	
40
260	
226	
167	
115	
76	
50	
32
];
Sigma = Sigma*1e6;
Sigma0 = reshape(Sigma,7,3)

Sigma1 = Sigma(1:7);
Sigma2 = Sigma(8:14);
Sigma3 = Sigma(15:21);
Sigma560 = [Sigma1(end), Sigma2(end), Sigma3(end) ] ;

Sigma10k = Sigma0(:,1)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 560 degC model
% Sigma560
% Temp560
% Time560
%
%T560 = Temp560(1,1);
%LMP560 = T560*(log10())
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMPARISON OF GENERAL MODEL WITH DATA
% LMP vs sigma
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%P3 = b3 + a3*log10(sigma)
%tr3 = 10^(P3/T - C3)
SIG 	= Sigma560   %560 degC
LMP_1 = a1*log10(SIG) + b1
LMP_2 = a2*log10(SIG) + b2
LMP_3 = a3*log10(SIG) + b3

LMP_4 = a4*log10(Sigma10k) + b4





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ATTEMPT TO FIT SINGLE CURVES - like in Mosunova's comparison
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% In Mosunovas fit there are two curves for different temperatures
% For 15Mo3 there is not data for high temperatures








%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMPARISON WITH OTHER DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% LMP Data from Mosunova presentation
% LMP*1e-3 vs log10(Sigma0) in MPa
LMP_Mosunova_LowT_fit 	= csvread('..\data\SA533B1\LMP_Mosunova_LowT_fit.csv');
LMP_Mosunova_HighT_fit 	= csvread('..\data\SA533B1\LMP_Mosunova_HighT_fit.csv');
x1 = LMP_Mosunova_LowT_fit(:,1);
y1 = LMP_Mosunova_LowT_fit(:,2);

x2 = LMP_Mosunova_HighT_fit(:,1);
y2 = LMP_Mosunova_HighT_fit(:,2);

figure(1)
hold on; grid on; box on;figure(1)

plot(x1,y1,'--k',x2,y2,'-k');

plot(log10(SIG), LMP_1.*1e-3,'x')
plot(log10(SIG), LMP_2.*1e-3,'o')
plot(log10(SIG), LMP_3.*1e-3,'<')
plot(log10(Sigma10k), LMP_4.*1e-3,'*')


legend('SA533B1 Low T<1050 fit', 'SA533B1 High T>1050 fit');
%ylabel('sigma yield strength, [Pa]');
%xlabel('Temp. [K]');


