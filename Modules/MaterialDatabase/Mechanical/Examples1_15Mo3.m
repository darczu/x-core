clear; clc; close all;
% SS533B1 Data  - ref. [61]
% Data taken from plot, Forver report
			data1 = [
				286.8712836	358.0813936
				310.9751049	360.9697318
				340.9680524	362.8039319
				375.9087416	364.8700425
				404.2086566	366.3950288
				436.0259926	368.3065324
				466.6319346	368.448841
				487.167252	364.0899803
				510.5972658	351.7026873
				534.8446057	340.5522496
				563.1785759	327.0897357
				574.2124777	320.8815656
				601.0480168	312.4793828
				628.0197769	301.8536715
				656.353747	290.6376429
				684.6877172	279.4216144
				711.6594773	268.7959031
				733.999723	259.9411437
				758.791947	247.4351626
				776.5006783	231.4878502
				788.7605693	218.1466794
				801.0204602	205.0416355
				812.0543621	192.1530413
				826.6436323	172.5381095
				852.5120022	155.7079761
				869.6758495	141.8776853
				885.6137078	128.989091
				904.3538268	110.2160641
				913.3210613	104.6139062
				940.510775	95.89852726
				967.2645815	84.0265906
				994.7267372	71.03961016
				1004.53465	64.97544162
				1035.184377	59.52842296
				1062.5648	51.69032114
				1091.674183	42.2502899
				1115.223951	39.86523506
				1145.216899	33.84962074
				1170.836465	27.46030417
				1196.197612	28.14322025
				1224.293195	26.32307526
				1252.53473	24.74186823
				1283.140672	22.78117151
				1311.382206	21.19996448
				1341.988148	19.09168843
				1369.8794	15.77115366
				1396.85116	15.12812947
				1423.332525	12.3620713
				1454.584101	7.714664252
				1474.868648	6.534029667
			];
		T1= data1(:,1);
		sigma1= data1(:,2).*1E6;;

		% 15Mo3 Data		 - ref. [61]
		% Data taken from plot, Foerver Report
		%Only EXPerimental data - previous it was fitted curve
		data2_1 = [
		300.8403361	265.3179191
		473.7094838	254.9132948
		570.9483794	204.6242775
		621.3685474	194.2196532
		675.3901561	178.6127168
		718.607443	157.8034682
		765.4261705	145.6647399
		]; 
	
		data2 = [ 
			303.2688878	265.2003342
			343.3178649	262.4991853
			389.0881244	259.9411437
			424.0288136	257.5798746
			458.5608398	255.2186054
			478.8918256	253.5952328
			508.0090666	237.5090866
			533.7548376	225.1124235
			569.0633235	207.0487143
			589.3330099	204.0577734
			622.0260524	195.5965589
			645.7285082	186.545027
			671.0656162	176.7064055
			693.7464144	168.1468048
			722.5571582	156.6356176
			755.4136659	148.7253658
			775.2746892	146.6002236
			];	
		T2= data2_1(:,1);
		sigma2= data2_1(:,2).*1E6;;
			
%https://steelselector.sij.si/data/pdf/15MO3.pdf  for diameter <60 mm
%https://virgamet.com/16mo3-15mo3-15d3-a516-grade-65-a335-grade-p1-p2-boiler-steel 
%https://matmatch.com/materials/minfm31520-din-17175-en-10216-2-grade-st-35-8-normalized-or-normalized-formed-n-?utm_term=&utm_medium=ppc&utm_campaign=%5BSD%5D+Materials+Generic&utm_source=adwords&hsa_grp=61532612146&hsa_cam=918492904&hsa_src=g&hsa_net=adwords&hsa_tgt=aud-421245424252:dsa-19959388920&hsa_ad=311442906172&hsa_mt=b&hsa_kw=&hsa_ver=3&hsa_acc=2813184419&gclid=Cj0KCQjw6uT4BRD5ARIsADwJQ1_WzoGv0nOuAOM7KvjwSZ7-74ZZ1Snx_eS7_068mP2G1E66hsvvs98aAoW5EALw_wcB
%xxhttps://matmatch.com/materials/minfm32015-din-17243-grade-15mo3?utm_term=&utm_medium=ppc&utm_campaign=%5BSD%5D+Materials+Generic&utm_source=adwords&hsa_grp=61532612146&hsa_cam=918492904&hsa_src=g&hsa_net=adwords&hsa_tgt=aud-421245424252:dsa-19959388920&hsa_ad=311442906172&hsa_mt=b&hsa_kw=&hsa_ver=3&hsa_acc=2813184419&gclid=Cj0KCQjw6uT4BRD5ARIsADwJQ19_6P9SetIpT4pHUC5vWD2Vkl4b0TyYNoHHrWOoJAo6no3ldKrTYGsaApOxEALw_wcB
%http://www.htsteelmill.com/mat-no-1-5415-din-15mo3-aisi-a182-grade-f1.html?gclid=Cj0KCQjwjer4BRCZARIsABK4QeUmAVZoc-nL_C_9Tkx3-O3SqnG11Otzfbl0yHNKJM6M9psuprfbDrUaAnkfEALw_wcB

% 0.2% Proff Stress vs Temp  for diameter <60 mm/ 61-90 mm / 91-150 mm
% Temp
% Mudoulus of elascitiy 210e3 [N/mm2]
% Density 7.85 g/cm3
% Brand Name Ravne Mat. No. DIN EN AISI/SAE
% SIQUAL 5415 15MO3 1.5415 15Mo3 † 16Mo3 A182 Grade F1

% SIQUAL 5415 Steel
data2_2 = [
100+273.15  264 250 240
150+273.15	245 230 220
200+273.15	225 210 200
250+273.15	205 195 185
300+273.15	180	170 160
350+273.15	170 160 155
400+273.15	160 150 145
450+273.15	155 145 140
500+273.15	150 140 135
]			

T2_2 = data2_2(:,1);
sigma2_2 = data2_2(:,2).*1E6;
sigma2_3 = data2_2(:,3).*1E6;			
sigma2_4 = data2_2(:,4).*1E6;

data_SQUAL_all = [T2_2, sigma2_2; T2_2, sigma2_3; T2_2, sigma2_4];
T_SQUAL_all = data_SQUAL_all(:,1);
sigma_SQUAL_all = data_SQUAL_all(:,2);

data_all = [T2_2, sigma2_2; T2_2, sigma2_3; T2_2, sigma2_4; T2, sigma2]  %added report exp data
T_all = data_all(:,1);
sigma_all = data_all(:,2);



			
%%%%%%%%%%%%%%%%%%%%%%%%%%% YOUNGUS MODULUS %%%%%%%%%%%%%%%%			
% Data Exp
		data1 = [
			-97.53813180654637, 217.017701059008
			19.302330571585884, 212.0846940776016
			49.61240867804699, 211.5934402703246
			99.4773758854507, 207.09028037028554
			149.4175542047811, 205.75980130891037
			199.97553808636658, 199.214136739925
			250.050021962709, 197.86318876991328
			299.9149891701127, 191.590930337716
			349.77995637751644, 189.30718495983908
			399.6449235849201, 184.1651026973594
			449.50989079232386, 180.9924218586955
			500.3526024547747, 175.11784508000818
			550.2175696621784, 171.94516424134432
			600.0825368695821, 163.57484267390154
			];
	
		T_EXP_E1= data1(:,1) + 273.15;
		E1_EXP= data1(:,2).*1E9;		
			
			
		
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
T = linspace(300,1700,100);
			
figure(2)
hold on; grid on; box on;

Emelcor = E_SS(T, 1);
E1 = E_SS(T, 2);		
E2 = E_SS(T, 3);		
E3 = E_SS(T, 4);	
			
			
	plot(T_EXP_E1, E1_EXP,'ko');		
	plot(T, Emelcor,'b-');				
	plot(T, E1,'r-');				
	plot(T, E2,'c-');		
	plot(T, E3,'m-');
		
			legend('Exp 15Mo3', 'Melcor Default', 'Fit 15Mo3', 'Fit2 15Mo3', 'Fit3 15Mo3');
ylabel('Elastic modulus E, [Pa]');
xlabel('Temp. [K]');		
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
T = linspace(300,1700,100);
			
sigma3 = sigmaY_SS(T,'MELCOR');
sigma4 = sigmaY_SS(T,'15Mo3_CFT_Fit');
sigma5 = sigmaY_SS(T,'15Mo3_Manual_Fit');
sigma6 = sigmaY_SS(T,6);
sigma7 = sigmaY_SS(T,7);

sigma8 = sigmaY_SS(T,8);
sigma9 = sigmaY_SS(T,9);


figure(1)
hold on; grid on; box on;

plot(T, sigma3,'bo-');
plot(T1, sigma1, 'sk');
plot(T2, sigma2, '*r');
plot(T, sigma4, '-r');
plot(T, sigma5, '--m');
plot(T, sigma6, '--k');
plot(T, sigma7, '--c');
plot(T2_2,sigma2_2,'<g')
plot(T2_2,sigma2_3,'*g')
plot(T2_2,sigma2_4,'.g')
plot(T,sigma8,'r-o')
plot(T,sigma9,'y-')

ylabel('sigma yield strength, [Pa]');
xlabel('Temp. [K]');
legend('MELCOR Default', 'SA533B1 Data Report', '15Mo3 Data Report', '15Mo3 CFT Fit' ,'Manual Fit', 'Manual Fit 2',' Manual Fit 3 ', 'SIQUAL Data', 'SIQUAL Data', 'SIQUAL Data', 'Fit all SIQUAL Data', 'Fit all EXP Data')

