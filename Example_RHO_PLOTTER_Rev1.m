% TO BE REVISED !!!!!! IT USES OLD FUNCTIONS DEFINITIONS
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rev 01-10-2018
% Script to test density calculation module
% EXAMPLE APPLICATION OF DIFFERENT FUNCTIONS
clear; clc; close all;

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load initial data
run INPUT_SETUP.m  %initial setup

addpath('./Database/Density/FIGS/');  %Add MAT folder
folder = './Database/Density/FIGS/';
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
x_1 = linspace(0.0,1.0,100)';
name = {'ZrO2+UO2 ', 'Fe+U '};
namex = {'U', 'ZrO2'};
model1 = 'Hull';
model2 = 'AtomFrac';
model3 = 'MassFrac';

x_i = [x_1, 1.0-x_1];
Temp= [2000, 2300, 2600, 2900, 3200];
%Temp = [2600];
%T = 2600;   %Mixture temperature
 figure(1)
 list = {''};
 k=0;
 legendmatrix=cell(2*numel(Temp),1);
 %%
 rho = [];
for j=1:numel(Temp)
    rho = []; rhoo = []; rho1 =[]; rho2 =[]; rho_i=[]; rho_i2=[];
    rhoo3 =[]; rhoo4=[];
    T = Temp(j);
    for i=1:numel(x_1)
        rho_i = [ rho_ZRO2(T), rho_UO2(T,'Powers')];
        rho_i2 = [ rho_U(T), rho_FE(T)];
        rho1 = rho_mix(x_i(i,:),rho_i,model1);
        rho2 = rho_mix(x_i(i,:),rho_i2,model1);
        rho = [rho; rho1];
        rhoo =[rhoo; rho2];
   end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Pure material densities:
    T
    rho_pureZrO2 = [rho_ZRO2(T)]
    rho_pureUO2 = [rho_UO2(T,'Powers',[])]
    rho_pureFe = [rho_FE(T)]
    rho_pureU = [rho_U(T)]
    
    plot(x_1,rho,'-.','LineWidth',1)
	hold on;
    plot(x_1,rhoo,'-','LineWidth',1)
    
    
 grid on; box on;
    legendmatrix{k+j}   = strcat([name{1}, num2str(T),' K ', model1] );
    legendmatrix{k+j+1} = strcat([name{2}, num2str(T),' K ', model1] ); 
  %  legendmatrix{k+j+2} = strcat([name{1}, num2str(T),' K ', model2] );
  %  legendmatrix{k+j+3} = strcat([name{1}, num2str(T),' K ', model3] );
    k=k+1;
end


legend(legendmatrix,'Location','NorthWest');


%legend(['ZrO2 + UO2 ', num2str(T),' K' ], ['Fe+U ', num2str(T),' K']);

%legendmat = cell(n,1)
%for k=1:length(variable)
%    legendmatrix{k} = strcat('text',num2str(variable(k));
% end
%    legend(legendmat)

xlabel (['Mole Fraction of ', strcat( [namex{1},' or ',namex{2}] )]);
ylabel('Mixture density [g/cc]');
%title(['Mixture : ', num2str(T),' K']);
%ylim([0.0 18.0])


filename = 'U_and_ZRO2_mix';
savefig([folder, filename]);
print([folder, filename],'-dtiff');

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%UO2, PuO2, MOX test

T = [];
porosity = 0.0;
T = linspace(300,4000,100);
%T2 = linspace(3000,4500,100);
rho_1 = rho_UO2(T,'Carbajo',porosity);
rho_2 = rho_UO2(T,'Powers',porosity);
rho_3 = rho_PuO2(T,porosity);
rho_4 = rho_U(T);
%rho_5 = rho_UO2(T2);

y = linspace(0.2,0.8,4);
for i=1:numel(y)
	rho_6(i,:) = rho_MOX(T,y(i));
end

figure(2)
hold on; grid on; box on;
ylabel('Mixture density [g/cc]');
xlabel('Temperature, [K]');

plot(T,rho_1,'-.','LineWidth',2);
plot(T,rho_2,'-.','LineWidth',2);
plot(T,rho_3,'-.','LineWidth',2);
%plot(T,rho_4,'-.','LineWidth',2);
%plot(T2,rho_5,'-.','LineWidth',2);

%legend('UO2 Carbajo','UO2 Powers','PuO2 Carbajo');
%first = {'UO2 Carbajo','UO2 Powers','PuO2 Carbajo','U Metallic', 'UO2 liquid'};
first = {'UO2 Carbajo','UO2 Powers','PuO2 Carbajo'}
%list = {''};
 legendmatrix=cell(numel(y)+3,1);
 k = 3;
legendmatrix{1} = strcat([first{1}, '; p=', num2str(porosity)]);
legendmatrix{2} = strcat([first{2}, '; p=', num2str(porosity)]);
legendmatrix{3} = strcat([first{3}, '; p=', num2str(porosity)]);
%legendmatrix{4} = strcat([first{4} ]);
%legendmatrix{5} = strcat([first{5} ]);
  
for k=1:numel(y)
    legendmatrix{k+3}   = strcat([' MOX Carbajol y_{Pu}= ', num2str(y(k)), '; p=' num2str(porosity) ] );
    plot(T,rho_6(k,:),'-','LineWidth',2);
end

legend(legendmatrix);

%Uranium
figure(3)
%first = {'UO2 Carbajo','UO2 Powers','PuO2 Carbajo','U Metallic', 'UO2 liquid'};
first = {'UO2 Carbajo','UO2 Powers','PuO2 Carbajo','U Metallic'};
hold on; grid on; box on; ylabel('Density [g/cc]'); xlabel('Temperature, [K]');
plot(T,rho_1,'-.','LineWidth',2);
plot(T,rho_2,'-.','LineWidth',2);
plot(T,rho_3,'-.','LineWidth',2);
plot(T,rho_4,'-.','LineWidth',2);
%plot(T2,rho_5,'-.','LineWidth',2);
legend(first);
%for i=1:N
%     Legend1{i} =  ['FA0', num2str(i)];
%end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%OTHER MATERIALS
figure(4)
hold on; grid on; box on;
ylabel('Density [g/cc]'); xlabel('Temperature, [K]');
T = []; rho=[]; T = linspace(600,3000,100);

N = 5; leg = cell(N,1);

rho(1,:) = rho_U(T);
leg{1} = ['Uranium metallic'];

rho(2,:) = rho_ZR(T);
leg{2} = ['Zirconium metallic'];

rho(3,:) = rho_FE(T);
leg{3} = ['Iron'];

rho(4,:) = rho_SS304(T);
leg{4} = ['SS304'];

rho(5,:) = rho_ZRO2(T);
leg{5} = ['ZrO2'];

for i=2:N
plot(T,rho(i,:),'-.','LineWidth',2);
end
legend(leg(2:end));



%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Tmin = 273.0;
Tmax = 4000.0;
div = 100;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%B2O3
figure(5)
hold on; grid on; box on;
ylabel('Density [g/cc]'); xlabel('Temperature, [K]');
T = []; rho=[]; T = linspace(Tmin,Tmax,div);
hold on
N=2;

rho(1,:) = rho_B2O3(T,'Kolev');
leg{1} = ['B2O3 Kolev'];

rho(2,:) = rho_B2O3(T,'Exp');
leg{2} = ['B2O3 Exp'];

%axis([min(T) max(T) 0.0 ceil(max(rho(:)))]);
%axis([.0 max(T) 0.0 ceil(max(rho(:)))]);
axis([.0 max(T) 0.0 1+ceil(max(rho(:)))]);
for i=1:N
	plot(T,rho(i,:),'.-','LineWidth',2);
end
legend(leg(1:end),'Location','NorthEast');

filename = 'B2O3';
savefig([folder, filename]);
print([folder, filename],'-dtiff');

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%SS
figure(6)
hold on; grid on; box on; ylabel('Density [g/cc]'); xlabel('Temperature, [K]');
T = []; rho=[]; T = linspace(Tmin,Tmax,div);
hold on
N=3;

rho(1,:) = rho_SS(T,'Kolev');
leg{1} = ['Steel Kolev'];

rho(2,:) = rho_SS(T,'Powers');
leg{2} = ['Steel Powers'];

rho(3,:) = rho_SS(T,'MELCOR');
leg{3} = ['Steel MELCOR'];

%axis([min(T) max(T) 0.0 ceil(max(rho(:)))]);
axis([.0 max(T) 0.0 1+ceil(max(rho(:)))]);
for i=1:N
	plot(T,rho(i,:),'.-','LineWidth',2);
end
legend(leg(1:end),'Location','NorthEast');

filename = 'Steel';
savefig([folder, filename]);
print([folder, filename],'-dtiff');

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%UO2
figure(7)
hold on; grid on; box on; ylabel('Density [g/cc]'); xlabel('Temperature, [K]');
T = []; rho=[]; T = linspace(Tmin,Tmax,div);
hold on
N=4;
rho(1,:) = rho_UO2(T,'Carbajo');
leg{1} = ['UO2 Carbajo'];


rho(2,:) = rho_UO2(T,'Powers');
leg{2} = ['UO2 Powers'];

rho(3,:) = rho_UO2(T,'MELCOR');
leg{3} = ['UO2 MELCOR'];

rho(4,:) = rho_UO2(T,'Kolev');
leg{4} = ['UO2 Kolev'];

axis([.0 max(T) 0.0 1+ceil(max(rho(:)))]);
for i=1:N
plot(T,rho(i,:),'.-','LineWidth',2);
end
legend(leg(1:end),'Location','NorthEast');

filename = 'UO2';
savefig([folder, filename]);
print([folder, filename],'-dtiff');

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Tmin = 0.0;
Tmax = 4000.0;
div = 100;


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ZRO2
figure(8)
hold on; grid on; box on; ylabel('Density [g/cc]'); xlabel('Temperature, [K]');
T = []; rho=[]; T = linspace(Tmin,Tmax,div);
hold on
N=3;

rho(1,:) = rho_ZRO2(T,'Kolev');
leg{1} = ['ZrO2 Kolev/MATPRO'];

rho(2,:) = rho_ZRO2(T,'Powers');
leg{2} = ['ZrO2 Powers'];

rho(3,:) = rho_ZRO2(T,'MELCOR');
leg{3} = ['ZrO2 MELCOR'];

%axis([min(T) max(T) 0.0 ceil(max(rho(:)))]);
axis([.0 max(T) 0.0 1+ceil(max(rho(:)))]);
for i=1:N
plot(T,rho(i,:),'.-','LineWidth',2);
end
legend(leg(1:end),'Location','NorthEast');

filename = 'ZRO2';
savefig([folder, filename]);
print([folder, filename],'-dtiff');

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ZR
figure(9)
hold on; grid on; box on; ylabel('Density [g/cc]'); xlabel('Temperature, [K]');
T = []; rho=[]; T = linspace(Tmin,Tmax,div);
hold on
N=3;

rho(1,:) = rho_ZR(T,'Kolev');
leg{1} = ['Zr Kolev'];

rho(2,:) = rho_ZR(T,'Powers');
leg{2} = ['Zr Powers'];

rho(3,:) = rho_ZR(T,'MELCOR');
leg{3} = ['Zr MELCOR'];

%axis([min(T) max(T) 0.0 ceil(max(rho(:)))]);
axis([.0 max(T) 0.0 1+ceil(max(rho(:)))]);
for i=1:N
plot(T,rho(i,:),'.-','LineWidth',2);
end
legend(leg(1:end),'Location','NorthEast');

filename = 'ZR';
savefig([folder, filename]);
print([folder, filename],'-dtiff');

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FeO or SSOX
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Tmin = 273.0;
Tmax = 4000.0;
div = 100;


figure(10)
hold on; grid on; box on; ylabel('Density [g/cc]'); xlabel('Temperature, [K]');
T = []; rho=[]; T = linspace(Tmin,Tmax,div);
hold on
N=3;

rho(1,:) = rho_SSOX(T,'Kolev');
leg{1} = ['FeO+ Kolev'];

rho(2,:) = rho_SSOX(T,'MELCOR');
leg{2} = ['SSOX MELCOR'];

rho(3,:) = rho_SSOX(T,'MATPRO');
leg{3} = ['SSOX MATPRO'];

%axis([min(T) max(T) 0.0 ceil(max(rho(:)))]);
axis([.0 max(T) 0.0 1+ceil(max(rho(:)))]);
for i=1:N
plot(T,rho(i,:),'.-','LineWidth',2);
end
legend(leg(1:end),'Location','NorthEast');

filename = 'SSOX';
savefig([folder, filename]);
print([folder, filename],'-dtiff');

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% U metalic
Tmin = 273.15;
Tmax = 4000.0;
div = 100;

figure(11)
hold on; grid on; box on; ylabel('Density [g/cc]'); xlabel('Temperature, [K]');
T = []; rho=[]; T = linspace(Tmin,Tmax,div);
hold on
N=2;

rho(1,:) = rho_U(T,'Powers');
leg{1} = ['U metallic Powers'];

rho(2,:) = rho_U(T,'MELCOR');
leg{2} = ['U metallic MELCOR'];


%axis([min(T) max(T) 0.0 ceil(max(rho(:)))]);
axis([.0 max(T) 0.0 1+ceil(max(rho(:)))]);
for i=1:N
plot(T,rho(i,:),'.-','LineWidth',2);
end
legend(leg(1:end),'Location','NorthEast');

filename = 'U';
savefig([folder, filename]);
print([folder, filename],'-dtiff');

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% B4C
figure(12)
hold on; grid on; box on; ylabel('Density [g/cc]'); xlabel('Temperature, [K]');
T = []; rho=[]; T = linspace(Tmin,Tmax,div);
hold on
N=2;

rho(1,:) = rho_B4C(T,'MATPRO');
leg{1} = ['B4C MATPRO'];

rho(2,:) = rho_B4C(T,'MELCOR');
leg{2} = ['B4C MELCOR'];

%axis([min(T) max(T) 0.0 ceil(max(rho(:)))]);
axis([.0 max(T) 0.0 1+ceil(max(rho(:)))]);
for i=1:N
	plot(T,rho(i,:),'.-','LineWidth',2);
end
legend(leg(1:end),'Location','NorthEast');

filename = 'B4C';
savefig([folder, filename]);
print([folder, filename],'-dtiff');

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PuO2
Tmin = 273.15;
Tmax = 4000.0;
div = 100;

figure(13)
hold on; grid on; box on; ylabel('Density [g/cc]'); xlabel('Temperature, [K]');
T = []; rho=[]; T = linspace(Tmin,Tmax,div);
hold on
N=1;

rho(1,:) = rho_PuO2(T,'Carbajo');
leg{1} = ['PuO2 Carbajo'];


%axis([min(T) max(T) 0.0 ceil(max(rho(:)))]);
axis([.0 max(T) 0.0 2+ceil(max(rho(:)))]);
for i=1:N
	plot(T,rho(i,:),'.-','LineWidth',2);
end
legend(leg(1:end),'Location','NorthEast');

filename = 'PuO2';
savefig([folder, filename]);
print([folder, filename],'-dtiff');

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%











%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%










%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%