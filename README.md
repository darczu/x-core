% VERSION 0.0.0.9 21-02-2021; 
Author:  Piotr Darnowski 2021  piotr.darnowski@pw.edu.pl   https://gitlab.com/darczu/x-core
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
X-Core (XCore) - MATLAB TOOLBOX
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
X-Core Nuclear Engineering Toolbox MATLAB Toolbox/Package of functions with material properties, 
geometry and other functions useful for Nuclear Engineering, Nuclear Reactor Safety and Severe Accident Analysis
Previously called CETbox. Bascially it is collections of matlab scripts which I used in my research.
Developed by Piotr Darnowski in 2016/2017/2018/2019/2020/2021 
Several functions were developed in the framework of the NARSIS Horizon 2020 Research Project Funded by EU at Warsaw University of Technology, Institute of Heat Engineering, 
The XCore is partially based on subroutines developed for SARCAM code developed in 2015-2018.
Initial intention was to create package allowing to calculate material propoties, i.e. atomic number densities for different corium mixture and crticiality calculations. 
Package is under constant development. I hope to add object oriented functionalities but it is matter of future.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
IT IS BETA VERSION. MANY FUNCTIONALITIES ARE NOT AVAILABLE OR NOT WORKING PROPERLY.
MANUAL IS STILL NOT READY. BASICALLY MOST FUNCTIONS ARE DESCRIBED IN THE SOURCE CODE.
AUTHOR TAKE NO RESPONSIBILITY FOR THE RESULTS OF THIS SOFTWARE
IN THIS TOOLBOX SOME EXTERNAL SCRIPTS WERE ADD TO INCREASE TOOLBOX FUNCTIONALITY. PROPER LICENSES WERE ADDED. IN CASE OF LICENSE ISSUES CONTACT ME.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Reports bugs to: piotr.darnowski@pw.edu.pl  
Code site:	https://gitlab.com/darczu/x-core
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% HOW TO CITE AND MANUAL
Cite: P. Darnowski, X-Core Code Manual, 2021.
@misc{[XCore] , title = "X-Core Code Manual", author={Darnowski, P}, year={2021}, url={https://gitlab.com/darczu/x-core} }
WARNING: MANUAL IS STILL NOT READY 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
X-Core was developed with MATLAB but it works also with OCTAVE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Basic Files:
XCore.m				main function - actual X-Core. XCore.m  is actual code. It can be used to load package and run basic material databse functions
INPUT_SETUP.m 		file with basic options for the XCore.
MAIN_XCore.m		testing script
Example_basic.m             script with basic usage
Example_RHO_PLOTTER_Rev1.m	script useful for plotting and testing results
Example_TH.m                script with some examples for flow path calculations
Other examples are in source code or separate folders.

README.md 			readme file
LICENSE.md			license file

Folders:
/Dcoumentation/             Manual to be added
/ToolBoxInstallationFile/   Installation - to be added
/Modules/                   All basic functions  
/Modules/
/Modules/CommonFunction/        Various useful functions, ploting, files operations, coloring etc.
/Modules/Complexfunction/       Various function dedicated for different purposes, mainly complex materials operations. E.g. Calculations of corium mixture density, atomic number density, etc. Most of them are dediacted for neutronics calcs.
/Modules/EBEPU/                 To be added
/Modules/MaterialDatabase/      Basic material proporties, e.g. densities, spec. heat capacities etc.
/Modules/MLC_package/           MELCOR package, including pre-processing, running, post-processing and senstivity and uncertanity
/Modules/ReactorPhysics/        Reactor Physics package, currently boron calculations, power density visualization, etc.
/Modules/ThermalHydrualics/     Functions for hydraulics, critical flow, criterial numbers, heat transfer etc.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INSTALLATION 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OPTION 1 % Recommended:
% The simpliest:  % Use command (below) to load all data to matlab within you folder with downloaded package:
% Type:
XCore()
% It will automatically load current folder with all subfolders (option in INPUT_SETUP.m) using addpath function

% OPTION 2
% Insert this line to your code to add package (change to your path):
%addpath(genpath('D:\NARSIS_DOCS\MODELE_AND_RESEARCH\SCRIPTS\MATLAB_XCore\x-core'));  % Add X-Core
addpath(genpath('D:\....your_path.....\x-core'));  % Add X-Core

% OPTION 3
You can also add all the catalog as MatLab Toolbox (you have to do it once). Use matlab GUI
Go to Matlab  File->AddPath-> Add folder with x-core and subfolders     and save changes

% OPTION 4
In the future I will create MATLAB Toolbox installation file

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% USAGE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
XCore('Command',parameters)  % It has limited applciablity only to material propotires.
Other functions have to be used directly.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXAMPLES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Many functions has examples in source code.
Example_basic.m             script with basic usage
Example_RHO_PLOTTER_Rev1.m	script useful for plotting and testing results
Example_TH.m                script with some examples for flow path calculations

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OTHER PACKAGES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
X-Steam Package with Steam-Tables was added
Shadded Plot was added
other also can be found
I am using these codes as found in the Internet. These are not mine, but are publicly available. I have added proper licenses to folder with these scripts.
In case of problems with license please contact piotr.darnowski@pw.edu.pl 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



