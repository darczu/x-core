%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MAIN_XCore.m  TESTING SCRIPT - LOAD X-CORE - Revision 20.02.2021
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SEE README !
% X-Core (XCore) -  MATLAB Toolbox/Set of functions with material properties and other functions useful for Severe Accident Analysis Previously called CETbox
% Piotr Darnowski 2021 Warsaw University of Technology, Institute of Heat Engineering, All rights reserved
% Developed for NARSIS H2020 Project 2017-2021
% Based on SARCAM-LIV code developed in 2015-2017
% Reports bugs: piotr.darnowski@pw.edu.pl
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% XCore.m  is an actual code
% X-Core was developed with MATLAB but it also works with OCTAVE
% Currently XCore.m has limited usability - only density and specific heat capacity
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Basic usage: see Examples_Basic.m
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic; close all; clear; clc;  						%Timer, Workspace, format and screen cleaning

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Documentation
addpath('./Documentation/'); 
run HISTORY.m; run REFERENCES.m; 	                 	%File with code history and references library 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load the toolbox. You can use it instead of this function
% It is not needed when toolbox added as paths.
% Details in Readme
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
XCore();  %load X-Core Data, it includs INPUT_SETUP.m which load all paths
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Example 1: Density of Uranium Dioxide in 2500K - both produce same result
rho = XCore('rho_UO2',[2500]) 
rho = rho_UO2(2500)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Example 2: Density of Uranium Dioxide as a function of Temperature - calculated with Carbajo correlations.
T=linspace(300,3000);  
figure(1)
plot(T, XCore('rho_UO2',T,'Carbajo'),'.r-')
hold on;
plot(T,rho_UO2(T,'Carbajo'),'bs')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
