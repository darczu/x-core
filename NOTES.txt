2023Change to :

XCore.ModuleName.Function(parameters)



https://uk.mathworks.com/matlabcentral/answers/438218-many-plots-in-object-oriented-way

git rm --cached `git ls-files -i --exclude-from=.gitignore` 
git commit -m 'Removed all files that are in the .gitignore' 
git push origin master


 git rm -r --cached . 
 git add .
 git commit -m 'Removed all files that are in the .gitignore' 
 git push origin master



file=imread('sample.png');  
imwrite(file,'sample2.tiff','tiff');

! ADD possiblity to use
https://www.nist.gov/srd/refprop
http://www.coolprop.org/
https://www.mathworks.com/help/physmod/simscape/ref/twophasefluidtables.html

!                 tfname        tfscal        
TF_ID     'THC FUEL GAP'           1.0
!      size
TF_TAB   27 ! n             x             y
              1         294.3         0.158
              2         422.0         0.196
              3        533.15         0.231
              4         644.3         0.264
              5         755.4         0.296
              6         866.5         0.326
              7         977.6         0.355
              8        1088.7         0.383
              9        1199.8          0.41
             10        1310.9         0.437
             11        1422.0         0.463
             12       1533.15         0.488
             13        1644.3         0.513
             14        1755.4         0.537
             15        1866.5         0.561
             16        1977.6         0.584
             17        2088.7         0.607
             18        2200.0          0.63
             19        2310.9         0.653
             20        2422.0         0.675
             21       2533.15         0.696
             22        2644.3         0.718
             23        2755.4         0.739
             24        2866.5          0.76
             25        2977.6         0.781
             26        3088.7         0.802
             27        3200.0         0.822
			 
			 !                 tfname        tfscal        
TF_ID     'CPS FUEL GAP'           1.0
!      size
TF_TAB    2 !n             x             y
             1        273.15          5.03
             2        3300.0          5.03
!

!
!cc: 87
!                 tfname        tfscal        
TF_ID     'RHO FUEL GAP'           1.0
!      size
TF_TAB    1 !n             x             y
             1        273.15           1.0
!





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OLD JUNK
	% input type: cell
	%if(strcmp(class(in),'cell'))
	%	switch N
	%		case 1
	%			in1 = cell2mat(in(1)); 
	%		case 2	
	%			in1 = cell2mat(in(1)); 
	%			in2 = cell2mat(in(2)); 
	%		case 3 
	%			in1 = cell2mat(in(1)); 
	%			in2 = cell2mat(in(2)); 
	%			in3 = cell2mat(in(3)); 
	%		case 4
	%			in1 = cell2mat(in(1)); 
	%			in2 = cell2mat(in(2)); 
	%			in3 = cell2mat(in(3)); 
	%			in4 = cell2mat(in(4));
	%		case 5
	%			in1 = cell2mat(in(1)); 
	%			in2 = cell2mat(in(2)); 
	%			in3 = cell2mat(in(3)); 
	%			in4 = cell2mat(in(4));
	%			in5 = cell2mat(in(5));
	%		otherwise
	%			error('Too much input variables...')
	%	end
	%end
	% input type: matrix/vector
	%if(strcmp(class(in),'double'))
	%	switch N
	%		case 1
	%			in1 = in(1); 
	%		case 2	
	%			in1 = in(1); 
	%			in2 = in(2); 
	%		case 3 
	%			in1 = in(1); 
	%			in2 = in(2); 
	%			in3 = in(3); 
	%		case 4
	%			in1 = in(1); 
	%			in2 = in(2); 
	%			in3 = in(3); 
	%			in4 = in(4);
	%		case 5
	%			in1 = in(1); 
	%			in2 = in(2); 
	%			in3 = in(3); 
	%			in4 = in(4);
	%			in5 = in(5);
	%		otherwise
	%			error('Too much input variables...')
	%		end
	%end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	